import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public final class class180 extends class315 implements Runnable {

   boolean field2090 = false;
   int field2087 = 0;
   int field2088 = 0;
   boolean field2089 = false;
   class175 field2086;
   Socket field2082;
   final int field2084;
   final int field2080;
   InputStream field2085;
   OutputStream field2081;
   byte[] field2091;
   class174 field2083;

   public class180(Socket socket_1, class175 class175_2, int i_3) throws IOException {
      this.field2086 = class175_2;
      this.field2082 = socket_1;
      this.field2084 = i_3;
      this.field2080 = i_3 - 100;
      this.field2082.setSoTimeout(30000);
      this.field2082.setTcpNoDelay(true);
      this.field2082.setReceiveBufferSize(65536);
      this.field2082.setSendBufferSize(65536);
      this.field2085 = this.field2082.getInputStream();
      this.field2081 = this.field2082.getOutputStream();
   }

   void method3560(byte[] bytes_1, int i_2, int i_3, int i_4) throws IOException {
      if (!this.field2090) {
         if (this.field2089) {
            this.field2089 = false;
            throw new IOException();
         } else {
            if (this.field2091 == null) {
               this.field2091 = new byte[this.field2084];
            }

            synchronized(this) {
               for (int i_6 = 0; i_6 < i_3; i_6++) {
                  this.field2091[this.field2088] = bytes_1[i_6 + i_2];
                  this.field2088 = (this.field2088 + 1) % this.field2084;
                  if ((this.field2080 + this.field2087) % this.field2084 == this.field2088) {
                     throw new IOException();
                  }
               }

               if (this.field2083 == null) {
                  this.field2083 = this.field2086.method3521(this, 3, -1553715037);
               }

               this.notifyAll();
            }
         }
      }
   }

   public void vmethod5831(byte b_1) {
      if (!this.field2090) {
         synchronized(this) {
            this.field2090 = true;
            this.notifyAll();
         }

         if (this.field2083 != null) {
            while (this.field2083.field2045 == 0) {
               class236.method4132(1L);
            }

            if (this.field2083.field2045 == 1) {
               try {
                  ((Thread) this.field2083.field2047).join();
               } catch (InterruptedException interruptedexception_4) {
                  ;
               }
            }
         }

         this.field2083 = null;
      }
   }

   public boolean vmethod5836(int i_1, byte b_2) throws IOException {
      return this.field2090 ? false : this.field2085.available() >= i_1;
   }

   public int vmethod5830(byte[] bytes_1, int i_2, int i_3, byte b_4) throws IOException {
      if (this.field2090) {
         return 0;
      } else {
         int i_5;
         int i_6;
         for (i_5 = i_3; i_3 > 0; i_3 -= i_6) {
            i_6 = this.field2085.read(bytes_1, i_2, i_3);
            if (i_6 <= 0) {
               throw new EOFException();
            }

            i_2 += i_6;
         }

         return i_5;
      }
   }

   public void vmethod5844(byte[] bytes_1, int i_2, int i_3, int i_4) throws IOException {
      this.method3560(bytes_1, i_2, i_3, 2119529681);
   }

   public int vmethod5828(int i_1) throws IOException {
      return this.field2090 ? 0 : this.field2085.available();
   }

   public int vmethod5834(byte b_1) throws IOException {
      return this.field2090 ? 0 : this.field2085.read();
   }

   public void run() {
      try {
         while (true) {
            label84: {
               int i_1;
               int i_2;
               synchronized(this) {
                  if (this.field2087 == this.field2088) {
                     if (this.field2090) {
                        break label84;
                     }

                     try {
                        this.wait();
                     } catch (InterruptedException interruptedexception_10) {
                        ;
                     }
                  }

                  i_2 = this.field2087;
                  if (this.field2088 >= this.field2087) {
                     i_1 = this.field2088 - this.field2087;
                  } else {
                     i_1 = this.field2084 - this.field2087;
                  }
               }

               if (i_1 <= 0) {
                  continue;
               }

               try {
                  this.field2081.write(this.field2091, i_2, i_1);
               } catch (IOException ioexception_9) {
                  this.field2089 = true;
               }

               this.field2087 = (i_1 + this.field2087) % this.field2084;

               try {
                  if (this.field2088 == this.field2087) {
                     this.field2081.flush();
                  }
               } catch (IOException ioexception_8) {
                  this.field2089 = true;
               }
               continue;
            }

            try {
               if (this.field2085 != null) {
                  this.field2085.close();
               }

               if (this.field2081 != null) {
                  this.field2081.close();
               }

               if (this.field2082 != null) {
                  this.field2082.close();
               }
            } catch (IOException ioexception_7) {
               ;
            }

            this.field2091 = null;
            break;
         }
      } catch (Exception exception_12) {
         class223.method4011((String) null, exception_12, 692473873);
      }

   }

   protected void finalize() {
      this.vmethod5831((byte) -10);
   }

   protected void abo() {
      this.vmethod5831((byte) -15);
   }

   static final class326 method3576(class310 class310_0, class326 class326_1, int i_2) {
      int i_3 = class310_0.method5661((byte) -30);
      int i_4;
      if (class326_1 == null) {
         i_4 = class80.method1842(i_3, 1029726685);
         class326_1 = new class326(i_4);
      }

      for (i_4 = 0; i_4 < i_3; i_4++) {
         boolean bool_5 = class310_0.method5661((byte) -13) == 1;
         int i_6 = class310_0.method5506((short) 239);
         Object obj_7;
         if (bool_5) {
            obj_7 = new class185(class310_0.method5589(-1679972801));
         } else {
            obj_7 = new class188(class310_0.method5507(-1413085602));
         }

         class326_1.method5928((class189) obj_7, (long)i_6);
      }

      return class326_1;
   }

   static final void method3579(int i_0) {
      if (class151.field1949 != client.field874) {
         client.field874 = class151.field1949;
         int i_1 = class151.field1949;
         int[] ints_2 = class25.field126.field3917;
         int i_3 = ints_2.length;

         int i_4;
         for (i_4 = 0; i_4 < i_3; i_4++) {
            ints_2[i_4] = 0;
         }

         int i_5;
         int i_6;
         for (i_4 = 1; i_4 < 103; i_4++) {
            i_5 = (103 - i_4) * 2048 + 24628;

            for (i_6 = 1; i_6 < 103; i_6++) {
               if ((class64.field507[i_1][i_6][i_4] & 0x18) == 0) {
                  class67.field536.method3165(ints_2, i_5, 512, i_1, i_6, i_4);
               }

               if (i_1 < 3 && (class64.field507[i_1 + 1][i_6][i_4] & 0x8) != 0) {
                  class67.field536.method3165(ints_2, i_5, 512, i_1 + 1, i_6, i_4);
               }

               i_5 += 4;
            }
         }

         i_4 = (238 + (int)(Math.random() * 20.0D) - 10 << 16) + (238 + (int)(Math.random() * 20.0D) - 10 << 8) + (238 + (int)(Math.random() * 20.0D) - 10);
         i_5 = 238 + (int)(Math.random() * 20.0D) - 10 << 16;
         class25.field126.method6117();

         int i_7;
         for (i_6 = 1; i_6 < 103; i_6++) {
            for (i_7 = 1; i_7 < 103; i_7++) {
               if ((class64.field507[i_1][i_7][i_6] & 0x18) == 0) {
                  class1.method5(i_1, i_7, i_6, i_4, i_5, (byte) 3);
               }

               if (i_1 < 3 && (class64.field507[i_1 + 1][i_7][i_6] & 0x8) != 0) {
                  class1.method5(i_1 + 1, i_7, i_6, i_4, i_5, (byte) 3);
               }
            }
         }

         client.field756 = 0;

         for (i_6 = 0; i_6 < 104; i_6++) {
            for (i_7 = 0; i_7 < 104; i_7++) {
               long long_8 = class67.field536.method3160(class151.field1949, i_6, i_7);
               if (long_8 != 0L) {
                  int i_10 = class149.method3348(long_8);
                  int i_11 = class34.method383(i_10, 110938948).field3414;
                  if (i_11 >= 0 && class163.method3458(i_11, -1778501776).field3263) {
                     client.field878[client.field756] = class163.method3458(i_11, -1890678962).method4374(false, -375882692);
                     client.field717[client.field756] = i_6;
                     client.field877[client.field756] = i_7;
                     ++client.field756;
                  }
               }
            }
         }

         class27.field150.method6075(11265563);
      }

   }

   static int method3559(int i_0, int i_1, byte b_2) {
      int i_3 = i_1 - 334;
      if (i_3 < 0) {
         i_3 = 0;
      } else if (i_3 > 100) {
         i_3 = 100;
      }

      int i_4 = (client.field899 - client.field914) * i_3 / 100 + client.field914;
      return i_0 * i_4 / 256;
   }

   public static class333[] method3573(byte b_0) {
      return new class333[] {class333.field3897, class333.field3898, class333.field3901};
   }

}
