import java.io.File;
import java.io.RandomAccessFile;

public class class143 {

   public static int field1892;

   public static File method3300(String string_0, int i_1) {
      if (!class178.field2076) {
         throw new RuntimeException("");
      } else {
         File file_2 = (File) class178.field2077.get(string_0);
         if (file_2 != null) {
            return file_2;
         } else {
            File file_3 = new File(class178.field2072, string_0);
            RandomAccessFile randomaccessfile_4 = null;

            try {
               File file_5 = new File(file_3.getParent());
               if (!file_5.exists()) {
                  throw new RuntimeException("");
               } else {
                  randomaccessfile_4 = new RandomAccessFile(file_3, "rw");
                  int i_6 = randomaccessfile_4.read();
                  randomaccessfile_4.seek(0L);
                  randomaccessfile_4.write(i_6);
                  randomaccessfile_4.seek(0L);
                  randomaccessfile_4.close();
                  class178.field2077.put(string_0, file_3);
                  return file_3;
               }
            } catch (Exception exception_9) {
               try {
                  if (randomaccessfile_4 != null) {
                     randomaccessfile_4.close();
                     randomaccessfile_4 = null;
                  }
               } catch (Exception exception_8) {
                  ;
               }

               throw new RuntimeException();
            }
         }
      }
   }

   public static void method3303(class244 class244_0, class244 class244_1, int i_2) {
      class254.field3285 = class244_0;
      class254.field3271 = class244_1;
   }

   static final String method3301(int i_0, int i_1) {
      return i_0 < 999999999 ? Integer.toString(i_0) : "*";
   }

   public static void method3302(class244 class244_0, int i_1) {
      class253.field3266 = class244_0;
   }

}
