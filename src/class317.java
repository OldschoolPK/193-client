import java.io.IOException;
import java.io.OutputStream;

public class class317 implements Runnable {

   int field3816 = 0;
   int field3817 = 0;
   OutputStream field3813;
   int field3812;
   byte[] field3814;
   Thread field3815;
   boolean field3819;
   IOException field3818;

   class317(OutputStream outputstream_1, int i_2) {
      this.field3813 = outputstream_1;
      this.field3812 = i_2 + 1;
      this.field3814 = new byte[this.field3812];
      this.field3815 = new Thread(this);
      this.field3815.setDaemon(true);
      this.field3815.start();
   }

   void method5849(byte[] bytes_1, int i_2, int i_3, int i_4) throws IOException {
      if (i_3 >= 0 && i_2 >= 0 && i_3 + i_2 <= bytes_1.length) {
         synchronized(this) {
            if (this.field3818 != null) {
               throw new IOException(this.field3818.toString());
            } else {
               int i_6;
               if (this.field3816 <= this.field3817) {
                  i_6 = this.field3812 - this.field3817 + this.field3816 - 1;
               } else {
                  i_6 = this.field3816 - this.field3817 - 1;
               }

               if (i_6 < i_3) {
                  throw new IOException("");
               } else {
                  if (i_3 + this.field3817 <= this.field3812) {
                     System.arraycopy(bytes_1, i_2, this.field3814, this.field3817, i_3);
                  } else {
                     int i_7 = this.field3812 - this.field3817;
                     System.arraycopy(bytes_1, i_2, this.field3814, this.field3817, i_7);
                     System.arraycopy(bytes_1, i_7 + i_2, this.field3814, 0, i_3 - i_7);
                  }

                  this.field3817 = (i_3 + this.field3817) % this.field3812;
                  this.notifyAll();
               }
            }
         }
      } else {
         throw new IOException();
      }
   }

   void method5857(byte b_1) {
      synchronized(this) {
         this.field3819 = true;
         this.notifyAll();
      }

      try {
         this.field3815.join();
      } catch (InterruptedException interruptedexception_4) {
         ;
      }

   }

   boolean method5847(int i_1) {
      if (this.field3819) {
         try {
            this.field3813.close();
            if (this.field3818 == null) {
               this.field3818 = new IOException("");
            }
         } catch (IOException ioexception_3) {
            if (this.field3818 == null) {
               this.field3818 = new IOException(ioexception_3);
            }
         }

         return true;
      } else {
         return false;
      }
   }

   public void run() {
      do {
         int i_1;
         synchronized(this) {
            while (true) {
               if (this.field3818 != null) {
                  return;
               }

               if (this.field3816 <= this.field3817) {
                  i_1 = this.field3817 - this.field3816;
               } else {
                  i_1 = this.field3812 - this.field3816 + this.field3817;
               }

               if (i_1 > 0) {
                  break;
               }

               try {
                  this.field3813.flush();
               } catch (IOException ioexception_11) {
                  this.field3818 = ioexception_11;
                  return;
               }

               if (this.method5847(459105101)) {
                  return;
               }

               try {
                  this.wait();
               } catch (InterruptedException interruptedexception_12) {
                  ;
               }
            }
         }

         try {
            if (i_1 + this.field3816 <= this.field3812) {
               this.field3813.write(this.field3814, this.field3816, i_1);
            } else {
               int i_7 = this.field3812 - this.field3816;
               this.field3813.write(this.field3814, this.field3816, i_7);
               this.field3813.write(this.field3814, 0, i_1 - i_7);
            }
         } catch (IOException ioexception_10) {
            IOException ioexception_2 = ioexception_10;
            synchronized(this) {
               this.field3818 = ioexception_2;
               return;
            }
         }

         synchronized(this) {
            this.field3816 = (i_1 + this.field3816) % this.field3812;
         }
      } while (!this.method5847(-1898190098));

   }

   public static class7[] method5848(int i_0) {
      return new class7[] {class7.field25};
   }

}
