public class class67 {

   static class142 field536;
   int field537 = 0;
   final class246 field532;
   final int field534;

   class67(class246 class246_1, String string_2) {
      this.field532 = class246_1;
      this.field534 = class246_1.method4173(1967551838);
   }

   boolean method1228(int i_1) {
      this.field537 = 0;

      for (int i_2 = 0; i_2 < this.field534; i_2++) {
         if (!this.field532.method4272(i_2, (byte) 2) || this.field532.method4286(i_2, -215383381)) {
            ++this.field537;
         }
      }

      return this.field537 >= this.field534;
   }

   static String method1230(int i_0, int i_1) {
      return i_0 < 0 ? "" : (client.field791[i_0].length() > 0 ? client.field790[i_0] + " " + client.field791[i_0] : client.field790[i_0]);
   }

   public static class240 method1225(int i_0, byte b_1) {
      class240[] arr_2 = class1.method11((byte) 0);

      for (int i_3 = 0; i_3 < arr_2.length; i_3++) {
         class240 class240_4 = arr_2[i_3];
         if (i_0 == class240_4.field3134) {
            return class240_4;
         }
      }

      return null;
   }

   static final int method1232(class348 class348_0, int i_1) {
      if (class348_0 == null) {
         return 12;
      } else {
         switch(class348_0.field4067) {
         case 2:
            return 20;
         default:
            return 12;
         }
      }
   }

   static void method1231(class305 class305_0, class305 class305_1, class305 class305_2, int i_3) {
      class94.field1188 = (class286.field3638 - 765) / 2;
      class94.field1185 = class94.field1188 + 202;
      class94.field1186 = class94.field1185 + 180;
      byte b_4;
      int i_5;
      int i_7;
      int i_9;
      int i_10;
      int i_12;
      int i_33;
      int i_37;
      if (class94.field1198) {
         if (class301.field3697 == null) {
            class301.field3697 = class280.method4975(class100.field1291, "sl_back", "", (byte) 0);
         }

         if (class90.field1157 == null) {
            class90.field1157 = class89.method2095(class100.field1291, "sl_flags", "", -1519109689);
         }

         if (class29.field168 == null) {
            class29.field168 = class89.method2095(class100.field1291, "sl_arrows", "", -1397234071);
         }

         if (class349.field4076 == null) {
            class349.field4076 = class89.method2095(class100.field1291, "sl_stars", "", -973703137);
         }

         if (class249.field3224 == null) {
            class249.field3224 = class51.method836(class100.field1291, "leftarrow", "", -973430603);
         }

         if (class8.field34 == null) {
            class8.field34 = class51.method836(class100.field1291, "rightarrow", "", -1795731871);
         }

         class331.method6004(class94.field1188, 23, 765, 480, 0);
         class331.method6005(class94.field1188, 0, 125, 23, 12425273, 9135624);
         class331.method6005(class94.field1188 + 125, 0, 640, 23, 5197647, 2697513);
         class305_0.method5436("Select a world", class94.field1188 + 62, 15, 0, -1);
         if (class349.field4076 != null) {
            class349.field4076[1].method6091(class94.field1188 + 140, 1);
            class305_1.method5335("Members only world", class94.field1188 + 152, 10, 16777215, -1);
            class349.field4076[0].method6091(class94.field1188 + 140, 12);
            class305_1.method5335("Free world", class94.field1188 + 152, 21, 16777215, -1);
         }

         int i_38;
         if (class29.field168 != null) {
            i_33 = class94.field1188 + 280;
            if (class80.field1016[0] == 0 && class80.field1015[0] == 0) {
               class29.field168[2].method6091(i_33, 4);
            } else {
               class29.field168[0].method6091(i_33, 4);
            }

            if (class80.field1016[0] == 0 && class80.field1015[0] == 1) {
               class29.field168[3].method6091(i_33 + 15, 4);
            } else {
               class29.field168[1].method6091(i_33 + 15, 4);
            }

            class305_0.method5335("World", i_33 + 32, 17, 16777215, -1);
            i_5 = class94.field1188 + 390;
            if (class80.field1016[0] == 1 && class80.field1015[0] == 0) {
               class29.field168[2].method6091(i_5, 4);
            } else {
               class29.field168[0].method6091(i_5, 4);
            }

            if (class80.field1016[0] == 1 && class80.field1015[0] == 1) {
               class29.field168[3].method6091(i_5 + 15, 4);
            } else {
               class29.field168[1].method6091(i_5 + 15, 4);
            }

            class305_0.method5335("Players", i_5 + 32, 17, 16777215, -1);
            i_38 = class94.field1188 + 500;
            if (class80.field1016[0] == 2 && class80.field1015[0] == 0) {
               class29.field168[2].method6091(i_38, 4);
            } else {
               class29.field168[0].method6091(i_38, 4);
            }

            if (class80.field1016[0] == 2 && class80.field1015[0] == 1) {
               class29.field168[3].method6091(i_38 + 15, 4);
            } else {
               class29.field168[1].method6091(i_38 + 15, 4);
            }

            class305_0.method5335("Location", i_38 + 32, 17, 16777215, -1);
            i_7 = class94.field1188 + 610;
            if (class80.field1016[0] == 3 && class80.field1015[0] == 0) {
               class29.field168[2].method6091(i_7, 4);
            } else {
               class29.field168[0].method6091(i_7, 4);
            }

            if (class80.field1016[0] == 3 && class80.field1015[0] == 1) {
               class29.field168[3].method6091(i_7 + 15, 4);
            } else {
               class29.field168[1].method6091(i_7 + 15, 4);
            }

            class305_0.method5335("Type", i_7 + 32, 17, 16777215, -1);
         }

         class331.method6004(class94.field1188 + 708, 4, 50, 16, 0);
         class305_1.method5436("Cancel", class94.field1188 + 708 + 25, 16, 16777215, -1);
         class94.field1206 = -1;
         if (class301.field3697 != null) {
            b_4 = 88;
            byte b_40 = 19;
            i_38 = 765 / (b_4 + 1) - 1;
            i_7 = 480 / (b_40 + 1);

            do {
               i_37 = i_7;
               i_9 = i_38;
               if (i_7 * (i_38 - 1) >= class80.field1017) {
                  --i_38;
               }

               if (i_38 * (i_7 - 1) >= class80.field1017) {
                  --i_7;
               }

               if (i_38 * (i_7 - 1) >= class80.field1017) {
                  --i_7;
               }
            } while (i_37 != i_7 || i_9 != i_38);

            i_37 = (765 - b_4 * i_38) / (i_38 + 1);
            if (i_37 > 5) {
               i_37 = 5;
            }

            i_9 = (480 - b_40 * i_7) / (i_7 + 1);
            if (i_9 > 5) {
               i_9 = 5;
            }

            i_10 = (765 - i_38 * b_4 - i_37 * (i_38 - 1)) / 2;
            int i_11 = (480 - i_7 * b_40 - i_9 * (i_7 - 1)) / 2;
            i_12 = (i_7 + class80.field1017 - 1) / i_7;
            class94.field1207 = i_12 - i_38;
            if (class249.field3224 != null && class94.field1182 > 0) {
               class249.field3224.method6091(8, class143.field1892 / 2 - class249.field3224.field3909 / 2);
            }

            if (class8.field34 != null && class94.field1182 < class94.field1207) {
               class8.field34.method6091(class286.field3638 - class8.field34.field3905 - 8, class143.field1892 / 2 - class8.field34.field3909 / 2);
            }

            int i_41 = i_11 + 23;
            int i_14 = i_10 + class94.field1188;
            int i_15 = 0;
            boolean bool_16 = false;
            int i_17 = class94.field1182;

            int i_18;
            for (i_18 = i_17 * i_7; i_18 < class80.field1017 && i_17 - class94.field1182 < i_38; i_18++) {
               class80 class80_24 = class80.field1012[i_18];
               boolean bool_20 = true;
               String string_21 = Integer.toString(class80_24.field1020);
               if (class80_24.field1020 == -1) {
                  string_21 = "OFF";
                  bool_20 = false;
               } else if (class80_24.field1020 > 1980) {
                  string_21 = "FULL";
                  bool_20 = false;
               }

               int i_23 = 0;
               byte b_22;
               if (class80_24.method1805(495435944)) {
                  if (class80_24.method1800((byte) 1)) {
                     b_22 = 7;
                  } else {
                     b_22 = 6;
                  }
               } else if (class80_24.method1804((byte) -65)) {
                  i_23 = 16711680;
                  if (class80_24.method1800((byte) 1)) {
                     b_22 = 5;
                  } else {
                     b_22 = 4;
                  }
               } else if (class80_24.method1817((byte) 48)) {
                  if (class80_24.method1800((byte) 1)) {
                     b_22 = 9;
                  } else {
                     b_22 = 8;
                  }
               } else if (class80_24.method1802(-1586384705)) {
                  if (class80_24.method1800((byte) 1)) {
                     b_22 = 3;
                  } else {
                     b_22 = 2;
                  }
               } else if (class80_24.method1800((byte) 1)) {
                  b_22 = 1;
               } else {
                  b_22 = 0;
               }

               if (class63.field492 >= i_14 && class63.field499 >= i_41 && class63.field492 < i_14 + b_4 && class63.field499 < b_40 + i_41 && bool_20) {
                  class94.field1206 = i_18;
                  class301.field3697[b_22].method6130(i_14, i_41, 128, 16777215);
                  bool_16 = true;
               } else {
                  class301.field3697[b_22].method6124(i_14, i_41);
               }

               if (class90.field1157 != null) {
                  class90.field1157[(class80_24.method1800((byte) 1) ? 8 : 0) + class80_24.field1023].method6091(i_14 + 29, i_41);
               }

               class305_0.method5436(Integer.toString(class80_24.field1018), i_14 + 15, b_40 / 2 + i_41 + 5, i_23, -1);
               class305_1.method5436(string_21, i_14 + 60, b_40 / 2 + i_41 + 5, 268435455, -1);
               i_41 = i_41 + b_40 + i_9;
               ++i_15;
               if (i_15 >= i_7) {
                  i_41 = i_11 + 23;
                  i_14 = i_14 + i_37 + b_4;
                  i_15 = 0;
                  ++i_17;
               }
            }

            if (bool_16) {
               i_18 = class305_1.method5340(class80.field1012[class94.field1206].field1022) + 6;
               int i_19 = class305_1.field3727 + 8;
               int i_42 = class63.field499 + 25;
               if (i_42 + i_19 > 480) {
                  i_42 = class63.field499 - 25 - i_19;
               }

               class331.method6004(class63.field492 - i_18 / 2, i_42, i_18, i_19, 16777120);
               class331.method6008(class63.field492 - i_18 / 2, i_42, i_18, i_19, 0);
               class305_1.method5436(class80.field1012[class94.field1206].field1022, class63.field492, i_42 + class305_1.field3727 + 4, 0, -1);
            }
         }

         class27.field150.vmethod6077(0, 0, 1926506843);
      } else {
         class25.field124.method6124(class94.field1188, 0);
         class224.field2564.method6124(class94.field1188 + 382, 0);
         class224.field2571.method6091(class94.field1188 + 382 - class224.field2571.field3905 / 2, 18);
         if (client.field653 == 0 || client.field653 == 5) {
            b_4 = 20;
            class305_0.method5436("RuneScape is loading - please wait...", class94.field1185 + 180, 245 - b_4, 16777215, -1);
            i_5 = 253 - b_4;
            class331.method6008(class94.field1185 + 180 - 152, i_5, 304, 34, 9179409);
            class331.method6008(class94.field1185 + 180 - 151, i_5 + 1, 302, 32, 0);
            class331.method6004(class94.field1185 + 180 - 150, i_5 + 2, class94.field1187 * 3, 30, 9179409);
            class331.method6004(class94.field1185 + 180 - 150 + class94.field1187 * 3, i_5 + 2, 300 - class94.field1187 * 3, 30, 0);
            class305_0.method5436(class94.field1189, class94.field1185 + 180, 276 - b_4, 16777215, -1);
         }

         String string_25;
         String string_26;
         char[] arr_27;
         String string_29;
         short s_32;
         short s_34;
         if (client.field653 == 20) {
            class50.field381.method6091(class94.field1185 + 180 - class50.field381.field3905 / 2, 271 - class50.field381.field3909 / 2);
            s_32 = 201;
            class305_0.method5436(class94.field1193, class94.field1185 + 180, s_32, 16776960, 0);
            i_33 = s_32 + 15;
            class305_0.method5436(class94.field1194, class94.field1185 + 180, i_33, 16776960, 0);
            i_33 += 15;
            class305_0.method5436(class94.field1195, class94.field1185 + 180, i_33, 16776960, 0);
            i_33 += 15;
            i_33 += 7;
            if (class94.field1191 != 4) {
               class305_0.method5335("Login: ", class94.field1185 + 180 - 110, i_33, 16777215, 0);
               s_34 = 200;

               for (string_25 = class193.method3645(1922563005); class305_0.method5340(string_25) > s_34; string_25 = string_25.substring(0, string_25.length() - 1)) {
                  ;
               }

               class305_0.method5335(class306.method5386(string_25), class94.field1185 + 180 - 70, i_33, 16777215, 0);
               i_33 += 15;
               string_26 = class94.field1197;
               i_10 = string_26.length();
               arr_27 = new char[i_10];

               for (i_12 = 0; i_12 < i_10; i_12++) {
                  arr_27[i_12] = 42;
               }

               string_29 = new String(arr_27);

               for (string_29 = string_29; class305_0.method5340(string_29) > s_34; string_29 = string_29.substring(1)) {
                  ;
               }

               class305_0.method5335("Password: " + string_29, class94.field1185 + 180 - 108, i_33, 16777215, 0);
               i_33 += 15;
            }
         }

         if (client.field653 == 10 || client.field653 == 11) {
            class50.field381.method6091(class94.field1185, 171);
            short s_6;
            if (class94.field1191 == 0) {
               s_32 = 251;
               class305_0.method5436("Welcome to RuneScape", class94.field1185 + 180, s_32, 16776960, 0);
               i_33 = s_32 + 30;
               i_5 = class94.field1185 + 180 - 80;
               s_6 = 291;
               class94.field1183.method6091(i_5 - 73, s_6 - 20);
               class305_0.method5349("New User", i_5 - 73, s_6 - 20, 144, 40, 16777215, 0, 1, 1, 0);
               i_5 = class94.field1185 + 180 + 80;
               class94.field1183.method6091(i_5 - 73, s_6 - 20);
               class305_0.method5349("Existing User", i_5 - 73, s_6 - 20, 144, 40, 16777215, 0, 1, 1, 0);
            } else if (class94.field1191 == 1) {
               class305_0.method5436(class94.field1201, class94.field1185 + 180, 201, 16776960, 0);
               s_32 = 236;
               class305_0.method5436(class94.field1193, class94.field1185 + 180, s_32, 16777215, 0);
               i_33 = s_32 + 15;
               class305_0.method5436(class94.field1194, class94.field1185 + 180, i_33, 16777215, 0);
               i_33 += 15;
               class305_0.method5436(class94.field1195, class94.field1185 + 180, i_33, 16777215, 0);
               i_33 += 15;
               i_5 = class94.field1185 + 180 - 80;
               s_6 = 321;
               class94.field1183.method6091(i_5 - 73, s_6 - 20);
               class305_0.method5436("Continue", i_5, s_6 + 5, 16777215, 0);
               i_5 = class94.field1185 + 180 + 80;
               class94.field1183.method6091(i_5 - 73, s_6 - 20);
               class305_0.method5436("Cancel", i_5, s_6 + 5, 16777215, 0);
            } else {
               short s_13;
               class334 class334_43;
               if (class94.field1191 == 2) {
                  s_32 = 201;
                  class305_0.method5436(class94.field1193, class94.field1186, s_32, 16776960, 0);
                  i_33 = s_32 + 15;
                  class305_0.method5436(class94.field1194, class94.field1186, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_0.method5436(class94.field1195, class94.field1186, i_33, 16776960, 0);
                  i_33 += 15;
                  i_33 += 7;
                  class305_0.method5335("Login: ", class94.field1186 - 110, i_33, 16777215, 0);
                  s_34 = 200;

                  for (string_25 = class193.method3645(1540002700); class305_0.method5340(string_25) > s_34; string_25 = string_25.substring(1)) {
                     ;
                  }

                  class305_0.method5335(class306.method5386(string_25) + (class94.field1181 == 0 & client.field655 % 40 < 20 ? class23.method222(16776960, (byte) 8) + "|" : ""), class94.field1186 - 70, i_33, 16777215, 0);
                  i_33 += 15;
                  string_26 = class94.field1197;
                  i_10 = string_26.length();
                  arr_27 = new char[i_10];

                  for (i_12 = 0; i_12 < i_10; i_12++) {
                     arr_27[i_12] = 42;
                  }

                  string_29 = new String(arr_27);

                  for (string_29 = string_29; class305_0.method5340(string_29) > s_34; string_29 = string_29.substring(1)) {
                     ;
                  }

                  class305_0.method5335("Password: " + string_29 + (class94.field1181 == 1 & client.field655 % 40 < 20 ? class23.method222(16776960, (byte) 34) + "|" : ""), class94.field1186 - 108, i_33, 16777215, 0);
                  i_33 += 15;
                  s_32 = 277;
                  i_10 = class94.field1186 + -117;
                  class334_43 = class233.method4125(client.field684, class94.field1192, 1809126106);
                  class334_43.method6091(i_10, s_32);
                  i_10 = i_10 + class334_43.field3905 + 5;
                  class305_1.method5335("Remember username", i_10, s_32 + 13, 16776960, 0);
                  i_10 = class94.field1186 + 24;
                  class334_43 = class233.method4125(class282.field3617.field1053, class94.field1180, -297261967);
                  class334_43.method6091(i_10, s_32);
                  i_10 = i_10 + class334_43.field3905 + 5;
                  class305_1.method5335("Hide username", i_10, s_32 + 13, 16776960, 0);
                  i_33 = s_32 + 15;
                  i_12 = class94.field1186 - 80;
                  s_13 = 321;
                  class94.field1183.method6091(i_12 - 73, s_13 - 20);
                  class305_0.method5436("Login", i_12, s_13 + 5, 16777215, 0);
                  i_12 = class94.field1186 + 80;
                  class94.field1183.method6091(i_12 - 73, s_13 - 20);
                  class305_0.method5436("Cancel", i_12, s_13 + 5, 16777215, 0);
                  s_32 = 357;
                  switch(class94.field1190) {
                  case 2:
                     class22.field107 = "Having trouble logging in?";
                     break;
                  default:
                     class22.field107 = "Can\'t login? Click here.";
                  }

                  class62.field481 = new class329(class94.field1186, s_32, class305_1.method5340(class22.field107), 11);
                  class76.field643 = new class329(class94.field1186, s_32, class305_1.method5340("Still having trouble logging in?"), 11);
                  class305_1.method5436(class22.field107, class94.field1186, s_32, 16777215, 0);
               } else if (class94.field1191 == 3) {
                  s_32 = 201;
                  class305_0.method5436("Invalid credentials.", class94.field1185 + 180, s_32, 16776960, 0);
                  i_33 = s_32 + 20;
                  class305_1.method5436("For accounts created after 24th November 2010, please use your", class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_1.method5436("email address to login. Otherwise please login with your username.", class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  i_5 = class94.field1185 + 180;
                  s_6 = 276;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_2.method5436("Try again", i_5, s_6 + 5, 16777215, 0);
                  i_5 = class94.field1185 + 180;
                  s_6 = 326;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_2.method5436("Forgotten password?", i_5, s_6 + 5, 16777215, 0);
               } else if (class94.field1191 == 4) {
                  class305_0.method5436("Authenticator", class94.field1185 + 180, 201, 16776960, 0);
                  s_32 = 236;
                  class305_0.method5436(class94.field1193, class94.field1185 + 180, s_32, 16777215, 0);
                  i_33 = s_32 + 15;
                  class305_0.method5436(class94.field1194, class94.field1185 + 180, i_33, 16777215, 0);
                  i_33 += 15;
                  class305_0.method5436(class94.field1195, class94.field1185 + 180, i_33, 16777215, 0);
                  i_33 += 15;
                  string_25 = "PIN: ";
                  string_26 = class66.field528;
                  i_10 = string_26.length();
                  arr_27 = new char[i_10];

                  for (i_12 = 0; i_12 < i_10; i_12++) {
                     arr_27[i_12] = 42;
                  }

                  string_29 = new String(arr_27);
                  class305_0.method5335(string_25 + string_29 + (client.field655 % 40 < 20 ? class23.method222(16776960, (byte) 1) + "|" : ""), class94.field1185 + 180 - 108, i_33, 16777215, 0);
                  i_33 -= 8;
                  class305_0.method5335("Trust this computer", class94.field1185 + 180 - 9, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_0.method5335("for 30 days: ", class94.field1185 + 180 - 9, i_33, 16776960, 0);
                  i_9 = class94.field1185 + 180 - 9 + class305_0.method5340("for 30 days: ") + 15;
                  i_10 = i_33 - class305_0.field3727;
                  if (class94.field1200) {
                     class334_43 = class68.field543;
                  } else {
                     class334_43 = class215.field2513;
                  }

                  class334_43.method6091(i_9, i_10);
                  i_33 += 15;
                  i_12 = class94.field1185 + 180 - 80;
                  s_13 = 321;
                  class94.field1183.method6091(i_12 - 73, s_13 - 20);
                  class305_0.method5436("Continue", i_12, s_13 + 5, 16777215, 0);
                  i_12 = class94.field1185 + 180 + 80;
                  class94.field1183.method6091(i_12 - 73, s_13 - 20);
                  class305_0.method5436("Cancel", i_12, s_13 + 5, 16777215, 0);
                  class305_1.method5436("<u=ff>Can\'t Log In?</u>", class94.field1185 + 180, s_13 + 36, 255, 0);
               } else if (class94.field1191 == 5) {
                  class305_0.method5436("Forgotten your password?", class94.field1185 + 180, 201, 16776960, 0);
                  s_32 = 221;
                  class305_2.method5436(class94.field1193, class94.field1185 + 180, s_32, 16776960, 0);
                  i_33 = s_32 + 15;
                  class305_2.method5436(class94.field1194, class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_2.method5436(class94.field1195, class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  i_33 += 14;
                  class305_0.method5335("Username/email: ", class94.field1185 + 180 - 145, i_33, 16777215, 0);
                  s_34 = 174;

                  for (string_25 = class193.method3645(2104896148); class305_0.method5340(string_25) > s_34; string_25 = string_25.substring(1)) {
                     ;
                  }

                  class305_0.method5335(class306.method5386(string_25) + (client.field655 % 40 < 20 ? class23.method222(16776960, (byte) 33) + "|" : ""), class94.field1185 + 180 - 34, i_33, 16777215, 0);
                  i_33 += 15;
                  i_7 = class94.field1185 + 180 - 80;
                  short s_8 = 321;
                  class94.field1183.method6091(i_7 - 73, s_8 - 20);
                  class305_0.method5436("Recover", i_7, s_8 + 5, 16777215, 0);
                  i_7 = class94.field1185 + 180 + 80;
                  class94.field1183.method6091(i_7 - 73, s_8 - 20);
                  class305_0.method5436("Back", i_7, s_8 + 5, 16777215, 0);
                  s_8 = 356;
                  class305_1.method5436("Still having trouble logging in?", class94.field1186, s_8, 268435455, 0);
               } else if (class94.field1191 == 6) {
                  s_32 = 201;
                  class305_0.method5436(class94.field1193, class94.field1185 + 180, s_32, 16776960, 0);
                  i_33 = s_32 + 15;
                  class305_0.method5436(class94.field1194, class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_0.method5436(class94.field1195, class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  i_5 = class94.field1185 + 180;
                  s_6 = 321;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_0.method5436("Back", i_5, s_6 + 5, 16777215, 0);
               } else if (class94.field1191 == 7) {
                  s_32 = 216;
                  class305_0.method5436("Your date of birth isn\'t set.", class94.field1185 + 180, s_32, 16776960, 0);
                  i_33 = s_32 + 15;
                  class305_2.method5436("Please verify your account status by", class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_2.method5436("setting your date of birth.", class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  i_5 = class94.field1185 + 180 - 80;
                  s_6 = 321;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_0.method5436("Set Date of Birth", i_5, s_6 + 5, 16777215, 0);
                  i_5 = class94.field1185 + 180 + 80;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_0.method5436("Back", i_5, s_6 + 5, 16777215, 0);
               } else if (class94.field1191 == 8) {
                  s_32 = 216;
                  class305_0.method5436("Sorry, but your account is not eligible to play.", class94.field1185 + 180, s_32, 16776960, 0);
                  i_33 = s_32 + 15;
                  class305_2.method5436("For more information, please take a look at", class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_2.method5436("our privacy policy.", class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  i_5 = class94.field1185 + 180 - 80;
                  s_6 = 321;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_0.method5436("Privacy Policy", i_5, s_6 + 5, 16777215, 0);
                  i_5 = class94.field1185 + 180 + 80;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_0.method5436("Back", i_5, s_6 + 5, 16777215, 0);
               } else if (class94.field1191 == 12) {
                  s_32 = 201;
                  String str_31 = "";
                  string_25 = "";
                  String str_30 = "";
                  switch(class94.field1199) {
                  case 0:
                     str_31 = "Your account has been disabled.";
                     string_25 = class233.field3011;
                     str_30 = "";
                     break;
                  case 1:
                     str_31 = "Account locked as we suspect it has been stolen.";
                     string_25 = class233.field2877;
                     str_30 = "";
                     break;
                  default:
                     class43.method631(false, 339234172);
                  }

                  class305_0.method5436(str_31, class94.field1185 + 180, s_32, 16776960, 0);
                  i_33 = s_32 + 15;
                  class305_2.method5436(string_25, class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  class305_2.method5436(str_30, class94.field1185 + 180, i_33, 16776960, 0);
                  i_33 += 15;
                  i_37 = class94.field1185 + 180;
                  short s_39 = 276;
                  class94.field1183.method6091(i_37 - 73, s_39 - 20);
                  class305_0.method5436("Support Page", i_37, s_39 + 5, 16777215, 0);
                  i_37 = class94.field1185 + 180;
                  s_39 = 326;
                  class94.field1183.method6091(i_37 - 73, s_39 - 20);
                  class305_0.method5436("Back", i_37, s_39 + 5, 16777215, 0);
               } else if (class94.field1191 == 24) {
                  s_32 = 221;
                  class305_0.method5436(class94.field1193, class94.field1185 + 180, s_32, 16777215, 0);
                  i_33 = s_32 + 15;
                  class305_0.method5436(class94.field1194, class94.field1185 + 180, i_33, 16777215, 0);
                  i_33 += 15;
                  class305_0.method5436(class94.field1195, class94.field1185 + 180, i_33, 16777215, 0);
                  i_33 += 15;
                  i_5 = class94.field1185 + 180;
                  s_6 = 301;
                  class94.field1183.method6091(i_5 - 73, s_6 - 20);
                  class305_0.method5436("Ok", i_5, s_6 + 5, 16777215, 0);
               }
            }
         }

         if (client.field653 >= 10) {
            int[] ints_28 = new int[4];
            class331.method5998(ints_28);
            class331.method5996(class94.field1188, 0, class94.field1188 + 765, class143.field1892);
            class1.field1.method1850(class94.field1188 - 22, client.field655, -387686576);
            class1.field1.method1850(class94.field1188 + 22 + 765 - 128, client.field655, -387686576);
            class331.method6012(ints_28);
         }

         class94.field1184[class282.field3617.field1063 ? 1 : 0].method6091(class94.field1188 + 765 - 40, 463);
         if (client.field653 > 5 && class199.field2382 == class49.field372) {
            if (class9.field42 != null) {
               i_33 = class94.field1188 + 5;
               s_34 = 463;
               byte b_36 = 100;
               byte b_35 = 35;
               class9.field42.method6091(i_33, s_34);
               class305_0.method5436("World" + " " + client.field645, b_36 / 2 + i_33, b_35 / 2 + s_34 - 2, 16777215, 0);
               if (class80.field1026 != null) {
                  class305_1.method5436("Loading...", b_36 / 2 + i_33, b_35 / 2 + s_34 + 12, 16777215, 0);
               } else {
                  class305_1.method5436("Click to switch", b_36 / 2 + i_33, b_35 / 2 + s_34 + 12, 16777215, 0);
               }
            } else {
               class9.field42 = class51.method836(class100.field1291, "sl_button", "", -946302917);
            }
         }

      }
   }

   static final void method1224(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9) {
      class77 class77_10 = null;

      for (class77 class77_11 = (class77) client.field876.method4879(); class77_11 != null; class77_11 = (class77) client.field876.method4884()) {
         if (i_0 == class77_11.field924 && class77_11.field926 == i_1 && i_2 == class77_11.field927 && i_3 == class77_11.field925) {
            class77_10 = class77_11;
            break;
         }
      }

      if (class77_10 == null) {
         class77_10 = new class77();
         class77_10.field924 = i_0;
         class77_10.field925 = i_3;
         class77_10.field926 = i_1;
         class77_10.field927 = i_2;
         class7.method86(class77_10, -1795007208);
         client.field876.method4888(class77_10);
      }

      class77_10.field932 = i_4;
      class77_10.field933 = i_5;
      class77_10.field936 = i_6;
      class77_10.field934 = i_7;
      class77_10.field930 = i_8;
   }

   public static String method1229(byte[] bytes_0, int i_1, int i_2, int i_3) {
      StringBuilder stringbuilder_4 = new StringBuilder();

      for (int i_5 = i_1; i_5 < i_2 + i_1; i_5 += 3) {
         int i_6 = bytes_0[i_5] & 0xff;
         stringbuilder_4.append(class300.field3695[i_6 >>> 2]);
         if (i_5 < i_2 - 1) {
            int i_7 = bytes_0[i_5 + 1] & 0xff;
            stringbuilder_4.append(class300.field3695[(i_6 & 0x3) << 4 | i_7 >>> 4]);
            if (i_5 < i_2 - 2) {
               int i_8 = bytes_0[i_5 + 2] & 0xff;
               stringbuilder_4.append(class300.field3695[(i_7 & 0xf) << 2 | i_8 >>> 6]).append(class300.field3695[i_8 & 0x3f]);
            } else {
               stringbuilder_4.append(class300.field3695[(i_7 & 0xf) << 2]).append("=");
            }
         } else {
            stringbuilder_4.append(class300.field3695[(i_6 & 0x3) << 4]).append("==");
         }
      }

      return stringbuilder_4.toString();
   }

}
