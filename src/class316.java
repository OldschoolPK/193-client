import java.io.IOException;
import java.net.Socket;

public class class316 extends class315 {

   Socket field3807;
   class319 field3808;
   class317 field3811;

   class316(Socket socket_1, int i_2, int i_3) throws IOException {
      this.field3807 = socket_1;
      this.field3807.setSoTimeout(30000);
      this.field3807.setTcpNoDelay(true);
      this.field3807.setReceiveBufferSize(65536);
      this.field3807.setSendBufferSize(65536);
      this.field3808 = new class319(this.field3807.getInputStream(), i_2);
      this.field3811 = new class317(this.field3807.getOutputStream(), i_3);
   }

   public void vmethod5831(byte b_1) {
      this.field3811.method5857((byte) 8);

      try {
         this.field3807.close();
      } catch (IOException ioexception_3) {
         ;
      }

      this.field3808.method5879((short) 180);
   }

   public boolean vmethod5836(int i_1, byte b_2) throws IOException {
      return this.field3808.method5870(i_1, -2116238538);
   }

   public int vmethod5830(byte[] bytes_1, int i_2, int i_3, byte b_4) throws IOException {
      return this.field3808.method5873(bytes_1, i_2, i_3, (byte) 30);
   }

   public void vmethod5844(byte[] bytes_1, int i_2, int i_3, int i_4) throws IOException {
      this.field3811.method5849(bytes_1, i_2, i_3, 1673669386);
   }

   public int vmethod5828(int i_1) throws IOException {
      return this.field3808.method5871(405355900);
   }

   public int vmethod5834(byte b_1) throws IOException {
      return this.field3808.method5881(1222346600);
   }

   protected void abo() {
      this.vmethod5831((byte) 86);
   }

   protected void finalize() {
      this.vmethod5831((byte) -48);
   }

}
