public final class class135 extends class189 {

   class129 field1670;
   class140 field1661;
   class150 field1668;
   class145 field1667;
   class132 field1669;
   int field1671;
   class131 field1672;
   boolean field1678;
   int field1675;
   class135 field1683;
   boolean field1676;
   int field1665;
   boolean field1681;
   int field1680;
   int field1679;
   int field1677;
   class151[] field1662 = new class151[5];
   int[] field1673 = new int[5];
   int field1674 = 0;
   int field1682;
   int field1664;
   int field1666;
   int field1663;

   class135(int i_1, int i_2, int i_3) {
      this.field1664 = this.field1682 = i_1;
      this.field1666 = i_2;
      this.field1663 = i_3;
   }

   public static synchronized void method2917(byte[] bytes_0, int i_1) {
      if (bytes_0.length == 100 && class312.field3754 < 1000) {
         class312.field3757[++class312.field3754 - 1] = bytes_0;
      } else if (bytes_0.length == 5000 && class312.field3755 < 250) {
         class312.field3758[++class312.field3755 - 1] = bytes_0;
      } else if (bytes_0.length == 30000 && class312.field3756 < 50) {
         class312.field3759[++class312.field3756 - 1] = bytes_0;
      } else {
         if (class23.field109 != null) {
            for (int i_2 = 0; i_2 < class117.field1460.length; i_2++) {
               if (bytes_0.length == class117.field1460[i_2] && class176.field2069[i_2] < class23.field109[i_2].length) {
                  class23.field109[i_2][class176.field2069[i_2]++] = bytes_0;
                  return;
               }
            }
         }

      }
   }

   static final int method2918(int i_0, int i_1, byte b_2) {
      if (i_0 == -1) {
         return 12345678;
      } else {
         i_1 = (i_0 & 0x7f) * i_1 / 128;
         if (i_1 < 2) {
            i_1 = 2;
         } else if (i_1 > 126) {
            i_1 = 126;
         }

         return (i_0 & 0xff80) + i_1;
      }
   }

}
