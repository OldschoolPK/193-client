public final class class64 {

   static byte[][][] field514;
   static byte[][][] field508;
   static byte[][][] field511;
   static int[][][] field519 = new int[4][105][105];
   static byte[][][] field507 = new byte[4][104][104];
   static int field506 = 99;
   static final int[] field512 = new int[] {1, 2, 4, 8};
   static final int[] field513 = new int[] {16, 32, 64, 128};
   static final int[] field518 = new int[] {1, 0, -1, 0};
   static final int[] field515 = new int[] {0, -1, 0, 1};
   static final int[] field509 = new int[] {1, -1, -1, 1};
   static final int[] field517 = new int[] {-1, -1, 1, 1};
   static int field516 = (int)(Math.random() * 17.0D) - 8;
   static int field510 = (int)(Math.random() * 33.0D) - 16;

   public static class191[] method1209(byte b_0) {
      return new class191[] {class191.field2179, class191.field2202, class191.field2141, class191.field2142, class191.field2143, class191.field2144, class191.field2201, class191.field2146, class191.field2182, class191.field2148, class191.field2200, class191.field2150, class191.field2190, class191.field2152, class191.field2139, class191.field2188, class191.field2155, class191.field2156, class191.field2163, class191.field2158, class191.field2159, class191.field2160, class191.field2161, class191.field2162, class191.field2218, class191.field2164, class191.field2165, class191.field2192, class191.field2167, class191.field2212, class191.field2208, class191.field2170, class191.field2171, class191.field2172, class191.field2173, class191.field2174, class191.field2175, class191.field2176, class191.field2140, class191.field2178, class191.field2151, class191.field2180, class191.field2181, class191.field2157, class191.field2166, class191.field2184, class191.field2197, class191.field2186, class191.field2149, class191.field2214, class191.field2189, class191.field2209, class191.field2191, class191.field2145, class191.field2183, class191.field2194, class191.field2195, class191.field2196, class191.field2215, class191.field2198, class191.field2177, class191.field2207, class191.field2154, class191.field2153, class191.field2203, class191.field2204, class191.field2205, class191.field2169, class191.field2206, class191.field2185, class191.field2187, class191.field2210, class191.field2211, class191.field2193, class191.field2213, class191.field2168, class191.field2199, class191.field2216, class191.field2217, class191.field2147, class191.field2219, class191.field2220, class191.field2221, class191.field2222, class191.field2223, class191.field2224};
   }

   static void method1208(class244 class244_0, class244 class244_1, boolean bool_2, int i_3, int i_4) {
      if (class94.field1204) {
         if (i_3 == 4) {
            class94.field1191 = 4;
         }

      } else {
         class94.field1191 = i_3;
         class331.method6000();
         byte[] bytes_5 = class244_0.method4181("title.jpg", "", 948692602);
         class25.field124 = class28.method285(bytes_5, 1856556337);
         class224.field2564 = class25.field124.method6183();
         int i_6 = client.field646;
         if ((i_6 & 0x20000000) != 0) {
            class224.field2571 = class51.method836(class244_1, "logo_deadman_mode", "", -486304580);
         } else if ((i_6 & 0x40000000) != 0) {
            class224.field2571 = class51.method836(class244_1, "logo_seasonal_mode", "", -176419231);
         } else {
            class224.field2571 = class51.method836(class244_1, "logo", "", -522981979);
         }

         class50.field381 = class51.method836(class244_1, "titlebox", "", -1775376910);
         class94.field1183 = class51.method836(class244_1, "titlebutton", "", -423544317);
         class344.field4050 = class89.method2095(class244_1, "runes", "", 746444135);
         class94.field1184 = class89.method2095(class244_1, "title_mute", "", -8453434);
         class215.field2513 = class51.method836(class244_1, "options_radio_buttons,0", "", -1130881569);
         class353.field4101 = class51.method836(class244_1, "options_radio_buttons,4", "", -477795219);
         class68.field543 = class51.method836(class244_1, "options_radio_buttons,2", "", -741680373);
         class204.field2408 = class51.method836(class244_1, "options_radio_buttons,6", "", -422404739);
         class83.field1071 = class215.field2513.field3905;
         class98.field1260 = class215.field2513.field3909;
         class1.field1 = new class81(class344.field4050);
         if (bool_2) {
            class94.field1196 = "";
            class94.field1197 = "";
         }

         class42.field313 = 0;
         class66.field528 = "";
         class94.field1200 = true;
         class94.field1198 = false;
         if (!class282.field3617.field1063) {
            class29.method300(2, class217.field2520, "scape main", "", 255, false, -519940907);
         } else {
            class66.method1220(2, (byte) 5);
         }

         class116.method2536(false, 1078944983);
         class94.field1204 = true;
         class94.field1188 = (class286.field3638 - 765) / 2;
         class94.field1185 = class94.field1188 + 202;
         class94.field1186 = class94.field1185 + 180;
         class25.field124.method6124(class94.field1188, 0);
         class224.field2564.method6124(class94.field1188 + 382, 0);
         class224.field2571.method6091(class94.field1188 + 382 - class224.field2571.field3905 / 2, 18);
      }
   }

   static void method1203(int i_0) {
      class100.field1288.clear();
      class100.field1285.method5920();
      class100.field1284.method4968();
      class100.field1287 = 0;
   }

   public static int method1170(int i_0, byte b_1) {
      long long_3 = class137.field1768[i_0];
      int i_2 = (int)(long_3 >>> 7 & 0x7fL);
      return i_2;
   }

   static void method1210(class75 class75_0, int i_1, int i_2, int i_3) {
      if (class75_0.field970 == i_1 && i_1 != -1) {
         int i_4 = class260.method4510(i_1, -2076618794).field3548;
         if (i_4 == 1) {
            class75_0.field971 = 0;
            class75_0.field972 = 0;
            class75_0.field937 = i_2;
            class75_0.field974 = 0;
         }

         if (i_4 == 2) {
            class75_0.field974 = 0;
         }
      } else if (i_1 == -1 || class75_0.field970 == -1 || class260.method4510(i_1, -1851700252).field3557 >= class260.method4510(class75_0.field970, -2118769664).field3557) {
         class75_0.field970 = i_1;
         class75_0.field971 = 0;
         class75_0.field972 = 0;
         class75_0.field937 = i_2;
         class75_0.field974 = 0;
         class75_0.field996 = class75_0.field985;
      }

   }

   static final void method1207(String string_0, short s_1) {
      if (class35.field244 != null) {
         class196 class196_2 = class68.method1249(class192.field2228, client.field694.field1328, (byte) 1);
         class196_2.field2360.method5482(class132.method2906(string_0, (byte) -84), (byte) -121);
         class196_2.field2360.method5492(string_0, -1748540705);
         client.field694.method2295(class196_2, 1001754459);
      }
   }

}
