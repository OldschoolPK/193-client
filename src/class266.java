public class class266 implements class203 {

   static final class266 field3490 = new class266(2, 0);
   static final class266 field3489 = new class266(1, 1);
   static final class266 field3491 = new class266(0, 2);
   public final int field3492;
   final int field3493;

   class266(int i_1, int i_2) {
      this.field3492 = i_1;
      this.field3493 = i_2;
   }

   public int vmethod6086(int i_1) {
      return this.field3493;
   }

   static int method4696(int i_0, class101 class101_1, boolean bool_2, byte b_3) {
      if (i_0 == 6500) {
         class85.field1095[++class253.field3267 - 1] = class16.method157(945138367) ? 1 : 0;
         return 1;
      } else {
         class80 class80_4;
         if (i_0 == 6501) {
            class80_4 = class236.method4131(-2109446409);
            if (class80_4 != null) {
               class85.field1095[++class253.field3267 - 1] = class80_4.field1018;
               class85.field1095[++class253.field3267 - 1] = class80_4.field1019;
               class85.field1096[++class85.field1105 - 1] = class80_4.field1022;
               class85.field1095[++class253.field3267 - 1] = class80_4.field1023;
               class85.field1095[++class253.field3267 - 1] = class80_4.field1020;
               class85.field1096[++class85.field1105 - 1] = class80_4.field1021;
            } else {
               class85.field1095[++class253.field3267 - 1] = -1;
               class85.field1095[++class253.field3267 - 1] = 0;
               class85.field1096[++class85.field1105 - 1] = "";
               class85.field1095[++class253.field3267 - 1] = 0;
               class85.field1095[++class253.field3267 - 1] = 0;
               class85.field1096[++class85.field1105 - 1] = "";
            }

            return 1;
         } else if (i_0 == 6502) {
            class80_4 = class15.method145(-114002754);
            if (class80_4 != null) {
               class85.field1095[++class253.field3267 - 1] = class80_4.field1018;
               class85.field1095[++class253.field3267 - 1] = class80_4.field1019;
               class85.field1096[++class85.field1105 - 1] = class80_4.field1022;
               class85.field1095[++class253.field3267 - 1] = class80_4.field1023;
               class85.field1095[++class253.field3267 - 1] = class80_4.field1020;
               class85.field1096[++class85.field1105 - 1] = class80_4.field1021;
            } else {
               class85.field1095[++class253.field3267 - 1] = -1;
               class85.field1095[++class253.field3267 - 1] = 0;
               class85.field1096[++class85.field1105 - 1] = "";
               class85.field1095[++class253.field3267 - 1] = 0;
               class85.field1095[++class253.field3267 - 1] = 0;
               class85.field1096[++class85.field1105 - 1] = "";
            }

            return 1;
         } else {
            class80 class80_5;
            int i_6;
            int i_8;
            if (i_0 == 6506) {
               i_8 = class85.field1095[--class253.field3267];
               class80_5 = null;

               for (i_6 = 0; i_6 < class80.field1017; i_6++) {
                  if (i_8 == class80.field1012[i_6].field1018) {
                     class80_5 = class80.field1012[i_6];
                     break;
                  }
               }

               if (class80_5 != null) {
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1018;
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1019;
                  class85.field1096[++class85.field1105 - 1] = class80_5.field1022;
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1023;
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1020;
                  class85.field1096[++class85.field1105 - 1] = class80_5.field1021;
               } else {
                  class85.field1095[++class253.field3267 - 1] = -1;
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1096[++class85.field1105 - 1] = "";
               }

               return 1;
            } else if (i_0 == 6507) {
               class253.field3267 -= 4;
               i_8 = class85.field1095[class253.field3267];
               boolean bool_11 = class85.field1095[class253.field3267 + 1] == 1;
               i_6 = class85.field1095[class253.field3267 + 2];
               boolean bool_7 = class85.field1095[class253.field3267 + 3] == 1;
               class175.method3529(i_8, bool_11, i_6, bool_7, 766328157);
               return 1;
            } else if (i_0 != 6511) {
               if (i_0 == 6512) {
                  client.field793 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else {
                  int i_9;
                  class260 class260_10;
                  if (i_0 == 6513) {
                     class253.field3267 -= 2;
                     i_8 = class85.field1095[class253.field3267];
                     i_9 = class85.field1095[class253.field3267 + 1];
                     class260_10 = class103.method2288(i_9, 1207819240);
                     if (class260_10.method4499((byte) 0)) {
                        class85.field1096[++class85.field1105 - 1] = class27.method272(i_8, -1845456878).method4707(i_9, class260_10.field3330, -1136396836);
                     } else {
                        class85.field1095[++class253.field3267 - 1] = class27.method272(i_8, -1204810967).method4706(i_9, class260_10.field3332, 2117097592);
                     }

                     return 1;
                  } else if (i_0 == 6514) {
                     class253.field3267 -= 2;
                     i_8 = class85.field1095[class253.field3267];
                     i_9 = class85.field1095[class253.field3267 + 1];
                     class260_10 = class103.method2288(i_9, 1207819240);
                     if (class260_10.method4499((byte) 0)) {
                        class85.field1096[++class85.field1105 - 1] = class34.method383(i_8, -1262903423).method4590(i_9, class260_10.field3330, 2059045293);
                     } else {
                        class85.field1095[++class253.field3267 - 1] = class34.method383(i_8, -887595677).method4589(i_9, class260_10.field3332, (byte) -50);
                     }

                     return 1;
                  } else if (i_0 == 6515) {
                     class253.field3267 -= 2;
                     i_8 = class85.field1095[class253.field3267];
                     i_9 = class85.field1095[class253.field3267 + 1];
                     class260_10 = class103.method2288(i_9, 1207819240);
                     if (class260_10.method4499((byte) 0)) {
                        class85.field1096[++class85.field1105 - 1] = class66.method1223(i_8, -1043397735).method4664(i_9, class260_10.field3330, (byte) 37);
                     } else {
                        class85.field1095[++class253.field3267 - 1] = class66.method1223(i_8, -481807962).method4639(i_9, class260_10.field3332, (byte) -79);
                     }

                     return 1;
                  } else if (i_0 == 6516) {
                     class253.field3267 -= 2;
                     i_8 = class85.field1095[class253.field3267];
                     i_9 = class85.field1095[class253.field3267 + 1];
                     class260_10 = class103.method2288(i_9, 1207819240);
                     if (class260_10.method4499((byte) 0)) {
                        class85.field1096[++class85.field1105 - 1] = class43.method660(i_8, (byte) 5).method4517(i_9, class260_10.field3330, 116269360);
                     } else {
                        class85.field1095[++class253.field3267 - 1] = class43.method660(i_8, (byte) 5).method4516(i_9, class260_10.field3332, -1178143086);
                     }

                     return 1;
                  } else if (i_0 == 6518) {
                     class85.field1095[++class253.field3267 - 1] = client.field652 ? 1 : 0;
                     return 1;
                  } else if (i_0 == 6519) {
                     class85.field1095[++class253.field3267 - 1] = client.field650 & 0x3;
                     return 1;
                  } else if (i_0 == 6520) {
                     return 1;
                  } else if (i_0 == 6521) {
                     return 1;
                  } else if (i_0 == 6522) {
                     --class85.field1105;
                     --class253.field3267;
                     return 1;
                  } else if (i_0 == 6523) {
                     --class85.field1105;
                     --class253.field3267;
                     return 1;
                  } else if (i_0 == 6524) {
                     class85.field1095[++class253.field3267 - 1] = -1;
                     return 1;
                  } else if (i_0 == 6525) {
                     class85.field1095[++class253.field3267 - 1] = 1;
                     return 1;
                  } else if (i_0 == 6526) {
                     class85.field1095[++class253.field3267 - 1] = 1;
                     return 1;
                  } else {
                     return 2;
                  }
               }
            } else {
               i_8 = class85.field1095[--class253.field3267];
               if (i_8 >= 0 && i_8 < class80.field1017) {
                  class80_5 = class80.field1012[i_8];
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1018;
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1019;
                  class85.field1096[++class85.field1105 - 1] = class80_5.field1022;
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1023;
                  class85.field1095[++class253.field3267 - 1] = class80_5.field1020;
                  class85.field1096[++class85.field1105 - 1] = class80_5.field1021;
               } else {
                  class85.field1095[++class253.field3267 - 1] = -1;
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1096[++class85.field1105 - 1] = "";
               }

               return 1;
            }
         }
      }
   }

}
