import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class class45 {

   static class174 field348;
   static class107 field350;
   static class322 field349;
   boolean field342 = false;
   boolean field333 = false;
   HashMap field338 = new HashMap();
   public int field347 = 0;
   class334[] field344;
   final HashMap field341;
   final class244 field340;
   final class244 field332;
   HashMap field336;
   class47 field343;
   int field334;
   int field346;
   int field345;
   int field351;
   class38[][] field337;
   class335 field335;

   public class45(class334[] arr_1, HashMap hashmap_2, class244 class244_3, class244 class244_4) {
      this.field344 = arr_1;
      this.field341 = hashmap_2;
      this.field340 = class244_3;
      this.field332 = class244_4;
   }

   void method708(int i_1) {
      if (this.field336 == null) {
         this.field336 = new HashMap();
      }

      this.field336.clear();

      for (int i_2 = 0; i_2 < this.field337.length; i_2++) {
         for (int i_3 = 0; i_3 < this.field337[i_2].length; i_3++) {
            List list_4 = this.field337[i_2][i_3].method502(1048345279);
            Iterator iterator_5 = list_4.iterator();

            while (iterator_5.hasNext()) {
               class43 class43_6 = (class43) iterator_5.next();
               if (class43_6.method634(1161567228)) {
                  int i_7 = class43_6.vmethod630(2061139165);
                  if (!this.field336.containsKey(Integer.valueOf(i_7))) {
                     LinkedList linkedlist_8 = new LinkedList();
                     linkedlist_8.add(class43_6);
                     this.field336.put(Integer.valueOf(i_7), linkedlist_8);
                  } else {
                     List list_9 = (List) this.field336.get(Integer.valueOf(i_7));
                     list_9.add(class43_6);
                  }
               }
            }
         }
      }

   }

   class31 method678(int i_1, int i_2, int i_3, int i_4, int i_5) {
      class31 class31_6 = new class31(this);
      int i_7 = this.field334 + i_1;
      int i_8 = i_2 + this.field346;
      int i_9 = i_3 + this.field334;
      int i_10 = i_4 + this.field346;
      int i_11 = i_7 / 64;
      int i_12 = i_8 / 64;
      int i_13 = i_9 / 64;
      int i_14 = i_10 / 64;
      class31_6.field194 = i_13 - i_11 + 1;
      class31_6.field197 = i_14 - i_12 + 1;
      class31_6.field195 = i_11 - this.field343.method398((byte) -122);
      class31_6.field196 = i_12 - this.field343.method400(245541248);
      if (class31_6.field195 < 0) {
         class31_6.field194 += class31_6.field195;
         class31_6.field195 = 0;
      }

      if (class31_6.field195 > this.field337.length - class31_6.field194) {
         class31_6.field194 = this.field337.length - class31_6.field195;
      }

      if (class31_6.field196 < 0) {
         class31_6.field197 += class31_6.field196;
         class31_6.field196 = 0;
      }

      if (class31_6.field196 > this.field337[0].length - class31_6.field197) {
         class31_6.field197 = this.field337[0].length - class31_6.field196;
      }

      class31_6.field194 = Math.min(class31_6.field194, this.field337.length);
      class31_6.field197 = Math.min(class31_6.field197, this.field337[0].length);
      return class31_6;
   }

   float method668(int i_1, int i_2, int i_3) {
      float f_4 = (float)i_1 / (float)i_2;
      if (f_4 > 8.0F) {
         return 8.0F;
      } else if (f_4 < 1.0F) {
         return 1.0F;
      } else {
         int i_5 = Math.round(f_4);
         return Math.abs((float)i_5 - f_4) < 0.05F ? (float)i_5 : f_4;
      }
   }

   public boolean method673(int i_1) {
      return this.field342;
   }

   public HashMap method674(int i_1) {
      this.method708(2005270001);
      return this.field336;
   }

   public void method670(int i_1, int i_2, int i_3, int i_4, HashSet hashset_5, int i_6, int i_7, byte b_8) {
      if (this.field335 != null) {
         this.field335.method6128(i_1, i_2, i_3, i_4);
         if (i_6 > 0 && i_6 % i_7 < i_7 / 2) {
            if (this.field336 == null) {
               this.method708(2005270001);
            }

            Iterator iterator_9 = hashset_5.iterator();

            while (true) {
               List list_11;
               do {
                  if (!iterator_9.hasNext()) {
                     return;
                  }

                  int i_10 = ((Integer) iterator_9.next()).intValue();
                  list_11 = (List) this.field336.get(Integer.valueOf(i_10));
               } while (list_11 == null);

               Iterator iterator_12 = list_11.iterator();

               while (iterator_12.hasNext()) {
                  class43 class43_13 = (class43) iterator_12.next();
                  int i_14 = i_3 * (class43_13.field321.field2546 - this.field334) / this.field345;
                  int i_15 = i_4 - (class43_13.field321.field2547 - this.field346) * i_4 / this.field351;
                  class331.method6044(i_14 + i_1, i_15 + i_2, 2, 16776960, 256);
               }
            }
         }
      }
   }

   public void method694(class244 class244_1, String string_2, boolean bool_3, int i_4) {
      if (!this.field333) {
         this.field342 = false;
         this.field333 = true;
         System.nanoTime();
         int i_5 = class244_1.method4206(class44.field328.field330, -1939456533);
         int i_6 = class244_1.method4235(i_5, string_2, 1284626629);
         class310 class310_7 = new class310(class244_1.method4181(class44.field328.field330, string_2, 721337656));
         class310 class310_8 = new class310(class244_1.method4181(class44.field326.field330, string_2, 1726770868));
         System.nanoTime();
         System.nanoTime();
         this.field343 = new class47();

         try {
            this.field343.method735(class310_7, class310_8, i_6, bool_3, -303993957);
         } catch (IllegalStateException illegalstateexception_20) {
            return;
         }

         this.field343.method402(-766770835);
         this.field343.method403(1397357554);
         this.field343.method404(-865757102);
         this.field334 = this.field343.method398((byte) -108) * 64;
         this.field346 = this.field343.method400(-1079477931) * 64;
         this.field345 = (this.field343.method425(1569108350) - this.field343.method398((byte) -24) + 1) * 64;
         this.field351 = (this.field343.method446(-1976282322) - this.field343.method400(188593121) + 1) * 64;
         int i_17 = this.field343.method425(1492130454) - this.field343.method398((byte) -98) + 1;
         int i_10 = this.field343.method446(-1714328131) - this.field343.method400(-190009352) + 1;
         System.nanoTime();
         System.nanoTime();
         class20.method201((byte) 1);
         this.field337 = new class38[i_17][i_10];
         Iterator iterator_11 = this.field343.field355.iterator();

         while (iterator_11.hasNext()) {
            class24 class24_12 = (class24) iterator_11.next();
            int i_13 = class24_12.field180;
            int i_14 = class24_12.field188;
            int i_15 = i_13 - this.field343.method398((byte) -66);
            int i_16 = i_14 - this.field343.method400(-1981272490);
            this.field337[i_15][i_16] = new class38(i_13, i_14, this.field343.method450(473258076), this.field341);
            this.field337[i_15][i_16].method472(class24_12, this.field343.field354, (byte) 1);
         }

         for (int i_18 = 0; i_18 < i_17; i_18++) {
            for (int i_19 = 0; i_19 < i_10; i_19++) {
               if (this.field337[i_18][i_19] == null) {
                  this.field337[i_18][i_19] = new class38(this.field343.method398((byte) -83) + i_18, this.field343.method400(222256995) + i_19, this.field343.method450(-1159303653), this.field341);
                  this.field337[i_18][i_19].method506(this.field343.field353, this.field343.field354, 390590880);
               }
            }
         }

         System.nanoTime();
         System.nanoTime();
         if (class244_1.method4180(class44.field327.field330, string_2, -1842632770)) {
            byte[] bytes_21 = class244_1.method4181(class44.field327.field330, string_2, 1063688668);
            this.field335 = class28.method285(bytes_21, -1534200890);
         }

         System.nanoTime();
         class244_1.method4222((byte) 3);
         class244_1.method4217(-724744505);
         this.field342 = true;
      }
   }

   public List method707(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11) {
      LinkedList linkedlist_12 = new LinkedList();
      if (!this.field342) {
         return linkedlist_12;
      } else {
         class31 class31_13 = this.method678(i_1, i_2, i_3, i_4, 604537210);
         float f_14 = this.method668(i_7, i_3 - i_1, -1229276140);
         int i_15 = (int)(f_14 * 64.0F);
         int i_16 = this.field334 + i_1;
         int i_17 = i_2 + this.field346;

         for (int i_18 = class31_13.field195; i_18 < class31_13.field195 + class31_13.field194; i_18++) {
            for (int i_19 = class31_13.field196; i_19 < class31_13.field197 + class31_13.field196; i_19++) {
               List list_20 = this.field337[i_18][i_19].method501(i_5 + i_15 * (this.field337[i_18][i_19].field265 * 64 - i_16) / 64, i_8 + i_6 - i_15 * (this.field337[i_18][i_19].field266 * 64 - i_17 + 64) / 64, i_15, i_9, i_10, -1578345699);
               if (!list_20.isEmpty()) {
                  linkedlist_12.addAll(list_20);
               }
            }
         }

         return linkedlist_12;
      }
   }

   public final void method667(byte b_1) {
      this.field336 = null;
   }

   public final void method683(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9) {
      int[] ints_10 = class331.field3886;
      int i_11 = class331.field3889;
      int i_12 = class331.field3885;
      int[] ints_13 = new int[4];
      class331.method5998(ints_13);
      class31 class31_14 = this.method678(i_1, i_2, i_3, i_4, 215648879);
      float f_15 = this.method668(i_7 - i_5, i_3 - i_1, 203989944);
      int i_16 = (int)Math.ceil((double)f_15);
      this.field347 = i_16;
      if (!this.field338.containsKey(Integer.valueOf(i_16))) {
         class49 class49_17 = new class49(i_16);
         class49_17.method793(-717419640);
         this.field338.put(Integer.valueOf(i_16), class49_17);
      }

      int i_24 = class31_14.field195 + class31_14.field194 - 1;
      int i_18 = class31_14.field197 + class31_14.field196 - 1;

      int i_19;
      int i_20;
      for (i_19 = class31_14.field195; i_19 <= i_24; i_19++) {
         for (i_20 = class31_14.field196; i_20 <= i_18; i_20++) {
            this.field337[i_19][i_20].method480(i_16, (class49) this.field338.get(Integer.valueOf(i_16)), this.field344, this.field340, this.field332, (byte) 71);
         }
      }

      class331.method5999(ints_10, i_11, i_12);
      class331.method6012(ints_13);
      i_19 = (int)(f_15 * 64.0F);
      i_20 = this.field334 + i_1;
      int i_21 = i_2 + this.field346;

      for (int i_22 = class31_14.field195; i_22 < class31_14.field194 + class31_14.field195; i_22++) {
         for (int i_23 = class31_14.field196; i_23 < class31_14.field196 + class31_14.field197; i_23++) {
            this.field337[i_22][i_23].method534(i_5 + i_19 * (this.field337[i_22][i_23].field265 * 64 - i_20) / 64, i_8 - i_19 * (this.field337[i_22][i_23].field266 * 64 - i_21 + 64) / 64, i_19, -1252499183);
         }
      }

   }

   public final void method669(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, HashSet hashset_9, HashSet hashset_10, int i_11, int i_12, boolean bool_13, int i_14) {
      class31 class31_15 = this.method678(i_1, i_2, i_3, i_4, -252322888);
      float f_16 = this.method668(i_7 - i_5, i_3 - i_1, -1641409927);
      int i_17 = (int)(f_16 * 64.0F);
      int i_18 = this.field334 + i_1;
      int i_19 = i_2 + this.field346;

      int i_20;
      int i_21;
      for (i_20 = class31_15.field195; i_20 < class31_15.field195 + class31_15.field194; i_20++) {
         for (i_21 = class31_15.field196; i_21 < class31_15.field196 + class31_15.field197; i_21++) {
            if (bool_13) {
               this.field337[i_20][i_21].method476(-1147787882);
            }

            this.field337[i_20][i_21].method481(i_5 + i_17 * (this.field337[i_20][i_21].field265 * 64 - i_18) / 64, i_8 - i_17 * (this.field337[i_20][i_21].field266 * 64 - i_19 + 64) / 64, i_17, hashset_9, -935222416);
         }
      }

      if (hashset_10 != null && i_11 > 0) {
         for (i_20 = class31_15.field195; i_20 < class31_15.field194 + class31_15.field195; i_20++) {
            for (i_21 = class31_15.field196; i_21 < class31_15.field197 + class31_15.field196; i_21++) {
               this.field337[i_20][i_21].method486(hashset_10, i_11, i_12, -879873974);
            }
         }
      }

   }

   static final void method686(int i_0, int i_1, int i_2, boolean bool_3, int i_4) {
      if (class41.method603(i_0, -640309657)) {
         class85.method2038(class9.field44[i_0], -1, i_1, i_2, bool_3, -1607186197);
      }
   }

   public static int method675(CharSequence charsequence_0, int i_1, int i_2, byte[] bytes_3, int i_4, byte b_5) {
      int i_6 = i_2 - i_1;

      for (int i_7 = 0; i_7 < i_6; i_7++) {
         char var_8 = charsequence_0.charAt(i_7 + i_1);
         if (var_8 > 0 && var_8 < 128 || var_8 >= 160 && var_8 <= 255) {
            bytes_3[i_7 + i_4] = (byte)var_8;
         } else if (var_8 == 8364) {
            bytes_3[i_7 + i_4] = -128;
         } else if (var_8 == 8218) {
            bytes_3[i_7 + i_4] = -126;
         } else if (var_8 == 402) {
            bytes_3[i_7 + i_4] = -125;
         } else if (var_8 == 8222) {
            bytes_3[i_7 + i_4] = -124;
         } else if (var_8 == 8230) {
            bytes_3[i_7 + i_4] = -123;
         } else if (var_8 == 8224) {
            bytes_3[i_7 + i_4] = -122;
         } else if (var_8 == 8225) {
            bytes_3[i_7 + i_4] = -121;
         } else if (var_8 == 710) {
            bytes_3[i_7 + i_4] = -120;
         } else if (var_8 == 8240) {
            bytes_3[i_7 + i_4] = -119;
         } else if (var_8 == 352) {
            bytes_3[i_7 + i_4] = -118;
         } else if (var_8 == 8249) {
            bytes_3[i_7 + i_4] = -117;
         } else if (var_8 == 338) {
            bytes_3[i_7 + i_4] = -116;
         } else if (var_8 == 381) {
            bytes_3[i_7 + i_4] = -114;
         } else if (var_8 == 8216) {
            bytes_3[i_7 + i_4] = -111;
         } else if (var_8 == 8217) {
            bytes_3[i_7 + i_4] = -110;
         } else if (var_8 == 8220) {
            bytes_3[i_7 + i_4] = -109;
         } else if (var_8 == 8221) {
            bytes_3[i_7 + i_4] = -108;
         } else if (var_8 == 8226) {
            bytes_3[i_7 + i_4] = -107;
         } else if (var_8 == 8211) {
            bytes_3[i_7 + i_4] = -106;
         } else if (var_8 == 8212) {
            bytes_3[i_7 + i_4] = -105;
         } else if (var_8 == 732) {
            bytes_3[i_7 + i_4] = -104;
         } else if (var_8 == 8482) {
            bytes_3[i_7 + i_4] = -103;
         } else if (var_8 == 353) {
            bytes_3[i_7 + i_4] = -102;
         } else if (var_8 == 8250) {
            bytes_3[i_7 + i_4] = -101;
         } else if (var_8 == 339) {
            bytes_3[i_7 + i_4] = -100;
         } else if (var_8 == 382) {
            bytes_3[i_7 + i_4] = -98;
         } else if (var_8 == 376) {
            bytes_3[i_7 + i_4] = -97;
         } else {
            bytes_3[i_7 + i_4] = 63;
         }
      }

      return i_6;
   }

   static void method706(int i_0) {
      if (client.field803) {
         class226 class226_1 = class92.method2103(class63.field493, client.field804, -100010964);
         if (class226_1 != null && class226_1.field2584 != null) {
            class71 class71_2 = new class71();
            class71_2.field571 = class226_1;
            class71_2.field572 = class226_1.field2584;
            class22.method219(class71_2, -1715982028);
         }

         client.field803 = false;
         class181.method3609(class226_1, 289898501);
      }
   }

   public static final class335 method710(int i_0, int i_1, int i_2, int i_3, int i_4, boolean bool_5, int i_6) {
      if (i_1 == -1) {
         i_4 = 0;
      } else if (i_4 == 2 && i_1 != 1) {
         i_4 = 1;
      }

      long long_7 = ((long)i_3 << 42) + ((long)i_4 << 40) + ((long)i_2 << 38) + (long)i_0 + ((long)i_1 << 16);
      class335 class335_9;
      if (!bool_5) {
         class335_9 = (class335) class265.field3440.method3376(long_7);
         if (class335_9 != null) {
            return class335_9;
         }
      }

      class265 class265_10 = class66.method1223(i_0, -1801255948);
      if (i_1 > 1 && class265_10.field3442 != null) {
         int i_11 = -1;

         for (int i_12 = 0; i_12 < 10; i_12++) {
            if (i_1 >= class265_10.field3445[i_12] && class265_10.field3445[i_12] != 0) {
               i_11 = class265_10.field3442[i_12];
            }
         }

         if (i_11 != -1) {
            class265_10 = class66.method1223(i_11, -2113280092);
         }
      }

      class136 class136_20 = class265_10.method4633(1, -408968972);
      if (class136_20 == null) {
         return null;
      } else {
         class335 class335_21 = null;
         if (class265_10.field3432 * -582464211 != -1) {
            class335_21 = method710(class265_10.field3475, 10, 1, 0, 0, true, -1786223770);
            if (class335_21 == null) {
               return null;
            }
         } else if (class265_10.field3486 != -1) {
            class335_21 = method710(class265_10.field3485, i_1, i_2, i_3, 0, false, -1786223770);
            if (class335_21 == null) {
               return null;
            }
         } else if (class265_10.field3488 != -1) {
            class335_21 = method710(class265_10.field3487, i_1, 0, 0, 0, false, -1786223770);
            if (class335_21 == null) {
               return null;
            }
         }

         int[] ints_13 = class331.field3886;
         int i_14 = class331.field3889;
         int i_15 = class331.field3885;
         int[] ints_16 = new int[4];
         class331.method5998(ints_16);
         class335_9 = new class335(36, 32);
         class331.method5999(class335_9.field3917, 36, 32);
         class331.method6000();
         class139.method3109();
         class139.method3110(16, 16);
         class139.field1782 = false;
         if (class265_10.field3488 != -1) {
            class335_21.method6121(0, 0);
         }

         int i_17 = class265_10.field3449;
         if (bool_5) {
            i_17 = (int)(1.5D * (double)i_17);
         } else if (i_2 == 2) {
            i_17 = (int)(1.04D * (double)i_17);
         }

         int i_18 = i_17 * class139.field1797[class265_10.field3457] >> 16;
         int i_19 = i_17 * class139.field1780[class265_10.field3457] >> 16;
         class136_20.method2946();
         class136_20.method2938(0, class265_10.field3451, class265_10.field3452, class265_10.field3457, class265_10.field3453, class136_20.field1894 / 2 + i_18 + class265_10.field3472, i_19 + class265_10.field3472);
         if (class265_10.field3486 != -1) {
            class335_21.method6121(0, 0);
         }

         if (i_2 >= 1) {
            class335_9.method6122(1);
         }

         if (i_2 >= 2) {
            class335_9.method6122(16777215);
         }

         if (i_3 != 0) {
            class335_9.method6123(i_3);
         }

         class331.method5999(class335_9.field3917, 36, 32);
         if (class265_10.field3432 * -582464211 != -1) {
            class335_21.method6121(0, 0);
         }

         if (i_4 == 1 || i_4 == 2 && class265_10.field3455 == 1) {
            class265.field3446.method5335(class195.method3649(i_1, (byte) 1), 0, 9, 16776960, 1);
         }

         if (!bool_5) {
            class265.field3440.method3374(class335_9, long_7);
         }

         class331.method5999(ints_13, i_14, i_15);
         class331.method6012(ints_16);
         class139.method3109();
         class139.field1782 = true;
         return class335_9;
      }
   }

   static final void method679(int i_0) {
      for (int i_1 = 0; i_1 < client.field884; i_1++) {
         --client.field887[i_1];
         if (client.field887[i_1] >= -10) {
            class110 class110_10 = client.field889[i_1];
            if (class110_10 == null) {
               Object obj_10000 = null;
               class110_10 = class110.method2385(class227.field2730, client.field857[i_1], 0);
               if (class110_10 == null) {
                  continue;
               }

               client.field887[i_1] += class110_10.method2382();
               client.field889[i_1] = class110_10;
            }

            if (client.field887[i_1] < 0) {
               int i_3;
               if (client.field888[i_1] != 0) {
                  int i_4 = (client.field888[i_1] & 0xff) * 128;
                  int i_5 = client.field888[i_1] >> 16 & 0xff;
                  int i_6 = i_5 * 128 + 64 - class223.field2562.field990;
                  if (i_6 < 0) {
                     i_6 = -i_6;
                  }

                  int i_7 = client.field888[i_1] >> 8 & 0xff;
                  int i_8 = i_7 * 128 + 64 - class223.field2562.field938;
                  if (i_8 < 0) {
                     i_8 = -i_8;
                  }

                  int i_9 = i_6 + i_8 - 128;
                  if (i_9 > i_4) {
                     client.field887[i_1] = -100;
                     continue;
                  }

                  if (i_9 < 0) {
                     i_9 = 0;
                  }

                  i_3 = (i_4 - i_9) * class282.field3617.field1060 / i_4;
               } else {
                  i_3 = class282.field3617.field1059;
               }

               if (i_3 > 0) {
                  class112 class112_11 = class110_10.method2387().method2428(class129.field1557);
                  class122 class122_12 = class122.method2578(class112_11, 100, i_3);
                  class122_12.method2581(client.field886[i_1] - 1);
                  field350.method2324(class122_12);
               }

               client.field887[i_1] = -100;
            }
         } else {
            --client.field884;

            for (int i_2 = i_1; i_2 < client.field884; i_2++) {
               client.field857[i_2] = client.field857[i_2 + 1];
               client.field889[i_2] = client.field889[i_2 + 1];
               client.field886[i_2] = client.field886[i_2 + 1];
               client.field887[i_2] = client.field887[i_2 + 1];
               client.field888[i_2] = client.field888[i_2 + 1];
            }

            --i_1;
         }
      }

      if (client.field883 && !class176.method3538((byte) 3)) {
         if (class282.field3617.field1056 != 0 && client.field779 != -1) {
            class85.method2054(class217.field2520, client.field779, 0, class282.field3617.field1056, false, 2000036832);
         }

         client.field883 = false;
      }

   }

   static final void method703(int i_0) {
      int[] ints_1 = class98.field1268;

      int i_2;
      for (i_2 = 0; i_2 < class98.field1263; i_2++) {
         class75 class75_3 = client.field764[ints_1[i_2]];
         if (class75_3 != null && class75_3.field991 > 0) {
            --class75_3.field991;
            if (class75_3.field991 == 0) {
               class75_3.field951 = null;
            }
         }
      }

      for (i_2 = 0; i_2 < client.field690; i_2++) {
         int i_5 = client.field691[i_2];
         class88 class88_4 = client.field689[i_5];
         if (class88_4 != null && class88_4.field991 > 0) {
            --class88_4.field991;
            if (class88_4.field991 == 0) {
               class88_4.field951 = null;
            }
         }
      }

   }

   static final void method672(class78 class78_0, int i_1) {
      int i_2 = Math.max(1, class78_0.field984 - client.field655);
      int i_3 = class78_0.field981 * 128 + class78_0.field941 * 64;
      int i_4 = class78_0.field982 * 128 + class78_0.field941 * 64;
      class78_0.field990 += (i_3 - class78_0.field990) / i_2;
      class78_0.field938 += (i_4 - class78_0.field938) / i_2;
      class78_0.field963 = 0;
      class78_0.field989 = class78_0.field986;
   }

   static int method704(int i_0, int i_1) {
      return i_0 * 3 + 600;
   }

   static final int method712(int i_0) {
      if (class282.field3617.field1052) {
         return class151.field1949;
      } else {
         int i_1 = class97.method2165(class75.field630, class69.field558, class151.field1949, 1611184503);
         return i_1 - class54.field421 < 800 && (class64.field507[class151.field1949][class75.field630 >> 7][class69.field558 >> 7] & 0x4) != 0 ? class151.field1949 : 3;
      }
   }

   static final void method709(class309 class309_0, int i_1, class75 class75_2, int i_3, int i_4) {
      byte b_5 = -1;
      if ((i_3 & 0x100) != 0) {
         class75_2.field981 = class309_0.method5625(1585396608);
         class75_2.field982 = class309_0.method5625(2111335492);
         class75_2.field958 = class309_0.method5625(1618816773);
         class75_2.field956 = class309_0.method5621((byte) 4);
         class75_2.field984 = class309_0.method5542(468907616) + client.field655;
         class75_2.field973 = class309_0.method5499(-1597569596) + client.field655;
         class75_2.field986 = class309_0.method5729(2102717279);
         if (class75_2.field631) {
            class75_2.field981 += class75_2.field632;
            class75_2.field982 += class75_2.field633;
            class75_2.field958 += class75_2.field632;
            class75_2.field956 += class75_2.field633;
            class75_2.field985 = 0;
         } else {
            class75_2.field981 += class75_2.field993[0];
            class75_2.field982 += class75_2.field968[0];
            class75_2.field958 += class75_2.field993[0];
            class75_2.field956 += class75_2.field968[0];
            class75_2.field985 = 1;
         }

         class75_2.field996 = 0;
      }

      if ((i_3 & 0x20) != 0) {
         class75_2.field951 = class309_0.method5589(-1900339128);
         if (class75_2.field951.charAt(0) == 126) {
            class75_2.field951 = class75_2.field951.substring(1);
            class234.method4129(2, class75_2.field619.method5186(-1541455272), class75_2.field951, (byte) -6);
         } else if (class75_2 == class223.field2562) {
            class234.method4129(2, class75_2.field619.method5186(-1413202507), class75_2.field951, (byte) 82);
         }

         class75_2.field952 = false;
         class75_2.field955 = 0;
         class75_2.field994 = 0;
         class75_2.field991 = 150;
      }

      int i_6;
      int i_9;
      int i_10;
      int i_13;
      if ((i_3 & 0x80) != 0) {
         i_6 = class309_0.method5542(468907616);
         class239 class239_7 = (class239) class152.method3363(class234.method4128((byte) 101), class309_0.method5531(-1468519987), -1641734364);
         boolean bool_8 = class309_0.method5661((byte) 7) == 1;
         i_9 = class309_0.method5532(-1946834755);
         i_10 = class309_0.field3751;
         if (class75_2.field619 != null && class75_2.field621 != null) {
            boolean bool_11 = false;
            if (class239_7.field3123 && class173.field2036.method1936(class75_2.field619, 1739203337)) {
               bool_11 = true;
            }

            if (!bool_11 && client.field762 == 0 && !class75_2.field625) {
               class98.field1264.field3751 = 0;
               class309_0.method5553(class98.field1264.field3752, 0, i_9, (byte) 15);
               class98.field1264.field3751 = 0;
               String string_12 = class306.method5386(class191.method3634(class93.method2107(class98.field1264, (byte) 18), 1710358127));
               class75_2.field951 = string_12.trim();
               class75_2.field955 = i_6 >> 8;
               class75_2.field994 = i_6 & 0xff;
               class75_2.field991 = 150;
               class75_2.field952 = bool_8;
               class75_2.field953 = class75_2 != class223.field2562 && class239_7.field3123 && "" != client.field871 && string_12.toLowerCase().indexOf(client.field871) == -1;
               if (class239_7.field3120) {
                  i_13 = bool_8 ? 91 : 1;
               } else {
                  i_13 = bool_8 ? 90 : 2;
               }

               if (class239_7.field3126 != -1) {
                  class234.method4129(i_13, class25.method248(class239_7.field3126, -1372315550) + class75_2.field619.method5186(-1643842937), string_12, (byte) -116);
               } else {
                  class234.method4129(i_13, class75_2.field619.method5186(-1017646730), string_12, (byte) -13);
               }
            }
         }

         class309_0.field3751 = i_9 + i_10;
      }

      if ((i_3 & 0x400) != 0) {
         for (i_6 = 0; i_6 < 3; i_6++) {
            class75_2.field610[i_6] = class309_0.method5589(-1160497151);
         }
      }

      if ((i_3 & 0x8) != 0) {
         class75_2.field959 = class309_0.method5542(468907616);
         if (class75_2.field985 == 0) {
            class75_2.field989 = class75_2.field959;
            class75_2.field959 = -1;
         }
      }

      int i_14;
      if ((i_3 & 0x10) != 0) {
         i_6 = class309_0.method5531(-1430927529);
         int i_15;
         int i_18;
         int i_20;
         if (i_6 > 0) {
            for (i_14 = 0; i_14 < i_6; i_14++) {
               i_9 = -1;
               i_10 = -1;
               i_20 = -1;
               i_18 = class309_0.method5483(772809583);
               if (i_18 == 32767) {
                  i_18 = class309_0.method5483(772809583);
                  i_10 = class309_0.method5483(772809583);
                  i_9 = class309_0.method5483(772809583);
                  i_20 = class309_0.method5483(772809583);
               } else if (i_18 != 32766) {
                  i_10 = class309_0.method5483(772809583);
               } else {
                  i_18 = -1;
               }

               i_15 = class309_0.method5483(772809583);
               class75_2.method1778(i_18, i_10, i_9, i_20, client.field655, i_15, -95637861);
            }
         }

         i_14 = class309_0.method5532(-1946834755);
         if (i_14 > 0) {
            for (i_18 = 0; i_18 < i_14; i_18++) {
               i_9 = class309_0.method5483(772809583);
               i_10 = class309_0.method5483(772809583);
               if (i_10 != 32767) {
                  i_20 = class309_0.method5483(772809583);
                  i_15 = class309_0.method5532(-1946834755);
                  i_13 = i_10 > 0 ? class309_0.method5533((byte) -43) : i_15;
                  class75_2.method1787(i_9, client.field655, i_10, i_20, i_15, i_13, -2088112043);
               } else {
                  class75_2.method1779(i_9, 640126998);
               }
            }
         }
      }

      if ((i_3 & 0x200) != 0) {
         class98.field1253[i_1] = class309_0.method5621((byte) 4);
      }

      if ((i_3 & 0x1) != 0) {
         class75_2.field980 = class309_0.method5499(-1597569596);
         if (class75_2.field980 == 65535) {
            class75_2.field980 = -1;
         }
      }

      if ((i_3 & 0x1000) != 0) {
         class75_2.field975 = class309_0.method5499(-1597569596);
         i_6 = class309_0.method5603(-380994417);
         class75_2.field979 = i_6 >> 16;
         class75_2.field978 = (i_6 & 0xffff) + client.field655;
         class75_2.field976 = 0;
         class75_2.field977 = 0;
         if (class75_2.field978 > client.field655) {
            class75_2.field976 = -1;
         }

         if (class75_2.field975 == 65535) {
            class75_2.field975 = -1;
         }
      }

      if ((i_3 & 0x800) != 0) {
         b_5 = class309_0.method5625(1823511603);
      }

      if ((i_3 & 0x2) != 0) {
         i_6 = class309_0.method5499(-1597569596);
         if (i_6 == 65535) {
            i_6 = -1;
         }

         i_14 = class309_0.method5531(1180645964);
         class64.method1210(class75_2, i_6, i_14, 1651593642);
      }

      if ((i_3 & 0x4) != 0) {
         i_6 = class309_0.method5533((byte) -63);
         byte[] bytes_17 = new byte[i_6];
         class310 class310_16 = new class310(bytes_17);
         class309_0.method5693(bytes_17, 0, i_6, (byte) -110);
         class98.field1254[i_1] = class310_16;
         class75_2.method1334(class310_16, (byte) 18);
      }

      if (class75_2.field631) {
         if (b_5 == 127) {
            class75_2.method1314(class75_2.field632, class75_2.field633, 273949660);
         } else {
            byte b_19;
            if (b_5 != -1) {
               b_19 = b_5;
            } else {
               b_19 = class98.field1253[i_1];
            }

            class75_2.method1313(class75_2.field632, class75_2.field633, b_19, 1635620178);
         }
      }

   }

}
