public class class258 {

   static int method4483(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      if (i_0 == 5000) {
         class85.field1095[++class253.field3267 - 1] = client.field795;
         return 1;
      } else if (i_0 == 5001) {
         class253.field3267 -= 3;
         client.field795 = class85.field1095[class253.field3267];
         client.field729 = class52.method840(class85.field1095[class253.field3267 + 1], 1066778190);
         if (client.field729 == null) {
            client.field729 = class320.field3832;
         }

         client.field866 = class85.field1095[class253.field3267 + 2];
         class196 class196_19 = class68.method1249(class192.field2308, client.field694.field1328, (byte) 1);
         class196_19.field2360.method5482(client.field795, (byte) -40);
         class196_19.field2360.method5482(client.field729.field3835, (byte) -111);
         class196_19.field2360.method5482(client.field866, (byte) -114);
         client.field694.method2295(class196_19, -378301257);
         return 1;
      } else {
         String string_4;
         int i_5;
         if (i_0 == 5002) {
            string_4 = class85.field1096[--class85.field1105];
            class253.field3267 -= 2;
            i_5 = class85.field1095[class253.field3267];
            int i_6 = class85.field1095[class253.field3267 + 1];
            class196 class196_7 = class68.method1249(class192.field2301, client.field694.field1328, (byte) 1);
            class196_7.field2360.method5482(class132.method2906(string_4, (byte) -1) + 2, (byte) -122);
            class196_7.field2360.method5492(string_4, -1748540705);
            class196_7.field2360.method5482(i_5 - 1, (byte) -96);
            class196_7.field2360.method5482(i_6, (byte) -98);
            client.field694.method2295(class196_7, 946852859);
            return 1;
         } else {
            int i_11;
            if (i_0 == 5003) {
               class253.field3267 -= 2;
               i_11 = class85.field1095[class253.field3267];
               i_5 = class85.field1095[class253.field3267 + 1];
               class73 class73_16 = class150.method3352(i_11, i_5, -876680442);
               if (class73_16 != null) {
                  class85.field1095[++class253.field3267 - 1] = class73_16.field598;
                  class85.field1095[++class253.field3267 - 1] = class73_16.field595;
                  class85.field1096[++class85.field1105 - 1] = class73_16.field597 != null ? class73_16.field597 : "";
                  class85.field1096[++class85.field1105 - 1] = class73_16.field594 != null ? class73_16.field594 : "";
                  class85.field1096[++class85.field1105 - 1] = class73_16.field602 != null ? class73_16.field602 : "";
                  class85.field1095[++class253.field3267 - 1] = class73_16.method1270(2060086850) ? 1 : (class73_16.method1273(2018415105) ? 2 : 0);
               } else {
                  class85.field1095[++class253.field3267 - 1] = -1;
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 5004) {
               i_11 = class85.field1095[--class253.field3267];
               class73 class73_17 = class195.method3650(i_11, 36521879);
               if (class73_17 != null) {
                  class85.field1095[++class253.field3267 - 1] = class73_17.field596;
                  class85.field1095[++class253.field3267 - 1] = class73_17.field595;
                  class85.field1096[++class85.field1105 - 1] = class73_17.field597 != null ? class73_17.field597 : "";
                  class85.field1096[++class85.field1105 - 1] = class73_17.field594 != null ? class73_17.field594 : "";
                  class85.field1096[++class85.field1105 - 1] = class73_17.field602 != null ? class73_17.field602 : "";
                  class85.field1095[++class253.field3267 - 1] = class73_17.method1270(1931732353) ? 1 : (class73_17.method1273(-433043885) ? 2 : 0);
               } else {
                  class85.field1095[++class253.field3267 - 1] = -1;
                  class85.field1095[++class253.field3267 - 1] = 0;
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 5005) {
               if (client.field729 == null) {
                  class85.field1095[++class253.field3267 - 1] = -1;
               } else {
                  class85.field1095[++class253.field3267 - 1] = client.field729.field3835;
               }

               return 1;
            } else if (i_0 == 5008) {
               string_4 = class85.field1096[--class85.field1105];
               i_5 = class85.field1095[--class253.field3267];
               String string_15 = string_4.toLowerCase();
               byte b_18 = 0;
               if (string_15.startsWith("yellow:")) {
                  b_18 = 0;
                  string_4 = string_4.substring("yellow:".length());
               } else if (string_15.startsWith("red:")) {
                  b_18 = 1;
                  string_4 = string_4.substring("red:".length());
               } else if (string_15.startsWith("green:")) {
                  b_18 = 2;
                  string_4 = string_4.substring("green:".length());
               } else if (string_15.startsWith("cyan:")) {
                  b_18 = 3;
                  string_4 = string_4.substring("cyan:".length());
               } else if (string_15.startsWith("purple:")) {
                  b_18 = 4;
                  string_4 = string_4.substring("purple:".length());
               } else if (string_15.startsWith("white:")) {
                  b_18 = 5;
                  string_4 = string_4.substring("white:".length());
               } else if (string_15.startsWith("flash1:")) {
                  b_18 = 6;
                  string_4 = string_4.substring("flash1:".length());
               } else if (string_15.startsWith("flash2:")) {
                  b_18 = 7;
                  string_4 = string_4.substring("flash2:".length());
               } else if (string_15.startsWith("flash3:")) {
                  b_18 = 8;
                  string_4 = string_4.substring("flash3:".length());
               } else if (string_15.startsWith("glow1:")) {
                  b_18 = 9;
                  string_4 = string_4.substring("glow1:".length());
               } else if (string_15.startsWith("glow2:")) {
                  b_18 = 10;
                  string_4 = string_4.substring("glow2:".length());
               } else if (string_15.startsWith("glow3:")) {
                  b_18 = 11;
                  string_4 = string_4.substring("glow3:".length());
               } else if (class49.field372 != class199.field2382) {
                  if (string_15.startsWith("yellow:")) {
                     b_18 = 0;
                     string_4 = string_4.substring("yellow:".length());
                  } else if (string_15.startsWith("red:")) {
                     b_18 = 1;
                     string_4 = string_4.substring("red:".length());
                  } else if (string_15.startsWith("green:")) {
                     b_18 = 2;
                     string_4 = string_4.substring("green:".length());
                  } else if (string_15.startsWith("cyan:")) {
                     b_18 = 3;
                     string_4 = string_4.substring("cyan:".length());
                  } else if (string_15.startsWith("purple:")) {
                     b_18 = 4;
                     string_4 = string_4.substring("purple:".length());
                  } else if (string_15.startsWith("white:")) {
                     b_18 = 5;
                     string_4 = string_4.substring("white:".length());
                  } else if (string_15.startsWith("flash1:")) {
                     b_18 = 6;
                     string_4 = string_4.substring("flash1:".length());
                  } else if (string_15.startsWith("flash2:")) {
                     b_18 = 7;
                     string_4 = string_4.substring("flash2:".length());
                  } else if (string_15.startsWith("flash3:")) {
                     b_18 = 8;
                     string_4 = string_4.substring("flash3:".length());
                  } else if (string_15.startsWith("glow1:")) {
                     b_18 = 9;
                     string_4 = string_4.substring("glow1:".length());
                  } else if (string_15.startsWith("glow2:")) {
                     b_18 = 10;
                     string_4 = string_4.substring("glow2:".length());
                  } else if (string_15.startsWith("glow3:")) {
                     b_18 = 11;
                     string_4 = string_4.substring("glow3:".length());
                  }
               }

               string_15 = string_4.toLowerCase();
               byte b_8 = 0;
               if (string_15.startsWith("wave:")) {
                  b_8 = 1;
                  string_4 = string_4.substring("wave:".length());
               } else if (string_15.startsWith("wave2:")) {
                  b_8 = 2;
                  string_4 = string_4.substring("wave2:".length());
               } else if (string_15.startsWith("shake:")) {
                  b_8 = 3;
                  string_4 = string_4.substring("shake:".length());
               } else if (string_15.startsWith("scroll:")) {
                  b_8 = 4;
                  string_4 = string_4.substring("scroll:".length());
               } else if (string_15.startsWith("slide:")) {
                  b_8 = 5;
                  string_4 = string_4.substring("slide:".length());
               } else if (class199.field2382 != class49.field372) {
                  if (string_15.startsWith("wave:")) {
                     b_8 = 1;
                     string_4 = string_4.substring("wave:".length());
                  } else if (string_15.startsWith("wave2:")) {
                     b_8 = 2;
                     string_4 = string_4.substring("wave2:".length());
                  } else if (string_15.startsWith("shake:")) {
                     b_8 = 3;
                     string_4 = string_4.substring("shake:".length());
                  } else if (string_15.startsWith("scroll:")) {
                     b_8 = 4;
                     string_4 = string_4.substring("scroll:".length());
                  } else if (string_15.startsWith("slide:")) {
                     b_8 = 5;
                     string_4 = string_4.substring("slide:".length());
                  }
               }

               class196 class196_9 = class68.method1249(class192.field2304, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5482(0, (byte) -57);
               int i_10 = class196_9.field2360.field3751;
               class196_9.field2360.method5482(i_5, (byte) -119);
               class196_9.field2360.method5482(b_18, (byte) -5);
               class196_9.field2360.method5482(b_8, (byte) -80);
               class152.method3355(class196_9.field2360, string_4, -1031927437);
               class196_9.field2360.method5697(class196_9.field2360.field3751 - i_10, -2075050787);
               client.field694.method2295(class196_9, 1775721640);
               return 1;
            } else if (i_0 == 5009) {
               class85.field1105 -= 2;
               string_4 = class85.field1096[class85.field1105];
               String string_13 = class85.field1096[class85.field1105 + 1];
               class196 class196_12 = class68.method1249(class192.field2230, client.field694.field1328, (byte) 1);
               class196_12.field2360.method5486(0, (byte) -86);
               int i_14 = class196_12.field2360.field3751;
               class196_12.field2360.method5492(string_4, -1748540705);
               class152.method3355(class196_12.field2360, string_13, -2102652811);
               class196_12.field2360.method5509(class196_12.field2360.field3751 - i_14, (byte) 67);
               client.field694.method2295(class196_12, 479541239);
               return 1;
            } else if (i_0 != 5015) {
               if (i_0 == 5016) {
                  class85.field1095[++class253.field3267 - 1] = client.field866;
                  return 1;
               } else if (i_0 == 5017) {
                  i_11 = class85.field1095[--class253.field3267];
                  class85.field1095[++class253.field3267 - 1] = class57.method907(i_11, 1227984661);
                  return 1;
               } else if (i_0 == 5018) {
                  i_11 = class85.field1095[--class253.field3267];
                  class85.field1095[++class253.field3267 - 1] = class216.method3958(i_11, (byte) 75);
                  return 1;
               } else if (i_0 == 5019) {
                  i_11 = class85.field1095[--class253.field3267];
                  class85.field1095[++class253.field3267 - 1] = class39.method595(i_11, -1859709852);
                  return 1;
               } else if (i_0 == 5020) {
                  string_4 = class85.field1096[--class85.field1105];
                  class66.method1219(string_4, (byte) 90);
                  return 1;
               } else if (i_0 == 5021) {
                  client.field871 = class85.field1096[--class85.field1105].toLowerCase().trim();
                  return 1;
               } else if (i_0 == 5022) {
                  class85.field1096[++class85.field1105 - 1] = client.field871;
                  return 1;
               } else if (i_0 == 5023) {
                  string_4 = class85.field1096[--class85.field1105];
                  System.out.println(string_4);
                  return 1;
               } else {
                  return 2;
               }
            } else {
               if (class223.field2562 != null && class223.field2562.field619 != null) {
                  string_4 = class223.field2562.field619.method5186(-1783348700);
               } else {
                  string_4 = "";
               }

               class85.field1096[++class85.field1105 - 1] = string_4;
               return 1;
            }
         }
      }
   }

}
