import java.util.Random;

public class class126 {

   static int[] field1531 = new int[32768];
   static int[] field1533;
   static int[] field1526;
   static int[] field1535;
   static int[] field1539;
   static int[] field1537;
   static int[] field1538;
   static int[] field1530;
   int[] field1522 = new int[] {0, 0, 0, 0, 0};
   int[] field1523 = new int[] {0, 0, 0, 0, 0};
   int[] field1532 = new int[] {0, 0, 0, 0, 0};
   int field1536 = 0;
   int field1514 = 100;
   int field1529 = 500;
   int field1525 = 0;
   class115 field1534;
   class115 field1515;
   class115 field1516;
   class115 field1517;
   class115 field1524;
   class115 field1519;
   class115 field1520;
   class115 field1521;
   class123 field1527;
   class115 field1528;

   static {
      Random random_0 = new Random(0L);

      int i_1;
      for (i_1 = 0; i_1 < 32768; i_1++) {
         field1531[i_1] = (random_0.nextInt() & 0x2) - 1;
      }

      field1533 = new int[32768];

      for (i_1 = 0; i_1 < 32768; i_1++) {
         field1533[i_1] = (int)(Math.sin((double)i_1 / 5215.1903D) * 16384.0D);
      }

      field1526 = new int[220500];
      field1535 = new int[5];
      field1539 = new int[5];
      field1537 = new int[5];
      field1538 = new int[5];
      field1530 = new int[5];
   }

   final void method2782(class310 class310_1) {
      this.field1534 = new class115();
      this.field1534.method2531(class310_1);
      this.field1515 = new class115();
      this.field1515.method2531(class310_1);
      int i_2 = class310_1.method5661((byte) 30);
      if (i_2 != 0) {
         --class310_1.field3751;
         this.field1516 = new class115();
         this.field1516.method2531(class310_1);
         this.field1517 = new class115();
         this.field1517.method2531(class310_1);
      }

      i_2 = class310_1.method5661((byte) -61);
      if (i_2 != 0) {
         --class310_1.field3751;
         this.field1524 = new class115();
         this.field1524.method2531(class310_1);
         this.field1519 = new class115();
         this.field1519.method2531(class310_1);
      }

      i_2 = class310_1.method5661((byte) 54);
      if (i_2 != 0) {
         --class310_1.field3751;
         this.field1520 = new class115();
         this.field1520.method2531(class310_1);
         this.field1521 = new class115();
         this.field1521.method2531(class310_1);
      }

      for (int i_3 = 0; i_3 < 10; i_3++) {
         int i_4 = class310_1.method5483(772809583);
         if (i_4 == 0) {
            break;
         }

         this.field1522[i_3] = i_4;
         this.field1523[i_3] = class310_1.method5515(130333316);
         this.field1532[i_3] = class310_1.method5483(772809583);
      }

      this.field1536 = class310_1.method5483(772809583);
      this.field1514 = class310_1.method5483(772809583);
      this.field1529 = class310_1.method5729(316978676);
      this.field1525 = class310_1.method5729(-2125043501);
      this.field1527 = new class123();
      this.field1528 = new class115();
      this.field1527.method2744(class310_1, this.field1528);
   }

   final int[] method2785(int i_1, int i_2) {
      class311.method5754(field1526, 0, i_1);
      if (i_2 < 10) {
         return field1526;
      } else {
         double d_3 = (double)i_1 / ((double)i_2 + 0.0D);
         this.field1534.method2521();
         this.field1515.method2521();
         int i_5 = 0;
         int i_6 = 0;
         int i_7 = 0;
         if (this.field1516 != null) {
            this.field1516.method2521();
            this.field1517.method2521();
            i_5 = (int)((double)(this.field1516.field1448 - this.field1516.field1447) * 32.768D / d_3);
            i_6 = (int)((double)this.field1516.field1447 * 32.768D / d_3);
         }

         int i_8 = 0;
         int i_9 = 0;
         int i_10 = 0;
         if (this.field1524 != null) {
            this.field1524.method2521();
            this.field1519.method2521();
            i_8 = (int)((double)(this.field1524.field1448 - this.field1524.field1447) * 32.768D / d_3);
            i_9 = (int)((double)this.field1524.field1447 * 32.768D / d_3);
         }

         int i_11;
         for (i_11 = 0; i_11 < 5; i_11++) {
            if (this.field1522[i_11] != 0) {
               field1535[i_11] = 0;
               field1539[i_11] = (int)((double)this.field1532[i_11] * d_3);
               field1537[i_11] = (this.field1522[i_11] << 14) / 100;
               field1538[i_11] = (int)((double)(this.field1534.field1448 - this.field1534.field1447) * 32.768D * Math.pow(1.0057929410678534D, (double)this.field1523[i_11]) / d_3);
               field1530[i_11] = (int)((double)this.field1534.field1447 * 32.768D / d_3);
            }
         }

         int i_12;
         int i_13;
         int i_14;
         int i_15;
         for (i_11 = 0; i_11 < i_1; i_11++) {
            i_12 = this.field1534.method2522(i_1);
            i_13 = this.field1515.method2522(i_1);
            if (this.field1516 != null) {
               i_14 = this.field1516.method2522(i_1);
               i_15 = this.field1517.method2522(i_1);
               i_12 += this.method2781(i_7, i_15, this.field1516.field1444) >> 1;
               i_7 = i_7 + i_6 + (i_14 * i_5 >> 16);
            }

            if (this.field1524 != null) {
               i_14 = this.field1524.method2522(i_1);
               i_15 = this.field1519.method2522(i_1);
               i_13 = i_13 * ((this.method2781(i_10, i_15, this.field1524.field1444) >> 1) + 32768) >> 15;
               i_10 = i_10 + i_9 + (i_14 * i_8 >> 16);
            }

            for (i_14 = 0; i_14 < 5; i_14++) {
               if (this.field1522[i_14] != 0) {
                  i_15 = field1539[i_14] + i_11;
                  if (i_15 < i_1) {
                     field1526[i_15] += this.method2781(field1535[i_14], i_13 * field1537[i_14] >> 15, this.field1534.field1444);
                     field1535[i_14] += (i_12 * field1538[i_14] >> 16) + field1530[i_14];
                  }
               }
            }
         }

         int i_16;
         if (this.field1520 != null) {
            this.field1520.method2521();
            this.field1521.method2521();
            i_11 = 0;
            boolean bool_19 = false;
            boolean bool_20 = true;

            for (i_14 = 0; i_14 < i_1; i_14++) {
               i_15 = this.field1520.method2522(i_1);
               i_16 = this.field1521.method2522(i_1);
               if (bool_20) {
                  i_12 = (i_15 * (this.field1520.field1448 - this.field1520.field1447) >> 8) + this.field1520.field1447;
               } else {
                  i_12 = (i_16 * (this.field1520.field1448 - this.field1520.field1447) >> 8) + this.field1520.field1447;
               }

               i_11 += 256;
               if (i_11 >= i_12) {
                  i_11 = 0;
                  bool_20 = !bool_20;
               }

               if (bool_20) {
                  field1526[i_14] = 0;
               }
            }
         }

         if (this.field1536 > 0 && this.field1514 > 0) {
            i_11 = (int)((double)this.field1536 * d_3);

            for (i_12 = i_11; i_12 < i_1; i_12++) {
               field1526[i_12] += field1526[i_12 - i_11] * this.field1514 / 100;
            }
         }

         if (this.field1527.field1500[0] > 0 || this.field1527.field1500[1] > 0) {
            this.field1528.method2521();
            i_11 = this.field1528.method2522(i_1 + 1);
            i_12 = this.field1527.method2743(0, (float)i_11 / 65536.0F);
            i_13 = this.field1527.method2743(1, (float)i_11 / 65536.0F);
            if (i_1 >= i_12 + i_13) {
               i_14 = 0;
               i_15 = i_13;
               if (i_13 > i_1 - i_12) {
                  i_15 = i_1 - i_12;
               }

               int i_17;
               while (i_14 < i_15) {
                  i_16 = (int)((long)field1526[i_14 + i_12] * (long)class123.field1502 >> 16);

                  for (i_17 = 0; i_17 < i_12; i_17++) {
                     i_16 += (int)((long)field1526[i_14 + i_12 - 1 - i_17] * (long)class123.field1504[0][i_17] >> 16);
                  }

                  for (i_17 = 0; i_17 < i_14; i_17++) {
                     i_16 -= (int)((long)field1526[i_14 - 1 - i_17] * (long)class123.field1504[1][i_17] >> 16);
                  }

                  field1526[i_14] = i_16;
                  i_11 = this.field1528.method2522(i_1 + 1);
                  ++i_14;
               }

               i_15 = 128;

               while (true) {
                  if (i_15 > i_1 - i_12) {
                     i_15 = i_1 - i_12;
                  }

                  int i_18;
                  while (i_14 < i_15) {
                     i_17 = (int)((long)field1526[i_14 + i_12] * (long)class123.field1502 >> 16);

                     for (i_18 = 0; i_18 < i_12; i_18++) {
                        i_17 += (int)((long)field1526[i_14 + i_12 - 1 - i_18] * (long)class123.field1504[0][i_18] >> 16);
                     }

                     for (i_18 = 0; i_18 < i_13; i_18++) {
                        i_17 -= (int)((long)field1526[i_14 - 1 - i_18] * (long)class123.field1504[1][i_18] >> 16);
                     }

                     field1526[i_14] = i_17;
                     i_11 = this.field1528.method2522(i_1 + 1);
                     ++i_14;
                  }

                  if (i_14 >= i_1 - i_12) {
                     while (i_14 < i_1) {
                        i_17 = 0;

                        for (i_18 = i_14 + i_12 - i_1; i_18 < i_12; i_18++) {
                           i_17 += (int)((long)field1526[i_14 + i_12 - 1 - i_18] * (long)class123.field1504[0][i_18] >> 16);
                        }

                        for (i_18 = 0; i_18 < i_13; i_18++) {
                           i_17 -= (int)((long)field1526[i_14 - 1 - i_18] * (long)class123.field1504[1][i_18] >> 16);
                        }

                        field1526[i_14] = i_17;
                        this.field1528.method2522(i_1 + 1);
                        ++i_14;
                     }
                     break;
                  }

                  i_12 = this.field1527.method2743(0, (float)i_11 / 65536.0F);
                  i_13 = this.field1527.method2743(1, (float)i_11 / 65536.0F);
                  i_15 += 128;
               }
            }
         }

         for (i_11 = 0; i_11 < i_1; i_11++) {
            if (field1526[i_11] < -32768) {
               field1526[i_11] = -32768;
            }

            if (field1526[i_11] > 32767) {
               field1526[i_11] = 32767;
            }
         }

         return field1526;
      }
   }

   final int method2781(int i_1, int i_2, int i_3) {
      return i_3 == 1 ? ((i_1 & 0x7fff) < 16384 ? i_2 : -i_2) : (i_3 == 2 ? field1533[i_1 & 0x7fff] * i_2 >> 14 : (i_3 == 3 ? (i_2 * (i_1 & 0x7fff) >> 14) - i_2 : (i_3 == 4 ? i_2 * field1531[i_1 / 2607 & 0x7fff] : 0)));
   }

}
