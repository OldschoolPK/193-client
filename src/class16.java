import java.awt.Font;
import java.net.URL;

public class class16 {

   static Font field65;
   public static int field71;
   String field66;
   String field67;
   public final int field68;
   public final long field69;
   public final class18 field72;

   class16(class310 class310_1, byte b_2, int i_3) {
      this.field66 = class310_1.method5589(-1953792272);
      this.field67 = class310_1.method5589(1455153283);
      this.field68 = class310_1.method5729(-461173336);
      this.field69 = class310_1.method5508(2054741955);
      int i_4 = class310_1.method5507(-424973928);
      int i_5 = class310_1.method5507(-1385052339);
      this.field72 = new class18();
      this.field72.method171(2, 1068160748);
      this.field72.method176(b_2, 1708580418);
      this.field72.field79 = i_4;
      this.field72.field78 = i_5;
      this.field72.field81 = 0;
      this.field72.field82 = 0;
      this.field72.field77 = i_3;
   }

   public String method156(int i_1) {
      return this.field66;
   }

   public String method151(byte b_1) {
      return this.field67;
   }

   protected static final void method153(byte b_0) {
      class225.field2574.vmethod3550(1951011592);

      int i_1;
      for (i_1 = 0; i_1 < 32; i_1++) {
         class62.field449[i_1] = 0L;
      }

      for (i_1 = 0; i_1 < 32; i_1++) {
         class62.field459[i_1] = 0L;
      }

      class8.field31 = 0;
   }

   static boolean method157(int i_0) {
      try {
         if (class80.field1026 == null) {
            class80.field1026 = class21.field100.method3354(new URL(class17.field75), -1941564889);
         } else if (class80.field1026.method3368((byte) 109)) {
            byte[] bytes_1 = class80.field1026.method3369(-884175242);
            class310 class310_2 = new class310(bytes_1);
            class310_2.method5507(2013157336);
            class80.field1017 = class310_2.method5729(-354101496);
            class80.field1012 = new class80[class80.field1017];

            class80 class80_4;
            for (int i_3 = 0; i_3 < class80.field1017; class80_4.field1024 = i_3++) {
               class80_4 = class80.field1012[i_3] = new class80();
               class80_4.field1018 = class310_2.method5729(1832311683);
               class80_4.field1019 = class310_2.method5507(-1321066780);
               class80_4.field1021 = class310_2.method5589(1812484623);
               class80_4.field1022 = class310_2.method5589(1508923265);
               class80_4.field1023 = class310_2.method5661((byte) -43);
               class80_4.field1020 = class310_2.method5505((short) 1559);
            }

            class103.method2287(class80.field1012, 0, class80.field1012.length - 1, class80.field1016, class80.field1015, -2106242984);
            class80.field1026 = null;
            return true;
         }
      } catch (Exception exception_5) {
         exception_5.printStackTrace();
         class80.field1026 = null;
      }

      return false;
   }

}
