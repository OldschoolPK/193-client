public class class320 {

   static final class320 field3833 = new class320(0);
   public static final class320 field3832 = new class320(1);
   static final class320 field3834 = new class320(2);
   public final int field3835;

   class320(int i_1) {
      this.field3835 = i_1;
   }

   static final byte[] method5895(byte[] bytes_0, byte b_1) {
      class310 class310_2 = new class310(bytes_0);
      int i_3 = class310_2.method5661((byte) -5);
      int i_4 = class310_2.method5507(1915562503);
      if (i_4 < 0 || class244.field3171 != 0 && i_4 > class244.field3171) {
         throw new RuntimeException();
      } else if (i_3 == 0) {
         byte[] bytes_5 = new byte[i_4];
         class310_2.method5514(bytes_5, 0, i_4, 1075075180);
         return bytes_5;
      } else {
         int i_7 = class310_2.method5507(309541888);
         if (i_7 < 0 || class244.field3171 != 0 && i_7 > class244.field3171) {
            throw new RuntimeException();
         } else {
            byte[] bytes_6 = new byte[i_7];
            if (i_3 == 1) {
               class314.method5787(bytes_6, i_7, bytes_0, i_4, 9);
            } else {
               class244.field3175.method6511(class310_2, bytes_6, (byte) -11);
            }

            return bytes_6;
         }
      }
   }

   public static class305 method5891(class244 class244_0, class244 class244_1, int i_2, int i_3, int i_4) {
      return !class304.method5323(class244_0, i_2, i_3, 975575420) ? null : class308.method5442(class244_1.method4160(i_2, i_3, (short) -10490), -1217906561);
   }

   static class334 method5894(byte b_0) {
      class334 class334_1 = new class334();
      class334_1.field3910 = class215.field2511;
      class334_1.field3906 = class336.field3929;
      class334_1.field3907 = class336.field3925[0];
      class334_1.field3908 = class336.field3926[0];
      class334_1.field3905 = class336.field3927[0];
      class334_1.field3909 = class336.field3928[0];
      class334_1.field3904 = class336.field3924;
      class334_1.field3903 = class15.field62[0];
      class336.field3925 = null;
      class336.field3926 = null;
      class336.field3927 = null;
      class336.field3928 = null;
      class336.field3924 = null;
      class15.field62 = null;
      return class334_1;
   }

}
