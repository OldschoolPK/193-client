import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;

public abstract class class287 {

   int field3640 = 0;
   Comparator field3644 = null;
   final int field3647;
   class284[] field3641;
   HashMap field3642;
   HashMap field3643;

   class287(int i_1) {
      this.field3647 = i_1;
      this.field3641 = this.vmethod5226(i_1, -1524839332);
      this.field3642 = new HashMap(i_1 / 8);
      this.field3643 = new HashMap(i_1 / 8);
   }

   public class284 method5074(class293 class293_1, int i_2) {
      class284 class284_3 = this.method5124(class293_1, -1927286377);
      return class284_3 != null ? class284_3 : this.method5076(class293_1, 2109358184);
   }

   class284 method5072(class293 class293_1, class293 class293_2, int i_3) {
      if (this.method5124(class293_1, -1605823231) != null) {
         throw new IllegalStateException();
      } else {
         class284 class284_4 = this.vmethod5247((byte) -99);
         class284_4.method5033(class293_1, class293_2, -1682462851);
         this.method5089(class284_4, 1107214584);
         this.method5140(class284_4, -483791929);
         return class284_4;
      }
   }

   final int method5137(class284 class284_1, int i_2) {
      for (int i_3 = 0; i_3 < this.field3640; i_3++) {
         if (this.field3641[i_3] == class284_1) {
            return i_3;
         }
      }

      return -1;
   }

   final void method5085(class284 class284_1, short s_2) {
      if (this.field3642.remove(class284_1.field3628) == null) {
         throw new IllegalStateException();
      } else {
         if (class284_1.field3627 != null) {
            this.field3643.remove(class284_1.field3627);
         }

      }
   }

   class284 method5124(class293 class293_1, int i_2) {
      return !class293_1.method5187((byte) -108) ? null : (class284) this.field3642.get(class293_1);
   }

   public int method5071(int i_1) {
      return this.field3640;
   }

   public boolean method5122(byte b_1) {
      return this.field3640 == this.field3647;
   }

   public final void method5082(int i_1) {
      if (this.field3644 == null) {
         Arrays.sort(this.field3641, 0, this.field3640);
      } else {
         Arrays.sort(this.field3641, 0, this.field3640, this.field3644);
      }

   }

   public boolean method5073(class293 class293_1, byte b_2) {
      return !class293_1.method5187((byte) -68) ? false : (this.field3642.containsKey(class293_1) ? true : this.field3643.containsKey(class293_1));
   }

   class284 method5076(class293 class293_1, int i_2) {
      return !class293_1.method5187((byte) -107) ? null : (class284) this.field3643.get(class293_1);
   }

   abstract class284 vmethod5247(byte var1);

   final void method5088(int i_1, int i_2) {
      --this.field3640;
      if (i_1 < this.field3640) {
         System.arraycopy(this.field3641, i_1 + 1, this.field3641, i_1, this.field3640 - i_1);
      }

   }

   public void method5101(byte b_1) {
      this.field3640 = 0;
      Arrays.fill(this.field3641, (Object) null);
      this.field3642.clear();
      this.field3643.clear();
   }

   final void method5078(class284 class284_1, int i_2) {
      int i_3 = this.method5137(class284_1, 1245630013);
      if (i_3 != -1) {
         this.method5088(i_3, 365639793);
         this.method5085(class284_1, (short) 255);
      }
   }

   public final class284 method5081(int i_1, int i_2) {
      if (i_1 >= 0 && i_1 < this.field3640) {
         return this.field3641[i_1];
      } else {
         throw new ArrayIndexOutOfBoundsException(i_1);
      }
   }

   final void method5140(class284 class284_1, int i_2) {
      this.field3642.put(class284_1.field3628, class284_1);
      if (class284_1.field3627 != null) {
         class284 class284_3 = (class284) this.field3643.put(class284_1.field3627, class284_1);
         if (class284_3 != null && class284_3 != class284_1) {
            class284_3.field3627 = null;
         }
      }

   }

   abstract class284[] vmethod5226(int var1, int var2);

   public final boolean method5077(class293 class293_1, byte b_2) {
      class284 class284_3 = this.method5124(class293_1, -1755740358);
      if (class284_3 == null) {
         return false;
      } else {
         this.method5078(class284_3, 1649316438);
         return true;
      }
   }

   final void method5089(class284 class284_1, int i_2) {
      this.field3641[++this.field3640 - 1] = class284_1;
   }

   class284 method5079(class293 class293_1, byte b_2) {
      return this.method5072(class293_1, (class293) null, 1547669313);
   }

   final void method5083(class284 class284_1, class293 class293_2, class293 class293_3, int i_4) {
      this.method5085(class284_1, (short) 255);
      class284_1.method5033(class293_2, class293_3, -1682462851);
      this.method5140(class284_1, 961436842);
   }

   public final void method5069(int i_1) {
      this.field3644 = null;
   }

   public final void method5139(Comparator comparator_1, int i_2) {
      if (this.field3644 == null) {
         this.field3644 = comparator_1;
      } else if (this.field3644 instanceof class285) {
         ((class285) this.field3644).method5048(comparator_1, 118963424);
      }

   }

}
