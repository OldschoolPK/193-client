public abstract class class43 {

   static class335[] field318;
   int field320;
   int field323;
   public final class222 field319;
   public final class222 field321;

   class43(class222 class222_1, class222 class222_2) {
      this.field319 = class222_1;
      this.field321 = class222_2;
   }

   abstract class37 vmethod633(int var1);

   boolean method637(int i_1, int i_2, int i_3) {
      if (!this.method634(1161567228)) {
         return false;
      } else {
         class252 class252_4 = class163.method3458(this.vmethod630(1563693233), -1804544338);
         int i_5 = this.vmethod654((byte) -79);
         int i_6 = this.vmethod635(1082591682);
         switch(class252_4.field3259.field3492) {
         case 0:
            if (i_1 < this.field320 || i_1 >= i_5 + this.field320) {
               return false;
            }
            break;
         case 1:
            if (i_1 >= this.field320 - i_5 / 2 && i_1 <= i_5 / 2 + this.field320) {
               break;
            }

            return false;
         case 2:
            if (i_1 <= this.field320 - i_5 || i_1 > this.field320) {
               return false;
            }
         }

         switch(class252_4.field3260.field3227) {
         case 0:
            if (i_2 >= this.field323 && i_2 < i_6 + this.field323) {
               break;
            }

            return false;
         case 1:
            if (i_2 > this.field323 - i_6 && i_2 <= this.field323) {
               break;
            }

            return false;
         case 2:
            if (i_2 < this.field323 - i_6 / 2 || i_2 > i_6 / 2 + this.field323) {
               return false;
            }
         }

         return true;
      }
   }

   boolean method634(int i_1) {
      return this.vmethod630(1020192645) >= 0;
   }

   public abstract int vmethod630(int var1);

   boolean method638(int i_1, int i_2, int i_3) {
      class37 class37_4 = this.vmethod633(1889474780);
      return class37_4 == null ? false : (i_1 >= this.field320 - class37_4.field255 / 2 && i_1 <= class37_4.field255 / 2 + this.field320 ? i_2 >= this.field323 && i_2 <= class37_4.field256 + this.field323 : false);
   }

   abstract int vmethod654(byte var1);

   abstract int vmethod635(int var1);

   boolean method642(int i_1, int i_2, int i_3) {
      return this.method637(i_1, i_2, 762931724) ? true : this.method638(i_1, i_2, -373903296);
   }

   static boolean method657(long long_0) {
      return (int)(long_0 >>> 16 & 0x1L) == 1;
   }

   static void method631(boolean bool_0, int i_1) {
      class94.field1193 = "";
      class94.field1194 = "Enter your username/email & password.";
      class94.field1195 = "";
      class94.field1191 = 2;
      if (bool_0) {
         class94.field1197 = "";
      }

      class219.method3978(-2069010716);
      if (client.field684 && class94.field1196 != null && class94.field1196.length() > 0) {
         class94.field1181 = 1;
      } else {
         class94.field1181 = 0;
      }

   }

   static String method632(String string_0, boolean bool_1, byte b_2) {
      String str_3 = bool_1 ? "https://" : "http://";
      if (client.field647 == 1) {
         string_0 = string_0 + "-wtrc";
      } else if (client.field647 == 2) {
         string_0 = string_0 + "-wtqa";
      } else if (client.field647 == 3) {
         string_0 = string_0 + "-wtwip";
      } else if (client.field647 == 5) {
         string_0 = string_0 + "-wti";
      } else if (client.field647 == 4) {
         string_0 = "local";
      }

      String str_4 = "";
      if (class42.field317 != null) {
         str_4 = "/p=" + class42.field317;
      }

      String str_5 = "runescape.com";
      return str_3 + string_0 + "." + str_5 + "/l=" + class49.field372 + "/a=" + class207.field2455 + str_4 + "/";
   }

   static int method656(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      if (i_0 == 5630) {
         client.field733 = 250;
         return 1;
      } else if (i_0 != 5631 && i_0 != 5633) {
         if (i_0 == 5632) {
            class85.field1095[++class253.field3267 - 1] = 26;
            return 1;
         } else {
            return 2;
         }
      } else {
         class85.field1105 -= 2;
         return 1;
      }
   }

   static final void method655(class78 class78_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if (class78_0 != null && class78_0.vmethod2080(901185537)) {
         if (class78_0 instanceof class88) {
            class267 class267_7 = ((class88) class78_0).field1135;
            if (class267_7.field3524 != null) {
               class267_7 = class267_7.method4731((byte) -87);
            }

            if (class267_7 == null) {
               return;
            }
         }

         int i_76 = class98.field1263;
         int[] ints_8 = class98.field1268;
         byte b_9 = 0;
         if (i_1 < i_76 && class78_0.field983 == client.field655 && class3.method42((class75) class78_0, (byte) 0)) {
            class75 class75_10 = (class75) class78_0;
            if (i_1 < i_76) {
               class15.method144(class78_0, class78_0.field988 + 15, -563338359);
               class306 class306_11 = (class306) client.field699.get(class302.field3699);
               byte b_12 = 9;
               class306_11.method5436(class75_10.field619.method5186(-1744472032), i_2 + client.field747, i_3 + client.field748 - b_12, 16777215, 0);
               b_9 = 18;
            }
         }

         int i_77 = -2;
         int i_16;
         int i_23;
         int i_24;
         if (!class78_0.field957.method4807()) {
            class15.method144(class78_0, class78_0.field988 + 15, -1594388879);

            for (class87 class87_88 = (class87) class78_0.field957.method4806(); class87_88 != null; class87_88 = (class87) class78_0.field957.method4805()) {
               class79 class79_78 = class87_88.method2065(client.field655, 241304774);
               if (class79_78 == null) {
                  if (class87_88.method2066((byte) 127)) {
                     class87_88.method3628();
                  }
               } else {
                  class257 class257_13 = class87_88.field1126;
                  class335 class335_14 = class257_13.method4459((short) -8563);
                  class335 class335_15 = class257_13.method4462(-1946834755);
                  int i_17 = 0;
                  if (class335_14 != null && class335_15 != null) {
                     if (class257_13.field3310 * 2 < class335_15.field3913) {
                        i_17 = class257_13.field3310;
                     }

                     i_16 = class335_15.field3913 - i_17 * 2;
                  } else {
                     i_16 = class257_13.field3319;
                  }

                  int i_18 = 255;
                  boolean bool_19 = true;
                  int i_20 = client.field655 - class79_78.field999;
                  int i_21 = i_16 * class79_78.field1001 / class257_13.field3319;
                  int i_22;
                  int i_93;
                  if (class79_78.field1007 > i_20) {
                     i_22 = class257_13.field3307 == 0 ? 0 : class257_13.field3307 * (i_20 / class257_13.field3307);
                     i_23 = i_16 * class79_78.field1000 / class257_13.field3319;
                     i_93 = i_22 * (i_21 - i_23) / class79_78.field1007 + i_23;
                  } else {
                     i_93 = i_21;
                     i_22 = class79_78.field1007 + class257_13.field3316 - i_20;
                     if (class257_13.field3314 >= 0) {
                        i_18 = (i_22 << 8) / (class257_13.field3316 - class257_13.field3314);
                     }
                  }

                  if (class79_78.field1001 > 0 && i_93 < 1) {
                     i_93 = 1;
                  }

                  if (class335_14 != null && class335_15 != null) {
                     if (i_93 == i_16) {
                        i_93 += i_17 * 2;
                     } else {
                        i_93 += i_17;
                     }

                     i_22 = class335_14.field3911;
                     i_77 += i_22;
                     i_23 = i_2 + client.field747 - (i_16 >> 1);
                     i_24 = i_3 + client.field748 - i_77;
                     i_23 -= i_17;
                     if (i_18 >= 0 && i_18 < 255) {
                        class335_14.method6132(i_23, i_24, i_18);
                        class331.method5997(i_23, i_24, i_93 + i_23, i_24 + i_22);
                        class335_15.method6132(i_23, i_24, i_18);
                     } else {
                        class335_14.method6121(i_23, i_24);
                        class331.method5997(i_23, i_24, i_93 + i_23, i_22 + i_24);
                        class335_15.method6121(i_23, i_24);
                     }

                     class331.method5996(i_2, i_3, i_2 + i_4, i_3 + i_5);
                     i_77 += 2;
                  } else {
                     i_77 += 5;
                     if (client.field747 > -1) {
                        i_22 = i_2 + client.field747 - (i_16 >> 1);
                        i_23 = i_3 + client.field748 - i_77;
                        class331.method6004(i_22, i_23, i_93, 5, 65280);
                        class331.method6004(i_22 + i_93, i_23, i_16 - i_93, 5, 16711680);
                     }

                     i_77 += 2;
                  }
               }
            }
         }

         if (i_77 == -2) {
            i_77 += 7;
         }

         i_77 += b_9;
         if (i_1 < i_76) {
            class75 class75_89 = (class75) class78_0;
            if (class75_89.field625) {
               return;
            }

            if (class75_89.field608 != -1 || class75_89.field609 != -1) {
               class15.method144(class78_0, class78_0.field988 + 15, -489982967);
               if (client.field747 > -1) {
                  if (class75_89.field608 != -1) {
                     i_77 += 25;
                     class73.field604[class75_89.field608].method6121(i_2 + client.field747 - 12, i_3 + client.field748 - i_77);
                  }

                  if (class75_89.field609 != -1) {
                     i_77 += 25;
                     client.field710[class75_89.field609].method6121(i_2 + client.field747 - 12, i_3 + client.field748 - i_77);
                  }
               }
            }

            if (i_1 >= 0 && client.field740 == 10 && ints_8[i_1] == client.field665) {
               class15.method144(class78_0, class78_0.field988 + 15, -121975932);
               if (client.field747 > -1) {
                  i_77 += field318[1].field3911;
                  field318[1].method6121(i_2 + client.field747 - 12, i_3 + client.field748 - i_77);
               }
            }
         } else {
            class267 class267_90 = ((class88) class78_0).field1135;
            if (class267_90.field3524 != null) {
               class267_90 = class267_90.method4731((byte) -123);
            }

            if (class267_90.field3519 >= 0 && class267_90.field3519 < client.field710.length) {
               class15.method144(class78_0, class78_0.field988 + 15, -1541208303);
               if (client.field747 > -1) {
                  client.field710[class267_90.field3519].method6121(i_2 + client.field747 - 12, i_3 + client.field748 - 30);
               }
            }

            if (client.field740 == 1 && client.field691[i_1 - i_76] == client.field664 && client.field655 % 20 < 10) {
               class15.method144(class78_0, class78_0.field988 + 15, -1871925935);
               if (client.field747 > -1) {
                  field318[0].method6121(i_2 + client.field747 - 12, i_3 + client.field748 - 28);
               }
            }
         }

         if (class78_0.field951 != null && (i_1 >= i_76 || !class78_0.field953 && (client.field795 == 4 || !class78_0.field952 && (client.field795 == 0 || client.field795 == 3 || client.field795 == 1 && ((class75) class78_0).method1331(1301881513))))) {
            class15.method144(class78_0, class78_0.field988, -636186605);
            if (client.field747 > -1 && client.field735 < client.field736) {
               client.field726[client.field735] = class17.field74.method5340(class78_0.field951) / 2;
               client.field739[client.field735] = class17.field74.field3727;
               client.field783[client.field735] = client.field747;
               client.field738[client.field735] = client.field748;
               client.field741[client.field735] = class78_0.field955;
               client.field742[client.field735] = class78_0.field994;
               client.field743[client.field735] = class78_0.field991;
               client.field841[client.field735] = class78_0.field951;
               ++client.field735;
            }
         }

         for (int i_79 = 0; i_79 < 4; i_79++) {
            int i_91 = class78_0.field960[i_79];
            int i_80 = class78_0.field966[i_79];
            class263 class263_92 = null;
            int i_81 = 0;
            if (i_80 >= 0) {
               if (i_91 <= client.field655) {
                  continue;
               }

               class263_92 = class198.method3668(class78_0.field966[i_79], 1390109728);
               i_81 = class263_92.field3364;
               if (class263_92 != null && class263_92.field3375 != null) {
                  class263_92 = class263_92.method4548((byte) -127);
                  if (class263_92 == null) {
                     class78_0.field960[i_79] = -1;
                     continue;
                  }
               }
            } else if (i_91 < 0) {
               continue;
            }

            i_16 = class78_0.field961[i_79];
            class263 class263_82 = null;
            if (i_16 >= 0) {
               class263_82 = class198.method3668(i_16, 1390109728);
               if (class263_82 != null && class263_82.field3375 != null) {
                  class263_82 = class263_82.method4548((byte) -14);
               }
            }

            if (i_91 - i_81 <= client.field655) {
               if (class263_92 == null) {
                  class78_0.field960[i_79] = -1;
               } else {
                  class15.method144(class78_0, class78_0.field988 / 2, -1184200290);
                  if (client.field747 > -1) {
                     if (i_79 == 1) {
                        client.field748 -= 20;
                     }

                     if (i_79 == 2) {
                        client.field747 -= 15;
                        client.field748 -= 10;
                     }

                     if (i_79 == 3) {
                        client.field747 += 15;
                        client.field748 -= 10;
                     }

                     class335 class335_83 = null;
                     class335 class335_84 = null;
                     class335 class335_85 = null;
                     class335 class335_86 = null;
                     i_23 = 0;
                     i_24 = 0;
                     int i_25 = 0;
                     int i_26 = 0;
                     int i_27 = 0;
                     int i_28 = 0;
                     int i_29 = 0;
                     int i_30 = 0;
                     class335 class335_31 = null;
                     class335 class335_32 = null;
                     class335 class335_33 = null;
                     class335 class335_34 = null;
                     int i_35 = 0;
                     int i_36 = 0;
                     int i_37 = 0;
                     int i_38 = 0;
                     int i_39 = 0;
                     int i_40 = 0;
                     int i_41 = 0;
                     int i_42 = 0;
                     int i_43 = 0;
                     class335_83 = class263_92.method4550(-1476611889);
                     int i_44;
                     if (class335_83 != null) {
                        i_23 = class335_83.field3913;
                        i_44 = class335_83.field3911;
                        if (i_44 > i_43) {
                           i_43 = i_44;
                        }

                        i_27 = class335_83.field3918;
                     }

                     class335_84 = class263_92.method4545(809304727);
                     if (class335_84 != null) {
                        i_24 = class335_84.field3913;
                        i_44 = class335_84.field3911;
                        if (i_44 > i_43) {
                           i_43 = i_44;
                        }

                        i_28 = class335_84.field3918;
                     }

                     class335_85 = class263_92.method4554(644601071);
                     if (class335_85 != null) {
                        i_25 = class335_85.field3913;
                        i_44 = class335_85.field3911;
                        if (i_44 > i_43) {
                           i_43 = i_44;
                        }

                        i_29 = class335_85.field3918;
                     }

                     class335_86 = class263_92.method4552((short) 500);
                     if (class335_86 != null) {
                        i_26 = class335_86.field3913;
                        i_44 = class335_86.field3911;
                        if (i_44 > i_43) {
                           i_43 = i_44;
                        }

                        i_30 = class335_86.field3918;
                     }

                     if (class263_82 != null) {
                        class335_31 = class263_82.method4550(-1093350508);
                        if (class335_31 != null) {
                           i_35 = class335_31.field3913;
                           i_44 = class335_31.field3911;
                           if (i_44 > i_43) {
                              i_43 = i_44;
                           }

                           i_39 = class335_31.field3918;
                        }

                        class335_32 = class263_82.method4545(-532192594);
                        if (class335_32 != null) {
                           i_36 = class335_32.field3913;
                           i_44 = class335_32.field3911;
                           if (i_44 > i_43) {
                              i_43 = i_44;
                           }

                           i_40 = class335_32.field3918;
                        }

                        class335_33 = class263_82.method4554(644601071);
                        if (class335_33 != null) {
                           i_37 = class335_33.field3913;
                           i_44 = class335_33.field3911;
                           if (i_44 > i_43) {
                              i_43 = i_44;
                           }

                           i_41 = class335_33.field3918;
                        }

                        class335_34 = class263_82.method4552((short) 500);
                        if (class335_34 != null) {
                           i_38 = class335_34.field3913;
                           i_44 = class335_34.field3911;
                           if (i_44 > i_43) {
                              i_43 = i_44;
                           }

                           i_42 = class335_34.field3918;
                        }
                     }

                     class305 class305_87 = class263_92.method4553((byte) 0);
                     if (class305_87 == null) {
                        class305_87 = class297.field3684;
                     }

                     class305 class305_45;
                     if (class263_82 != null) {
                        class305_45 = class263_82.method4553((byte) 0);
                        if (class305_45 == null) {
                           class305_45 = class297.field3684;
                        }
                     } else {
                        class305_45 = class297.field3684;
                     }

                     String string_46 = null;
                     String string_47 = null;
                     boolean bool_48 = false;
                     int i_49 = 0;
                     string_46 = class263_92.method4549(class78_0.field992[i_79], -1302541631);
                     int i_94 = class305_87.method5340(string_46);
                     if (class263_82 != null) {
                        string_47 = class263_82.method4549(class78_0.field962[i_79], -1302541631);
                        i_49 = class305_45.method5340(string_47);
                     }

                     int i_50 = 0;
                     int i_51 = 0;
                     if (i_24 > 0) {
                        if (class335_85 == null && class335_86 == null) {
                           i_50 = 1;
                        } else {
                           i_50 = i_94 / i_24 + 1;
                        }
                     }

                     if (class263_82 != null && i_36 > 0) {
                        if (class335_33 == null && class335_34 == null) {
                           i_51 = 1;
                        } else {
                           i_51 = i_49 / i_36 + 1;
                        }
                     }

                     int i_52 = 0;
                     int i_53 = i_52;
                     if (i_23 > 0) {
                        i_52 += i_23;
                     }

                     i_52 += 2;
                     int i_54 = i_52;
                     if (i_25 > 0) {
                        i_52 += i_25;
                     }

                     int i_55 = i_52;
                     int i_56 = i_52;
                     int i_57;
                     if (i_24 > 0) {
                        i_57 = i_24 * i_50;
                        i_52 += i_57;
                        i_56 += (i_57 - i_94) / 2;
                     } else {
                        i_52 += i_94;
                     }

                     i_57 = i_52;
                     if (i_26 > 0) {
                        i_52 += i_26;
                     }

                     int i_58 = 0;
                     int i_59 = 0;
                     int i_60 = 0;
                     int i_61 = 0;
                     int i_62 = 0;
                     int i_63;
                     if (class263_82 != null) {
                        i_52 += 2;
                        i_58 = i_52;
                        if (i_35 > 0) {
                           i_52 += i_35;
                        }

                        i_52 += 2;
                        i_59 = i_52;
                        if (i_37 > 0) {
                           i_52 += i_37;
                        }

                        i_60 = i_52;
                        i_62 = i_52;
                        if (i_36 > 0) {
                           i_63 = i_51 * i_36;
                           i_52 += i_63;
                           i_62 += (i_63 - i_49) / 2;
                        } else {
                           i_52 += i_49;
                        }

                        i_61 = i_52;
                        if (i_38 > 0) {
                           i_52 += i_38;
                        }
                     }

                     i_63 = class78_0.field960[i_79] - client.field655;
                     int i_64 = class263_92.field3377 - i_63 * class263_92.field3377 / class263_92.field3364;
                     int i_65 = i_63 * class263_92.field3353 / class263_92.field3364 + -class263_92.field3353;
                     int i_66 = i_64 + (i_2 + client.field747 - (i_52 >> 1));
                     int i_67 = i_3 + client.field748 - 12 + i_65;
                     int i_68 = i_67;
                     int i_69 = i_43 + i_67;
                     int i_70 = i_67 + class263_92.field3374 + 15;
                     int i_71 = i_70 - class305_87.field3728;
                     int i_72 = i_70 + class305_87.field3729;
                     if (i_71 < i_67) {
                        i_68 = i_71;
                     }

                     if (i_72 > i_69) {
                        i_69 = i_72;
                     }

                     int i_73 = 0;
                     int i_74;
                     int i_75;
                     if (class263_82 != null) {
                        i_73 = i_67 + class263_82.field3374 + 15;
                        i_74 = i_73 - class305_45.field3728;
                        i_75 = i_73 + class305_45.field3729;
                        if (i_74 < i_68) {
                           ;
                        }

                        if (i_75 > i_69) {
                           ;
                        }
                     }

                     i_74 = 255;
                     if (class263_92.field3378 >= 0) {
                        i_74 = (i_63 << 8) / (class263_92.field3364 - class263_92.field3378);
                     }

                     if (i_74 >= 0 && i_74 < 255) {
                        if (class335_83 != null) {
                           class335_83.method6132(i_53 + i_66 - i_27, i_67, i_74);
                        }

                        if (class335_85 != null) {
                           class335_85.method6132(i_54 + i_66 - i_29, i_67, i_74);
                        }

                        if (class335_84 != null) {
                           for (i_75 = 0; i_75 < i_50; i_75++) {
                              class335_84.method6132(i_75 * i_24 + (i_55 + i_66 - i_28), i_67, i_74);
                           }
                        }

                        if (class335_86 != null) {
                           class335_86.method6132(i_66 + i_57 - i_30, i_67, i_74);
                        }

                        class305_87.method5400(string_46, i_66 + i_56, i_70, class263_92.field3363, 0, i_74);
                        if (class263_82 != null) {
                           if (class335_31 != null) {
                              class335_31.method6132(i_58 + i_66 - i_39, i_67, i_74);
                           }

                           if (class335_33 != null) {
                              class335_33.method6132(i_59 + i_66 - i_41, i_67, i_74);
                           }

                           if (class335_32 != null) {
                              for (i_75 = 0; i_75 < i_51; i_75++) {
                                 class335_32.method6132(i_36 * i_75 + (i_60 + i_66 - i_40), i_67, i_74);
                              }
                           }

                           if (class335_34 != null) {
                              class335_34.method6132(i_66 + i_61 - i_42, i_67, i_74);
                           }

                           class305_45.method5400(string_47, i_66 + i_62, i_73, class263_82.field3363, 0, i_74);
                        }
                     } else {
                        if (class335_83 != null) {
                           class335_83.method6121(i_53 + i_66 - i_27, i_67);
                        }

                        if (class335_85 != null) {
                           class335_85.method6121(i_54 + i_66 - i_29, i_67);
                        }

                        if (class335_84 != null) {
                           for (i_75 = 0; i_75 < i_50; i_75++) {
                              class335_84.method6121(i_75 * i_24 + (i_66 + i_55 - i_28), i_67);
                           }
                        }

                        if (class335_86 != null) {
                           class335_86.method6121(i_57 + i_66 - i_30, i_67);
                        }

                        class305_87.method5335(string_46, i_66 + i_56, i_70, class263_92.field3363 | ~0xffffff, 0);
                        if (class263_82 != null) {
                           if (class335_31 != null) {
                              class335_31.method6121(i_66 + i_58 - i_39, i_67);
                           }

                           if (class335_33 != null) {
                              class335_33.method6121(i_66 + i_59 - i_41, i_67);
                           }

                           if (class335_32 != null) {
                              for (i_75 = 0; i_75 < i_51; i_75++) {
                                 class335_32.method6121(i_36 * i_75 + (i_60 + i_66 - i_40), i_67);
                              }
                           }

                           if (class335_34 != null) {
                              class335_34.method6121(i_66 + i_61 - i_42, i_67);
                           }

                           class305_45.method5335(string_47, i_66 + i_62, i_73, class263_82.field3363 | ~0xffffff, 0);
                        }
                     }
                  }
               }
            }
         }

      }
   }

   public static class261 method660(int i_0, byte b_1) {
      class261 class261_2 = (class261) class261.field3336.method3376((long)i_0);
      if (class261_2 != null) {
         return class261_2;
      } else {
         byte[] bytes_3 = class261.field3335.method4160(34, i_0, (short) -9962);
         class261_2 = new class261();
         if (bytes_3 != null) {
            class261_2.method4514(new class310(bytes_3), -2120336035);
         }

         class261_2.method4513(-839418144);
         class261.field3336.method3374(class261_2, (long)i_0);
         return class261_2;
      }
   }

}
