public final class class88 extends class78 {

   static class244 field1136;
   static class226 field1131;
   class267 field1135;

   protected final class136 vmethod3305(int i_1) {
      if (this.field1135 == null) {
         return null;
      } else {
         class269 class269_2 = super.field970 != -1 && super.field937 == 0 ? class260.method4510(super.field970, -2135697435) : null;
         class269 class269_3 = super.field967 != -1 && (super.field943 != super.field967 || class269_2 == null) ? class260.method4510(super.field967, -1512426820) : null;
         class136 class136_4 = this.field1135.method4704(class269_2, super.field971, class269_3, super.field964, (byte) 107);
         if (class136_4 == null) {
            return null;
         } else {
            class136_4.method2946();
            super.field988 = class136_4.field1894;
            if (super.field975 != -1 && super.field976 != -1) {
               class136 class136_5 = class96.method2160(super.field975, (byte) 125).method4411(super.field976, -634517050);
               if (class136_5 != null) {
                  class136_5.method2923(0, -super.field979, 0);
                  class136[] arr_6 = new class136[] {class136_4, class136_5};
                  class136_4 = new class136(arr_6, 2);
               }
            }

            if (this.field1135.field3500 == 1) {
               class136_4.field1710 = true;
            }

            return class136_4;
         }
      }
   }

   final void method2074(int i_1, byte b_2, int i_3) {
      int i_4 = super.field993[0];
      int i_5 = super.field968[0];
      if (i_1 == 0) {
         --i_4;
         ++i_5;
      }

      if (i_1 == 1) {
         ++i_5;
      }

      if (i_1 == 2) {
         ++i_4;
         ++i_5;
      }

      if (i_1 == 3) {
         --i_4;
      }

      if (i_1 == 4) {
         ++i_4;
      }

      if (i_1 == 5) {
         --i_4;
         --i_5;
      }

      if (i_1 == 6) {
         --i_5;
      }

      if (i_1 == 7) {
         ++i_4;
         --i_5;
      }

      if (super.field970 != -1 && class260.method4510(super.field970, -1433400504).field3555 == 1) {
         super.field970 = -1;
      }

      if (super.field985 < 9) {
         ++super.field985;
      }

      for (int i_6 = super.field985; i_6 > 0; --i_6) {
         super.field993[i_6] = super.field993[i_6 - 1];
         super.field968[i_6] = super.field968[i_6 - 1];
         super.field995[i_6] = super.field995[i_6 - 1];
      }

      super.field993[0] = i_4;
      super.field968[0] = i_5;
      super.field995[0] = b_2;
   }

   final boolean vmethod2080(int i_1) {
      return this.field1135 != null;
   }

   final void method2078(int i_1, int i_2, boolean bool_3, int i_4) {
      if (super.field970 != -1 && class260.method4510(super.field970, -2093388817).field3555 == 1) {
         super.field970 = -1;
      }

      if (!bool_3) {
         int i_5 = i_1 - super.field993[0];
         int i_6 = i_2 - super.field968[0];
         if (i_5 >= -8 && i_5 <= 8 && i_6 >= -8 && i_6 <= 8) {
            if (super.field985 < 9) {
               ++super.field985;
            }

            for (int i_7 = super.field985; i_7 > 0; --i_7) {
               super.field993[i_7] = super.field993[i_7 - 1];
               super.field968[i_7] = super.field968[i_7 - 1];
               super.field995[i_7] = super.field995[i_7 - 1];
            }

            super.field993[0] = i_1;
            super.field968[0] = i_2;
            super.field995[0] = 1;
            return;
         }
      }

      super.field985 = 0;
      super.field996 = 0;
      super.field963 = 0;
      super.field993[0] = i_1;
      super.field968[0] = i_2;
      super.field990 = super.field941 * 64 + super.field993[0] * 128;
      super.field938 = super.field941 * 64 + super.field968[0] * 128;
   }

   public static long method2085(int i_0, int i_1, int i_2, boolean bool_3, int i_4, byte b_5) {
      long long_6 = (long)((i_0 & 0x7f) << 0 | (i_1 & 0x7f) << 7 | (i_2 & 0x3) << 14) | ((long)i_4 & 0xffffffffL) << 17;
      if (bool_3) {
         long_6 |= 0x10000L;
      }

      return long_6;
   }

}
