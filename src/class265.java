public class class265 extends class184 {

   static boolean field3466;
   static class305 field3446;
   public int field3480;
   static class154 field3437 = new class154(64);
   static class154 field3450 = new class154(50);
   public static class154 field3440 = new class154(200);
   public String field3444 = "null";
   public int field3449 = 2000;
   public int field3457 = 0;
   public int field3451 = 0;
   public int field3452 = 0;
   public int field3453 = 0;
   public int field3472 = 0;
   public int field3455 = 0;
   public int field3477 = 1;
   public boolean field3441 = false;
   public String[] field3458 = new String[] {null, null, "Take", null, null};
   public String[] field3459 = new String[] {null, null, null, null, "Drop"};
   int field3447 = -2;
   int field3438 = -1;
   int field3462 = -1;
   int field3463 = 0;
   int field3464 = -1;
   int field3465 = -1;
   int field3454 = 0;
   int field3467 = -1;
   int field3468 = -1;
   int field3469 = -1;
   int field3470 = -1;
   int field3471 = -1;
   int field3476 = -1;
   public int field3475 = -1;
   public int field3432 = -964463269;
   int field3478 = 128;
   int field3461 = 128;
   int field3479 = 128;
   public int field3481 = 0;
   public int field3474 = 0;
   public int field3482 = 0;
   public boolean field3484 = false;
   int field3485 = -1;
   int field3486 = -1;
   public int field3487 = -1;
   public int field3488 = -1;
   int field3443;
   short[] field3460;
   short[] field3473;
   short[] field3439;
   short[] field3448;
   int[] field3442;
   int[] field3445;
   class326 field3483;

   void method4628(class310 class310_1, int i_2, int i_3) {
      if (i_2 == 1) {
         this.field3443 = class310_1.method5729(-237295090);
      } else if (i_2 == 2) {
         this.field3444 = class310_1.method5589(-828164366);
      } else if (i_2 == 4) {
         this.field3449 = class310_1.method5729(74421251);
      } else if (i_2 == 5) {
         this.field3457 = class310_1.method5729(480228800);
      } else if (i_2 == 6) {
         this.field3451 = class310_1.method5729(1410554769);
      } else if (i_2 == 7) {
         this.field3453 = class310_1.method5729(-964422795);
         if (this.field3453 > 32767) {
            this.field3453 -= 65536;
         }
      } else if (i_2 == 8) {
         this.field3472 = class310_1.method5729(-2016756420);
         if (this.field3472 > 32767) {
            this.field3472 -= 65536;
         }
      } else if (i_2 == 11) {
         this.field3455 = 1;
      } else if (i_2 == 12) {
         this.field3477 = class310_1.method5507(-569619024);
      } else if (i_2 == 16) {
         this.field3441 = true;
      } else if (i_2 == 23) {
         this.field3438 = class310_1.method5729(931212127);
         this.field3463 = class310_1.method5661((byte) -24);
      } else if (i_2 == 24) {
         this.field3462 = class310_1.method5729(-1937441023);
      } else if (i_2 == 25) {
         this.field3464 = class310_1.method5729(1638693605);
         this.field3454 = class310_1.method5661((byte) 24);
      } else if (i_2 == 26) {
         this.field3465 = class310_1.method5729(-1726053905);
      } else if (i_2 >= 30 && i_2 < 35) {
         this.field3458[i_2 - 30] = class310_1.method5589(823888587);
         if (this.field3458[i_2 - 30].equalsIgnoreCase("Hidden")) {
            this.field3458[i_2 - 30] = null;
         }
      } else if (i_2 >= 35 && i_2 < 40) {
         this.field3459[i_2 - 35] = class310_1.method5589(-623721237);
      } else {
         int i_4;
         int i_5;
         if (i_2 == 40) {
            i_4 = class310_1.method5661((byte) 29);
            this.field3460 = new short[i_4];
            this.field3473 = new short[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3460[i_5] = (short)class310_1.method5729(-1983211858);
               this.field3473[i_5] = (short)class310_1.method5729(294435525);
            }
         } else if (i_2 == 41) {
            i_4 = class310_1.method5661((byte) 74);
            this.field3439 = new short[i_4];
            this.field3448 = new short[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3439[i_5] = (short)class310_1.method5729(-1576157778);
               this.field3448[i_5] = (short)class310_1.method5729(-1765334150);
            }
         } else if (i_2 == 42) {
            this.field3447 = class310_1.method5535((byte) 0);
         } else if (i_2 == 65) {
            this.field3484 = true;
         } else if (i_2 == 78) {
            this.field3467 = class310_1.method5729(-1072657089);
         } else if (i_2 == 79) {
            this.field3468 = class310_1.method5729(-73157175);
         } else if (i_2 == 90) {
            this.field3469 = class310_1.method5729(1577969193);
         } else if (i_2 == 91) {
            this.field3471 = class310_1.method5729(1505132237);
         } else if (i_2 == 92) {
            this.field3470 = class310_1.method5729(1012404920);
         } else if (i_2 == 93) {
            this.field3476 = class310_1.method5729(-1196631163);
         } else if (i_2 == 95) {
            this.field3452 = class310_1.method5729(-1943913019);
         } else if (i_2 == 97) {
            this.field3475 = class310_1.method5729(1603661418);
         } else if (i_2 == 98) {
            this.field3432 = class310_1.method5729(1892698829) * 964463269;
         } else if (i_2 >= 100 && i_2 < 110) {
            if (this.field3442 == null) {
               this.field3442 = new int[10];
               this.field3445 = new int[10];
            }

            this.field3442[i_2 - 100] = class310_1.method5729(1125309951);
            this.field3445[i_2 - 100] = class310_1.method5729(-902684930);
         } else if (i_2 == 110) {
            this.field3478 = class310_1.method5729(-293442463);
         } else if (i_2 == 111) {
            this.field3461 = class310_1.method5729(-1588565180);
         } else if (i_2 == 112) {
            this.field3479 = class310_1.method5729(702838740);
         } else if (i_2 == 113) {
            this.field3481 = class310_1.method5535((byte) 0);
         } else if (i_2 == 114) {
            this.field3474 = class310_1.method5535((byte) 0) * 5;
         } else if (i_2 == 115) {
            this.field3482 = class310_1.method5661((byte) 34);
         } else if (i_2 == 139) {
            this.field3485 = class310_1.method5729(-1950218402);
         } else if (i_2 == 140) {
            this.field3486 = class310_1.method5729(1936389203);
         } else if (i_2 == 148) {
            this.field3487 = class310_1.method5729(132247022);
         } else if (i_2 == 149) {
            this.field3488 = class310_1.method5729(-1284051416);
         } else if (i_2 == 249) {
            this.field3483 = class180.method3576(class310_1, this.field3483, 1338394648);
         }
      }

   }

   public final class136 method4633(int i_1, int i_2) {
      if (this.field3442 != null && i_1 > 1) {
         int i_3 = -1;

         for (int i_4 = 0; i_4 < 10; i_4++) {
            if (i_1 >= this.field3445[i_4] && this.field3445[i_4] != 0) {
               i_3 = this.field3442[i_4];
            }
         }

         if (i_3 != -1) {
            return class66.method1223(i_3, -118732017).method4633(1, 298550072);
         }
      }

      class136 class136_6 = (class136) field3450.method3376((long)this.field3480);
      if (class136_6 != null) {
         return class136_6;
      } else {
         class130 class130_7 = class130.method2824(class138.field1778, this.field3443, 0);
         if (class130_7 == null) {
            return null;
         } else {
            if (this.field3478 != 128 || this.field3461 != 128 || this.field3479 != 128) {
               class130_7.method2840(this.field3478, this.field3461, this.field3479);
            }

            int i_5;
            if (this.field3460 != null) {
               for (i_5 = 0; i_5 < this.field3460.length; i_5++) {
                  class130_7.method2837(this.field3460[i_5], this.field3473[i_5]);
               }
            }

            if (this.field3439 != null) {
               for (i_5 = 0; i_5 < this.field3439.length; i_5++) {
                  class130_7.method2833(this.field3439[i_5], this.field3448[i_5]);
               }
            }

            class136_6 = class130_7.method2876(this.field3481 + 64, this.field3474 + 768, -50, -10, -50);
            class136_6.field1710 = true;
            field3450.method3374(class136_6, (long)this.field3480);
            return class136_6;
         }
      }
   }

   public final class130 method4678(int i_1, int i_2) {
      int i_4;
      if (this.field3442 != null && i_1 > 1) {
         int i_3 = -1;

         for (i_4 = 0; i_4 < 10; i_4++) {
            if (i_1 >= this.field3445[i_4] && this.field3445[i_4] != 0) {
               i_3 = this.field3442[i_4];
            }
         }

         if (i_3 != -1) {
            return class66.method1223(i_3, -592666817).method4678(1, 870819489);
         }
      }

      class130 class130_5 = class130.method2824(class138.field1778, this.field3443, 0);
      if (class130_5 == null) {
         return null;
      } else {
         if (this.field3478 != 128 || this.field3461 != 128 || this.field3479 != 128) {
            class130_5.method2840(this.field3478, this.field3461, this.field3479);
         }

         if (this.field3460 != null) {
            for (i_4 = 0; i_4 < this.field3460.length; i_4++) {
               class130_5.method2837(this.field3460[i_4], this.field3473[i_4]);
            }
         }

         if (this.field3439 != null) {
            for (i_4 = 0; i_4 < this.field3439.length; i_4++) {
               class130_5.method2833(this.field3439[i_4], this.field3448[i_4]);
            }
         }

         return class130_5;
      }
   }

   void method4626(int i_1) {
   }

   public final boolean method4627(boolean bool_1, byte b_2) {
      int i_3 = this.field3469;
      int i_4 = this.field3470;
      if (bool_1) {
         i_3 = this.field3471;
         i_4 = this.field3476;
      }

      if (i_3 == -1) {
         return true;
      } else {
         boolean bool_5 = true;
         if (!class138.field1778.method4162(i_3, 0, -1537343571)) {
            bool_5 = false;
         }

         if (i_4 != -1 && !class138.field1778.method4162(i_4, 0, 31392433)) {
            bool_5 = false;
         }

         return bool_5;
      }
   }

   void method4674(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -44);
         if (i_3 == 0) {
            return;
         }

         this.method4628(class310_1, i_3, 1800832423);
      }
   }

   public final class130 method4638(boolean bool_1, int i_2) {
      int i_3 = this.field3469;
      int i_4 = this.field3470;
      if (bool_1) {
         i_3 = this.field3471;
         i_4 = this.field3476;
      }

      if (i_3 == -1) {
         return null;
      } else {
         class130 class130_5 = class130.method2824(class138.field1778, i_3, 0);
         if (i_4 != -1) {
            class130 class130_6 = class130.method2824(class138.field1778, i_4, 0);
            class130[] arr_7 = new class130[] {class130_5, class130_6};
            class130_5 = new class130(arr_7, 2);
         }

         int i_8;
         if (this.field3460 != null) {
            for (i_8 = 0; i_8 < this.field3460.length; i_8++) {
               class130_5.method2837(this.field3460[i_8], this.field3473[i_8]);
            }
         }

         if (this.field3439 != null) {
            for (i_8 = 0; i_8 < this.field3439.length; i_8++) {
               class130_5.method2833(this.field3439[i_8], this.field3448[i_8]);
            }
         }

         return class130_5;
      }
   }

   public final boolean method4635(boolean bool_1, short s_2) {
      int i_3 = this.field3438;
      int i_4 = this.field3462;
      int i_5 = this.field3467;
      if (bool_1) {
         i_3 = this.field3464;
         i_4 = this.field3465;
         i_5 = this.field3468;
      }

      if (i_3 == -1) {
         return true;
      } else {
         boolean bool_6 = true;
         if (!class138.field1778.method4162(i_3, 0, 1994925765)) {
            bool_6 = false;
         }

         if (i_4 != -1 && !class138.field1778.method4162(i_4, 0, 1342739417)) {
            bool_6 = false;
         }

         if (i_5 != -1 && !class138.field1778.method4162(i_5, 0, 1330794039)) {
            bool_6 = false;
         }

         return bool_6;
      }
   }

   public final class130 method4636(boolean bool_1, int i_2) {
      int i_3 = this.field3438;
      int i_4 = this.field3462;
      int i_5 = this.field3467;
      if (bool_1) {
         i_3 = this.field3464;
         i_4 = this.field3465;
         i_5 = this.field3468;
      }

      if (i_3 == -1) {
         return null;
      } else {
         class130 class130_6 = class130.method2824(class138.field1778, i_3, 0);
         if (i_4 != -1) {
            class130 class130_7 = class130.method2824(class138.field1778, i_4, 0);
            if (i_5 != -1) {
               class130 class130_8 = class130.method2824(class138.field1778, i_5, 0);
               class130[] arr_9 = new class130[] {class130_6, class130_7, class130_8};
               class130_6 = new class130(arr_9, 3);
            } else {
               class130[] arr_11 = new class130[] {class130_6, class130_7};
               class130_6 = new class130(arr_11, 2);
            }
         }

         if (!bool_1 && this.field3463 != 0) {
            class130_6.method2836(0, this.field3463, 0);
         }

         if (bool_1 && this.field3454 != 0) {
            class130_6.method2836(0, this.field3454, 0);
         }

         int i_10;
         if (this.field3460 != null) {
            for (i_10 = 0; i_10 < this.field3460.length; i_10++) {
               class130_6.method2837(this.field3460[i_10], this.field3473[i_10]);
            }
         }

         if (this.field3439 != null) {
            for (i_10 = 0; i_10 < this.field3439.length; i_10++) {
               class130_6.method2833(this.field3439[i_10], this.field3448[i_10]);
            }
         }

         return class130_6;
      }
   }

   void method4629(class265 class265_1, class265 class265_2, int i_3) {
      this.field3443 = class265_1.field3443;
      this.field3449 = class265_1.field3449;
      this.field3457 = class265_1.field3457;
      this.field3451 = class265_1.field3451;
      this.field3452 = class265_1.field3452;
      this.field3453 = class265_1.field3453;
      this.field3472 = class265_1.field3472;
      this.field3460 = class265_1.field3460;
      this.field3473 = class265_1.field3473;
      this.field3439 = class265_1.field3439;
      this.field3448 = class265_1.field3448;
      this.field3444 = class265_2.field3444;
      this.field3441 = class265_2.field3441;
      this.field3477 = class265_2.field3477;
      this.field3455 = 1;
   }

   void method4630(class265 class265_1, class265 class265_2, int i_3) {
      this.field3443 = class265_1.field3443;
      this.field3449 = class265_1.field3449;
      this.field3457 = class265_1.field3457;
      this.field3451 = class265_1.field3451;
      this.field3452 = class265_1.field3452;
      this.field3453 = class265_1.field3453;
      this.field3472 = class265_1.field3472;
      this.field3460 = class265_2.field3460;
      this.field3473 = class265_2.field3473;
      this.field3439 = class265_2.field3439;
      this.field3448 = class265_2.field3448;
      this.field3444 = class265_2.field3444;
      this.field3441 = class265_2.field3441;
      this.field3455 = class265_2.field3455;
      this.field3438 = class265_2.field3438;
      this.field3462 = class265_2.field3462;
      this.field3467 = class265_2.field3467;
      this.field3464 = class265_2.field3464;
      this.field3465 = class265_2.field3465;
      this.field3468 = class265_2.field3468;
      this.field3469 = class265_2.field3469;
      this.field3470 = class265_2.field3470;
      this.field3471 = class265_2.field3471;
      this.field3476 = class265_2.field3476;
      this.field3482 = class265_2.field3482;
      this.field3458 = class265_2.field3458;
      this.field3459 = new String[5];
      if (class265_2.field3459 != null) {
         for (int i_4 = 0; i_4 < 4; i_4++) {
            this.field3459[i_4] = class265_2.field3459[i_4];
         }
      }

      this.field3459[4] = "Discard";
      this.field3477 = 0;
   }

   void method4631(class265 class265_1, class265 class265_2, int i_3) {
      this.field3443 = class265_1.field3443;
      this.field3449 = class265_1.field3449;
      this.field3457 = class265_1.field3457;
      this.field3451 = class265_1.field3451;
      this.field3452 = class265_1.field3452;
      this.field3453 = class265_1.field3453;
      this.field3472 = class265_1.field3472;
      this.field3460 = class265_1.field3460;
      this.field3473 = class265_1.field3473;
      this.field3439 = class265_1.field3439;
      this.field3448 = class265_1.field3448;
      this.field3455 = class265_1.field3455;
      this.field3444 = class265_2.field3444;
      this.field3477 = 0;
      this.field3441 = false;
      this.field3484 = false;
   }

   public class265 method4634(int i_1, int i_2) {
      if (this.field3442 != null && i_1 > 1) {
         int i_3 = -1;

         for (int i_4 = 0; i_4 < 10; i_4++) {
            if (i_1 >= this.field3445[i_4] && this.field3445[i_4] != 0) {
               i_3 = this.field3442[i_4];
            }
         }

         if (i_3 != -1) {
            return class66.method1223(i_3, -1545491046);
         }
      }

      return this;
   }

   public int method4639(int i_1, int i_2, byte b_3) {
      class326 class326_5 = this.field3483;
      int i_4;
      if (class326_5 == null) {
         i_4 = i_2;
      } else {
         class188 class188_6 = (class188) class326_5.method5919((long)i_1);
         if (class188_6 == null) {
            i_4 = i_2;
         } else {
            i_4 = class188_6.field2135;
         }
      }

      return i_4;
   }

   public String method4664(int i_1, String string_2, byte b_3) {
      class326 class326_5 = this.field3483;
      String string_4;
      if (class326_5 == null) {
         string_4 = string_2;
      } else {
         class185 class185_6 = (class185) class326_5.method5919((long)i_1);
         if (class185_6 == null) {
            string_4 = string_2;
         } else {
            string_4 = (String) class185_6.field2131;
         }
      }

      return string_4;
   }

   public int method4690(short s_1) {
      return this.field3447 != -1 && this.field3459 != null ? (this.field3447 >= 0 ? (this.field3459[this.field3447] != null ? this.field3447 : -1) : ("Drop".equalsIgnoreCase(this.field3459[4]) ? 4 : -1)) : -1;
   }

}
