public class class292 extends class287 {

   int field3657 = 1;
   public class273 field3656 = new class273();
   final class348 field3658;

   public class292(class348 class348_1) {
      super(400);
      this.field3658 = class348_1;
   }

   public void method5171(class310 class310_1, int i_2, int i_3) {
      while (true) {
         if (class310_1.field3751 < i_2) {
            boolean bool_4 = class310_1.method5661((byte) -76) == 1;
            class293 class293_5 = new class293(class310_1.method5589(1179279092), this.field3658);
            class293 class293_6 = new class293(class310_1.method5589(1535363567), this.field3658);
            int i_7 = class310_1.method5729(1981531296);
            int i_8 = class310_1.method5661((byte) 27);
            int i_9 = class310_1.method5661((byte) -74);
            boolean bool_10 = (i_9 & 0x2) != 0;
            boolean bool_11 = (i_9 & 0x1) != 0;
            if (i_7 > 0) {
               class310_1.method5589(-1906808256);
               class310_1.method5661((byte) 16);
               class310_1.method5507(1015370780);
            }

            class310_1.method5589(-1547020700);
            if (class293_5 != null && class293_5.method5187((byte) -114)) {
               class294 class294_12 = (class294) this.method5124(class293_5, -1651031508);
               if (bool_4) {
                  class294 class294_13 = (class294) this.method5124(class293_6, -1575582607);
                  if (class294_13 != null && class294_13 != class294_12) {
                     if (class294_12 != null) {
                        this.method5078(class294_13, -441929933);
                     } else {
                        class294_12 = class294_13;
                     }
                  }
               }

               if (class294_12 != null) {
                  this.method5083(class294_12, class293_5, class293_6, 661941579);
                  if (i_7 != class294_12.field3650) {
                     boolean bool_15 = true;

                     for (class296 class296_14 = (class296) this.field3656.method4909(); class296_14 != null; class296_14 = (class296) this.field3656.method4912()) {
                        if (class296_14.field3678.equals(class293_5)) {
                           if (i_7 != 0 && class296_14.field3679 == 0) {
                              class296_14.method3626();
                              bool_15 = false;
                           } else if (i_7 == 0 && class296_14.field3679 != 0) {
                              class296_14.method3626();
                              bool_15 = false;
                           }
                        }
                     }

                     if (bool_15) {
                        this.field3656.method4910(new class296(class293_5, i_7));
                     }
                  }
               } else {
                  if (this.method5071(1474495463) >= 400) {
                     continue;
                  }

                  class294_12 = (class294) this.method5072(class293_5, class293_6, 1547669313);
               }

               if (i_7 != class294_12.field3650) {
                  class294_12.field3649 = ++this.field3657 - 1;
                  if (class294_12.field3650 == -1 && i_7 == 0) {
                     class294_12.field3649 = -(class294_12.field3649 * -579950033) * 503110863;
                  }

                  class294_12.field3650 = i_7;
               }

               class294_12.field3651 = i_8;
               class294_12.field3666 = bool_10;
               class294_12.field3667 = bool_11;
               continue;
            }

            throw new IllegalStateException();
         }

         this.method5082(2029910396);
         return;
      }
   }

   class284 vmethod5247(byte b_1) {
      return new class294();
   }

   public boolean method5170(class293 class293_1, boolean bool_2, byte b_3) {
      class294 class294_4 = (class294) this.method5074(class293_1, 709076888);
      return class294_4 == null ? false : !bool_2 || class294_4.field3650 != 0;
   }

   class284[] vmethod5226(int i_1, int i_2) {
      return new class294[i_1];
   }

   static void method5184(int i_0, int i_1, int i_2, int i_3, int i_4) {
      for (class84 class84_5 = (class84) class84.field1075.method4879(); class84_5 != null; class84_5 = (class84) class84.field1075.method4884()) {
         if (class84_5.field1080 != -1 || class84_5.field1084 != null) {
            int i_6 = 0;
            if (i_1 > class84_5.field1073) {
               i_6 += i_1 - class84_5.field1073;
            } else if (i_1 < class84_5.field1079) {
               i_6 += class84_5.field1079 - i_1;
            }

            if (i_2 > class84_5.field1078) {
               i_6 += i_2 - class84_5.field1078;
            } else if (i_2 < class84_5.field1076) {
               i_6 += class84_5.field1076 - i_2;
            }

            if (i_6 - 64 <= class84_5.field1086 && class282.field3617.field1060 != 0 && i_0 == class84_5.field1074) {
               i_6 -= 64;
               if (i_6 < 0) {
                  i_6 = 0;
               }

               int i_7 = (class84_5.field1086 - i_6) * class282.field3617.field1060 / class84_5.field1086;
               if (class84_5.field1088 == null) {
                  if (class84_5.field1080 >= 0) {
                     class110 class110_8 = class110.method2385(class227.field2730, class84_5.field1080, 0);
                     if (class110_8 != null) {
                        class112 class112_9 = class110_8.method2387().method2428(class129.field1557);
                        class122 class122_10 = class122.method2578(class112_9, 100, i_7);
                        class122_10.method2581(-1);
                        class45.field350.method2324(class122_10);
                        class84_5.field1088 = class122_10;
                     }
                  }
               } else {
                  class84_5.field1088.method2582(i_7);
               }

               if (class84_5.field1082 == null) {
                  if (class84_5.field1084 != null && (class84_5.field1081 -= i_3) <= 0) {
                     int i_12 = (int)(Math.random() * (double)class84_5.field1084.length);
                     class110 class110_13 = class110.method2385(class227.field2730, class84_5.field1084[i_12], 0);
                     if (class110_13 != null) {
                        class112 class112_14 = class110_13.method2387().method2428(class129.field1557);
                        class122 class122_11 = class122.method2578(class112_14, 100, i_7);
                        class122_11.method2581(0);
                        class45.field350.method2324(class122_11);
                        class84_5.field1082 = class122_11;
                        class84_5.field1081 = class84_5.field1085 + (int)(Math.random() * (double)(class84_5.field1083 - class84_5.field1085));
                     }
                  }
               } else {
                  class84_5.field1082.method2582(i_7);
                  if (!class84_5.field1082.method3629()) {
                     class84_5.field1082 = null;
                  }
               }
            } else {
               if (class84_5.field1088 != null) {
                  class45.field350.method2325(class84_5.field1088);
                  class84_5.field1088 = null;
               }

               if (class84_5.field1082 != null) {
                  class45.field350.method2325(class84_5.field1082);
                  class84_5.field1082 = null;
               }
            }
         }
      }

   }

}
