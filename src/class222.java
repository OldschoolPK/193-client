public class class222 {

   public int field2549;
   public int field2546;
   public int field2547;

   public class222(class222 class222_1) {
      this.field2549 = class222_1.field2549;
      this.field2546 = class222_1.field2546;
      this.field2547 = class222_1.field2547;
   }

   public class222(int i_1, int i_2, int i_3) {
      this.field2549 = i_1;
      this.field2546 = i_2;
      this.field2547 = i_3;
   }

   public class222(int i_1) {
      if (i_1 == -1) {
         this.field2549 = -1;
      } else {
         this.field2549 = i_1 >> 28 & 0x3;
         this.field2546 = i_1 >> 14 & 0x3fff;
         this.field2547 = i_1 & 0x3fff;
      }

   }

   String method4006(String string_1, int i_2) {
      return this.field2549 + string_1 + (this.field2546 >> 6) + string_1 + (this.field2547 >> 6) + string_1 + (this.field2546 & 0x3f) + string_1 + (this.field2547 & 0x3f);
   }

   public int method3994(byte b_1) {
      return this.field2549 << 28 | this.field2546 << 14 | this.field2547;
   }

   boolean method3995(class222 class222_1, int i_2) {
      return this.field2549 != class222_1.field2549 ? false : (this.field2546 != class222_1.field2546 ? false : this.field2547 == class222_1.field2547);
   }

   public boolean equals(Object object_1) {
      return this == object_1 ? true : (!(object_1 instanceof class222) ? false : this.method3995((class222) object_1, 902297370));
   }

   public int hashCode() {
      return this.method3994((byte) -27);
   }

   public String toString() {
      return this.method4006(",", -1856185425);
   }

   public String abd() {
      return this.method4006(",", -1856185425);
   }

   public String aba() {
      return this.method4006(",", -1856185425);
   }

   public static int method3999(int i_0, int i_1) {
      class259 class259_3 = (class259) class259.field3324.method3376((long)i_0);
      class259 class259_2;
      if (class259_3 != null) {
         class259_2 = class259_3;
      } else {
         byte[] bytes_8 = class259.field3323.method4160(14, i_0, (short) -14209);
         class259_3 = new class259();
         if (bytes_8 != null) {
            class259_3.method4484(new class310(bytes_8), 1196473375);
         }

         class259.field3324.method3374(class259_3, (long)i_0);
         class259_2 = class259_3;
      }

      int i_4 = class259_2.field3326;
      int i_5 = class259_2.field3328;
      int i_6 = class259_2.field3327;
      int i_7 = class221.field2540[i_6 - i_5];
      return class221.field2541[i_4] >> i_5 & i_7;
   }

}
