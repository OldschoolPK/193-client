import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import javax.imageio.ImageIO;

public class class58 {

   public static class244 field427;
   static class341 field434;
   static int field433;

   static {
      ImageIO.setUseCache(false);
   }

   public static File method917(String string_0, String string_1, int i_2, int i_3) {
      String str_4 = i_2 == 0 ? "" : "" + i_2;
      class176.field2061 = new File(class129.field1559, "jagex_cl_" + string_0 + "_" + string_1 + str_4 + ".dat");
      String string_5 = null;
      String string_6 = null;
      boolean bool_7 = false;
      class310 class310_9;
      int i_12;
      File file_28;
      if (class176.field2061.exists()) {
         try {
            class353 class353_8 = new class353(class176.field2061, "rw", 10000L);

            int i_10;
            for (class310_9 = new class310((int)class353_8.method6568(1212887106)); class310_9.field3751 < class310_9.field3752.length; class310_9.field3751 += i_10) {
               i_10 = class353_8.method6567(class310_9.field3752, class310_9.field3751, class310_9.field3752.length - class310_9.field3751, 1817916794);
               if (i_10 == -1) {
                  throw new IOException();
               }
            }

            class310_9.field3751 = 0;
            i_10 = class310_9.method5661((byte) -35);
            if (i_10 < 1 || i_10 > 3) {
               throw new IOException("" + i_10);
            }

            int i_11 = 0;
            if (i_10 > 1) {
               i_11 = class310_9.method5661((byte) 0);
            }

            if (i_10 <= 2) {
               string_5 = class310_9.method5734(-519852136);
               if (i_11 == 1) {
                  string_6 = class310_9.method5734(-519852136);
               }
            } else {
               string_5 = class310_9.method5606((byte) -33);
               if (i_11 == 1) {
                  string_6 = class310_9.method5606((byte) -3);
               }
            }

            class353_8.method6569((byte) 0);
         } catch (IOException ioexception_26) {
            ioexception_26.printStackTrace();
         }

         if (string_5 != null) {
            file_28 = new File(string_5);
            if (!file_28.exists()) {
               string_5 = null;
            }
         }

         if (string_5 != null) {
            file_28 = new File(string_5, "test.dat");

            boolean bool_29;
            try {
               RandomAccessFile randomaccessfile_16 = new RandomAccessFile(file_28, "rw");
               i_12 = randomaccessfile_16.read();
               randomaccessfile_16.seek(0L);
               randomaccessfile_16.write(i_12);
               randomaccessfile_16.seek(0L);
               randomaccessfile_16.close();
               file_28.delete();
               bool_29 = true;
            } catch (Exception exception_24) {
               bool_29 = false;
            }

            if (!bool_29) {
               string_5 = null;
            }
         }
      }

      if (string_5 == null && i_2 == 0) {
         label159:
         for (int i_17 = 0; i_17 < class102.field1318.length; i_17++) {
            for (int i_18 = 0; i_18 < class86.field1117.length; i_18++) {
               File file_19 = new File(class86.field1117[i_18] + class102.field1318[i_17] + File.separatorChar + string_0 + File.separatorChar);
               if (file_19.exists()) {
                  File file_20 = new File(file_19, "test.dat");

                  boolean bool_30;
                  try {
                     RandomAccessFile randomaccessfile_13 = new RandomAccessFile(file_20, "rw");
                     int i_14 = randomaccessfile_13.read();
                     randomaccessfile_13.seek(0L);
                     randomaccessfile_13.write(i_14);
                     randomaccessfile_13.seek(0L);
                     randomaccessfile_13.close();
                     file_20.delete();
                     bool_30 = true;
                  } catch (Exception exception_23) {
                     bool_30 = false;
                  }

                  if (bool_30) {
                     string_5 = file_19.toString();
                     bool_7 = true;
                     break label159;
                  }
               }
            }
         }
      }

      if (string_5 == null) {
         string_5 = class129.field1559 + File.separatorChar + "jagexcache" + str_4 + File.separatorChar + string_0 + File.separatorChar + string_1 + File.separatorChar;
         bool_7 = true;
      }

      File file_27;
      if (string_6 != null) {
         file_27 = new File(string_6);
         file_28 = new File(string_5);

         try {
            File[] arr_34 = file_27.listFiles();
            File[] arr_32 = arr_34;

            for (i_12 = 0; i_12 < arr_32.length; i_12++) {
               File file_31 = arr_32[i_12];
               File file_21 = new File(file_28, file_31.getName());
               boolean bool_15 = file_31.renameTo(file_21);
               if (!bool_15) {
                  throw new IOException();
               }
            }
         } catch (Exception exception_25) {
            exception_25.printStackTrace();
         }

         bool_7 = true;
      }

      if (bool_7) {
         file_27 = new File(string_5);
         class310_9 = null;

         try {
            class353 class353_35 = new class353(class176.field2061, "rw", 10000L);
            class310 class310_33 = new class310(500);
            class310_33.method5482(3, (byte) -124);
            class310_33.method5482(class310_9 != null ? 1 : 0, (byte) -76);
            class310_33.method5494(file_27.getPath(), 707162171);
            if (class310_9 != null) {
               //class310_33.method5494(class310_9.getPath(), 707162171);
            }

            class353_35.method6565(class310_33.field3752, 0, class310_33.field3751, 65432301);
            class353_35.method6569((byte) 0);
         } catch (IOException ioexception_22) {
            ioexception_22.printStackTrace();
         }
      }

      return new File(string_5);
   }

   static int method916(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      if (i_0 == 5306) {
         class85.field1095[++class253.field3267 - 1] = class116.method2537((byte) 92);
         return 1;
      } else {
         int i_4;
         if (i_0 == 5307) {
            i_4 = class85.field1095[--class253.field3267];
            if (i_4 == 1 || i_4 == 2) {
               class13.method133(i_4, 1786412488);
            }

            return 1;
         } else if (i_0 == 5308) {
            class85.field1095[++class253.field3267 - 1] = class282.field3617.field1054;
            return 1;
         } else if (i_0 != 5309) {
            if (i_0 == 5310) {
               --class253.field3267;
               return 1;
            } else {
               return 2;
            }
         } else {
            i_4 = class85.field1095[--class253.field3267];
            if (i_4 == 1 || i_4 == 2) {
               class282.field3617.field1054 = i_4;
               class18.method187(-1317127057);
            }

            return 1;
         }
      }
   }

}
