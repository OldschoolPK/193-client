import java.util.Locale;

public class class199 implements class203 {

   public static final class199 field2382;
   static final class199 field2374;
   public static final class199 field2375;
   static final class199 field2383;
   static final class199 field2377;
   public static final class199 field2378;
   static final class199 field2379;
   static final class199[] field2376;
   final String field2380;
   final String field2381;
   final int field2373;

   static {
      field2382 = new class199("EN", "en", "English", class201.field2402, 0, "GB");
      field2374 = new class199("DE", "de", "German", class201.field2402, 1, "DE");
      field2375 = new class199("FR", "fr", "French", class201.field2402, 2, "FR");
      field2383 = new class199("PT", "pt", "Portuguese", class201.field2402, 3, "BR");
      field2377 = new class199("NL", "nl", "Dutch", class201.field2397, 4, "NL");
      field2378 = new class199("ES", "es", "Spanish", class201.field2397, 5, "ES");
      field2379 = new class199("ES_MX", "es-mx", "Spanish (Latin American)", class201.field2402, 6, "MX");
      class199[] arr_0 = method3677(-1061791428);
      field2376 = new class199[arr_0.length];
      class199[] arr_1 = arr_0;

      for (int i_2 = 0; i_2 < arr_1.length; i_2++) {
         class199 class199_3 = arr_1[i_2];
         if (field2376[class199_3.field2373] != null) {
            throw new IllegalStateException();
         }

         field2376[class199_3.field2373] = class199_3;
      }

   }

   class199(String string_1, String string_2, String string_3, class201 class201_4, int i_5, String string_6) {
      this.field2380 = string_1;
      this.field2381 = string_2;
      this.field2373 = i_5;
      if (string_6 != null) {
         new Locale(string_2.substring(0, 2), string_6);
      } else {
         new Locale(string_2.substring(0, 2));
      }

   }

   String method3669(int i_1) {
      return this.field2381;
   }

   public int vmethod6086(int i_1) {
      return this.field2373;
   }

   public String toString() {
      return this.method3669(379304040).toLowerCase(Locale.ENGLISH);
   }

   public String abd() {
      return this.method3669(1133100072).toLowerCase(Locale.ENGLISH);
   }

   public String aba() {
      return this.method3669(-1480281622).toLowerCase(Locale.ENGLISH);
   }

   public static class199 method3671(int i_0, byte b_1) {
      return i_0 >= 0 && i_0 < field2376.length ? field2376[i_0] : null;
   }

   static class199[] method3677(int i_0) {
      return new class199[] {field2383, field2377, field2378, field2374, field2382, field2375, field2379};
   }

   static void method3676(int i_0, int i_1) {
      class68 class68_2 = (class68) class68.field546.method5968((long)i_0);
      if (class68_2 != null) {
         for (int i_3 = 0; i_3 < class68_2.field540.length; i_3++) {
            class68_2.field540[i_3] = -1;
            class68_2.field539[i_3] = 0;
         }

      }
   }

   static int method3678(int i_0, class101 class101_1, boolean bool_2, byte b_3) {
      if (i_0 == 3300) {
         class85.field1095[++class253.field3267 - 1] = client.field655;
         return 1;
      } else {
         int i_4;
         int i_5;
         if (i_0 == 3301) {
            class253.field3267 -= 2;
            i_4 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            class85.field1095[++class253.field3267 - 1] = class60.method942(i_4, i_5, (byte) 24);
            return 1;
         } else if (i_0 == 3302) {
            class253.field3267 -= 2;
            i_4 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            class85.field1095[++class253.field3267 - 1] = class8.method93(i_4, i_5, -497960753);
            return 1;
         } else if (i_0 == 3303) {
            class253.field3267 -= 2;
            i_4 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            class85.field1095[++class253.field3267 - 1] = class147.method3328(i_4, i_5, (byte) -67);
            return 1;
         } else if (i_0 == 3304) {
            i_4 = class85.field1095[--class253.field3267];
            class85.field1095[++class253.field3267 - 1] = class81.method1864(i_4, -1374757475).field3234;
            return 1;
         } else if (i_0 == 3305) {
            i_4 = class85.field1095[--class253.field3267];
            class85.field1095[++class253.field3267 - 1] = client.field780[i_4];
            return 1;
         } else if (i_0 == 3306) {
            i_4 = class85.field1095[--class253.field3267];
            class85.field1095[++class253.field3267 - 1] = client.field781[i_4];
            return 1;
         } else if (i_0 == 3307) {
            i_4 = class85.field1095[--class253.field3267];
            class85.field1095[++class253.field3267 - 1] = client.field675[i_4];
            return 1;
         } else {
            int i_6;
            if (i_0 == 3308) {
               i_4 = class151.field1949;
               i_5 = (class223.field2562.field990 >> 7) + class243.field3156;
               i_6 = (class223.field2562.field938 >> 7) + class41.field300;
               class85.field1095[++class253.field3267 - 1] = (i_5 << 14) + i_6 + (i_4 << 28);
               return 1;
            } else if (i_0 == 3309) {
               i_4 = class85.field1095[--class253.field3267];
               class85.field1095[++class253.field3267 - 1] = i_4 >> 14 & 0x3fff;
               return 1;
            } else if (i_0 == 3310) {
               i_4 = class85.field1095[--class253.field3267];
               class85.field1095[++class253.field3267 - 1] = i_4 >> 28;
               return 1;
            } else if (i_0 == 3311) {
               i_4 = class85.field1095[--class253.field3267];
               class85.field1095[++class253.field3267 - 1] = i_4 & 0x3fff;
               return 1;
            } else if (i_0 == 3312) {
               class85.field1095[++class253.field3267 - 1] = client.field659 ? 1 : 0;
               return 1;
            } else if (i_0 == 3313) {
               class253.field3267 -= 2;
               i_4 = class85.field1095[class253.field3267] + 32768;
               i_5 = class85.field1095[class253.field3267 + 1];
               class85.field1095[++class253.field3267 - 1] = class60.method942(i_4, i_5, (byte) 24);
               return 1;
            } else if (i_0 == 3314) {
               class253.field3267 -= 2;
               i_4 = class85.field1095[class253.field3267] + 32768;
               i_5 = class85.field1095[class253.field3267 + 1];
               class85.field1095[++class253.field3267 - 1] = class8.method93(i_4, i_5, -497960753);
               return 1;
            } else if (i_0 == 3315) {
               class253.field3267 -= 2;
               i_4 = class85.field1095[class253.field3267] + 32768;
               i_5 = class85.field1095[class253.field3267 + 1];
               class85.field1095[++class253.field3267 - 1] = class147.method3328(i_4, i_5, (byte) -26);
               return 1;
            } else if (i_0 == 3316) {
               if (client.field775 >= 2) {
                  class85.field1095[++class253.field3267 - 1] = client.field775;
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 3317) {
               class85.field1095[++class253.field3267 - 1] = client.field662;
               return 1;
            } else if (i_0 == 3318) {
               class85.field1095[++class253.field3267 - 1] = client.field645;
               return 1;
            } else if (i_0 == 3321) {
               class85.field1095[++class253.field3267 - 1] = client.field815;
               return 1;
            } else if (i_0 == 3322) {
               class85.field1095[++class253.field3267 - 1] = client.field816;
               return 1;
            } else if (i_0 == 3323) {
               if (client.field819) {
                  class85.field1095[++class253.field3267 - 1] = 1;
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 3324) {
               class85.field1095[++class253.field3267 - 1] = client.field646;
               return 1;
            } else if (i_0 == 3325) {
               class253.field3267 -= 4;
               i_4 = class85.field1095[class253.field3267];
               i_5 = class85.field1095[class253.field3267 + 1];
               i_6 = class85.field1095[class253.field3267 + 2];
               int i_7 = class85.field1095[class253.field3267 + 3];
               i_4 += i_5 << 14;
               i_4 += i_6 << 28;
               i_4 += i_7;
               class85.field1095[++class253.field3267 - 1] = i_4;
               return 1;
            } else {
               return 2;
            }
         }
      }
   }

}
