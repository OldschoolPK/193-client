public class class259 extends class184 {

   public static class244 field3323;
   static int[] field3325;
   public static class154 field3324 = new class154(64);
   public int field3326;
   public int field3328;
   public int field3327;

   void method4487(class310 class310_1, int i_2, int i_3) {
      if (i_2 == 1) {
         this.field3326 = class310_1.method5729(-12371553);
         this.field3328 = class310_1.method5661((byte) 2);
         this.field3327 = class310_1.method5661((byte) 96);
      }

   }

   public void method4484(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -90);
         if (i_3 == 0) {
            return;
         }

         this.method4487(class310_1, i_3, -1699368159);
      }
   }

   public static String method4492(CharSequence charsequence_0, class348 class348_1, byte b_2) {
      if (charsequence_0 == null) {
         return null;
      } else {
         int i_3 = 0;

         int i_4;
         boolean bool_5;
         char var_6;
         for (i_4 = charsequence_0.length(); i_3 < i_4; i_3++) {
            var_6 = charsequence_0.charAt(i_3);
            bool_5 = var_6 == 160 || var_6 == 32 || var_6 == 95 || var_6 == 45;
            if (!bool_5) {
               break;
            }
         }

         while (i_4 > i_3) {
            var_6 = charsequence_0.charAt(i_4 - 1);
            bool_5 = var_6 == 160 || var_6 == 32 || var_6 == 95 || var_6 == 45;
            if (!bool_5) {
               break;
            }

            --i_4;
         }

         int i_11 = i_4 - i_3;
         if (i_11 >= 1 && i_11 <= class67.method1232(class348_1, -737934411)) {
            StringBuilder stringbuilder_10 = new StringBuilder(i_11);

            for (int i_7 = i_3; i_7 < i_4; i_7++) {
               char var_8 = charsequence_0.charAt(i_7);
               if (class264.method4624(var_8, 1234402449)) {
                  char var_9 = class163.method3464(var_8, 1725294085);
                  if (var_9 != 0) {
                     stringbuilder_10.append(var_9);
                  }
               }
            }

            if (stringbuilder_10.length() == 0) {
               return null;
            } else {
               return stringbuilder_10.toString();
            }
         } else {
            return null;
         }
      }
   }

}
