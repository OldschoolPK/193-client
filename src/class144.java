public abstract class class144 extends class184 {

   static int field1893;
   public int field1894 = 1000;

   protected class136 vmethod3305(int i_1) {
      return null;
   }

   void vmethod3310(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, long long_9) {
      class136 class136_11 = this.vmethod3305(-1096484145);
      if (class136_11 != null) {
         this.field1894 = class136_11.field1894;
         class136_11.vmethod3310(i_1, i_2, i_3, i_4, i_5, i_6, i_7, i_8, long_9);
      }

   }

   static long method3312(CharSequence charsequence_0, byte b_1) {
      long long_2 = 0L;
      int i_4 = charsequence_0.length();

      for (int i_5 = 0; i_5 < i_4; i_5++) {
         long_2 *= 37L;
         char var_6 = charsequence_0.charAt(i_5);
         if (var_6 >= 65 && var_6 <= 90) {
            long_2 += (long)(var_6 + 1 - 65);
         } else if (var_6 >= 97 && var_6 <= 122) {
            long_2 += (long)(var_6 + 1 - 97);
         } else if (var_6 >= 48 && var_6 <= 57) {
            long_2 += (long)(var_6 + 27 - 48);
         }

         if (long_2 >= 177917621779460413L) {
            break;
         }
      }

      while (long_2 % 37L == 0L && long_2 != 0L) {
         long_2 /= 37L;
      }

      return long_2;
   }

}
