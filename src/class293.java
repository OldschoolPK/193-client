public class class293 implements Comparable {

   static byte[][][] field3663;
   static String[] field3665;
   String field3664;
   String field3662;

   public class293(String string_1, class348 class348_2) {
      this.field3664 = string_1;
      this.field3662 = class259.method4492(string_1, class348_2, (byte) 3);
   }

   public int method5193(class293 class293_1, int i_2) {
      return this.field3662 == null ? (class293_1.field3662 == null ? 0 : 1) : (class293_1.field3662 == null ? -1 : this.field3662.compareTo(class293_1.field3662));
   }

   public boolean method5187(byte b_1) {
      return this.field3662 != null;
   }

   public String method5186(int i_1) {
      return this.field3664;
   }

   public boolean equals(Object object_1) {
      if (object_1 instanceof class293) {
         class293 class293_2 = (class293) object_1;
         return this.field3662 == null ? class293_2.field3662 == null : (class293_2.field3662 == null ? false : (this.hashCode() != class293_2.hashCode() ? false : this.field3662.equals(class293_2.field3662)));
      } else {
         return false;
      }
   }

   public int hashCode() {
      return this.field3662 == null ? 0 : this.field3662.hashCode();
   }

   public int compareTo(Object object_1) {
      return this.method5193((class293) object_1, -1152802722);
   }

   public String aba() {
      return this.method5186(-1707895677);
   }

   public String toString() {
      return this.method5186(-1189841453);
   }

   public String abd() {
      return this.method5186(-1926640042);
   }

}
