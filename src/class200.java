public class class200 {

   static class226[] field2390;
   static final class200 field2387 = new class200();
   static final class200 field2388 = new class200();
   static final class200 field2389 = new class200();

   public static void method3681(class244 class244_0, byte b_1) {
      class259.field3323 = class244_0;
   }

   static int method3680(int i_0, class101 class101_1, boolean bool_2, short s_3) {
      int i_4 = -1;
      class226 class226_5;
      if (i_0 >= 2000) {
         i_0 -= 1000;
         i_4 = class85.field1095[--class253.field3267];
         class226_5 = class181.method3610(i_4, -1098483259);
      } else {
         class226_5 = bool_2 ? class223.field2561 : class253.field3264;
      }

      if (i_0 == 1000) {
         class253.field3267 -= 4;
         class226_5.field2595 = class85.field1095[class253.field3267];
         class226_5.field2579 = class85.field1095[class253.field3267 + 1];
         class226_5.field2591 = class85.field1095[class253.field3267 + 2];
         class226_5.field2692 = class85.field1095[class253.field3267 + 3];
         class181.method3609(class226_5, 309655613);
         class27.field147.method1672(class226_5, 1783089751);
         if (i_4 != -1 && class226_5.field2588 == 0) {
            class77.method1773(class9.field44[i_4 >> 16], class226_5, false, (byte) 0);
         }

         return 1;
      } else if (i_0 == 1001) {
         class253.field3267 -= 4;
         class226_5.field2597 = class85.field1095[class253.field3267];
         class226_5.field2598 = class85.field1095[class253.field3267 + 1];
         class226_5.field2593 = class85.field1095[class253.field3267 + 2];
         class226_5.field2594 = class85.field1095[class253.field3267 + 3];
         class181.method3609(class226_5, -1154432841);
         class27.field147.method1672(class226_5, 925643940);
         if (i_4 != -1 && class226_5.field2588 == 0) {
            class77.method1773(class9.field44[i_4 >> 16], class226_5, false, (byte) 0);
         }

         return 1;
      } else if (i_0 == 1003) {
         boolean bool_6 = class85.field1095[--class253.field3267] == 1;
         if (bool_6 != class226_5.field2643) {
            class226_5.field2643 = bool_6;
            class181.method3609(class226_5, 771546584);
         }

         return 1;
      } else if (i_0 == 1005) {
         class226_5.field2726 = class85.field1095[--class253.field3267] == 1;
         return 1;
      } else if (i_0 == 1006) {
         class226_5.field2727 = class85.field1095[--class253.field3267] == 1;
         return 1;
      } else {
         return 2;
      }
   }

}
