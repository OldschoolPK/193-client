import java.util.Iterator;

public class class327 implements Iterator {

   class189 field3868 = null;
   class326 field3871;
   class189 field3869;
   int field3870;

   class327(class326 class326_1) {
      this.field3871 = class326_1;
      this.method5948();
   }

   void method5948() {
      this.field3869 = this.field3871.field3864[0].field2136;
      this.field3870 = 1;
      this.field3868 = null;
   }

   public Object next() {
      class189 class189_1;
      if (this.field3871.field3864[this.field3870 - 1] != this.field3869) {
         class189_1 = this.field3869;
         this.field3869 = class189_1.field2136;
         this.field3868 = class189_1;
         return class189_1;
      } else {
         do {
            if (this.field3870 >= this.field3871.field3867) {
               return null;
            }

            class189_1 = this.field3871.field3864[this.field3870++].field2136;
         } while (class189_1 == this.field3871.field3864[this.field3870 - 1]);

         this.field3869 = class189_1.field2136;
         this.field3868 = class189_1;
         return class189_1;
      }
   }

   public void remove() {
      this.field3868.method3628();
      this.field3868 = null;
   }

   public boolean hasNext() {
      if (this.field3871.field3864[this.field3870 - 1] != this.field3869) {
         return true;
      } else {
         while (this.field3870 < this.field3871.field3867) {
            if (this.field3871.field3864[this.field3870++].field2136 != this.field3871.field3864[this.field3870 - 1]) {
               this.field3869 = this.field3871.field3864[this.field3870 - 1].field2136;
               return true;
            }

            this.field3869 = this.field3871.field3864[this.field3870 - 1];
         }

         return false;
      }
   }

}
