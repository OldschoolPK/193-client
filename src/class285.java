import java.util.Comparator;

public abstract class class285 implements Comparator {

   static int field3631;
   static class15 field3634;
   Comparator field3632;

   protected final int method5051(class284 class284_1, class284 class284_2, int i_3) {
      return this.field3632 == null ? 0 : this.field3632.compare(class284_1, class284_2);
   }

   final void method5048(Comparator comparator_1, int i_2) {
      if (this.field3632 == null) {
         this.field3632 = comparator_1;
      } else if (this.field3632 instanceof class285) {
         ((class285) this.field3632).method5048(comparator_1, 118963424);
      }

   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

}
