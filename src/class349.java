public class class349 {

   static class334[] field4076;
   static final char[] field4077 = new char[] {' ', ' ', '_', '-', 'à', 'á', 'â', 'ä', 'ã', 'À', 'Á', 'Â', 'Ä', 'Ã', 'è', 'é', 'ê', 'ë', 'È', 'É', 'Ê', 'Ë', 'í', 'î', 'ï', 'Í', 'Î', 'Ï', 'ò', 'ó', 'ô', 'ö', 'õ', 'Ò', 'Ó', 'Ô', 'Ö', 'Õ', 'ù', 'ú', 'û', 'ü', 'Ù', 'Ú', 'Û', 'Ü', 'ç', 'Ç', 'ÿ', 'Ÿ', 'ñ', 'Ñ', 'ß'};
   static final char[] field4079 = new char[] {'[', ']', '#'};

   static void method6517(class309 class309_0, int i_1, int i_2) {
      boolean bool_3 = class309_0.method5455(1, (byte) -16) == 1;
      if (bool_3) {
         class98.field1265[++class98.field1262 - 1] = i_1;
      }

      int i_4 = class309_0.method5455(2, (byte) -127);
      class75 class75_5 = client.field764[i_1];
      if (i_4 == 0) {
         if (bool_3) {
            class75_5.field631 = false;
         } else if (client.field765 == i_1) {
            throw new RuntimeException();
         } else {
            class98.field1259[i_1] = (class75_5.field627 << 28) + (class243.field3156 + class75_5.field993[0] >> 13 << 14) + (class41.field300 + class75_5.field968[0] >> 13);
            if (class75_5.field959 != -1) {
               class98.field1269[i_1] = class75_5.field959;
            } else {
               class98.field1269[i_1] = class75_5.field989;
            }

            class98.field1261[i_1] = class75_5.field980;
            client.field764[i_1] = null;
            if (class309_0.method5455(1, (byte) -62) != 0) {
               class28.method286(class309_0, i_1, 330246375);
            }

         }
      } else {
         int i_6;
         int i_7;
         int i_8;
         if (i_4 == 1) {
            i_6 = class309_0.method5455(3, (byte) -90);
            i_7 = class75_5.field993[0];
            i_8 = class75_5.field968[0];
            if (i_6 == 0) {
               --i_7;
               --i_8;
            } else if (i_6 == 1) {
               --i_8;
            } else if (i_6 == 2) {
               ++i_7;
               --i_8;
            } else if (i_6 == 3) {
               --i_7;
            } else if (i_6 == 4) {
               ++i_7;
            } else if (i_6 == 5) {
               --i_7;
               ++i_8;
            } else if (i_6 == 6) {
               ++i_8;
            } else if (i_6 == 7) {
               ++i_7;
               ++i_8;
            }

            if (client.field765 != i_1 || class75_5.field990 >= 1536 && class75_5.field938 >= 1536 && class75_5.field990 < 11776 && class75_5.field938 < 11776) {
               if (bool_3) {
                  class75_5.field631 = true;
                  class75_5.field632 = i_7;
                  class75_5.field633 = i_8;
               } else {
                  class75_5.field631 = false;
                  class75_5.method1313(i_7, i_8, class98.field1253[i_1], -1294248834);
               }
            } else {
               class75_5.method1314(i_7, i_8, -1865128015);
               class75_5.field631 = false;
            }

         } else if (i_4 == 2) {
            i_6 = class309_0.method5455(4, (byte) -102);
            i_7 = class75_5.field993[0];
            i_8 = class75_5.field968[0];
            if (i_6 == 0) {
               i_7 -= 2;
               i_8 -= 2;
            } else if (i_6 == 1) {
               --i_7;
               i_8 -= 2;
            } else if (i_6 == 2) {
               i_8 -= 2;
            } else if (i_6 == 3) {
               ++i_7;
               i_8 -= 2;
            } else if (i_6 == 4) {
               i_7 += 2;
               i_8 -= 2;
            } else if (i_6 == 5) {
               i_7 -= 2;
               --i_8;
            } else if (i_6 == 6) {
               i_7 += 2;
               --i_8;
            } else if (i_6 == 7) {
               i_7 -= 2;
            } else if (i_6 == 8) {
               i_7 += 2;
            } else if (i_6 == 9) {
               i_7 -= 2;
               ++i_8;
            } else if (i_6 == 10) {
               i_7 += 2;
               ++i_8;
            } else if (i_6 == 11) {
               i_7 -= 2;
               i_8 += 2;
            } else if (i_6 == 12) {
               --i_7;
               i_8 += 2;
            } else if (i_6 == 13) {
               i_8 += 2;
            } else if (i_6 == 14) {
               ++i_7;
               i_8 += 2;
            } else if (i_6 == 15) {
               i_7 += 2;
               i_8 += 2;
            }

            if (client.field765 != i_1 || class75_5.field990 >= 1536 && class75_5.field938 >= 1536 && class75_5.field990 < 11776 && class75_5.field938 < 11776) {
               if (bool_3) {
                  class75_5.field631 = true;
                  class75_5.field632 = i_7;
                  class75_5.field633 = i_8;
               } else {
                  class75_5.field631 = false;
                  class75_5.method1313(i_7, i_8, class98.field1253[i_1], 1374853564);
               }
            } else {
               class75_5.method1314(i_7, i_8, -104505225);
               class75_5.field631 = false;
            }

         } else {
            i_6 = class309_0.method5455(1, (byte) -41);
            int i_9;
            int i_10;
            int i_11;
            int i_12;
            if (i_6 == 0) {
               i_7 = class309_0.method5455(12, (byte) -79);
               i_8 = i_7 >> 10;
               i_9 = i_7 >> 5 & 0x1f;
               if (i_9 > 15) {
                  i_9 -= 32;
               }

               i_10 = i_7 & 0x1f;
               if (i_10 > 15) {
                  i_10 -= 32;
               }

               i_11 = i_9 + class75_5.field993[0];
               i_12 = i_10 + class75_5.field968[0];
               if (client.field765 == i_1 && (class75_5.field990 < 1536 || class75_5.field938 < 1536 || class75_5.field990 >= 11776 || class75_5.field938 >= 11776)) {
                  class75_5.method1314(i_11, i_12, -1820009842);
                  class75_5.field631 = false;
               } else if (bool_3) {
                  class75_5.field631 = true;
                  class75_5.field632 = i_11;
                  class75_5.field633 = i_12;
               } else {
                  class75_5.field631 = false;
                  class75_5.method1313(i_11, i_12, class98.field1253[i_1], -157527049);
               }

               class75_5.field627 = (byte)(i_8 + class75_5.field627 & 0x3);
               if (client.field765 == i_1) {
                  class151.field1949 = class75_5.field627;
               }

            } else {
               i_7 = class309_0.method5455(30, (byte) -49);
               i_8 = i_7 >> 28;
               i_9 = i_7 >> 14 & 0x3fff;
               i_10 = i_7 & 0x3fff;
               i_11 = (i_9 + class243.field3156 + class75_5.field993[0] & 0x3fff) - class243.field3156;
               i_12 = (i_10 + class41.field300 + class75_5.field968[0] & 0x3fff) - class41.field300;
               if (client.field765 == i_1 && (class75_5.field990 < 1536 || class75_5.field938 < 1536 || class75_5.field990 >= 11776 || class75_5.field938 >= 11776)) {
                  class75_5.method1314(i_11, i_12, -1469036934);
                  class75_5.field631 = false;
               } else if (bool_3) {
                  class75_5.field631 = true;
                  class75_5.field632 = i_11;
                  class75_5.field633 = i_12;
               } else {
                  class75_5.field631 = false;
                  class75_5.method1313(i_11, i_12, class98.field1253[i_1], 46931018);
               }

               class75_5.field627 = (byte)(i_8 + class75_5.field627 & 0x3);
               if (client.field765 == i_1) {
                  class151.field1949 = class75_5.field627;
               }

            }
         }
      }
   }

}
