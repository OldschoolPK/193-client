public class class254 extends class184 {

   static class244 field3271;
   static class244 field3285;
   int field3274;
   static class154 field3281 = new class154(64);
   static class154 field3273 = new class154(30);
   public int field3284 = -1;
   int field3279 = 128;
   int field3272 = 128;
   int field3283 = 0;
   int field3276 = 0;
   int field3270 = 0;
   int field3275;
   short[] field3277;
   short[] field3278;
   short[] field3282;
   short[] field3280;

   void method4398(class310 class310_1, int i_2, byte b_3) {
      if (i_2 == 1) {
         this.field3275 = class310_1.method5729(-1628284242);
      } else if (i_2 == 2) {
         this.field3284 = class310_1.method5729(1058579761);
      } else if (i_2 == 4) {
         this.field3279 = class310_1.method5729(-745117621);
      } else if (i_2 == 5) {
         this.field3272 = class310_1.method5729(-828777222);
      } else if (i_2 == 6) {
         this.field3283 = class310_1.method5729(-973202882);
      } else if (i_2 == 7) {
         this.field3276 = class310_1.method5661((byte) -29);
      } else if (i_2 == 8) {
         this.field3270 = class310_1.method5661((byte) -52);
      } else {
         int i_4;
         int i_5;
         if (i_2 == 40) {
            i_4 = class310_1.method5661((byte) -18);
            this.field3277 = new short[i_4];
            this.field3278 = new short[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3277[i_5] = (short)class310_1.method5729(-42515011);
               this.field3278[i_5] = (short)class310_1.method5729(487131406);
            }
         } else if (i_2 == 41) {
            i_4 = class310_1.method5661((byte) 28);
            this.field3282 = new short[i_4];
            this.field3280 = new short[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3282[i_5] = (short)class310_1.method5729(102864453);
               this.field3280[i_5] = (short)class310_1.method5729(827070317);
            }
         }
      }

   }

   public final class136 method4411(int i_1, int i_2) {
      class136 class136_3 = (class136) field3273.method3376((long)this.field3274);
      if (class136_3 == null) {
         class130 class130_4 = class130.method2824(field3271, this.field3275, 0);
         if (class130_4 == null) {
            return null;
         }

         int i_5;
         if (this.field3277 != null) {
            for (i_5 = 0; i_5 < this.field3277.length; i_5++) {
               class130_4.method2837(this.field3277[i_5], this.field3278[i_5]);
            }
         }

         if (this.field3282 != null) {
            for (i_5 = 0; i_5 < this.field3282.length; i_5++) {
               class130_4.method2833(this.field3282[i_5], this.field3280[i_5]);
            }
         }

         class136_3 = class130_4.method2876(this.field3276 + 64, this.field3270 + 850, -30, -50, -30);
         field3273.method3374(class136_3, (long)this.field3274);
      }

      class136 class136_6;
      if (this.field3284 != -1 && i_1 != -1) {
         class136_6 = class260.method4510(this.field3284, -1759630570).method4785(class136_3, i_1, -109933088);
      } else {
         class136_6 = class136_3.method2922(true);
      }

      if (this.field3279 != 128 || this.field3272 != 128) {
         class136_6.method2937(this.field3279, this.field3272, this.field3279);
      }

      if (this.field3283 != 0) {
         if (this.field3283 == 90) {
            class136_6.method2945();
         }

         if (this.field3283 == 180) {
            class136_6.method2945();
            class136_6.method2945();
         }

         if (this.field3283 == 270) {
            class136_6.method2945();
            class136_6.method2945();
            class136_6.method2945();
         }
      }

      return class136_6;
   }

   void method4397(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -78);
         if (i_3 == 0) {
            return;
         }

         this.method4398(class310_1, i_3, (byte) 7);
      }
   }

}
