public final class class334 extends class331 {

   public int[] field3904;
   public int field3910;
   public int field3906;
   public byte[] field3903;
   public int field3905;
   public int field3909;
   public int field3907;
   public int field3908;

   public void method6108() {
      if (this.field3905 != this.field3910 || this.field3909 != this.field3906) {
         byte[] bytes_1 = new byte[this.field3910 * this.field3906];
         int i_2 = 0;

         for (int i_3 = 0; i_3 < this.field3909; i_3++) {
            for (int i_4 = 0; i_4 < this.field3905; i_4++) {
               bytes_1[i_4 + (i_3 + this.field3908) * this.field3910 + this.field3907] = this.field3903[i_2++];
            }
         }

         this.field3903 = bytes_1;
         this.field3905 = this.field3910;
         this.field3909 = this.field3906;
         this.field3907 = 0;
         this.field3908 = 0;
      }
   }

   public void method6091(int i_1, int i_2) {
      i_1 += this.field3907;
      i_2 += this.field3908;
      int i_3 = i_1 + i_2 * class331.field3889;
      int i_4 = 0;
      int i_5 = this.field3909;
      int i_6 = this.field3905;
      int i_7 = class331.field3889 - i_6;
      int i_8 = 0;
      int i_9;
      if (i_2 < class331.field3887) {
         i_9 = class331.field3887 - i_2;
         i_5 -= i_9;
         i_2 = class331.field3887;
         i_4 += i_9 * i_6;
         i_3 += i_9 * class331.field3889;
      }

      if (i_5 + i_2 > class331.field3888) {
         i_5 -= i_5 + i_2 - class331.field3888;
      }

      if (i_1 < class331.field3890) {
         i_9 = class331.field3890 - i_1;
         i_6 -= i_9;
         i_1 = class331.field3890;
         i_4 += i_9;
         i_3 += i_9;
         i_8 += i_9;
         i_7 += i_9;
      }

      if (i_6 + i_1 > class331.field3884) {
         i_9 = i_6 + i_1 - class331.field3884;
         i_6 -= i_9;
         i_8 += i_9;
         i_7 += i_9;
      }

      if (i_6 > 0 && i_5 > 0) {
         method6095(class331.field3886, this.field3903, this.field3904, i_4, i_3, i_6, i_5, i_7, i_8);
      }
   }

   public void method6096(int i_1, int i_2, int i_3, int i_4) {
      int i_5 = this.field3905;
      int i_6 = this.field3909;
      int i_7 = 0;
      int i_8 = 0;
      int i_9 = this.field3910;
      int i_10 = this.field3906;
      int i_11 = (i_9 << 16) / i_3;
      int i_12 = (i_10 << 16) / i_4;
      int i_13;
      if (this.field3907 > 0) {
         i_13 = (i_11 + (this.field3907 << 16) - 1) / i_11;
         i_1 += i_13;
         i_7 += i_13 * i_11 - (this.field3907 << 16);
      }

      if (this.field3908 > 0) {
         i_13 = (i_12 + (this.field3908 << 16) - 1) / i_12;
         i_2 += i_13;
         i_8 += i_13 * i_12 - (this.field3908 << 16);
      }

      if (i_5 < i_9) {
         i_3 = (i_11 + ((i_5 << 16) - i_7) - 1) / i_11;
      }

      if (i_6 < i_10) {
         i_4 = (i_12 + ((i_6 << 16) - i_8) - 1) / i_12;
      }

      i_13 = i_1 + i_2 * class331.field3889;
      int i_14 = class331.field3889 - i_3;
      if (i_2 + i_4 > class331.field3888) {
         i_4 -= i_2 + i_4 - class331.field3888;
      }

      int i_15;
      if (i_2 < class331.field3887) {
         i_15 = class331.field3887 - i_2;
         i_4 -= i_15;
         i_13 += i_15 * class331.field3889;
         i_8 += i_12 * i_15;
      }

      if (i_3 + i_1 > class331.field3884) {
         i_15 = i_3 + i_1 - class331.field3884;
         i_3 -= i_15;
         i_14 += i_15;
      }

      if (i_1 < class331.field3890) {
         i_15 = class331.field3890 - i_1;
         i_3 -= i_15;
         i_13 += i_15;
         i_7 += i_11 * i_15;
         i_14 += i_15;
      }

      method6092(class331.field3886, this.field3903, this.field3904, i_7, i_8, i_13, i_14, i_3, i_4, i_11, i_12, i_5);
   }

   public void method6093(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < this.field3904.length; i_4++) {
         int i_5 = this.field3904[i_4] >> 16 & 0xff;
         i_5 += i_1;
         if (i_5 < 0) {
            i_5 = 0;
         } else if (i_5 > 255) {
            i_5 = 255;
         }

         int i_6 = this.field3904[i_4] >> 8 & 0xff;
         i_6 += i_2;
         if (i_6 < 0) {
            i_6 = 0;
         } else if (i_6 > 255) {
            i_6 = 255;
         }

         int i_7 = this.field3904[i_4] & 0xff;
         i_7 += i_3;
         if (i_7 < 0) {
            i_7 = 0;
         } else if (i_7 > 255) {
            i_7 = 255;
         }

         this.field3904[i_4] = i_7 + (i_6 << 8) + (i_5 << 16);
      }

   }

   static void method6095(int[] ints_0, byte[] bytes_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8) {
      int i_9 = -(i_5 >> 2);
      i_5 = -(i_5 & 0x3);

      for (int i_10 = -i_6; i_10 < 0; i_10++) {
         int i_11;
         byte b_12;
         for (i_11 = i_9; i_11 < 0; i_11++) {
            b_12 = bytes_1[i_3++];
            if (b_12 != 0) {
               ints_0[i_4++] = ints_2[b_12 & 0xff];
            } else {
               ++i_4;
            }

            b_12 = bytes_1[i_3++];
            if (b_12 != 0) {
               ints_0[i_4++] = ints_2[b_12 & 0xff];
            } else {
               ++i_4;
            }

            b_12 = bytes_1[i_3++];
            if (b_12 != 0) {
               ints_0[i_4++] = ints_2[b_12 & 0xff];
            } else {
               ++i_4;
            }

            b_12 = bytes_1[i_3++];
            if (b_12 != 0) {
               ints_0[i_4++] = ints_2[b_12 & 0xff];
            } else {
               ++i_4;
            }
         }

         for (i_11 = i_5; i_11 < 0; i_11++) {
            b_12 = bytes_1[i_3++];
            if (b_12 != 0) {
               ints_0[i_4++] = ints_2[b_12 & 0xff];
            } else {
               ++i_4;
            }
         }

         i_4 += i_7;
         i_3 += i_8;
      }

   }

   static void method6092(int[] ints_0, byte[] bytes_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11) {
      int i_12 = i_3;

      for (int i_13 = -i_8; i_13 < 0; i_13++) {
         int i_14 = i_11 * (i_4 >> 16);

         for (int i_15 = -i_7; i_15 < 0; i_15++) {
            byte b_16 = bytes_1[(i_3 >> 16) + i_14];
            if (b_16 != 0) {
               ints_0[i_5++] = ints_2[b_16 & 0xff];
            } else {
               ++i_5;
            }

            i_3 += i_9;
         }

         i_4 += i_10;
         i_3 = i_12;
         i_5 += i_6;
      }

   }

}
