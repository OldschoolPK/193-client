public class class79 extends class189 {

   static int field1002;
   int field999;
   int field1000;
   int field1001;
   int field1007;

   class79(int i_1, int i_2, int i_3, int i_4) {
      this.field999 = i_1;
      this.field1000 = i_2;
      this.field1001 = i_3;
      this.field1007 = i_4;
   }

   void method1794(int i_1, int i_2, int i_3, int i_4, int i_5) {
      this.field999 = i_1;
      this.field1000 = i_2;
      this.field1001 = i_3;
      this.field1007 = i_4;
   }

   public static class257 method1798(int i_0, byte b_1) {
      class257 class257_2 = (class257) class257.field3308.method3376((long)i_0);
      if (class257_2 != null) {
         return class257_2;
      } else {
         byte[] bytes_3 = class225.field2573.method4160(33, i_0, (short) -18051);
         class257_2 = new class257();
         if (bytes_3 != null) {
            class257_2.method4463(new class310(bytes_3), (byte) 72);
         }

         class257.field3308.method3374(class257_2, (long)i_0);
         return class257_2;
      }
   }

   static final boolean method1797(class226 class226_0, int i_1) {
      int i_2 = class226_0.field2695;
      if (i_2 == 205) {
         client.field733 = 250;
         return true;
      } else {
         int i_3;
         int i_4;
         if (i_2 >= 300 && i_2 <= 313) {
            i_3 = (i_2 - 300) / 2;
            i_4 = i_2 & 0x1;
            client.field909.method4019(i_3, i_4 == 1, -1452083272);
         }

         if (i_2 >= 314 && i_2 <= 323) {
            i_3 = (i_2 - 314) / 2;
            i_4 = i_2 & 0x1;
            client.field909.method4010(i_3, i_4 == 1, -1113913855);
         }

         if (i_2 == 324) {
            client.field909.method4021(false, (byte) 77);
         }

         if (i_2 == 325) {
            client.field909.method4021(true, (byte) 23);
         }

         if (i_2 == 326) {
            class196 class196_5 = class68.method1249(class192.field2239, client.field694.field1328, (byte) 1);
            client.field909.method4012(class196_5.field2360, 1256612207);
            client.field694.method2295(class196_5, -1625966318);
            return true;
         } else {
            return false;
         }
      }
   }

   static int method1795(int i_0, int i_1) {
      return (int)Math.pow(2.0D, (double)((float)i_0 / 256.0F + 7.0F));
   }

}
