public class class87 extends class189 {

   class271 field1127 = new class271();
   class257 field1126;

   class87(class257 class257_1) {
      this.field1126 = class257_1;
   }

   void method2071(int i_1, int i_2, int i_3, int i_4, int i_5) {
      class79 class79_6 = null;
      int i_7 = 0;

      for (class79 class79_8 = (class79) this.field1127.method4806(); class79_8 != null; class79_8 = (class79) this.field1127.method4805()) {
         ++i_7;
         if (class79_8.field999 == i_1) {
            class79_8.method1794(i_1, i_2, i_3, i_4, 2048204816);
            return;
         }

         if (class79_8.field999 <= i_1) {
            class79_6 = class79_8;
         }
      }

      if (class79_6 == null) {
         if (i_7 < 4) {
            this.field1127.method4801(new class79(i_1, i_2, i_3, i_4));
         }

      } else {
         class271.method4802(new class79(i_1, i_2, i_3, i_4), class79_6);
         if (i_7 >= 4) {
            this.field1127.method4806().method3628();
         }

      }
   }

   class79 method2065(int i_1, int i_2) {
      class79 class79_3 = (class79) this.field1127.method4806();
      if (class79_3 != null && class79_3.field999 <= i_1) {
         for (class79 class79_4 = (class79) this.field1127.method4805(); class79_4 != null && class79_4.field999 <= i_1; class79_4 = (class79) this.field1127.method4805()) {
            class79_3.method3628();
            class79_3 = class79_4;
         }

         if (this.field1126.field3316 + class79_3.field999 + class79_3.field1007 > i_1) {
            return class79_3;
         } else {
            class79_3.method3628();
            return null;
         }
      } else {
         return null;
      }
   }

   boolean method2066(byte b_1) {
      return this.field1127.method4807();
   }

}
