final class class2 implements class0 {

   void method21(Long long_1, class310 class310_2, int i_3) {
      class310_2.method5490(long_1.longValue());
   }

   public Object vmethod48(class310 class310_1, byte b_2) {
      return Long.valueOf(class310_1.method5508(1081452177));
   }

   public void vmethod49(Object object_1, class310 class310_2, int i_3) {
      this.method21((Long) object_1, class310_2, -110177189);
   }

   public static final int method23(double d_0, double d_2, double d_4) {
      double d_6 = d_4;
      double d_8 = d_4;
      double d_10 = d_4;
      if (d_2 != 0.0D) {
         double d_12;
         if (d_4 < 0.5D) {
            d_12 = d_4 * (1.0D + d_2);
         } else {
            d_12 = d_4 + d_2 - d_2 * d_4;
         }

         double d_14 = d_4 * 2.0D - d_12;
         double d_16 = 0.3333333333333333D + d_0;
         if (d_16 > 1.0D) {
            --d_16;
         }

         double d_20 = d_0 - 0.3333333333333333D;
         if (d_20 < 0.0D) {
            ++d_20;
         }

         if (d_16 * 6.0D < 1.0D) {
            d_6 = (d_12 - d_14) * 6.0D * d_16 + d_14;
         } else if (2.0D * d_16 < 1.0D) {
            d_6 = d_12;
         } else if (d_16 * 3.0D < 2.0D) {
            d_6 = d_14 + 6.0D * (d_12 - d_14) * (0.6666666666666666D - d_16);
         } else {
            d_6 = d_14;
         }

         if (6.0D * d_0 < 1.0D) {
            d_8 = 6.0D * (d_12 - d_14) * d_0 + d_14;
         } else if (d_0 * 2.0D < 1.0D) {
            d_8 = d_12;
         } else if (d_0 * 3.0D < 2.0D) {
            d_8 = (d_12 - d_14) * (0.6666666666666666D - d_0) * 6.0D + d_14;
         } else {
            d_8 = d_14;
         }

         if (6.0D * d_20 < 1.0D) {
            d_10 = d_20 * (d_12 - d_14) * 6.0D + d_14;
         } else if (2.0D * d_20 < 1.0D) {
            d_10 = d_12;
         } else if (3.0D * d_20 < 2.0D) {
            d_10 = 6.0D * (0.6666666666666666D - d_20) * (d_12 - d_14) + d_14;
         } else {
            d_10 = d_14;
         }
      }

      int i_22 = (int)(256.0D * d_6);
      int i_13 = (int)(256.0D * d_8);
      int i_23 = (int)(d_10 * 256.0D);
      int i_15 = i_23 + (i_13 << 8) + (i_22 << 16);
      return i_15;
   }

}
