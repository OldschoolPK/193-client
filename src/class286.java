public class class286 extends class287 {

   public static int field3638;
   final class348 field3636;

   public class286(class348 class348_1) {
      super(400);
      this.field3636 = class348_1;
   }

   class284 vmethod5247(byte b_1) {
      return new class288();
   }

   class284[] vmethod5226(int i_1, int i_2) {
      return new class288[i_1];
   }

   public void method5057(class310 class310_1, int i_2, int i_3) {
      while (true) {
         if (class310_1.field3751 < i_2) {
            int i_4 = class310_1.method5661((byte) 8);
            boolean bool_5 = (i_4 & 0x1) == 1;
            class293 class293_6 = new class293(class310_1.method5589(-91384413), this.field3636);
            class293 class293_7 = new class293(class310_1.method5589(1814767571), this.field3636);
            class310_1.method5589(-814263514);
            if (class293_6 != null && class293_6.method5187((byte) -4)) {
               class288 class288_8 = (class288) this.method5124(class293_6, -1959972080);
               if (bool_5) {
                  class288 class288_9 = (class288) this.method5124(class293_7, -1673614871);
                  if (class288_9 != null && class288_9 != class288_8) {
                     if (class288_8 != null) {
                        this.method5078(class288_9, -1895086019);
                     } else {
                        class288_8 = class288_9;
                     }
                  }
               }

               if (class288_8 != null) {
                  this.method5083(class288_8, class293_6, class293_7, -2012575511);
                  continue;
               }

               if (this.method5071(1885552472) < 400) {
                  int i_10 = this.method5071(1505794173);
                  class288_8 = (class288) this.method5072(class293_6, class293_7, 1547669313);
                  class288_8.field3648 = i_10;
               }
               continue;
            }

            throw new IllegalStateException();
         }

         return;
      }
   }

   public static int method5061(int i_0, short s_1) {
      int i_2 = 0;
      if (i_0 < 0 || i_0 >= 65536) {
         i_0 >>>= 16;
         i_2 += 16;
      }

      if (i_0 >= 256) {
         i_0 >>>= 8;
         i_2 += 8;
      }

      if (i_0 >= 16) {
         i_0 >>>= 4;
         i_2 += 4;
      }

      if (i_0 >= 4) {
         i_0 >>>= 2;
         i_2 += 2;
      }

      if (i_0 >= 1) {
         i_0 >>>= 1;
         ++i_2;
      }

      return i_0 + i_2;
   }

}
