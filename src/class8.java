import java.util.concurrent.Callable;

public class class8 implements Callable {

   protected static int field31;
   static class334 field34;
   // $FF: synthetic field
   final class9 this$0;
   final class310 field35;
   final class12 field32;

   class8(class9 class9_1, class310 class310_2, class12 class12_3) {
      this.this$0 = class9_1;
      this.field35 = class310_2;
      this.field32 = class12_3;
   }

   public Object call() {
      return this.field32.vmethod123(this.field35);
   }

   static final boolean method91(class226 class226_0, int i_1) {
      if (class226_0.field2705 == null) {
         return false;
      } else {
         for (int i_2 = 0; i_2 < class226_0.field2705.length; i_2++) {
            int i_3 = class257.method4479(class226_0, i_2, (short) 128);
            int i_4 = class226_0.field2706[i_2];
            if (class226_0.field2705[i_2] == 2) {
               if (i_3 >= i_4) {
                  return false;
               }
            } else if (class226_0.field2705[i_2] == 3) {
               if (i_3 <= i_4) {
                  return false;
               }
            } else if (class226_0.field2705[i_2] == 4) {
               if (i_3 == i_4) {
                  return false;
               }
            } else if (i_4 != i_3) {
               return false;
            }
         }

         return true;
      }
   }

   static int method93(int i_0, int i_1, int i_2) {
      class68 class68_3 = (class68) class68.field546.method5968((long)i_0);
      return class68_3 == null ? 0 : (i_1 >= 0 && i_1 < class68_3.field539.length ? class68_3.field539[i_1] : 0);
   }

}
