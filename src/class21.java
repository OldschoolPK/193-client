import java.util.Comparator;
import java.util.Date;

final class class21 implements Comparator {

   static class335[] field98;
   static class152 field100;

   int method213(class16 class16_1, class16 class16_2, int i_3) {
      return class16_1.field69 < class16_2.field69 ? -1 : (class16_1.field69 == class16_2.field69 ? 0 : 1);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method213((class16) object_1, (class16) object_2, -382122880);
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   static int method218(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      String string_4;
      int i_5;
      if (i_0 == 4100) {
         string_4 = class85.field1096[--class85.field1105];
         i_5 = class85.field1095[--class253.field3267];
         class85.field1096[++class85.field1105 - 1] = string_4 + i_5;
         return 1;
      } else {
         String string_10;
         if (i_0 == 4101) {
            class85.field1105 -= 2;
            string_4 = class85.field1096[class85.field1105];
            string_10 = class85.field1096[class85.field1105 + 1];
            class85.field1096[++class85.field1105 - 1] = string_4 + string_10;
            return 1;
         } else if (i_0 == 4102) {
            string_4 = class85.field1096[--class85.field1105];
            i_5 = class85.field1095[--class253.field3267];
            class85.field1096[++class85.field1105 - 1] = string_4 + class257.method4470(i_5, true, (byte) 20);
            return 1;
         } else if (i_0 == 4103) {
            string_4 = class85.field1096[--class85.field1105];
            class85.field1096[++class85.field1105 - 1] = string_4.toLowerCase();
            return 1;
         } else {
            int i_7;
            int i_11;
            if (i_0 == 4104) {
               i_11 = class85.field1095[--class253.field3267];
               long long_12 = ((long)i_11 + 11745L) * 86400000L;
               class85.field1091.setTime(new Date(long_12));
               i_7 = class85.field1091.get(5);
               int i_17 = class85.field1091.get(2);
               int i_9 = class85.field1091.get(1);
               class85.field1096[++class85.field1105 - 1] = i_7 + "-" + class85.field1101[i_17] + "-" + i_9;
               return 1;
            } else if (i_0 != 4105) {
               if (i_0 == 4106) {
                  i_11 = class85.field1095[--class253.field3267];
                  class85.field1096[++class85.field1105 - 1] = Integer.toString(i_11);
                  return 1;
               } else if (i_0 == 4107) {
                  class85.field1105 -= 2;
                  class85.field1095[++class253.field3267 - 1] = class80.method1846(class152.method3365(class85.field1096[class85.field1105], class85.field1096[class85.field1105 + 1], class49.field372, -530251547), 614025062);
                  return 1;
               } else {
                  int i_6;
                  byte[] bytes_14;
                  class305 class305_15;
                  if (i_0 == 4108) {
                     string_4 = class85.field1096[--class85.field1105];
                     class253.field3267 -= 2;
                     i_5 = class85.field1095[class253.field3267];
                     i_6 = class85.field1095[class253.field3267 + 1];
                     bytes_14 = class19.field89.method4160(i_6, 0, (short) -27530);
                     class305_15 = new class305(bytes_14);
                     class85.field1095[++class253.field3267 - 1] = class305_15.method5398(string_4, i_5);
                     return 1;
                  } else if (i_0 == 4109) {
                     string_4 = class85.field1096[--class85.field1105];
                     class253.field3267 -= 2;
                     i_5 = class85.field1095[class253.field3267];
                     i_6 = class85.field1095[class253.field3267 + 1];
                     bytes_14 = class19.field89.method4160(i_6, 0, (short) -27564);
                     class305_15 = new class305(bytes_14);
                     class85.field1095[++class253.field3267 - 1] = class305_15.method5342(string_4, i_5);
                     return 1;
                  } else if (i_0 == 4110) {
                     class85.field1105 -= 2;
                     string_4 = class85.field1096[class85.field1105];
                     string_10 = class85.field1096[class85.field1105 + 1];
                     if (class85.field1095[--class253.field3267] == 1) {
                        class85.field1096[++class85.field1105 - 1] = string_4;
                     } else {
                        class85.field1096[++class85.field1105 - 1] = string_10;
                     }

                     return 1;
                  } else if (i_0 == 4111) {
                     string_4 = class85.field1096[--class85.field1105];
                     class85.field1096[++class85.field1105 - 1] = class306.method5386(string_4);
                     return 1;
                  } else if (i_0 == 4112) {
                     string_4 = class85.field1096[--class85.field1105];
                     i_5 = class85.field1095[--class253.field3267];
                     class85.field1096[++class85.field1105 - 1] = string_4 + (char)i_5;
                     return 1;
                  } else if (i_0 == 4113) {
                     i_11 = class85.field1095[--class253.field3267];
                     class85.field1095[++class253.field3267 - 1] = class291.method5166((char)i_11, 538675617) ? 1 : 0;
                     return 1;
                  } else if (i_0 == 4114) {
                     i_11 = class85.field1095[--class253.field3267];
                     class85.field1095[++class253.field3267 - 1] = class296.method5249((char)i_11, -1404693123) ? 1 : 0;
                     return 1;
                  } else if (i_0 == 4115) {
                     i_11 = class85.field1095[--class253.field3267];
                     class85.field1095[++class253.field3267 - 1] = class247.method4307((char)i_11, (byte) -111) ? 1 : 0;
                     return 1;
                  } else if (i_0 == 4116) {
                     i_11 = class85.field1095[--class253.field3267];
                     class85.field1095[++class253.field3267 - 1] = class39.method592((char)i_11, 724537409) ? 1 : 0;
                     return 1;
                  } else if (i_0 == 4117) {
                     string_4 = class85.field1096[--class85.field1105];
                     if (string_4 != null) {
                        class85.field1095[++class253.field3267 - 1] = string_4.length();
                     } else {
                        class85.field1095[++class253.field3267 - 1] = 0;
                     }

                     return 1;
                  } else if (i_0 == 4118) {
                     string_4 = class85.field1096[--class85.field1105];
                     class253.field3267 -= 2;
                     i_5 = class85.field1095[class253.field3267];
                     i_6 = class85.field1095[class253.field3267 + 1];
                     class85.field1096[++class85.field1105 - 1] = string_4.substring(i_5, i_6);
                     return 1;
                  } else if (i_0 == 4119) {
                     string_4 = class85.field1096[--class85.field1105];
                     StringBuilder stringbuilder_18 = new StringBuilder(string_4.length());
                     boolean bool_16 = false;

                     for (i_7 = 0; i_7 < string_4.length(); i_7++) {
                        char var_8 = string_4.charAt(i_7);
                        if (var_8 == 60) {
                           bool_16 = true;
                        } else if (var_8 == 62) {
                           bool_16 = false;
                        } else if (!bool_16) {
                           stringbuilder_18.append(var_8);
                        }
                     }

                     class85.field1096[++class85.field1105 - 1] = stringbuilder_18.toString();
                     return 1;
                  } else if (i_0 == 4120) {
                     string_4 = class85.field1096[--class85.field1105];
                     i_5 = class85.field1095[--class253.field3267];
                     class85.field1095[++class253.field3267 - 1] = string_4.indexOf(i_5);
                     return 1;
                  } else if (i_0 == 4121) {
                     class85.field1105 -= 2;
                     string_4 = class85.field1096[class85.field1105];
                     string_10 = class85.field1096[class85.field1105 + 1];
                     i_6 = class85.field1095[--class253.field3267];
                     class85.field1095[++class253.field3267 - 1] = string_4.indexOf(string_10, i_6);
                     return 1;
                  } else if (i_0 == 4122) {
                     string_4 = class85.field1096[--class85.field1105];
                     class85.field1096[++class85.field1105 - 1] = string_4.toUpperCase();
                     return 1;
                  } else {
                     return 2;
                  }
               }
            } else {
               class85.field1105 -= 2;
               string_4 = class85.field1096[class85.field1105];
               string_10 = class85.field1096[class85.field1105 + 1];
               if (class223.field2562.field621 != null && class223.field2562.field621.field2552) {
                  class85.field1096[++class85.field1105 - 1] = string_10;
               } else {
                  class85.field1096[++class85.field1105 - 1] = string_4;
               }

               return 1;
            }
         }
      }
   }

}
