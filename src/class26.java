public class class26 {

   static class305 field137;
   static int field140;
   public static final class26 field131 = new class26(2, 0, 4);
   public static final class26 field134 = new class26(1, 1, 2);
   public static final class26 field130 = new class26(0, 2, 0);
   final int field142;
   final int field133;
   final int field135;

   class26(int i_1, int i_2, int i_3) {
      this.field142 = i_1;
      this.field133 = i_2;
      this.field135 = i_3;
   }

   boolean method254(float f_1, byte b_2) {
      return f_1 >= (float)this.field135;
   }

   static final int method255(int i_0, int i_1, int i_2, byte b_3) {
      if (i_2 > 179) {
         i_1 /= 2;
      }

      if (i_2 > 192) {
         i_1 /= 2;
      }

      if (i_2 > 217) {
         i_1 /= 2;
      }

      if (i_2 > 243) {
         i_1 /= 2;
      }

      int i_4 = (i_1 / 32 << 7) + (i_0 / 4 << 10) + i_2 / 2;
      return i_4;
   }

}
