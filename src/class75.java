public final class class75 extends class78 {

   static int field630;
   static int field637;
   int field634;
   int field617;
   int field613;
   int field626;
   int field627;
   int field628;
   int field620;
   int field606;
   int field622;
   int field623;
   int field632;
   int field633;
   int field608 = -1;
   int field609 = -1;
   String[] field610 = new String[3];
   int field611;
   int field612;
   int field614;
   int field615;
   boolean field624;
   int field616;
   boolean field625;
   class291 field629;
   class291 field618;
   boolean field631;
   class136 field607;
   class293 field619;
   class223 field621;

   class75() {
      for (int i_1 = 0; i_1 < 3; i_1++) {
         this.field610[i_1] = "";
      }

      this.field611 = 0;
      this.field612 = 0;
      this.field614 = 0;
      this.field615 = 0;
      this.field624 = false;
      this.field616 = 0;
      this.field625 = false;
      this.field629 = class291.field3653;
      this.field618 = class291.field3653;
      this.field631 = false;
   }

   protected final class136 vmethod3305(int i_1) {
      if (this.field621 == null) {
         return null;
      } else {
         class269 class269_2 = super.field970 != -1 && super.field937 == 0 ? class260.method4510(super.field970, -1739616604) : null;
         class269 class269_3 = super.field967 != -1 && !this.field624 && (super.field943 != super.field967 || class269_2 == null) ? class260.method4510(super.field967, -1874764337) : null;
         class136 class136_4 = this.field621.method4014(class269_2, super.field971, class269_3, super.field964, -2063173617);
         if (class136_4 == null) {
            return null;
         } else {
            class136_4.method2946();
            super.field988 = class136_4.field1894;
            class136 class136_5;
            class136[] arr_6;
            if (!this.field624 && super.field975 != -1 && super.field976 != -1) {
               class136_5 = class96.method2160(super.field975, (byte) 42).method4411(super.field976, -888239890);
               if (class136_5 != null) {
                  class136_5.method2923(0, -super.field979, 0);
                  arr_6 = new class136[] {class136_4, class136_5};
                  class136_4 = new class136(arr_6, 2);
               }
            }

            if (!this.field624 && this.field607 != null) {
               if (client.field655 >= this.field615) {
                  this.field607 = null;
               }

               if (client.field655 >= this.field614 && client.field655 < this.field615) {
                  class136_5 = this.field607;
                  class136_5.method2923(this.field634 - super.field990, this.field617 - this.field613, this.field626 - super.field938);
                  if (super.field989 == 512) {
                     class136_5.method2945();
                     class136_5.method2945();
                     class136_5.method2945();
                  } else if (super.field989 == 1024) {
                     class136_5.method2945();
                     class136_5.method2945();
                  } else if (super.field989 == 1536) {
                     class136_5.method2945();
                  }

                  arr_6 = new class136[] {class136_4, class136_5};
                  class136_4 = new class136(arr_6, 2);
                  if (super.field989 == 512) {
                     class136_5.method2945();
                  } else if (super.field989 == 1024) {
                     class136_5.method2945();
                     class136_5.method2945();
                  } else if (super.field989 == 1536) {
                     class136_5.method2945();
                     class136_5.method2945();
                     class136_5.method2945();
                  }

                  class136_5.method2923(super.field990 - this.field634, this.field613 - this.field617, super.field938 - this.field626);
               }
            }

            class136_4.field1710 = true;
            return class136_4;
         }
      }
   }

   void method1312(int i_1) {
      this.field618 = class35.field244 != null && class35.field244.method5073(this.field619, (byte) 1) ? class291.field3652 : class291.field3654;
   }

   void method1307(int i_1) {
      this.field629 = class173.field2036.method1905(this.field619, -1294359433) ? class291.field3652 : class291.field3654;
   }

   void method1314(int i_1, int i_2, int i_3) {
      super.field985 = 0;
      super.field996 = 0;
      super.field963 = 0;
      super.field993[0] = i_1;
      super.field968[0] = i_2;
      int i_4 = this.method1311((byte) -57);
      super.field990 = i_4 * 64 + super.field993[0] * 128;
      super.field938 = super.field968[0] * 128 + i_4 * 64;
   }

   int method1311(byte b_1) {
      return this.field621 != null && this.field621.field2553 != -1 ? class27.method272(this.field621.field2553, -1636100587).field3500 : 1;
   }

   void method1305(int i_1) {
      this.field629 = class291.field3653;
   }

   final void method1315(int i_1, int i_2, byte b_3, int i_4) {
      if (super.field985 < 9) {
         ++super.field985;
      }

      for (int i_5 = super.field985; i_5 > 0; --i_5) {
         super.field993[i_5] = super.field993[i_5 - 1];
         super.field968[i_5] = super.field968[i_5 - 1];
         super.field995[i_5] = super.field995[i_5 - 1];
      }

      super.field993[0] = i_1;
      super.field968[0] = i_2;
      super.field995[0] = b_3;
   }

   void method1309(int i_1) {
      this.field618 = class291.field3653;
   }

   final boolean vmethod2080(int i_1) {
      return this.field621 != null;
   }

   boolean method1331(int i_1) {
      if (this.field629 == class291.field3653) {
         this.method1307(886706251);
      }

      return this.field629 == class291.field3652;
   }

   boolean method1330(byte b_1) {
      if (this.field618 == class291.field3653) {
         this.method1312(327273294);
      }

      return this.field618 == class291.field3652;
   }

   final void method1313(int i_1, int i_2, byte b_3, int i_4) {
      if (super.field970 != -1 && class260.method4510(super.field970, -1971166819).field3555 == 1) {
         super.field970 = -1;
      }

      super.field959 = -1;
      if (i_1 >= 0 && i_1 < 104 && i_2 >= 0 && i_2 < 104) {
         if (super.field993[0] >= 0 && super.field993[0] < 104 && super.field968[0] >= 0 && super.field968[0] < 104) {
            if (b_3 == 2) {
               class75 class75_5 = this;
               int i_6 = super.field993[0];
               int i_7 = super.field968[0];
               int i_8 = this.method1311((byte) -50);
               if (i_6 >= i_8 && i_6 < 104 - i_8 && i_7 >= i_8 && i_7 < 104 - i_8 && i_1 >= i_8 && i_1 < 104 - i_8 && i_2 >= i_8 && i_2 < 104 - i_8) {
                  int i_10 = this.method1311((byte) 55);
                  client.field919.field2125 = i_1;
                  client.field919.field2123 = i_2;
                  client.field919.field2122 = 1;
                  client.field919.field2127 = 1;
                  class74 class74_11 = client.field919;
                  class74 class74_12 = class74_11;
                  class181 class181_13 = client.field831[this.field627];
                  int[] ints_14 = client.field920;
                  int[] ints_15 = client.field921;
                  int i_16 = 0;

                  label726:
                  while (true) {
                     int i_17;
                     if (i_16 >= 128) {
                        int i_18;
                        int i_19;
                        byte b_20;
                        byte b_21;
                        int i_22;
                        int i_23;
                        byte b_24;
                        int i_25;
                        int[][] ints_26;
                        int i_27;
                        int i_28;
                        int i_29;
                        int i_30;
                        boolean bool_36;
                        boolean bool_37;
                        int i_38;
                        int i_39;
                        int i_41;
                        if (i_10 == 1) {
                           i_18 = i_6;
                           i_19 = i_7;
                           b_20 = 64;
                           b_21 = 64;
                           i_22 = i_6 - b_20;
                           i_23 = i_7 - b_21;
                           class182.field2111[b_20][b_21] = 99;
                           class182.field2114[b_20][b_21] = 0;
                           b_24 = 0;
                           i_25 = 0;
                           class182.field2118[b_24] = i_6;
                           i_41 = b_24 + 1;
                           class182.field2119[b_24] = i_7;
                           ints_26 = class181_13.field2107;

                           while (true) {
                              if (i_25 == i_41) {
                                 class182.field2115 = i_18;
                                 class182.field2121 = i_19;
                                 bool_37 = false;
                                 break;
                              }

                              i_18 = class182.field2118[i_25];
                              i_19 = class182.field2119[i_25];
                              i_25 = i_25 + 1 & 0xfff;
                              i_38 = i_18 - i_22;
                              i_39 = i_19 - i_23;
                              i_27 = i_18 - class181_13.field2103;
                              i_28 = i_19 - class181_13.field2104;
                              if (class74_12.vmethod3613(1, i_18, i_19, class181_13, (byte) 104)) {
                                 class182.field2115 = i_18;
                                 class182.field2121 = i_19;
                                 bool_37 = true;
                                 break;
                              }

                              i_29 = class182.field2114[i_38][i_39] + 1;
                              if (i_38 > 0 && class182.field2111[i_38 - 1][i_39] == 0 && (ints_26[i_27 - 1][i_28] & 0x1240108) == 0) {
                                 class182.field2118[i_41] = i_18 - 1;
                                 class182.field2119[i_41] = i_19;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 - 1][i_39] = 2;
                                 class182.field2114[i_38 - 1][i_39] = i_29;
                              }

                              if (i_38 < 127 && class182.field2111[i_38 + 1][i_39] == 0 && (ints_26[i_27 + 1][i_28] & 0x1240180) == 0) {
                                 class182.field2118[i_41] = i_18 + 1;
                                 class182.field2119[i_41] = i_19;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 + 1][i_39] = 8;
                                 class182.field2114[i_38 + 1][i_39] = i_29;
                              }

                              if (i_39 > 0 && class182.field2111[i_38][i_39 - 1] == 0 && (ints_26[i_27][i_28 - 1] & 0x1240102) == 0) {
                                 class182.field2118[i_41] = i_18;
                                 class182.field2119[i_41] = i_19 - 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38][i_39 - 1] = 1;
                                 class182.field2114[i_38][i_39 - 1] = i_29;
                              }

                              if (i_39 < 127 && class182.field2111[i_38][i_39 + 1] == 0 && (ints_26[i_27][i_28 + 1] & 0x1240120) == 0) {
                                 class182.field2118[i_41] = i_18;
                                 class182.field2119[i_41] = i_19 + 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38][i_39 + 1] = 4;
                                 class182.field2114[i_38][i_39 + 1] = i_29;
                              }

                              if (i_38 > 0 && i_39 > 0 && class182.field2111[i_38 - 1][i_39 - 1] == 0 && (ints_26[i_27 - 1][i_28 - 1] & 0x124010e) == 0 && (ints_26[i_27 - 1][i_28] & 0x1240108) == 0 && (ints_26[i_27][i_28 - 1] & 0x1240102) == 0) {
                                 class182.field2118[i_41] = i_18 - 1;
                                 class182.field2119[i_41] = i_19 - 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 - 1][i_39 - 1] = 3;
                                 class182.field2114[i_38 - 1][i_39 - 1] = i_29;
                              }

                              if (i_38 < 127 && i_39 > 0 && class182.field2111[i_38 + 1][i_39 - 1] == 0 && (ints_26[i_27 + 1][i_28 - 1] & 0x1240183) == 0 && (ints_26[i_27 + 1][i_28] & 0x1240180) == 0 && (ints_26[i_27][i_28 - 1] & 0x1240102) == 0) {
                                 class182.field2118[i_41] = i_18 + 1;
                                 class182.field2119[i_41] = i_19 - 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 + 1][i_39 - 1] = 9;
                                 class182.field2114[i_38 + 1][i_39 - 1] = i_29;
                              }

                              if (i_38 > 0 && i_39 < 127 && class182.field2111[i_38 - 1][i_39 + 1] == 0 && (ints_26[i_27 - 1][i_28 + 1] & 0x1240138) == 0 && (ints_26[i_27 - 1][i_28] & 0x1240108) == 0 && (ints_26[i_27][i_28 + 1] & 0x1240120) == 0) {
                                 class182.field2118[i_41] = i_18 - 1;
                                 class182.field2119[i_41] = i_19 + 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 - 1][i_39 + 1] = 6;
                                 class182.field2114[i_38 - 1][i_39 + 1] = i_29;
                              }

                              if (i_38 < 127 && i_39 < 127 && class182.field2111[i_38 + 1][i_39 + 1] == 0 && (ints_26[i_27 + 1][i_28 + 1] & 0x12401e0) == 0 && (ints_26[i_27 + 1][i_28] & 0x1240180) == 0 && (ints_26[i_27][i_28 + 1] & 0x1240120) == 0) {
                                 class182.field2118[i_41] = i_18 + 1;
                                 class182.field2119[i_41] = i_19 + 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 + 1][i_39 + 1] = 12;
                                 class182.field2114[i_38 + 1][i_39 + 1] = i_29;
                              }
                           }

                           bool_36 = bool_37;
                        } else if (i_10 == 2) {
                           bool_36 = class231.method4119(i_6, i_7, class74_11, class181_13, (short) -4762);
                        } else {
                           i_18 = i_6;
                           i_19 = i_7;
                           b_20 = 64;
                           b_21 = 64;
                           i_22 = i_6 - b_20;
                           i_23 = i_7 - b_21;
                           class182.field2111[b_20][b_21] = 99;
                           class182.field2114[b_20][b_21] = 0;
                           b_24 = 0;
                           i_25 = 0;
                           class182.field2118[b_24] = i_6;
                           i_41 = b_24 + 1;
                           class182.field2119[b_24] = i_7;
                           ints_26 = class181_13.field2107;

                           label704:
                           while (true) {
                              label702:
                              while (true) {
                                 do {
                                    do {
                                       do {
                                          label679:
                                          do {
                                             if (i_25 == i_41) {
                                                class182.field2115 = i_18;
                                                class182.field2121 = i_19;
                                                bool_37 = false;
                                                break label704;
                                             }

                                             i_18 = class182.field2118[i_25];
                                             i_19 = class182.field2119[i_25];
                                             i_25 = i_25 + 1 & 0xfff;
                                             i_38 = i_18 - i_22;
                                             i_39 = i_19 - i_23;
                                             i_27 = i_18 - class181_13.field2103;
                                             i_28 = i_19 - class181_13.field2104;
                                             if (class74_12.vmethod3613(i_10, i_18, i_19, class181_13, (byte) 104)) {
                                                class182.field2115 = i_18;
                                                class182.field2121 = i_19;
                                                bool_37 = true;
                                                break label704;
                                             }

                                             i_29 = class182.field2114[i_38][i_39] + 1;
                                             if (i_38 > 0 && class182.field2111[i_38 - 1][i_39] == 0 && (ints_26[i_27 - 1][i_28] & 0x124010e) == 0 && (ints_26[i_27 - 1][i_28 + i_10 - 1] & 0x1240138) == 0) {
                                                i_30 = 1;

                                                while (true) {
                                                   if (i_30 >= i_10 - 1) {
                                                      class182.field2118[i_41] = i_18 - 1;
                                                      class182.field2119[i_41] = i_19;
                                                      i_41 = i_41 + 1 & 0xfff;
                                                      class182.field2111[i_38 - 1][i_39] = 2;
                                                      class182.field2114[i_38 - 1][i_39] = i_29;
                                                      break;
                                                   }

                                                   if ((ints_26[i_27 - 1][i_30 + i_28] & 0x124013e) != 0) {
                                                      break;
                                                   }

                                                   ++i_30;
                                                }
                                             }

                                             if (i_38 < 128 - i_10 && class182.field2111[i_38 + 1][i_39] == 0 && (ints_26[i_10 + i_27][i_28] & 0x1240183) == 0 && (ints_26[i_27 + i_10][i_10 + i_28 - 1] & 0x12401e0) == 0) {
                                                i_30 = 1;

                                                while (true) {
                                                   if (i_30 >= i_10 - 1) {
                                                      class182.field2118[i_41] = i_18 + 1;
                                                      class182.field2119[i_41] = i_19;
                                                      i_41 = i_41 + 1 & 0xfff;
                                                      class182.field2111[i_38 + 1][i_39] = 8;
                                                      class182.field2114[i_38 + 1][i_39] = i_29;
                                                      break;
                                                   }

                                                   if ((ints_26[i_27 + i_10][i_28 + i_30] & 0x12401e3) != 0) {
                                                      break;
                                                   }

                                                   ++i_30;
                                                }
                                             }

                                             if (i_39 > 0 && class182.field2111[i_38][i_39 - 1] == 0 && (ints_26[i_27][i_28 - 1] & 0x124010e) == 0 && (ints_26[i_10 + i_27 - 1][i_28 - 1] & 0x1240183) == 0) {
                                                i_30 = 1;

                                                while (true) {
                                                   if (i_30 >= i_10 - 1) {
                                                      class182.field2118[i_41] = i_18;
                                                      class182.field2119[i_41] = i_19 - 1;
                                                      i_41 = i_41 + 1 & 0xfff;
                                                      class182.field2111[i_38][i_39 - 1] = 1;
                                                      class182.field2114[i_38][i_39 - 1] = i_29;
                                                      break;
                                                   }

                                                   if ((ints_26[i_27 + i_30][i_28 - 1] & 0x124018f) != 0) {
                                                      break;
                                                   }

                                                   ++i_30;
                                                }
                                             }

                                             if (i_39 < 128 - i_10 && class182.field2111[i_38][i_39 + 1] == 0 && (ints_26[i_27][i_28 + i_10] & 0x1240138) == 0 && (ints_26[i_10 + i_27 - 1][i_10 + i_28] & 0x12401e0) == 0) {
                                                i_30 = 1;

                                                while (true) {
                                                   if (i_30 >= i_10 - 1) {
                                                      class182.field2118[i_41] = i_18;
                                                      class182.field2119[i_41] = i_19 + 1;
                                                      i_41 = i_41 + 1 & 0xfff;
                                                      class182.field2111[i_38][i_39 + 1] = 4;
                                                      class182.field2114[i_38][i_39 + 1] = i_29;
                                                      break;
                                                   }

                                                   if ((ints_26[i_30 + i_27][i_10 + i_28] & 0x12401f8) != 0) {
                                                      break;
                                                   }

                                                   ++i_30;
                                                }
                                             }

                                             if (i_38 > 0 && i_39 > 0 && class182.field2111[i_38 - 1][i_39 - 1] == 0 && (ints_26[i_27 - 1][i_28 - 1] & 0x124010e) == 0) {
                                                i_30 = 1;

                                                while (true) {
                                                   if (i_30 >= i_10) {
                                                      class182.field2118[i_41] = i_18 - 1;
                                                      class182.field2119[i_41] = i_19 - 1;
                                                      i_41 = i_41 + 1 & 0xfff;
                                                      class182.field2111[i_38 - 1][i_39 - 1] = 3;
                                                      class182.field2114[i_38 - 1][i_39 - 1] = i_29;
                                                      break;
                                                   }

                                                   if ((ints_26[i_27 - 1][i_30 + (i_28 - 1)] & 0x124013e) != 0 || (ints_26[i_30 + (i_27 - 1)][i_28 - 1] & 0x124018f) != 0) {
                                                      break;
                                                   }

                                                   ++i_30;
                                                }
                                             }

                                             if (i_38 < 128 - i_10 && i_39 > 0 && class182.field2111[i_38 + 1][i_39 - 1] == 0 && (ints_26[i_27 + i_10][i_28 - 1] & 0x1240183) == 0) {
                                                i_30 = 1;

                                                while (true) {
                                                   if (i_30 >= i_10) {
                                                      class182.field2118[i_41] = i_18 + 1;
                                                      class182.field2119[i_41] = i_19 - 1;
                                                      i_41 = i_41 + 1 & 0xfff;
                                                      class182.field2111[i_38 + 1][i_39 - 1] = 9;
                                                      class182.field2114[i_38 + 1][i_39 - 1] = i_29;
                                                      break;
                                                   }

                                                   if ((ints_26[i_27 + i_10][i_30 + (i_28 - 1)] & 0x12401e3) != 0 || (ints_26[i_27 + i_30][i_28 - 1] & 0x124018f) != 0) {
                                                      break;
                                                   }

                                                   ++i_30;
                                                }
                                             }

                                             if (i_38 > 0 && i_39 < 128 - i_10 && class182.field2111[i_38 - 1][i_39 + 1] == 0 && (ints_26[i_27 - 1][i_10 + i_28] & 0x1240138) == 0) {
                                                for (i_30 = 1; i_30 < i_10; i_30++) {
                                                   if ((ints_26[i_27 - 1][i_30 + i_28] & 0x124013e) != 0 || (ints_26[i_30 + (i_27 - 1)][i_28 + i_10] & 0x12401f8) != 0) {
                                                      continue label679;
                                                   }
                                                }

                                                class182.field2118[i_41] = i_18 - 1;
                                                class182.field2119[i_41] = i_19 + 1;
                                                i_41 = i_41 + 1 & 0xfff;
                                                class182.field2111[i_38 - 1][i_39 + 1] = 6;
                                                class182.field2114[i_38 - 1][i_39 + 1] = i_29;
                                             }
                                          } while (i_38 >= 128 - i_10);
                                       } while (i_39 >= 128 - i_10);
                                    } while (class182.field2111[i_38 + 1][i_39 + 1] != 0);
                                 } while ((ints_26[i_10 + i_27][i_10 + i_28] & 0x12401e0) != 0);

                                 for (i_30 = 1; i_30 < i_10; i_30++) {
                                    if ((ints_26[i_27 + i_30][i_10 + i_28] & 0x12401f8) != 0 || (ints_26[i_10 + i_27][i_30 + i_28] & 0x12401e3) != 0) {
                                       continue label702;
                                    }
                                 }

                                 class182.field2118[i_41] = i_18 + 1;
                                 class182.field2119[i_41] = i_19 + 1;
                                 i_41 = i_41 + 1 & 0xfff;
                                 class182.field2111[i_38 + 1][i_39 + 1] = 12;
                                 class182.field2114[i_38 + 1][i_39 + 1] = i_29;
                              }
                           }

                           bool_36 = bool_37;
                        }

                        int i_9;
                        label767: {
                           i_17 = i_6 - 64;
                           i_18 = i_7 - 64;
                           i_19 = class182.field2115;
                           i_38 = class182.field2121;
                           if (!bool_36) {
                              i_39 = Integer.MAX_VALUE;
                              i_22 = Integer.MAX_VALUE;
                              byte b_40 = 10;
                              i_41 = class74_12.field2125;
                              i_25 = class74_12.field2123;
                              int i_35 = class74_12.field2122;
                              i_27 = class74_12.field2127;

                              for (i_28 = i_41 - b_40; i_28 <= b_40 + i_41; i_28++) {
                                 for (i_29 = i_25 - b_40; i_29 <= b_40 + i_25; i_29++) {
                                    i_30 = i_28 - i_17;
                                    int i_31 = i_29 - i_18;
                                    if (i_30 >= 0 && i_31 >= 0 && i_30 < 128 && i_31 < 128 && class182.field2114[i_30][i_31] < 100) {
                                       int i_32 = 0;
                                       if (i_28 < i_41) {
                                          i_32 = i_41 - i_28;
                                       } else if (i_28 > i_41 + i_35 - 1) {
                                          i_32 = i_28 - (i_41 + i_35 - 1);
                                       }

                                       int i_33 = 0;
                                       if (i_29 < i_25) {
                                          i_33 = i_25 - i_29;
                                       } else if (i_29 > i_27 + i_25 - 1) {
                                          i_33 = i_29 - (i_25 + i_27 - 1);
                                       }

                                       int i_34 = i_32 * i_32 + i_33 * i_33;
                                       if (i_34 < i_39 || i_39 == i_34 && class182.field2114[i_30][i_31] < i_22) {
                                          i_39 = i_34;
                                          i_22 = class182.field2114[i_30][i_31];
                                          i_19 = i_28;
                                          i_38 = i_29;
                                       }
                                    }
                                 }
                              }

                              if (i_39 == Integer.MAX_VALUE) {
                                 i_9 = -1;
                                 break label767;
                              }
                           }

                           if (i_19 == i_6 && i_38 == i_7) {
                              i_9 = 0;
                           } else {
                              b_21 = 0;
                              class182.field2118[b_21] = i_19;
                              i_39 = b_21 + 1;
                              class182.field2119[b_21] = i_38;

                              for (i_22 = i_23 = class182.field2111[i_19 - i_17][i_38 - i_18]; i_19 != i_6 || i_38 != i_7; i_22 = class182.field2111[i_19 - i_17][i_38 - i_18]) {
                                 if (i_23 != i_22) {
                                    i_23 = i_22;
                                    class182.field2118[i_39] = i_19;
                                    class182.field2119[i_39++] = i_38;
                                 }

                                 if ((i_22 & 0x2) != 0) {
                                    ++i_19;
                                 } else if ((i_22 & 0x8) != 0) {
                                    --i_19;
                                 }

                                 if ((i_22 & 0x1) != 0) {
                                    ++i_38;
                                 } else if ((i_22 & 0x4) != 0) {
                                    --i_38;
                                 }
                              }

                              i_41 = 0;

                              while (i_39-- > 0) {
                                 ints_14[i_41] = class182.field2118[i_39];
                                 ints_15[i_41++] = class182.field2119[i_39];
                                 if (i_41 >= ints_14.length) {
                                    break;
                                 }
                              }

                              i_9 = i_41;
                           }
                        }

                        i_16 = i_9;
                        if (i_9 < 1) {
                           break;
                        }

                        i_17 = 0;

                        while (true) {
                           if (i_17 >= i_16 - 1) {
                              break label726;
                           }

                           class75_5.method1315(client.field920[i_17], client.field921[i_17], (byte) 2, -779239850);
                           ++i_17;
                        }
                     }

                     for (i_17 = 0; i_17 < 128; i_17++) {
                        class182.field2111[i_16][i_17] = 0;
                        class182.field2114[i_16][i_17] = 99999999;
                     }

                     ++i_16;
                  }
               }
            }

            this.method1315(i_1, i_2, b_3, -52932388);
         } else {
            this.method1314(i_1, i_2, -555008596);
         }
      } else {
         this.method1314(i_1, i_2, 1335034021);
      }

   }

   final void method1334(class310 class310_1, byte b_2) {
      class310_1.field3751 = 0;
      int i_3 = class310_1.method5661((byte) 50);
      this.field608 = class310_1.method5535((byte) 0);
      this.field609 = class310_1.method5535((byte) 0);
      int i_4 = -1;
      this.field616 = 0;
      int[] ints_5 = new int[12];

      int i_7;
      int i_8;
      for (int i_6 = 0; i_6 < 12; i_6++) {
         i_7 = class310_1.method5661((byte) -67);
         if (i_7 == 0) {
            ints_5[i_6] = 0;
         } else {
            i_8 = class310_1.method5661((byte) 51);
            ints_5[i_6] = i_8 + (i_7 << 8);
            if (i_6 == 0 && ints_5[0] == 65535) {
               i_4 = class310_1.method5729(-582226848);
               break;
            }

            if (ints_5[i_6] >= 512) {
               int i_9 = class66.method1223(ints_5[i_6] - 512, -668396449).field3482;
               if (i_9 != 0) {
                  this.field616 = i_9;
               }
            }
         }
      }

      int[] ints_10 = new int[5];

      for (i_7 = 0; i_7 < 5; i_7++) {
         i_8 = class310_1.method5661((byte) 67);
         if (i_8 < 0 || i_8 >= class49.field366[i_7].length) {
            i_8 = 0;
         }

         ints_10[i_7] = i_8;
      }

      super.field943 = class310_1.method5729(90811185);
      if (super.field943 == 65535) {
         super.field943 = -1;
      }

      super.field944 = class310_1.method5729(2118124813);
      if (super.field944 == 65535) {
         super.field944 = -1;
      }

      super.field945 = super.field944;
      super.field946 = class310_1.method5729(-912864098);
      if (super.field946 == 65535) {
         super.field946 = -1;
      }

      super.field947 = class310_1.method5729(-2084455474);
      if (super.field947 == 65535) {
         super.field947 = -1;
      }

      super.field948 = class310_1.method5729(-813368510);
      if (super.field948 == 65535) {
         super.field948 = -1;
      }

      super.field949 = class310_1.method5729(-1567927572);
      if (super.field949 == 65535) {
         super.field949 = -1;
      }

      super.field942 = class310_1.method5729(-1682562441);
      if (super.field942 == 65535) {
         super.field942 = -1;
      }

      this.field619 = new class293(class310_1.method5589(-1564120906), class19.field92);
      this.method1305(-730921547);
      this.method1309(154647913);
      if (this == class223.field2562) {
         class351.field4082 = this.field619.method5186(-1145738754);
      }

      this.field611 = class310_1.method5661((byte) 3);
      this.field612 = class310_1.method5729(-1626476292);
      this.field625 = class310_1.method5661((byte) -25) == 1;
      if (client.field647 == 0 && client.field775 >= 2) {
         this.field625 = false;
      }

      if (this.field621 == null) {
         this.field621 = new class223();
      }

      this.field621.method4008(ints_5, ints_10, i_3 == 1, i_4, 1899379562);
   }

   static final void method1303(class226 class226_0, int i_1, int i_2, int i_3, int i_4) {
      if (class226_0.field2679 == null) {
         throw new RuntimeException();
      } else {
         class226_0.field2679[i_1] = i_2;
         class226_0.field2664[i_1] = i_3;
      }
   }

   static class226 method1351(class226 class226_0, int i_1) {
      class226 class226_3 = class226_0;
      int i_5 = class146.method3318(class226_0, 2016145076);
      int i_4 = i_5 >> 17 & 0x7;
      int i_6 = i_4;
      class226 class226_2;
      if (i_4 == 0) {
         class226_2 = null;
      } else {
         int i_7 = 0;

         while (true) {
            if (i_7 >= i_6) {
               class226_2 = class226_3;
               break;
            }

            class226_3 = class181.method3610(class226_3.field2670, -1999947569);
            if (class226_3 == null) {
               class226_2 = null;
               break;
            }

            ++i_7;
         }
      }

      class226 class226_8 = class226_2;
      if (class226_2 == null) {
         class226_8 = class226_0.field2667;
      }

      return class226_8;
   }

}
