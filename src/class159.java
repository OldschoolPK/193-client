import java.lang.ref.SoftReference;

public class class159 extends class157 {

   SoftReference field1985;

   class159(Object object_1, int i_2) {
      super(i_2);
      this.field1985 = new SoftReference(object_1);
   }

   boolean vmethod3426() {
      return true;
   }

   Object vmethod3427() {
      return this.field1985.get();
   }

}
