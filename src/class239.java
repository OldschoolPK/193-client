public class class239 implements class203 {

   static final class239 field3125 = new class239(0, -1, true, false, true);
   static final class239 field3128 = new class239(1, 0, true, true, true);
   static final class239 field3118 = new class239(2, 1, true, true, false);
   static final class239 field3121 = new class239(3, 2, false, false, true);
   static final class239 field3122 = new class239(4, 3, false, false, true);
   static final class239 field3127 = new class239(5, 10, false, false, true);
   static final class239 field3124 = new class239(6, 22, false, false, true);
   final int field3119;
   public final int field3126;
   public final boolean field3120;
   public final boolean field3123;

   class239(int i_1, int i_2, boolean bool_3, boolean bool_4, boolean bool_5) {
      this.field3119 = i_1;
      this.field3126 = i_2;
      this.field3120 = bool_4;
      this.field3123 = bool_5;
   }

   public int vmethod6086(int i_1) {
      return this.field3119;
   }

   static class335 method4137(int i_0, int i_1, int i_2, int i_3) {
      class158 class158_4 = class38.field273;
      long long_5 = (long)(i_2 << 16 | i_0 << 8 | i_1);
      return (class335) class158_4.method3422(long_5);
   }

}
