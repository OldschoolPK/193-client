import java.util.zip.CRC32;

public class class246 extends class244 {

   static CRC32 field3186 = new CRC32();
   volatile boolean field3185 = false;
   boolean field3192 = false;
   int field3191 = -1;
   class318 field3188;
   class318 field3182;
   int field3183;
   volatile boolean[] field3187;
   int field3189;
   int field3190;

   public class246(class318 class318_1, class318 class318_2, int i_3, boolean bool_4, boolean bool_5, boolean bool_6) {
      super(bool_4, bool_5);
      this.field3188 = class318_1;
      this.field3182 = class318_2;
      this.field3183 = i_3;
      this.field3192 = bool_6;
      int i_8 = this.field3183;
      if (class37.field259 != null) {
         class37.field259.field3751 = i_8 * 8 + 5;
         int i_9 = class37.field259.method5507(-1011560661);
         int i_10 = class37.field259.method5507(1555932812);
         this.method4266(i_9, i_10, 973305795);
      } else {
         class226.method4085((class246) null, 255, 255, 0, (byte) 0, true, 1993463107);
         class247.field3209[i_8] = this;
      }

   }

   void vmethod4279(int i_1, int i_2) {
      if (this.field3188 != null && this.field3187 != null && this.field3187[i_1]) {
         class318 class318_3 = this.field3188;
         byte[] bytes_5 = null;
         class272 class272_6 = class245.field3177;
         synchronized(class245.field3177) {
            for (class242 class242_7 = (class242) class245.field3177.method4879(); class242_7 != null; class242_7 = (class242) class245.field3177.method4884()) {
               if (class242_7.field2137 == (long)i_1 && class318_3 == class242_7.field3146 && class242_7.field3149 == 0) {
                  bytes_5 = class242_7.field3145;
                  break;
               }
            }
         }

         if (bytes_5 != null) {
            this.method4274(class318_3, i_1, bytes_5, true, 1026472902);
         } else {
            byte[] bytes_10 = class318_3.method5864(i_1, -1489212511);
            this.method4274(class318_3, i_1, bytes_10, true, 973929783);
         }
      } else {
         class226.method4085(this, this.field3183, i_1, super.field3163[i_1], (byte) 2, true, -388750473);
      }

   }

   public boolean method4272(int i_1, byte b_2) {
      return this.method4171(i_1, 1636489004) != null;
   }

   int vmethod4270(int i_1, int i_2) {
      if (super.field3169[i_1] != null) {
         return 100;
      } else if (this.field3187[i_1]) {
         return 100;
      } else {
         int i_4 = this.field3183;
         long long_5 = (long)((i_4 << 16) + i_1);
         int i_3;
         if (class247.field3202 != null && class247.field3202.field2137 == long_5) {
            i_3 = class206.field2427.field3751 * 99 / (class206.field2427.field3752.length - class247.field3202.field3153) + 1;
         } else {
            i_3 = 0;
         }

         return i_3;
      }
   }

   void method4269(int i_1) {
      this.field3187 = new boolean[super.field3169.length];

      int i_2;
      for (i_2 = 0; i_2 < this.field3187.length; i_2++) {
         this.field3187[i_2] = false;
      }

      if (this.field3188 == null) {
         this.field3185 = true;
      } else {
         this.field3191 = -1;

         for (i_2 = 0; i_2 < this.field3187.length; i_2++) {
            if (super.field3165[i_2] > 0) {
               class230.method4114(i_2, this.field3188, this, -1369689255);
               this.field3191 = i_2;
            }
         }

         if (this.field3191 == -1) {
            this.field3185 = true;
         }

      }
   }

   void vmethod4264(int i_1, int i_2) {
      int i_3 = this.field3183;
      long long_4 = (long)((i_3 << 16) + i_1);
      class243 class243_6 = (class243) class247.field3201.method5968(long_4);
      if (class243_6 != null) {
         class247.field3200.method4793(class243_6);
      }

   }

   public boolean method4286(int i_1, int i_2) {
      return this.field3187[i_1];
   }

   void method4274(class318 class318_1, int i_2, byte[] bytes_3, boolean bool_4, int i_5) {
      int i_6;
      if (class318_1 == this.field3182) {
         if (this.field3185) {
            throw new RuntimeException();
         }

         if (bytes_3 == null) {
            class226.method4085(this, 255, this.field3183, this.field3189, (byte) 0, true, -1191163798);
            return;
         }

         field3186.reset();
         field3186.update(bytes_3, 0, bytes_3.length);
         i_6 = (int)field3186.getValue();
         if (i_6 != this.field3189) {
            class226.method4085(this, 255, this.field3183, this.field3189, (byte) 0, true, 1544995454);
            return;
         }

         class310 class310_12 = new class310(class320.method5895(bytes_3, (byte) 1));
         int i_13 = class310_12.method5661((byte) -73);
         if (i_13 != 5 && i_13 != 6) {
            throw new RuntimeException(i_13 + "," + this.field3183 + "," + i_2);
         }

         int i_9 = 0;
         if (i_13 >= 6) {
            i_9 = class310_12.method5507(-994237447);
         }

         if (i_9 != this.field3190) {
            class226.method4085(this, 255, this.field3183, this.field3189, (byte) 0, true, -442567106);
            return;
         }

         this.method4158(bytes_3, 1228121512);
         this.method4269(-457246550);
      } else {
         if (!bool_4 && i_2 == this.field3191) {
            this.field3185 = true;
         }

         if (bytes_3 == null || bytes_3.length <= 2) {
            this.field3187[i_2] = false;
            if (this.field3192 || bool_4) {
               class226.method4085(this, this.field3183, i_2, super.field3163[i_2], (byte) 2, bool_4, -592598015);
            }

            return;
         }

         field3186.reset();
         field3186.update(bytes_3, 0, bytes_3.length - 2);
         i_6 = (int)field3186.getValue();
         int i_7 = ((bytes_3[bytes_3.length - 2] & 0xff) << 8) + (bytes_3[bytes_3.length - 1] & 0xff);
         if (i_6 != super.field3163[i_2] || i_7 != super.field3158[i_2]) {
            this.field3187[i_2] = false;
            if (this.field3192 || bool_4) {
               class226.method4085(this, this.field3183, i_2, super.field3163[i_2], (byte) 2, bool_4, 1362599057);
            }

            return;
         }

         this.field3187[i_2] = true;
         if (bool_4) {
            Object[] arr_8 = super.field3169;
            Object obj_10;
            if (bytes_3 == null) {
               obj_10 = null;
            } else if (bytes_3.length > 136) {
               class215 class215_11 = new class215();
               class215_11.vmethod3947(bytes_3, 704887954);
               obj_10 = class215_11;
            } else {
               obj_10 = bytes_3;
            }

            arr_8[i_2] = obj_10;
         }
      }

   }

   public void method4266(int i_1, int i_2, int i_3) {
      this.field3189 = i_1;
      this.field3190 = i_2;
      if (this.field3182 != null) {
         int i_4 = this.field3183;
         class318 class318_5 = this.field3182;
         byte[] bytes_7 = null;
         class272 class272_8 = class245.field3177;
         synchronized(class245.field3177) {
            for (class242 class242_9 = (class242) class245.field3177.method4879(); class242_9 != null; class242_9 = (class242) class245.field3177.method4884()) {
               if ((long)i_4 == class242_9.field2137 && class318_5 == class242_9.field3146 && class242_9.field3149 == 0) {
                  bytes_7 = class242_9.field3145;
                  break;
               }
            }
         }

         if (bytes_7 != null) {
            this.method4274(class318_5, i_4, bytes_7, true, 593580937);
         } else {
            byte[] bytes_12 = class318_5.method5864(i_4, 1443006702);
            this.method4274(class318_5, i_4, bytes_12, true, 614223747);
         }
      } else {
         class226.method4085(this, 255, this.field3183, this.field3189, (byte) 0, true, 452837182);
      }

   }

   public int method4275(int i_1) {
      if (this.field3185) {
         return 100;
      } else if (super.field3169 != null) {
         return 99;
      } else {
         int i_3 = this.field3183;
         long long_4 = (long)(i_3 + 16711680);
         int i_2;
         if (class247.field3202 != null && class247.field3202.field2137 == long_4) {
            i_2 = class206.field2427.field3751 * 99 / (class206.field2427.field3752.length - class247.field3202.field3153) + 1;
         } else {
            i_2 = 0;
         }

         int i_6 = i_2;
         if (i_2 >= 100) {
            i_6 = 99;
         }

         return i_6;
      }
   }

   public int method4273(int i_1) {
      int i_2 = 0;
      int i_3 = 0;

      int i_4;
      for (i_4 = 0; i_4 < super.field3169.length; i_4++) {
         if (super.field3165[i_4] > 0) {
            i_2 += 100;
            i_3 += this.vmethod4270(i_4, 2120197275);
         }
      }

      if (i_2 == 0) {
         return 100;
      } else {
         i_4 = i_3 * 100 / i_2;
         return i_4;
      }
   }

   public void method4284(int i_1, byte[] bytes_2, boolean bool_3, boolean bool_4, int i_5) {
      if (bool_3) {
         if (this.field3185) {
            throw new RuntimeException();
         }

         if (this.field3182 != null) {
            class54.method872(this.field3183, bytes_2, this.field3182, -1752347776);
         }

         this.method4158(bytes_2, 907674687);
         this.method4269(-457246550);
      } else {
         bytes_2[bytes_2.length - 2] = (byte)(super.field3158[i_1] >> 8);
         bytes_2[bytes_2.length - 1] = (byte)super.field3158[i_1];
         if (this.field3188 != null) {
            class54.method872(i_1, bytes_2, this.field3188, -1114556828);
            this.field3187[i_1] = true;
         }

         if (bool_4) {
            Object[] arr_6 = super.field3169;
            Object obj_8;
            if (bytes_2 == null) {
               obj_8 = null;
            } else if (bytes_2.length > 136) {
               class215 class215_9 = new class215();
               class215_9.vmethod3947(bytes_2, -277286562);
               obj_8 = class215_9;
            } else {
               obj_8 = bytes_2;
            }

            arr_6[i_1] = obj_8;
         }
      }

   }

   public boolean method4271(byte b_1) {
      return this.field3185;
   }

   public static boolean method4296(int i_0, int i_1) {
      return i_0 == 10 || i_0 == 11 || i_0 == 12 || i_0 == 13 || i_0 == 14 || i_0 == 15 || i_0 == 16 || i_0 == 17;
   }

}
