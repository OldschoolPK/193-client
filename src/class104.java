import java.io.IOException;

public class class104 {

   static int field1327;
   static int field1331;
   static int field1341;
   class191 field1336;
   class191 field1337;
   public class338 field1328;
   class191 field1335;
   class271 field1324 = new class271();
   int field1326 = 0;
   class310 field1329 = new class310(5000);
   class309 field1325 = new class309(40000);
   class191 field1330 = null;
   int field1338 = 0;
   boolean field1332 = true;
   int field1333 = 0;
   int field1334 = 0;
   class315 field1339;

   class315 method2299(int i_1) {
      return this.field1339;
   }

   void method2294(int i_1) {
      if (this.field1339 != null) {
         this.field1339.vmethod5831((byte) -19);
         this.field1339 = null;
      }

   }

   void method2293(int i_1) {
      this.field1339 = null;
   }

   final void method2307(short s_1) {
      this.field1324.method4800();
      this.field1326 = 0;
   }

   public final void method2295(class196 class196_1, int i_2) {
      this.field1324.method4811(class196_1);
      class196_1.field2366 = class196_1.field2360.field3751;
      class196_1.field2360.field3751 = 0;
      this.field1326 += class196_1.field2366;
   }

   final void method2298(int i_1) throws IOException {
      if (this.field1339 != null && this.field1326 > 0) {
         this.field1329.field3751 = 0;

         while (true) {
            class196 class196_2 = (class196) this.field1324.method4806();
            if (class196_2 == null || class196_2.field2366 > this.field1329.field3752.length - this.field1329.field3751) {
               this.field1339.vmethod5844(this.field1329.field3752, 0, this.field1329.field3751, -2028715299);
               this.field1334 = 0;
               break;
            }

            this.field1329.method5624(class196_2.field2360.field3752, 0, class196_2.field2366, 1585895405);
            this.field1326 -= class196_2.field2366;
            class196_2.method3628();
            class196_2.field2360.method5512((byte) 118);
            class196_2.method3654((byte) -111);
         }
      }

   }

   void method2296(class315 class315_1, int i_2) {
      this.field1339 = class315_1;
   }

}
