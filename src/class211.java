public class class211 extends class124 {

   public static int field2505;
   class272 field2502 = new class272();
   class107 field2504 = new class107();
   class207 field2503;

   class211(class207 class207_1) {
      this.field2503 = class207_1;
   }

   protected void vmethod3905(int[] ints_1, int i_2, int i_3) {
      this.field2504.vmethod3905(ints_1, i_2, i_3);

      for (class208 class208_6 = (class208) this.field2502.method4879(); class208_6 != null; class208_6 = (class208) this.field2502.method4884()) {
         if (!this.field2503.method3820(class208_6, 610995738)) {
            int i_4 = i_2;
            int i_5 = i_3;

            do {
               if (i_5 <= class208_6.field2475) {
                  this.method3899(class208_6, ints_1, i_4, i_5, i_4 + i_5, 1581372981);
                  class208_6.field2475 -= i_5;
                  break;
               }

               this.method3899(class208_6, ints_1, i_4, class208_6.field2475, i_4 + i_5, 882339994);
               i_4 += class208_6.field2475;
               i_5 -= class208_6.field2475;
            } while (!this.field2503.method3755(class208_6, ints_1, i_4, i_5, -638435532));
         }
      }

   }

   protected void vmethod3898(int i_1) {
      this.field2504.vmethod3898(i_1);

      for (class208 class208_3 = (class208) this.field2502.method4879(); class208_3 != null; class208_3 = (class208) this.field2502.method4884()) {
         if (!this.field2503.method3820(class208_3, 610995738)) {
            int i_2 = i_1;

            do {
               if (i_2 <= class208_3.field2475) {
                  this.method3900(class208_3, i_2, 936470010);
                  class208_3.field2475 -= i_2;
                  break;
               }

               this.method3900(class208_3, class208_3.field2475, 1389060283);
               i_2 -= class208_3.field2475;
            } while (!this.field2503.method3755(class208_3, (int[]) null, 0, i_2, -638435532));
         }
      }

   }

   protected class124 vmethod3917() {
      class208 class208_1;
      do {
         class208_1 = (class208) this.field2502.method4884();
         if (class208_1 == null) {
            return null;
         }
      } while (class208_1.field2474 == null);

      return class208_1.field2474;
   }

   void method3900(class208 class208_1, int i_2, int i_3) {
      if ((this.field2503.field2454[class208_1.field2477] & 0x4) != 0 && class208_1.field2470 < 0) {
         int i_4 = this.field2503.field2442[class208_1.field2477] / class114.field1443;
         int i_5 = (i_4 + 1048575 - class208_1.field2456) / i_4;
         class208_1.field2456 = i_4 * i_2 + class208_1.field2456 & 0xfffff;
         if (i_5 <= i_2) {
            if (this.field2503.field2430[class208_1.field2477] == 0) {
               class208_1.field2474 = class122.method2602(class208_1.field2458, class208_1.field2474.method2594(), class208_1.field2474.method2585(), class208_1.field2474.method2586());
            } else {
               class208_1.field2474 = class122.method2602(class208_1.field2458, class208_1.field2474.method2594(), 0, class208_1.field2474.method2586());
               this.field2503.method3732(class208_1, class208_1.field2457.field2494[class208_1.field2461] < 0, (byte) 1);
            }

            if (class208_1.field2457.field2494[class208_1.field2461] < 0) {
               class208_1.field2474.method2581(-1);
            }

            i_2 = class208_1.field2456 / i_4;
         }
      }

      class208_1.field2474.vmethod3898(i_2);
   }

   void method3899(class208 class208_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6) {
      if ((this.field2503.field2454[class208_1.field2477] & 0x4) != 0 && class208_1.field2470 < 0) {
         int i_7 = this.field2503.field2442[class208_1.field2477] / class114.field1443;

         while (true) {
            int i_8 = (i_7 + 1048575 - class208_1.field2456) / i_7;
            if (i_8 > i_4) {
               class208_1.field2456 += i_4 * i_7;
               break;
            }

            class208_1.field2474.vmethod3905(ints_2, i_3, i_8);
            i_3 += i_8;
            i_4 -= i_8;
            class208_1.field2456 += i_8 * i_7 - 1048576;
            int i_9 = class114.field1443 / 100;
            int i_10 = 262144 / i_7;
            if (i_10 < i_9) {
               i_9 = i_10;
            }

            class122 class122_11 = class208_1.field2474;
            if (this.field2503.field2430[class208_1.field2477] == 0) {
               class208_1.field2474 = class122.method2602(class208_1.field2458, class122_11.method2594(), class122_11.method2585(), class122_11.method2586());
            } else {
               class208_1.field2474 = class122.method2602(class208_1.field2458, class122_11.method2594(), 0, class122_11.method2586());
               this.field2503.method3732(class208_1, class208_1.field2457.field2494[class208_1.field2461] < 0, (byte) 1);
               class208_1.field2474.method2679(i_9, class122_11.method2585());
            }

            if (class208_1.field2457.field2494[class208_1.field2461] < 0) {
               class208_1.field2474.method2581(-1);
            }

            class122_11.method2592(i_9);
            class122_11.vmethod3905(ints_2, i_3, i_5 - i_3);
            if (class122_11.method2596()) {
               this.field2504.method2324(class122_11);
            }
         }
      }

      class208_1.field2474.vmethod3905(ints_2, i_3, i_4);
   }

   protected class124 vmethod3894() {
      class208 class208_1 = (class208) this.field2502.method4879();
      return (class124) (class208_1 == null ? null : (class208_1.field2474 != null ? class208_1.field2474 : this.vmethod3917())) ;
   }

   protected int vmethod3896() {
      return 0;
   }

   public static void method3922(int i_0) {
      class255.field3288.method3375();
   }

   public static int method3923(int i_0, int i_1, byte b_2) {
      return (i_0 + 40000 << 8) + i_1;
   }

}
