import java.security.SecureRandom;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class class97 {

   static Thread field1247;
   static int field1244;
   ExecutorService field1243 = Executors.newSingleThreadExecutor();
   Future field1242;

   class97() {
      this.field1242 = this.field1243.submit(new class66());
   }

   boolean method2174(int i_1) {
      return this.field1242.isDone();
   }

   SecureRandom method2176(int i_1) {
      try {
         return (SecureRandom) this.field1242.get();
      } catch (Exception exception_5) {
         SecureRandom securerandom_4 = new SecureRandom();
         securerandom_4.nextInt();
         return securerandom_4;
      }
   }

   void method2166(int i_1) {
      this.field1243.shutdown();
      this.field1243 = null;
   }

   public static void method2177(boolean bool_0, int i_1) {
      if (bool_0 != class265.field3466) {
         class96.method2159(-1747466644);
         class265.field3466 = bool_0;
      }

   }

   static final void method2175(int i_0, int i_1, int i_2, int i_3, int i_4) {
      ++client.field746;
      class93.method2110(1150447373);
      class51.method832(1731859133);
      if (client.field814 >= 0 && client.field764[client.field814] != null) {
         class82.method1879(client.field764[client.field814], false, -864569617);
      }

      class141.method3129(true, -1484457450);
      class31.method332(1899321298);
      class141.method3129(false, -1004526873);

      int i_7;
      for (class95 class95_5 = (class95) client.field778.method4879(); class95_5 != null; class95_5 = (class95) client.field778.method4884()) {
         if (class95_5.field1225 == class151.field1949 && client.field655 <= class95_5.field1234) {
            if (client.field655 >= class95_5.field1218) {
               if (class95_5.field1231 > 0) {
                  class88 class88_32 = client.field689[class95_5.field1231 - 1];
                  if (class88_32 != null && class88_32.field990 >= 0 && class88_32.field990 < 13312 && class88_32.field938 >= 0 && class88_32.field938 < 13312) {
                     class95_5.method2147(class88_32.field990, class88_32.field938, method2165(class88_32.field990, class88_32.field938, class95_5.field1225, 2000555105) - class95_5.field1227, client.field655, 882904706);
                  }
               }

               if (class95_5.field1231 < 0) {
                  i_7 = -class95_5.field1231 - 1;
                  class75 class75_37;
                  if (i_7 == client.field765) {
                     class75_37 = class223.field2562;
                  } else {
                     class75_37 = client.field764[i_7];
                  }

                  if (class75_37 != null && class75_37.field990 >= 0 && class75_37.field990 < 13312 && class75_37.field938 >= 0 && class75_37.field938 < 13312) {
                     class95_5.method2147(class75_37.field990, class75_37.field938, method2165(class75_37.field990, class75_37.field938, class95_5.field1225, 1684938854) - class95_5.field1227, client.field655, 768896507);
                  }
               }

               class95_5.method2149(client.field850, (byte) -20);
               class67.field536.method3142(class151.field1949, (int)class95_5.field1224, (int)class95_5.field1217, (int)class95_5.field1232, 60, class95_5, class95_5.field1222, -1L, false);
            }
         } else {
            class95_5.method3628();
         }
      }

      class36.method467(-1287826553);
      class25.method249(i_0, i_1, i_2, i_3, true, 473576557);
      i_0 = client.field904;
      i_1 = client.field905;
      i_2 = client.field906;
      i_3 = client.field695;
      class331.method5996(i_0, i_1, i_0 + i_2, i_3 + i_1);
      class139.method3109();
      int i_6;
      int i_8;
      int i_9;
      int i_10;
      int i_11;
      int i_12;
      int i_13;
      int i_14;
      int i_15;
      int i_16;
      int i_17;
      int i_29;
      if (!client.field890) {
         i_29 = client.field777;
         if (client.field918 / 256 > i_29) {
            i_29 = client.field918 / 256;
         }

         if (client.field902[4] && client.field893[4] + 128 > i_29) {
            i_29 = client.field893[4] + 128;
         }

         i_6 = client.field830 & 0x7ff;
         i_7 = field1244;
         i_8 = class104.field1341;
         i_9 = class121.field1477;
         i_10 = class45.method704(i_29, 2111583994);
         i_10 = class180.method3559(i_10, i_3, (byte) -60);
         i_11 = 2048 - i_29 & 0x7ff;
         i_12 = 2048 - i_6 & 0x7ff;
         i_13 = 0;
         i_14 = 0;
         i_15 = i_10;
         int i_18;
         if (i_11 != 0) {
            i_16 = class139.field1797[i_11];
            i_17 = class139.field1780[i_11];
            i_18 = i_17 * i_14 - i_16 * i_10 >> 16;
            i_15 = i_16 * i_14 + i_17 * i_10 >> 16;
            i_14 = i_18;
         }

         if (i_12 != 0) {
            i_16 = class139.field1797[i_12];
            i_17 = class139.field1780[i_12];
            i_18 = i_13 * i_17 + i_15 * i_16 >> 16;
            i_15 = i_15 * i_17 - i_16 * i_13 >> 16;
            i_13 = i_18;
         }

         class75.field630 = i_7 - i_13;
         class54.field421 = i_8 - i_14;
         class69.field558 = i_9 - i_15;
         class141.field1823 = i_29;
         class244.field3176 = i_6;
         if (client.field723 == 1 && client.field775 >= 2 && client.field655 % 50 == 0 && (field1244 >> 7 != class223.field2562.field990 >> 7 || class121.field1477 >> 7 != class223.field2562.field938 >> 7)) {
            i_16 = class223.field2562.field627;
            i_17 = (field1244 >> 7) + class243.field3156;
            i_18 = (class121.field1477 >> 7) + class41.field300;
            class196 class196_19 = class68.method1249(class192.field2269, client.field694.field1328, (byte) 1);
            class196_19.field2360.method5538(i_17, (short) -351);
            class196_19.field2360.method5547(client.field730, (byte) -113);
            class196_19.field2360.method5699(i_18, -884805466);
            class196_19.field2360.method5482(i_16, (byte) -28);
            client.field694.method2295(class196_19, 289855124);
         }
      }

      if (!client.field890) {
         if (class282.field3617.field1052) {
            i_6 = class151.field1949;
         } else {
            label692: {
               i_7 = 3;
               if (class141.field1823 < 310) {
                  if (client.field723 == 1) {
                     i_8 = field1244 >> 7;
                     i_9 = class121.field1477 >> 7;
                  } else {
                     i_8 = class223.field2562.field990 >> 7;
                     i_9 = class223.field2562.field938 >> 7;
                  }

                  i_10 = class75.field630 >> 7;
                  i_11 = class69.field558 >> 7;
                  if (i_10 < 0 || i_11 < 0 || i_10 >= 104 || i_11 >= 104) {
                     i_6 = class151.field1949;
                     break label692;
                  }

                  if (i_8 < 0 || i_9 < 0 || i_8 >= 104 || i_9 >= 104) {
                     i_6 = class151.field1949;
                     break label692;
                  }

                  if ((class64.field507[class151.field1949][i_10][i_11] & 0x4) != 0) {
                     i_7 = class151.field1949;
                  }

                  if (i_8 > i_10) {
                     i_12 = i_8 - i_10;
                  } else {
                     i_12 = i_10 - i_8;
                  }

                  if (i_9 > i_11) {
                     i_13 = i_9 - i_11;
                  } else {
                     i_13 = i_11 - i_9;
                  }

                  if (i_12 > i_13) {
                     i_14 = i_13 * 65536 / i_12;
                     i_15 = 32768;

                     while (i_8 != i_10) {
                        if (i_10 < i_8) {
                           ++i_10;
                        } else if (i_10 > i_8) {
                           --i_10;
                        }

                        if ((class64.field507[class151.field1949][i_10][i_11] & 0x4) != 0) {
                           i_7 = class151.field1949;
                        }

                        i_15 += i_14;
                        if (i_15 >= 65536) {
                           i_15 -= 65536;
                           if (i_11 < i_9) {
                              ++i_11;
                           } else if (i_11 > i_9) {
                              --i_11;
                           }

                           if ((class64.field507[class151.field1949][i_10][i_11] & 0x4) != 0) {
                              i_7 = class151.field1949;
                           }
                        }
                     }
                  } else if (i_13 > 0) {
                     i_14 = i_12 * 65536 / i_13;
                     i_15 = 32768;

                     while (i_11 != i_9) {
                        if (i_11 < i_9) {
                           ++i_11;
                        } else if (i_11 > i_9) {
                           --i_11;
                        }

                        if ((class64.field507[class151.field1949][i_10][i_11] & 0x4) != 0) {
                           i_7 = class151.field1949;
                        }

                        i_15 += i_14;
                        if (i_15 >= 65536) {
                           i_15 -= 65536;
                           if (i_10 < i_8) {
                              ++i_10;
                           } else if (i_10 > i_8) {
                              --i_10;
                           }

                           if ((class64.field507[class151.field1949][i_10][i_11] & 0x4) != 0) {
                              i_7 = class151.field1949;
                           }
                        }
                     }
                  }
               }

               if (class223.field2562.field990 >= 0 && class223.field2562.field938 >= 0 && class223.field2562.field990 < 13312 && class223.field2562.field938 < 13312) {
                  if ((class64.field507[class151.field1949][class223.field2562.field990 >> 7][class223.field2562.field938 >> 7] & 0x4) != 0) {
                     i_7 = class151.field1949;
                  }

                  i_6 = i_7;
               } else {
                  i_6 = class151.field1949;
               }
            }
         }

         i_29 = i_6;
      } else {
         i_29 = class45.method712(1888675083);
      }

      i_6 = class75.field630;
      i_7 = class54.field421;
      i_8 = class69.field558;
      i_9 = class141.field1823;
      i_10 = class244.field3176;

      for (i_11 = 0; i_11 < 5; i_11++) {
         if (client.field902[i_11]) {
            i_12 = (int)(Math.random() * (double)(client.field679[i_11] * 2 + 1) - (double)client.field679[i_11] + Math.sin((double)client.field705[i_11] / 100.0D * (double)client.field644[i_11]) * (double)client.field893[i_11]);
            if (i_11 == 0) {
               class75.field630 += i_12;
            }

            if (i_11 == 1) {
               class54.field421 += i_12;
            }

            if (i_11 == 2) {
               class69.field558 += i_12;
            }

            if (i_11 == 3) {
               class244.field3176 = i_12 + class244.field3176 & 0x7ff;
            }

            if (i_11 == 4) {
               class141.field1823 += i_12;
               if (class141.field1823 < 128) {
                  class141.field1823 = 128;
               }

               if (class141.field1823 > 383) {
                  class141.field1823 = 383;
               }
            }
         }
      }

      i_11 = class63.field492;
      i_12 = class63.field499;
      if (class63.field483 != 0) {
         i_11 = class63.field489;
         i_12 = class63.field502;
      }

      if (i_11 >= i_0 && i_11 < i_0 + i_2 && i_12 >= i_1 && i_12 < i_3 + i_1) {
         class3.method41(i_11 - i_0, i_12 - i_1, -1239840122);
      } else {
         class137.field1756 = false;
         class137.field1763 = 0;
      }

      class302.method5318((byte) 8);
      class331.method6004(i_0, i_1, i_2, i_3, 0);
      class302.method5318((byte) 8);
      i_13 = class139.field1796;
      class139.field1796 = client.field908;
      class67.field536.method3181(class75.field630, class54.field421, class69.field558, class141.field1823, class244.field3176, i_29);
      class139.field1796 = i_13;
      class302.method5318((byte) 8);
      class67.field536.method3197();
      client.field735 = 0;
      boolean bool_35 = false;
      i_15 = -1;
      i_16 = -1;
      i_17 = class98.field1263;
      int[] ints_30 = class98.field1268;

      int i_31;
      for (i_31 = 0; i_31 < i_17 + client.field690; i_31++) {
         Object obj_20;
         if (i_31 < i_17) {
            obj_20 = client.field764[ints_30[i_31]];
            if (ints_30[i_31] == client.field814) {
               bool_35 = true;
               i_15 = i_31;
               continue;
            }

            if (obj_20 == class223.field2562) {
               i_16 = i_31;
               continue;
            }
         } else {
            obj_20 = client.field689[client.field691[i_31 - i_17]];
         }

         class43.method655((class78) obj_20, i_31, i_0, i_1, i_2, i_3, -2028403898);
      }

      if (client.field725 && i_16 != -1) {
         class43.method655(class223.field2562, i_16, i_0, i_1, i_2, i_3, -970580745);
      }

      if (bool_35) {
         class43.method655(client.field764[client.field814], i_15, i_0, i_1, i_2, i_3, -1640953003);
      }

      for (i_31 = 0; i_31 < client.field735; i_31++) {
         int i_33 = client.field783[i_31];
         int i_21 = client.field738[i_31];
         int i_22 = client.field726[i_31];
         int i_23 = client.field739[i_31];
         boolean bool_24 = true;

         while (bool_24) {
            bool_24 = false;

            for (int i_25 = 0; i_25 < i_31; i_25++) {
               if (i_21 + 2 > client.field738[i_25] - client.field739[i_25] && i_21 - i_23 < client.field738[i_25] + 2 && i_33 - i_22 < client.field726[i_25] + client.field783[i_25] && i_33 + i_22 > client.field783[i_25] - client.field726[i_25] && client.field738[i_25] - client.field739[i_25] < i_21) {
                  i_21 = client.field738[i_25] - client.field739[i_25];
                  bool_24 = true;
               }
            }
         }

         client.field747 = client.field783[i_31];
         client.field748 = client.field738[i_31] = i_21;
         String string_34 = client.field841[i_31];
         if (client.field812 == 0) {
            int i_26 = 16776960;
            if (client.field741[i_31] < 6) {
               i_26 = client.field863[client.field741[i_31]];
            }

            if (client.field741[i_31] == 6) {
               i_26 = client.field746 % 20 < 10 ? 16711680 : 16776960;
            }

            if (client.field741[i_31] == 7) {
               i_26 = client.field746 % 20 < 10 ? 255 : 65535;
            }

            if (client.field741[i_31] == 8) {
               i_26 = client.field746 % 20 < 10 ? 45056 : 8454016;
            }

            int i_27;
            if (client.field741[i_31] == 9) {
               i_27 = 150 - client.field743[i_31];
               if (i_27 < 50) {
                  i_26 = i_27 * 1280 + 16711680;
               } else if (i_27 < 100) {
                  i_26 = 16776960 - (i_27 - 50) * 327680;
               } else if (i_27 < 150) {
                  i_26 = (i_27 - 100) * 5 + 65280;
               }
            }

            if (client.field741[i_31] == 10) {
               i_27 = 150 - client.field743[i_31];
               if (i_27 < 50) {
                  i_26 = i_27 * 5 + 16711680;
               } else if (i_27 < 100) {
                  i_26 = 16711935 - (i_27 - 50) * 327680;
               } else if (i_27 < 150) {
                  i_26 = (i_27 - 100) * 327680 + 255 - (i_27 - 100) * 5;
               }
            }

            if (client.field741[i_31] == 11) {
               i_27 = 150 - client.field743[i_31];
               if (i_27 < 50) {
                  i_26 = 16777215 - i_27 * 327685;
               } else if (i_27 < 100) {
                  i_26 = (i_27 - 50) * 327685 + 65280;
               } else if (i_27 < 150) {
                  i_26 = 16777215 - (i_27 - 100) * 327680;
               }
            }

            if (client.field742[i_31] == 0) {
               class17.field74.method5436(string_34, i_0 + client.field747, client.field748 + i_1, i_26, 0);
            }

            if (client.field742[i_31] == 1) {
               class17.field74.method5350(string_34, i_0 + client.field747, client.field748 + i_1, i_26, 0, client.field746);
            }

            if (client.field742[i_31] == 2) {
               class17.field74.method5357(string_34, i_0 + client.field747, client.field748 + i_1, i_26, 0, client.field746);
            }

            if (client.field742[i_31] == 3) {
               class17.field74.method5351(string_34, i_0 + client.field747, client.field748 + i_1, i_26, 0, client.field746, 150 - client.field743[i_31]);
            }

            if (client.field742[i_31] == 4) {
               i_27 = (150 - client.field743[i_31]) * (class17.field74.method5340(string_34) + 100) / 150;
               class331.method5997(i_0 + client.field747 - 50, i_1, i_0 + client.field747 + 50, i_3 + i_1);
               class17.field74.method5335(string_34, i_0 + client.field747 + 50 - i_27, client.field748 + i_1, i_26, 0);
               class331.method5996(i_0, i_1, i_0 + i_2, i_3 + i_1);
            }

            if (client.field742[i_31] == 5) {
               i_27 = 150 - client.field743[i_31];
               int i_28 = 0;
               if (i_27 < 25) {
                  i_28 = i_27 - 25;
               } else if (i_27 > 125) {
                  i_28 = i_27 - 125;
               }

               class331.method5997(i_0, client.field748 + i_1 - class17.field74.field3727 - 1, i_0 + i_2, client.field748 + i_1 + 5);
               class17.field74.method5436(string_34, i_0 + client.field747, i_28 + client.field748 + i_1, i_26, 0);
               class331.method5996(i_0, i_1, i_0 + i_2, i_3 + i_1);
            }
         } else {
            class17.field74.method5436(string_34, i_0 + client.field747, client.field748 + i_1, 16776960, 0);
         }
      }

      if (client.field740 == 2) {
         class18.method175((client.field709 - class243.field3156 << 7) + client.field669, (client.field667 - class41.field300 << 7) + client.field670, client.field820 * 2, -18578608);
         if (client.field747 > -1 && client.field655 % 20 < 10) {
            class43.field318[0].method6121(i_0 + client.field747 - 12, client.field748 + i_1 - 28);
         }
      }

      ((class128) class139.field1801).method2794(client.field850, -1550111062);
      class231.method4120(-1604724512);
      class75.field630 = i_6;
      class54.field421 = i_7;
      class69.field558 = i_8;
      class141.field1823 = i_9;
      class244.field3176 = i_10;
      if (client.field895) {
         byte b_36 = 0;
         i_15 = b_36 + class247.field3193 + class247.field3199;
         if (i_15 == 0) {
            client.field895 = false;
         }
      }

      if (client.field895) {
         class331.method6004(i_0, i_1, i_2, i_3, 0);
         class221.method3988("Loading - please wait.", false, (byte) -19);
      }

   }

   static final int method2165(int i_0, int i_1, int i_2, int i_3) {
      int i_4 = i_0 >> 7;
      int i_5 = i_1 >> 7;
      if (i_4 >= 0 && i_5 >= 0 && i_4 <= 103 && i_5 <= 103) {
         int i_6 = i_2;
         if (i_2 < 3 && (class64.field507[1][i_4][i_5] & 0x2) == 2) {
            i_6 = i_2 + 1;
         }

         int i_7 = i_0 & 0x7f;
         int i_8 = i_1 & 0x7f;
         int i_9 = (128 - i_7) * class64.field519[i_6][i_4][i_5] + class64.field519[i_6][i_4 + 1][i_5] * i_7 >> 7;
         int i_10 = i_7 * class64.field519[i_6][i_4 + 1][i_5 + 1] + class64.field519[i_6][i_4][i_5 + 1] * (128 - i_7) >> 7;
         return i_9 * (128 - i_8) + i_10 * i_8 >> 7;
      } else {
         return 0;
      }
   }

}
