import java.io.DataInputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;

public class class175 implements Runnable {

   static class315 field2057;
   class174 field2050 = null;
   class174 field2049 = null;
   boolean field2053 = false;
   public static String field2052;
   public static String field2051;
   Thread field2048;

   public class175() {
      field2052 = "Unknown";
      field2051 = "1.6";

      try {
         field2052 = System.getProperty("java.vendor");
         field2051 = System.getProperty("java.version");
      } catch (Exception exception_2) {
         ;
      }

      this.field2053 = false;
      this.field2048 = new Thread(this);
      this.field2048.setPriority(10);
      this.field2048.setDaemon(true);
      this.field2048.start();
   }

   final class174 method3532(int i_1, int i_2, int i_3, Object object_4, int i_5) {
      class174 class174_6 = new class174();
      class174_6.field2044 = i_1;
      class174_6.field2043 = i_2;
      class174_6.field2046 = object_4;
      synchronized(this) {
         if (this.field2049 != null) {
            this.field2049.field2039 = class174_6;
            this.field2049 = class174_6;
         } else {
            this.field2049 = this.field2050 = class174_6;
         }

         this.notify();
         return class174_6;
      }
   }

   public final class174 method3521(Runnable runnable_1, int i_2, int i_3) {
      return this.method3532(2, i_2, 0, runnable_1, -108529883);
   }

   public final void method3520(int i_1) {
      synchronized(this) {
         this.field2053 = true;
         this.notifyAll();
      }

      try {
         this.field2048.join();
      } catch (InterruptedException interruptedexception_4) {
         ;
      }

   }

   public final class174 method3522(String string_1, int i_2, byte b_3) {
      return this.method3532(1, i_2, 0, string_1, -108529883);
   }

   public final void run() {
      while (true) {
         class174 class174_1;
         synchronized(this) {
            while (true) {
               if (this.field2053) {
                  return;
               }

               if (this.field2050 != null) {
                  class174_1 = this.field2050;
                  this.field2050 = this.field2050.field2039;
                  if (this.field2050 == null) {
                     this.field2049 = null;
                  }
                  break;
               }

               try {
                  this.wait();
               } catch (InterruptedException interruptedexception_8) {
                  ;
               }
            }
         }

         try {
            int i_5 = class174_1.field2044;
            if (i_5 == 1) {
               class174_1.field2047 = new Socket(InetAddress.getByName((String) class174_1.field2046), class174_1.field2043);
            } else if (i_5 == 2) {
               Thread thread_3 = new Thread((Runnable) class174_1.field2046);
               thread_3.setDaemon(true);
               thread_3.start();
               thread_3.setPriority(class174_1.field2043);
               class174_1.field2047 = thread_3;
            } else if (i_5 == 4) {
               class174_1.field2047 = new DataInputStream(((URL) class174_1.field2046).openStream());
            }

            class174_1.field2045 = 1;
         } catch (ThreadDeath threaddeath_6) {
            throw threaddeath_6;
         } catch (Throwable throwable_7) {
            class174_1.field2045 = 2;
         }
      }
   }

   static void method3529(int i_0, boolean bool_1, int i_2, boolean bool_3, int i_4) {
      if (class80.field1012 != null) {
         class25.method252(0, class80.field1012.length - 1, i_0, bool_1, i_2, bool_3, -1262042728);
      }

   }

}
