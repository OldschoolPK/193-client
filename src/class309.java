public class class309 extends class310 {

   static final int[] field3747 = new int[] {0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143, 524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431, 67108863, 134217727, 268435455, 536870911, 1073741823, Integer.MAX_VALUE, -1};
   class338 field3748;
   int field3746;

   public class309(int i_1) {
      super(i_1);
   }

   public boolean method5451(int i_1) {
      int i_2 = super.field3752[super.field3751] - this.field3748.method6262(1809615981) & 0xff;
      return i_2 >= 128;
   }

   public void method5448(class338 class338_1, byte b_2) {
      this.field3748 = class338_1;
   }

   public void method5479(int i_1, int i_2) {
      super.field3752[++super.field3751 - 1] = (byte)(i_1 + this.field3748.method6270(860542732));
   }

   public void method5454(int i_1) {
      this.field3746 = super.field3751 * 8;
   }

   public int method5455(int i_1, byte b_2) {
      int i_3 = this.field3746 >> 3;
      int i_4 = 8 - (this.field3746 & 0x7);
      int i_5 = 0;

      for (this.field3746 += i_1; i_1 > i_4; i_4 = 8) {
         i_5 += (super.field3752[i_3++] & field3747[i_4]) << i_1 - i_4;
         i_1 -= i_4;
      }

      if (i_4 == i_1) {
         i_5 += super.field3752[i_3] & field3747[i_4];
      } else {
         i_5 += super.field3752[i_3] >> i_4 - i_1 & field3747[i_1];
      }

      return i_5;
   }

   public int method5457(int i_1, int i_2) {
      return i_1 * 8 - this.field3746;
   }

   public void method5477(int i_1) {
      super.field3751 = (this.field3746 + 7) / 8;
   }

   public void method5453(byte[] bytes_1, int i_2, int i_3, int i_4) {
      for (int i_5 = 0; i_5 < i_3; i_5++) {
         bytes_1[i_5 + i_2] = (byte)(super.field3752[++super.field3751 - 1] - this.field3748.method6270(1342039697));
      }

   }

   public int method5467(byte b_1) {
      int i_2 = super.field3752[++super.field3751 - 1] - this.field3748.method6270(1842363535) & 0xff;
      return i_2 < 128 ? i_2 : (i_2 - 128 << 8) + (super.field3752[++super.field3751 - 1] - this.field3748.method6270(-719003053) & 0xff);
   }

   public int method5450(int i_1) {
      return super.field3752[++super.field3751 - 1] - this.field3748.method6270(1023281998) & 0xff;
   }

   public void method5475(int[] ints_1, int i_2) {
      this.field3748 = new class338(ints_1);
   }

}
