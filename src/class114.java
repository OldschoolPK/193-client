public class class114 {

   public static int field1443;
   protected static boolean field1421;
   static int field1424;
   int field1431;
   int field1425 = 32;
   long field1429 = class298.method5270(255749540);
   long field1433 = 0L;
   int field1427 = 0;
   int field1435 = 0;
   int field1436 = 0;
   long field1437 = 0L;
   boolean field1422 = true;
   int field1439 = 0;
   class124[] field1440 = new class124[8];
   class124[] field1428 = new class124[8];
   int field1434;
   int field1432;
   protected int[] field1426;
   static class116 field1441;
   class124 field1423;

   protected void vmethod2487(byte b_1) throws Exception {
   }

   protected void vmethod2469(int i_1) {
   }

   public final synchronized void method2499(int i_1) {
      if (this.field1426 != null) {
         long long_2 = class298.method5270(255749540);

         try {
            if (this.field1433 != 0L) {
               if (long_2 < this.field1433) {
                  return;
               }

               this.vmethod2466(this.field1434, (short) 29768);
               this.field1433 = 0L;
               this.field1422 = true;
            }

            int i_4 = this.vmethod2467((byte) 60);
            if (this.field1436 - i_4 > this.field1427) {
               this.field1427 = this.field1436 - i_4;
            }

            int i_5 = this.field1431 + this.field1432;
            if (i_5 + 256 > 16384) {
               i_5 = 16128;
            }

            if (i_5 + 256 > this.field1434) {
               this.field1434 += 1024;
               if (this.field1434 > 16384) {
                  this.field1434 = 16384;
               }

               this.vmethod2469(1237438731);
               this.vmethod2466(this.field1434, (short) 3677);
               i_4 = 0;
               this.field1422 = true;
               if (i_5 + 256 > this.field1434) {
                  i_5 = this.field1434 - 256;
                  this.field1432 = i_5 - this.field1431;
               }
            }

            while (i_4 < i_5) {
               this.method2463(this.field1426, 256);
               this.vmethod2468();
               i_4 += 256;
            }

            if (long_2 > this.field1437) {
               if (!this.field1422) {
                  if (this.field1427 == 0 && this.field1435 == 0) {
                     this.vmethod2469(-1029280801);
                     this.field1433 = long_2 + 2000L;
                     return;
                  }

                  this.field1432 = Math.min(this.field1435, this.field1427);
                  this.field1435 = this.field1427;
               } else {
                  this.field1422 = false;
               }

               this.field1427 = 0;
               this.field1437 = long_2 + 2000L;
            }

            this.field1436 = i_4;
         } catch (Exception exception_8) {
            this.vmethod2469(-310479525);
            this.field1433 = long_2 + 2000L;
         }

         try {
            if (long_2 > this.field1429 + 500000L) {
               long_2 = this.field1429;
            }

            while (long_2 > 5000L + this.field1429) {
               this.method2462(256, (byte) 4);
               this.field1429 += (long)(256000 / field1443);
            }
         } catch (Exception exception_7) {
            this.field1429 = long_2;
         }

      }
   }

   protected int vmethod2467(byte b_1) throws Exception {
      return this.field1434;
   }

   protected void vmethod2466(int i_1, short s_2) throws Exception {
   }

   final void method2462(int i_1, byte b_2) {
      this.field1439 -= i_1;
      if (this.field1439 < 0) {
         this.field1439 = 0;
      }

      if (this.field1423 != null) {
         this.field1423.vmethod3898(i_1);
      }

   }

   public final synchronized void method2460(int i_1) {
      this.field1422 = true;

      try {
         this.vmethod2487((byte) -7);
      } catch (Exception exception_3) {
         this.vmethod2469(1699809355);
         this.field1433 = class298.method5270(255749540) + 2000L;
      }

   }

   final void method2464(class124 class124_1, int i_2, byte b_3) {
      int i_4 = i_2 >> 5;
      class124 class124_5 = this.field1428[i_4];
      if (class124_5 == null) {
         this.field1440[i_4] = class124_1;
      } else {
         class124_5.field1507 = class124_1;
      }

      this.field1428[i_4] = class124_1;
      class124_1.field1508 = i_2;
   }

   public final synchronized void method2461(int i_1) {
      if (field1441 != null) {
         boolean bool_2 = true;

         for (int i_3 = 0; i_3 < 2; i_3++) {
            if (this == field1441.field1455[i_3]) {
               field1441.field1455[i_3] = null;
            }

            if (field1441.field1455[i_3] != null) {
               bool_2 = false;
            }
         }

         if (bool_2) {
            class70.field566.shutdownNow();
            class70.field566 = null;
            field1441 = null;
         }
      }

      this.vmethod2469(-1084630500);
      this.field1426 = null;
   }

   public final void method2457(int i_1) {
      this.field1422 = true;
   }

   final void method2463(int[] ints_1, int i_2) {
      int i_3 = i_2;
      if (field1421) {
         i_3 = i_2 << 1;
      }

      class311.method5754(ints_1, 0, i_3);
      this.field1439 -= i_2;
      if (this.field1423 != null && this.field1439 <= 0) {
         this.field1439 += field1443 >> 4;
         class99.method2213(this.field1423, 1243916403);
         this.method2464(this.field1423, this.field1423.vmethod2772(), (byte) -108);
         int i_4 = 0;
         int i_5 = 255;

         int i_6;
         class124 class124_10;
         label104:
         for (i_6 = 7; i_5 != 0; --i_6) {
            int i_7;
            int i_8;
            if (i_6 < 0) {
               i_7 = i_6 & 0x3;
               i_8 = -(i_6 >> 2);
            } else {
               i_7 = i_6;
               i_8 = 0;
            }

            for (int i_9 = i_5 >>> i_7 & 0x11111111; i_9 != 0; i_9 >>>= 4) {
               if ((i_9 & 0x1) != 0) {
                  i_5 &= ~(1 << i_7);
                  class124_10 = null;
                  class124 class124_11 = this.field1440[i_7];

                  label98:
                  while (true) {
                     while (true) {
                        if (class124_11 == null) {
                           break label98;
                        }

                        class127 class127_12 = class124_11.field1506;
                        if (class127_12 != null && class127_12.field1540 > i_8) {
                           i_5 |= 1 << i_7;
                           class124_10 = class124_11;
                           class124_11 = class124_11.field1507;
                        } else {
                           class124_11.field1509 = true;
                           int i_13 = class124_11.vmethod3896();
                           i_4 += i_13;
                           if (class127_12 != null) {
                              class127_12.field1540 += i_13;
                           }

                           if (i_4 >= this.field1425) {
                              break label104;
                           }

                           class124 class124_14 = class124_11.vmethod3894();
                           if (class124_14 != null) {
                              for (int i_15 = class124_11.field1508; class124_14 != null; class124_14 = class124_11.vmethod3917()) {
                                 this.method2464(class124_14, i_15 * class124_14.vmethod2772() >> 8, (byte) -18);
                              }
                           }

                           class124 class124_18 = class124_11.field1507;
                           class124_11.field1507 = null;
                           if (class124_10 == null) {
                              this.field1440[i_7] = class124_18;
                           } else {
                              class124_10.field1507 = class124_18;
                           }

                           if (class124_18 == null) {
                              this.field1428[i_7] = class124_10;
                           }

                           class124_11 = class124_18;
                        }
                     }
                  }
               }

               i_7 += 4;
               ++i_8;
            }
         }

         for (i_6 = 0; i_6 < 8; i_6++) {
            class124 class124_16 = this.field1440[i_6];
            class124[] arr_17 = this.field1440;
            this.field1428[i_6] = null;

            for (arr_17[i_6] = null; class124_16 != null; class124_16 = class124_10) {
               class124_10 = class124_16.field1507;
               class124_16.field1507 = null;
            }
         }
      }

      if (this.field1439 < 0) {
         this.field1439 = 0;
      }

      if (this.field1423 != null) {
         this.field1423.vmethod3905(ints_1, 0, i_2);
      }

      this.field1429 = class298.method5270(255749540);
   }

   protected void vmethod2468() throws Exception {
   }

   public final synchronized void method2458(class124 class124_1, int i_2) {
      this.field1423 = class124_1;
   }

   protected void vmethod2465(byte b_1) throws Exception {
   }

   public static void method2510(class244 class244_0, class244 class244_1, class244 class244_2, byte b_3) {
      class269.field3553 = class244_0;
      class269.field3546 = class244_1;
      class269.field3547 = class244_2;
   }

   static final int method2516(int i_0, int i_1, int i_2) {
      int i_3 = class4.method56(45365 + i_0, i_1 + 91923, 4, (byte) -36) - 128 + (class4.method56(i_0 + 10294, 37821 + i_1, 2, (byte) -37) - 128 >> 1) + (class4.method56(i_0, i_1, 1, (byte) -72) - 128 >> 2);
      i_3 = (int)(0.3D * (double)i_3) + 35;
      if (i_3 < 10) {
         i_3 = 10;
      } else if (i_3 > 60) {
         i_3 = 60;
      }

      return i_3;
   }

}
