public class class109 {

   int field1363;
   int field1364;
   int[] field1362;
   int[] field1361;
   float[][] field1360;
   int[] field1365;

   class109() {
      class111.method2394(24);
      this.field1363 = class111.method2394(16);
      this.field1364 = class111.method2394(24);
      this.field1362 = new int[this.field1364];
      boolean bool_1 = class111.method2393() != 0;
      int i_2;
      int i_3;
      int i_5;
      if (bool_1) {
         i_2 = 0;

         for (i_3 = class111.method2394(5) + 1; i_2 < this.field1364; i_3++) {
            int i_4 = class111.method2394(class286.method5061(this.field1364 - i_2, (short) -28942));

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field1362[i_2++] = i_3;
            }
         }
      } else {
         boolean bool_14 = class111.method2393() != 0;

         for (i_3 = 0; i_3 < this.field1364; i_3++) {
            if (bool_14 && class111.method2393() == 0) {
               this.field1362[i_3] = 0;
            } else {
               this.field1362[i_3] = class111.method2394(5) + 1;
            }
         }
      }

      this.method2368();
      i_2 = class111.method2394(4);
      if (i_2 > 0) {
         float f_15 = class111.method2398(class111.method2394(32));
         float f_16 = class111.method2398(class111.method2394(32));
         i_5 = class111.method2394(4) + 1;
         boolean bool_6 = class111.method2393() != 0;
         int i_7;
         if (i_2 == 1) {
            i_7 = method2369(this.field1364, this.field1363);
         } else {
            i_7 = this.field1364 * this.field1363;
         }

         this.field1361 = new int[i_7];

         int i_8;
         for (i_8 = 0; i_8 < i_7; i_8++) {
            this.field1361[i_8] = class111.method2394(i_5);
         }

         this.field1360 = new float[this.field1364][this.field1363];
         float f_9;
         int i_10;
         int i_11;
         if (i_2 == 1) {
            for (i_8 = 0; i_8 < this.field1364; i_8++) {
               f_9 = 0.0F;
               i_10 = 1;

               for (i_11 = 0; i_11 < this.field1363; i_11++) {
                  int i_12 = i_8 / i_10 % i_7;
                  float f_13 = (float)this.field1361[i_12] * f_16 + f_15 + f_9;
                  this.field1360[i_8][i_11] = f_13;
                  if (bool_6) {
                     f_9 = f_13;
                  }

                  i_10 *= i_7;
               }
            }
         } else {
            for (i_8 = 0; i_8 < this.field1364; i_8++) {
               f_9 = 0.0F;
               i_10 = i_8 * this.field1363;

               for (i_11 = 0; i_11 < this.field1363; i_11++) {
                  float f_17 = (float)this.field1361[i_10] * f_16 + f_15 + f_9;
                  this.field1360[i_8][i_11] = f_17;
                  if (bool_6) {
                     f_9 = f_17;
                  }

                  ++i_10;
               }
            }
         }
      }

   }

   int method2374() {
      int i_1;
      for (i_1 = 0; this.field1365[i_1] >= 0; i_1 = class111.method2393() != 0 ? this.field1365[i_1] : i_1 + 1) {
         ;
      }

      return ~this.field1365[i_1];
   }

   float[] method2372() {
      return this.field1360[this.method2374()];
   }

   void method2368() {
      int[] ints_1 = new int[this.field1364];
      int[] ints_2 = new int[33];

      int i_3;
      int i_4;
      int i_5;
      int i_6;
      int i_7;
      int i_8;
      int i_10;
      for (i_3 = 0; i_3 < this.field1364; i_3++) {
         i_4 = this.field1362[i_3];
         if (i_4 != 0) {
            i_5 = 1 << 32 - i_4;
            i_6 = ints_2[i_4];
            ints_1[i_3] = i_6;
            int i_9;
            if ((i_6 & i_5) != 0) {
               i_7 = ints_2[i_4 - 1];
            } else {
               i_7 = i_6 | i_5;

               for (i_8 = i_4 - 1; i_8 >= 1; --i_8) {
                  i_9 = ints_2[i_8];
                  if (i_9 != i_6) {
                     break;
                  }

                  i_10 = 1 << 32 - i_8;
                  if ((i_9 & i_10) != 0) {
                     ints_2[i_8] = ints_2[i_8 - 1];
                     break;
                  }

                  ints_2[i_8] = i_9 | i_10;
               }
            }

            ints_2[i_4] = i_7;

            for (i_8 = i_4 + 1; i_8 <= 32; i_8++) {
               i_9 = ints_2[i_8];
               if (i_9 == i_6) {
                  ints_2[i_8] = i_7;
               }
            }
         }
      }

      this.field1365 = new int[8];
      int i_11 = 0;

      for (i_3 = 0; i_3 < this.field1364; i_3++) {
         i_4 = this.field1362[i_3];
         if (i_4 != 0) {
            i_5 = ints_1[i_3];
            i_6 = 0;

            for (i_7 = 0; i_7 < i_4; i_7++) {
               i_8 = Integer.MIN_VALUE >>> i_7;
               if ((i_5 & i_8) != 0) {
                  if (this.field1365[i_6] == 0) {
                     this.field1365[i_6] = i_11;
                  }

                  i_6 = this.field1365[i_6];
               } else {
                  ++i_6;
               }

               if (i_6 >= this.field1365.length) {
                  int[] ints_12 = new int[this.field1365.length * 2];

                  for (i_10 = 0; i_10 < this.field1365.length; i_10++) {
                     ints_12[i_10] = this.field1365[i_10];
                  }

                  this.field1365 = ints_12;
               }

               i_8 >>>= 1;
            }

            this.field1365[i_6] = ~i_3;
            if (i_6 >= i_11) {
               i_11 = i_6 + 1;
            }
         }
      }

   }

   static int method2369(int i_0, int i_1) {
      int i_2 = (int)Math.pow((double)i_0, 1.0D / (double)i_1) + 1;

      while (true) {
         int i_4 = i_2;
         int i_5 = i_1;

         int i_6;
         for (i_6 = 1; i_5 > 1; i_5 >>= 1) {
            if ((i_5 & 0x1) != 0) {
               i_6 *= i_4;
            }

            i_4 *= i_4;
         }

         int i_3;
         if (i_5 == 1) {
            i_3 = i_6 * i_4;
         } else {
            i_3 = i_6;
         }

         if (i_3 <= i_0) {
            return i_2;
         }

         --i_2;
      }
   }

}
