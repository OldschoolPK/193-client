public class class122 extends class124 {

   int field1487;
   int field1481;
   boolean field1489;
   int field1488;
   int field1480;
   int field1482;
   int field1479;
   int field1490;
   int field1491;
   int field1492;
   int field1493;
   int field1486;
   int field1483;
   int field1484;
   int field1485;

   class122(class112 class112_1, int i_2, int i_3, int i_4) {
      super.field1506 = class112_1;
      this.field1487 = class112_1.field1404;
      this.field1481 = class112_1.field1406;
      this.field1489 = class112_1.field1408;
      this.field1488 = i_2;
      this.field1480 = i_3;
      this.field1482 = i_4;
      this.field1479 = 0;
      this.method2580();
   }

   class122(class112 class112_1, int i_2, int i_3) {
      super.field1506 = class112_1;
      this.field1487 = class112_1.field1404;
      this.field1481 = class112_1.field1406;
      this.field1489 = class112_1.field1408;
      this.field1488 = i_2;
      this.field1480 = i_3;
      this.field1482 = 8192;
      this.field1479 = 0;
      this.method2580();
   }

   public synchronized int method2586() {
      return this.field1482 < 0 ? -1 : this.field1482;
   }

   synchronized void method2583(int i_1) {
      this.method2620(i_1, this.method2586());
   }

   synchronized void method2620(int i_1, int i_2) {
      this.field1480 = i_1;
      this.field1482 = i_2;
      this.field1490 = 0;
      this.method2580();
   }

   public synchronized void method2716(int i_1, int i_2, int i_3) {
      if (i_1 == 0) {
         this.method2620(i_2, i_3);
      } else {
         int i_4 = method2641(i_2, i_3);
         int i_5 = method2575(i_2, i_3);
         if (i_4 == this.field1484 && i_5 == this.field1485) {
            this.field1490 = 0;
         } else {
            int i_6 = i_2 - this.field1483;
            if (this.field1483 - i_2 > i_6) {
               i_6 = this.field1483 - i_2;
            }

            if (i_4 - this.field1484 > i_6) {
               i_6 = i_4 - this.field1484;
            }

            if (this.field1484 - i_4 > i_6) {
               i_6 = this.field1484 - i_4;
            }

            if (i_5 - this.field1485 > i_6) {
               i_6 = i_5 - this.field1485;
            }

            if (this.field1485 - i_5 > i_6) {
               i_6 = this.field1485 - i_5;
            }

            if (i_1 > i_6) {
               i_1 = i_6;
            }

            this.field1490 = i_1;
            this.field1480 = i_2;
            this.field1482 = i_3;
            this.field1491 = (i_2 - this.field1483) / i_1;
            this.field1492 = (i_4 - this.field1484) / i_1;
            this.field1493 = (i_5 - this.field1485) / i_1;
         }
      }
   }

   public synchronized void vmethod3905(int[] ints_1, int i_2, int i_3) {
      if (this.field1480 == 0 && this.field1490 == 0) {
         this.vmethod3898(i_3);
      } else {
         class112 class112_4 = (class112) super.field1506;
         int i_5 = this.field1487 << 8;
         int i_6 = this.field1481 << 8;
         int i_7 = class112_4.field1405.length << 8;
         int i_8 = i_6 - i_5;
         if (i_8 <= 0) {
            this.field1486 = 0;
         }

         int i_9 = i_2;
         i_3 += i_2;
         if (this.field1479 < 0) {
            if (this.field1488 <= 0) {
               this.method2589();
               this.method3628();
               return;
            }

            this.field1479 = 0;
         }

         if (this.field1479 >= i_7) {
            if (this.field1488 >= 0) {
               this.method2589();
               this.method3628();
               return;
            }

            this.field1479 = i_7 - 1;
         }

         if (this.field1486 < 0) {
            if (this.field1489) {
               if (this.field1488 < 0) {
                  i_9 = this.method2603(ints_1, i_2, i_5, i_3, class112_4.field1405[this.field1487]);
                  if (this.field1479 >= i_5) {
                     return;
                  }

                  this.field1479 = i_5 + i_5 - 1 - this.field1479;
                  this.field1488 = -this.field1488;
               }

               while (true) {
                  i_9 = this.method2579(ints_1, i_9, i_6, i_3, class112_4.field1405[this.field1481 - 1]);
                  if (this.field1479 < i_6) {
                     return;
                  }

                  this.field1479 = i_6 + i_6 - 1 - this.field1479;
                  this.field1488 = -this.field1488;
                  i_9 = this.method2603(ints_1, i_9, i_5, i_3, class112_4.field1405[this.field1487]);
                  if (this.field1479 >= i_5) {
                     return;
                  }

                  this.field1479 = i_5 + i_5 - 1 - this.field1479;
                  this.field1488 = -this.field1488;
               }
            } else if (this.field1488 < 0) {
               while (true) {
                  i_9 = this.method2603(ints_1, i_9, i_5, i_3, class112_4.field1405[this.field1481 - 1]);
                  if (this.field1479 >= i_5) {
                     return;
                  }

                  this.field1479 = i_6 - 1 - (i_6 - 1 - this.field1479) % i_8;
               }
            } else {
               while (true) {
                  i_9 = this.method2579(ints_1, i_9, i_6, i_3, class112_4.field1405[this.field1487]);
                  if (this.field1479 < i_6) {
                     return;
                  }

                  this.field1479 = i_5 + (this.field1479 - i_5) % i_8;
               }
            }
         } else {
            if (this.field1486 > 0) {
               if (this.field1489) {
                  label132: {
                     if (this.field1488 < 0) {
                        i_9 = this.method2603(ints_1, i_2, i_5, i_3, class112_4.field1405[this.field1487]);
                        if (this.field1479 >= i_5) {
                           return;
                        }

                        this.field1479 = i_5 + i_5 - 1 - this.field1479;
                        this.field1488 = -this.field1488;
                        if (--this.field1486 == 0) {
                           break label132;
                        }
                     }

                     do {
                        i_9 = this.method2579(ints_1, i_9, i_6, i_3, class112_4.field1405[this.field1481 - 1]);
                        if (this.field1479 < i_6) {
                           return;
                        }

                        this.field1479 = i_6 + i_6 - 1 - this.field1479;
                        this.field1488 = -this.field1488;
                        if (--this.field1486 == 0) {
                           break;
                        }

                        i_9 = this.method2603(ints_1, i_9, i_5, i_3, class112_4.field1405[this.field1487]);
                        if (this.field1479 >= i_5) {
                           return;
                        }

                        this.field1479 = i_5 + i_5 - 1 - this.field1479;
                        this.field1488 = -this.field1488;
                     } while (--this.field1486 != 0);
                  }
               } else {
                  int i_10;
                  if (this.field1488 < 0) {
                     while (true) {
                        i_9 = this.method2603(ints_1, i_9, i_5, i_3, class112_4.field1405[this.field1481 - 1]);
                        if (this.field1479 >= i_5) {
                           return;
                        }

                        i_10 = (i_6 - 1 - this.field1479) / i_8;
                        if (i_10 >= this.field1486) {
                           this.field1479 += i_8 * this.field1486;
                           this.field1486 = 0;
                           break;
                        }

                        this.field1479 += i_8 * i_10;
                        this.field1486 -= i_10;
                     }
                  } else {
                     while (true) {
                        i_9 = this.method2579(ints_1, i_9, i_6, i_3, class112_4.field1405[this.field1487]);
                        if (this.field1479 < i_6) {
                           return;
                        }

                        i_10 = (this.field1479 - i_5) / i_8;
                        if (i_10 >= this.field1486) {
                           this.field1479 -= i_8 * this.field1486;
                           this.field1486 = 0;
                           break;
                        }

                        this.field1479 -= i_8 * i_10;
                        this.field1486 -= i_10;
                     }
                  }
               }
            }

            if (this.field1488 < 0) {
               this.method2603(ints_1, i_9, 0, i_3, 0);
               if (this.field1479 < 0) {
                  this.field1479 = -1;
                  this.method2589();
                  this.method3628();
               }
            } else {
               this.method2579(ints_1, i_9, i_7, i_3, 0);
               if (this.field1479 >= i_7) {
                  this.field1479 = i_7;
                  this.method2589();
                  this.method3628();
               }
            }

         }
      }
   }

   public synchronized void vmethod3898(int i_1) {
      if (this.field1490 > 0) {
         if (i_1 >= this.field1490) {
            if (this.field1480 == Integer.MIN_VALUE) {
               this.field1480 = 0;
               this.field1485 = 0;
               this.field1484 = 0;
               this.field1483 = 0;
               this.method3628();
               i_1 = this.field1490;
            }

            this.field1490 = 0;
            this.method2580();
         } else {
            this.field1483 += this.field1491 * i_1;
            this.field1484 += this.field1492 * i_1;
            this.field1485 += this.field1493 * i_1;
            this.field1490 -= i_1;
         }
      }

      class112 class112_2 = (class112) super.field1506;
      int i_3 = this.field1487 << 8;
      int i_4 = this.field1481 << 8;
      int i_5 = class112_2.field1405.length << 8;
      int i_6 = i_4 - i_3;
      if (i_6 <= 0) {
         this.field1486 = 0;
      }

      if (this.field1479 < 0) {
         if (this.field1488 <= 0) {
            this.method2589();
            this.method3628();
            return;
         }

         this.field1479 = 0;
      }

      if (this.field1479 >= i_5) {
         if (this.field1488 >= 0) {
            this.method2589();
            this.method3628();
            return;
         }

         this.field1479 = i_5 - 1;
      }

      this.field1479 += this.field1488 * i_1;
      if (this.field1486 < 0) {
         if (!this.field1489) {
            if (this.field1488 < 0) {
               if (this.field1479 >= i_3) {
                  return;
               }

               this.field1479 = i_4 - 1 - (i_4 - 1 - this.field1479) % i_6;
            } else {
               if (this.field1479 < i_4) {
                  return;
               }

               this.field1479 = i_3 + (this.field1479 - i_3) % i_6;
            }

         } else {
            if (this.field1488 < 0) {
               if (this.field1479 >= i_3) {
                  return;
               }

               this.field1479 = i_3 + i_3 - 1 - this.field1479;
               this.field1488 = -this.field1488;
            }

            while (this.field1479 >= i_4) {
               this.field1479 = i_4 + i_4 - 1 - this.field1479;
               this.field1488 = -this.field1488;
               if (this.field1479 >= i_3) {
                  return;
               }

               this.field1479 = i_3 + i_3 - 1 - this.field1479;
               this.field1488 = -this.field1488;
            }

         }
      } else {
         if (this.field1486 > 0) {
            if (this.field1489) {
               label126: {
                  if (this.field1488 < 0) {
                     if (this.field1479 >= i_3) {
                        return;
                     }

                     this.field1479 = i_3 + i_3 - 1 - this.field1479;
                     this.field1488 = -this.field1488;
                     if (--this.field1486 == 0) {
                        break label126;
                     }
                  }

                  do {
                     if (this.field1479 < i_4) {
                        return;
                     }

                     this.field1479 = i_4 + i_4 - 1 - this.field1479;
                     this.field1488 = -this.field1488;
                     if (--this.field1486 == 0) {
                        break;
                     }

                     if (this.field1479 >= i_3) {
                        return;
                     }

                     this.field1479 = i_3 + i_3 - 1 - this.field1479;
                     this.field1488 = -this.field1488;
                  } while (--this.field1486 != 0);
               }
            } else {
               label158: {
                  int i_7;
                  if (this.field1488 < 0) {
                     if (this.field1479 >= i_3) {
                        return;
                     }

                     i_7 = (i_4 - 1 - this.field1479) / i_6;
                     if (i_7 >= this.field1486) {
                        this.field1479 += i_6 * this.field1486;
                        this.field1486 = 0;
                        break label158;
                     }

                     this.field1479 += i_6 * i_7;
                     this.field1486 -= i_7;
                  } else {
                     if (this.field1479 < i_4) {
                        return;
                     }

                     i_7 = (this.field1479 - i_3) / i_6;
                     if (i_7 >= this.field1486) {
                        this.field1479 -= i_6 * this.field1486;
                        this.field1486 = 0;
                        break label158;
                     }

                     this.field1479 -= i_6 * i_7;
                     this.field1486 -= i_7;
                  }

                  return;
               }
            }
         }

         if (this.field1488 < 0) {
            if (this.field1479 < 0) {
               this.field1479 = -1;
               this.method2589();
               this.method3628();
            }
         } else if (this.field1479 >= i_5) {
            this.field1479 = i_5;
            this.method2589();
            this.method3628();
         }

      }
   }

   void method2580() {
      this.field1483 = this.field1480;
      this.field1484 = method2641(this.field1480, this.field1482);
      this.field1485 = method2575(this.field1480, this.field1482);
   }

   public synchronized void method2587(int i_1) {
      int i_2 = ((class112) super.field1506).field1405.length << 8;
      if (i_1 < -1) {
         i_1 = -1;
      }

      if (i_1 > i_2) {
         i_1 = i_2;
      }

      this.field1479 = i_1;
   }

   protected class124 vmethod3917() {
      return null;
   }

   public boolean method2595() {
      return this.field1479 < 0 || this.field1479 >= ((class112) super.field1506).field1405.length << 8;
   }

   public synchronized void method2593(int i_1) {
      if (this.field1488 < 0) {
         this.field1488 = -i_1;
      } else {
         this.field1488 = i_1;
      }

   }

   public synchronized void method2592(int i_1) {
      if (i_1 == 0) {
         this.method2583(0);
         this.method3628();
      } else if (this.field1484 == 0 && this.field1485 == 0) {
         this.field1490 = 0;
         this.field1480 = 0;
         this.field1483 = 0;
         this.method3628();
      } else {
         int i_2 = -this.field1483;
         if (this.field1483 > i_2) {
            i_2 = this.field1483;
         }

         if (-this.field1484 > i_2) {
            i_2 = -this.field1484;
         }

         if (this.field1484 > i_2) {
            i_2 = this.field1484;
         }

         if (-this.field1485 > i_2) {
            i_2 = -this.field1485;
         }

         if (this.field1485 > i_2) {
            i_2 = this.field1485;
         }

         if (i_1 > i_2) {
            i_1 = i_2;
         }

         this.field1490 = i_1;
         this.field1480 = Integer.MIN_VALUE;
         this.field1491 = -this.field1483 / i_1;
         this.field1492 = -this.field1484 / i_1;
         this.field1493 = -this.field1485 / i_1;
      }
   }

   public boolean method2596() {
      return this.field1490 != 0;
   }

   void method2589() {
      if (this.field1490 != 0) {
         if (this.field1480 == Integer.MIN_VALUE) {
            this.field1480 = 0;
         }

         this.field1490 = 0;
         this.method2580();
      }

   }

   public synchronized void method2588(boolean bool_1) {
      this.field1488 = (this.field1488 ^ this.field1488 >> 31) + (this.field1488 >>> 31);
      this.field1488 = -this.field1488;
   }

   protected class124 vmethod3894() {
      return null;
   }

   int vmethod2772() {
      int i_1 = this.field1483 * 3 >> 6;
      i_1 = (i_1 ^ i_1 >> 31) + (i_1 >>> 31);
      if (this.field1486 == 0) {
         i_1 -= i_1 * this.field1479 / (((class112) super.field1506).field1405.length << 8);
      } else if (this.field1486 >= 0) {
         i_1 -= i_1 * this.field1487 / ((class112) super.field1506).field1405.length;
      }

      return i_1 > 255 ? 255 : i_1;
   }

   int method2603(int[] ints_1, int i_2, int i_3, int i_4, int i_5) {
      while (true) {
         if (this.field1490 > 0) {
            int i_6 = i_2 + this.field1490;
            if (i_6 > i_4) {
               i_6 = i_4;
            }

            this.field1490 += i_2;
            if (this.field1488 == -256 && (this.field1479 & 0xff) == 0) {
               if (class114.field1421) {
                  i_2 = method2616(0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, this.field1492, this.field1493, 0, i_6, i_3, this);
               } else {
                  i_2 = method2615(((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, this.field1491, 0, i_6, i_3, this);
               }
            } else if (class114.field1421) {
               i_2 = method2635(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, this.field1492, this.field1493, 0, i_6, i_3, this, this.field1488, i_5);
            } else {
               i_2 = method2591(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, this.field1491, 0, i_6, i_3, this, this.field1488, i_5);
            }

            this.field1490 -= i_2;
            if (this.field1490 != 0) {
               return i_2;
            }

            if (!this.method2604()) {
               continue;
            }

            return i_4;
         }

         if (this.field1488 == -256 && (this.field1479 & 0xff) == 0) {
            if (class114.field1421) {
               return method2608(0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, 0, i_4, i_3, this);
            }

            return method2577(((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, 0, i_4, i_3, this);
         }

         if (class114.field1421) {
            return method2612(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, 0, i_4, i_3, this, this.field1488, i_5);
         }

         return method2611(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, 0, i_4, i_3, this, this.field1488, i_5);
      }
   }

   public synchronized int method2594() {
      return this.field1488 < 0 ? -this.field1488 : this.field1488;
   }

   boolean method2604() {
      int i_1 = this.field1480;
      int i_2;
      int i_3;
      if (i_1 == Integer.MIN_VALUE) {
         i_3 = 0;
         i_2 = 0;
         i_1 = 0;
      } else {
         i_2 = method2641(i_1, this.field1482);
         i_3 = method2575(i_1, this.field1482);
      }

      if (i_1 == this.field1483 && i_2 == this.field1484 && i_3 == this.field1485) {
         if (this.field1480 == Integer.MIN_VALUE) {
            this.field1480 = 0;
            this.field1485 = 0;
            this.field1484 = 0;
            this.field1483 = 0;
            this.method3628();
            return true;
         } else {
            this.method2580();
            return false;
         }
      } else {
         if (this.field1483 < i_1) {
            this.field1491 = 1;
            this.field1490 = i_1 - this.field1483;
         } else if (this.field1483 > i_1) {
            this.field1491 = -1;
            this.field1490 = this.field1483 - i_1;
         } else {
            this.field1491 = 0;
         }

         if (this.field1484 < i_2) {
            this.field1492 = 1;
            if (this.field1490 == 0 || this.field1490 > i_2 - this.field1484) {
               this.field1490 = i_2 - this.field1484;
            }
         } else if (this.field1484 > i_2) {
            this.field1492 = -1;
            if (this.field1490 == 0 || this.field1490 > this.field1484 - i_2) {
               this.field1490 = this.field1484 - i_2;
            }
         } else {
            this.field1492 = 0;
         }

         if (this.field1485 < i_3) {
            this.field1493 = 1;
            if (this.field1490 == 0 || this.field1490 > i_3 - this.field1485) {
               this.field1490 = i_3 - this.field1485;
            }
         } else if (this.field1485 > i_3) {
            this.field1493 = -1;
            if (this.field1490 == 0 || this.field1490 > this.field1485 - i_3) {
               this.field1490 = this.field1485 - i_3;
            }
         } else {
            this.field1493 = 0;
         }

         return false;
      }
   }

   int method2579(int[] ints_1, int i_2, int i_3, int i_4, int i_5) {
      while (true) {
         if (this.field1490 > 0) {
            int i_6 = i_2 + this.field1490;
            if (i_6 > i_4) {
               i_6 = i_4;
            }

            this.field1490 += i_2;
            if (this.field1488 == 256 && (this.field1479 & 0xff) == 0) {
               if (class114.field1421) {
                  i_2 = method2614(0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, this.field1492, this.field1493, 0, i_6, i_3, this);
               } else {
                  i_2 = method2613(((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, this.field1491, 0, i_6, i_3, this);
               }
            } else if (class114.field1421) {
               i_2 = method2618(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, this.field1492, this.field1493, 0, i_6, i_3, this, this.field1488, i_5);
            } else {
               i_2 = method2621(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, this.field1491, 0, i_6, i_3, this, this.field1488, i_5);
            }

            this.field1490 -= i_2;
            if (this.field1490 != 0) {
               return i_2;
            }

            if (!this.method2604()) {
               continue;
            }

            return i_4;
         }

         if (this.field1488 == 256 && (this.field1479 & 0xff) == 0) {
            if (class114.field1421) {
               return method2737(0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, 0, i_4, i_3, this);
            }

            return method2598(((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, 0, i_4, i_3, this);
         }

         if (class114.field1421) {
            return method2610(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1484, this.field1485, 0, i_4, i_3, this, this.field1488, i_5);
         }

         return method2711(0, 0, ((class112) super.field1506).field1405, ints_1, this.field1479, i_2, this.field1483, 0, i_4, i_3, this, this.field1488, i_5);
      }
   }

   public synchronized int method2585() {
      return this.field1480 == Integer.MIN_VALUE ? 0 : this.field1480;
   }

   public synchronized void method2581(int i_1) {
      this.field1486 = i_1;
   }

   protected int vmethod3896() {
      return this.field1480 == 0 && this.field1490 == 0 ? 0 : 1;
   }

   public synchronized void method2679(int i_1, int i_2) {
      this.method2716(i_1, i_2, this.method2586());
   }

   public synchronized void method2582(int i_1) {
      this.method2620(i_1 << 6, this.method2586());
   }

   static int method2641(int i_0, int i_1) {
      return i_1 < 0 ? i_0 : (int)((double)i_0 * Math.sqrt((double)(16384 - i_1) * 1.220703125E-4D) + 0.5D);
   }

   static int method2575(int i_0, int i_1) {
      return i_1 < 0 ? -i_0 : (int)((double)i_0 * Math.sqrt((double)i_1 * 1.220703125E-4D) + 0.5D);
   }

   static int method2711(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, class122 class122_10, int i_11, int i_12) {
      if (i_11 == 0 || (i_7 = i_5 + (i_11 + (i_9 - i_4) - 257) / i_11) > i_8) {
         i_7 = i_8;
      }

      byte b_13;
      int i_10001;
      while (i_5 < i_7) {
         i_1 = i_4 >> 8;
         b_13 = bytes_2[i_1];
         i_10001 = i_5++;
         ints_3[i_10001] += ((b_13 << 8) + (bytes_2[i_1 + 1] - b_13) * (i_4 & 0xff)) * i_6 >> 6;
         i_4 += i_11;
      }

      if (i_11 == 0 || (i_7 = i_5 + (i_11 + (i_9 - i_4) - 1) / i_11) > i_8) {
         i_7 = i_8;
      }

      for (i_1 = i_12; i_5 < i_7; i_4 += i_11) {
         b_13 = bytes_2[i_4 >> 8];
         i_10001 = i_5++;
         ints_3[i_10001] += ((b_13 << 8) + (i_1 - b_13) * (i_4 & 0xff)) * i_6 >> 6;
      }

      class122_10.field1479 = i_4;
      return i_5;
   }

   static int method2611(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, class122 class122_10, int i_11, int i_12) {
      if (i_11 == 0 || (i_7 = i_5 + (i_11 + (i_9 + 256 - i_4)) / i_11) > i_8) {
         i_7 = i_8;
      }

      int i_10001;
      while (i_5 < i_7) {
         i_1 = i_4 >> 8;
         byte b_13 = bytes_2[i_1 - 1];
         i_10001 = i_5++;
         ints_3[i_10001] += ((b_13 << 8) + (bytes_2[i_1] - b_13) * (i_4 & 0xff)) * i_6 >> 6;
         i_4 += i_11;
      }

      if (i_11 == 0 || (i_7 = i_5 + (i_11 + (i_9 - i_4)) / i_11) > i_8) {
         i_7 = i_8;
      }

      i_0 = i_12;

      for (i_1 = i_11; i_5 < i_7; i_4 += i_1) {
         i_10001 = i_5++;
         ints_3[i_10001] += ((i_0 << 8) + (bytes_2[i_4 >> 8] - i_0) * (i_4 & 0xff)) * i_6 >> 6;
      }

      class122_10.field1479 = i_4;
      return i_5;
   }

   static int method2598(byte[] bytes_0, int[] ints_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, class122 class122_8) {
      i_2 >>= 8;
      i_7 >>= 8;
      i_4 <<= 2;
      if ((i_5 = i_3 + i_7 - i_2) > i_6) {
         i_5 = i_6;
      }

      int i_10001;
      for (i_5 -= 3; i_3 < i_5; ints_1[i_10001] += bytes_0[i_2++] * i_4) {
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
         i_10001 = i_3++;
      }

      for (i_5 += 3; i_3 < i_5; ints_1[i_10001] += bytes_0[i_2++] * i_4) {
         i_10001 = i_3++;
      }

      class122_8.field1479 = i_2 << 8;
      return i_3;
   }

   static int method2610(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, class122 class122_11, int i_12, int i_13) {
      if (i_12 == 0 || (i_8 = i_5 + (i_10 - i_4 + i_12 - 257) / i_12) > i_9) {
         i_8 = i_9;
      }

      i_5 <<= 1;

      byte b_14;
      int i_10001;
      for (i_8 <<= 1; i_5 < i_8; i_4 += i_12) {
         i_1 = i_4 >> 8;
         b_14 = bytes_2[i_1];
         i_0 = (b_14 << 8) + (i_4 & 0xff) * (bytes_2[i_1 + 1] - b_14);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
      }

      if (i_12 == 0 || (i_8 = (i_5 >> 1) + (i_10 - i_4 + i_12 - 1) / i_12) > i_9) {
         i_8 = i_9;
      }

      i_8 <<= 1;

      for (i_1 = i_13; i_5 < i_8; i_4 += i_12) {
         b_14 = bytes_2[i_4 >> 8];
         i_0 = (b_14 << 8) + (i_1 - b_14) * (i_4 & 0xff);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
      }

      class122_11.field1479 = i_4;
      return i_5 >> 1;
   }

   static int method2577(byte[] bytes_0, int[] ints_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, class122 class122_8) {
      i_2 >>= 8;
      i_7 >>= 8;
      i_4 <<= 2;
      if ((i_5 = i_3 + i_2 - (i_7 - 1)) > i_6) {
         i_5 = i_6;
      }

      int i_10001;
      for (i_5 -= 3; i_3 < i_5; ints_1[i_10001] += bytes_0[i_2--] * i_4) {
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
         i_10001 = i_3++;
      }

      for (i_5 += 3; i_3 < i_5; ints_1[i_10001] += bytes_0[i_2--] * i_4) {
         i_10001 = i_3++;
      }

      class122_8.field1479 = i_2 << 8;
      return i_3;
   }

   static int method2612(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, class122 class122_11, int i_12, int i_13) {
      if (i_12 == 0 || (i_8 = i_5 + (i_10 + 256 - i_4 + i_12) / i_12) > i_9) {
         i_8 = i_9;
      }

      i_5 <<= 1;

      int i_10001;
      for (i_8 <<= 1; i_5 < i_8; i_4 += i_12) {
         i_1 = i_4 >> 8;
         byte b_14 = bytes_2[i_1 - 1];
         i_0 = (bytes_2[i_1] - b_14) * (i_4 & 0xff) + (b_14 << 8);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
      }

      if (i_12 == 0 || (i_8 = (i_5 >> 1) + (i_10 - i_4 + i_12) / i_12) > i_9) {
         i_8 = i_9;
      }

      i_8 <<= 1;

      for (i_1 = i_13; i_5 < i_8; i_4 += i_12) {
         i_0 = (i_1 << 8) + (i_4 & 0xff) * (bytes_2[i_4 >> 8] - i_1);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
      }

      class122_11.field1479 = i_4;
      return i_5 >> 1;
   }

   static int method2737(int i_0, byte[] bytes_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, class122 class122_10) {
      i_3 >>= 8;
      i_9 >>= 8;
      i_5 <<= 2;
      i_6 <<= 2;
      if ((i_7 = i_4 + i_9 - i_3) > i_8) {
         i_7 = i_8;
      }

      i_4 <<= 1;
      i_7 <<= 1;

      int i_10001;
      byte b_11;
      for (i_7 -= 6; i_4 < i_7; ints_2[i_10001] += b_11 * i_6) {
         b_11 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_6;
         b_11 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_6;
         b_11 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_6;
         b_11 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
      }

      for (i_7 += 6; i_4 < i_7; ints_2[i_10001] += b_11 * i_6) {
         b_11 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
      }

      class122_10.field1479 = i_3 << 8;
      return i_4 >> 1;
   }

   static int method2608(int i_0, byte[] bytes_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, class122 class122_10) {
      i_3 >>= 8;
      i_9 >>= 8;
      i_5 <<= 2;
      i_6 <<= 2;
      if ((i_7 = i_3 + i_4 - (i_9 - 1)) > i_8) {
         i_7 = i_8;
      }

      i_4 <<= 1;
      i_7 <<= 1;

      int i_10001;
      byte b_11;
      for (i_7 -= 6; i_4 < i_7; ints_2[i_10001] += b_11 * i_6) {
         b_11 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_6;
         b_11 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_6;
         b_11 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_6;
         b_11 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
      }

      for (i_7 += 6; i_4 < i_7; ints_2[i_10001] += b_11 * i_6) {
         b_11 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_11 * i_5;
         i_10001 = i_4++;
      }

      class122_10.field1479 = i_3 << 8;
      return i_4 >> 1;
   }

   static int method2621(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, class122 class122_11, int i_12, int i_13) {
      class122_11.field1484 -= class122_11.field1492 * i_5;
      class122_11.field1485 -= class122_11.field1493 * i_5;
      if (i_12 == 0 || (i_8 = i_5 + (i_10 - i_4 + i_12 - 257) / i_12) > i_9) {
         i_8 = i_9;
      }

      byte b_14;
      int i_10001;
      while (i_5 < i_8) {
         i_1 = i_4 >> 8;
         b_14 = bytes_2[i_1];
         i_10001 = i_5++;
         ints_3[i_10001] += ((b_14 << 8) + (bytes_2[i_1 + 1] - b_14) * (i_4 & 0xff)) * i_6 >> 6;
         i_6 += i_7;
         i_4 += i_12;
      }

      if (i_12 == 0 || (i_8 = i_5 + (i_10 - i_4 + i_12 - 1) / i_12) > i_9) {
         i_8 = i_9;
      }

      for (i_1 = i_13; i_5 < i_8; i_4 += i_12) {
         b_14 = bytes_2[i_4 >> 8];
         i_10001 = i_5++;
         ints_3[i_10001] += ((b_14 << 8) + (i_1 - b_14) * (i_4 & 0xff)) * i_6 >> 6;
         i_6 += i_7;
      }

      class122_11.field1484 += class122_11.field1492 * i_5;
      class122_11.field1485 += class122_11.field1493 * i_5;
      class122_11.field1483 = i_6;
      class122_11.field1479 = i_4;
      return i_5;
   }

   static int method2591(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, class122 class122_11, int i_12, int i_13) {
      class122_11.field1484 -= class122_11.field1492 * i_5;
      class122_11.field1485 -= class122_11.field1493 * i_5;
      if (i_12 == 0 || (i_8 = i_5 + (i_10 + 256 - i_4 + i_12) / i_12) > i_9) {
         i_8 = i_9;
      }

      int i_10001;
      while (i_5 < i_8) {
         i_1 = i_4 >> 8;
         byte b_14 = bytes_2[i_1 - 1];
         i_10001 = i_5++;
         ints_3[i_10001] += ((b_14 << 8) + (bytes_2[i_1] - b_14) * (i_4 & 0xff)) * i_6 >> 6;
         i_6 += i_7;
         i_4 += i_12;
      }

      if (i_12 == 0 || (i_8 = i_5 + (i_10 - i_4 + i_12) / i_12) > i_9) {
         i_8 = i_9;
      }

      i_0 = i_13;

      for (i_1 = i_12; i_5 < i_8; i_4 += i_1) {
         i_10001 = i_5++;
         ints_3[i_10001] += ((i_0 << 8) + (bytes_2[i_4 >> 8] - i_0) * (i_4 & 0xff)) * i_6 >> 6;
         i_6 += i_7;
      }

      class122_11.field1484 += class122_11.field1492 * i_5;
      class122_11.field1485 += class122_11.field1493 * i_5;
      class122_11.field1483 = i_6;
      class122_11.field1479 = i_4;
      return i_5;
   }

   static int method2613(byte[] bytes_0, int[] ints_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, class122 class122_9) {
      i_2 >>= 8;
      i_8 >>= 8;
      i_4 <<= 2;
      i_5 <<= 2;
      if ((i_6 = i_3 + i_8 - i_2) > i_7) {
         i_6 = i_7;
      }

      class122_9.field1484 += class122_9.field1492 * (i_6 - i_3);
      class122_9.field1485 += class122_9.field1493 * (i_6 - i_3);

      int i_10001;
      for (i_6 -= 3; i_3 < i_6; i_4 += i_5) {
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
         i_4 += i_5;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
         i_4 += i_5;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
         i_4 += i_5;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
      }

      for (i_6 += 3; i_3 < i_6; i_4 += i_5) {
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2++] * i_4;
      }

      class122_9.field1483 = i_4 >> 2;
      class122_9.field1479 = i_2 << 8;
      return i_3;
   }

   static int method2615(byte[] bytes_0, int[] ints_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, class122 class122_9) {
      i_2 >>= 8;
      i_8 >>= 8;
      i_4 <<= 2;
      i_5 <<= 2;
      if ((i_6 = i_3 + i_2 - (i_8 - 1)) > i_7) {
         i_6 = i_7;
      }

      class122_9.field1484 += class122_9.field1492 * (i_6 - i_3);
      class122_9.field1485 += class122_9.field1493 * (i_6 - i_3);

      int i_10001;
      for (i_6 -= 3; i_3 < i_6; i_4 += i_5) {
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
         i_4 += i_5;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
         i_4 += i_5;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
         i_4 += i_5;
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
      }

      for (i_6 += 3; i_3 < i_6; i_4 += i_5) {
         i_10001 = i_3++;
         ints_1[i_10001] += bytes_0[i_2--] * i_4;
      }

      class122_9.field1483 = i_4 >> 2;
      class122_9.field1479 = i_2 << 8;
      return i_3;
   }

   static int method2618(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11, int i_12, class122 class122_13, int i_14, int i_15) {
      class122_13.field1483 -= i_5 * class122_13.field1491;
      if (i_14 == 0 || (i_10 = i_5 + (i_12 - i_4 + i_14 - 257) / i_14) > i_11) {
         i_10 = i_11;
      }

      i_5 <<= 1;

      byte b_16;
      int i_10001;
      for (i_10 <<= 1; i_5 < i_10; i_4 += i_14) {
         i_1 = i_4 >> 8;
         b_16 = bytes_2[i_1];
         i_0 = (b_16 << 8) + (i_4 & 0xff) * (bytes_2[i_1 + 1] - b_16);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_6 += i_8;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
         i_7 += i_9;
      }

      if (i_14 == 0 || (i_10 = (i_5 >> 1) + (i_12 - i_4 + i_14 - 1) / i_14) > i_11) {
         i_10 = i_11;
      }

      i_10 <<= 1;

      for (i_1 = i_15; i_5 < i_10; i_4 += i_14) {
         b_16 = bytes_2[i_4 >> 8];
         i_0 = (b_16 << 8) + (i_1 - b_16) * (i_4 & 0xff);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_6 += i_8;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
         i_7 += i_9;
      }

      i_5 >>= 1;
      class122_13.field1483 += class122_13.field1491 * i_5;
      class122_13.field1484 = i_6;
      class122_13.field1485 = i_7;
      class122_13.field1479 = i_4;
      return i_5;
   }

   static int method2635(int i_0, int i_1, byte[] bytes_2, int[] ints_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11, int i_12, class122 class122_13, int i_14, int i_15) {
      class122_13.field1483 -= i_5 * class122_13.field1491;
      if (i_14 == 0 || (i_10 = i_5 + (i_12 + 256 - i_4 + i_14) / i_14) > i_11) {
         i_10 = i_11;
      }

      i_5 <<= 1;

      int i_10001;
      for (i_10 <<= 1; i_5 < i_10; i_4 += i_14) {
         i_1 = i_4 >> 8;
         byte b_16 = bytes_2[i_1 - 1];
         i_0 = (bytes_2[i_1] - b_16) * (i_4 & 0xff) + (b_16 << 8);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_6 += i_8;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
         i_7 += i_9;
      }

      if (i_14 == 0 || (i_10 = (i_5 >> 1) + (i_12 - i_4 + i_14) / i_14) > i_11) {
         i_10 = i_11;
      }

      i_10 <<= 1;

      for (i_1 = i_15; i_5 < i_10; i_4 += i_14) {
         i_0 = (i_1 << 8) + (i_4 & 0xff) * (bytes_2[i_4 >> 8] - i_1);
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_6 >> 6;
         i_6 += i_8;
         i_10001 = i_5++;
         ints_3[i_10001] += i_0 * i_7 >> 6;
         i_7 += i_9;
      }

      i_5 >>= 1;
      class122_13.field1483 += class122_13.field1491 * i_5;
      class122_13.field1484 = i_6;
      class122_13.field1485 = i_7;
      class122_13.field1479 = i_4;
      return i_5;
   }

   static int method2614(int i_0, byte[] bytes_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11, class122 class122_12) {
      i_3 >>= 8;
      i_11 >>= 8;
      i_5 <<= 2;
      i_6 <<= 2;
      i_7 <<= 2;
      i_8 <<= 2;
      if ((i_9 = i_11 + i_4 - i_3) > i_10) {
         i_9 = i_10;
      }

      class122_12.field1483 += class122_12.field1491 * (i_9 - i_4);
      i_4 <<= 1;
      i_9 <<= 1;

      byte b_13;
      int i_10001;
      for (i_9 -= 6; i_4 < i_9; i_6 += i_8) {
         b_13 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
         i_6 += i_8;
         b_13 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
         i_6 += i_8;
         b_13 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
         i_6 += i_8;
         b_13 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
      }

      for (i_9 += 6; i_4 < i_9; i_6 += i_8) {
         b_13 = bytes_1[i_3++];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
      }

      class122_12.field1484 = i_5 >> 2;
      class122_12.field1485 = i_6 >> 2;
      class122_12.field1479 = i_3 << 8;
      return i_4 >> 1;
   }

   static int method2616(int i_0, byte[] bytes_1, int[] ints_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11, class122 class122_12) {
      i_3 >>= 8;
      i_11 >>= 8;
      i_5 <<= 2;
      i_6 <<= 2;
      i_7 <<= 2;
      i_8 <<= 2;
      if ((i_9 = i_3 + i_4 - (i_11 - 1)) > i_10) {
         i_9 = i_10;
      }

      class122_12.field1483 += class122_12.field1491 * (i_9 - i_4);
      i_4 <<= 1;
      i_9 <<= 1;

      byte b_13;
      int i_10001;
      for (i_9 -= 6; i_4 < i_9; i_6 += i_8) {
         b_13 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
         i_6 += i_8;
         b_13 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
         i_6 += i_8;
         b_13 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
         i_6 += i_8;
         b_13 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
      }

      for (i_9 += 6; i_4 < i_9; i_6 += i_8) {
         b_13 = bytes_1[i_3--];
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_5;
         i_5 += i_7;
         i_10001 = i_4++;
         ints_2[i_10001] += b_13 * i_6;
      }

      class122_12.field1484 = i_5 >> 2;
      class122_12.field1485 = i_6 >> 2;
      class122_12.field1479 = i_3 << 8;
      return i_4 >> 1;
   }

   public static class122 method2602(class112 class112_0, int i_1, int i_2, int i_3) {
      return class112_0.field1405 != null && class112_0.field1405.length != 0 ? new class122(class112_0, i_1, i_2, i_3) : null;
   }

   public static class122 method2578(class112 class112_0, int i_1, int i_2) {
      return class112_0.field1405 != null && class112_0.field1405.length != 0 ? new class122(class112_0, (int)((long)class112_0.field1407 * 256L * (long)i_1 / (long)(class114.field1443 * 100)), i_2 << 6) : null;
   }

}
