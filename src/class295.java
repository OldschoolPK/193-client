public class class295 extends class287 {

   public String field3669 = null;
   public String field3673 = null;
   int field3676 = 1;
   final class348 field3675;
   final class290 field3671;
   public int field3672;
   public byte field3674;

   public class295(class348 class348_1, class290 class290_2) {
      super(100);
      this.field3675 = class348_1;
      this.field3671 = class290_2;
   }

   final void method5235(String string_1, byte b_2) {
      this.field3673 = class238.method4133(string_1, 1585982662);
   }

   class284 vmethod5247(byte b_1) {
      return new class283();
   }

   final void method5212(String string_1, int i_2) {
      this.field3669 = class238.method4133(string_1, 1637976595);
   }

   class284[] vmethod5226(int i_1, int i_2) {
      return new class283[i_1];
   }

   public final void method5218(byte b_1) {
      for (int i_2 = 0; i_2 < this.method5071(1501367235); i_2++) {
         ((class283) this.method5081(i_2, 572864516)).method5003(-1060606481);
      }

   }

   public final void method5219(byte b_1) {
      for (int i_2 = 0; i_2 < this.method5071(1510866905); i_2++) {
         ((class283) this.method5081(i_2, 1977107439)).method5009(-1987329827);
      }

   }

   final void method5220(class283 class283_1, int i_2) {
      if (class283_1.method5038(1670678120).equals(this.field3671.vmethod5165(82536799))) {
         this.field3672 = class283_1.field3651;
      }

   }

   public final void method5211(class310 class310_1, short s_2) {
      class293 class293_3 = new class293(class310_1.method5589(1939633002), this.field3675);
      int i_4 = class310_1.method5729(1592752545);
      byte b_5 = class310_1.method5535((byte) 0);
      boolean bool_6 = false;
      if (b_5 == -128) {
         bool_6 = true;
      }

      class283 class283_7;
      if (bool_6) {
         if (this.method5071(1488862486) == 0) {
            return;
         }

         class283_7 = (class283) this.method5124(class293_3, -2089307567);
         if (class283_7 != null && class283_7.method5155(-1196815789) == i_4) {
            this.method5078(class283_7, -202922237);
         }
      } else {
         class310_1.method5589(-1797309040);
         class283_7 = (class283) this.method5124(class293_3, -2053834091);
         if (class283_7 == null) {
            if (this.method5071(1354608462) > super.field3647) {
               return;
            }

            class283_7 = (class283) this.method5079(class293_3, (byte) 8);
         }

         class283_7.method5156(i_4, ++this.field3676 - 1, 1333742475);
         class283_7.field3651 = b_5;
         this.method5220(class283_7, 353715814);
      }

   }

   public final void method5217(class310 class310_1, int i_2) {
      this.method5235(class310_1.method5589(-1745090323), (byte) -83);
      long long_3 = class310_1.method5508(-1523542150);
      this.method5212(class66.method1218(long_3), -513801120);
      this.field3674 = class310_1.method5535((byte) 0);
      int i_5 = class310_1.method5661((byte) 4);
      if (i_5 != 255) {
         this.method5101((byte) -91);

         for (int i_6 = 0; i_6 < i_5; i_6++) {
            class283 class283_7 = (class283) this.method5079(new class293(class310_1.method5589(606714810), this.field3675), (byte) 8);
            int i_8 = class310_1.method5729(1314454443);
            class283_7.method5156(i_8, ++this.field3676 - 1, 817942248);
            class283_7.field3651 = class310_1.method5535((byte) 0);
            class310_1.method5589(-1925162790);
            this.method5220(class283_7, 56604081);
         }

      }
   }

}
