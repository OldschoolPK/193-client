import java.lang.management.GarbageCollectorMXBean;

public class class25 {

   static class240 field125;
   static class335 field124;
   static class335 field126;
   static GarbageCollectorMXBean field121;
   static int field128;

   public static synchronized byte[] method253(int i_0, int i_1) {
      return class312.method5775(i_0, false, (byte) 1);
   }

   static String method248(int i_0, int i_1) {
      return "<img=" + i_0 + ">";
   }

   static final void method249(int i_0, int i_1, int i_2, int i_3, boolean bool_4, int i_5) {
      if (i_2 < 1) {
         i_2 = 1;
      }

      if (i_3 < 1) {
         i_3 = 1;
      }

      int i_6 = i_3 - 334;
      int i_7;
      if (i_6 < 0) {
         i_7 = client.field896;
      } else if (i_6 >= 100) {
         i_7 = client.field897;
      } else {
         i_7 = (client.field897 - client.field896) * i_6 / 100 + client.field896;
      }

      int i_8 = i_3 * i_7 * 512 / (i_2 * 334);
      int i_9;
      int i_10;
      short s_11;
      if (i_8 < client.field828) {
         s_11 = client.field828;
         i_7 = s_11 * i_2 * 334 / (i_3 * 512);
         if (i_7 > client.field731) {
            i_7 = client.field731;
            i_9 = i_3 * i_7 * 512 / (s_11 * 334);
            i_10 = (i_2 - i_9) / 2;
            if (bool_4) {
               class331.method6047();
               class331.method6004(i_0, i_1, i_10, i_3, -16777216);
               class331.method6004(i_0 + i_2 - i_10, i_1, i_10, i_3, -16777216);
            }

            i_0 += i_10;
            i_2 -= i_10 * 2;
         }
      } else if (i_8 > client.field903) {
         s_11 = client.field903;
         i_7 = s_11 * i_2 * 334 / (i_3 * 512);
         if (i_7 < client.field900) {
            i_7 = client.field900;
            i_9 = s_11 * i_2 * 334 / (i_7 * 512);
            i_10 = (i_3 - i_9) / 2;
            if (bool_4) {
               class331.method6047();
               class331.method6004(i_0, i_1, i_2, i_10, -16777216);
               class331.method6004(i_0, i_3 + i_1 - i_10, i_2, i_10, -16777216);
            }

            i_1 += i_10;
            i_3 -= i_10 * 2;
         }
      }

      client.field908 = i_3 * i_7 / 334;
      if (i_2 != client.field906 || i_3 != client.field695) {
         class39.method593(i_2, i_3, -1974117172);
      }

      client.field904 = i_0;
      client.field905 = i_1;
      client.field906 = i_2;
      client.field695 = i_3;
   }

   static final void method250(int i_0, int i_1, int i_2, int i_3, class335 class335_4, class220 class220_5, int i_6) {
      if (class335_4 != null) {
         int i_7 = client.field830 & 0x7ff;
         int i_8 = i_3 * i_3 + i_2 * i_2;
         if (i_8 <= 6400) {
            int i_9 = class139.field1797[i_7];
            int i_10 = class139.field1780[i_7];
            int i_11 = i_3 * i_9 + i_10 * i_2 >> 16;
            int i_12 = i_3 * i_10 - i_9 * i_2 >> 16;
            if (i_8 > 2500) {
               class335_4.method6191(i_11 + class220_5.field2537 / 2 - class335_4.field3916 / 2, class220_5.field2532 / 2 - i_12 - class335_4.field3915 / 2, i_0, i_1, class220_5.field2537, class220_5.field2532, class220_5.field2531, class220_5.field2533);
            } else {
               class335_4.method6121(i_0 + i_11 + class220_5.field2537 / 2 - class335_4.field3916 / 2, class220_5.field2532 / 2 + i_1 - i_12 - class335_4.field3915 / 2);
            }

         }
      }
   }

   static void method251(boolean bool_0, byte b_1) {
      client.field724 = bool_0;
   }

   static void method252(int i_0, int i_1, int i_2, boolean bool_3, int i_4, boolean bool_5, int i_6) {
      if (i_0 < i_1) {
         int i_7 = (i_0 + i_1) / 2;
         int i_8 = i_0;
         class80 class80_9 = class80.field1012[i_7];
         class80.field1012[i_7] = class80.field1012[i_1];
         class80.field1012[i_1] = class80_9;

         for (int i_10 = i_0; i_10 < i_1; i_10++) {
            class80 class80_12 = class80.field1012[i_10];
            int i_13 = class72.method1266(class80_12, class80_9, i_2, bool_3, 1286095524);
            int i_11;
            if (i_13 != 0) {
               if (bool_3) {
                  i_11 = -i_13;
               } else {
                  i_11 = i_13;
               }
            } else if (i_4 == -1) {
               i_11 = 0;
            } else {
               int i_14 = class72.method1266(class80_12, class80_9, i_4, bool_5, 88497125);
               if (bool_5) {
                  i_11 = -i_14;
               } else {
                  i_11 = i_14;
               }
            }

            if (i_11 <= 0) {
               class80 class80_15 = class80.field1012[i_10];
               class80.field1012[i_10] = class80.field1012[i_8];
               class80.field1012[i_8++] = class80_15;
            }
         }

         class80.field1012[i_1] = class80.field1012[i_8];
         class80.field1012[i_8] = class80_9;
         method252(i_0, i_8 - 1, i_2, bool_3, i_4, bool_5, -764881537);
         method252(i_8 + 1, i_1, i_2, bool_3, i_4, bool_5, 526773642);
      }

   }

}
