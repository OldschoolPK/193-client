public class class115 {

   int field1454 = 2;
   int[] field1445 = new int[2];
   int[] field1453 = new int[2];
   int field1444;
   int field1447;
   int field1448;
   int field1450;
   int field1451;
   int field1452;
   int field1449;
   int field1446;

   class115() {
      this.field1445[0] = 0;
      this.field1445[1] = 65535;
      this.field1453[0] = 0;
      this.field1453[1] = 65535;
   }

   final void method2531(class310 class310_1) {
      this.field1444 = class310_1.method5661((byte) 22);
      this.field1447 = class310_1.method5507(2095791721);
      this.field1448 = class310_1.method5507(556155546);
      this.method2520(class310_1);
   }

   final void method2521() {
      this.field1450 = 0;
      this.field1451 = 0;
      this.field1452 = 0;
      this.field1449 = 0;
      this.field1446 = 0;
   }

   final void method2520(class310 class310_1) {
      this.field1454 = class310_1.method5661((byte) 59);
      this.field1445 = new int[this.field1454];
      this.field1453 = new int[this.field1454];

      for (int i_2 = 0; i_2 < this.field1454; i_2++) {
         this.field1445[i_2] = class310_1.method5729(-768854309);
         this.field1453[i_2] = class310_1.method5729(-272555063);
      }

   }

   final int method2522(int i_1) {
      if (this.field1446 >= this.field1450) {
         this.field1449 = this.field1453[this.field1451++] << 15;
         if (this.field1451 >= this.field1454) {
            this.field1451 = this.field1454 - 1;
         }

         this.field1450 = (int)((double)this.field1445[this.field1451] / 65536.0D * (double)i_1);
         if (this.field1450 > this.field1446) {
            this.field1452 = ((this.field1453[this.field1451] << 15) - this.field1449) / (this.field1450 - this.field1446);
         }
      }

      this.field1449 += this.field1452;
      ++this.field1446;
      return this.field1449 - this.field1452 >> 15;
   }

}
