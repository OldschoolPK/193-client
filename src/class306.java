import java.util.Random;

public abstract class class306 extends class331 {

   public static class334[] field3730;
   static int field3734 = -1;
   static int field3733 = -1;
   static int field3739 = -1;
   static int field3721 = -1;
   static int field3736 = 0;
   static int field3737 = 0;
   static int field3738 = 256;
   static int field3722 = 0;
   static int field3732 = 0;
   static Random field3741 = new Random();
   static String[] field3735 = new String[100];
   byte[][] field3740 = new byte[256][];
   public int field3727 = 0;
   int[] field3725;
   int[] field3726;
   int[] field3723;
   int[] field3724;
   public int field3728;
   public int field3729;
   int[] field3731;
   byte[] field3742;

   class306(byte[] bytes_1, int[] ints_2, int[] ints_3, int[] ints_4, int[] ints_5, int[] ints_6, byte[][] bytes_7) {
      this.field3725 = ints_2;
      this.field3726 = ints_3;
      this.field3723 = ints_4;
      this.field3724 = ints_5;
      this.method5337(bytes_1);
      this.field3740 = bytes_7;
      int i_8 = Integer.MAX_VALUE;
      int i_9 = Integer.MIN_VALUE;

      for (int i_10 = 0; i_10 < 256; i_10++) {
         if (this.field3726[i_10] < i_8 && this.field3724[i_10] != 0) {
            i_8 = this.field3726[i_10];
         }

         if (this.field3726[i_10] + this.field3724[i_10] > i_9) {
            i_9 = this.field3726[i_10] + this.field3724[i_10];
         }
      }

      this.field3728 = this.field3727 - i_8;
      this.field3729 = i_9 - this.field3727;
   }

   class306(byte[] bytes_1) {
      this.method5337(bytes_1);
   }

   void method5353(int i_1, int i_2) {
      field3734 = -1;
      field3733 = -1;
      field3739 = i_2;
      field3721 = i_2;
      field3736 = i_1;
      field3737 = i_1;
      field3738 = 256;
      field3722 = 0;
      field3732 = 0;
   }

   public int method5340(String string_1) {
      if (string_1 == null) {
         return 0;
      } else {
         int i_2 = -1;
         int i_3 = -1;
         int i_4 = 0;

         for (int i_5 = 0; i_5 < string_1.length(); i_5++) {
            char var_6 = string_1.charAt(i_5);
            if (var_6 == 60) {
               i_2 = i_5;
            } else {
               if (var_6 == 62 && i_2 != -1) {
                  String string_7 = string_1.substring(i_2 + 1, i_5);
                  i_2 = -1;
                  if (string_7.equals("lt")) {
                     var_6 = 60;
                  } else {
                     if (!string_7.equals("gt")) {
                        if (string_7.startsWith("img=")) {
                           try {
                              int i_8 = class279.method4974(string_7.substring(4), (byte) 7);
                              i_4 += field3730[i_8].field3910;
                              i_3 = -1;
                           } catch (Exception exception_10) {
                              ;
                           }
                        }
                        continue;
                     }

                     var_6 = 62;
                  }
               }

               if (var_6 == 160) {
                  var_6 = 32;
               }

               if (i_2 == -1) {
                  i_4 += this.field3731[(char)(class221.method3992(var_6, -1905849275) & 0xff)];
                  if (this.field3742 != null && i_3 != -1) {
                     i_4 += this.field3742[var_6 + (i_3 << 8)];
                  }

                  i_3 = var_6;
               }
            }
         }

         return i_4;
      }
   }

   public int method5341(String string_1, int[] ints_2, String[] arr_3) {
      if (string_1 == null) {
         return 0;
      } else {
         int i_4 = 0;
         int i_5 = 0;
         StringBuilder stringbuilder_6 = new StringBuilder(100);
         int i_7 = -1;
         int i_8 = 0;
         byte b_9 = 0;
         int i_10 = -1;
         char var_11 = 0;
         int i_12 = 0;
         int i_13 = string_1.length();

         for (int i_14 = 0; i_14 < i_13; i_14++) {
            char var_15 = string_1.charAt(i_14);
            if (var_15 == 60) {
               i_10 = i_14;
            } else {
               if (var_15 == 62 && i_10 != -1) {
                  String string_16 = string_1.substring(i_10 + 1, i_14);
                  i_10 = -1;
                  stringbuilder_6.append('<');
                  stringbuilder_6.append(string_16);
                  stringbuilder_6.append('>');
                  if (string_16.equals("br")) {
                     arr_3[i_12] = stringbuilder_6.toString().substring(i_5, stringbuilder_6.length());
                     ++i_12;
                     i_5 = stringbuilder_6.length();
                     i_4 = 0;
                     i_7 = -1;
                     var_11 = 0;
                  } else if (string_16.equals("lt")) {
                     i_4 += this.method5339('<');
                     if (this.field3742 != null && var_11 != -1) {
                        i_4 += this.field3742[(var_11 << 8) + 60];
                     }

                     var_11 = 60;
                  } else if (string_16.equals("gt")) {
                     i_4 += this.method5339('>');
                     if (this.field3742 != null && var_11 != -1) {
                        i_4 += this.field3742[(var_11 << 8) + 62];
                     }

                     var_11 = 62;
                  } else if (string_16.startsWith("img=")) {
                     try {
                        int i_17 = class279.method4974(string_16.substring(4), (byte) 7);
                        i_4 += field3730[i_17].field3910;
                        var_11 = 0;
                     } catch (Exception exception_20) {
                        ;
                     }
                  }

                  var_15 = 0;
               }

               if (i_10 == -1) {
                  if (var_15 != 0) {
                     stringbuilder_6.append(var_15);
                     i_4 += this.method5339(var_15);
                     if (this.field3742 != null && var_11 != -1) {
                        i_4 += this.field3742[var_15 + (var_11 << 8)];
                     }

                     var_11 = var_15;
                  }

                  if (var_15 == 32) {
                     i_7 = stringbuilder_6.length();
                     i_8 = i_4;
                     b_9 = 1;
                  }

                  if (ints_2 != null && i_4 > ints_2[i_12 < ints_2.length ? i_12 : ints_2.length - 1] && i_7 >= 0) {
                     arr_3[i_12] = stringbuilder_6.toString().substring(i_5, i_7 - b_9);
                     ++i_12;
                     i_5 = i_7;
                     i_7 = -1;
                     i_4 -= i_8;
                     var_11 = 0;
                  }

                  if (var_15 == 45) {
                     i_7 = stringbuilder_6.length();
                     i_8 = i_4;
                     b_9 = 0;
                  }
               }
            }
         }

         String string_19 = stringbuilder_6.toString();
         if (string_19.length() > i_5) {
            arr_3[i_12++] = string_19.substring(i_5, string_19.length());
         }

         return i_12;
      }
   }

   void method5356(String string_1, int i_2, int i_3) {
      i_3 -= this.field3727;
      int i_4 = -1;
      int i_5 = -1;

      for (int i_6 = 0; i_6 < string_1.length(); i_6++) {
         if (string_1.charAt(i_6) != 0) {
            char var_7 = (char)(class221.method3992(string_1.charAt(i_6), -2098609604) & 0xff);
            if (var_7 == 60) {
               i_4 = i_6;
            } else {
               int i_9;
               if (var_7 == 62 && i_4 != -1) {
                  String string_8 = string_1.substring(i_4 + 1, i_6);
                  i_4 = -1;
                  if (string_8.equals("lt")) {
                     var_7 = 60;
                  } else {
                     if (!string_8.equals("gt")) {
                        if (string_8.startsWith("img=")) {
                           try {
                              i_9 = class279.method4974(string_8.substring(4), (byte) 7);
                              class334 class334_10 = field3730[i_9];
                              class334_10.method6091(i_2, i_3 + this.field3727 - class334_10.field3906);
                              i_2 += class334_10.field3910;
                              i_5 = -1;
                           } catch (Exception exception_14) {
                              ;
                           }
                        } else {
                           this.method5354(string_8);
                        }
                        continue;
                     }

                     var_7 = 62;
                  }
               }

               if (var_7 == 160) {
                  var_7 = 32;
               }

               if (i_4 == -1) {
                  if (this.field3742 != null && i_5 != -1) {
                     i_2 += this.field3742[var_7 + (i_5 << 8)];
                  }

                  int i_12 = this.field3723[var_7];
                  i_9 = this.field3724[var_7];
                  if (var_7 != 32) {
                     if (field3738 == 256) {
                        if (field3721 != -1) {
                           method5431(this.field3740[var_7], i_2 + this.field3725[var_7] + 1, i_3 + this.field3726[var_7] + 1, i_12, i_9, field3721);
                        }

                        this.vmethod5358(this.field3740[var_7], i_2 + this.field3725[var_7], i_3 + this.field3726[var_7], i_12, i_9, field3737);
                     } else {
                        if (field3721 != -1) {
                           method5362(this.field3740[var_7], i_2 + this.field3725[var_7] + 1, i_3 + this.field3726[var_7] + 1, i_12, i_9, field3721, field3738);
                        }

                        this.vmethod5359(this.field3740[var_7], i_2 + this.field3725[var_7], i_3 + this.field3726[var_7], i_12, i_9, field3737, field3738);
                     }
                  } else if (field3722 > 0) {
                     field3732 += field3722;
                     i_2 += field3732 >> 8;
                     field3732 &= 0xff;
                  }

                  int i_13 = this.field3731[var_7];
                  if (field3734 != -1) {
                     class331.method6052(i_2, i_3 + (int)((double)this.field3727 * 0.7D), i_13, field3734);
                  }

                  if (field3733 != -1) {
                     class331.method6052(i_2, i_3 + this.field3727 + 1, i_13, field3733);
                  }

                  i_2 += i_13;
                  i_5 = var_7;
               }
            }
         }
      }

   }

   void method5337(byte[] bytes_1) {
      this.field3731 = new int[256];
      int i_2;
      if (bytes_1.length == 257) {
         for (i_2 = 0; i_2 < this.field3731.length; i_2++) {
            this.field3731[i_2] = bytes_1[i_2] & 0xff;
         }

         this.field3727 = bytes_1[256] & 0xff;
      } else {
         i_2 = 0;

         for (int i_3 = 0; i_3 < 256; i_3++) {
            this.field3731[i_3] = bytes_1[i_2++] & 0xff;
         }

         int[] ints_10 = new int[256];
         int[] ints_4 = new int[256];

         int i_5;
         for (i_5 = 0; i_5 < 256; i_5++) {
            ints_10[i_5] = bytes_1[i_2++] & 0xff;
         }

         for (i_5 = 0; i_5 < 256; i_5++) {
            ints_4[i_5] = bytes_1[i_2++] & 0xff;
         }

         byte[][] bytes_11 = new byte[256][];

         int i_8;
         for (int i_6 = 0; i_6 < 256; i_6++) {
            bytes_11[i_6] = new byte[ints_10[i_6]];
            byte b_7 = 0;

            for (i_8 = 0; i_8 < bytes_11[i_6].length; i_8++) {
               b_7 += bytes_1[i_2++];
               bytes_11[i_6][i_8] = b_7;
            }
         }

         byte[][] bytes_12 = new byte[256][];

         int i_13;
         for (i_13 = 0; i_13 < 256; i_13++) {
            bytes_12[i_13] = new byte[ints_10[i_13]];
            byte b_14 = 0;

            for (int i_9 = 0; i_9 < bytes_12[i_13].length; i_9++) {
               b_14 += bytes_1[i_2++];
               bytes_12[i_13][i_9] = b_14;
            }
         }

         this.field3742 = new byte[65536];

         for (i_13 = 0; i_13 < 256; i_13++) {
            if (i_13 != 32 && i_13 != 160) {
               for (i_8 = 0; i_8 < 256; i_8++) {
                  if (i_8 != 32 && i_8 != 160) {
                     this.field3742[i_8 + (i_13 << 8)] = (byte)method5379(bytes_11, bytes_12, ints_4, this.field3731, ints_10, i_13, i_8);
                  }
               }
            }
         }

         this.field3727 = ints_4[32] + ints_10[32];
      }

   }

   void method5434(String string_1, int i_2, int i_3, int[] ints_4, int[] ints_5) {
      i_3 -= this.field3727;
      int i_6 = -1;
      int i_7 = -1;
      int i_8 = 0;

      for (int i_9 = 0; i_9 < string_1.length(); i_9++) {
         if (string_1.charAt(i_9) != 0) {
            char var_10 = (char)(class221.method3992(string_1.charAt(i_9), -1363707796) & 0xff);
            if (var_10 == 60) {
               i_6 = i_9;
            } else {
               int i_12;
               int i_13;
               int i_14;
               if (var_10 == 62 && i_6 != -1) {
                  String string_11 = string_1.substring(i_6 + 1, i_9);
                  i_6 = -1;
                  if (string_11.equals("lt")) {
                     var_10 = 60;
                  } else {
                     if (!string_11.equals("gt")) {
                        if (string_11.startsWith("img=")) {
                           try {
                              if (ints_4 != null) {
                                 i_12 = ints_4[i_8];
                              } else {
                                 i_12 = 0;
                              }

                              if (ints_5 != null) {
                                 i_13 = ints_5[i_8];
                              } else {
                                 i_13 = 0;
                              }

                              ++i_8;
                              i_14 = class279.method4974(string_11.substring(4), (byte) 7);
                              class334 class334_15 = field3730[i_14];
                              class334_15.method6091(i_12 + i_2, i_13 + (i_3 + this.field3727 - class334_15.field3906));
                              i_2 += class334_15.field3910;
                              i_7 = -1;
                           } catch (Exception exception_19) {
                              ;
                           }
                        } else {
                           this.method5354(string_11);
                        }
                        continue;
                     }

                     var_10 = 62;
                  }
               }

               if (var_10 == 160) {
                  var_10 = 32;
               }

               if (i_6 == -1) {
                  if (this.field3742 != null && i_7 != -1) {
                     i_2 += this.field3742[var_10 + (i_7 << 8)];
                  }

                  int i_17 = this.field3723[var_10];
                  i_12 = this.field3724[var_10];
                  if (ints_4 != null) {
                     i_13 = ints_4[i_8];
                  } else {
                     i_13 = 0;
                  }

                  if (ints_5 != null) {
                     i_14 = ints_5[i_8];
                  } else {
                     i_14 = 0;
                  }

                  ++i_8;
                  if (var_10 != 32) {
                     if (field3738 == 256) {
                        if (field3721 != -1) {
                           method5431(this.field3740[var_10], i_13 + i_2 + this.field3725[var_10] + 1, i_3 + i_14 + this.field3726[var_10] + 1, i_17, i_12, field3721);
                        }

                        this.vmethod5358(this.field3740[var_10], i_13 + i_2 + this.field3725[var_10], i_3 + i_14 + this.field3726[var_10], i_17, i_12, field3737);
                     } else {
                        if (field3721 != -1) {
                           method5362(this.field3740[var_10], i_13 + i_2 + this.field3725[var_10] + 1, i_3 + i_14 + this.field3726[var_10] + 1, i_17, i_12, field3721, field3738);
                        }

                        this.vmethod5359(this.field3740[var_10], i_13 + i_2 + this.field3725[var_10], i_3 + i_14 + this.field3726[var_10], i_17, i_12, field3737, field3738);
                     }
                  } else if (field3722 > 0) {
                     field3732 += field3722;
                     i_2 += field3732 >> 8;
                     field3732 &= 0xff;
                  }

                  int i_18 = this.field3731[var_10];
                  if (field3734 != -1) {
                     class331.method6052(i_2, i_3 + (int)((double)this.field3727 * 0.7D), i_18, field3734);
                  }

                  if (field3733 != -1) {
                     class331.method6052(i_2, i_3 + this.field3727, i_18, field3733);
                  }

                  i_2 += i_18;
                  i_7 = var_10;
               }
            }
         }
      }

   }

   int method5339(char var_1) {
      if (var_1 == 160) {
         var_1 = 32;
      }

      return this.field3731[class221.method3992(var_1, -1623168744) & 0xff];
   }

   void method5354(String string_1) {
      try {
         int i_2;
         String string_3;
         if (string_1.startsWith("col=")) {
            string_3 = string_1.substring(4);
            i_2 = class80.method1845(string_3, 16, true, -1564336124);
            field3737 = i_2;
         } else if (string_1.equals("/col")) {
            field3737 = field3736;
         } else if (string_1.startsWith("str=")) {
            string_3 = string_1.substring(4);
            i_2 = class80.method1845(string_3, 16, true, -157493395);
            field3734 = i_2;
         } else if (string_1.equals("str")) {
            field3734 = 8388608;
         } else if (string_1.equals("/str")) {
            field3734 = -1;
         } else if (string_1.startsWith("u=")) {
            string_3 = string_1.substring(2);
            i_2 = class80.method1845(string_3, 16, true, 1453699943);
            field3733 = i_2;
         } else if (string_1.equals("u")) {
            field3733 = 0;
         } else if (string_1.equals("/u")) {
            field3733 = -1;
         } else if (string_1.startsWith("shad=")) {
            string_3 = string_1.substring(5);
            i_2 = class80.method1845(string_3, 16, true, 1395528338);
            field3721 = i_2;
         } else if (string_1.equals("shad")) {
            field3721 = 0;
         } else if (string_1.equals("/shad")) {
            field3721 = field3739;
         } else if (string_1.equals("br")) {
            this.method5353(field3736, field3739);
         }
      } catch (Exception exception_5) {
         ;
      }

   }

   public int method5349(String string_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10) {
      if (string_1 == null) {
         return 0;
      } else {
         this.method5353(i_6, i_7);
         if (i_10 == 0) {
            i_10 = this.field3727;
         }

         int[] ints_11 = new int[] {i_4};
         if (i_5 < i_10 + this.field3728 + this.field3729 && i_5 < i_10 + i_10) {
            ints_11 = null;
         }

         int i_12 = this.method5341(string_1, ints_11, field3735);
         if (i_9 == 3 && i_12 == 1) {
            i_9 = 1;
         }

         int i_13;
         int i_14;
         if (i_9 == 0) {
            i_13 = i_3 + this.field3728;
         } else if (i_9 == 1) {
            i_13 = i_3 + (i_5 - this.field3728 - this.field3729 - i_10 * (i_12 - 1)) / 2 + this.field3728;
         } else if (i_9 == 2) {
            i_13 = i_3 + i_5 - this.field3729 - i_10 * (i_12 - 1);
         } else {
            i_14 = (i_5 - this.field3728 - this.field3729 - i_10 * (i_12 - 1)) / (i_12 + 1);
            if (i_14 < 0) {
               i_14 = 0;
            }

            i_13 = i_3 + i_14 + this.field3728;
            i_10 += i_14;
         }

         for (i_14 = 0; i_14 < i_12; i_14++) {
            if (i_8 == 0) {
               this.method5356(field3735[i_14], i_2, i_13);
            } else if (i_8 == 1) {
               this.method5356(field3735[i_14], i_2 + (i_4 - this.method5340(field3735[i_14])) / 2, i_13);
            } else if (i_8 == 2) {
               this.method5356(field3735[i_14], i_2 + i_4 - this.method5340(field3735[i_14]), i_13);
            } else if (i_14 == i_12 - 1) {
               this.method5356(field3735[i_14], i_2, i_13);
            } else {
               this.method5360(field3735[i_14], i_4);
               this.method5356(field3735[i_14], i_2, i_13);
               field3722 = 0;
            }

            i_13 += i_10;
         }

         return i_12;
      }
   }

   public int method5398(String string_1, int i_2) {
      return this.method5341(string_1, new int[] {i_2}, field3735);
   }

   void method5360(String string_1, int i_2) {
      int i_3 = 0;
      boolean bool_4 = false;

      for (int i_5 = 0; i_5 < string_1.length(); i_5++) {
         char var_6 = string_1.charAt(i_5);
         if (var_6 == 60) {
            bool_4 = true;
         } else if (var_6 == 62) {
            bool_4 = false;
         } else if (!bool_4 && var_6 == 32) {
            ++i_3;
         }
      }

      if (i_3 > 0) {
         field3722 = (i_2 - this.method5340(string_1) << 8) / i_3;
      }

   }

   abstract void vmethod5358(byte[] var1, int var2, int var3, int var4, int var5, int var6);

   abstract void vmethod5359(byte[] var1, int var2, int var3, int var4, int var5, int var6, int var7);

   public int method5342(String string_1, int i_2) {
      int i_3 = this.method5341(string_1, new int[] {i_2}, field3735);
      int i_4 = 0;

      for (int i_5 = 0; i_5 < i_3; i_5++) {
         int i_6 = this.method5340(field3735[i_5]);
         if (i_6 > i_4) {
            i_4 = i_6;
         }
      }

      return i_4;
   }

   public void method5436(String string_1, int i_2, int i_3, int i_4, int i_5) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         this.method5356(string_1, i_2 - this.method5340(string_1) / 2, i_3);
      }
   }

   public void method5389(String string_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         field3741.setSeed((long)i_6);
         field3738 = 192 + (field3741.nextInt() & 0x1f);
         int[] ints_7 = new int[string_1.length()];
         int i_8 = 0;

         for (int i_9 = 0; i_9 < string_1.length(); i_9++) {
            ints_7[i_9] = i_8;
            if ((field3741.nextInt() & 0x3) == 0) {
               ++i_8;
            }
         }

         this.method5434(string_1, i_2, i_3, ints_7, (int[]) null);
      }
   }

   public void method5335(String string_1, int i_2, int i_3, int i_4, int i_5) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         this.method5356(string_1, i_2, i_3);
      }
   }

   public void method5347(String string_1, int i_2, int i_3, int i_4, int i_5) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         this.method5356(string_1, i_2 - this.method5340(string_1), i_3);
      }
   }

   public void method5350(String string_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         int[] ints_7 = new int[string_1.length()];

         for (int i_8 = 0; i_8 < string_1.length(); i_8++) {
            ints_7[i_8] = (int)(Math.sin((double)i_8 / 2.0D + (double)i_6 / 5.0D) * 5.0D);
         }

         this.method5434(string_1, i_2 - this.method5340(string_1) / 2, i_3, (int[]) null, ints_7);
      }
   }

   public void method5357(String string_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         int[] ints_7 = new int[string_1.length()];
         int[] ints_8 = new int[string_1.length()];

         for (int i_9 = 0; i_9 < string_1.length(); i_9++) {
            ints_7[i_9] = (int)(Math.sin((double)i_9 / 5.0D + (double)i_6 / 5.0D) * 5.0D);
            ints_8[i_9] = (int)(Math.sin((double)i_9 / 3.0D + (double)i_6 / 5.0D) * 5.0D);
         }

         this.method5434(string_1, i_2 - this.method5340(string_1) / 2, i_3, ints_7, ints_8);
      }
   }

   public void method5351(String string_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         double d_8 = 7.0D - (double)i_7 / 8.0D;
         if (d_8 < 0.0D) {
            d_8 = 0.0D;
         }

         int[] ints_10 = new int[string_1.length()];

         for (int i_11 = 0; i_11 < string_1.length(); i_11++) {
            ints_10[i_11] = (int)(Math.sin((double)i_11 / 1.5D + (double)i_6 / 1.0D) * d_8);
         }

         this.method5434(string_1, i_2 - this.method5340(string_1) / 2, i_3, (int[]) null, ints_10);
      }
   }

   public void method5400(String string_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if (string_1 != null) {
         this.method5353(i_4, i_5);
         field3738 = i_6;
         this.method5356(string_1, i_2, i_3);
      }
   }

   static int method5379(byte[][] bytes_0, byte[][] bytes_1, int[] ints_2, int[] ints_3, int[] ints_4, int i_5, int i_6) {
      int i_7 = ints_2[i_5];
      int i_8 = i_7 + ints_4[i_5];
      int i_9 = ints_2[i_6];
      int i_10 = i_9 + ints_4[i_6];
      int i_11 = i_7;
      if (i_9 > i_7) {
         i_11 = i_9;
      }

      int i_12 = i_8;
      if (i_10 < i_8) {
         i_12 = i_10;
      }

      int i_13 = ints_3[i_5];
      if (ints_3[i_6] < i_13) {
         i_13 = ints_3[i_6];
      }

      byte[] bytes_14 = bytes_1[i_5];
      byte[] bytes_15 = bytes_0[i_6];
      int i_16 = i_11 - i_7;
      int i_17 = i_11 - i_9;

      for (int i_18 = i_11; i_18 < i_12; i_18++) {
         int i_19 = bytes_14[i_16++] + bytes_15[i_17++];
         if (i_19 < i_13) {
            i_13 = i_19;
         }
      }

      return -i_13;
   }

   static void method5431(byte[] bytes_0, int i_1, int i_2, int i_3, int i_4, int i_5) {
      int i_6 = i_1 + i_2 * class331.field3889;
      int i_7 = class331.field3889 - i_3;
      int i_8 = 0;
      int i_9 = 0;
      int i_10;
      if (i_2 < class331.field3887) {
         i_10 = class331.field3887 - i_2;
         i_4 -= i_10;
         i_2 = class331.field3887;
         i_9 += i_3 * i_10;
         i_6 += i_10 * class331.field3889;
      }

      if (i_2 + i_4 > class331.field3888) {
         i_4 -= i_2 + i_4 - class331.field3888;
      }

      if (i_1 < class331.field3890) {
         i_10 = class331.field3890 - i_1;
         i_3 -= i_10;
         i_1 = class331.field3890;
         i_9 += i_10;
         i_6 += i_10;
         i_8 += i_10;
         i_7 += i_10;
      }

      if (i_3 + i_1 > class331.field3884) {
         i_10 = i_3 + i_1 - class331.field3884;
         i_3 -= i_10;
         i_8 += i_10;
         i_7 += i_10;
      }

      if (i_3 > 0 && i_4 > 0) {
         method5361(class331.field3886, bytes_0, i_5, i_9, i_6, i_3, i_4, i_7, i_8);
      }
   }

   static void method5361(int[] ints_0, byte[] bytes_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8) {
      int i_9 = -(i_5 >> 2);
      i_5 = -(i_5 & 0x3);

      for (int i_10 = -i_6; i_10 < 0; i_10++) {
         int i_11;
         for (i_11 = i_9; i_11 < 0; i_11++) {
            if (bytes_1[i_3++] != 0) {
               ints_0[i_4++] = i_2;
            } else {
               ++i_4;
            }

            if (bytes_1[i_3++] != 0) {
               ints_0[i_4++] = i_2;
            } else {
               ++i_4;
            }

            if (bytes_1[i_3++] != 0) {
               ints_0[i_4++] = i_2;
            } else {
               ++i_4;
            }

            if (bytes_1[i_3++] != 0) {
               ints_0[i_4++] = i_2;
            } else {
               ++i_4;
            }
         }

         for (i_11 = i_5; i_11 < 0; i_11++) {
            if (bytes_1[i_3++] != 0) {
               ints_0[i_4++] = i_2;
            } else {
               ++i_4;
            }
         }

         i_4 += i_7;
         i_3 += i_8;
      }

   }

   static void method5396(int[] ints_0, byte[] bytes_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9) {
      i_2 = ((i_2 & 0xff00) * i_9 & 0xff0000) + (i_9 * (i_2 & 0xff00ff) & ~0xff00ff) >> 8;
      i_9 = 256 - i_9;

      for (int i_10 = -i_6; i_10 < 0; i_10++) {
         for (int i_11 = -i_5; i_11 < 0; i_11++) {
            if (bytes_1[i_3++] != 0) {
               int i_12 = ints_0[i_4];
               ints_0[i_4++] = (((i_12 & 0xff00) * i_9 & 0xff0000) + ((i_12 & 0xff00ff) * i_9 & ~0xff00ff) >> 8) + i_2;
            } else {
               ++i_4;
            }
         }

         i_4 += i_7;
         i_3 += i_8;
      }

   }

   static void method5362(byte[] bytes_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      int i_7 = i_1 + i_2 * class331.field3889;
      int i_8 = class331.field3889 - i_3;
      int i_9 = 0;
      int i_10 = 0;
      int i_11;
      if (i_2 < class331.field3887) {
         i_11 = class331.field3887 - i_2;
         i_4 -= i_11;
         i_2 = class331.field3887;
         i_10 += i_3 * i_11;
         i_7 += i_11 * class331.field3889;
      }

      if (i_2 + i_4 > class331.field3888) {
         i_4 -= i_2 + i_4 - class331.field3888;
      }

      if (i_1 < class331.field3890) {
         i_11 = class331.field3890 - i_1;
         i_3 -= i_11;
         i_1 = class331.field3890;
         i_10 += i_11;
         i_7 += i_11;
         i_9 += i_11;
         i_8 += i_11;
      }

      if (i_3 + i_1 > class331.field3884) {
         i_11 = i_3 + i_1 - class331.field3884;
         i_3 -= i_11;
         i_9 += i_11;
         i_8 += i_11;
      }

      if (i_3 > 0 && i_4 > 0) {
         method5396(class331.field3886, bytes_0, i_5, i_10, i_7, i_3, i_4, i_8, i_9, i_6);
      }
   }

   public static String method5386(String string_0) {
      int i_1 = string_0.length();
      int i_2 = 0;

      for (int i_3 = 0; i_3 < i_1; i_3++) {
         char var_4 = string_0.charAt(i_3);
         if (var_4 == 60 || var_4 == 62) {
            i_2 += 3;
         }
      }

      StringBuilder stringbuilder_6 = new StringBuilder(i_1 + i_2);

      for (int i_7 = 0; i_7 < i_1; i_7++) {
         char var_5 = string_0.charAt(i_7);
         if (var_5 == 60) {
            stringbuilder_6.append("<lt>");
         } else if (var_5 == 62) {
            stringbuilder_6.append("<gt>");
         } else {
            stringbuilder_6.append(var_5);
         }
      }

      return stringbuilder_6.toString();
   }

}
