public class class133 extends class189 {

   boolean field1635 = false;
   int field1638;
   boolean field1640;
   int[] field1641;
   int[] field1642;
   int[] field1648;
   int[] field1637;
   int field1645;
   int field1646;
   int[] field1647;
   static int[] field1649;

   class133(class310 class310_1) {
      this.field1638 = class310_1.method5729(617563058);
      this.field1640 = class310_1.method5661((byte) 32) == 1;
      int i_2 = class310_1.method5661((byte) -70);
      if (i_2 >= 1 && i_2 <= 4) {
         this.field1641 = new int[i_2];

         int i_3;
         for (i_3 = 0; i_3 < i_2; i_3++) {
            this.field1641[i_3] = class310_1.method5729(-1522308195);
         }

         if (i_2 > 1) {
            this.field1642 = new int[i_2 - 1];

            for (i_3 = 0; i_3 < i_2 - 1; i_3++) {
               this.field1642[i_3] = class310_1.method5661((byte) 2);
            }
         }

         if (i_2 > 1) {
            this.field1648 = new int[i_2 - 1];

            for (i_3 = 0; i_3 < i_2 - 1; i_3++) {
               this.field1648[i_3] = class310_1.method5661((byte) -9);
            }
         }

         this.field1637 = new int[i_2];

         for (i_3 = 0; i_3 < i_2; i_3++) {
            this.field1637[i_3] = class310_1.method5507(1762539679);
         }

         this.field1645 = class310_1.method5661((byte) 18);
         this.field1646 = class310_1.method5661((byte) 109);
         this.field1647 = null;
      } else {
         throw new RuntimeException();
      }
   }

   void method2909() {
      this.field1647 = null;
   }

   boolean method2908(double d_1, int i_3, class244 class244_4) {
      int i_5;
      for (i_5 = 0; i_5 < this.field1641.length; i_5++) {
         if (class244_4.method4176(this.field1641[i_5], (byte) 66) == null) {
            return false;
         }
      }

      i_5 = i_3 * i_3;
      this.field1647 = new int[i_5];

      for (int i_6 = 0; i_6 < this.field1641.length; i_6++) {
         class334 class334_7 = class268.method4756(class244_4, this.field1641[i_6], (byte) -54);
         class334_7.method6108();
         byte[] bytes_8 = class334_7.field3903;
         int[] ints_9 = class334_7.field3904;
         int i_10 = this.field1637[i_6];
         if ((i_10 & ~0xffffff) == 16777216) {
            ;
         }

         if ((i_10 & ~0xffffff) == 33554432) {
            ;
         }

         int i_11;
         int i_12;
         int i_13;
         int i_14;
         if ((i_10 & ~0xffffff) == 50331648) {
            i_11 = i_10 & 0xff00ff;
            i_12 = i_10 >> 8 & 0xff;

            for (i_13 = 0; i_13 < ints_9.length; i_13++) {
               i_14 = ints_9[i_13];
               if (i_14 >> 8 == (i_14 & 0xffff)) {
                  i_14 &= 0xff;
                  ints_9[i_13] = i_11 * i_14 >> 8 & 0xff00ff | i_12 * i_14 & 0xff00;
               }
            }
         }

         for (i_11 = 0; i_11 < ints_9.length; i_11++) {
            ints_9[i_11] = class139.method3049(ints_9[i_11], d_1);
         }

         if (i_6 == 0) {
            i_11 = 0;
         } else {
            i_11 = this.field1642[i_6 - 1];
         }

         if (i_11 == 0) {
            if (i_3 == class334_7.field3905) {
               for (i_12 = 0; i_12 < i_5; i_12++) {
                  this.field1647[i_12] = ints_9[bytes_8[i_12] & 0xff];
               }
            } else if (class334_7.field3905 == 64 && i_3 == 128) {
               i_12 = 0;

               for (i_13 = 0; i_13 < i_3; i_13++) {
                  for (i_14 = 0; i_14 < i_3; i_14++) {
                     this.field1647[i_12++] = ints_9[bytes_8[(i_13 >> 1 << 6) + (i_14 >> 1)] & 0xff];
                  }
               }
            } else {
               if (class334_7.field3905 != 128 || i_3 != 64) {
                  throw new RuntimeException();
               }

               i_12 = 0;

               for (i_13 = 0; i_13 < i_3; i_13++) {
                  for (i_14 = 0; i_14 < i_3; i_14++) {
                     this.field1647[i_12++] = ints_9[bytes_8[(i_14 << 1) + (i_13 << 1 << 7)] & 0xff];
                  }
               }
            }
         }

         if (i_11 == 1) {
            ;
         }

         if (i_11 == 2) {
            ;
         }

         if (i_11 == 3) {
            ;
         }
      }

      return true;
   }

   void method2910(int i_1) {
      if (this.field1647 != null) {
         short s_2;
         int i_3;
         int i_4;
         int i_5;
         int i_6;
         int i_7;
         int[] ints_10;
         if (this.field1645 == 1 || this.field1645 == 3) {
            if (field1649 == null || field1649.length < this.field1647.length) {
               field1649 = new int[this.field1647.length];
            }

            if (this.field1647.length == 4096) {
               s_2 = 64;
            } else {
               s_2 = 128;
            }

            i_3 = this.field1647.length;
            i_4 = s_2 * this.field1646 * i_1;
            i_5 = i_3 - 1;
            if (this.field1645 == 1) {
               i_4 = -i_4;
            }

            for (i_6 = 0; i_6 < i_3; i_6++) {
               i_7 = i_6 + i_4 & i_5;
               field1649[i_6] = this.field1647[i_7];
            }

            ints_10 = this.field1647;
            this.field1647 = field1649;
            field1649 = ints_10;
         }

         if (this.field1645 == 2 || this.field1645 == 4) {
            if (field1649 == null || field1649.length < this.field1647.length) {
               field1649 = new int[this.field1647.length];
            }

            if (this.field1647.length == 4096) {
               s_2 = 64;
            } else {
               s_2 = 128;
            }

            i_3 = this.field1647.length;
            i_4 = this.field1646 * i_1;
            i_5 = s_2 - 1;
            if (this.field1645 == 2) {
               i_4 = -i_4;
            }

            for (i_6 = 0; i_6 < i_3; i_6 += s_2) {
               for (i_7 = 0; i_7 < s_2; i_7++) {
                  int i_8 = i_6 + i_7;
                  int i_9 = i_6 + (i_7 + i_4 & i_5);
                  field1649[i_8] = this.field1647[i_9];
               }
            }

            ints_10 = this.field1647;
            this.field1647 = field1649;
            field1649 = ints_10;
         }

      }
   }

}
