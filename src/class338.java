public final class class338 {

   int[] field3934 = new int[256];
   int[] field3933 = new int[256];
   int field3937;
   int field3931;
   int field3935;
   int field3932;

   public class338(int[] ints_1) {
      for (int i_2 = 0; i_2 < ints_1.length; i_2++) {
         this.field3933[i_2] = ints_1[i_2];
      }

      this.method6264(1669913589);
   }

   final void method6263(int i_1) {
      this.field3931 += ++this.field3937;

      for (int i_2 = 0; i_2 < 256; i_2++) {
         int i_3 = this.field3934[i_2];
         if ((i_2 & 0x2) == 0) {
            if ((i_2 & 0x1) == 0) {
               this.field3935 ^= this.field3935 << 13;
            } else {
               this.field3935 ^= this.field3935 >>> 6;
            }
         } else if ((i_2 & 0x1) == 0) {
            this.field3935 ^= this.field3935 << 2;
         } else {
            this.field3935 ^= this.field3935 >>> 16;
         }

         this.field3935 += this.field3934[i_2 + 128 & 0xff];
         int i_4;
         this.field3934[i_2] = i_4 = this.field3934[(i_3 & 0x3fc) >> 2] + this.field3935 + this.field3931;
         this.field3933[i_2] = this.field3931 = this.field3934[(i_4 >> 8 & 0x3fc) >> 2] + i_3;
      }

   }

   public final int method6262(int i_1) {
      if (this.field3932 == 0) {
         this.method6263(-127327099);
         this.field3932 = 256;
      }

      return this.field3933[this.field3932 - 1];
   }

   final void method6264(int i_1) {
      int i_10 = -1640531527;
      int i_9 = -1640531527;
      int i_8 = -1640531527;
      int i_7 = -1640531527;
      int i_6 = -1640531527;
      int i_5 = -1640531527;
      int i_4 = -1640531527;
      int i_3 = -1640531527;

      int i_2;
      for (i_2 = 0; i_2 < 4; i_2++) {
         i_3 ^= i_4 << 11;
         i_6 += i_3;
         i_4 += i_5;
         i_4 ^= i_5 >>> 2;
         i_7 += i_4;
         i_5 += i_6;
         i_5 ^= i_6 << 8;
         i_8 += i_5;
         i_6 += i_7;
         i_6 ^= i_7 >>> 16;
         i_9 += i_6;
         i_7 += i_8;
         i_7 ^= i_8 << 10;
         i_10 += i_7;
         i_8 += i_9;
         i_8 ^= i_9 >>> 4;
         i_3 += i_8;
         i_9 += i_10;
         i_9 ^= i_10 << 8;
         i_4 += i_9;
         i_10 += i_3;
         i_10 ^= i_3 >>> 9;
         i_5 += i_10;
         i_3 += i_4;
      }

      for (i_2 = 0; i_2 < 256; i_2 += 8) {
         i_3 += this.field3933[i_2];
         i_4 += this.field3933[i_2 + 1];
         i_5 += this.field3933[i_2 + 2];
         i_6 += this.field3933[i_2 + 3];
         i_7 += this.field3933[i_2 + 4];
         i_8 += this.field3933[i_2 + 5];
         i_9 += this.field3933[i_2 + 6];
         i_10 += this.field3933[i_2 + 7];
         i_3 ^= i_4 << 11;
         i_6 += i_3;
         i_4 += i_5;
         i_4 ^= i_5 >>> 2;
         i_7 += i_4;
         i_5 += i_6;
         i_5 ^= i_6 << 8;
         i_8 += i_5;
         i_6 += i_7;
         i_6 ^= i_7 >>> 16;
         i_9 += i_6;
         i_7 += i_8;
         i_7 ^= i_8 << 10;
         i_10 += i_7;
         i_8 += i_9;
         i_8 ^= i_9 >>> 4;
         i_3 += i_8;
         i_9 += i_10;
         i_9 ^= i_10 << 8;
         i_4 += i_9;
         i_10 += i_3;
         i_10 ^= i_3 >>> 9;
         i_5 += i_10;
         i_3 += i_4;
         this.field3934[i_2] = i_3;
         this.field3934[i_2 + 1] = i_4;
         this.field3934[i_2 + 2] = i_5;
         this.field3934[i_2 + 3] = i_6;
         this.field3934[i_2 + 4] = i_7;
         this.field3934[i_2 + 5] = i_8;
         this.field3934[i_2 + 6] = i_9;
         this.field3934[i_2 + 7] = i_10;
      }

      for (i_2 = 0; i_2 < 256; i_2 += 8) {
         i_3 += this.field3934[i_2];
         i_4 += this.field3934[i_2 + 1];
         i_5 += this.field3934[i_2 + 2];
         i_6 += this.field3934[i_2 + 3];
         i_7 += this.field3934[i_2 + 4];
         i_8 += this.field3934[i_2 + 5];
         i_9 += this.field3934[i_2 + 6];
         i_10 += this.field3934[i_2 + 7];
         i_3 ^= i_4 << 11;
         i_6 += i_3;
         i_4 += i_5;
         i_4 ^= i_5 >>> 2;
         i_7 += i_4;
         i_5 += i_6;
         i_5 ^= i_6 << 8;
         i_8 += i_5;
         i_6 += i_7;
         i_6 ^= i_7 >>> 16;
         i_9 += i_6;
         i_7 += i_8;
         i_7 ^= i_8 << 10;
         i_10 += i_7;
         i_8 += i_9;
         i_8 ^= i_9 >>> 4;
         i_3 += i_8;
         i_9 += i_10;
         i_9 ^= i_10 << 8;
         i_4 += i_9;
         i_10 += i_3;
         i_10 ^= i_3 >>> 9;
         i_5 += i_10;
         i_3 += i_4;
         this.field3934[i_2] = i_3;
         this.field3934[i_2 + 1] = i_4;
         this.field3934[i_2 + 2] = i_5;
         this.field3934[i_2 + 3] = i_6;
         this.field3934[i_2 + 4] = i_7;
         this.field3934[i_2 + 5] = i_8;
         this.field3934[i_2 + 6] = i_9;
         this.field3934[i_2 + 7] = i_10;
      }

      this.method6263(-127327099);
      this.field3932 = 256;
   }

   public final int method6270(int i_1) {
      if (this.field3932 == 0) {
         this.method6263(-127327099);
         this.field3932 = 256;
      }

      return this.field3933[--this.field3932];
   }

}
