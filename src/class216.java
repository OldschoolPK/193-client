import java.util.Calendar;
import java.util.TimeZone;

public class class216 {

   static final String[][] field2515 = new String[][] {{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}, {"Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"}, {"jan", "fév", "mars", "avr", "mai", "juin", "juil", "août", "sept", "oct", "nov", "déc"}, {"jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez"}, {"jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"}, {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}, {"ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"}};
   static final String[] field2517 = new String[] {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
   static Calendar field2516;

   static {
      Calendar.getInstance();
      field2516 = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
   }

   static final void method3954(int i_0) {
      Object obj_10000 = null;
      String str_1 = "Your friend list is full. Max of 200 for free users, and 400 for members";
      class234.method4129(30, "", str_1, (byte) -23);
   }

   static int method3958(int i_0, byte b_1) {
      class73 class73_2 = (class73) class100.field1285.method5919((long)i_0);
      return class73_2 == null ? -1 : (class73_2.field2129 == class100.field1284.field3579 ? -1 : ((class73) class73_2.field2129).field598);
   }

}
