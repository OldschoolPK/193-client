public class class40 {

   static class174 field293;
   static final class40 field292 = new class40(0);
   static final class40 field296 = new class40(1);
   final int field291;

   class40(int i_1) {
      this.field291 = i_1;
   }

   static final void method597(String string_0, String string_1, int i_2, int i_3, int i_4, int i_5, boolean bool_6, int i_7) {
      if (!client.field784) {
         if (client.field785 < 500) {
            client.field790[client.field785] = string_0;
            client.field791[client.field785] = string_1;
            client.field844[client.field785] = i_2;
            client.field789[client.field785] = i_3;
            client.field786[client.field785] = i_4;
            client.field787[client.field785] = i_5;
            client.field792[client.field785] = bool_6;
            ++client.field785;
         }

      }
   }

   public static void method601(int i_0) {
      class251.field3236.method3375();
   }

   static final void method598(class78 class78_0, int i_1, int i_2) {
      int i_4;
      int i_5;
      int i_9;
      if (class78_0.field984 >= client.field655) {
         class45.method672(class78_0, -939198295);
      } else if (class78_0.field973 >= client.field655) {
         if (class78_0.field973 == client.field655 || class78_0.field970 == -1 || class78_0.field937 != 0 || class78_0.field972 + 1 > class260.method4510(class78_0.field970, -1708661851).field3552[class78_0.field971]) {
            i_9 = class78_0.field973 - class78_0.field984;
            i_4 = client.field655 - class78_0.field984;
            i_5 = class78_0.field981 * 128 + class78_0.field941 * 64;
            int i_6 = class78_0.field982 * 128 + class78_0.field941 * 64;
            int i_7 = class78_0.field958 * 128 + class78_0.field941 * 64;
            int i_8 = class78_0.field956 * 128 + class78_0.field941 * 64;
            class78_0.field990 = (i_7 * i_4 + i_5 * (i_9 - i_4)) / i_9;
            class78_0.field938 = (i_4 * i_8 + i_6 * (i_9 - i_4)) / i_9;
         }

         class78_0.field963 = 0;
         class78_0.field989 = class78_0.field986;
         class78_0.field939 = class78_0.field989;
      } else {
         class51.method831(class78_0, 1281357003);
      }

      if (class78_0.field990 < 128 || class78_0.field938 < 128 || class78_0.field990 >= 13184 || class78_0.field938 >= 13184) {
         class78_0.field970 = -1;
         class78_0.field975 = -1;
         class78_0.field984 = 0;
         class78_0.field973 = 0;
         class78_0.field990 = class78_0.field993[0] * 128 + class78_0.field941 * 64;
         class78_0.field938 = class78_0.field968[0] * 128 + class78_0.field941 * 64;
         class78_0.method1775(-874125335);
      }

      if (class223.field2562 == class78_0 && (class78_0.field990 < 1536 || class78_0.field938 < 1536 || class78_0.field990 >= 11776 || class78_0.field938 >= 11776)) {
         class78_0.field970 = -1;
         class78_0.field975 = -1;
         class78_0.field984 = 0;
         class78_0.field973 = 0;
         class78_0.field990 = class78_0.field993[0] * 128 + class78_0.field941 * 64;
         class78_0.field938 = class78_0.field968[0] * 128 + class78_0.field941 * 64;
         class78_0.method1775(-874125335);
      }

      if (class78_0.field940 != 0) {
         if (class78_0.field980 != -1) {
            Object obj_3 = null;
            if (class78_0.field980 < 32768) {
               obj_3 = client.field689[class78_0.field980];
            } else if (class78_0.field980 >= 32768) {
               obj_3 = client.field764[class78_0.field980 - 32768];
            }

            if (obj_3 != null) {
               i_4 = class78_0.field990 - ((class78) obj_3).field990;
               i_5 = class78_0.field938 - ((class78) obj_3).field938;
               if (i_4 != 0 || i_5 != 0) {
                  class78_0.field989 = (int)(Math.atan2((double)i_4, (double)i_5) * 325.949D) & 0x7ff;
               }
            } else if (class78_0.field965) {
               class78_0.field980 = -1;
               class78_0.field965 = false;
            }
         }

         if (class78_0.field959 != -1 && (class78_0.field985 == 0 || class78_0.field963 > 0)) {
            class78_0.field989 = class78_0.field959;
            class78_0.field959 = -1;
         }

         i_9 = class78_0.field989 - class78_0.field939 & 0x7ff;
         if (i_9 == 0 && class78_0.field965) {
            class78_0.field980 = -1;
            class78_0.field965 = false;
         }

         if (i_9 != 0) {
            ++class78_0.field954;
            boolean bool_11;
            if (i_9 > 1024) {
               class78_0.field939 -= class78_0.field940;
               bool_11 = true;
               if (i_9 < class78_0.field940 || i_9 > 2048 - class78_0.field940) {
                  class78_0.field939 = class78_0.field989;
                  bool_11 = false;
               }

               if (class78_0.field943 == class78_0.field967 && (class78_0.field954 > 25 || bool_11)) {
                  if (class78_0.field944 != -1) {
                     class78_0.field967 = class78_0.field944;
                  } else {
                     class78_0.field967 = class78_0.field946;
                  }
               }
            } else {
               class78_0.field939 += class78_0.field940;
               bool_11 = true;
               if (i_9 < class78_0.field940 || i_9 > 2048 - class78_0.field940) {
                  class78_0.field939 = class78_0.field989;
                  bool_11 = false;
               }

               if (class78_0.field967 == class78_0.field943 && (class78_0.field954 > 25 || bool_11)) {
                  if (class78_0.field945 != -1) {
                     class78_0.field967 = class78_0.field945;
                  } else {
                     class78_0.field967 = class78_0.field946;
                  }
               }
            }

            class78_0.field939 &= 0x7ff;
         } else {
            class78_0.field954 = 0;
         }
      }

      class140.method3126(class78_0, 311945184);
   }

   static int method600(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      String string_4;
      if (i_0 == 3100) {
         string_4 = class85.field1096[--class85.field1105];
         class234.method4129(0, "", string_4, (byte) 2);
         return 1;
      } else if (i_0 == 3101) {
         class253.field3267 -= 2;
         class64.method1210(class223.field2562, class85.field1095[class253.field3267], class85.field1095[class253.field3267 + 1], 1651593642);
         return 1;
      } else if (i_0 == 3103) {
         if (!class85.field1103) {
            class85.field1104 = true;
         }

         return 1;
      } else {
         int i_11;
         if (i_0 == 3104) {
            string_4 = class85.field1096[--class85.field1105];
            i_11 = 0;
            if (class242.method4155(string_4, -446270285)) {
               i_11 = class279.method4974(string_4, (byte) 7);
            }

            class196 class196_13 = class68.method1249(class192.field2291, client.field694.field1328, (byte) 1);
            class196_13.field2360.method5510(i_11, (byte) -34);
            client.field694.method2295(class196_13, -819771458);
            return 1;
         } else {
            class196 class196_15;
            if (i_0 == 3105) {
               string_4 = class85.field1096[--class85.field1105];
               class196_15 = class68.method1249(class192.field2245, client.field694.field1328, (byte) 1);
               class196_15.field2360.method5482(string_4.length() + 1, (byte) -10);
               class196_15.field2360.method5492(string_4, -1748540705);
               client.field694.method2295(class196_15, -594344795);
               return 1;
            } else if (i_0 == 3106) {
               string_4 = class85.field1096[--class85.field1105];
               class196_15 = class68.method1249(class192.field2261, client.field694.field1328, (byte) 1);
               class196_15.field2360.method5482(string_4.length() + 1, (byte) -49);
               class196_15.field2360.method5492(string_4, -1748540705);
               client.field694.method2295(class196_15, -1284095519);
               return 1;
            } else {
               String string_8;
               int i_16;
               if (i_0 == 3107) {
                  i_16 = class85.field1095[--class253.field3267];
                  string_8 = class85.field1096[--class85.field1105];
                  class132.method2903(i_16, string_8, (byte) -74);
                  return 1;
               } else if (i_0 == 3108) {
                  class253.field3267 -= 3;
                  i_16 = class85.field1095[class253.field3267];
                  i_11 = class85.field1095[class253.field3267 + 1];
                  int i_10 = class85.field1095[class253.field3267 + 2];
                  class226 class226_14 = class181.method3610(i_10, 162707049);
                  class13.method130(class226_14, i_16, i_11, 1968316955);
                  return 1;
               } else if (i_0 == 3109) {
                  class253.field3267 -= 2;
                  i_16 = class85.field1095[class253.field3267];
                  i_11 = class85.field1095[class253.field3267 + 1];
                  class226 class226_12 = bool_2 ? class223.field2561 : class253.field3264;
                  class13.method130(class226_12, i_16, i_11, 1885720113);
                  return 1;
               } else if (i_0 == 3110) {
                  class161.field1989 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else if (i_0 == 3111) {
                  class85.field1095[++class253.field3267 - 1] = class282.field3617.field1052 ? 1 : 0;
                  return 1;
               } else if (i_0 == 3112) {
                  class282.field3617.field1052 = class85.field1095[--class253.field3267] == 1;
                  class18.method187(552807793);
                  return 1;
               } else if (i_0 == 3113) {
                  string_4 = class85.field1096[--class85.field1105];
                  boolean bool_5 = class85.field1095[--class253.field3267] == 1;
                  class93.method2109(string_4, bool_5, false, -22845210);
                  return 1;
               } else if (i_0 == 3115) {
                  i_16 = class85.field1095[--class253.field3267];
                  class196_15 = class68.method1249(class192.field2250, client.field694.field1328, (byte) 1);
                  class196_15.field2360.method5486(i_16, (byte) -22);
                  client.field694.method2295(class196_15, 1299497009);
                  return 1;
               } else if (i_0 == 3116) {
                  i_16 = class85.field1095[--class253.field3267];
                  class85.field1105 -= 2;
                  string_8 = class85.field1096[class85.field1105];
                  String string_6 = class85.field1096[class85.field1105 + 1];
                  if (string_8.length() > 500) {
                     return 1;
                  } else if (string_6.length() > 500) {
                     return 1;
                  } else {
                     class196 class196_7 = class68.method1249(class192.field2270, client.field694.field1328, (byte) 1);
                     class196_7.field2360.method5486(1 + class132.method2906(string_8, (byte) 23) + class132.method2906(string_6, (byte) -115), (byte) -105);
                     class196_7.field2360.method5482(i_16, (byte) -71);
                     class196_7.field2360.method5492(string_8, -1748540705);
                     class196_7.field2360.method5492(string_6, -1748540705);
                     client.field694.method2295(class196_7, -1271409012);
                     return 1;
                  }
               } else if (i_0 == 3117) {
                  client.field663 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else if (i_0 == 3118) {
                  client.field796 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else if (i_0 == 3119) {
                  client.field725 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else if (i_0 == 3120) {
                  if (class85.field1095[--class253.field3267] == 1) {
                     client.field768 |= 0x1;
                  } else {
                     client.field768 &= ~0x1;
                  }

                  return 1;
               } else if (i_0 == 3121) {
                  if (class85.field1095[--class253.field3267] == 1) {
                     client.field768 |= 0x2;
                  } else {
                     client.field768 &= ~0x2;
                  }

                  return 1;
               } else if (i_0 == 3122) {
                  if (class85.field1095[--class253.field3267] == 1) {
                     client.field768 |= 0x4;
                  } else {
                     client.field768 &= ~0x4;
                  }

                  return 1;
               } else if (i_0 == 3123) {
                  if (class85.field1095[--class253.field3267] == 1) {
                     client.field768 |= 0x8;
                  } else {
                     client.field768 &= ~0x8;
                  }

                  return 1;
               } else if (i_0 == 3124) {
                  client.field768 = 0;
                  return 1;
               } else if (i_0 == 3125) {
                  client.field753 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else if (i_0 == 3126) {
                  client.field763 = class85.field1095[--class253.field3267] == 1;
                  return 1;
               } else if (i_0 == 3127) {
                  class25.method251(class85.field1095[--class253.field3267] == 1, (byte) -67);
                  return 1;
               } else if (i_0 == 3128) {
                  class85.field1095[++class253.field3267 - 1] = class3.method39(-1332893941) ? 1 : 0;
                  return 1;
               } else if (i_0 == 3129) {
                  class253.field3267 -= 2;
                  client.field728 = class85.field1095[class253.field3267];
                  client.field676 = class85.field1095[class253.field3267 + 1];
                  return 1;
               } else if (i_0 == 3130) {
                  class253.field3267 -= 2;
                  return 1;
               } else if (i_0 == 3131) {
                  --class253.field3267;
                  return 1;
               } else if (i_0 == 3132) {
                  class85.field1095[++class253.field3267 - 1] = class286.field3638;
                  class85.field1095[++class253.field3267 - 1] = class143.field1892;
                  return 1;
               } else if (i_0 == 3133) {
                  --class253.field3267;
                  return 1;
               } else if (i_0 == 3134) {
                  return 1;
               } else if (i_0 == 3135) {
                  class253.field3267 -= 2;
                  return 1;
               } else if (i_0 == 3136) {
                  client.field810 = 3;
                  client.field811 = class85.field1095[--class253.field3267];
                  return 1;
               } else if (i_0 == 3137) {
                  client.field810 = 2;
                  client.field811 = class85.field1095[--class253.field3267];
                  return 1;
               } else if (i_0 == 3138) {
                  client.field810 = 0;
                  return 1;
               } else if (i_0 == 3139) {
                  client.field810 = 1;
                  return 1;
               } else if (i_0 == 3140) {
                  client.field810 = 3;
                  client.field811 = bool_2 ? class223.field2561.field2586 : class253.field3264.field2586;
                  return 1;
               } else {
                  boolean bool_17;
                  if (i_0 == 3141) {
                     bool_17 = class85.field1095[--class253.field3267] == 1;
                     class282.field3617.field1053 = bool_17;
                     class18.method187(-214572887);
                     return 1;
                  } else if (i_0 == 3142) {
                     class85.field1095[++class253.field3267 - 1] = class282.field3617.field1053 ? 1 : 0;
                     return 1;
                  } else if (i_0 == 3143) {
                     bool_17 = class85.field1095[--class253.field3267] == 1;
                     client.field684 = bool_17;
                     if (!bool_17) {
                        class282.field3617.field1050 = "";
                        class18.method187(25813270);
                     }

                     return 1;
                  } else if (i_0 == 3144) {
                     class85.field1095[++class253.field3267 - 1] = client.field684 ? 1 : 0;
                     return 1;
                  } else if (i_0 == 3145) {
                     return 1;
                  } else if (i_0 == 3146) {
                     bool_17 = class85.field1095[--class253.field3267] == 1;
                     if (bool_17 == class282.field3617.field1063) {
                        class282.field3617.field1063 = !bool_17;
                        class18.method187(-1606156887);
                     }

                     return 1;
                  } else if (i_0 == 3147) {
                     class85.field1095[++class253.field3267 - 1] = class282.field3617.field1063 ? 0 : 1;
                     return 1;
                  } else if (i_0 == 3148) {
                     return 1;
                  } else if (i_0 == 3149) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3150) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3151) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3152) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3153) {
                     class85.field1095[++class253.field3267 - 1] = class94.field1187;
                     return 1;
                  } else if (i_0 == 3154) {
                     class85.field1095[++class253.field3267 - 1] = class13.method132(2039505638);
                     return 1;
                  } else if (i_0 == 3155) {
                     --class85.field1105;
                     return 1;
                  } else if (i_0 == 3156) {
                     return 1;
                  } else if (i_0 == 3157) {
                     class253.field3267 -= 2;
                     return 1;
                  } else if (i_0 == 3158) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3159) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3160) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3161) {
                     --class253.field3267;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3162) {
                     --class253.field3267;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3163) {
                     --class85.field1105;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3164) {
                     --class253.field3267;
                     class85.field1096[++class85.field1105 - 1] = "";
                     return 1;
                  } else if (i_0 == 3165) {
                     --class253.field3267;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3166) {
                     class253.field3267 -= 2;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3167) {
                     class253.field3267 -= 2;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3168) {
                     class253.field3267 -= 2;
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     class85.field1096[++class85.field1105 - 1] = "";
                     return 1;
                  } else if (i_0 == 3169) {
                     return 1;
                  } else if (i_0 == 3170) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3171) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3172) {
                     --class253.field3267;
                     return 1;
                  } else if (i_0 == 3173) {
                     --class253.field3267;
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3174) {
                     --class253.field3267;
                     return 1;
                  } else if (i_0 == 3175) {
                     class85.field1095[++class253.field3267 - 1] = 0;
                     return 1;
                  } else if (i_0 == 3176) {
                     return 1;
                  } else if (i_0 == 3177) {
                     return 1;
                  } else if (i_0 == 3178) {
                     --class85.field1105;
                     return 1;
                  } else if (i_0 == 3179) {
                     return 1;
                  } else if (i_0 == 3180) {
                     --class85.field1105;
                     return 1;
                  } else if (i_0 == 3181) {
                     i_16 = 100 - Math.min(Math.max(class85.field1095[--class253.field3267], 0), 100);
                     class42.method626((double)((float)i_16 / 200.0F + 0.5F));
                     return 1;
                  } else if (i_0 == 3182) {
                     float f_9 = ((float)class282.field3617.field1057 - 0.5F) * 200.0F;
                     class85.field1095[++class253.field3267 - 1] = 100 - Math.round(f_9);
                     return 1;
                  } else {
                     return 2;
                  }
               }
            }
         }
      }
   }

   static int method599(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      if (i_0 == 6200) {
         class253.field3267 -= 2;
         client.field896 = (short)class79.method1795(class85.field1095[class253.field3267], 307117656);
         if (client.field896 <= 0) {
            client.field896 = 256;
         }

         client.field897 = (short)class79.method1795(class85.field1095[class253.field3267 + 1], -2025366096);
         if (client.field897 <= 0) {
            client.field897 = 256;
         }

         return 1;
      } else if (i_0 == 6201) {
         class253.field3267 -= 2;
         client.field914 = (short)class85.field1095[class253.field3267];
         if (client.field914 <= 0) {
            client.field914 = 256;
         }

         client.field899 = (short)class85.field1095[class253.field3267 + 1];
         if (client.field899 <= 0) {
            client.field899 = 320;
         }

         return 1;
      } else if (i_0 == 6202) {
         class253.field3267 -= 4;
         client.field900 = (short)class85.field1095[class253.field3267];
         if (client.field900 <= 0) {
            client.field900 = 1;
         }

         client.field731 = (short)class85.field1095[class253.field3267 + 1];
         if (client.field731 <= 0) {
            client.field731 = 32767;
         } else if (client.field731 < client.field900) {
            client.field731 = client.field900;
         }

         client.field828 = (short)class85.field1095[class253.field3267 + 2];
         if (client.field828 <= 0) {
            client.field828 = 1;
         }

         client.field903 = (short)class85.field1095[class253.field3267 + 3];
         if (client.field903 <= 0) {
            client.field903 = 32767;
         } else if (client.field903 < client.field828) {
            client.field903 = client.field828;
         }

         return 1;
      } else if (i_0 == 6203) {
         if (client.field869 != null) {
            class25.method249(0, 0, client.field869.field2601, client.field869.field2602, false, 473576557);
            class85.field1095[++class253.field3267 - 1] = client.field906;
            class85.field1095[++class253.field3267 - 1] = client.field695;
         } else {
            class85.field1095[++class253.field3267 - 1] = -1;
            class85.field1095[++class253.field3267 - 1] = -1;
         }

         return 1;
      } else if (i_0 == 6204) {
         class85.field1095[++class253.field3267 - 1] = client.field914;
         class85.field1095[++class253.field3267 - 1] = client.field899;
         return 1;
      } else if (i_0 == 6205) {
         class85.field1095[++class253.field3267 - 1] = class63.method1156(client.field896, -870366396);
         class85.field1095[++class253.field3267 - 1] = class63.method1156(client.field897, -1678759171);
         return 1;
      } else if (i_0 == 6220) {
         class85.field1095[++class253.field3267 - 1] = 0;
         return 1;
      } else if (i_0 == 6221) {
         class85.field1095[++class253.field3267 - 1] = 0;
         return 1;
      } else if (i_0 == 6222) {
         class85.field1095[++class253.field3267 - 1] = class286.field3638;
         return 1;
      } else if (i_0 == 6223) {
         class85.field1095[++class253.field3267 - 1] = class143.field1892;
         return 1;
      } else {
         return 2;
      }
   }

}
