import java.io.IOException;
import java.net.Socket;

public class class219 {

   static class218 field2528;

   static final void method3972(String string_0, int i_1) {
      StringBuilder stringbuilder_10000 = (new StringBuilder()).append(string_0);
      Object obj_10001 = null;
      String string_2 = stringbuilder_10000.append(" is already on your friend list").toString();
      class234.method4129(30, "", string_2, (byte) 66);
   }

   public static class315 method3980(Socket socket_0, int i_1, int i_2, byte b_3) throws IOException {
      return new class316(socket_0, i_1, i_2);
   }

   public static class251 method3976(int i_0, int i_1) {
      class251 class251_2 = (class251) class251.field3236.method3376((long)i_0);
      if (class251_2 != null) {
         return class251_2;
      } else {
         byte[] bytes_3 = class1.field3.method4160(16, i_0, (short) -5442);
         class251_2 = new class251();
         if (bytes_3 != null) {
            class251_2.method4355(new class310(bytes_3), -1194573420);
         }

         class251.field3236.method3374(class251_2, (long)i_0);
         return class251_2;
      }
   }

   static void method3978(int i_0) {
      if (class94.field1196 == null || class94.field1196.length() <= 0) {
         if (class282.field3617.field1050 != null) {
            class94.field1196 = class282.field3617.field1050;
            client.field684 = true;
         } else {
            client.field684 = false;
         }

      }
   }

   public static boolean method3979(class244 class244_0, class244 class244_1, int i_2) {
      class252.field3241 = class244_1;
      if (!class244_0.method4165(-1616202119)) {
         return false;
      } else {
         class252.field3262 = class244_0.method4172(35, (byte) 12);
         class252.field3243 = new class252[class252.field3262];

         for (int i_3 = 0; i_3 < class252.field3262; i_3++) {
            byte[] bytes_4 = class244_0.method4160(35, i_3, (short) -27430);
            class252.field3243[i_3] = new class252(i_3);
            if (bytes_4 != null) {
               class252.field3243[i_3].method4359(new class310(bytes_4), -1959908559);
               class252.field3243[i_3].method4358(-604734124);
            }
         }

         return true;
      }
   }

}
