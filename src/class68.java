public class class68 extends class189 {

   static class246 field544;
   static class334 field543;
   static class328 field546 = new class328(32);
   int[] field540 = new int[] {-1};
   int[] field539 = new int[] {0};

   public static class196 method1249(class192 class192_0, class338 class338_1, byte b_2) {
      class196 class196_3;
      if (class196.field2358 == 0) {
         class196_3 = new class196();
      } else {
         class196_3 = class196.field2365[--class196.field2358];
      }

      class196_3.field2362 = class192_0;
      class196_3.field2359 = class192_0.field2331;
      if (class196_3.field2359 == -1) {
         class196_3.field2360 = new class309(260);
      } else if (class196_3.field2359 == -2) {
         class196_3.field2360 = new class309(10000);
      } else if (class196_3.field2359 <= 18) {
         class196_3.field2360 = new class309(20);
      } else if (class196_3.field2359 <= 98) {
         class196_3.field2360 = new class309(100);
      } else {
         class196_3.field2360 = new class309(260);
      }

      class196_3.field2360.method5448(class338_1, (byte) -50);
      class196_3.field2360.method5479(class196_3.field2362.field2300, 781650330);
      class196_3.field2366 = 0;
      return class196_3;
   }

   static final void method1251(boolean bool_0, class309 class309_1, int i_2) {
      client.field706 = bool_0;
      int i_3;
      int i_5;
      int i_6;
      int i_7;
      int i_8;
      int i_9;
      if (!client.field706) {
         i_3 = class309_1.method5729(-1686392111);
         int i_4 = class309_1.method5499(-1597569596);
         i_5 = class309_1.method5729(1461531494);
         class227.field2731 = new int[i_5][4];

         for (i_6 = 0; i_6 < i_5; i_6++) {
            for (i_7 = 0; i_7 < 4; i_7++) {
               class227.field2731[i_6][i_7] = class309_1.method5507(-320414121);
            }
         }

         class178.field2075 = new int[i_5];
         class41.field297 = new int[i_5];
         class17.field76 = new int[i_5];
         class27.field151 = new byte[i_5][];
         class72.field590 = new byte[i_5][];
         boolean bool_17 = false;
         if ((i_3 / 8 == 48 || i_3 / 8 == 49) && i_4 / 8 == 48) {
            bool_17 = true;
         }

         if (i_3 / 8 == 48 && i_4 / 8 == 148) {
            bool_17 = true;
         }

         i_5 = 0;

         for (i_7 = (i_3 - 6) / 8; i_7 <= (i_3 + 6) / 8; i_7++) {
            for (i_8 = (i_4 - 6) / 8; i_8 <= (i_4 + 6) / 8; i_8++) {
               i_9 = i_8 + (i_7 << 8);
               if (!bool_17 || i_8 != 49 && i_8 != 149 && i_8 != 147 && i_7 != 50 && (i_7 != 49 || i_8 != 47)) {
                  class178.field2075[i_5] = i_9;
                  class41.field297[i_5] = class62.field478.method4206("m" + i_7 + "_" + i_8, -1992991390);
                  class17.field76[i_5] = class62.field478.method4206("l" + i_7 + "_" + i_8, -2076091005);
                  ++i_5;
               }
            }
         }

         class253.method4395(i_3, i_4, true, 1702821448);
      } else {
         i_3 = class309_1.method5729(383996803);
         boolean bool_16 = class309_1.method5533((byte) -9) == 1;
         i_5 = class309_1.method5540(-1856185425);
         i_6 = class309_1.method5729(-561363575);
         class309_1.method5454(402633538);

         int i_10;
         for (i_7 = 0; i_7 < 4; i_7++) {
            for (i_8 = 0; i_8 < 13; i_8++) {
               for (i_9 = 0; i_9 < 13; i_9++) {
                  i_10 = class309_1.method5455(1, (byte) -76);
                  if (i_10 == 1) {
                     client.field683[i_7][i_8][i_9] = class309_1.method5455(26, (byte) -90);
                  } else {
                     client.field683[i_7][i_8][i_9] = -1;
                  }
               }
            }
         }

         class309_1.method5477(-349919515);
         class227.field2731 = new int[i_6][4];

         for (i_7 = 0; i_7 < i_6; i_7++) {
            for (i_8 = 0; i_8 < 4; i_8++) {
               class227.field2731[i_7][i_8] = class309_1.method5507(1871720653);
            }
         }

         class178.field2075 = new int[i_6];
         class41.field297 = new int[i_6];
         class17.field76 = new int[i_6];
         class27.field151 = new byte[i_6][];
         class72.field590 = new byte[i_6][];
         i_6 = 0;

         for (i_7 = 0; i_7 < 4; i_7++) {
            for (i_8 = 0; i_8 < 13; i_8++) {
               for (i_9 = 0; i_9 < 13; i_9++) {
                  i_10 = client.field683[i_7][i_8][i_9];
                  if (i_10 != -1) {
                     int i_11 = i_10 >> 14 & 0x3ff;
                     int i_12 = i_10 >> 3 & 0x7ff;
                     int i_13 = (i_11 / 8 << 8) + i_12 / 8;

                     int i_14;
                     for (i_14 = 0; i_14 < i_6; i_14++) {
                        if (class178.field2075[i_14] == i_13) {
                           i_13 = -1;
                           break;
                        }
                     }

                     if (i_13 != -1) {
                        class178.field2075[i_6] = i_13;
                        i_14 = i_13 >> 8 & 0xff;
                        int i_15 = i_13 & 0xff;
                        class41.field297[i_6] = class62.field478.method4206("m" + i_14 + "_" + i_15, -1921993952);
                        class17.field76[i_6] = class62.field478.method4206("l" + i_14 + "_" + i_15, -2093727987);
                        ++i_6;
                     }
                  }
               }
            }
         }

         class253.method4395(i_5, i_3, !bool_16, -7034516);
      }

   }

   public static class12 method1250(class7 class7_0, int i_1) {
      switch(class7_0.field26) {
      case 0:
         return new class5();
      default:
         throw new IllegalArgumentException();
      }
   }

   static final void method1248(class226 class226_0, int i_1, byte[] bytes_2, byte[] bytes_3, int i_4) {
      if (class226_0.field2649 == null) {
         if (bytes_2 == null) {
            return;
         }

         class226_0.field2649 = new byte[11][];
         class226_0.field2637 = new byte[11][];
         class226_0.field2679 = new int[11];
         class226_0.field2664 = new int[11];
      }

      class226_0.field2649[i_1] = bytes_2;
      if (bytes_2 != null) {
         class226_0.field2590 = true;
      } else {
         class226_0.field2590 = false;

         for (int i_5 = 0; i_5 < class226_0.field2649.length; i_5++) {
            if (class226_0.field2649[i_5] != null) {
               class226_0.field2590 = true;
               break;
            }
         }
      }

      class226_0.field2637[i_1] = bytes_3;
   }

}
