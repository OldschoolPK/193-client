import java.util.Date;

public class class3 implements class203 {

   public static final class3 field12 = new class3(0, 0, Integer.class, new class1());
   public static final class3 field13 = new class3(1, 1, Long.class, new class2());
   public static final class3 field8 = new class3(2, 2, String.class, new class4());
   final int field9;
   public final int field10;
   public final Class field6;
   final class0 field14;

   class3(int i_1, int i_2, Class class_3, class0 class0_4) {
      this.field9 = i_1;
      this.field10 = i_2;
      this.field6 = class_3;
      this.field14 = class0_4;
   }

   public int vmethod6086(int i_1) {
      return this.field10;
   }

   public Object method28(class310 class310_1, int i_2) {
      return this.field14.vmethod48(class310_1, (byte) 12);
   }

   public static void method43(int i_0) {
      if (class63.field485 != null) {
         class63 class63_1 = class63.field485;
         synchronized(class63.field485) {
            class63.field485 = null;
         }
      }

   }

   public static String method40(long long_0) {
      class216.field2516.setTime(new Date(long_0));
      int i_2 = class216.field2516.get(7);
      int i_3 = class216.field2516.get(5);
      int i_4 = class216.field2516.get(2);
      int i_5 = class216.field2516.get(1);
      int i_6 = class216.field2516.get(11);
      int i_7 = class216.field2516.get(12);
      int i_8 = class216.field2516.get(13);
      return class216.field2517[i_2 - 1] + ", " + i_3 / 10 + i_3 % 10 + "-" + class216.field2515[0][i_4] + "-" + i_5 + " " + i_6 / 10 + i_6 % 10 + ":" + i_7 / 10 + i_7 % 10 + ":" + i_8 / 10 + i_8 % 10 + " GMT";
   }

   public static void method25(Object object_0, class310 class310_1, int i_2) {
      class0 class0_3 = method37(object_0.getClass(), 1667520167);
      class0_3.vmethod49(object_0, class310_1, 781194334);
   }

   static class0 method37(Class class_0, int i_1) {
      class3[] arr_3 = new class3[] {field13, field12, field8};
      class3[] arr_4 = arr_3;
      int i_5 = 0;

      class3 class3_2;
      while (true) {
         if (i_5 >= arr_4.length) {
            class3_2 = null;
            break;
         }

         class3 class3_6 = arr_4[i_5];
         if (class3_6.field6 == class_0) {
            class3_2 = class3_6;
            break;
         }

         ++i_5;
      }

      if (class3_2 == null) {
         throw new IllegalArgumentException();
      } else {
         return class3_2.field14;
      }
   }

   static void method31(int i_0, int i_1, int i_2, int i_3, String string_4, int i_5) {
      class226 class226_6 = class92.method2103(i_1, i_2, -100010964);
      if (class226_6 != null) {
         if (class226_6.field2613 != null) {
            class71 class71_7 = new class71();
            class71_7.field571 = class226_6;
            class71_7.field574 = i_0;
            class71_7.field578 = string_4;
            class71_7.field572 = class226_6.field2613;
            class22.method219(class71_7, -1834138284);
         }

         boolean bool_12 = true;
         if (class226_6.field2695 > 0) {
            bool_12 = class79.method1797(class226_6, -1033575500);
         }

         if (bool_12) {
            int i_9 = class146.method3318(class226_6, 630832052);
            int i_10 = i_0 - 1;
            boolean bool_8 = (i_9 >> i_10 + 1 & 0x1) != 0;
            if (bool_8) {
               class196 class196_11;
               if (i_0 == 1) {
                  class196_11 = class68.method1249(class192.field2240, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -3);
                  class196_11.field2360.method5486(i_2, (byte) -57);
                  class196_11.field2360.method5486(i_3, (byte) -77);
                  client.field694.method2295(class196_11, 1927516970);
               }

               if (i_0 == 2) {
                  class196_11 = class68.method1249(class192.field2286, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -13);
                  class196_11.field2360.method5486(i_2, (byte) -77);
                  class196_11.field2360.method5486(i_3, (byte) -105);
                  client.field694.method2295(class196_11, 908054610);
               }

               if (i_0 == 3) {
                  class196_11 = class68.method1249(class192.field2249, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -54);
                  class196_11.field2360.method5486(i_2, (byte) -87);
                  class196_11.field2360.method5486(i_3, (byte) -124);
                  client.field694.method2295(class196_11, -74237440);
               }

               if (i_0 == 4) {
                  class196_11 = class68.method1249(class192.field2320, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -10);
                  class196_11.field2360.method5486(i_2, (byte) -49);
                  class196_11.field2360.method5486(i_3, (byte) -12);
                  client.field694.method2295(class196_11, 2042147502);
               }

               if (i_0 == 5) {
                  class196_11 = class68.method1249(class192.field2247, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -74);
                  class196_11.field2360.method5486(i_2, (byte) -70);
                  class196_11.field2360.method5486(i_3, (byte) -65);
                  client.field694.method2295(class196_11, -71979887);
               }

               if (i_0 == 6) {
                  class196_11 = class68.method1249(class192.field2289, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -87);
                  class196_11.field2360.method5486(i_2, (byte) -86);
                  class196_11.field2360.method5486(i_3, (byte) -96);
                  client.field694.method2295(class196_11, -820523880);
               }

               if (i_0 == 7) {
                  class196_11 = class68.method1249(class192.field2316, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -112);
                  class196_11.field2360.method5486(i_2, (byte) -19);
                  class196_11.field2360.method5486(i_3, (byte) -46);
                  client.field694.method2295(class196_11, -1846878343);
               }

               if (i_0 == 8) {
                  class196_11 = class68.method1249(class192.field2252, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -56);
                  class196_11.field2360.method5486(i_2, (byte) -37);
                  class196_11.field2360.method5486(i_3, (byte) -99);
                  client.field694.method2295(class196_11, -1984128429);
               }

               if (i_0 == 9) {
                  class196_11 = class68.method1249(class192.field2307, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -48);
                  class196_11.field2360.method5486(i_2, (byte) -100);
                  class196_11.field2360.method5486(i_3, (byte) -106);
                  client.field694.method2295(class196_11, -1684190816);
               }

               if (i_0 == 10) {
                  class196_11 = class68.method1249(class192.field2231, client.field694.field1328, (byte) 1);
                  class196_11.field2360.method5510(i_1, (byte) -113);
                  class196_11.field2360.method5486(i_2, (byte) -111);
                  class196_11.field2360.method5486(i_3, (byte) -62);
                  client.field694.method2295(class196_11, 1454170637);
               }

            }
         }
      }
   }

   public static final void method41(int i_0, int i_1, int i_2) {
      class137.field1764 = i_0;
      class137.field1755 = i_1;
      class137.field1756 = true;
      class137.field1763 = 0;
      class137.field1757 = false;
   }

   static boolean method39(int i_0) {
      return client.field724;
   }

   static boolean method42(class75 class75_0, byte b_1) {
      if (client.field768 == 0) {
         return false;
      } else if (class223.field2562 != class75_0) {
         boolean bool_2 = (client.field768 & 0x4) != 0;
         boolean bool_3 = bool_2;
         if (!bool_2) {
            boolean bool_4 = (client.field768 & 0x1) != 0;
            bool_3 = bool_4 && class75_0.method1331(1089384688);
         }

         return bool_3 || class183.method3620((byte) 16) && class75_0.method1330((byte) -22);
      } else {
         return class63.method1150(-1220632952);
      }
   }

}
