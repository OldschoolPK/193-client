public class class107 extends class124 {

   class272 field1358 = new class272();
   class272 field1357 = new class272();
   int field1356 = 0;
   int field1359 = -1;

   void method2333(int[] ints_1, int i_2, int i_3) {
      for (class124 class124_4 = (class124) this.field1358.method4879(); class124_4 != null; class124_4 = (class124) this.field1358.method4884()) {
         class124_4.method2775(ints_1, i_2, i_3);
      }

   }

   void method2335(int i_1) {
      for (class124 class124_2 = (class124) this.field1358.method4879(); class124_2 != null; class124_2 = (class124) this.field1358.method4884()) {
         class124_2.vmethod3898(i_1);
      }

   }

   public final synchronized void vmethod3905(int[] ints_1, int i_2, int i_3) {
      do {
         if (this.field1359 < 0) {
            this.method2333(ints_1, i_2, i_3);
            return;
         }

         if (i_3 + this.field1356 < this.field1359) {
            this.field1356 += i_3;
            this.method2333(ints_1, i_2, i_3);
            return;
         }

         int i_4 = this.field1359 - this.field1356;
         this.method2333(ints_1, i_2, i_4);
         i_2 += i_4;
         i_3 -= i_4;
         this.field1356 += i_4;
         this.method2326();
         class119 class119_5 = (class119) this.field1357.method4879();
         synchronized(class119_5) {
            int i_7 = class119_5.method2551(this);
            if (i_7 < 0) {
               class119_5.field1464 = 0;
               this.method2323(class119_5);
            } else {
               class119_5.field1464 = i_7;
               this.method2327(class119_5.field2136, class119_5);
            }
         }
      } while (i_3 != 0);

   }

   public final synchronized void vmethod3898(int i_1) {
      do {
         if (this.field1359 < 0) {
            this.method2335(i_1);
            return;
         }

         if (this.field1356 + i_1 < this.field1359) {
            this.field1356 += i_1;
            this.method2335(i_1);
            return;
         }

         int i_2 = this.field1359 - this.field1356;
         this.method2335(i_2);
         i_1 -= i_2;
         this.field1356 += i_2;
         this.method2326();
         class119 class119_3 = (class119) this.field1357.method4879();
         synchronized(class119_3) {
            int i_5 = class119_3.method2551(this);
            if (i_5 < 0) {
               class119_3.field1464 = 0;
               this.method2323(class119_3);
            } else {
               class119_3.field1464 = i_5;
               this.method2327(class119_3.field2136, class119_3);
            }
         }
      } while (i_1 != 0);

   }

   protected class124 vmethod3917() {
      return (class124) this.field1358.method4884();
   }

   void method2326() {
      if (this.field1356 > 0) {
         for (class119 class119_1 = (class119) this.field1357.method4879(); class119_1 != null; class119_1 = (class119) this.field1357.method4884()) {
            class119_1.field1464 -= this.field1356;
         }

         this.field1359 -= this.field1356;
         this.field1356 = 0;
      }

   }

   public final synchronized void method2324(class124 class124_1) {
      this.field1358.method4878(class124_1);
   }

   protected class124 vmethod3894() {
      return (class124) this.field1358.method4879();
   }

   public final synchronized void method2325(class124 class124_1) {
      class124_1.method3628();
   }

   void method2323(class119 class119_1) {
      class119_1.method3628();
      class119_1.method2553();
      class189 class189_2 = this.field1357.field3568.field2136;
      if (class189_2 == this.field1357.field3568) {
         this.field1359 = -1;
      } else {
         this.field1359 = ((class119) class189_2).field1464;
      }

   }

   void method2327(class189 class189_1, class119 class119_2) {
      while (this.field1357.field3568 != class189_1 && ((class119) class189_1).field1464 <= class119_2.field1464) {
         class189_1 = class189_1.field2136;
      }

      class272.method4885(class119_2, class189_1);
      this.field1359 = ((class119) this.field1357.field3568.field2136).field1464;
   }

   protected int vmethod3896() {
      return 0;
   }

}
