import java.math.BigInteger;

public class class92 {

   static class246 field1167;
   static int[][] field1173;
   static int[] field1172;
   static final BigInteger field1170 = new BigInteger("10001", 16);
   static final BigInteger field1168 = new BigInteger("dd21a8a5967a8b649a4d1980e05ac798da2042dbdb5e69474723d07eccdaf072255335961f8bf0fe349cf76dabac033009bedd996f245a7767a82616f5e54ad27b8ad4b4b02c518c6a1c17dade843016db14f03d54fabc3d8ccb18f51b24b1c0757a16e3f7f6636293112d4b02a5a88a17bed6dd2e95d303a4007b49cf5c5f41f4e8cabba07bec2c11c5f35a0da29d217d0ff67b4d9aa43dfa7523e0af7fd0b86d49f91b55d01643c1e8aabe5f0803f8a9971fc2cd6225ffe7fcf32ad1286311a1a09e0dbfd9d75cc2c81db35ca4e920ae9a8a1c15f6acc13ebc320c7ffbb1232be0b91d6c84d869d31a9fb4fef130f344f9a6229d6ac7cae1c865ba4b15cfd9", 16);

   public static class226 method2103(int i_0, int i_1, int i_2) {
      class226 class226_3 = class181.method3610(i_0, -1331276193);
      return i_1 == -1 ? class226_3 : (class226_3 != null && class226_3.field2672 != null && i_1 < class226_3.field2672.length ? class226_3.field2672[i_1] : null);
   }

}
