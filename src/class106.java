import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class class106 extends class189 {

   int field1354;
   int field1348;
   int[] field1350;
   int[] field1347;
   Field[] field1351;
   int[] field1352;
   Method[] field1353;
   byte[][][] field1349;

   static final void method2322(int i_0, int i_1, int i_2, int i_3) {
      int i_4;
      for (i_4 = 0; i_4 < 8; i_4++) {
         for (int i_5 = 0; i_5 < 8; i_5++) {
            class64.field519[i_0][i_4 + i_1][i_5 + i_2] = 0;
         }
      }

      if (i_1 > 0) {
         for (i_4 = 1; i_4 < 8; i_4++) {
            class64.field519[i_0][i_1][i_4 + i_2] = class64.field519[i_0][i_1 - 1][i_4 + i_2];
         }
      }

      if (i_2 > 0) {
         for (i_4 = 1; i_4 < 8; i_4++) {
            class64.field519[i_0][i_4 + i_1][i_2] = class64.field519[i_0][i_4 + i_1][i_2 - 1];
         }
      }

      if (i_1 > 0 && class64.field519[i_0][i_1 - 1][i_2] != 0) {
         class64.field519[i_0][i_1][i_2] = class64.field519[i_0][i_1 - 1][i_2];
      } else if (i_2 > 0 && class64.field519[i_0][i_1][i_2 - 1] != 0) {
         class64.field519[i_0][i_1][i_2] = class64.field519[i_0][i_1][i_2 - 1];
      } else if (i_1 > 0 && i_2 > 0 && class64.field519[i_0][i_1 - 1][i_2 - 1] != 0) {
         class64.field519[i_0][i_1][i_2] = class64.field519[i_0][i_1 - 1][i_2 - 1];
      }

   }

}
