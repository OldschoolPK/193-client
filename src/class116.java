import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class class116 implements Runnable {

   volatile class114[] field1455 = new class114[2];

   public void run() {
      try {
         for (int i_1 = 0; i_1 < 2; i_1++) {
            class114 class114_2 = this.field1455[i_1];
            if (class114_2 != null) {
               class114_2.method2499(2081423658);
            }
         }
      } catch (Exception exception_4) {
         class223.method4011((String) null, exception_4, 1294501888);
      }

   }

   public static int method2538(String string_0, byte b_1) {
      return string_0.length() + 2;
   }

   static int method2537(byte b_0) {
      return client.field668 ? 2 : 1;
   }

   public static void method2536(boolean bool_0, int i_1) {
      if (class247.field3194 != null) {
         try {
            class310 class310_2 = new class310(4);
            class310_2.method5482(bool_0 ? 2 : 3, (byte) -121);
            class310_2.method5487(0, (byte) 1);
            class247.field3194.vmethod5844(class310_2.field3752, 0, 4, -2028715299);
         } catch (IOException ioexception_5) {
            try {
               class247.field3194.vmethod5831((byte) 56);
            } catch (Exception exception_4) {
               ;
            }

            ++class247.field3212;
            class247.field3194 = null;
         }

      }
   }

   public static void method2533(class310 class310_0, int i_1, int i_2) {
      class106 class106_3 = new class106();
      class106_3.field1348 = class310_0.method5661((byte) -6);
      class106_3.field1354 = class310_0.method5507(360463185);
      class106_3.field1347 = new int[class106_3.field1348];
      class106_3.field1350 = new int[class106_3.field1348];
      class106_3.field1351 = new Field[class106_3.field1348];
      class106_3.field1352 = new int[class106_3.field1348];
      class106_3.field1353 = new Method[class106_3.field1348];
      class106_3.field1349 = new byte[class106_3.field1348][][];

      for (int i_4 = 0; i_4 < class106_3.field1348; i_4++) {
         try {
            int i_5 = class310_0.method5661((byte) 81);
            String string_6;
            String string_7;
            int i_8;
            if (i_5 != 0 && i_5 != 1 && i_5 != 2) {
               if (i_5 == 3 || i_5 == 4) {
                  string_6 = class310_0.method5589(-1722777706);
                  string_7 = class310_0.method5589(1274902023);
                  i_8 = class310_0.method5661((byte) -5);
                  String[] arr_9 = new String[i_8];

                  for (int i_10 = 0; i_10 < i_8; i_10++) {
                     arr_9[i_10] = class310_0.method5589(635441800);
                  }

                  String string_21 = class310_0.method5589(823637052);
                  byte[][] bytes_11 = new byte[i_8][];
                  int i_13;
                  if (i_5 == 3) {
                     for (int i_12 = 0; i_12 < i_8; i_12++) {
                        i_13 = class310_0.method5507(1574917850);
                        bytes_11[i_12] = new byte[i_13];
                        class310_0.method5514(bytes_11[i_12], 0, i_13, 1075075180);
                     }
                  }

                  class106_3.field1347[i_4] = i_5;
                  Class[] arr_22 = new Class[i_8];

                  for (i_13 = 0; i_13 < i_8; i_13++) {
                     arr_22[i_13] = class101.method2260(arr_9[i_13], 877971309);
                  }

                  Class class_23 = class101.method2260(string_21, 877971309);
                  if (class101.method2260(string_6, 877971309).getClassLoader() == null) {
                     throw new SecurityException();
                  }

                  Method[] arr_14 = class101.method2260(string_6, 877971309).getDeclaredMethods();
                  Method[] arr_15 = arr_14;

                  for (int i_16 = 0; i_16 < arr_15.length; i_16++) {
                     Method method_17 = arr_15[i_16];
                     if (method_17.getName().equals(string_7)) {
                        Class[] arr_18 = method_17.getParameterTypes();
                        if (arr_18.length == arr_22.length) {
                           boolean bool_19 = true;

                           for (int i_20 = 0; i_20 < arr_22.length; i_20++) {
                              if (arr_22[i_20] != arr_18[i_20]) {
                                 bool_19 = false;
                                 break;
                              }
                           }

                           if (bool_19 && class_23 == method_17.getReturnType()) {
                              class106_3.field1353[i_4] = method_17;
                           }
                        }
                     }
                  }

                  class106_3.field1349[i_4] = bytes_11;
               }
            } else {
               string_6 = class310_0.method5589(-903480561);
               string_7 = class310_0.method5589(-103632293);
               i_8 = 0;
               if (i_5 == 1) {
                  i_8 = class310_0.method5507(1679620690);
               }

               class106_3.field1347[i_4] = i_5;
               class106_3.field1352[i_4] = i_8;
               if (class101.method2260(string_6, 877971309).getClassLoader() == null) {
                  throw new SecurityException();
               }

               class106_3.field1351[i_4] = class101.method2260(string_6, 877971309).getDeclaredField(string_7);
            }
         } catch (ClassNotFoundException classnotfoundexception_25) {
            class106_3.field1350[i_4] = -1;
         } catch (SecurityException securityexception_26) {
            class106_3.field1350[i_4] = -2;
         } catch (NullPointerException nullpointerexception_27) {
            class106_3.field1350[i_4] = -3;
         } catch (Exception exception_28) {
            class106_3.field1350[i_4] = -4;
         } catch (Throwable throwable_29) {
            class106_3.field1350[i_4] = -5;
         }
      }

      class105.field1342.method4811(class106_3);
   }

}
