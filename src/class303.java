import java.util.HashMap;

public class class303 {

   class244 field3713;
   class244 field3710;
   HashMap field3711;

   public class303(class244 class244_1, class244 class244_2) {
      this.field3713 = class244_1;
      this.field3710 = class244_2;
      this.field3711 = new HashMap();
   }

   public HashMap method5320(class302[] arr_1, short s_2) {
      HashMap hashmap_3 = new HashMap();
      class302[] arr_4 = arr_1;

      for (int i_5 = 0; i_5 < arr_4.length; i_5++) {
         class302 class302_6 = arr_4[i_5];
         if (this.field3711.containsKey(class302_6)) {
            hashmap_3.put(class302_6, this.field3711.get(class302_6));
         } else {
            class305 class305_7 = class169.method3506(this.field3713, this.field3710, class302_6.field3706, "", (byte) 35);
            if (class305_7 != null) {
               this.field3711.put(class302_6, class305_7);
               hashmap_3.put(class302_6, class305_7);
            }
         }
      }

      return hashmap_3;
   }

}
