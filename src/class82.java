import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class class82 {

   static int field1055 = 7;
   int field1054 = 1;
   String field1050 = null;
   boolean field1053 = false;
   double field1057 = 0.8D;
   int field1056 = 127;
   int field1059 = 127;
   int field1060 = 127;
   LinkedHashMap field1058 = new LinkedHashMap();
   boolean field1052;
   boolean field1063;

   class82() {
      this.method1880(true, -1410665690);
   }

   class82(class310 class310_1) {
      if (class310_1 != null && class310_1.field3752 != null) {
         int i_2 = class310_1.method5661((byte) 21);
         if (i_2 >= 0 && i_2 <= field1055) {
            if (class310_1.method5661((byte) 31) == 1) {
               this.field1052 = true;
            }

            if (i_2 > 1) {
               this.field1063 = class310_1.method5661((byte) 43) == 1;
            }

            if (i_2 > 3) {
               this.field1054 = class310_1.method5661((byte) 34);
            }

            if (i_2 > 2) {
               int i_3 = class310_1.method5661((byte) 109);

               for (int i_4 = 0; i_4 < i_3; i_4++) {
                  int i_5 = class310_1.method5507(111775970);
                  int i_6 = class310_1.method5507(-864563346);
                  this.field1058.put(Integer.valueOf(i_5), Integer.valueOf(i_6));
               }
            }

            if (i_2 > 4) {
               this.field1050 = class310_1.method5596(186780327);
            }

            if (i_2 > 5) {
               this.field1053 = class310_1.method5720(1305798677);
            }

            if (i_2 > 6) {
               this.field1057 = (double)class310_1.method5661((byte) 32) / 100.0D;
               this.field1056 = class310_1.method5661((byte) -54);
               this.field1059 = class310_1.method5661((byte) 68);
               this.field1060 = class310_1.method5661((byte) -23);
            }
         } else {
            this.method1880(true, -257082335);
         }
      } else {
         this.method1880(true, -1853335631);
      }

   }

   void method1880(boolean bool_1, int i_2) {
   }

   class310 method1881(byte b_1) {
      class310 class310_2 = new class310(100);
      class310_2.method5482(field1055, (byte) -48);
      class310_2.method5482(this.field1052 ? 1 : 0, (byte) -58);
      class310_2.method5482(this.field1063 ? 1 : 0, (byte) -78);
      class310_2.method5482(this.field1054, (byte) 0);
      class310_2.method5482(this.field1058.size(), (byte) -84);
      Iterator iterator_3 = this.field1058.entrySet().iterator();

      while (iterator_3.hasNext()) {
         Entry map$entry_4 = (Entry) iterator_3.next();
         class310_2.method5510(((Integer) map$entry_4.getKey()).intValue(), (byte) -61);
         class310_2.method5510(((Integer) map$entry_4.getValue()).intValue(), (byte) -123);
      }

      class310_2.method5492(this.field1050 != null ? this.field1050 : "", -1748540705);
      class310_2.method5491(this.field1053, (byte) 115);
      class310_2.method5482((int)(100.0D * this.field1057), (byte) -95);
      class310_2.method5482(this.field1056, (byte) -67);
      class310_2.method5482(this.field1059, (byte) -24);
      class310_2.method5482(this.field1060, (byte) -46);
      return class310_2;
   }

   public static class335[] method1890(class244 class244_0, int i_1, int i_2, byte b_3) {
      if (!class304.method5323(class244_0, i_1, i_2, 975575420)) {
         return null;
      } else {
         class335[] arr_5 = new class335[class336.field3923];

         for (int i_6 = 0; i_6 < class336.field3923; i_6++) {
            class335 class335_7 = arr_5[i_6] = new class335();
            class335_7.field3916 = class215.field2511;
            class335_7.field3915 = class336.field3929;
            class335_7.field3918 = class336.field3925[i_6];
            class335_7.field3912 = class336.field3926[i_6];
            class335_7.field3913 = class336.field3927[i_6];
            class335_7.field3911 = class336.field3928[i_6];
            int i_8 = class335_7.field3911 * class335_7.field3913;
            byte[] bytes_9 = class15.field62[i_6];
            class335_7.field3917 = new int[i_8];

            for (int i_10 = 0; i_10 < i_8; i_10++) {
               class335_7.field3917[i_10] = class336.field3924[bytes_9[i_10] & 0xff];
            }
         }

         class336.field3925 = null;
         class336.field3926 = null;
         class336.field3927 = null;
         class336.field3928 = null;
         class336.field3924 = null;
         class15.field62 = null;
         return arr_5;
      }
   }

   static void method1879(class75 class75_0, boolean bool_1, int i_2) {
      if (class75_0 != null && class75_0.vmethod2080(446535211) && !class75_0.field625) {
         class75_0.field624 = false;
         if ((client.field910 && class98.field1263 > 50 || class98.field1263 > 200) && bool_1 && class75_0.field967 == class75_0.field943) {
            class75_0.field624 = true;
         }

         int i_3 = class75_0.field990 >> 7;
         int i_4 = class75_0.field938 >> 7;
         if (i_3 >= 0 && i_3 < 104 && i_4 >= 0 && i_4 < 104) {
            long long_5 = class88.method2085(0, 0, 0, false, class75_0.field628, (byte) -77);
            if (class75_0.field607 != null && client.field655 >= class75_0.field614 && client.field655 < class75_0.field615) {
               class75_0.field624 = false;
               class75_0.field613 = class97.method2165(class75_0.field990, class75_0.field938, class151.field1949, 1956611346);
               class75_0.field983 = client.field655;
               class67.field536.method3143(class151.field1949, class75_0.field990, class75_0.field938, class75_0.field613, 60, class75_0, class75_0.field939, long_5, class75_0.field620, class75_0.field606, class75_0.field622, class75_0.field623);
            } else {
               if ((class75_0.field990 & 0x7f) == 64 && (class75_0.field938 & 0x7f) == 64) {
                  if (client.field745[i_3][i_4] == client.field746) {
                     return;
                  }

                  client.field745[i_3][i_4] = client.field746;
               }

               class75_0.field613 = class97.method2165(class75_0.field990, class75_0.field938, class151.field1949, 2139248354);
               class75_0.field983 = client.field655;
               class67.field536.method3142(class151.field1949, class75_0.field990, class75_0.field938, class75_0.field613, 60, class75_0, class75_0.field939, long_5, class75_0.field950);
            }
         }
      }

   }

}
