import java.util.Iterator;

public final class class326 implements Iterable {

   int field3865 = 0;
   int field3867;
   class189[] field3864;
   class189 field3863;
   class189 field3866;

   public class326(int i_1) {
      this.field3867 = i_1;
      this.field3864 = new class189[i_1];

      for (int i_2 = 0; i_2 < i_1; i_2++) {
         class189 class189_3 = this.field3864[i_2] = new class189();
         class189_3.field2136 = class189_3;
         class189_3.field2138 = class189_3;
      }

   }

   public class189 method5919(long long_1) {
      class189 class189_3 = this.field3864[(int)(long_1 & (long)(this.field3867 - 1))];

      for (this.field3863 = class189_3.field2136; class189_3 != this.field3863; this.field3863 = this.field3863.field2136) {
         if (this.field3863.field2137 == long_1) {
            class189 class189_4 = this.field3863;
            this.field3863 = this.field3863.field2136;
            return class189_4;
         }
      }

      this.field3863 = null;
      return null;
   }

   public class189 method5922() {
      class189 class189_1;
      if (this.field3865 > 0 && this.field3864[this.field3865 - 1] != this.field3866) {
         class189_1 = this.field3866;
         this.field3866 = class189_1.field2136;
         return class189_1;
      } else {
         do {
            if (this.field3865 >= this.field3867) {
               return null;
            }

            class189_1 = this.field3864[this.field3865++].field2136;
         } while (class189_1 == this.field3864[this.field3865 - 1]);

         this.field3866 = class189_1.field2136;
         return class189_1;
      }
   }

   public void method5920() {
      for (int i_1 = 0; i_1 < this.field3867; i_1++) {
         class189 class189_2 = this.field3864[i_1];

         while (true) {
            class189 class189_3 = class189_2.field2136;
            if (class189_3 == class189_2) {
               break;
            }

            class189_3.method3628();
         }
      }

      this.field3863 = null;
      this.field3866 = null;
   }

   public void method5928(class189 class189_1, long long_2) {
      if (class189_1.field2138 != null) {
         class189_1.method3628();
      }

      class189 class189_4 = this.field3864[(int)(long_2 & (long)(this.field3867 - 1))];
      class189_1.field2138 = class189_4.field2138;
      class189_1.field2136 = class189_4;
      class189_1.field2138.field2136 = class189_1;
      class189_1.field2136.field2138 = class189_1;
      class189_1.field2137 = long_2;
   }

   public Iterator iterator() {
      return new class327(this);
   }

   public class189 method5933() {
      this.field3865 = 0;
      return this.method5922();
   }

}
