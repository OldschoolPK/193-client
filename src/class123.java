public class class123 {

   static float[][] field1499 = new float[2][8];
   static int[][] field1504 = new int[2][8];
   int[] field1500 = new int[2];
   int[][][] field1495 = new int[2][2][4];
   int[][][] field1497 = new int[2][2][4];
   int[] field1498 = new int[2];
   static float field1501;
   static int field1502;

   float method2740(int i_1, int i_2, float f_3) {
      float f_4 = (float)this.field1497[i_1][0][i_2] + f_3 * (float)(this.field1497[i_1][1][i_2] - this.field1497[i_1][0][i_2]);
      f_4 *= 0.0015258789F;
      return 1.0F - (float)Math.pow(10.0D, (double)(-f_4 / 20.0F));
   }

   float method2742(int i_1, int i_2, float f_3) {
      float f_4 = (float)this.field1495[i_1][0][i_2] + f_3 * (float)(this.field1495[i_1][1][i_2] - this.field1495[i_1][0][i_2]);
      f_4 *= 1.2207031E-4F;
      return method2741(f_4);
   }

   int method2743(int i_1, float f_2) {
      float f_3;
      if (i_1 == 0) {
         f_3 = (float)this.field1498[0] + (float)(this.field1498[1] - this.field1498[0]) * f_2;
         f_3 *= 0.0030517578F;
         field1501 = (float)Math.pow(0.1D, (double)(f_3 / 20.0F));
         field1502 = (int)(field1501 * 65536.0F);
      }

      if (this.field1500[i_1] == 0) {
         return 0;
      } else {
         f_3 = this.method2740(i_1, 0, f_2);
         field1499[i_1][0] = -2.0F * f_3 * (float)Math.cos((double)this.method2742(i_1, 0, f_2));
         field1499[i_1][1] = f_3 * f_3;

         int i_4;
         for (i_4 = 1; i_4 < this.field1500[i_1]; i_4++) {
            f_3 = this.method2740(i_1, i_4, f_2);
            float f_5 = -2.0F * f_3 * (float)Math.cos((double)this.method2742(i_1, i_4, f_2));
            float f_6 = f_3 * f_3;
            field1499[i_1][i_4 * 2 + 1] = field1499[i_1][i_4 * 2 - 1] * f_6;
            field1499[i_1][i_4 * 2] = field1499[i_1][i_4 * 2 - 1] * f_5 + field1499[i_1][i_4 * 2 - 2] * f_6;

            for (int i_7 = i_4 * 2 - 1; i_7 >= 2; --i_7) {
               field1499[i_1][i_7] += field1499[i_1][i_7 - 1] * f_5 + field1499[i_1][i_7 - 2] * f_6;
            }

            field1499[i_1][1] += field1499[i_1][0] * f_5 + f_6;
            field1499[i_1][0] += f_5;
         }

         if (i_1 == 0) {
            for (i_4 = 0; i_4 < this.field1500[0] * 2; i_4++) {
               field1499[0][i_4] *= field1501;
            }
         }

         for (i_4 = 0; i_4 < this.field1500[i_1] * 2; i_4++) {
            field1504[i_1][i_4] = (int)(field1499[i_1][i_4] * 65536.0F);
         }

         return this.field1500[i_1] * 2;
      }
   }

   final void method2744(class310 class310_1, class115 class115_2) {
      int i_3 = class310_1.method5661((byte) -63);
      this.field1500[0] = i_3 >> 4;
      this.field1500[1] = i_3 & 0xf;
      if (i_3 != 0) {
         this.field1498[0] = class310_1.method5729(-1826922284);
         this.field1498[1] = class310_1.method5729(-1308029196);
         int i_4 = class310_1.method5661((byte) 117);

         int i_5;
         int i_6;
         for (i_5 = 0; i_5 < 2; i_5++) {
            for (i_6 = 0; i_6 < this.field1500[i_5]; i_6++) {
               this.field1495[i_5][0][i_6] = class310_1.method5729(-592330380);
               this.field1497[i_5][0][i_6] = class310_1.method5729(-222136621);
            }
         }

         for (i_5 = 0; i_5 < 2; i_5++) {
            for (i_6 = 0; i_6 < this.field1500[i_5]; i_6++) {
               if ((i_4 & 1 << i_5 * 4 << i_6) != 0) {
                  this.field1495[i_5][1][i_6] = class310_1.method5729(1015810325);
                  this.field1497[i_5][1][i_6] = class310_1.method5729(-177340164);
               } else {
                  this.field1495[i_5][1][i_6] = this.field1495[i_5][0][i_6];
                  this.field1497[i_5][1][i_6] = this.field1497[i_5][0][i_6];
               }
            }
         }

         if (i_4 != 0 || this.field1498[1] != this.field1498[0]) {
            class115_2.method2520(class310_1);
         }
      } else {
         int[] ints_7 = this.field1498;
         this.field1498[1] = 0;
         ints_7[0] = 0;
      }

   }

   static float method2741(float f_0) {
      float f_1 = 32.703197F * (float)Math.pow(2.0D, (double)f_0);
      return f_1 * 3.1415927F / 11025.0F;
   }

}
