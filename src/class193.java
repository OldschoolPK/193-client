public class class193 implements class190 {

   static long field2341;
   public static final class193 field2332 = new class193(14, 0);
   static final class193 field2339 = new class193(15, 4);
   public static final class193 field2334 = new class193(16, -2);
   public static final class193 field2335 = new class193(18, -2);
   public static final class193 field2336 = new class193(19, -2);
   static final class193 field2337 = new class193(27, 0);
   static final class193[] field2333 = new class193[32];
   public final int field2338;

   static {
      class193[] arr_0 = class137.method3034((byte) 12);

      for (int i_1 = 0; i_1 < arr_0.length; i_1++) {
         field2333[arr_0[i_1].field2338] = arr_0[i_1];
      }

   }

   class193(int i_1, int i_2) {
      this.field2338 = i_1;
   }

   static void method3643(int i_0, int i_1, byte b_2) {
      if (class282.field3617.field1056 != 0 && i_0 != -1) {
         class85.method2054(class68.field544, i_0, 0, class282.field3617.field1056, false, 1903599166);
         client.field883 = true;
      }

   }

   static String method3645(int i_0) {
      String string_1;
      if (class282.field3617.field1053) {
         String string_3 = class94.field1196;
         int i_5 = string_3.length();
         char[] arr_6 = new char[i_5];

         for (int i_7 = 0; i_7 < i_5; i_7++) {
            arr_6[i_7] = 42;
         }

         String string_4 = new String(arr_6);
         string_1 = string_4;
      } else {
         string_1 = class94.field1196;
      }

      return string_1;
   }

   static final void method3644(class142 class142_0, class181[] arr_1, int i_2) {
      int i_3;
      int i_4;
      int i_5;
      int i_6;
      for (i_3 = 0; i_3 < 4; i_3++) {
         for (i_4 = 0; i_4 < 104; i_4++) {
            for (i_5 = 0; i_5 < 104; i_5++) {
               if ((class64.field507[i_3][i_4][i_5] & 0x1) == 1) {
                  i_6 = i_3;
                  if ((class64.field507[1][i_4][i_5] & 0x2) == 2) {
                     i_6 = i_3 - 1;
                  }

                  if (i_6 >= 0) {
                     arr_1[i_6].method3584(i_4, i_5, -2097917762);
                  }
               }
            }
         }
      }

      class64.field516 += (int)(Math.random() * 5.0D) - 2;
      if (class64.field516 < -8) {
         class64.field516 = -8;
      }

      if (class64.field516 > 8) {
         class64.field516 = 8;
      }

      class64.field510 += (int)(Math.random() * 5.0D) - 2;
      if (class64.field510 < -16) {
         class64.field510 = -16;
      }

      if (class64.field510 > 16) {
         class64.field510 = 16;
      }

      int i_10;
      int i_11;
      int i_12;
      int i_13;
      int i_14;
      int i_15;
      int i_16;
      int i_17;
      int i_18;
      int i_19;
      for (i_3 = 0; i_3 < 4; i_3++) {
         byte[][] bytes_43 = class64.field511[i_3];
         i_10 = (int)Math.sqrt(5100.0D);
         i_11 = i_10 * 768 >> 8;

         int i_20;
         int i_21;
         for (i_12 = 1; i_12 < 103; i_12++) {
            for (i_13 = 1; i_13 < 103; i_13++) {
               i_14 = class64.field519[i_3][i_13 + 1][i_12] - class64.field519[i_3][i_13 - 1][i_12];
               i_15 = class64.field519[i_3][i_13][i_12 + 1] - class64.field519[i_3][i_13][i_12 - 1];
               i_16 = (int)Math.sqrt((double)(i_15 * i_15 + i_14 * i_14 + 65536));
               i_17 = (i_14 << 8) / i_16;
               i_18 = 65536 / i_16;
               i_19 = (i_15 << 8) / i_16;
               i_20 = (i_19 * -50 + i_17 * -50 + i_18 * -10) / i_11 + 96;
               i_21 = (bytes_43[i_13][i_12 + 1] >> 3) + (bytes_43[i_13 - 1][i_12] >> 2) + (bytes_43[i_13][i_12 - 1] >> 2) + (bytes_43[i_13 + 1][i_12] >> 3) + (bytes_43[i_13][i_12] >> 1);
               class92.field1173[i_13][i_12] = i_20 - i_21;
            }
         }

         for (i_12 = 0; i_12 < 104; i_12++) {
            class259.field3325[i_12] = 0;
            class245.field3181[i_12] = 0;
            class92.field1172[i_12] = 0;
            class164.field1998[i_12] = 0;
            class99.field1283[i_12] = 0;
         }

         for (i_12 = -5; i_12 < 109; i_12++) {
            for (i_13 = 0; i_13 < 104; i_13++) {
               i_14 = i_12 + 5;
               if (i_14 >= 0 && i_14 < 104) {
                  i_15 = class64.field514[i_3][i_14][i_13] & 0xff;
                  if (i_15 > 0) {
                     class255 class255_44 = class252.method4372(i_15 - 1, 2016437188);
                     class259.field3325[i_13] += class255_44.field3290;
                     class245.field3181[i_13] += class255_44.field3292;
                     class92.field1172[i_13] += class255_44.field3291;
                     class164.field1998[i_13] += class255_44.field3293;
                     ++class99.field1283[i_13];
                  }
               }

               i_15 = i_12 - 5;
               if (i_15 >= 0 && i_15 < 104) {
                  i_16 = class64.field514[i_3][i_15][i_13] & 0xff;
                  if (i_16 > 0) {
                     class255 class255_45 = class252.method4372(i_16 - 1, 1557457090);
                     class259.field3325[i_13] -= class255_45.field3290;
                     class245.field3181[i_13] -= class255_45.field3292;
                     class92.field1172[i_13] -= class255_45.field3291;
                     class164.field1998[i_13] -= class255_45.field3293;
                     --class99.field1283[i_13];
                  }
               }
            }

            if (i_12 >= 1 && i_12 < 103) {
               i_13 = 0;
               i_14 = 0;
               i_15 = 0;
               i_16 = 0;
               i_17 = 0;

               for (i_18 = -5; i_18 < 109; i_18++) {
                  i_19 = i_18 + 5;
                  if (i_19 >= 0 && i_19 < 104) {
                     i_13 += class259.field3325[i_19];
                     i_14 += class245.field3181[i_19];
                     i_15 += class92.field1172[i_19];
                     i_16 += class164.field1998[i_19];
                     i_17 += class99.field1283[i_19];
                  }

                  i_20 = i_18 - 5;
                  if (i_20 >= 0 && i_20 < 104) {
                     i_13 -= class259.field3325[i_20];
                     i_14 -= class245.field3181[i_20];
                     i_15 -= class92.field1172[i_20];
                     i_16 -= class164.field1998[i_20];
                     i_17 -= class99.field1283[i_20];
                  }

                  if (i_18 >= 1 && i_18 < 103 && (!client.field910 || (class64.field507[0][i_12][i_18] & 0x2) != 0 || (class64.field507[i_3][i_12][i_18] & 0x10) == 0)) {
                     if (i_3 < class64.field506) {
                        class64.field506 = i_3;
                     }

                     i_21 = class64.field514[i_3][i_12][i_18] & 0xff;
                     int i_22 = class64.field508[i_3][i_12][i_18] & 0xff;
                     if (i_21 > 0 || i_22 > 0) {
                        int i_23 = class64.field519[i_3][i_12][i_18];
                        int i_24 = class64.field519[i_3][i_12 + 1][i_18];
                        int i_25 = class64.field519[i_3][i_12 + 1][i_18 + 1];
                        int i_26 = class64.field519[i_3][i_12][i_18 + 1];
                        int i_27 = class92.field1173[i_12][i_18];
                        int i_28 = class92.field1173[i_12 + 1][i_18];
                        int i_29 = class92.field1173[i_12 + 1][i_18 + 1];
                        int i_30 = class92.field1173[i_12][i_18 + 1];
                        int i_31 = -1;
                        int i_32 = -1;
                        int i_33;
                        int i_34;
                        if (i_21 > 0) {
                           i_33 = i_13 * 256 / i_16;
                           i_34 = i_14 / i_17;
                           int i_35 = i_15 / i_17;
                           i_31 = class26.method255(i_33, i_34, i_35, (byte) -3);
                           i_33 = i_33 + class64.field516 & 0xff;
                           i_35 += class64.field510;
                           if (i_35 < 0) {
                              i_35 = 0;
                           } else if (i_35 > 255) {
                              i_35 = 255;
                           }

                           i_32 = class26.method255(i_33, i_34, i_35, (byte) -87);
                        }

                        if (i_3 > 0) {
                           boolean bool_48 = true;
                           if (i_21 == 0 && class9.field40[i_3][i_12][i_18] != 0) {
                              bool_48 = false;
                           }

                           if (i_22 > 0 && !class183.method3619(i_22 - 1, 1923247344).field3538) {
                              bool_48 = false;
                           }

                           if (bool_48 && i_24 == i_23 && i_23 == i_25 && i_26 == i_23) {
                              class14.field52[i_3][i_12][i_18] |= 0x924;
                           }
                        }

                        i_33 = 0;
                        if (i_32 != -1) {
                           i_33 = class139.field1800[class135.method2918(i_32, 96, (byte) 123)];
                        }

                        if (i_22 == 0) {
                           class142_0.method3136(i_3, i_12, i_18, 0, 0, -1, i_23, i_24, i_25, i_26, class135.method2918(i_31, i_27, (byte) 16), class135.method2918(i_31, i_28, (byte) 111), class135.method2918(i_31, i_29, (byte) 60), class135.method2918(i_31, i_30, (byte) 72), 0, 0, 0, 0, i_33, 0);
                        } else {
                           i_34 = class9.field40[i_3][i_12][i_18] + 1;
                           byte b_49 = class293.field3663[i_3][i_12][i_18];
                           class268 class268_36 = class183.method3619(i_22 - 1, 1923247344);
                           int i_37 = class268_36.field3542;
                           int i_38;
                           int i_39;
                           int i_40;
                           int i_41;
                           if (i_37 >= 0) {
                              i_39 = class139.field1801.vmethod3330(i_37, -1799220931);
                              i_38 = -1;
                           } else if (class268_36.field3534 == 16711935) {
                              i_38 = -2;
                              i_37 = -1;
                              i_39 = -2;
                           } else {
                              i_38 = class26.method255(class268_36.field3532, class268_36.field3539, class268_36.field3540, (byte) -105);
                              i_40 = class268_36.field3532 + class64.field516 & 0xff;
                              i_41 = class268_36.field3540 + class64.field510;
                              if (i_41 < 0) {
                                 i_41 = 0;
                              } else if (i_41 > 255) {
                                 i_41 = 255;
                              }

                              i_39 = class26.method255(i_40, class268_36.field3539, i_41, (byte) -20);
                           }

                           i_40 = 0;
                           if (i_39 != -2) {
                              i_40 = class139.field1800[class9.method101(i_39, 96, (byte) 20)];
                           }

                           if (class268_36.field3537 != -1) {
                              i_41 = class268_36.field3541 + class64.field516 & 0xff;
                              int i_42 = class268_36.field3536 + class64.field510;
                              if (i_42 < 0) {
                                 i_42 = 0;
                              } else if (i_42 > 255) {
                                 i_42 = 255;
                              }

                              i_39 = class26.method255(i_41, class268_36.field3535, i_42, (byte) -89);
                              i_40 = class139.field1800[class9.method101(i_39, 96, (byte) 114)];
                           }

                           class142_0.method3136(i_3, i_12, i_18, i_34, b_49, i_37, i_23, i_24, i_25, i_26, class135.method2918(i_31, i_27, (byte) 55), class135.method2918(i_31, i_28, (byte) 25), class135.method2918(i_31, i_29, (byte) 109), class135.method2918(i_31, i_30, (byte) 102), class9.method101(i_38, i_27, (byte) 45), class9.method101(i_38, i_28, (byte) -33), class9.method101(i_38, i_29, (byte) -3), class9.method101(i_38, i_30, (byte) -1), i_33, i_40);
                        }
                     }
                  }
               }
            }
         }

         for (i_12 = 1; i_12 < 103; i_12++) {
            for (i_13 = 1; i_13 < 103; i_13++) {
               if ((class64.field507[i_3][i_13][i_12] & 0x8) != 0) {
                  i_18 = 0;
               } else if (i_3 > 0 && (class64.field507[1][i_13][i_12] & 0x2) != 0) {
                  i_18 = i_3 - 1;
               } else {
                  i_18 = i_3;
               }

               class142_0.method3209(i_3, i_13, i_12, i_18);
            }
         }

         class64.field514[i_3] = null;
         class64.field508[i_3] = null;
         class9.field40[i_3] = null;
         class293.field3663[i_3] = null;
         class64.field511[i_3] = null;
      }

      class142_0.method3162(-50, -10, -50);

      for (i_3 = 0; i_3 < 104; i_3++) {
         for (i_4 = 0; i_4 < 104; i_4++) {
            if ((class64.field507[1][i_3][i_4] & 0x2) == 2) {
               class142_0.method3257(i_3, i_4);
            }
         }
      }

      i_3 = 1;
      i_4 = 2;
      i_5 = 4;

      for (i_6 = 0; i_6 < 4; i_6++) {
         if (i_6 > 0) {
            i_3 <<= 3;
            i_4 <<= 3;
            i_5 <<= 3;
         }

         for (int i_7 = 0; i_7 <= i_6; i_7++) {
            for (int i_8 = 0; i_8 <= 104; i_8++) {
               for (int i_9 = 0; i_9 <= 104; i_9++) {
                  short s_47;
                  if ((class14.field52[i_7][i_9][i_8] & i_3) != 0) {
                     i_10 = i_8;
                     i_11 = i_8;
                     i_12 = i_7;

                     for (i_13 = i_7; i_10 > 0 && (class14.field52[i_7][i_9][i_10 - 1] & i_3) != 0; --i_10) {
                        ;
                     }

                     while (i_11 < 104 && (class14.field52[i_7][i_9][i_11 + 1] & i_3) != 0) {
                        ++i_11;
                     }

                     label448:
                     while (i_12 > 0) {
                        for (i_14 = i_10; i_14 <= i_11; i_14++) {
                           if ((class14.field52[i_12 - 1][i_9][i_14] & i_3) == 0) {
                              break label448;
                           }
                        }

                        --i_12;
                     }

                     label437:
                     while (i_13 < i_6) {
                        for (i_14 = i_10; i_14 <= i_11; i_14++) {
                           if ((class14.field52[i_13 + 1][i_9][i_14] & i_3) == 0) {
                              break label437;
                           }
                        }

                        ++i_13;
                     }

                     i_14 = (i_13 + 1 - i_12) * (i_11 - i_10 + 1);
                     if (i_14 >= 8) {
                        s_47 = 240;
                        i_16 = class64.field519[i_13][i_9][i_10] - s_47;
                        i_17 = class64.field519[i_12][i_9][i_10];
                        class142.method3134(i_6, 1, i_9 * 128, i_9 * 128, i_10 * 128, i_11 * 128 + 128, i_16, i_17);

                        for (i_18 = i_12; i_18 <= i_13; i_18++) {
                           for (i_19 = i_10; i_19 <= i_11; i_19++) {
                              class14.field52[i_18][i_9][i_19] &= ~i_3;
                           }
                        }
                     }
                  }

                  if ((class14.field52[i_7][i_9][i_8] & i_4) != 0) {
                     i_10 = i_9;
                     i_11 = i_9;
                     i_12 = i_7;

                     for (i_13 = i_7; i_10 > 0 && (class14.field52[i_7][i_10 - 1][i_8] & i_4) != 0; --i_10) {
                        ;
                     }

                     while (i_11 < 104 && (class14.field52[i_7][i_11 + 1][i_8] & i_4) != 0) {
                        ++i_11;
                     }

                     label501:
                     while (i_12 > 0) {
                        for (i_14 = i_10; i_14 <= i_11; i_14++) {
                           if ((class14.field52[i_12 - 1][i_14][i_8] & i_4) == 0) {
                              break label501;
                           }
                        }

                        --i_12;
                     }

                     label490:
                     while (i_13 < i_6) {
                        for (i_14 = i_10; i_14 <= i_11; i_14++) {
                           if ((class14.field52[i_13 + 1][i_14][i_8] & i_4) == 0) {
                              break label490;
                           }
                        }

                        ++i_13;
                     }

                     i_14 = (i_11 - i_10 + 1) * (i_13 + 1 - i_12);
                     if (i_14 >= 8) {
                        s_47 = 240;
                        i_16 = class64.field519[i_13][i_10][i_8] - s_47;
                        i_17 = class64.field519[i_12][i_10][i_8];
                        class142.method3134(i_6, 2, i_10 * 128, i_11 * 128 + 128, i_8 * 128, i_8 * 128, i_16, i_17);

                        for (i_18 = i_12; i_18 <= i_13; i_18++) {
                           for (i_19 = i_10; i_19 <= i_11; i_19++) {
                              class14.field52[i_18][i_19][i_8] &= ~i_4;
                           }
                        }
                     }
                  }

                  if ((class14.field52[i_7][i_9][i_8] & i_5) != 0) {
                     i_10 = i_9;
                     i_11 = i_9;
                     i_12 = i_8;

                     for (i_13 = i_8; i_12 > 0 && (class14.field52[i_7][i_9][i_12 - 1] & i_5) != 0; --i_12) {
                        ;
                     }

                     while (i_13 < 104 && (class14.field52[i_7][i_9][i_13 + 1] & i_5) != 0) {
                        ++i_13;
                     }

                     label554:
                     while (i_10 > 0) {
                        for (i_14 = i_12; i_14 <= i_13; i_14++) {
                           if ((class14.field52[i_7][i_10 - 1][i_14] & i_5) == 0) {
                              break label554;
                           }
                        }

                        --i_10;
                     }

                     label543:
                     while (i_11 < 104) {
                        for (i_14 = i_12; i_14 <= i_13; i_14++) {
                           if ((class14.field52[i_7][i_11 + 1][i_14] & i_5) == 0) {
                              break label543;
                           }
                        }

                        ++i_11;
                     }

                     if ((i_11 - i_10 + 1) * (i_13 - i_12 + 1) >= 4) {
                        i_14 = class64.field519[i_7][i_10][i_12];
                        class142.method3134(i_6, 4, i_10 * 128, i_11 * 128 + 128, i_12 * 128, i_13 * 128 + 128, i_14, i_14);

                        for (i_15 = i_10; i_15 <= i_11; i_15++) {
                           for (i_16 = i_12; i_16 <= i_13; i_16++) {
                              class14.field52[i_7][i_15][i_16] &= ~i_5;
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   }

}
