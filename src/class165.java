public class class165 extends class285 {

   final boolean field2000;

   public class165(boolean bool_1) {
      this.field2000 = bool_1;
   }

   int method3473(class289 class289_1, class289 class289_2, byte b_3) {
      if (class289_1.field3650 != 0) {
         if (class289_2.field3650 == 0) {
            return this.field2000 ? -1 : 1;
         }
      } else if (class289_2.field3650 != 0) {
         return this.field2000 ? 1 : -1;
      }

      return this.method5051(class289_1, class289_2, -1505750487);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3473((class289) object_1, (class289) object_2, (byte) 8);
   }

   public static int method3472(CharSequence charsequence_0, int i_1) {
      int i_2 = charsequence_0.length();
      int i_3 = 0;

      for (int i_4 = 0; i_4 < i_2; i_4++) {
         char var_5 = charsequence_0.charAt(i_4);
         if (var_5 <= 127) {
            ++i_3;
         } else if (var_5 <= 2047) {
            i_3 += 2;
         } else {
            i_3 += 3;
         }
      }

      return i_3;
   }

   static final void method3480(class226 class226_0, int i_1, int i_2, int i_3) {
      if (client.field894 == 0 || client.field894 == 3) {
         if (!client.field784 && (class63.field483 == 1 || !class161.field1989 && class63.field483 == 4)) {
            class220 class220_4 = class226_0.method4072(true, (byte) -21);
            if (class220_4 == null) {
               return;
            }

            int i_5 = class63.field489 - i_1;
            int i_6 = class63.field502 - i_2;
            if (class220_4.method3981(i_5, i_6, (short) 20494)) {
               i_5 -= class220_4.field2537 / 2;
               i_6 -= class220_4.field2532 / 2;
               int i_7 = client.field830 & 0x7ff;
               int i_8 = class139.field1797[i_7];
               int i_9 = class139.field1780[i_7];
               int i_10 = i_5 * i_9 + i_8 * i_6 >> 11;
               int i_11 = i_9 * i_6 - i_8 * i_5 >> 11;
               int i_12 = i_10 + class223.field2562.field990 >> 7;
               int i_13 = class223.field2562.field938 - i_11 >> 7;
               class196 class196_14 = class68.method1249(class192.field2272, client.field694.field1328, (byte) 1);
               class196_14.field2360.method5482(18, (byte) -29);
               class196_14.field2360.method5699(i_12 + class243.field3156, 212343514);
               class196_14.field2360.method5530(class54.field416[82] ? (class54.field416[81] ? 2 : 1) : 0, -1493586164);
               class196_14.field2360.method5721(i_13 + class41.field300, 1133202553);
               class196_14.field2360.method5482(i_5, (byte) -81);
               class196_14.field2360.method5482(i_6, (byte) -121);
               class196_14.field2360.method5486(client.field830, (byte) -107);
               class196_14.field2360.method5482(57, (byte) -88);
               class196_14.field2360.method5482(0, (byte) -90);
               class196_14.field2360.method5482(0, (byte) -39);
               class196_14.field2360.method5482(89, (byte) -94);
               class196_14.field2360.method5486(class223.field2562.field990, (byte) -27);
               class196_14.field2360.method5486(class223.field2562.field938, (byte) -21);
               class196_14.field2360.method5482(63, (byte) -127);
               client.field694.method2295(class196_14, -1820660418);
               client.field774 = i_12;
               client.field880 = i_13;
            }
         }

      }
   }

   static char method3481(char var_0, class199 class199_1, int i_2) {
      if (var_0 >= 192 && var_0 <= 255) {
         if (var_0 >= 192 && var_0 <= 198) {
            return 'A';
         }

         if (var_0 == 199) {
            return 'C';
         }

         if (var_0 >= 200 && var_0 <= 203) {
            return 'E';
         }

         if (var_0 >= 204 && var_0 <= 207) {
            return 'I';
         }

         if (var_0 == 209 && class199_1 != class199.field2378) {
            return 'N';
         }

         if (var_0 >= 210 && var_0 <= 214) {
            return 'O';
         }

         if (var_0 >= 217 && var_0 <= 220) {
            return 'U';
         }

         if (var_0 == 221) {
            return 'Y';
         }

         if (var_0 == 223) {
            return 's';
         }

         if (var_0 >= 224 && var_0 <= 230) {
            return 'a';
         }

         if (var_0 == 231) {
            return 'c';
         }

         if (var_0 >= 232 && var_0 <= 235) {
            return 'e';
         }

         if (var_0 >= 236 && var_0 <= 239) {
            return 'i';
         }

         if (var_0 == 241 && class199_1 != class199.field2378) {
            return 'n';
         }

         if (var_0 >= 242 && var_0 <= 246) {
            return 'o';
         }

         if (var_0 >= 249 && var_0 <= 252) {
            return 'u';
         }

         if (var_0 == 253 || var_0 == 255) {
            return 'y';
         }
      }

      if (var_0 == 338) {
         return 'O';
      } else if (var_0 == 339) {
         return 'o';
      } else if (var_0 == 376) {
         return 'Y';
      } else {
         return var_0;
      }
   }

}
