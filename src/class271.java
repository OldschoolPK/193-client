import java.util.Collection;
import java.util.Iterator;

public class class271 implements Iterable, Collection {

   class189 field3566 = new class189();
   class189 field3567;

   public class271() {
      this.field3566.field2136 = this.field3566;
      this.field3566.field2138 = this.field3566;
   }

   boolean method4799(class189 class189_1) {
      this.method4811(class189_1);
      return true;
   }

   class189[] method4808() {
      class189[] arr_1 = new class189[this.method4820()];
      int i_2 = 0;

      for (class189 class189_3 = this.field3566.field2136; class189_3 != this.field3566; class189_3 = class189_3.field2136) {
         arr_1[i_2++] = class189_3;
      }

      return arr_1;
   }

   int method4820() {
      int i_1 = 0;

      for (class189 class189_2 = this.field3566.field2136; class189_2 != this.field3566; class189_2 = class189_2.field2136) {
         ++i_1;
      }

      return i_1;
   }

   public void method4800() {
      while (this.field3566.field2136 != this.field3566) {
         this.field3566.field2136.method3628();
      }

   }

   class189 method4804(class189 class189_1) {
      class189 class189_2;
      if (class189_1 == null) {
         class189_2 = this.field3566.field2136;
      } else {
         class189_2 = class189_1;
      }

      if (class189_2 == this.field3566) {
         this.field3567 = null;
         return null;
      } else {
         this.field3567 = class189_2.field2136;
         return class189_2;
      }
   }

   public boolean method4807() {
      return this.field3566.field2136 == this.field3566;
   }

   public void method4811(class189 class189_1) {
      if (class189_1.field2138 != null) {
         class189_1.method3628();
      }

      class189_1.field2138 = this.field3566.field2138;
      class189_1.field2136 = this.field3566;
      class189_1.field2138.field2136 = class189_1;
      class189_1.field2136.field2138 = class189_1;
   }

   public class189 method4806() {
      return this.method4804((class189) null);
   }

   public class189 method4805() {
      class189 class189_1 = this.field3567;
      if (class189_1 == this.field3566) {
         this.field3567 = null;
         return null;
      } else {
         this.field3567 = class189_1.field2136;
         return class189_1;
      }
   }

   public void method4801(class189 class189_1) {
      if (class189_1.field2138 != null) {
         class189_1.method3628();
      }

      class189_1.field2138 = this.field3566;
      class189_1.field2136 = this.field3566.field2136;
      class189_1.field2138.field2136 = class189_1;
      class189_1.field2136.field2138 = class189_1;
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   public int size() {
      return this.method4820();
   }

   public boolean isEmpty() {
      return this.method4807();
   }

   public boolean contains(Object object_1) {
      throw new RuntimeException();
   }

   public Object[] toArray() {
      return this.method4808();
   }

   public Object[] toArray(Object[] arr_1) {
      int i_2 = 0;

      for (class189 class189_3 = this.field3566.field2136; class189_3 != this.field3566; class189_3 = class189_3.field2136) {
         arr_1[i_2++] = class189_3;
      }

      return arr_1;
   }

   public boolean addAll(Collection collection_1) {
      throw new RuntimeException();
   }

   public boolean removeAll(Collection collection_1) {
      throw new RuntimeException();
   }

   public void clear() {
      this.method4800();
   }

   public boolean add(Object object_1) {
      return this.method4799((class189) object_1);
   }

   public boolean remove(Object object_1) {
      throw new RuntimeException();
   }

   public Iterator iterator() {
      return new class275(this);
   }

   public boolean retainAll(Collection collection_1) {
      throw new RuntimeException();
   }

   public int hashCode() {
      return super.hashCode();
   }

   public boolean containsAll(Collection collection_1) {
      throw new RuntimeException();
   }

   public static void method4802(class189 class189_0, class189 class189_1) {
      if (class189_0.field2138 != null) {
         class189_0.method3628();
      }

      class189_0.field2138 = class189_1;
      class189_0.field2136 = class189_1.field2136;
      class189_0.field2138.field2136 = class189_0;
      class189_0.field2136.field2138 = class189_0;
   }

}
