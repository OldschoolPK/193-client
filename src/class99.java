import java.io.EOFException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class class99 {

   static class99 field1270;
   public static int field1278;
   static int field1279;
   static int[] field1283;
   boolean field1275 = false;
   Map field1273;
   boolean[] field1272;
   String[] field1281;
   long field1276;

   class99() {
      int i_1 = class102.field1310.method4172(19, (byte) 61);
      this.field1273 = new HashMap();
      this.field1272 = new boolean[i_1];

      int i_2;
      for (i_2 = 0; i_2 < i_1; i_2++) {
         class253 class253_3 = class128.method2815(i_2, 1406545495);
         this.field1272[i_2] = class253_3.field3268;
      }

      i_2 = 0;
      if (class102.field1310.method4272(15, (byte) 2)) {
         i_2 = class102.field1310.method4172(15, (byte) 39);
      }

      this.field1281 = new String[i_2];
      this.method2208((byte) 20);
   }

   class353 method2206(boolean bool_1, byte b_2) {
      return class38.method587("2", class93.field1174.field3141, bool_1, 686097505);
   }

   boolean method2199(int i_1) {
      return this.field1275;
   }

   void method2207(int i_1) {
      class353 class353_2 = this.method2206(true, (byte) -102);

      try {
         int i_3 = 3;
         int i_4 = 0;
         Iterator iterator_5 = this.field1273.entrySet().iterator();

         while (iterator_5.hasNext()) {
            Entry map$entry_6 = (Entry) iterator_5.next();
            int i_7 = ((Integer) map$entry_6.getKey()).intValue();
            if (this.field1272[i_7]) {
               Object object_8 = map$entry_6.getValue();
               i_3 += 3;
               if (object_8 instanceof Integer) {
                  i_3 += 4;
               } else if (object_8 instanceof String) {
                  i_3 += class132.method2906((String) object_8, (byte) -120);
               }

               ++i_4;
            }
         }

         class310 class310_29 = new class310(i_3);
         class310_29.method5482(2, (byte) -34);
         class310_29.method5486(i_4, (byte) -96);
         Iterator iterator_30 = this.field1273.entrySet().iterator();

         label146:
         while (true) {
            Entry map$entry_18;
            int i_19;
            do {
               if (!iterator_30.hasNext()) {
                  class353_2.method6565(class310_29.field3752, 0, class310_29.field3751, -5868778);
                  break label146;
               }

               map$entry_18 = (Entry) iterator_30.next();
               i_19 = ((Integer) map$entry_18.getKey()).intValue();
            } while (!this.field1272[i_19]);

            class310_29.method5486(i_19, (byte) -12);
            Object object_9 = map$entry_18.getValue();
            Class class_11 = object_9.getClass();
            class3[] arr_12 = new class3[] {class3.field13, class3.field12, class3.field8};
            class3[] arr_13 = arr_12;
            int i_14 = 0;

            class3 class3_10;
            while (true) {
               if (i_14 >= arr_13.length) {
                  class3_10 = null;
                  break;
               }

               class3 class3_15 = arr_13[i_14];
               if (class_11 == class3_15.field6) {
                  class3_10 = class3_15;
                  break;
               }

               ++i_14;
            }

            class310_29.method5482(class3_10.field10, (byte) -108);
            class3.method25(object_9, class310_29, -1494098506);
         }
      } catch (Exception exception_27) {
         ;
      } finally {
         try {
            class353_2.method6569((byte) 0);
         } catch (Exception exception_26) {
            ;
         }

      }

      this.field1275 = false;
      this.field1276 = class298.method5270(255749540);
   }

   void method2208(byte b_1) {
      class353 class353_2 = this.method2206(false, (byte) -42);

      label210: {
         try {
            byte[] bytes_3 = new byte[(int)class353_2.method6568(-1863935784)];

            int i_5;
            for (int i_4 = 0; i_4 < bytes_3.length; i_4 += i_5) {
               i_5 = class353_2.method6567(bytes_3, i_4, bytes_3.length - i_4, -517315128);
               if (i_5 == -1) {
                  throw new EOFException();
               }
            }

            class310 class310_16 = new class310(bytes_3);
            if (class310_16.field3752.length - class310_16.field3751 >= 1) {
               int i_17 = class310_16.method5661((byte) -67);
               if (i_17 < 0 || i_17 > 2) {
                  return;
               }

               int i_8;
               int i_9;
               int i_10;
               int i_18;
               if (i_17 >= 2) {
                  i_18 = class310_16.method5729(-1633656276);
                  i_8 = 0;

                  while (true) {
                     if (i_8 >= i_18) {
                        break label210;
                     }

                     i_9 = class310_16.method5729(1918039538);
                     i_10 = class310_16.method5661((byte) 18);
                     class3[] arr_11 = new class3[] {class3.field13, class3.field12, class3.field8};
                     class3 class3_12 = (class3) class152.method3363(arr_11, i_10, -2066411971);
                     Object object_13 = class3_12.method28(class310_16, 1456492535);
                     if (this.field1272[i_9]) {
                        this.field1273.put(Integer.valueOf(i_9), object_13);
                     }

                     ++i_8;
                  }
               } else {
                  i_18 = class310_16.method5729(-1350883704);

                  for (i_8 = 0; i_8 < i_18; i_8++) {
                     i_9 = class310_16.method5729(-218748635);
                     i_10 = class310_16.method5507(-777718667);
                     if (this.field1272[i_9]) {
                        this.field1273.put(Integer.valueOf(i_9), Integer.valueOf(i_10));
                     }
                  }

                  i_8 = class310_16.method5729(279881763);
                  i_9 = 0;

                  while (true) {
                     if (i_9 >= i_8) {
                        break label210;
                     }

                     class310_16.method5729(1080934980);
                     class310_16.method5589(182808864);
                     ++i_9;
                  }
               }
            }
         } catch (Exception exception_27) {
            break label210;
         } finally {
            try {
               class353_2.method6569((byte) 0);
            } catch (Exception exception_26) {
               ;
            }

         }

         return;
      }

      this.field1275 = false;
   }

   String method2204(int i_1, int i_2) {
      return this.field1281[i_1];
   }

   int method2200(int i_1, int i_2) {
      Object object_3 = this.field1273.get(Integer.valueOf(i_1));
      return object_3 instanceof Integer ? ((Integer) object_3).intValue() : -1;
   }

   void method2202(int i_1, int i_2, int i_3) {
      this.field1273.put(Integer.valueOf(i_1), Integer.valueOf(i_2));
      if (this.field1272[i_1]) {
         this.field1275 = true;
      }

   }

   String method2205(int i_1, byte b_2) {
      Object object_3 = this.field1273.get(Integer.valueOf(i_1));
      return object_3 instanceof String ? (String) object_3 : "";
   }

   void method2203(int i_1, String string_2, byte b_3) {
      this.field1281[i_1] = string_2;
   }

   void method2201(int i_1, String string_2, int i_3) {
      this.field1273.put(Integer.valueOf(i_1), string_2);
   }

   void method2209(int i_1) {
      if (this.field1275 && this.field1276 < class298.method5270(255749540) - 60000L) {
         this.method2207(484119221);
      }

   }

   void method2229(int i_1) {
      int i_2;
      for (i_2 = 0; i_2 < this.field1272.length; i_2++) {
         if (!this.field1272[i_2]) {
            this.field1273.remove(Integer.valueOf(i_2));
         }
      }

      for (i_2 = 0; i_2 < this.field1281.length; i_2++) {
         this.field1281[i_2] = null;
      }

   }

   static final void method2213(class124 class124_0, int i_1) {
      class124_0.field1509 = false;
      if (class124_0.field1506 != null) {
         class124_0.field1506.field1540 = 0;
      }

      for (class124 class124_2 = class124_0.vmethod3894(); class124_2 != null; class124_2 = class124_0.vmethod3917()) {
         method2213(class124_2, 1243551405);
      }

   }

   static final void method2235(class226 class226_0, int i_1, int i_2, int i_3, int i_4) {
      class220 class220_5 = class226_0.method4072(false, (byte) -5);
      if (class220_5 != null) {
         if (client.field894 < 3) {
            class152.field1964.method6143(i_1, i_2, class220_5.field2537, class220_5.field2532, 25, 25, client.field830, 256, class220_5.field2531, class220_5.field2533);
         } else {
            class331.method6018(i_1, i_2, 0, class220_5.field2531, class220_5.field2533);
         }

      }
   }

   static final void method2234(String string_0, int i_1) {
      if (!string_0.equals("")) {
         class196 class196_2 = class68.method1249(class192.field2285, client.field694.field1328, (byte) 1);
         class196_2.field2360.method5482(class132.method2906(string_0, (byte) -78), (byte) -18);
         class196_2.field2360.method5492(string_0, -1748540705);
         client.field694.method2295(class196_2, 974957734);
      }
   }

}
