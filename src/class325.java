import java.util.Comparator;

public class class325 implements Comparator {

   final boolean field3862;

   public class325(boolean bool_1) {
      this.field3862 = bool_1;
   }

   int method5908(class284 class284_1, class284 class284_2, byte b_3) {
      return this.field3862 ? class284_1.method5038(1670678120).method5193(class284_2.method5038(1670678120), -1152802722) : class284_2.method5038(1670678120).method5193(class284_1.method5038(1670678120), -1152802722);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method5908((class284) object_1, (class284) object_2, (byte) 26);
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   public static void method5916(int i_0) {
      class256.field3294.method3375();
   }

}
