public class class166 extends class285 {

   final boolean field2005;

   public class166(boolean bool_1) {
      this.field2005 = bool_1;
   }

   int method3485(class289 class289_1, class289 class289_2, byte b_3) {
      return class289_2.field3650 != class289_1.field3650 ? (this.field2005 ? class289_1.field3650 - class289_2.field3650 : class289_2.field3650 - class289_1.field3650) : this.method5051(class289_1, class289_2, 1699525726);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3485((class289) object_1, (class289) object_2, (byte) 96);
   }

   static final void method3487(class309 class309_0, byte b_1) {
      int i_2 = 0;
      class309_0.method5454(51952319);

      int i_3;
      int i_4;
      int i_5;
      for (i_3 = 0; i_3 < class98.field1263; i_3++) {
         i_4 = class98.field1268[i_3];
         if ((class98.field1252[i_4] & 0x1) == 0) {
            if (i_2 > 0) {
               --i_2;
               class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
            } else {
               i_5 = class309_0.method5455(1, (byte) -33);
               if (i_5 == 0) {
                  i_2 = class230.method4113(class309_0, -1376261869);
                  class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
               } else {
                  class349.method6517(class309_0, i_4, 2034684239);
               }
            }
         }
      }

      class309_0.method5477(1608179850);
      if (i_2 != 0) {
         throw new RuntimeException();
      } else {
         class309_0.method5454(976643865);

         for (i_3 = 0; i_3 < class98.field1263; i_3++) {
            i_4 = class98.field1268[i_3];
            if ((class98.field1252[i_4] & 0x1) != 0) {
               if (i_2 > 0) {
                  --i_2;
                  class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
               } else {
                  i_5 = class309_0.method5455(1, (byte) -122);
                  if (i_5 == 0) {
                     i_2 = class230.method4113(class309_0, 243711741);
                     class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                  } else {
                     class349.method6517(class309_0, i_4, 2143869720);
                  }
               }
            }
         }

         class309_0.method5477(-622620701);
         if (i_2 != 0) {
            throw new RuntimeException();
         } else {
            class309_0.method5454(-202324582);

            for (i_3 = 0; i_3 < class98.field1257; i_3++) {
               i_4 = class98.field1258[i_3];
               if ((class98.field1252[i_4] & 0x1) != 0) {
                  if (i_2 > 0) {
                     --i_2;
                     class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                  } else {
                     i_5 = class309_0.method5455(1, (byte) -119);
                     if (i_5 == 0) {
                        i_2 = class230.method4113(class309_0, 336282975);
                        class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                     } else if (class28.method286(class309_0, i_4, 92498700)) {
                        class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                     }
                  }
               }
            }

            class309_0.method5477(-1464377233);
            if (i_2 != 0) {
               throw new RuntimeException();
            } else {
               class309_0.method5454(1576708941);

               for (i_3 = 0; i_3 < class98.field1257; i_3++) {
                  i_4 = class98.field1258[i_3];
                  if ((class98.field1252[i_4] & 0x1) == 0) {
                     if (i_2 > 0) {
                        --i_2;
                        class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                     } else {
                        i_5 = class309_0.method5455(1, (byte) -114);
                        if (i_5 == 0) {
                           i_2 = class230.method4113(class309_0, -334559436);
                           class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                        } else if (class28.method286(class309_0, i_4, -1245980985)) {
                           class98.field1252[i_4] = (byte)(class98.field1252[i_4] | 0x2);
                        }
                     }
                  }
               }

               class309_0.method5477(1315413793);
               if (i_2 != 0) {
                  throw new RuntimeException();
               } else {
                  class98.field1263 = 0;
                  class98.field1257 = 0;

                  for (i_3 = 1; i_3 < 2048; i_3++) {
                     class98.field1252[i_3] = (byte)(class98.field1252[i_3] >> 1);
                     class75 class75_6 = client.field764[i_3];
                     if (class75_6 != null) {
                        class98.field1268[++class98.field1263 - 1] = i_3;
                     } else {
                        class98.field1258[++class98.field1257 - 1] = i_3;
                     }
                  }

               }
            }
         }
      }
   }

}
