import java.util.Iterator;

public class class274 implements Iterator {

   class184 field3574 = null;
   class276 field3572;
   class184 field3573;

   class274(class276 class276_1) {
      this.field3572 = class276_1;
      this.field3573 = this.field3572.field3579.field2129;
      this.field3574 = null;
   }

   public void remove() {
      if (this.field3574 == null) {
         throw new IllegalStateException();
      } else {
         this.field3574.method3622();
         this.field3574 = null;
      }
   }

   public Object next() {
      class184 class184_1 = this.field3573;
      if (class184_1 == this.field3572.field3579) {
         class184_1 = null;
         this.field3573 = null;
      } else {
         this.field3573 = class184_1.field2129;
      }

      this.field3574 = class184_1;
      return class184_1;
   }

   public boolean hasNext() {
      return this.field3572.field3579 != this.field3573;
   }

}
