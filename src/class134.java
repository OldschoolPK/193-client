public class class134 {

   static int[] field1657 = new int[500];
   static int[] field1651 = new int[500];
   static int[] field1652 = new int[500];
   static int[] field1659 = new int[500];
   class141 field1654 = null;
   int field1655 = -1;
   boolean field1660 = false;
   int[] field1656;
   int[] field1650;
   int[] field1658;
   int[] field1653;

   class134(byte[] bytes_1, class141 class141_2) {
      this.field1654 = class141_2;
      class310 class310_3 = new class310(bytes_1);
      class310 class310_4 = new class310(bytes_1);
      class310_3.field3751 = 2;
      int i_5 = class310_3.method5661((byte) 39);
      int i_6 = -1;
      int i_7 = 0;
      class310_4.field3751 = i_5 + class310_3.field3751;

      int i_8;
      for (i_8 = 0; i_8 < i_5; i_8++) {
         int i_9 = class310_3.method5661((byte) 87);
         if (i_9 > 0) {
            if (this.field1654.field1818[i_8] != 0) {
               for (int i_10 = i_8 - 1; i_10 > i_6; --i_10) {
                  if (this.field1654.field1818[i_10] == 0) {
                     field1657[i_7] = i_10;
                     field1651[i_7] = 0;
                     field1652[i_7] = 0;
                     field1659[i_7] = 0;
                     ++i_7;
                     break;
                  }
               }
            }

            field1657[i_7] = i_8;
            short s_11 = 0;
            if (this.field1654.field1818[i_8] == 3) {
               s_11 = 128;
            }

            if ((i_9 & 0x1) != 0) {
               field1651[i_7] = class310_4.method5515(1103901264);
            } else {
               field1651[i_7] = s_11;
            }

            if ((i_9 & 0x2) != 0) {
               field1652[i_7] = class310_4.method5515(1136202462);
            } else {
               field1652[i_7] = s_11;
            }

            if ((i_9 & 0x4) != 0) {
               field1659[i_7] = class310_4.method5515(-187945178);
            } else {
               field1659[i_7] = s_11;
            }

            i_6 = i_8;
            ++i_7;
            if (this.field1654.field1818[i_8] == 5) {
               this.field1660 = true;
            }
         }
      }

      if (bytes_1.length != class310_4.field3751) {
         throw new RuntimeException();
      } else {
         this.field1655 = i_7;
         this.field1656 = new int[i_7];
         this.field1650 = new int[i_7];
         this.field1658 = new int[i_7];
         this.field1653 = new int[i_7];

         for (i_8 = 0; i_8 < i_7; i_8++) {
            this.field1656[i_8] = field1657[i_8];
            this.field1650[i_8] = field1651[i_8];
            this.field1658[i_8] = field1652[i_8];
            this.field1653[i_8] = field1659[i_8];
         }

      }
   }

}
