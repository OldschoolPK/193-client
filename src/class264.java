public class class264 extends class184 {

   public static class244 field3381;
   public static class244 field3422;
   public int field3387;
   public static boolean field3399 = false;
   public static class154 field3395 = new class154(4096);
   public static class154 field3415 = new class154(500);
   public static class154 field3409 = new class154(30);
   public static class154 field3385 = new class154(30);
   static class130[] field3386 = new class130[4];
   public String field3394 = "null";
   public int field3418 = 1;
   public int field3396 = 1;
   public int field3397 = 2;
   public boolean field3398 = true;
   public int field3423 = -1;
   int field3400 = -1;
   boolean field3401 = false;
   public boolean field3402 = false;
   public int field3411 = -1;
   public int field3404 = 16;
   int field3405 = 0;
   int field3406 = 0;
   public String[] field3407 = new String[5];
   public int field3414 = -1;
   public int field3382 = -1;
   boolean field3410 = false;
   public boolean field3425 = true;
   int field3412 = 128;
   int field3413 = 128;
   int field3384 = 128;
   int field3383 = 0;
   int field3416 = 0;
   int field3379 = 0;
   public boolean field3380 = false;
   boolean field3419 = false;
   public int field3417 = -1;
   int field3427 = -1;
   int field3431 = -1;
   public int field3424 = -1;
   public int field3408 = 0;
   public int field3426 = 0;
   public int field3388 = 0;
   public boolean field3429 = true;
   int[] field3389;
   int[] field3420;
   short[] field3391;
   short[] field3403;
   short[] field3393;
   short[] field3392;
   public int[] field3428;
   public int[] field3421;
   class326 field3430;

   public final class264 method4599(int i_1) {
      int i_2 = -1;
      if (this.field3427 != -1) {
         i_2 = class222.method3999(this.field3427, -1933237949);
      } else if (this.field3431 != -1) {
         i_2 = class221.field2541[this.field3431];
      }

      int i_3;
      if (i_2 >= 0 && i_2 < this.field3421.length - 1) {
         i_3 = this.field3421[i_2];
      } else {
         i_3 = this.field3421[this.field3421.length - 1];
      }

      return i_3 != -1 ? class34.method383(i_3, 851834689) : null;
   }

   void method4582(class310 class310_1, int i_2, int i_3) {
      int i_4;
      int i_5;
      if (i_2 == 1) {
         i_4 = class310_1.method5661((byte) 6);
         if (i_4 > 0) {
            if (this.field3420 != null && !field3399) {
               class310_1.field3751 += 3 * i_4;
            } else {
               this.field3389 = new int[i_4];
               this.field3420 = new int[i_4];

               for (i_5 = 0; i_5 < i_4; i_5++) {
                  this.field3420[i_5] = class310_1.method5729(-1765330283);
                  this.field3389[i_5] = class310_1.method5661((byte) -5);
               }
            }
         }
      } else if (i_2 == 2) {
         this.field3394 = class310_1.method5589(1184233473);
      } else if (i_2 == 5) {
         i_4 = class310_1.method5661((byte) 28);
         if (i_4 > 0) {
            if (this.field3420 != null && !field3399) {
               class310_1.field3751 += 2 * i_4;
            } else {
               this.field3389 = null;
               this.field3420 = new int[i_4];

               for (i_5 = 0; i_5 < i_4; i_5++) {
                  this.field3420[i_5] = class310_1.method5729(-1978679524);
               }
            }
         }
      } else if (i_2 == 14) {
         this.field3418 = class310_1.method5661((byte) -80);
      } else if (i_2 == 15) {
         this.field3396 = class310_1.method5661((byte) 51);
      } else if (i_2 == 17) {
         this.field3397 = 0;
         this.field3398 = false;
      } else if (i_2 == 18) {
         this.field3398 = false;
      } else if (i_2 == 19) {
         this.field3423 = class310_1.method5661((byte) -65);
      } else if (i_2 == 21) {
         this.field3400 = 0;
      } else if (i_2 == 22) {
         this.field3401 = true;
      } else if (i_2 == 23) {
         this.field3402 = true;
      } else if (i_2 == 24) {
         this.field3411 = class310_1.method5729(-1719379981);
         if (this.field3411 == 65535) {
            this.field3411 = -1;
         }
      } else if (i_2 == 27) {
         this.field3397 = 1;
      } else if (i_2 == 28) {
         this.field3404 = class310_1.method5661((byte) -23);
      } else if (i_2 == 29) {
         this.field3405 = class310_1.method5535((byte) 0);
      } else if (i_2 == 39) {
         this.field3406 = class310_1.method5535((byte) 0);
      } else if (i_2 >= 30 && i_2 < 35) {
         this.field3407[i_2 - 30] = class310_1.method5589(-599414796);
         if (this.field3407[i_2 - 30].equalsIgnoreCase("Hidden")) {
            this.field3407[i_2 - 30] = null;
         }
      } else if (i_2 == 40) {
         i_4 = class310_1.method5661((byte) 21);
         this.field3391 = new short[i_4];
         this.field3403 = new short[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3391[i_5] = (short)class310_1.method5729(-1501722354);
            this.field3403[i_5] = (short)class310_1.method5729(-1676156909);
         }
      } else if (i_2 == 41) {
         i_4 = class310_1.method5661((byte) -4);
         this.field3393 = new short[i_4];
         this.field3392 = new short[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3393[i_5] = (short)class310_1.method5729(-1234453772);
            this.field3392[i_5] = (short)class310_1.method5729(821224620);
         }
      } else if (i_2 == 62) {
         this.field3410 = true;
      } else if (i_2 == 64) {
         this.field3425 = false;
      } else if (i_2 == 65) {
         this.field3412 = class310_1.method5729(-1371199811);
      } else if (i_2 == 66) {
         this.field3413 = class310_1.method5729(-1550915827);
      } else if (i_2 == 67) {
         this.field3384 = class310_1.method5729(-2058173742);
      } else if (i_2 == 68) {
         this.field3382 = class310_1.method5729(-615573474);
      } else if (i_2 == 69) {
         class310_1.method5661((byte) 78);
      } else if (i_2 == 70) {
         this.field3383 = class310_1.method5505((short) 13578);
      } else if (i_2 == 71) {
         this.field3416 = class310_1.method5505((short) 9015);
      } else if (i_2 == 72) {
         this.field3379 = class310_1.method5505((short) 3478);
      } else if (i_2 == 73) {
         this.field3380 = true;
      } else if (i_2 == 74) {
         this.field3419 = true;
      } else if (i_2 == 75) {
         this.field3417 = class310_1.method5661((byte) 43);
      } else if (i_2 != 77 && i_2 != 92) {
         if (i_2 == 78) {
            this.field3424 = class310_1.method5729(-484553499);
            this.field3408 = class310_1.method5661((byte) -29);
         } else if (i_2 == 79) {
            this.field3426 = class310_1.method5729(-59065227);
            this.field3388 = class310_1.method5729(-1296646542);
            this.field3408 = class310_1.method5661((byte) -67);
            i_4 = class310_1.method5661((byte) 16);
            this.field3428 = new int[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3428[i_5] = class310_1.method5729(1925487692);
            }
         } else if (i_2 == 81) {
            this.field3400 = class310_1.method5661((byte) 15) * 256;
         } else if (i_2 == 82) {
            this.field3414 = class310_1.method5729(2122361889);
         } else if (i_2 == 89) {
            this.field3429 = false;
         } else if (i_2 == 249) {
            this.field3430 = class180.method3576(class310_1, this.field3430, -1038873122);
         }
      } else {
         this.field3427 = class310_1.method5729(1458755750);
         if (this.field3427 == 65535) {
            this.field3427 = -1;
         }

         this.field3431 = class310_1.method5729(-655093377);
         if (this.field3431 == 65535) {
            this.field3431 = -1;
         }

         i_4 = -1;
         if (i_2 == 92) {
            i_4 = class310_1.method5729(38440006);
            if (i_4 == 65535) {
               i_4 = -1;
            }
         }

         i_5 = class310_1.method5661((byte) 11);
         this.field3421 = new int[i_5 + 2];

         for (int i_6 = 0; i_6 <= i_5; i_6++) {
            this.field3421[i_6] = class310_1.method5729(1001918348);
            if (this.field3421[i_6] == 65535) {
               this.field3421[i_6] = -1;
            }
         }

         this.field3421[i_5 + 1] = i_4;
      }

   }

   final class130 method4584(int i_1, int i_2, int i_3) {
      class130 class130_4 = null;
      boolean bool_5;
      int i_6;
      int i_8;
      if (this.field3389 == null) {
         if (i_1 != 10) {
            return null;
         }

         if (this.field3420 == null) {
            return null;
         }

         bool_5 = this.field3410;
         if (i_1 == 2 && i_2 > 3) {
            bool_5 = !bool_5;
         }

         i_6 = this.field3420.length;

         for (int i_7 = 0; i_7 < i_6; i_7++) {
            i_8 = this.field3420[i_7];
            if (bool_5) {
               i_8 += 65536;
            }

            class130_4 = (class130) field3415.method3376((long)i_8);
            if (class130_4 == null) {
               class130_4 = class130.method2824(field3381, i_8 & 0xffff, 0);
               if (class130_4 == null) {
                  return null;
               }

               if (bool_5) {
                  class130_4.method2839();
               }

               field3415.method3374(class130_4, (long)i_8);
            }

            if (i_6 > 1) {
               field3386[i_7] = class130_4;
            }
         }

         if (i_6 > 1) {
            class130_4 = new class130(field3386, i_6);
         }
      } else {
         int i_10 = -1;

         for (i_6 = 0; i_6 < this.field3389.length; i_6++) {
            if (this.field3389[i_6] == i_1) {
               i_10 = i_6;
               break;
            }
         }

         if (i_10 == -1) {
            return null;
         }

         i_6 = this.field3420[i_10];
         boolean bool_11 = this.field3410 ^ i_2 > 3;
         if (bool_11) {
            i_6 += 65536;
         }

         class130_4 = (class130) field3415.method3376((long)i_6);
         if (class130_4 == null) {
            class130_4 = class130.method2824(field3381, i_6 & 0xffff, 0);
            if (class130_4 == null) {
               return null;
            }

            if (bool_11) {
               class130_4.method2839();
            }

            field3415.method3374(class130_4, (long)i_6);
         }
      }

      if (this.field3412 == 128 && this.field3413 == 128 && this.field3384 == 128) {
         bool_5 = false;
      } else {
         bool_5 = true;
      }

      boolean bool_12;
      if (this.field3383 == 0 && this.field3416 == 0 && this.field3379 == 0) {
         bool_12 = false;
      } else {
         bool_12 = true;
      }

      class130 class130_9 = new class130(class130_4, i_2 == 0 && !bool_5 && !bool_12, this.field3391 == null, this.field3393 == null, true);
      if (i_1 == 4 && i_2 > 3) {
         class130_9.method2843(256);
         class130_9.method2836(45, 0, -45);
      }

      i_2 &= 0x3;
      if (i_2 == 1) {
         class130_9.method2832();
      } else if (i_2 == 2) {
         class130_9.method2868();
      } else if (i_2 == 3) {
         class130_9.method2835();
      }

      if (this.field3391 != null) {
         for (i_8 = 0; i_8 < this.field3391.length; i_8++) {
            class130_9.method2837(this.field3391[i_8], this.field3403[i_8]);
         }
      }

      if (this.field3393 != null) {
         for (i_8 = 0; i_8 < this.field3393.length; i_8++) {
            class130_9.method2833(this.field3393[i_8], this.field3392[i_8]);
         }
      }

      if (bool_5) {
         class130_9.method2840(this.field3412, this.field3413, this.field3384);
      }

      if (bool_12) {
         class130_9.method2836(this.field3383, this.field3416, this.field3379);
      }

      return class130_9;
   }

   void method4585(int i_1) {
      if (this.field3423 == -1) {
         this.field3423 = 0;
         if (this.field3420 != null && (this.field3389 == null || this.field3389[0] == 10)) {
            this.field3423 = 1;
         }

         for (int i_2 = 0; i_2 < 5; i_2++) {
            if (this.field3407[i_2] != null) {
               this.field3423 = 1;
            }
         }
      }

      if (this.field3417 == -1) {
         this.field3417 = this.field3397 != 0 ? 1 : 0;
      }

   }

   void method4611(class310 class310_1, byte b_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -16);
         if (i_3 == 0) {
            return;
         }

         this.method4582(class310_1, i_3, 1322137078);
      }
   }

   public final class136 method4587(int i_1, int i_2, int[][] ints_3, int i_4, int i_5, int i_6, class269 class269_7, int i_8, int i_9) {
      long long_10;
      if (this.field3389 == null) {
         long_10 = (long)(i_2 + (this.field3387 << 10));
      } else {
         long_10 = (long)(i_2 + (i_1 << 3) + (this.field3387 << 10));
      }

      class136 class136_12 = (class136) field3385.method3376(long_10);
      if (class136_12 == null) {
         class130 class130_13 = this.method4584(i_1, i_2, 1246121040);
         if (class130_13 == null) {
            return null;
         }

         class136_12 = class130_13.method2876(this.field3405 + 64, this.field3406 * 25 + 768, -50, -10, -50);
         field3385.method3374(class136_12, long_10);
      }

      if (class269_7 == null && this.field3400 == -1) {
         return class136_12;
      } else {
         if (class269_7 != null) {
            class136_12 = class269_7.method4759(class136_12, i_8, i_2, -2055004440);
         } else {
            class136_12 = class136_12.method2921(true);
         }

         if (this.field3400 >= 0) {
            class136_12 = class136_12.method2977(ints_3, i_4, i_5, i_6, false, this.field3400);
         }

         return class136_12;
      }
   }

   public final boolean method4616(int i_1) {
      if (this.field3420 == null) {
         return true;
      } else {
         boolean bool_2 = true;

         for (int i_3 = 0; i_3 < this.field3420.length; i_3++) {
            bool_2 &= field3381.method4162(this.field3420[i_3] & 0xffff, 0, 1736464380);
         }

         return bool_2;
      }
   }

   public final boolean method4581(int i_1, int i_2) {
      if (this.field3389 != null) {
         for (int i_5 = 0; i_5 < this.field3389.length; i_5++) {
            if (this.field3389[i_5] == i_1) {
               return field3381.method4162(this.field3420[i_5] & 0xffff, 0, 228574066);
            }
         }

         return true;
      } else if (this.field3420 == null) {
         return true;
      } else if (i_1 != 10) {
         return true;
      } else {
         boolean bool_3 = true;

         for (int i_4 = 0; i_4 < this.field3420.length; i_4++) {
            bool_3 &= field3381.method4162(this.field3420[i_4] & 0xffff, 0, -1155060591);
         }

         return bool_3;
      }
   }

   public final class136 method4586(int i_1, int i_2, int[][] ints_3, int i_4, int i_5, int i_6, int i_7) {
      long long_8;
      if (this.field3389 == null) {
         long_8 = (long)(i_2 + (this.field3387 << 10));
      } else {
         long_8 = (long)(i_2 + (i_1 << 3) + (this.field3387 << 10));
      }

      class136 class136_10 = (class136) field3385.method3376(long_8);
      if (class136_10 == null) {
         class130 class130_11 = this.method4584(i_1, i_2, 530489192);
         if (class130_11 == null) {
            return null;
         }

         class136_10 = class130_11.method2876(this.field3405 + 64, this.field3406 * 25 + 768, -50, -10, -50);
         field3385.method3374(class136_10, long_8);
      }

      if (this.field3400 >= 0) {
         class136_10 = class136_10.method2977(ints_3, i_4, i_5, i_6, true, this.field3400);
      }

      return class136_10;
   }

   public boolean method4591(int i_1) {
      if (this.field3421 == null) {
         return this.field3424 != -1 || this.field3428 != null;
      } else {
         for (int i_2 = 0; i_2 < this.field3421.length; i_2++) {
            if (this.field3421[i_2] != -1) {
               class264 class264_3 = class34.method383(this.field3421[i_2], 125545193);
               if (class264_3.field3424 != -1 || class264_3.field3428 != null) {
                  return true;
               }
            }
         }

         return false;
      }
   }

   public final class144 method4623(int i_1, int i_2, int[][] ints_3, int i_4, int i_5, int i_6, short s_7) {
      long long_8;
      if (this.field3389 == null) {
         long_8 = (long)(i_2 + (this.field3387 << 10));
      } else {
         long_8 = (long)(i_2 + (i_1 << 3) + (this.field3387 << 10));
      }

      Object obj_10 = (class144) field3409.method3376(long_8);
      if (obj_10 == null) {
         class130 class130_11 = this.method4584(i_1, i_2, 2119637200);
         if (class130_11 == null) {
            return null;
         }

         if (!this.field3401) {
            obj_10 = class130_11.method2876(this.field3405 + 64, this.field3406 * 25 + 768, -50, -10, -50);
         } else {
            class130_11.field1566 = (short)(this.field3405 + 64);
            class130_11.field1591 = (short)(this.field3406 * 25 + 768);
            class130_11.method2841();
            obj_10 = class130_11;
         }

         field3409.method3374((class184) obj_10, long_8);
      }

      if (this.field3401) {
         obj_10 = ((class130) obj_10).method2829();
      }

      if (this.field3400 >= 0) {
         if (obj_10 instanceof class136) {
            obj_10 = ((class136) obj_10).method2977(ints_3, i_4, i_5, i_6, true, this.field3400);
         } else if (obj_10 instanceof class130) {
            obj_10 = ((class130) obj_10).method2830(ints_3, i_4, i_5, i_6, true, this.field3400);
         }
      }

      return (class144) obj_10;
   }

   public int method4589(int i_1, int i_2, byte b_3) {
      class326 class326_5 = this.field3430;
      int i_4;
      if (class326_5 == null) {
         i_4 = i_2;
      } else {
         class188 class188_6 = (class188) class326_5.method5919((long)i_1);
         if (class188_6 == null) {
            i_4 = i_2;
         } else {
            i_4 = class188_6.field2135;
         }
      }

      return i_4;
   }

   public String method4590(int i_1, String string_2, int i_3) {
      class326 class326_5 = this.field3430;
      String string_4;
      if (class326_5 == null) {
         string_4 = string_2;
      } else {
         class185 class185_6 = (class185) class326_5.method5919((long)i_1);
         if (class185_6 == null) {
            string_4 = string_2;
         } else {
            string_4 = (String) class185_6.field2131;
         }
      }

      return string_4;
   }

   static final boolean method4624(char var_0, int i_1) {
      if (Character.isISOControl(var_0)) {
         return false;
      } else if (class296.method5249(var_0, -1404693123)) {
         return true;
      } else {
         char[] arr_2 = class349.field4077;

         int i_3;
         char var_4;
         for (i_3 = 0; i_3 < arr_2.length; i_3++) {
            var_4 = arr_2[i_3];
            if (var_0 == var_4) {
               return true;
            }
         }

         arr_2 = class349.field4079;

         for (i_3 = 0; i_3 < arr_2.length; i_3++) {
            var_4 = arr_2[i_3];
            if (var_0 == var_4) {
               return true;
            }
         }

         return false;
      }
   }

}
