public class class142 {

   public static boolean field1839 = true;
   static int field1870 = 0;
   static int field1872 = 0;
   static class151[] field1854 = new class151[100];
   static boolean field1852 = false;
   static int field1838 = 0;
   static int field1857 = 0;
   static int field1858 = 0;
   public static int field1859 = -1;
   public static int field1860 = -1;
   static boolean field1861 = false;
   static int field1864 = 4;
   static int[] field1865;
   static class149[][] field1834;
   static int field1867;
   static class149[] field1868;
   static class272 field1879;
   static final int[] field1826;
   static final int[] field1871;
   static final int[] field1829;
   static final int[] field1873;
   static final int[] field1866;
   static final int[] field1875;
   static final int[] field1876;
   static boolean[][][][] field1883;
   int field1830 = 0;
   int field1831 = 0;
   class151[] field1832 = new class151[5000];
   int[][] field1881 = new int[][] {{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1}, {1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, {0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1}, {0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1}, {1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0}, {1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1}, {1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1}};
   int[][] field1882 = new int[][] {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}, {12, 8, 4, 0, 13, 9, 5, 1, 14, 10, 6, 2, 15, 11, 7, 3}, {15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0}, {3, 7, 11, 15, 2, 6, 10, 14, 1, 5, 9, 13, 0, 4, 8, 12}};
   int field1825;
   int field1853;
   int field1827;
   class135[][][] field1847;
   int[][][] field1833;
   int[][][] field1828;
   static int field1840;
   static int field1850;
   static int field1851;
   static int field1862;
   static int field1855;
   static boolean[][] field1837;
   static int field1874;
   static int field1848;
   static int field1849;
   static int field1845;
   static int field1846;
   static int field1841;
   static int field1843;
   static int field1842;
   static int field1844;
   static int field1887;
   static int field1888;
   static int field1889;
   static int field1824;
   static int field1885;
   static int field1886;

   static {
      field1865 = new int[field1864];
      field1834 = new class149[field1864][500];
      field1867 = 0;
      field1868 = new class149[500];
      field1879 = new class272();
      field1826 = new int[] {19, 55, 38, 155, 255, 110, 137, 205, 76};
      field1871 = new int[] {160, 192, 80, 96, 0, 144, 80, 48, 160};
      field1829 = new int[] {76, 8, 137, 4, 0, 1, 38, 2, 19};
      field1873 = new int[] {0, 0, 2, 0, 0, 2, 1, 1, 0};
      field1866 = new int[] {2, 0, 0, 2, 0, 0, 0, 4, 4};
      field1875 = new int[] {0, 4, 4, 8, 0, 0, 8, 0, 0};
      field1876 = new int[] {1, 1, 0, 0, 0, 8, 0, 0, 8};
      field1883 = new boolean[8][32][51][51];
   }

   public class142(int i_1, int i_2, int i_3, int[][][] ints_4) {
      this.field1825 = i_1;
      this.field1853 = i_2;
      this.field1827 = i_3;
      this.field1847 = new class135[i_1][i_2][i_3];
      this.field1833 = new int[i_1][i_2 + 1][i_3 + 1];
      this.field1828 = ints_4;
      this.method3248();
   }

   boolean method3263(int i_1, int i_2, int i_3) {
      int i_4 = this.field1833[i_1][i_2][i_3];
      if (i_4 == -field1840) {
         return false;
      } else if (i_4 == field1840) {
         return true;
      } else {
         int i_5 = i_2 << 7;
         int i_6 = i_3 << 7;
         if (this.method3183(i_5 + 1, this.field1828[i_1][i_2][i_3], i_6 + 1) && this.method3183(i_5 + 128 - 1, this.field1828[i_1][i_2 + 1][i_3], i_6 + 1) && this.method3183(i_5 + 128 - 1, this.field1828[i_1][i_2 + 1][i_3 + 1], i_6 + 128 - 1) && this.method3183(i_5 + 1, this.field1828[i_1][i_2][i_3 + 1], i_6 + 128 - 1)) {
            this.field1833[i_1][i_2][i_3] = field1840;
            return true;
         } else {
            this.field1833[i_1][i_2][i_3] = -field1840;
            return false;
         }
      }
   }

   boolean method3144(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, class144 class144_9, int i_10, boolean bool_11, long long_12, int i_14) {
      int i_16;
      for (int i_15 = i_2; i_15 < i_2 + i_4; i_15++) {
         for (i_16 = i_3; i_16 < i_3 + i_5; i_16++) {
            if (i_15 < 0 || i_16 < 0 || i_15 >= this.field1853 || i_16 >= this.field1827) {
               return false;
            }

            class135 class135_17 = this.field1847[i_1][i_15][i_16];
            if (class135_17 != null && class135_17.field1671 >= 5) {
               return false;
            }
         }
      }

      class151 class151_21 = new class151();
      class151_21.field1957 = long_12;
      class151_21.field1958 = i_14;
      class151_21.field1955 = i_1;
      class151_21.field1947 = i_6;
      class151_21.field1945 = i_7;
      class151_21.field1950 = i_8;
      class151_21.field1946 = class144_9;
      class151_21.field1951 = i_10;
      class151_21.field1956 = i_2;
      class151_21.field1960 = i_3;
      class151_21.field1952 = i_2 + i_4 - 1;
      class151_21.field1954 = i_3 + i_5 - 1;

      for (i_16 = i_2; i_16 < i_2 + i_4; i_16++) {
         for (int i_22 = i_3; i_22 < i_3 + i_5; i_22++) {
            int i_18 = 0;
            if (i_16 > i_2) {
               ++i_18;
            }

            if (i_16 < i_2 + i_4 - 1) {
               i_18 += 4;
            }

            if (i_22 > i_3) {
               i_18 += 8;
            }

            if (i_22 < i_3 + i_5 - 1) {
               i_18 += 2;
            }

            for (int i_19 = i_1; i_19 >= 0; --i_19) {
               if (this.field1847[i_19][i_16][i_22] == null) {
                  this.field1847[i_19][i_16][i_22] = new class135(i_19, i_16, i_22);
               }
            }

            class135 class135_23 = this.field1847[i_1][i_16][i_22];
            class135_23.field1662[class135_23.field1671] = class151_21;
            class135_23.field1673[class135_23.field1671] = i_18;
            class135_23.field1674 |= i_18;
            ++class135_23.field1671;
         }
      }

      if (bool_11) {
         this.field1832[this.field1831++] = class151_21;
      }

      return true;
   }

   public void method3168(int i_1, int i_2, int i_3, boolean bool_4) {
      if (!method3170() || bool_4) {
         field1852 = true;
         field1861 = bool_4;
         field1838 = i_1;
         field1857 = i_2;
         field1858 = i_3;
         field1859 = -1;
         field1860 = -1;
      }
   }

   void method3146(class151 class151_1) {
      for (int i_2 = class151_1.field1956; i_2 <= class151_1.field1952; i_2++) {
         for (int i_3 = class151_1.field1960; i_3 <= class151_1.field1954; i_3++) {
            class135 class135_4 = this.field1847[class151_1.field1955][i_2][i_3];
            if (class135_4 != null) {
               int i_5;
               for (i_5 = 0; i_5 < class135_4.field1671; i_5++) {
                  if (class135_4.field1662[i_5] == class151_1) {
                     --class135_4.field1671;

                     for (int i_6 = i_5; i_6 < class135_4.field1671; i_6++) {
                        class135_4.field1662[i_6] = class135_4.field1662[i_6 + 1];
                        class135_4.field1673[i_6] = class135_4.field1673[i_6 + 1];
                     }

                     class135_4.field1662[class135_4.field1671] = null;
                     break;
                  }
               }

               class135_4.field1674 = 0;

               for (i_5 = 0; i_5 < class135_4.field1671; i_5++) {
                  class135_4.field1674 |= class135_4.field1673[i_5];
               }
            }
         }
      }

   }

   boolean method3183(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < field1867; i_4++) {
         class149 class149_5 = field1868[i_4];
         int i_6;
         int i_7;
         int i_8;
         int i_9;
         int i_10;
         if (class149_5.field1921 == 1) {
            i_6 = class149_5.field1915 - i_1;
            if (i_6 > 0) {
               i_7 = (i_6 * class149_5.field1924 >> 8) + class149_5.field1912;
               i_8 = (i_6 * class149_5.field1929 >> 8) + class149_5.field1918;
               i_9 = (i_6 * class149_5.field1926 >> 8) + class149_5.field1919;
               i_10 = (i_6 * class149_5.field1927 >> 8) + class149_5.field1920;
               if (i_3 >= i_7 && i_3 <= i_8 && i_2 >= i_9 && i_2 <= i_10) {
                  return true;
               }
            }
         } else if (class149_5.field1921 == 2) {
            i_6 = i_1 - class149_5.field1915;
            if (i_6 > 0) {
               i_7 = (i_6 * class149_5.field1924 >> 8) + class149_5.field1912;
               i_8 = (i_6 * class149_5.field1929 >> 8) + class149_5.field1918;
               i_9 = (i_6 * class149_5.field1926 >> 8) + class149_5.field1919;
               i_10 = (i_6 * class149_5.field1927 >> 8) + class149_5.field1920;
               if (i_3 >= i_7 && i_3 <= i_8 && i_2 >= i_9 && i_2 <= i_10) {
                  return true;
               }
            }
         } else if (class149_5.field1921 == 3) {
            i_6 = class149_5.field1912 - i_3;
            if (i_6 > 0) {
               i_7 = (i_6 * class149_5.field1925 >> 8) + class149_5.field1915;
               i_8 = (i_6 * class149_5.field1923 >> 8) + class149_5.field1916;
               i_9 = (i_6 * class149_5.field1926 >> 8) + class149_5.field1919;
               i_10 = (i_6 * class149_5.field1927 >> 8) + class149_5.field1920;
               if (i_1 >= i_7 && i_1 <= i_8 && i_2 >= i_9 && i_2 <= i_10) {
                  return true;
               }
            }
         } else if (class149_5.field1921 == 4) {
            i_6 = i_3 - class149_5.field1912;
            if (i_6 > 0) {
               i_7 = (i_6 * class149_5.field1925 >> 8) + class149_5.field1915;
               i_8 = (i_6 * class149_5.field1923 >> 8) + class149_5.field1916;
               i_9 = (i_6 * class149_5.field1926 >> 8) + class149_5.field1919;
               i_10 = (i_6 * class149_5.field1927 >> 8) + class149_5.field1920;
               if (i_1 >= i_7 && i_1 <= i_8 && i_2 >= i_9 && i_2 <= i_10) {
                  return true;
               }
            }
         } else if (class149_5.field1921 == 5) {
            i_6 = i_2 - class149_5.field1919;
            if (i_6 > 0) {
               i_7 = (i_6 * class149_5.field1925 >> 8) + class149_5.field1915;
               i_8 = (i_6 * class149_5.field1923 >> 8) + class149_5.field1916;
               i_9 = (i_6 * class149_5.field1924 >> 8) + class149_5.field1912;
               i_10 = (i_6 * class149_5.field1929 >> 8) + class149_5.field1918;
               if (i_1 >= i_7 && i_1 <= i_8 && i_3 >= i_9 && i_3 <= i_10) {
                  return true;
               }
            }
         }
      }

      return false;
   }

   void method3164(class130 class130_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      boolean bool_7 = true;
      int i_8 = i_3;
      int i_9 = i_3 + i_5;
      int i_10 = i_4 - 1;
      int i_11 = i_4 + i_6;

      for (int i_12 = i_2; i_12 <= i_2 + 1; i_12++) {
         if (i_12 != this.field1825) {
            for (int i_13 = i_8; i_13 <= i_9; i_13++) {
               if (i_13 >= 0 && i_13 < this.field1853) {
                  for (int i_14 = i_10; i_14 <= i_11; i_14++) {
                     if (i_14 >= 0 && i_14 < this.field1827 && (!bool_7 || i_13 >= i_9 || i_14 >= i_11 || i_14 < i_4 && i_3 != i_13)) {
                        class135 class135_15 = this.field1847[i_12][i_13][i_14];
                        if (class135_15 != null) {
                           int i_16 = (this.field1828[i_12][i_13 + 1][i_14] + this.field1828[i_12][i_13 + 1][i_14 + 1] + this.field1828[i_12][i_13][i_14] + this.field1828[i_12][i_13][i_14 + 1]) / 4 - (this.field1828[i_2][i_3 + 1][i_4] + this.field1828[i_2][i_3][i_4] + this.field1828[i_2][i_3 + 1][i_4 + 1] + this.field1828[i_2][i_3][i_4 + 1]) / 4;
                           class145 class145_17 = class135_15.field1667;
                           if (class145_17 != null) {
                              class130 class130_18;
                              if (class145_17.field1899 instanceof class130) {
                                 class130_18 = (class130) class145_17.field1899;
                                 class130.method2844(class130_1, class130_18, (1 - i_5) * 64 + (i_13 - i_3) * 128, i_16, (i_14 - i_4) * 128 + (1 - i_6) * 64, bool_7);
                              }

                              if (class145_17.field1897 instanceof class130) {
                                 class130_18 = (class130) class145_17.field1897;
                                 class130.method2844(class130_1, class130_18, (1 - i_5) * 64 + (i_13 - i_3) * 128, i_16, (i_14 - i_4) * 128 + (1 - i_6) * 64, bool_7);
                              }
                           }

                           for (int i_23 = 0; i_23 < class135_15.field1671; i_23++) {
                              class151 class151_19 = class135_15.field1662[i_23];
                              if (class151_19 != null && class151_19.field1946 instanceof class130) {
                                 class130 class130_20 = (class130) class151_19.field1946;
                                 int i_21 = class151_19.field1952 - class151_19.field1956 + 1;
                                 int i_22 = class151_19.field1954 - class151_19.field1960 + 1;
                                 class130.method2844(class130_1, class130_20, (i_21 - i_5) * 64 + (class151_19.field1956 - i_3) * 128, i_16, (class151_19.field1960 - i_4) * 128 + (i_22 - i_6) * 64, bool_7);
                              }
                           }
                        }
                     }
                  }
               }
            }

            --i_8;
            bool_7 = false;
         }
      }

   }

   boolean method3180(int i_1, int i_2, int i_3, int i_4) {
      if (!this.method3263(i_1, i_2, i_3)) {
         return false;
      } else {
         int i_5 = i_2 << 7;
         int i_6 = i_3 << 7;
         int i_7 = this.field1828[i_1][i_2][i_3] - 1;
         int i_8 = i_7 - 120;
         int i_9 = i_7 - 230;
         int i_10 = i_7 - 238;
         if (i_4 < 16) {
            if (i_4 == 1) {
               if (i_5 > field1874) {
                  if (!this.method3183(i_5, i_7, i_6)) {
                     return false;
                  }

                  if (!this.method3183(i_5, i_7, i_6 + 128)) {
                     return false;
                  }
               }

               if (i_1 > 0) {
                  if (!this.method3183(i_5, i_8, i_6)) {
                     return false;
                  }

                  if (!this.method3183(i_5, i_8, i_6 + 128)) {
                     return false;
                  }
               }

               if (!this.method3183(i_5, i_9, i_6)) {
                  return false;
               }

               if (!this.method3183(i_5, i_9, i_6 + 128)) {
                  return false;
               }

               return true;
            }

            if (i_4 == 2) {
               if (i_6 < field1849) {
                  if (!this.method3183(i_5, i_7, i_6 + 128)) {
                     return false;
                  }

                  if (!this.method3183(i_5 + 128, i_7, i_6 + 128)) {
                     return false;
                  }
               }

               if (i_1 > 0) {
                  if (!this.method3183(i_5, i_8, i_6 + 128)) {
                     return false;
                  }

                  if (!this.method3183(i_5 + 128, i_8, i_6 + 128)) {
                     return false;
                  }
               }

               if (!this.method3183(i_5, i_9, i_6 + 128)) {
                  return false;
               }

               if (!this.method3183(i_5 + 128, i_9, i_6 + 128)) {
                  return false;
               }

               return true;
            }

            if (i_4 == 4) {
               if (i_5 < field1874) {
                  if (!this.method3183(i_5 + 128, i_7, i_6)) {
                     return false;
                  }

                  if (!this.method3183(i_5 + 128, i_7, i_6 + 128)) {
                     return false;
                  }
               }

               if (i_1 > 0) {
                  if (!this.method3183(i_5 + 128, i_8, i_6)) {
                     return false;
                  }

                  if (!this.method3183(i_5 + 128, i_8, i_6 + 128)) {
                     return false;
                  }
               }

               if (!this.method3183(i_5 + 128, i_9, i_6)) {
                  return false;
               }

               if (!this.method3183(i_5 + 128, i_9, i_6 + 128)) {
                  return false;
               }

               return true;
            }

            if (i_4 == 8) {
               if (i_6 > field1849) {
                  if (!this.method3183(i_5, i_7, i_6)) {
                     return false;
                  }

                  if (!this.method3183(i_5 + 128, i_7, i_6)) {
                     return false;
                  }
               }

               if (i_1 > 0) {
                  if (!this.method3183(i_5, i_8, i_6)) {
                     return false;
                  }

                  if (!this.method3183(i_5 + 128, i_8, i_6)) {
                     return false;
                  }
               }

               if (!this.method3183(i_5, i_9, i_6)) {
                  return false;
               }

               if (!this.method3183(i_5 + 128, i_9, i_6)) {
                  return false;
               }

               return true;
            }
         }

         return !this.method3183(i_5 + 64, i_10, i_6 + 64) ? false : (i_4 == 16 ? this.method3183(i_5, i_9, i_6 + 128) : (i_4 == 32 ? this.method3183(i_5 + 128, i_9, i_6 + 128) : (i_4 == 64 ? this.method3183(i_5 + 128, i_9, i_6) : (i_4 == 128 ? this.method3183(i_5, i_9, i_6) : true))));
      }
   }

   void method3174(class140 class140_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8) {
      int i_9;
      int i_10 = i_9 = (i_7 << 7) - field1874;
      int i_11;
      int i_12 = i_11 = (i_8 << 7) - field1849;
      int i_13;
      int i_14 = i_13 = i_10 + 128;
      int i_15;
      int i_16 = i_15 = i_12 + 128;
      int i_17 = this.field1828[i_2][i_7][i_8] - field1848;
      int i_18 = this.field1828[i_2][i_7 + 1][i_8] - field1848;
      int i_19 = this.field1828[i_2][i_7 + 1][i_8 + 1] - field1848;
      int i_20 = this.field1828[i_2][i_7][i_8 + 1] - field1848;
      int i_21 = i_10 * i_6 + i_5 * i_12 >> 16;
      i_12 = i_12 * i_6 - i_5 * i_10 >> 16;
      i_10 = i_21;
      i_21 = i_17 * i_4 - i_3 * i_12 >> 16;
      i_12 = i_3 * i_17 + i_12 * i_4 >> 16;
      i_17 = i_21;
      if (i_12 >= 50) {
         i_21 = i_14 * i_6 + i_5 * i_11 >> 16;
         i_11 = i_11 * i_6 - i_5 * i_14 >> 16;
         i_14 = i_21;
         i_21 = i_18 * i_4 - i_3 * i_11 >> 16;
         i_11 = i_3 * i_18 + i_11 * i_4 >> 16;
         i_18 = i_21;
         if (i_11 >= 50) {
            i_21 = i_13 * i_6 + i_5 * i_16 >> 16;
            i_16 = i_16 * i_6 - i_5 * i_13 >> 16;
            i_13 = i_21;
            i_21 = i_19 * i_4 - i_3 * i_16 >> 16;
            i_16 = i_3 * i_19 + i_16 * i_4 >> 16;
            i_19 = i_21;
            if (i_16 >= 50) {
               i_21 = i_9 * i_6 + i_5 * i_15 >> 16;
               i_15 = i_15 * i_6 - i_5 * i_9 >> 16;
               i_9 = i_21;
               i_21 = i_20 * i_4 - i_3 * i_15 >> 16;
               i_15 = i_3 * i_20 + i_15 * i_4 >> 16;
               if (i_15 >= 50) {
                  int i_22 = i_10 * class139.field1796 / i_12 + class139.field1785;
                  int i_23 = i_17 * class139.field1796 / i_12 + class139.field1792;
                  int i_24 = i_14 * class139.field1796 / i_11 + class139.field1785;
                  int i_25 = i_18 * class139.field1796 / i_11 + class139.field1792;
                  int i_26 = i_13 * class139.field1796 / i_16 + class139.field1785;
                  int i_27 = i_19 * class139.field1796 / i_16 + class139.field1792;
                  int i_28 = i_9 * class139.field1796 / i_15 + class139.field1785;
                  int i_29 = i_21 * class139.field1796 / i_15 + class139.field1792;
                  class139.field1783 = 0;
                  int i_30;
                  if ((i_26 - i_28) * (i_25 - i_29) - (i_27 - i_29) * (i_24 - i_28) > 0) {
                     class139.field1791 = false;
                     if (i_26 < 0 || i_28 < 0 || i_24 < 0 || i_26 > class139.field1793 || i_28 > class139.field1793 || i_24 > class139.field1793) {
                        class139.field1791 = true;
                     }

                     if (field1852 && method3285(field1857, field1858, i_27, i_29, i_25, i_26, i_28, i_24)) {
                        field1859 = i_7;
                        field1860 = i_8;
                     }

                     if (class140_1.field1810 == -1) {
                        if (class140_1.field1806 != 12345678) {
                           class139.method3051(i_27, i_29, i_25, i_26, i_28, i_24, class140_1.field1806, class140_1.field1809, class140_1.field1807);
                        }
                     } else if (!field1839) {
                        if (class140_1.field1808) {
                           class139.method3057(i_27, i_29, i_25, i_26, i_28, i_24, class140_1.field1806, class140_1.field1809, class140_1.field1807, i_10, i_14, i_9, i_17, i_18, i_21, i_12, i_11, i_15, class140_1.field1810);
                        } else {
                           class139.method3057(i_27, i_29, i_25, i_26, i_28, i_24, class140_1.field1806, class140_1.field1809, class140_1.field1807, i_13, i_9, i_14, i_19, i_21, i_18, i_16, i_15, i_11, class140_1.field1810);
                        }
                     } else {
                        i_30 = class139.field1801.vmethod3330(class140_1.field1810, -1941996399);
                        class139.method3051(i_27, i_29, i_25, i_26, i_28, i_24, method3176(i_30, class140_1.field1806), method3176(i_30, class140_1.field1809), method3176(i_30, class140_1.field1807));
                     }
                  }

                  if ((i_22 - i_24) * (i_29 - i_25) - (i_23 - i_25) * (i_28 - i_24) > 0) {
                     class139.field1791 = false;
                     if (i_22 < 0 || i_24 < 0 || i_28 < 0 || i_22 > class139.field1793 || i_24 > class139.field1793 || i_28 > class139.field1793) {
                        class139.field1791 = true;
                     }

                     if (field1852 && method3285(field1857, field1858, i_23, i_25, i_29, i_22, i_24, i_28)) {
                        field1859 = i_7;
                        field1860 = i_8;
                     }

                     if (class140_1.field1810 == -1) {
                        if (class140_1.field1811 != 12345678) {
                           class139.method3051(i_23, i_25, i_29, i_22, i_24, i_28, class140_1.field1811, class140_1.field1807, class140_1.field1809);
                        }
                     } else if (!field1839) {
                        class139.method3057(i_23, i_25, i_29, i_22, i_24, i_28, class140_1.field1811, class140_1.field1807, class140_1.field1809, i_10, i_14, i_9, i_17, i_18, i_21, i_12, i_11, i_15, class140_1.field1810);
                     } else {
                        i_30 = class139.field1801.vmethod3330(class140_1.field1810, -1928906477);
                        class139.method3051(i_23, i_25, i_29, i_22, i_24, i_28, method3176(i_30, class140_1.field1811), method3176(i_30, class140_1.field1807), method3176(i_30, class140_1.field1809));
                     }
                  }

               }
            }
         }
      }
   }

   void method3252(class131 class131_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      int i_8 = class131_1.field1623.length;

      int i_9;
      int i_10;
      int i_11;
      int i_12;
      int i_13;
      for (i_9 = 0; i_9 < i_8; i_9++) {
         i_10 = class131_1.field1623[i_9] - field1874;
         i_11 = class131_1.field1609[i_9] - field1848;
         i_12 = class131_1.field1605[i_9] - field1849;
         i_13 = i_12 * i_4 + i_5 * i_10 >> 16;
         i_12 = i_5 * i_12 - i_10 * i_4 >> 16;
         i_10 = i_13;
         i_13 = i_3 * i_11 - i_12 * i_2 >> 16;
         i_12 = i_11 * i_2 + i_3 * i_12 >> 16;
         if (i_12 < 50) {
            return;
         }

         if (class131_1.field1604 != null) {
            class131.field1610[i_9] = i_10;
            class131.field1621[i_9] = i_13;
            class131.field1622[i_9] = i_12;
         }

         class131.field1618[i_9] = i_10 * class139.field1796 / i_12 + class139.field1785;
         class131.field1619[i_9] = i_13 * class139.field1796 / i_12 + class139.field1792;
      }

      class139.field1783 = 0;
      i_8 = class131_1.field1607.length;

      for (i_9 = 0; i_9 < i_8; i_9++) {
         i_10 = class131_1.field1607[i_9];
         i_11 = class131_1.field1612[i_9];
         i_12 = class131_1.field1611[i_9];
         i_13 = class131.field1618[i_10];
         int i_14 = class131.field1618[i_11];
         int i_15 = class131.field1618[i_12];
         int i_16 = class131.field1619[i_10];
         int i_17 = class131.field1619[i_11];
         int i_18 = class131.field1619[i_12];
         if ((i_13 - i_14) * (i_18 - i_17) - (i_16 - i_17) * (i_15 - i_14) > 0) {
            class139.field1791 = false;
            if (i_13 < 0 || i_14 < 0 || i_15 < 0 || i_13 > class139.field1793 || i_14 > class139.field1793 || i_15 > class139.field1793) {
               class139.field1791 = true;
            }

            if (field1852 && method3285(field1857, field1858, i_16, i_17, i_18, i_13, i_14, i_15)) {
               field1859 = i_6;
               field1860 = i_7;
            }

            if (class131_1.field1604 != null && class131_1.field1604[i_9] != -1) {
               if (!field1839) {
                  if (class131_1.field1613) {
                     class139.method3057(i_16, i_17, i_18, i_13, i_14, i_15, class131_1.field1606[i_9], class131_1.field1624[i_9], class131_1.field1608[i_9], class131.field1610[0], class131.field1610[1], class131.field1610[3], class131.field1621[0], class131.field1621[1], class131.field1621[3], class131.field1622[0], class131.field1622[1], class131.field1622[3], class131_1.field1604[i_9]);
                  } else {
                     class139.method3057(i_16, i_17, i_18, i_13, i_14, i_15, class131_1.field1606[i_9], class131_1.field1624[i_9], class131_1.field1608[i_9], class131.field1610[i_10], class131.field1610[i_11], class131.field1610[i_12], class131.field1621[i_10], class131.field1621[i_11], class131.field1621[i_12], class131.field1622[i_10], class131.field1622[i_11], class131.field1622[i_12], class131_1.field1604[i_9]);
                  }
               } else {
                  int i_19 = class139.field1801.vmethod3330(class131_1.field1604[i_9], -1984277269);
                  class139.method3051(i_16, i_17, i_18, i_13, i_14, i_15, method3176(i_19, class131_1.field1606[i_9]), method3176(i_19, class131_1.field1624[i_9]), method3176(i_19, class131_1.field1608[i_9]));
               }
            } else if (class131_1.field1606[i_9] != 12345678) {
               class139.method3051(i_16, i_17, i_18, i_13, i_14, i_15, class131_1.field1606[i_9], class131_1.field1624[i_9], class131_1.field1608[i_9]);
            }
         }
      }

   }

   void method3163(class130 class130_1, int i_2, int i_3, int i_4) {
      class135 class135_5;
      class130 class130_6;
      if (i_3 < this.field1853) {
         class135_5 = this.field1847[i_2][i_3 + 1][i_4];
         if (class135_5 != null && class135_5.field1669 != null && class135_5.field1669.field1628 instanceof class130) {
            class130_6 = (class130) class135_5.field1669.field1628;
            class130.method2844(class130_1, class130_6, 128, 0, 0, true);
         }
      }

      if (i_4 < this.field1853) {
         class135_5 = this.field1847[i_2][i_3][i_4 + 1];
         if (class135_5 != null && class135_5.field1669 != null && class135_5.field1669.field1628 instanceof class130) {
            class130_6 = (class130) class135_5.field1669.field1628;
            class130.method2844(class130_1, class130_6, 0, 0, 128, true);
         }
      }

      if (i_3 < this.field1853 && i_4 < this.field1827) {
         class135_5 = this.field1847[i_2][i_3 + 1][i_4 + 1];
         if (class135_5 != null && class135_5.field1669 != null && class135_5.field1669.field1628 instanceof class130) {
            class130_6 = (class130) class135_5.field1669.field1628;
            class130.method2844(class130_1, class130_6, 128, 0, 128, true);
         }
      }

      if (i_3 < this.field1853 && i_4 > 0) {
         class135_5 = this.field1847[i_2][i_3 + 1][i_4 - 1];
         if (class135_5 != null && class135_5.field1669 != null && class135_5.field1669.field1628 instanceof class130) {
            class130_6 = (class130) class135_5.field1669.field1628;
            class130.method2844(class130_1, class130_6, 128, 0, -128, true);
         }
      }

   }

   boolean method3185(int i_1, int i_2, int i_3, int i_4) {
      if (!this.method3263(i_1, i_2, i_3)) {
         return false;
      } else {
         int i_5 = i_2 << 7;
         int i_6 = i_3 << 7;
         return this.method3183(i_5 + 1, this.field1828[i_1][i_2][i_3] - i_4, i_6 + 1) && this.method3183(i_5 + 128 - 1, this.field1828[i_1][i_2 + 1][i_3] - i_4, i_6 + 1) && this.method3183(i_5 + 128 - 1, this.field1828[i_1][i_2 + 1][i_3 + 1] - i_4, i_6 + 128 - 1) && this.method3183(i_5 + 1, this.field1828[i_1][i_2][i_3 + 1] - i_4, i_6 + 128 - 1);
      }
   }

   boolean method3182(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      int i_7;
      int i_8;
      if (i_3 == i_2 && i_5 == i_4) {
         if (!this.method3263(i_1, i_2, i_4)) {
            return false;
         } else {
            i_7 = i_2 << 7;
            i_8 = i_4 << 7;
            return this.method3183(i_7 + 1, this.field1828[i_1][i_2][i_4] - i_6, i_8 + 1) && this.method3183(i_7 + 128 - 1, this.field1828[i_1][i_2 + 1][i_4] - i_6, i_8 + 1) && this.method3183(i_7 + 128 - 1, this.field1828[i_1][i_2 + 1][i_4 + 1] - i_6, i_8 + 128 - 1) && this.method3183(i_7 + 1, this.field1828[i_1][i_2][i_4 + 1] - i_6, i_8 + 128 - 1);
         }
      } else {
         for (i_7 = i_2; i_7 <= i_3; i_7++) {
            for (i_8 = i_4; i_8 <= i_5; i_8++) {
               if (this.field1833[i_1][i_7][i_8] == -field1840) {
                  return false;
               }
            }
         }

         i_7 = (i_2 << 7) + 1;
         i_8 = (i_4 << 7) + 2;
         int i_9 = this.field1828[i_1][i_2][i_4] - i_6;
         if (!this.method3183(i_7, i_9, i_8)) {
            return false;
         } else {
            int i_10 = (i_3 << 7) - 1;
            if (!this.method3183(i_10, i_9, i_8)) {
               return false;
            } else {
               int i_11 = (i_5 << 7) - 1;
               if (!this.method3183(i_7, i_9, i_11)) {
                  return false;
               } else if (!this.method3183(i_10, i_9, i_11)) {
                  return false;
               } else {
                  return true;
               }
            }
         }
      }
   }

   void method3178() {
      int i_1 = field1865[field1872];
      class149[] arr_2 = field1834[field1872];
      field1867 = 0;

      for (int i_3 = 0; i_3 < i_1; i_3++) {
         class149 class149_4 = arr_2[i_3];
         int i_5;
         int i_6;
         int i_7;
         int i_9;
         boolean bool_13;
         if (class149_4.field1914 == 1) {
            i_5 = class149_4.field1913 - field1845 + 25;
            if (i_5 >= 0 && i_5 <= 50) {
               i_6 = class149_4.field1917 - field1846 + 25;
               if (i_6 < 0) {
                  i_6 = 0;
               }

               i_7 = class149_4.field1910 - field1846 + 25;
               if (i_7 > 50) {
                  i_7 = 50;
               }

               bool_13 = false;

               while (i_6 <= i_7) {
                  if (field1837[i_5][i_6++]) {
                     bool_13 = true;
                     break;
                  }
               }

               if (bool_13) {
                  i_9 = field1874 - class149_4.field1915;
                  if (i_9 > 32) {
                     class149_4.field1921 = 1;
                  } else {
                     if (i_9 >= -32) {
                        continue;
                     }

                     class149_4.field1921 = 2;
                     i_9 = -i_9;
                  }

                  class149_4.field1924 = (class149_4.field1912 - field1849 << 8) / i_9;
                  class149_4.field1929 = (class149_4.field1918 - field1849 << 8) / i_9;
                  class149_4.field1926 = (class149_4.field1919 - field1848 << 8) / i_9;
                  class149_4.field1927 = (class149_4.field1920 - field1848 << 8) / i_9;
                  field1868[field1867++] = class149_4;
               }
            }
         } else if (class149_4.field1914 == 2) {
            i_5 = class149_4.field1917 - field1846 + 25;
            if (i_5 >= 0 && i_5 <= 50) {
               i_6 = class149_4.field1913 - field1845 + 25;
               if (i_6 < 0) {
                  i_6 = 0;
               }

               i_7 = class149_4.field1911 - field1845 + 25;
               if (i_7 > 50) {
                  i_7 = 50;
               }

               bool_13 = false;

               while (i_6 <= i_7) {
                  if (field1837[i_6++][i_5]) {
                     bool_13 = true;
                     break;
                  }
               }

               if (bool_13) {
                  i_9 = field1849 - class149_4.field1912;
                  if (i_9 > 32) {
                     class149_4.field1921 = 3;
                  } else {
                     if (i_9 >= -32) {
                        continue;
                     }

                     class149_4.field1921 = 4;
                     i_9 = -i_9;
                  }

                  class149_4.field1925 = (class149_4.field1915 - field1874 << 8) / i_9;
                  class149_4.field1923 = (class149_4.field1916 - field1874 << 8) / i_9;
                  class149_4.field1926 = (class149_4.field1919 - field1848 << 8) / i_9;
                  class149_4.field1927 = (class149_4.field1920 - field1848 << 8) / i_9;
                  field1868[field1867++] = class149_4;
               }
            }
         } else if (class149_4.field1914 == 4) {
            i_5 = class149_4.field1919 - field1848;
            if (i_5 > 128) {
               i_6 = class149_4.field1917 - field1846 + 25;
               if (i_6 < 0) {
                  i_6 = 0;
               }

               i_7 = class149_4.field1910 - field1846 + 25;
               if (i_7 > 50) {
                  i_7 = 50;
               }

               if (i_6 <= i_7) {
                  int i_8 = class149_4.field1913 - field1845 + 25;
                  if (i_8 < 0) {
                     i_8 = 0;
                  }

                  i_9 = class149_4.field1911 - field1845 + 25;
                  if (i_9 > 50) {
                     i_9 = 50;
                  }

                  boolean bool_10 = false;

                  label144:
                  for (int i_11 = i_8; i_11 <= i_9; i_11++) {
                     for (int i_12 = i_6; i_12 <= i_7; i_12++) {
                        if (field1837[i_11][i_12]) {
                           bool_10 = true;
                           break label144;
                        }
                     }
                  }

                  if (bool_10) {
                     class149_4.field1921 = 5;
                     class149_4.field1925 = (class149_4.field1915 - field1874 << 8) / i_5;
                     class149_4.field1923 = (class149_4.field1916 - field1874 << 8) / i_5;
                     class149_4.field1924 = (class149_4.field1912 - field1849 << 8) / i_5;
                     class149_4.field1929 = (class149_4.field1918 - field1849 << 8) / i_5;
                     field1868[field1867++] = class149_4;
                  }
               }
            }
         }
      }

   }

   public void method3248() {
      int i_1;
      int i_2;
      for (i_1 = 0; i_1 < this.field1825; i_1++) {
         for (i_2 = 0; i_2 < this.field1853; i_2++) {
            for (int i_3 = 0; i_3 < this.field1827; i_3++) {
               this.field1847[i_1][i_2][i_3] = null;
            }
         }
      }

      for (i_1 = 0; i_1 < field1864; i_1++) {
         for (i_2 = 0; i_2 < field1865[i_1]; i_2++) {
            field1834[i_1][i_2] = null;
         }

         field1865[i_1] = 0;
      }

      for (i_1 = 0; i_1 < this.field1831; i_1++) {
         this.field1832[i_1] = null;
      }

      this.field1831 = 0;

      for (i_1 = 0; i_1 < field1854.length; i_1++) {
         field1854[i_1] = null;
      }

   }

   void method3211(class135 class135_1, boolean bool_2) {
      field1879.method4888(class135_1);

      while (true) {
         class135 class135_3;
         int i_4;
         int i_5;
         int i_6;
         int i_7;
         class135[][] arr_8;
         class135 class135_9;
         int i_11;
         int i_14;
         int i_15;
         int i_16;
         int i_24;
         int i_25;
         do {
            do {
               do {
                  do {
                     do {
                        do {
                           while (true) {
                              class145 class145_10;
                              class151 class151_12;
                              int i_17;
                              int i_18;
                              boolean bool_20;
                              int i_21;
                              class135 class135_36;
                              while (true) {
                                 do {
                                    class135_3 = (class135) field1879.method4880();
                                    if (class135_3 == null) {
                                       return;
                                    }
                                 } while (!class135_3.field1678);

                                 i_4 = class135_3.field1666;
                                 i_5 = class135_3.field1663;
                                 i_6 = class135_3.field1682;
                                 i_7 = class135_3.field1664;
                                 arr_8 = this.field1847[i_6];
                                 if (!class135_3.field1676) {
                                    break;
                                 }

                                 if (bool_2) {
                                    if (i_6 > 0) {
                                       class135_9 = this.field1847[i_6 - 1][i_4][i_5];
                                       if (class135_9 != null && class135_9.field1678) {
                                          continue;
                                       }
                                    }

                                    if (i_4 <= field1845 && i_4 > field1841) {
                                       class135_9 = arr_8[i_4 - 1][i_5];
                                       if (class135_9 != null && class135_9.field1678 && (class135_9.field1676 || (class135_3.field1674 & 0x1) == 0)) {
                                          continue;
                                       }
                                    }

                                    if (i_4 >= field1845 && i_4 < field1842 - 1) {
                                       class135_9 = arr_8[i_4 + 1][i_5];
                                       if (class135_9 != null && class135_9.field1678 && (class135_9.field1676 || (class135_3.field1674 & 0x4) == 0)) {
                                          continue;
                                       }
                                    }

                                    if (i_5 <= field1846 && i_5 > field1843) {
                                       class135_9 = arr_8[i_4][i_5 - 1];
                                       if (class135_9 != null && class135_9.field1678 && (class135_9.field1676 || (class135_3.field1674 & 0x8) == 0)) {
                                          continue;
                                       }
                                    }

                                    if (i_5 >= field1846 && i_5 < field1844 - 1) {
                                       class135_9 = arr_8[i_4][i_5 + 1];
                                       if (class135_9 != null && class135_9.field1678 && (class135_9.field1676 || (class135_3.field1674 & 0x2) == 0)) {
                                          continue;
                                       }
                                    }
                                 } else {
                                    bool_2 = true;
                                 }

                                 class135_3.field1676 = false;
                                 if (class135_3.field1683 != null) {
                                    class135_9 = class135_3.field1683;
                                    if (class135_9.field1661 != null) {
                                       if (!this.method3263(0, i_4, i_5)) {
                                          this.method3174(class135_9.field1661, 0, field1850, field1851, field1862, field1855, i_4, i_5);
                                       }
                                    } else if (class135_9.field1672 != null && !this.method3263(0, i_4, i_5)) {
                                       this.method3252(class135_9.field1672, field1850, field1851, field1862, field1855, i_4, i_5);
                                    }

                                    class145_10 = class135_9.field1667;
                                    if (class145_10 != null) {
                                       class145_10.field1899.vmethod3310(0, field1850, field1851, field1862, field1855, class145_10.field1896 - field1874, class145_10.field1901 - field1848, class145_10.field1898 - field1849, class145_10.field1902);
                                    }

                                    for (i_11 = 0; i_11 < class135_9.field1671; i_11++) {
                                       class151_12 = class135_9.field1662[i_11];
                                       if (class151_12 != null) {
                                          class151_12.field1946.vmethod3310(class151_12.field1951, field1850, field1851, field1862, field1855, class151_12.field1947 - field1874, class151_12.field1950 - field1848, class151_12.field1945 - field1849, class151_12.field1957);
                                       }
                                    }
                                 }

                                 bool_20 = false;
                                 if (class135_3.field1661 != null) {
                                    if (!this.method3263(i_7, i_4, i_5)) {
                                       bool_20 = true;
                                       if (class135_3.field1661.field1806 != 12345678 || field1852 && i_6 <= field1838) {
                                          this.method3174(class135_3.field1661, i_7, field1850, field1851, field1862, field1855, i_4, i_5);
                                       }
                                    }
                                 } else if (class135_3.field1672 != null && !this.method3263(i_7, i_4, i_5)) {
                                    bool_20 = true;
                                    this.method3252(class135_3.field1672, field1850, field1851, field1862, field1855, i_4, i_5);
                                 }

                                 i_21 = 0;
                                 i_11 = 0;
                                 class145 class145_31 = class135_3.field1667;
                                 class150 class150_13 = class135_3.field1668;
                                 if (class145_31 != null || class150_13 != null) {
                                    if (i_4 == field1845) {
                                       ++i_21;
                                    } else if (field1845 < i_4) {
                                       i_21 += 2;
                                    }

                                    if (i_5 == field1846) {
                                       i_21 += 3;
                                    } else if (field1846 > i_5) {
                                       i_21 += 6;
                                    }

                                    i_11 = field1826[i_21];
                                    class135_3.field1679 = field1829[i_21];
                                 }

                                 if (class145_31 != null) {
                                    if ((class145_31.field1895 & field1871[i_21]) != 0) {
                                       if (class145_31.field1895 == 16) {
                                          class135_3.field1665 = 3;
                                          class135_3.field1680 = field1873[i_21];
                                          class135_3.field1677 = 3 - class135_3.field1680;
                                       } else if (class145_31.field1895 == 32) {
                                          class135_3.field1665 = 6;
                                          class135_3.field1680 = field1866[i_21];
                                          class135_3.field1677 = 6 - class135_3.field1680;
                                       } else if (class145_31.field1895 == 64) {
                                          class135_3.field1665 = 12;
                                          class135_3.field1680 = field1875[i_21];
                                          class135_3.field1677 = 12 - class135_3.field1680;
                                       } else {
                                          class135_3.field1665 = 9;
                                          class135_3.field1680 = field1876[i_21];
                                          class135_3.field1677 = 9 - class135_3.field1680;
                                       }
                                    } else {
                                       class135_3.field1665 = 0;
                                    }

                                    if ((class145_31.field1895 & i_11) != 0 && !this.method3180(i_7, i_4, i_5, class145_31.field1895)) {
                                       class145_31.field1899.vmethod3310(0, field1850, field1851, field1862, field1855, class145_31.field1896 - field1874, class145_31.field1901 - field1848, class145_31.field1898 - field1849, class145_31.field1902);
                                    }

                                    if ((class145_31.field1900 & i_11) != 0 && !this.method3180(i_7, i_4, i_5, class145_31.field1900)) {
                                       class145_31.field1897.vmethod3310(0, field1850, field1851, field1862, field1855, class145_31.field1896 - field1874, class145_31.field1901 - field1848, class145_31.field1898 - field1849, class145_31.field1902);
                                    }
                                 }

                                 if (class150_13 != null && !this.method3185(i_7, i_4, i_5, class150_13.field1938.field1894)) {
                                    if ((class150_13.field1934 & i_11) != 0) {
                                       class150_13.field1938.vmethod3310(0, field1850, field1851, field1862, field1855, class150_13.field1932 - field1874 + class150_13.field1936, class150_13.field1937 - field1848, class150_13.field1933 - field1849 + class150_13.field1941, class150_13.field1940);
                                    } else if (class150_13.field1934 == 256) {
                                       i_14 = class150_13.field1932 - field1874;
                                       i_15 = class150_13.field1937 - field1848;
                                       i_16 = class150_13.field1933 - field1849;
                                       i_17 = class150_13.field1935;
                                       if (i_17 != 1 && i_17 != 2) {
                                          i_18 = i_14;
                                       } else {
                                          i_18 = -i_14;
                                       }

                                       int i_19;
                                       if (i_17 != 2 && i_17 != 3) {
                                          i_19 = i_16;
                                       } else {
                                          i_19 = -i_16;
                                       }

                                       if (i_19 < i_18) {
                                          class150_13.field1938.vmethod3310(0, field1850, field1851, field1862, field1855, i_14 + class150_13.field1936, i_15, i_16 + class150_13.field1941, class150_13.field1940);
                                       } else if (class150_13.field1942 != null) {
                                          class150_13.field1942.vmethod3310(0, field1850, field1851, field1862, field1855, i_14, i_15, i_16, class150_13.field1940);
                                       }
                                    }
                                 }

                                 if (bool_20) {
                                    class132 class132_22 = class135_3.field1669;
                                    if (class132_22 != null) {
                                       class132_22.field1628.vmethod3310(0, field1850, field1851, field1862, field1855, class132_22.field1625 - field1874, class132_22.field1632 - field1848, class132_22.field1627 - field1849, class132_22.field1629);
                                    }

                                    class129 class129_23 = class135_3.field1670;
                                    if (class129_23 != null && class129_23.field1551 == 0) {
                                       if (class129_23.field1558 != null) {
                                          class129_23.field1558.vmethod3310(0, field1850, field1851, field1862, field1855, class129_23.field1552 - field1874, class129_23.field1556 - field1848, class129_23.field1553 - field1849, class129_23.field1555);
                                       }

                                       if (class129_23.field1562 != null) {
                                          class129_23.field1562.vmethod3310(0, field1850, field1851, field1862, field1855, class129_23.field1552 - field1874, class129_23.field1556 - field1848, class129_23.field1553 - field1849, class129_23.field1555);
                                       }

                                       if (class129_23.field1560 != null) {
                                          class129_23.field1560.vmethod3310(0, field1850, field1851, field1862, field1855, class129_23.field1552 - field1874, class129_23.field1556 - field1848, class129_23.field1553 - field1849, class129_23.field1555);
                                       }
                                    }
                                 }

                                 i_14 = class135_3.field1674;
                                 if (i_14 != 0) {
                                    if (i_4 < field1845 && (i_14 & 0x4) != 0) {
                                       class135_36 = arr_8[i_4 + 1][i_5];
                                       if (class135_36 != null && class135_36.field1678) {
                                          field1879.method4888(class135_36);
                                       }
                                    }

                                    if (i_5 < field1846 && (i_14 & 0x2) != 0) {
                                       class135_36 = arr_8[i_4][i_5 + 1];
                                       if (class135_36 != null && class135_36.field1678) {
                                          field1879.method4888(class135_36);
                                       }
                                    }

                                    if (i_4 > field1845 && (i_14 & 0x1) != 0) {
                                       class135_36 = arr_8[i_4 - 1][i_5];
                                       if (class135_36 != null && class135_36.field1678) {
                                          field1879.method4888(class135_36);
                                       }
                                    }

                                    if (i_5 > field1846 && (i_14 & 0x8) != 0) {
                                       class135_36 = arr_8[i_4][i_5 - 1];
                                       if (class135_36 != null && class135_36.field1678) {
                                          field1879.method4888(class135_36);
                                       }
                                    }
                                 }
                                 break;
                              }

                              if (class135_3.field1665 != 0) {
                                 bool_20 = true;

                                 for (i_21 = 0; i_21 < class135_3.field1671; i_21++) {
                                    if (class135_3.field1662[i_21].field1948 != field1840 && (class135_3.field1673[i_21] & class135_3.field1665) == class135_3.field1680) {
                                       bool_20 = false;
                                       break;
                                    }
                                 }

                                 if (bool_20) {
                                    class145_10 = class135_3.field1667;
                                    if (!this.method3180(i_7, i_4, i_5, class145_10.field1895)) {
                                       class145_10.field1899.vmethod3310(0, field1850, field1851, field1862, field1855, class145_10.field1896 - field1874, class145_10.field1901 - field1848, class145_10.field1898 - field1849, class145_10.field1902);
                                    }

                                    class135_3.field1665 = 0;
                                 }
                              }

                              if (!class135_3.field1681) {
                                 break;
                              }

                              try {
                                 int i_34 = class135_3.field1671;
                                 class135_3.field1681 = false;
                                 i_21 = 0;

                                 label563:
                                 for (i_11 = 0; i_11 < i_34; i_11++) {
                                    class151_12 = class135_3.field1662[i_11];
                                    if (class151_12.field1948 != field1840) {
                                       for (i_24 = class151_12.field1956; i_24 <= class151_12.field1952; i_24++) {
                                          for (i_14 = class151_12.field1960; i_14 <= class151_12.field1954; i_14++) {
                                             class135_36 = arr_8[i_24][i_14];
                                             if (class135_36.field1676) {
                                                class135_3.field1681 = true;
                                                continue label563;
                                             }

                                             if (class135_36.field1665 != 0) {
                                                i_16 = 0;
                                                if (i_24 > class151_12.field1956) {
                                                   ++i_16;
                                                }

                                                if (i_24 < class151_12.field1952) {
                                                   i_16 += 4;
                                                }

                                                if (i_14 > class151_12.field1960) {
                                                   i_16 += 8;
                                                }

                                                if (i_14 < class151_12.field1954) {
                                                   i_16 += 2;
                                                }

                                                if ((i_16 & class135_36.field1665) == class135_3.field1677) {
                                                   class135_3.field1681 = true;
                                                   continue label563;
                                                }
                                             }
                                          }
                                       }

                                       field1854[i_21++] = class151_12;
                                       i_24 = field1845 - class151_12.field1956;
                                       i_14 = class151_12.field1952 - field1845;
                                       if (i_14 > i_24) {
                                          i_24 = i_14;
                                       }

                                       i_15 = field1846 - class151_12.field1960;
                                       i_16 = class151_12.field1954 - field1846;
                                       if (i_16 > i_15) {
                                          class151_12.field1953 = i_24 + i_16;
                                       } else {
                                          class151_12.field1953 = i_24 + i_15;
                                       }
                                    }
                                 }

                                 while (i_21 > 0) {
                                    i_11 = -50;
                                    i_25 = -1;

                                    for (i_24 = 0; i_24 < i_21; i_24++) {
                                       class151 class151_35 = field1854[i_24];
                                       if (class151_35.field1948 != field1840) {
                                          if (class151_35.field1953 > i_11) {
                                             i_11 = class151_35.field1953;
                                             i_25 = i_24;
                                          } else if (i_11 == class151_35.field1953) {
                                             i_15 = class151_35.field1947 - field1874;
                                             i_16 = class151_35.field1945 - field1849;
                                             i_17 = field1854[i_25].field1947 - field1874;
                                             i_18 = field1854[i_25].field1945 - field1849;
                                             if (i_15 * i_15 + i_16 * i_16 > i_17 * i_17 + i_18 * i_18) {
                                                i_25 = i_24;
                                             }
                                          }
                                       }
                                    }

                                    if (i_25 == -1) {
                                       break;
                                    }

                                    class151 class151_33 = field1854[i_25];
                                    class151_33.field1948 = field1840;
                                    if (!this.method3182(i_7, class151_33.field1956, class151_33.field1952, class151_33.field1960, class151_33.field1954, class151_33.field1946.field1894)) {
                                       class151_33.field1946.vmethod3310(class151_33.field1951, field1850, field1851, field1862, field1855, class151_33.field1947 - field1874, class151_33.field1950 - field1848, class151_33.field1945 - field1849, class151_33.field1957);
                                    }

                                    for (i_14 = class151_33.field1956; i_14 <= class151_33.field1952; i_14++) {
                                       for (i_15 = class151_33.field1960; i_15 <= class151_33.field1954; i_15++) {
                                          class135 class135_26 = arr_8[i_14][i_15];
                                          if (class135_26.field1665 != 0) {
                                             field1879.method4888(class135_26);
                                          } else if ((i_14 != i_4 || i_15 != i_5) && class135_26.field1678) {
                                             field1879.method4888(class135_26);
                                          }
                                       }
                                    }
                                 }

                                 if (!class135_3.field1681) {
                                    break;
                                 }
                              } catch (Exception exception_28) {
                                 class135_3.field1681 = false;
                                 break;
                              }
                           }
                        } while (!class135_3.field1678);
                     } while (class135_3.field1665 != 0);

                     if (i_4 > field1845 || i_4 <= field1841) {
                        break;
                     }

                     class135_9 = arr_8[i_4 - 1][i_5];
                  } while (class135_9 != null && class135_9.field1678);

                  if (i_4 < field1845 || i_4 >= field1842 - 1) {
                     break;
                  }

                  class135_9 = arr_8[i_4 + 1][i_5];
               } while (class135_9 != null && class135_9.field1678);

               if (i_5 > field1846 || i_5 <= field1843) {
                  break;
               }

               class135_9 = arr_8[i_4][i_5 - 1];
            } while (class135_9 != null && class135_9.field1678);

            if (i_5 < field1846 || i_5 >= field1844 - 1) {
               break;
            }

            class135_9 = arr_8[i_4][i_5 + 1];
         } while (class135_9 != null && class135_9.field1678);

         class135_3.field1678 = false;
         --field1870;
         class129 class129_32 = class135_3.field1670;
         if (class129_32 != null && class129_32.field1551 != 0) {
            if (class129_32.field1558 != null) {
               class129_32.field1558.vmethod3310(0, field1850, field1851, field1862, field1855, class129_32.field1552 - field1874, class129_32.field1556 - field1848 - class129_32.field1551, class129_32.field1553 - field1849, class129_32.field1555);
            }

            if (class129_32.field1562 != null) {
               class129_32.field1562.vmethod3310(0, field1850, field1851, field1862, field1855, class129_32.field1552 - field1874, class129_32.field1556 - field1848 - class129_32.field1551, class129_32.field1553 - field1849, class129_32.field1555);
            }

            if (class129_32.field1560 != null) {
               class129_32.field1560.vmethod3310(0, field1850, field1851, field1862, field1855, class129_32.field1552 - field1874, class129_32.field1556 - field1848 - class129_32.field1551, class129_32.field1553 - field1849, class129_32.field1555);
            }
         }

         if (class135_3.field1679 != 0) {
            class150 class150_29 = class135_3.field1668;
            if (class150_29 != null && !this.method3185(i_7, i_4, i_5, class150_29.field1938.field1894)) {
               if ((class150_29.field1934 & class135_3.field1679) != 0) {
                  class150_29.field1938.vmethod3310(0, field1850, field1851, field1862, field1855, class150_29.field1932 - field1874 + class150_29.field1936, class150_29.field1937 - field1848, class150_29.field1933 - field1849 + class150_29.field1941, class150_29.field1940);
               } else if (class150_29.field1934 == 256) {
                  i_11 = class150_29.field1932 - field1874;
                  i_25 = class150_29.field1937 - field1848;
                  i_24 = class150_29.field1933 - field1849;
                  i_14 = class150_29.field1935;
                  if (i_14 != 1 && i_14 != 2) {
                     i_15 = i_11;
                  } else {
                     i_15 = -i_11;
                  }

                  if (i_14 != 2 && i_14 != 3) {
                     i_16 = i_24;
                  } else {
                     i_16 = -i_24;
                  }

                  if (i_16 >= i_15) {
                     class150_29.field1938.vmethod3310(0, field1850, field1851, field1862, field1855, i_11 + class150_29.field1936, i_25, i_24 + class150_29.field1941, class150_29.field1940);
                  } else if (class150_29.field1942 != null) {
                     class150_29.field1942.vmethod3310(0, field1850, field1851, field1862, field1855, i_11, i_25, i_24, class150_29.field1940);
                  }
               }
            }

            class145 class145_27 = class135_3.field1667;
            if (class145_27 != null) {
               if ((class145_27.field1900 & class135_3.field1679) != 0 && !this.method3180(i_7, i_4, i_5, class145_27.field1900)) {
                  class145_27.field1897.vmethod3310(0, field1850, field1851, field1862, field1855, class145_27.field1896 - field1874, class145_27.field1901 - field1848, class145_27.field1898 - field1849, class145_27.field1902);
               }

               if ((class145_27.field1895 & class135_3.field1679) != 0 && !this.method3180(i_7, i_4, i_5, class145_27.field1895)) {
                  class145_27.field1899.vmethod3310(0, field1850, field1851, field1862, field1855, class145_27.field1896 - field1874, class145_27.field1901 - field1848, class145_27.field1898 - field1849, class145_27.field1902);
               }
            }
         }

         class135 class135_30;
         if (i_6 < this.field1825 - 1) {
            class135_30 = this.field1847[i_6 + 1][i_4][i_5];
            if (class135_30 != null && class135_30.field1678) {
               field1879.method4888(class135_30);
            }
         }

         if (i_4 < field1845) {
            class135_30 = arr_8[i_4 + 1][i_5];
            if (class135_30 != null && class135_30.field1678) {
               field1879.method4888(class135_30);
            }
         }

         if (i_5 < field1846) {
            class135_30 = arr_8[i_4][i_5 + 1];
            if (class135_30 != null && class135_30.field1678) {
               field1879.method4888(class135_30);
            }
         }

         if (i_4 > field1845) {
            class135_30 = arr_8[i_4 - 1][i_5];
            if (class135_30 != null && class135_30.field1678) {
               field1879.method4888(class135_30);
            }
         }

         if (i_5 > field1846) {
            class135_30 = arr_8[i_4][i_5 - 1];
            if (class135_30 != null && class135_30.field1678) {
               field1879.method4888(class135_30);
            }
         }
      }
   }

   public void method3165(int[] ints_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      class135 class135_7 = this.field1847[i_4][i_5][i_6];
      if (class135_7 != null) {
         class140 class140_8 = class135_7.field1661;
         int i_10;
         if (class140_8 != null) {
            int i_9 = class140_8.field1812;
            if (i_9 != 0) {
               for (i_10 = 0; i_10 < 4; i_10++) {
                  ints_1[i_2] = i_9;
                  ints_1[i_2 + 1] = i_9;
                  ints_1[i_2 + 2] = i_9;
                  ints_1[i_2 + 3] = i_9;
                  i_2 += i_3;
               }

            }
         } else {
            class131 class131_18 = class135_7.field1672;
            if (class131_18 != null) {
               i_10 = class131_18.field1614;
               int i_11 = class131_18.field1615;
               int i_12 = class131_18.field1616;
               int i_13 = class131_18.field1617;
               int[] ints_14 = this.field1881[i_10];
               int[] ints_15 = this.field1882[i_11];
               int i_16 = 0;
               int i_17;
               if (i_12 != 0) {
                  for (i_17 = 0; i_17 < 4; i_17++) {
                     ints_1[i_2] = ints_14[ints_15[i_16++]] == 0 ? i_12 : i_13;
                     ints_1[i_2 + 1] = ints_14[ints_15[i_16++]] == 0 ? i_12 : i_13;
                     ints_1[i_2 + 2] = ints_14[ints_15[i_16++]] == 0 ? i_12 : i_13;
                     ints_1[i_2 + 3] = ints_14[ints_15[i_16++]] == 0 ? i_12 : i_13;
                     i_2 += i_3;
                  }
               } else {
                  for (i_17 = 0; i_17 < 4; i_17++) {
                     if (ints_14[ints_15[i_16++]] != 0) {
                        ints_1[i_2] = i_13;
                     }

                     if (ints_14[ints_15[i_16++]] != 0) {
                        ints_1[i_2 + 1] = i_13;
                     }

                     if (ints_14[ints_15[i_16++]] != 0) {
                        ints_1[i_2 + 2] = i_13;
                     }

                     if (ints_14[ints_15[i_16++]] != 0) {
                        ints_1[i_2 + 3] = i_13;
                     }

                     i_2 += i_3;
                  }
               }

            }
         }
      }
   }

   public long method3160(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      return class135_4 != null && class135_4.field1669 != null ? class135_4.field1669.field1629 : 0L;
   }

   public class145 method3169(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      return class135_4 == null ? null : class135_4.field1667;
   }

   public long method3138(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      return class135_4 != null && class135_4.field1667 != null ? class135_4.field1667.field1902 : 0L;
   }

   public class150 method3206(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      return class135_4 == null ? null : class135_4.field1668;
   }

   public class151 method3155(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 == null) {
         return null;
      } else {
         for (int i_5 = 0; i_5 < class135_4.field1671; i_5++) {
            class151 class151_6 = class135_4.field1662[i_5];
            if (class196.method3653(class151_6.field1957) && i_2 == class151_6.field1956 && i_3 == class151_6.field1960) {
               return class151_6;
            }
         }

         return null;
      }
   }

   public class132 method3154(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      return class135_4 != null && class135_4.field1669 != null ? class135_4.field1669 : null;
   }

   public void method3130(int i_1) {
      this.field1830 = i_1;

      for (int i_2 = 0; i_2 < this.field1853; i_2++) {
         for (int i_3 = 0; i_3 < this.field1827; i_3++) {
            if (this.field1847[i_1][i_2][i_3] == null) {
               this.field1847[i_1][i_2][i_3] = new class135(i_1, i_2, i_3);
            }
         }
      }

   }

   public int method3139(int i_1, int i_2, int i_3, long long_4) {
      class135 class135_6 = this.field1847[i_1][i_2][i_3];
      if (class135_6 == null) {
         return -1;
      } else if (class135_6.field1667 != null && class135_6.field1667.field1902 == long_4) {
         return class135_6.field1667.field1903 & 0xff;
      } else if (class135_6.field1668 != null && class135_6.field1668.field1940 == long_4) {
         return class135_6.field1668.field1939 & 0xff;
      } else if (class135_6.field1669 != null && class135_6.field1669.field1629 == long_4) {
         return class135_6.field1669.field1630 & 0xff;
      } else {
         for (int i_7 = 0; i_7 < class135_6.field1671; i_7++) {
            if (class135_6.field1662[i_7].field1957 == long_4) {
               return class135_6.field1662[i_7].field1958 & 0xff;
            }
         }

         return -1;
      }
   }

   public long method3159(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 == null) {
         return 0L;
      } else {
         for (int i_5 = 0; i_5 < class135_4.field1671; i_5++) {
            class151 class151_6 = class135_4.field1662[i_5];
            if (class196.method3653(class151_6.field1957) && i_2 == class151_6.field1956 && i_3 == class151_6.field1960) {
               return class151_6.field1957;
            }
         }

         return 0L;
      }
   }

   public void method3152(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 != null) {
         class135_4.field1670 = null;
      }
   }

   public boolean method3142(int i_1, int i_2, int i_3, int i_4, int i_5, class144 class144_6, int i_7, long long_8, boolean bool_10) {
      if (class144_6 == null) {
         return true;
      } else {
         int i_11 = i_2 - i_5;
         int i_12 = i_3 - i_5;
         int i_13 = i_5 + i_2;
         int i_14 = i_3 + i_5;
         if (bool_10) {
            if (i_7 > 640 && i_7 < 1408) {
               i_14 += 128;
            }

            if (i_7 > 1152 && i_7 < 1920) {
               i_13 += 128;
            }

            if (i_7 > 1664 || i_7 < 384) {
               i_12 -= 128;
            }

            if (i_7 > 128 && i_7 < 896) {
               i_11 -= 128;
            }
         }

         i_11 /= 128;
         i_12 /= 128;
         i_13 /= 128;
         i_14 /= 128;
         return this.method3144(i_1, i_11, i_12, i_13 - i_11 + 1, i_14 - i_12 + 1, i_2, i_3, i_4, class144_6, i_7, true, long_8, 0);
      }
   }

   public boolean method3143(int i_1, int i_2, int i_3, int i_4, int i_5, class144 class144_6, int i_7, long long_8, int i_10, int i_11, int i_12, int i_13) {
      return class144_6 == null ? true : this.method3144(i_1, i_10, i_11, i_12 - i_10 + 1, i_13 - i_11 + 1, i_2, i_3, i_4, class144_6, i_7, true, long_8, 0);
   }

   public long method3158(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      return class135_4 != null && class135_4.field1668 != null ? class135_4.field1668.field1940 : 0L;
   }

   public void method3242(int i_1, int i_2, int i_3, int i_4, class144 class144_5, long long_6, class144 class144_8, class144 class144_9) {
      class129 class129_10 = new class129();
      class129_10.field1560 = class144_5;
      class129_10.field1552 = i_2 * 128 + 64;
      class129_10.field1553 = i_3 * 128 + 64;
      class129_10.field1556 = i_4;
      class129_10.field1555 = long_6;
      class129_10.field1558 = class144_8;
      class129_10.field1562 = class144_9;
      int i_11 = 0;
      class135 class135_12 = this.field1847[i_1][i_2][i_3];
      if (class135_12 != null) {
         for (int i_13 = 0; i_13 < class135_12.field1671; i_13++) {
            if ((class135_12.field1662[i_13].field1958 & 0x100) == 256 && class135_12.field1662[i_13].field1946 instanceof class136) {
               class136 class136_14 = (class136) class135_12.field1662[i_13].field1946;
               class136_14.method2946();
               if (class136_14.field1894 > i_11) {
                  i_11 = class136_14.field1894;
               }
            }
         }
      }

      class129_10.field1551 = i_11;
      if (this.field1847[i_1][i_2][i_3] == null) {
         this.field1847[i_1][i_2][i_3] = new class135(i_1, i_2, i_3);
      }

      this.field1847[i_1][i_2][i_3].field1670 = class129_10;
   }

   public void method3264() {
      field1861 = true;
   }

   public void method3162(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < this.field1825; i_4++) {
         for (int i_5 = 0; i_5 < this.field1853; i_5++) {
            for (int i_6 = 0; i_6 < this.field1827; i_6++) {
               class135 class135_7 = this.field1847[i_4][i_5][i_6];
               if (class135_7 != null) {
                  class145 class145_8 = class135_7.field1667;
                  class130 class130_10;
                  if (class145_8 != null && class145_8.field1899 instanceof class130) {
                     class130 class130_9 = (class130) class145_8.field1899;
                     this.method3164(class130_9, i_4, i_5, i_6, 1, 1);
                     if (class145_8.field1897 instanceof class130) {
                        class130_10 = (class130) class145_8.field1897;
                        this.method3164(class130_10, i_4, i_5, i_6, 1, 1);
                        class130.method2844(class130_9, class130_10, 0, 0, 0, false);
                        class145_8.field1897 = class130_10.method2876(class130_10.field1566, class130_10.field1591, i_1, i_2, i_3);
                     }

                     class145_8.field1899 = class130_9.method2876(class130_9.field1566, class130_9.field1591, i_1, i_2, i_3);
                  }

                  for (int i_12 = 0; i_12 < class135_7.field1671; i_12++) {
                     class151 class151_14 = class135_7.field1662[i_12];
                     if (class151_14 != null && class151_14.field1946 instanceof class130) {
                        class130 class130_11 = (class130) class151_14.field1946;
                        this.method3164(class130_11, i_4, i_5, i_6, class151_14.field1952 - class151_14.field1956 + 1, class151_14.field1954 - class151_14.field1960 + 1);
                        class151_14.field1946 = class130_11.method2876(class130_11.field1566, class130_11.field1591, i_1, i_2, i_3);
                     }
                  }

                  class132 class132_13 = class135_7.field1669;
                  if (class132_13 != null && class132_13.field1628 instanceof class130) {
                     class130_10 = (class130) class132_13.field1628;
                     this.method3163(class130_10, i_4, i_5, i_6);
                     class132_13.field1628 = class130_10.method2876(class130_10.field1566, class130_10.field1591, i_1, i_2, i_3);
                  }
               }
            }
         }
      }

   }

   public void method3257(int i_1, int i_2) {
      class135 class135_3 = this.field1847[0][i_1][i_2];

      for (int i_4 = 0; i_4 < 3; i_4++) {
         class135 class135_5 = this.field1847[i_4][i_1][i_2] = this.field1847[i_4 + 1][i_1][i_2];
         if (class135_5 != null) {
            --class135_5.field1682;

            for (int i_6 = 0; i_6 < class135_5.field1671; i_6++) {
               class151 class151_7 = class135_5.field1662[i_6];
               if (class196.method3653(class151_7.field1957) && class151_7.field1956 == i_1 && i_2 == class151_7.field1960) {
                  --class151_7.field1955;
               }
            }
         }
      }

      if (this.field1847[0][i_1][i_2] == null) {
         this.field1847[0][i_1][i_2] = new class135(0, i_1, i_2);
      }

      this.field1847[0][i_1][i_2].field1683 = class135_3;
      this.field1847[3][i_1][i_2] = null;
   }

   public void method3209(int i_1, int i_2, int i_3, int i_4) {
      class135 class135_5 = this.field1847[i_1][i_2][i_3];
      if (class135_5 != null) {
         this.field1847[i_1][i_2][i_3].field1675 = i_4;
      }
   }

   public void method3148(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 != null) {
         class135_4.field1667 = null;
      }
   }

   public void method3149(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 != null) {
         class135_4.field1668 = null;
      }
   }

   public void method3150(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 != null) {
         for (int i_5 = 0; i_5 < class135_4.field1671; i_5++) {
            class151 class151_6 = class135_4.field1662[i_5];
            if (class196.method3653(class151_6.field1957) && i_2 == class151_6.field1956 && i_3 == class151_6.field1960) {
               this.method3146(class151_6);
               return;
            }
         }

      }
   }

   public void method3151(int i_1, int i_2, int i_3) {
      class135 class135_4 = this.field1847[i_1][i_2][i_3];
      if (class135_4 != null) {
         class135_4.field1669 = null;
      }
   }

   public void method3137(int i_1, int i_2, int i_3, int i_4, class144 class144_5, long long_6, int i_8) {
      if (class144_5 != null) {
         class132 class132_9 = new class132();
         class132_9.field1628 = class144_5;
         class132_9.field1625 = i_2 * 128 + 64;
         class132_9.field1627 = i_3 * 128 + 64;
         class132_9.field1632 = i_4;
         class132_9.field1629 = long_6;
         class132_9.field1630 = i_8;
         if (this.field1847[i_1][i_2][i_3] == null) {
            this.field1847[i_1][i_2][i_3] = new class135(i_1, i_2, i_3);
         }

         this.field1847[i_1][i_2][i_3].field1669 = class132_9;
      }
   }

   public boolean method3132(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, class144 class144_7, int i_8, long long_9, int i_11) {
      if (class144_7 == null) {
         return true;
      } else {
         int i_12 = i_5 * 64 + i_2 * 128;
         int i_13 = i_6 * 64 + i_3 * 128;
         return this.method3144(i_1, i_2, i_3, i_5, i_6, i_12, i_13, i_4, class144_7, i_8, false, long_9, i_11);
      }
   }

   public void method3258(int i_1, int i_2, int i_3, int i_4, class144 class144_5, class144 class144_6, int i_7, int i_8, long long_9, int i_11) {
      if (class144_5 != null || class144_6 != null) {
         class145 class145_12 = new class145();
         class145_12.field1902 = long_9;
         class145_12.field1903 = i_11;
         class145_12.field1896 = i_2 * 128 + 64;
         class145_12.field1898 = i_3 * 128 + 64;
         class145_12.field1901 = i_4;
         class145_12.field1899 = class144_5;
         class145_12.field1897 = class144_6;
         class145_12.field1895 = i_7;
         class145_12.field1900 = i_8;

         for (int i_13 = i_1; i_13 >= 0; --i_13) {
            if (this.field1847[i_13][i_2][i_3] == null) {
               this.field1847[i_13][i_2][i_3] = new class135(i_13, i_2, i_3);
            }
         }

         this.field1847[i_1][i_2][i_3].field1667 = class145_12;
      }
   }

   public void method3140(int i_1, int i_2, int i_3, int i_4, class144 class144_5, class144 class144_6, int i_7, int i_8, int i_9, int i_10, long long_11, int i_13) {
      if (class144_5 != null) {
         class150 class150_14 = new class150();
         class150_14.field1940 = long_11;
         class150_14.field1939 = i_13;
         class150_14.field1932 = i_2 * 128 + 64;
         class150_14.field1933 = i_3 * 128 + 64;
         class150_14.field1937 = i_4;
         class150_14.field1938 = class144_5;
         class150_14.field1942 = class144_6;
         class150_14.field1934 = i_7;
         class150_14.field1935 = i_8;
         class150_14.field1936 = i_9;
         class150_14.field1941 = i_10;

         for (int i_15 = i_1; i_15 >= 0; --i_15) {
            if (this.field1847[i_15][i_2][i_3] == null) {
               this.field1847[i_15][i_2][i_3] = new class135(i_15, i_2, i_3);
            }
         }

         this.field1847[i_1][i_2][i_3].field1668 = class150_14;
      }
   }

   public void method3136(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11, int i_12, int i_13, int i_14, int i_15, int i_16, int i_17, int i_18, int i_19, int i_20) {
      class140 class140_21;
      int i_22;
      if (i_4 == 0) {
         class140_21 = new class140(i_11, i_12, i_13, i_14, -1, i_19, false);

         for (i_22 = i_1; i_22 >= 0; --i_22) {
            if (this.field1847[i_22][i_2][i_3] == null) {
               this.field1847[i_22][i_2][i_3] = new class135(i_22, i_2, i_3);
            }
         }

         this.field1847[i_1][i_2][i_3].field1661 = class140_21;
      } else if (i_4 != 1) {
         class131 class131_23 = new class131(i_4, i_5, i_6, i_2, i_3, i_7, i_8, i_9, i_10, i_11, i_12, i_13, i_14, i_15, i_16, i_17, i_18, i_19, i_20);

         for (i_22 = i_1; i_22 >= 0; --i_22) {
            if (this.field1847[i_22][i_2][i_3] == null) {
               this.field1847[i_22][i_2][i_3] = new class135(i_22, i_2, i_3);
            }
         }

         this.field1847[i_1][i_2][i_3].field1672 = class131_23;
      } else {
         class140_21 = new class140(i_15, i_16, i_17, i_18, i_6, i_20, i_8 == i_7 && i_7 == i_9 && i_10 == i_7);

         for (i_22 = i_1; i_22 >= 0; --i_22) {
            if (this.field1847[i_22][i_2][i_3] == null) {
               this.field1847[i_22][i_2][i_3] = new class135(i_22, i_2, i_3);
            }
         }

         this.field1847[i_1][i_2][i_3].field1661 = class140_21;
      }
   }

   public void method3181(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if (i_1 < 0) {
         i_1 = 0;
      } else if (i_1 >= this.field1853 * 128) {
         i_1 = this.field1853 * 128 - 1;
      }

      if (i_3 < 0) {
         i_3 = 0;
      } else if (i_3 >= this.field1827 * 128) {
         i_3 = this.field1827 * 128 - 1;
      }

      if (i_4 < 128) {
         i_4 = 128;
      } else if (i_4 > 383) {
         i_4 = 383;
      }

      ++field1840;
      field1850 = class139.field1797[i_4];
      field1851 = class139.field1780[i_4];
      field1862 = class139.field1797[i_5];
      field1855 = class139.field1780[i_5];
      field1837 = field1883[(i_4 - 128) / 32][i_5 / 64];
      field1874 = i_1;
      field1848 = i_2;
      field1849 = i_3;
      field1845 = i_1 / 128;
      field1846 = i_3 / 128;
      field1872 = i_6;
      field1841 = field1845 - 25;
      if (field1841 < 0) {
         field1841 = 0;
      }

      field1843 = field1846 - 25;
      if (field1843 < 0) {
         field1843 = 0;
      }

      field1842 = field1845 + 25;
      if (field1842 > this.field1853) {
         field1842 = this.field1853;
      }

      field1844 = field1846 + 25;
      if (field1844 > this.field1827) {
         field1844 = this.field1827;
      }

      this.method3178();
      field1870 = 0;

      int i_7;
      class135[][] arr_8;
      int i_9;
      int i_10;
      for (i_7 = this.field1830; i_7 < this.field1825; i_7++) {
         arr_8 = this.field1847[i_7];

         for (i_9 = field1841; i_9 < field1842; i_9++) {
            for (i_10 = field1843; i_10 < field1844; i_10++) {
               class135 class135_11 = arr_8[i_9][i_10];
               if (class135_11 != null) {
                  if (class135_11.field1675 > i_6 || !field1837[i_9 - field1845 + 25][i_10 - field1846 + 25] && this.field1828[i_7][i_9][i_10] - i_2 < 2000) {
                     class135_11.field1676 = false;
                     class135_11.field1678 = false;
                     class135_11.field1665 = 0;
                  } else {
                     class135_11.field1676 = true;
                     class135_11.field1678 = true;
                     if (class135_11.field1671 > 0) {
                        class135_11.field1681 = true;
                     } else {
                        class135_11.field1681 = false;
                     }

                     ++field1870;
                  }
               }
            }
         }
      }

      int i_12;
      int i_13;
      int i_14;
      class135 class135_15;
      int i_16;
      for (i_7 = this.field1830; i_7 < this.field1825; i_7++) {
         arr_8 = this.field1847[i_7];

         for (i_9 = -25; i_9 <= 0; i_9++) {
            i_10 = i_9 + field1845;
            i_16 = field1845 - i_9;
            if (i_10 >= field1841 || i_16 < field1842) {
               for (i_12 = -25; i_12 <= 0; i_12++) {
                  i_13 = i_12 + field1846;
                  i_14 = field1846 - i_12;
                  if (i_10 >= field1841) {
                     if (i_13 >= field1843) {
                        class135_15 = arr_8[i_10][i_13];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, true);
                        }
                     }

                     if (i_14 < field1844) {
                        class135_15 = arr_8[i_10][i_14];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, true);
                        }
                     }
                  }

                  if (i_16 < field1842) {
                     if (i_13 >= field1843) {
                        class135_15 = arr_8[i_16][i_13];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, true);
                        }
                     }

                     if (i_14 < field1844) {
                        class135_15 = arr_8[i_16][i_14];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, true);
                        }
                     }
                  }

                  if (field1870 == 0) {
                     field1852 = false;
                     return;
                  }
               }
            }
         }
      }

      for (i_7 = this.field1830; i_7 < this.field1825; i_7++) {
         arr_8 = this.field1847[i_7];

         for (i_9 = -25; i_9 <= 0; i_9++) {
            i_10 = i_9 + field1845;
            i_16 = field1845 - i_9;
            if (i_10 >= field1841 || i_16 < field1842) {
               for (i_12 = -25; i_12 <= 0; i_12++) {
                  i_13 = i_12 + field1846;
                  i_14 = field1846 - i_12;
                  if (i_10 >= field1841) {
                     if (i_13 >= field1843) {
                        class135_15 = arr_8[i_10][i_13];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, false);
                        }
                     }

                     if (i_14 < field1844) {
                        class135_15 = arr_8[i_10][i_14];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, false);
                        }
                     }
                  }

                  if (i_16 < field1842) {
                     if (i_13 >= field1843) {
                        class135_15 = arr_8[i_16][i_13];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, false);
                        }
                     }

                     if (i_14 < field1844) {
                        class135_15 = arr_8[i_16][i_14];
                        if (class135_15 != null && class135_15.field1676) {
                           this.method3211(class135_15, false);
                        }
                     }
                  }

                  if (field1870 == 0) {
                     field1852 = false;
                     return;
                  }
               }
            }
         }
      }

      field1852 = false;
   }

   public void method3147(int i_1, int i_2, int i_3, int i_4) {
      class135 class135_5 = this.field1847[i_1][i_2][i_3];
      if (class135_5 != null) {
         class150 class150_6 = class135_5.field1668;
         if (class150_6 != null) {
            class150_6.field1936 = i_4 * class150_6.field1936 / 16;
            class150_6.field1941 = i_4 * class150_6.field1941 / 16;
         }
      }
   }

   public void method3197() {
      for (int i_1 = 0; i_1 < this.field1831; i_1++) {
         class151 class151_2 = this.field1832[i_1];
         this.method3146(class151_2);
         this.field1832[i_1] = null;
      }

      this.field1831 = 0;
   }

   public static boolean method3170() {
      return field1861 && field1859 != -1;
   }

   static boolean method3285(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (i_1 < i_2 && i_1 < i_3 && i_1 < i_4) {
         return false;
      } else if (i_1 > i_2 && i_1 > i_3 && i_1 > i_4) {
         return false;
      } else if (i_0 < i_5 && i_0 < i_6 && i_0 < i_7) {
         return false;
      } else if (i_0 > i_5 && i_0 > i_6 && i_0 > i_7) {
         return false;
      } else {
         int i_8 = (i_1 - i_2) * (i_6 - i_5) - (i_0 - i_5) * (i_3 - i_2);
         int i_9 = (i_7 - i_6) * (i_1 - i_3) - (i_0 - i_6) * (i_4 - i_3);
         int i_10 = (i_5 - i_7) * (i_1 - i_4) - (i_2 - i_4) * (i_0 - i_7);
         return i_8 == 0 ? (i_9 != 0 ? (i_9 < 0 ? i_10 <= 0 : i_10 >= 0) : true) : (i_8 < 0 ? i_9 <= 0 && i_10 <= 0 : i_9 >= 0 && i_10 >= 0);
      }
   }

   static final int method3176(int i_0, int i_1) {
      i_1 = (i_0 & 0x7f) * i_1 >> 7;
      if (i_1 < 2) {
         i_1 = 2;
      } else if (i_1 > 126) {
         i_1 = 126;
      }

      return (i_0 & 0xff80) + i_1;
   }

   public static void method3133(int[] ints_0, int i_1, int i_2, int i_3, int i_4) {
      field1887 = 0;
      field1888 = 0;
      field1889 = i_3;
      field1824 = i_4;
      field1885 = i_3 / 2;
      field1886 = i_4 / 2;
      boolean[][][][] bools_5 = new boolean[ints_0.length][32][53][53];

      int i_6;
      int i_7;
      int i_8;
      int i_9;
      int i_11;
      int i_12;
      for (i_6 = 128; i_6 <= 383; i_6 += 32) {
         for (i_7 = 0; i_7 < 2048; i_7 += 64) {
            field1850 = class139.field1797[i_6];
            field1851 = class139.field1780[i_6];
            field1862 = class139.field1797[i_7];
            field1855 = class139.field1780[i_7];
            i_8 = (i_6 - 128) / 32;
            i_9 = i_7 / 64;

            for (int i_10 = -26; i_10 < 26; i_10++) {
               for (i_11 = -26; i_11 < 26; i_11++) {
                  i_12 = i_10 * 128;
                  int i_13 = i_11 * 128;
                  boolean bool_14 = false;

                  for (int i_15 = -i_1; i_15 <= i_2; i_15 += 128) {
                     if (method3167(i_12, ints_0[i_8] + i_15, i_13)) {
                        bool_14 = true;
                        break;
                     }
                  }

                  bools_5[i_8][i_9][i_10 + 1 + 25][i_11 + 1 + 25] = bool_14;
               }
            }
         }
      }

      for (i_6 = 0; i_6 < 8; i_6++) {
         for (i_7 = 0; i_7 < 32; i_7++) {
            for (i_8 = -25; i_8 < 25; i_8++) {
               for (i_9 = -25; i_9 < 25; i_9++) {
                  boolean bool_16 = false;

                  label76:
                  for (i_11 = -1; i_11 <= 1; i_11++) {
                     for (i_12 = -1; i_12 <= 1; i_12++) {
                        if (bools_5[i_6][i_7][i_8 + i_11 + 1 + 25][i_9 + i_12 + 1 + 25]) {
                           bool_16 = true;
                           break label76;
                        }

                        if (bools_5[i_6][(i_7 + 1) % 31][i_8 + i_11 + 1 + 25][i_9 + i_12 + 1 + 25]) {
                           bool_16 = true;
                           break label76;
                        }

                        if (bools_5[i_6 + 1][i_7][i_8 + i_11 + 1 + 25][i_9 + i_12 + 1 + 25]) {
                           bool_16 = true;
                           break label76;
                        }

                        if (bools_5[i_6 + 1][(i_7 + 1) % 31][i_8 + i_11 + 1 + 25][i_9 + i_12 + 1 + 25]) {
                           bool_16 = true;
                           break label76;
                        }
                     }
                  }

                  field1883[i_6][i_7][i_8 + 25][i_9 + 25] = bool_16;
               }
            }
         }
      }

   }

   public static void method3134(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      class149 class149_8 = new class149();
      class149_8.field1913 = i_2 / 128;
      class149_8.field1911 = i_3 / 128;
      class149_8.field1917 = i_4 / 128;
      class149_8.field1910 = i_5 / 128;
      class149_8.field1914 = i_1;
      class149_8.field1915 = i_2;
      class149_8.field1916 = i_3;
      class149_8.field1912 = i_4;
      class149_8.field1918 = i_5;
      class149_8.field1919 = i_6;
      class149_8.field1920 = i_7;
      field1834[i_0][field1865[i_0]++] = class149_8;
   }

   public static void method3171() {
      field1859 = -1;
      field1861 = false;
   }

   static boolean method3167(int i_0, int i_1, int i_2) {
      int i_3 = i_0 * field1855 + i_2 * field1862 >> 16;
      int i_4 = i_2 * field1855 - i_0 * field1862 >> 16;
      int i_5 = i_4 * field1851 + field1850 * i_1 >> 16;
      int i_6 = field1851 * i_1 - i_4 * field1850 >> 16;
      if (i_5 >= 50 && i_5 <= 3500) {
         int i_7 = i_3 * 128 / i_5 + field1885;
         int i_8 = i_6 * 128 / i_5 + field1886;
         return i_7 >= field1887 && i_7 <= field1889 && i_8 >= field1888 && i_8 <= field1824;
      } else {
         return false;
      }
   }

}
