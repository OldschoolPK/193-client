public final class class307 {

   static long field3744;
   static long field3743;
   static class128 field3745;

   static final void method5441(class309 class309_0, int i_1) {
      class309_0.method5454(1090912894);
      int i_2 = client.field765;
      class75 class75_3 = class223.field2562 = client.field764[i_2] = new class75();
      class75_3.field628 = i_2;
      int i_4 = class309_0.method5455(30, (byte) -109);
      byte b_5 = (byte)(i_4 >> 28);
      int i_6 = i_4 >> 14 & 0x3fff;
      int i_7 = i_4 & 0x3fff;
      class75_3.field993[0] = i_6 - class243.field3156;
      class75_3.field990 = (class75_3.field993[0] << 7) + (class75_3.method1311((byte) -20) << 6);
      class75_3.field968[0] = i_7 - class41.field300;
      class75_3.field938 = (class75_3.field968[0] << 7) + (class75_3.method1311((byte) -68) << 6);
      class151.field1949 = class75_3.field627 = b_5;
      if (class98.field1254[i_2] != null) {
         class75_3.method1334(class98.field1254[i_2], (byte) -58);
      }

      class98.field1263 = 0;
      class98.field1268[++class98.field1263 - 1] = i_2;
      class98.field1252[i_2] = 0;
      class98.field1257 = 0;

      for (int i_8 = 1; i_8 < 2048; i_8++) {
         if (i_2 != i_8) {
            int i_9 = class309_0.method5455(18, (byte) -19);
            int i_10 = i_9 >> 16;
            int i_11 = i_9 >> 8 & 0x255;
            int i_12 = i_9 & 0x255;
            class98.field1259[i_8] = (i_11 << 14) + i_12 + (i_10 << 28);
            class98.field1269[i_8] = 0;
            class98.field1261[i_8] = -1;
            class98.field1258[++class98.field1257 - 1] = i_8;
            class98.field1252[i_8] = 0;
         }
      }

      class309_0.method5477(578417910);
   }

}
