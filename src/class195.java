public class class195 {

   public static boolean[] field2355;
   static class246 field2357;

   static void method3651(int i_0) {
      class248.field3219 = new int[2000];
      int i_1 = 0;
      int i_2 = 240;

      int i_4;
      for (byte b_3 = 12; i_1 < 16; i_2 -= b_3) {
         i_4 = class2.method23((double)((float)i_2 / 360.0F), 0.9998999834060669D, (double)(0.425F * (float)i_1 / 16.0F + 0.075F));
         class248.field3219[i_1] = i_4;
         ++i_1;
      }

      i_2 = 48;

      for (int i_6 = i_2 / 6; i_1 < class248.field3219.length; i_2 -= i_6) {
         i_4 = i_1 * 2;

         for (int i_5 = class2.method23((double)((float)i_2 / 360.0F), 0.9998999834060669D, 0.5D); i_1 < i_4 && i_1 < class248.field3219.length; i_1++) {
            class248.field3219[i_1] = i_5;
         }
      }

   }

   static void method3652(int i_0) {
      for (class70 class70_1 = (class70) client.field833.method5962(); class70_1 != null; class70_1 = (class70) client.field833.method5957()) {
         int i_2 = class70_1.field563;
         if (class41.method603(i_2, -364577120)) {
            boolean bool_3 = true;
            class226[] arr_4 = class9.field44[i_2];

            int i_5;
            for (i_5 = 0; i_5 < arr_4.length; i_5++) {
               if (arr_4[i_5] != null) {
                  bool_3 = arr_4[i_5].field2585;
                  break;
               }
            }

            if (!bool_3) {
               i_5 = (int)class70_1.field2137;
               class226 class226_6 = class181.method3610(i_5, -1344769243);
               if (class226_6 != null) {
                  class181.method3609(class226_6, -1900442183);
               }
            }
         }
      }

   }

   static final String method3649(int i_0, byte b_1) {
      return i_0 < 100000 ? "<col=ffff00>" + i_0 + "</col>" : (i_0 < 10000000 ? "<col=ffffff>" + i_0 / 1000 + "K" + "</col>" : "<col=00ff80>" + i_0 / 1000000 + "M" + "</col>");
   }

   static class73 method3650(int i_0, int i_1) {
      return (class73) class100.field1285.method5919((long)i_0);
   }

}
