public class class147 extends class184 {

   class134[] field1909;

   class147(class244 class244_1, class244 class244_2, int i_3, boolean bool_4) {
      class272 class272_5 = new class272();
      int i_6 = class244_1.method4172(i_3, (byte) 17);
      this.field1909 = new class134[i_6];
      int[] ints_7 = class244_1.method4171(i_3, -535725640);

      for (int i_8 = 0; i_8 < ints_7.length; i_8++) {
         byte[] bytes_9 = class244_1.method4160(i_3, ints_7[i_8], (short) -13733);
         class141 class141_10 = null;
         int i_11 = (bytes_9[0] & 0xff) << 8 | bytes_9[1] & 0xff;

         for (class141 class141_12 = (class141) class272_5.method4879(); class141_12 != null; class141_12 = (class141) class272_5.method4884()) {
            if (i_11 == class141_12.field1817) {
               class141_10 = class141_12;
               break;
            }
         }

         if (class141_10 == null) {
            byte[] bytes_13;
            if (bool_4) {
               bytes_13 = class244_2.method4245(0, i_11, -1763510335);
            } else {
               bytes_13 = class244_2.method4245(i_11, 0, -532314017);
            }

            class141_10 = new class141(i_11, bytes_13);
            class272_5.method4888(class141_10);
         }

         this.field1909[ints_7[i_8]] = new class134(bytes_9, class141_10);
      }

   }

   public boolean method3320(int i_1, int i_2) {
      return this.field1909[i_1].field1660;
   }

   static final void method3324(int i_0, int i_1, int i_2, int i_3, byte b_4) {
      for (int i_5 = 0; i_5 < client.field824; i_5++) {
         if (client.field856[i_5] + client.field858[i_5] > i_0 && client.field856[i_5] < i_0 + i_2 && client.field917[i_5] + client.field808[i_5] > i_1 && client.field917[i_5] < i_3 + i_1) {
            client.field901[i_5] = true;
         }
      }

   }

   static int method3326(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      class226 class226_4;
      if (i_0 != 1927 && i_0 != 2927) {
         int i_5;
         if (i_0 == 1928) {
            class226_4 = bool_2 ? class223.field2561 : class253.field3264;
            i_5 = class85.field1095[--class253.field3267];
            if (i_5 >= 1 && i_5 <= 10) {
               class3.method31(i_5, class226_4.field2586, class226_4.field2587, class226_4.field2712, "", 2087962129);
               return 1;
            } else {
               throw new RuntimeException();
            }
         } else if (i_0 == 2928) {
            class253.field3267 -= 3;
            int i_8 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            int i_6 = class85.field1095[class253.field3267 + 2];
            if (i_6 >= 1 && i_6 <= 10) {
               class3.method31(i_6, i_8, i_5, class181.method3610(i_8, -181504490).field2712, "", 2055383051);
               return 1;
            } else {
               throw new RuntimeException();
            }
         } else {
            return 2;
         }
      } else if (class85.field1097 >= 10) {
         throw new RuntimeException();
      } else {
         if (i_0 >= 2000) {
            class226_4 = class181.method3610(class85.field1095[--class253.field3267], 569015055);
         } else {
            class226_4 = bool_2 ? class223.field2561 : class253.field3264;
         }

         if (class226_4.field2701 == null) {
            return 0;
         } else {
            class71 class71_7 = new class71();
            class71_7.field571 = class226_4;
            class71_7.field572 = class226_4.field2701;
            class71_7.field569 = class85.field1097 + 1;
            client.field847.method4888(class71_7);
            return 1;
         }
      }
   }

   static int method3328(int i_0, int i_1, byte b_2) {
      class68 class68_3 = (class68) class68.field546.method5968((long)i_0);
      if (class68_3 == null) {
         return 0;
      } else if (i_1 == -1) {
         return 0;
      } else {
         int i_4 = 0;

         for (int i_5 = 0; i_5 < class68_3.field539.length; i_5++) {
            if (class68_3.field540[i_5] == i_1) {
               i_4 += class68_3.field539[i_5];
            }
         }

         return i_4;
      }
   }

}
