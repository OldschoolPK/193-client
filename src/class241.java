public class class241 implements class203 {

   public static int field3135;
   public static final class241 field3139 = new class241("runescape", "RuneScape", 0);
   public static final class241 field3136 = new class241("stellardawn", "Stellar Dawn", 1);
   public static final class241 field3137 = new class241("game3", "Game 3", 2);
   public static final class241 field3138 = new class241("game4", "Game 4", 3);
   public static final class241 field3142 = new class241("game5", "Game 5", 4);
   public static final class241 field3140 = new class241("oldscape", "RuneScape 2007", 5);
   public final String field3141;
   final int field3143;

   class241(String string_1, String string_2, int i_3) {
      this.field3141 = string_1;
      this.field3143 = i_3;
   }

   public int vmethod6086(int i_1) {
      return this.field3143;
   }

   static String method4150(class226 class226_0, byte b_1) {
      return class191.method3638(class146.method3318(class226_0, -489600903), -1761300214) == 0 ? null : (class226_0.field2576 != null && class226_0.field2576.trim().length() != 0 ? class226_0.field2576 : null);
   }

   public static void method4152(int i_0) {
      class260.field3329.method3375();
   }

   static int method4153(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      int i_4;
      if (i_0 == 4200) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1096[++class85.field1105 - 1] = class66.method1223(i_4, -877772844).field3444;
         return 1;
      } else {
         int i_5;
         class265 class265_6;
         if (i_0 == 4201) {
            class253.field3267 -= 2;
            i_4 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            class265_6 = class66.method1223(i_4, 181262994);
            if (i_5 >= 1 && i_5 <= 5 && class265_6.field3458[i_5 - 1] != null) {
               class85.field1096[++class85.field1105 - 1] = class265_6.field3458[i_5 - 1];
            } else {
               class85.field1096[++class85.field1105 - 1] = "";
            }

            return 1;
         } else if (i_0 == 4202) {
            class253.field3267 -= 2;
            i_4 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            class265_6 = class66.method1223(i_4, -936495674);
            if (i_5 >= 1 && i_5 <= 5 && class265_6.field3459[i_5 - 1] != null) {
               class85.field1096[++class85.field1105 - 1] = class265_6.field3459[i_5 - 1];
            } else {
               class85.field1096[++class85.field1105 - 1] = "";
            }

            return 1;
         } else if (i_0 == 4203) {
            i_4 = class85.field1095[--class253.field3267];
            class85.field1095[++class253.field3267 - 1] = class66.method1223(i_4, -1315806263).field3477;
            return 1;
         } else if (i_0 == 4204) {
            i_4 = class85.field1095[--class253.field3267];
            class85.field1095[++class253.field3267 - 1] = class66.method1223(i_4, 494059607).field3455 == 1 ? 1 : 0;
            return 1;
         } else {
            class265 class265_7;
            if (i_0 == 4205) {
               i_4 = class85.field1095[--class253.field3267];
               class265_7 = class66.method1223(i_4, -339842082);
               if (class265_7.field3432 * -582464211 == -1 && class265_7.field3475 >= 0) {
                  class85.field1095[++class253.field3267 - 1] = class265_7.field3475;
               } else {
                  class85.field1095[++class253.field3267 - 1] = i_4;
               }

               return 1;
            } else if (i_0 == 4206) {
               i_4 = class85.field1095[--class253.field3267];
               class265_7 = class66.method1223(i_4, -329313007);
               if (class265_7.field3432 * -582464211 >= 0 && class265_7.field3475 >= 0) {
                  class85.field1095[++class253.field3267 - 1] = class265_7.field3475;
               } else {
                  class85.field1095[++class253.field3267 - 1] = i_4;
               }

               return 1;
            } else if (i_0 == 4207) {
               i_4 = class85.field1095[--class253.field3267];
               class85.field1095[++class253.field3267 - 1] = class66.method1223(i_4, -681799575).field3441 ? 1 : 0;
               return 1;
            } else if (i_0 == 4208) {
               i_4 = class85.field1095[--class253.field3267];
               class265_7 = class66.method1223(i_4, -2121242448);
               if (class265_7.field3488 == -1 && class265_7.field3487 >= 0) {
                  class85.field1095[++class253.field3267 - 1] = class265_7.field3487;
               } else {
                  class85.field1095[++class253.field3267 - 1] = i_4;
               }

               return 1;
            } else if (i_0 == 4209) {
               i_4 = class85.field1095[--class253.field3267];
               class265_7 = class66.method1223(i_4, -932115498);
               if (class265_7.field3488 >= 0 && class265_7.field3487 >= 0) {
                  class85.field1095[++class253.field3267 - 1] = class265_7.field3487;
               } else {
                  class85.field1095[++class253.field3267 - 1] = i_4;
               }

               return 1;
            } else if (i_0 == 4210) {
               String string_8 = class85.field1096[--class85.field1105];
               i_5 = class85.field1095[--class253.field3267];
               class42.method627(string_8, i_5 == 1, (byte) 71);
               class85.field1095[++class253.field3267 - 1] = class196.field2367;
               return 1;
            } else if (i_0 != 4211) {
               if (i_0 == 4212) {
                  class247.field3214 = 0;
                  return 1;
               } else {
                  return 2;
               }
            } else {
               if (class153.field1970 != null && class247.field3214 < class196.field2367) {
                  class85.field1095[++class253.field3267 - 1] = class153.field1970[++class247.field3214 - 1] & 0xffff;
               } else {
                  class85.field1095[++class253.field3267 - 1] = -1;
               }

               return 1;
            }
         }
      }
   }

}
