public class class121 {

   static int field1477;
   int field1473;
   int field1474;
   int[][] field1476;

   public class121(int i_1, int i_2) {
      if (i_2 != i_1) {
         int i_4 = i_1;
         int i_5 = i_2;
         if (i_2 > i_1) {
            i_4 = i_2;
            i_5 = i_1;
         }

         while (i_5 != 0) {
            int i_6 = i_4 % i_5;
            i_4 = i_5;
            i_5 = i_6;
         }

         i_1 /= i_4;
         i_2 /= i_4;
         this.field1473 = i_1;
         this.field1474 = i_2;
         this.field1476 = new int[i_1][14];

         for (int i_7 = 0; i_7 < i_1; i_7++) {
            int[] ints_8 = this.field1476[i_7];
            double d_9 = 6.0D + (double)i_7 / (double)i_1;
            int i_11 = (int)Math.floor(1.0D + (d_9 - 7.0D));
            if (i_11 < 0) {
               i_11 = 0;
            }

            int i_12 = (int)Math.ceil(7.0D + d_9);
            if (i_12 > 14) {
               i_12 = 14;
            }

            for (double d_13 = (double)i_2 / (double)i_1; i_11 < i_12; i_11++) {
               double d_15 = 3.141592653589793D * ((double)i_11 - d_9);
               double d_17 = d_13;
               if (d_15 < -1.0E-4D || d_15 > 1.0E-4D) {
                  d_17 = d_13 * (Math.sin(d_15) / d_15);
               }

               d_17 *= 0.54D + 0.46D * Math.cos(((double)i_11 - d_9) * 0.2243994752564138D);
               ints_8[i_11] = (int)Math.floor(d_17 * 65536.0D + 0.5D);
            }
         }

      }
   }

   byte[] method2560(byte[] bytes_1, int i_2) {
      if (this.field1476 != null) {
         int i_3 = (int)((long)bytes_1.length * (long)this.field1474 / (long)this.field1473) + 14;
         int[] ints_4 = new int[i_3];
         int i_5 = 0;
         int i_6 = 0;

         int i_7;
         for (i_7 = 0; i_7 < bytes_1.length; i_7++) {
            byte b_8 = bytes_1[i_7];
            int[] ints_9 = this.field1476[i_6];

            int i_10;
            for (i_10 = 0; i_10 < 14; i_10++) {
               ints_4[i_5 + i_10] += ints_9[i_10] * b_8;
            }

            i_6 += this.field1474;
            i_10 = i_6 / this.field1473;
            i_5 += i_10;
            i_6 -= i_10 * this.field1473;
         }

         bytes_1 = new byte[i_3];

         for (i_7 = 0; i_7 < i_3; i_7++) {
            int i_11 = ints_4[i_7] + 32768 >> 16;
            if (i_11 < -128) {
               bytes_1[i_7] = -128;
            } else if (i_11 > 127) {
               bytes_1[i_7] = 127;
            } else {
               bytes_1[i_7] = (byte)i_11;
            }
         }
      }

      return bytes_1;
   }

   int method2562(int i_1, byte b_2) {
      if (this.field1476 != null) {
         i_1 = (int)((long)this.field1474 * (long)i_1 / (long)this.field1473);
      }

      return i_1;
   }

   int method2567(int i_1, byte b_2) {
      if (this.field1476 != null) {
         i_1 = (int)((long)i_1 * (long)this.field1474 / (long)this.field1473) + 6;
      }

      return i_1;
   }

   public static boolean method2570(int i_0, int i_1) {
      return (i_0 >> 21 & 0x1) != 0;
   }

}
