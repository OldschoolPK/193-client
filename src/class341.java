public class class341 extends class189 {

   int[] field3977 = new int[3];
   int field3950;
   boolean field3964;
   int field3970;
   int field3956;
   int field3959;
   int field3960;
   int field3957;
   boolean field3962;
   int field3945;
   int field3975;
   int field3958;
   int field3966;
   String field3967;
   String field3968;
   String field3949;
   String field3969;
   int field3971;
   int field3961;
   int field3973;
   int field3952;
   String field3963;
   String field3976;
   int field3978;
   String field3979;

   class341(int i_1, boolean bool_2, int i_3, int i_4, int i_5, int i_6, int i_7, boolean bool_8, int i_9, int i_10, int i_11, int i_12, String string_13, String string_14, String string_15, String string_16, int i_17, int i_18, int i_19, int i_20, String string_21, String string_22, int[] ints_23, int i_24, String string_25) {
      this.field3950 = i_1;
      this.field3964 = bool_2;
      this.field3970 = i_3;
      this.field3956 = i_4;
      this.field3959 = i_5;
      this.field3960 = i_6;
      this.field3957 = i_7;
      this.field3962 = bool_8;
      this.field3945 = i_9;
      this.field3975 = i_10;
      this.field3958 = i_11;
      this.field3966 = i_12;
      this.field3967 = string_13;
      this.field3968 = string_14;
      this.field3949 = string_15;
      this.field3969 = string_16;
      this.field3971 = i_17;
      this.field3961 = i_18;
      this.field3973 = i_19;
      this.field3952 = i_20;
      this.field3963 = string_21;
      this.field3976 = string_22;
      this.field3977 = ints_23;
      this.field3978 = i_24;
      this.field3979 = string_25;
   }

   public int method6293(byte b_1) {
      byte b_2 = 39;
      int i_3 = b_2 + class116.method2538(this.field3967, (byte) -51);
      i_3 += class116.method2538(this.field3968, (byte) 38);
      i_3 += class116.method2538(this.field3949, (byte) -87);
      i_3 += class116.method2538(this.field3969, (byte) -115);
      i_3 += class116.method2538(this.field3963, (byte) -38);
      i_3 += class116.method2538(this.field3976, (byte) 32);
      i_3 += class116.method2538(this.field3979, (byte) 34);
      return i_3;
   }

   public void method6296(class310 class310_1, byte b_2) {
      class310_1.method5482(8, (byte) -36);
      class310_1.method5482(this.field3950, (byte) -61);
      class310_1.method5482(this.field3964 ? 1 : 0, (byte) -37);
      class310_1.method5486(this.field3970, (byte) -13);
      class310_1.method5482(this.field3956, (byte) -104);
      class310_1.method5482(this.field3959, (byte) -18);
      class310_1.method5482(this.field3960, (byte) -74);
      class310_1.method5482(this.field3957, (byte) -79);
      class310_1.method5482(this.field3962 ? 1 : 0, (byte) -42);
      class310_1.method5486(this.field3945, (byte) -103);
      class310_1.method5482(this.field3975, (byte) -74);
      class310_1.method5487(this.field3958, (byte) 1);
      class310_1.method5486(this.field3966, (byte) -78);
      class310_1.method5493(this.field3967, (byte) 1);
      class310_1.method5493(this.field3968, (byte) 1);
      class310_1.method5493(this.field3949, (byte) 1);
      class310_1.method5493(this.field3969, (byte) 1);
      class310_1.method5482(this.field3961, (byte) -11);
      class310_1.method5486(this.field3971, (byte) -40);
      class310_1.method5493(this.field3963, (byte) 1);
      class310_1.method5493(this.field3976, (byte) 1);
      class310_1.method5482(this.field3973, (byte) -55);
      class310_1.method5482(this.field3952, (byte) -61);

      for (int i_3 = 0; i_3 < this.field3977.length; i_3++) {
         class310_1.method5510(this.field3977[i_3], (byte) -55);
      }

      class310_1.method5510(this.field3978, (byte) -74);
      class310_1.method5493(this.field3979, (byte) 1);
   }

}
