public class class164 extends class285 {

   static int[] field1998;
   final boolean field1999;

   public class164(boolean bool_1) {
      this.field1999 = bool_1;
   }

   int method3468(class289 class289_1, class289 class289_2, int i_3) {
      return class289_1.field3650 != 0 && class289_2.field3650 != 0 ? (this.field1999 ? class289_1.field3649 - class289_2.field3649 : class289_2.field3649 - class289_1.field3649) : this.method5051(class289_1, class289_2, 1850622744);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3468((class289) object_1, (class289) object_2, -1102022170);
   }

}
