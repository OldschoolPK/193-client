import java.util.LinkedHashMap;

public class class34 {

   final int field227;
   final int field222;
   final int field223;

   class34(int i_1, int i_2, int i_3) {
      this.field227 = i_1;
      this.field222 = i_2;
      this.field223 = i_3;
   }

   public static class264 method383(int i_0, int i_1) {
      class264 class264_2 = (class264) class264.field3395.method3376((long)i_0);
      if (class264_2 != null) {
         return class264_2;
      } else {
         byte[] bytes_3 = class264.field3422.method4160(6, i_0, (short) -16926);
         class264_2 = new class264();
         class264_2.field3387 = i_0;
         if (bytes_3 != null) {
            class264_2.method4611(new class310(bytes_3), (byte) 2);
         }

         class264_2.method4585(-883040110);
         if (class264_2.field3419) {
            class264_2.field3397 = 0;
            class264_2.field3398 = false;
         }

         class264.field3395.method3374(class264_2, (long)i_0);
         return class264_2;
      }
   }

   static final void method380(boolean bool_0, int i_1) {
      if (bool_0) {
         client.field872 = class94.field1200 ? class169.field2019 : class169.field2020;
      } else {
         LinkedHashMap linkedhashmap_2 = class282.field3617.field1058;
         String string_4 = class94.field1196;
         int i_5 = string_4.length();
         int i_6 = 0;

         for (int i_7 = 0; i_7 < i_5; i_7++) {
            i_6 = (i_6 << 5) - i_6 + string_4.charAt(i_7);
         }

         client.field872 = linkedhashmap_2.containsKey(Integer.valueOf(i_6)) ? class169.field2025 : class169.field2021;
      }

   }

   static final void method379(class91 class91_0, int i_1, int i_2, byte b_3) {
      class183.method3618(class91_0.field1164, class91_0.field1161, class91_0.field1162, class91_0.field1160, class91_0.field1163, class91_0.field1163, i_1, i_2, -824309681);
   }

   static final void method381(byte[] bytes_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, class181[] arr_8, byte b_9) {
      int i_11;
      for (int i_10 = 0; i_10 < 8; i_10++) {
         for (i_11 = 0; i_11 < 8; i_11++) {
            if (i_10 + i_2 > 0 && i_10 + i_2 < 103 && i_3 + i_11 > 0 && i_3 + i_11 < 103) {
               arr_8[i_1].field2107[i_10 + i_2][i_11 + i_3] &= ~0x1000000;
            }
         }
      }

      class310 class310_22 = new class310(bytes_0);

      for (i_11 = 0; i_11 < 4; i_11++) {
         for (int i_12 = 0; i_12 < 64; i_12++) {
            for (int i_13 = 0; i_13 < 64; i_13++) {
               if (i_11 == i_4 && i_12 >= i_5 && i_12 < i_5 + 8 && i_13 >= i_6 && i_13 < i_6 + 8) {
                  int i_16 = i_2 + class51.method815(i_12 & 0x7, i_13 & 0x7, i_7, 73723896);
                  int i_19 = i_12 & 0x7;
                  int i_20 = i_13 & 0x7;
                  int i_21 = i_7 & 0x3;
                  int i_18;
                  if (i_21 == 0) {
                     i_18 = i_20;
                  } else if (i_21 == 1) {
                     i_18 = 7 - i_19;
                  } else if (i_21 == 2) {
                     i_18 = 7 - i_20;
                  } else {
                     i_18 = i_19;
                  }

                  class218.method3965(class310_22, i_1, i_16, i_3 + i_18, 0, 0, i_7, 109073521);
               } else {
                  class218.method3965(class310_22, 0, -1, -1, 0, 0, 0, 109073521);
               }
            }
         }
      }

   }

   static final void method382(int i_0) {
      for (class77 class77_1 = (class77) client.field876.method4879(); class77_1 != null; class77_1 = (class77) client.field876.method4884()) {
         if (class77_1.field930 > 0) {
            --class77_1.field930;
         }

         if (class77_1.field930 == 0) {
            if (class77_1.field928 < 0 || class234.method4126(class77_1.field928, class77_1.field935, 1847408099)) {
               class48.method765(class77_1.field924, class77_1.field925, class77_1.field926, class77_1.field927, class77_1.field928, class77_1.field929, class77_1.field935, 1785957932);
               class77_1.method3628();
            }
         } else {
            if (class77_1.field934 > 0) {
               --class77_1.field934;
            }

            if (class77_1.field934 == 0 && class77_1.field926 >= 1 && class77_1.field927 >= 1 && class77_1.field926 <= 102 && class77_1.field927 <= 102 && (class77_1.field932 < 0 || class234.method4126(class77_1.field932, class77_1.field933, 1803839840))) {
               class48.method765(class77_1.field924, class77_1.field925, class77_1.field926, class77_1.field927, class77_1.field932, class77_1.field936, class77_1.field933, -1422019370);
               class77_1.field934 = -1;
               if (class77_1.field932 == class77_1.field928 && class77_1.field928 == -1) {
                  class77_1.method3628();
               } else if (class77_1.field932 == class77_1.field928 && class77_1.field929 == class77_1.field936 && class77_1.field935 == class77_1.field933) {
                  class77_1.method3628();
               }
            }
         }
      }

   }

}
