public class class342 {

   int field3986 = 0;
   boolean field3987 = false;
   class244 field3985;
   String field3983;

   class342(class244 class244_1) {
      this.field3985 = class244_1;
   }

   int method6301(byte b_1) {
      if (this.field3986 < 33) {
         if (!this.field3985.method4182(class44.field326.field330, this.field3983, (byte) 111)) {
            return this.field3986;
         }

         this.field3986 = 33;
      }

      if (this.field3986 == 33) {
         if (this.field3985.method4180(class44.field327.field330, this.field3983, -1088224070) && !this.field3985.method4182(class44.field327.field330, this.field3983, (byte) 49)) {
            return this.field3986;
         }

         this.field3986 = 66;
      }

      if (this.field3986 == 66) {
         if (!this.field3985.method4182(this.field3983, class44.field325.field330, (byte) 21)) {
            return this.field3986;
         }

         this.field3986 = 100;
         this.field3987 = true;
      }

      return this.field3986;
   }

   boolean method6308(int i_1) {
      return this.field3987;
   }

   void method6300(String string_1, int i_2) {
      if (string_1 != null && !string_1.isEmpty()) {
         if (string_1 != this.field3983) {
            this.field3983 = string_1;
            this.field3986 = 0;
            this.field3987 = false;
            this.method6301((byte) 98);
         }
      }
   }

   int method6303(int i_1) {
      return this.field3986;
   }

}
