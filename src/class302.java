public class class302 {

   static class246 field3705;
   public static final class302 field3704 = new class302("p11_full");
   public static final class302 field3699 = new class302("p12_full");
   public static final class302 field3700 = new class302("b12_full");
   public static final class302 field3701 = new class302("verdana_11pt_regular");
   public static final class302 field3703 = new class302("verdana_13pt_regular");
   public static final class302 field3698 = new class302("verdana_15pt_regular");
   String field3706;

   class302(String string_1) {
      this.field3706 = string_1;
   }

   static final void method5318(byte b_0) {
      if (class296.field3677 != null) {
         class296.field3677.method2499(2042587241);
      }

      if (class30.field192 != null) {
         class30.field192.method2499(1942115959);
      }

   }

   static final void method5316(int i_0, byte b_1) {
      if (i_0 >= 0) {
         int i_2 = client.field786[i_0];
         int i_3 = client.field787[i_0];
         int i_4 = client.field844[i_0];
         int i_5 = client.field789[i_0];
         String string_6 = client.field790[i_0];
         String string_7 = client.field791[i_0];
         class183.method3618(i_2, i_3, i_4, i_5, string_6, string_7, class63.field489, class63.field502, 771329208);
      }
   }

   static final void method5317(class226[] arr_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, byte b_9) {
      class331.method5996(i_2, i_3, i_4, i_5);
      class139.method3109();

      for (int i_10 = 0; i_10 < arr_0.length; i_10++) {
         class226 class226_11 = arr_0[i_10];
         if (class226_11 != null && (class226_11.field2670 == i_1 || i_1 == -1412584499 && class226_11 == client.field821)) {
            int i_12;
            if (i_8 == -1) {
               client.field856[client.field824] = class226_11.field2694 + i_6;
               client.field917[client.field824] = i_7 + class226_11.field2600;
               client.field858[client.field824] = class226_11.field2601;
               client.field808[client.field824] = class226_11.field2602;
               i_12 = ++client.field824 - 1;
            } else {
               i_12 = i_8;
            }

            class226_11.field2723 = i_12;
            class226_11.field2596 = client.field655;
            if (!class226_11.field2585 || !class55.method896(class226_11, (byte) 118)) {
               if (class226_11.field2695 > 0) {
                  class279.method4973(class226_11, -625858082);
               }

               int i_13 = class226_11.field2694 + i_6;
               int i_14 = i_7 + class226_11.field2600;
               int i_15 = class226_11.field2652;
               int i_16;
               int i_17;
               if (class226_11 == client.field821) {
                  if (i_1 != -1412584499 && !class226_11.field2671) {
                     class200.field2390 = arr_0;
                     client.field923 = i_6;
                     class144.field1893 = i_7;
                     continue;
                  }

                  if (client.field800 && client.field859) {
                     i_16 = class63.field492;
                     i_17 = class63.field499;
                     i_16 -= client.field823;
                     i_17 -= client.field809;
                     if (i_16 < client.field827) {
                        i_16 = client.field827;
                     }

                     if (i_16 + class226_11.field2601 > client.field827 + client.field832.field2601) {
                        i_16 = client.field827 + client.field832.field2601 - class226_11.field2601;
                     }

                     if (i_17 < client.field716) {
                        i_17 = client.field716;
                     }

                     if (i_17 + class226_11.field2602 > client.field716 + client.field832.field2602) {
                        i_17 = client.field716 + client.field832.field2602 - class226_11.field2602;
                     }

                     i_13 = i_16;
                     i_14 = i_17;
                  }

                  if (!class226_11.field2671) {
                     i_15 = 128;
                  }
               }

               int i_18;
               int i_19;
               int i_21;
               int i_22;
               int i_23;
               int i_31;
               if (class226_11.field2588 == 2) {
                  i_16 = i_2;
                  i_17 = i_3;
                  i_18 = i_4;
                  i_19 = i_5;
               } else if (class226_11.field2588 == 9) {
                  i_31 = i_13;
                  i_21 = i_14;
                  i_22 = i_13 + class226_11.field2601;
                  i_23 = i_14 + class226_11.field2602;
                  if (i_22 < i_13) {
                     i_31 = i_22;
                     i_22 = i_13;
                  }

                  if (i_23 < i_14) {
                     i_21 = i_23;
                     i_23 = i_14;
                  }

                  ++i_22;
                  ++i_23;
                  i_16 = i_31 > i_2 ? i_31 : i_2;
                  i_17 = i_21 > i_3 ? i_21 : i_3;
                  i_18 = i_22 < i_4 ? i_22 : i_4;
                  i_19 = i_23 < i_5 ? i_23 : i_5;
               } else {
                  i_31 = i_13 + class226_11.field2601;
                  i_21 = i_14 + class226_11.field2602;
                  i_16 = i_13 > i_2 ? i_13 : i_2;
                  i_17 = i_14 > i_3 ? i_14 : i_3;
                  i_18 = i_31 < i_4 ? i_31 : i_4;
                  i_19 = i_21 < i_5 ? i_21 : i_5;
               }

               if (!class226_11.field2585 || i_16 < i_18 && i_17 < i_19) {
                  if (class226_11.field2695 != 0) {
                     if (class226_11.field2695 == 1336) {
                        if (client.field782) {
                           i_14 += 15;
                           class26.field137.method5347("Fps:" + class62.field457, i_13 + class226_11.field2601, i_14, 16776960, -1);
                           i_14 += 15;
                           Runtime runtime_39 = Runtime.getRuntime();
                           i_21 = (int)((runtime_39.totalMemory() - runtime_39.freeMemory()) / 1024L);
                           i_22 = 16776960;
                           if (i_21 > 327680 && !client.field910) {
                              i_22 = 16711680;
                           }

                           class26.field137.method5347("Mem:" + i_21 + "k", i_13 + class226_11.field2601, i_14, i_22, -1);
                           i_14 += 15;
                        }
                        continue;
                     }

                     if (class226_11.field2695 == 1337) {
                        client.field797 = i_13;
                        client.field798 = i_14;
                        class97.method2175(i_13, i_14, class226_11.field2601, class226_11.field2602, 1070154541);
                        client.field901[class226_11.field2723] = true;
                        class331.method5996(i_2, i_3, i_4, i_5);
                        continue;
                     }

                     if (class226_11.field2695 == 1338) {
                        class215.method3951(class226_11, i_13, i_14, i_12, 29105444);
                        class331.method5996(i_2, i_3, i_4, i_5);
                        continue;
                     }

                     if (class226_11.field2695 == 1339) {
                        class99.method2235(class226_11, i_13, i_14, i_12, 842543427);
                        class331.method5996(i_2, i_3, i_4, i_5);
                        continue;
                     }

                     if (class226_11.field2695 == 1400) {
                        class31.field198.method6333(i_13, i_14, class226_11.field2601, class226_11.field2602, client.field655, 903679312);
                     }

                     if (class226_11.field2695 == 1401) {
                        class31.field198.method6336(i_13, i_14, class226_11.field2601, class226_11.field2602, 1415246881);
                     }

                     if (class226_11.field2695 == 1402) {
                        class1.field1.method1850(i_13, client.field655, -387686576);
                     }
                  }

                  if (class226_11.field2588 == 0) {
                     if (!class226_11.field2585 && class55.method896(class226_11, (byte) 53) && class226_11 != class262.field3350) {
                        continue;
                     }

                     if (!class226_11.field2585) {
                        if (class226_11.field2608 > class226_11.field2639 - class226_11.field2602) {
                           class226_11.field2608 = class226_11.field2639 - class226_11.field2602;
                        }

                        if (class226_11.field2608 < 0) {
                           class226_11.field2608 = 0;
                        }
                     }

                     method5317(arr_0, class226_11.field2586, i_16, i_17, i_18, i_19, i_13 - class226_11.field2607, i_14 - class226_11.field2608, i_12, (byte) 117);
                     if (class226_11.field2672 != null) {
                        method5317(class226_11.field2672, class226_11.field2586, i_16, i_17, i_18, i_19, i_13 - class226_11.field2607, i_14 - class226_11.field2608, i_12, (byte) 97);
                     }

                     class70 class70_20 = (class70) client.field833.method5968((long)class226_11.field2586);
                     if (class70_20 != null) {
                        class54.method866(class70_20.field563, i_16, i_17, i_18, i_19, i_13, i_14, i_12, (byte) 1);
                     }

                     class331.method5996(i_2, i_3, i_4, i_5);
                     class139.method3109();
                  }

                  if (client.field668 || client.field855[i_12] || client.field860 > 1) {
                     if (class226_11.field2588 == 0 && !class226_11.field2585 && class226_11.field2639 > class226_11.field2602) {
                        class89.method2092(i_13 + class226_11.field2601, i_14, class226_11.field2608, class226_11.field2602, class226_11.field2639, -373616735);
                     }

                     if (class226_11.field2588 != 1) {
                        int i_24;
                        int i_26;
                        int i_27;
                        int i_34;
                        if (class226_11.field2588 == 2) {
                           i_31 = 0;

                           for (i_21 = 0; i_21 < class226_11.field2598; i_21++) {
                              for (i_22 = 0; i_22 < class226_11.field2597; i_22++) {
                                 i_23 = i_13 + i_22 * (class226_11.field2707 + 32);
                                 i_24 = i_14 + i_21 * (class226_11.field2654 + 32);
                                 if (i_31 < 20) {
                                    i_23 += class226_11.field2655[i_31];
                                    i_24 += class226_11.field2656[i_31];
                                 }

                                 if (class226_11.field2704[i_31] <= 0) {
                                    if (class226_11.field2722 != null && i_31 < 20) {
                                       class335 class335_40 = class226_11.method4051(i_31, 1550530577);
                                       if (class335_40 != null) {
                                          class335_40.method6121(i_23, i_24);
                                       } else if (class226.field2603) {
                                          class181.method3609(class226_11, -561403978);
                                       }
                                    }
                                 } else {
                                    boolean bool_47 = false;
                                    boolean bool_41 = false;
                                    i_27 = class226_11.field2704[i_31] - 1;
                                    if (i_23 + 32 > i_2 && i_23 < i_4 && i_24 + 32 > i_3 && i_24 < i_5 || class226_11 == class171.field2027 && i_31 == client.field806) {
                                       class335 class335_36;
                                       if (client.field801 == 1 && i_31 == class103.field1322 && class226_11.field2586 == class26.field140) {
                                          class335_36 = class45.method710(i_27, class226_11.field2711[i_31], 2, 0, 2, false, -1786223770);
                                       } else {
                                          class335_36 = class45.method710(i_27, class226_11.field2711[i_31], 1, 3153952, 2, false, -1786223770);
                                       }

                                       if (class335_36 != null) {
                                          if (class226_11 == class171.field2027 && i_31 == client.field806) {
                                             i_34 = class63.field492 - client.field757;
                                             i_26 = class63.field499 - client.field758;
                                             if (i_34 < 5 && i_34 > -5) {
                                                i_34 = 0;
                                             }

                                             if (i_26 < 5 && i_26 > -5) {
                                                i_26 = 0;
                                             }

                                             if (client.field898 < 5) {
                                                i_34 = 0;
                                                i_26 = 0;
                                             }

                                             class335_36.method6132(i_23 + i_34, i_24 + i_26, 128);
                                             if (i_1 != -1) {
                                                class226 class226_29 = arr_0[i_1 & 0xffff];
                                                int i_30;
                                                if (i_24 + i_26 < class331.field3887 && class226_29.field2608 > 0) {
                                                   i_30 = (class331.field3887 - i_24 - i_26) * client.field850 / 3;
                                                   if (i_30 > client.field850 * 10) {
                                                      i_30 = client.field850 * 10;
                                                   }

                                                   if (i_30 > class226_29.field2608) {
                                                      i_30 = class226_29.field2608;
                                                   }

                                                   class226_29.field2608 -= i_30;
                                                   client.field758 += i_30;
                                                   class181.method3609(class226_29, -1020353984);
                                                }

                                                if (i_24 + i_26 + 32 > class331.field3888 && class226_29.field2608 < class226_29.field2639 - class226_29.field2602) {
                                                   i_30 = (i_26 + i_24 + 32 - class331.field3888) * client.field850 / 3;
                                                   if (i_30 > client.field850 * 10) {
                                                      i_30 = client.field850 * 10;
                                                   }

                                                   if (i_30 > class226_29.field2639 - class226_29.field2602 - class226_29.field2608) {
                                                      i_30 = class226_29.field2639 - class226_29.field2602 - class226_29.field2608;
                                                   }

                                                   class226_29.field2608 += i_30;
                                                   client.field758 -= i_30;
                                                   class181.method3609(class226_29, -1194170991);
                                                }
                                             }
                                          } else if (class226_11 == class88.field1131 && i_31 == client.field881) {
                                             class335_36.method6132(i_23, i_24, 128);
                                          } else {
                                             class335_36.method6121(i_23, i_24);
                                          }
                                       } else {
                                          class181.method3609(class226_11, -1880996813);
                                       }
                                    }
                                 }

                                 ++i_31;
                              }
                           }
                        } else if (class226_11.field2588 == 3) {
                           if (class8.method91(class226_11, -401270973)) {
                              i_31 = class226_11.field2612;
                              if (class226_11 == class262.field3350 && class226_11.field2614 != 0) {
                                 i_31 = class226_11.field2614;
                              }
                           } else {
                              i_31 = class226_11.field2611;
                              if (class226_11 == class262.field3350 && class226_11.field2666 != 0) {
                                 i_31 = class226_11.field2666;
                              }
                           }

                           if (class226_11.field2615) {
                              switch(class226_11.field2616.field3902) {
                              case 1:
                                 class331.method6005(i_13, i_14, class226_11.field2601, class226_11.field2602, class226_11.field2611, class226_11.field2612);
                                 break;
                              case 2:
                                 class331.method6070(i_13, i_14, class226_11.field2601, class226_11.field2602, class226_11.field2611, class226_11.field2612, 255 - (class226_11.field2652 & 0xff), 255 - (class226_11.field2636 & 0xff));
                                 break;
                              default:
                                 if (i_15 == 0) {
                                    class331.method6004(i_13, i_14, class226_11.field2601, class226_11.field2602, i_31);
                                 } else {
                                    class331.method6003(i_13, i_14, class226_11.field2601, class226_11.field2602, i_31, 256 - (i_15 & 0xff));
                                 }
                              }
                           } else if (i_15 == 0) {
                              class331.method6008(i_13, i_14, class226_11.field2601, class226_11.field2602, i_31);
                           } else {
                              class331.method6009(i_13, i_14, class226_11.field2601, class226_11.field2602, i_31, 256 - (i_15 & 0xff));
                           }
                        } else {
                           class305 class305_37;
                           if (class226_11.field2588 == 4) {
                              class305_37 = class226_11.method4050(521072072);
                              if (class305_37 == null) {
                                 if (class226.field2603) {
                                    class181.method3609(class226_11, -1784502629);
                                 }
                              } else {
                                 String string_43 = class226_11.field2647;
                                 if (class8.method91(class226_11, 330581450)) {
                                    i_21 = class226_11.field2612;
                                    if (class226_11 == class262.field3350 && class226_11.field2614 != 0) {
                                       i_21 = class226_11.field2614;
                                    }

                                    if (class226_11.field2648.length() > 0) {
                                       string_43 = class226_11.field2648;
                                    }
                                 } else {
                                    i_21 = class226_11.field2611;
                                    if (class226_11 == class262.field3350 && class226_11.field2666 != 0) {
                                       i_21 = class226_11.field2666;
                                    }
                                 }

                                 if (class226_11.field2585 && class226_11.field2712 != -1) {
                                    class265 class265_46 = class66.method1223(class226_11.field2712, -826165405);
                                    string_43 = class265_46.field3444;
                                    if (string_43 == null) {
                                       string_43 = "null";
                                    }

                                    if ((class265_46.field3455 == 1 || class226_11.field2713 != 1) && class226_11.field2713 != -1) {
                                       string_43 = class23.method222(16748608, (byte) 58) + string_43 + "</col>" + " " + 'x' + class55.method895(class226_11.field2713, 1976029859);
                                    }
                                 }

                                 if (class226_11 == client.field822) {
                                    string_43 = "Please wait...";
                                    i_21 = class226_11.field2611;
                                 }

                                 if (!class226_11.field2585) {
                                    string_43 = class234.method4127(string_43, class226_11, -1574592106);
                                 }

                                 class305_37.method5349(string_43, i_13, i_14, class226_11.field2601, class226_11.field2602, i_21, class226_11.field2693 ? 0 : -1, class226_11.field2650, class226_11.field2651, class226_11.field2663);
                              }
                           } else if (class226_11.field2588 == 5) {
                              class335 class335_38;
                              if (!class226_11.field2585) {
                                 class335_38 = class226_11.method4091(class8.method91(class226_11, 1976231737), (byte) 14);
                                 if (class335_38 != null) {
                                    class335_38.method6121(i_13, i_14);
                                 } else if (class226.field2603) {
                                    class181.method3609(class226_11, -1312279346);
                                 }
                              } else {
                                 if (class226_11.field2712 != -1) {
                                    class335_38 = class45.method710(class226_11.field2712, class226_11.field2713, class226_11.field2625, class226_11.field2626, class226_11.field2644, false, -1786223770);
                                 } else {
                                    class335_38 = class226_11.method4091(false, (byte) 14);
                                 }

                                 if (class335_38 == null) {
                                    if (class226.field2603) {
                                       class181.method3609(class226_11, 751420208);
                                    }
                                 } else {
                                    i_21 = class335_38.field3916;
                                    i_22 = class335_38.field3915;
                                    if (!class226_11.field2624) {
                                       i_23 = class226_11.field2601 * 4096 / i_21;
                                       if (class226_11.field2627 != 0) {
                                          class335_38.method6145(class226_11.field2601 / 2 + i_13, class226_11.field2602 / 2 + i_14, class226_11.field2627, i_23);
                                       } else if (i_15 != 0) {
                                          class335_38.method6159(i_13, i_14, class226_11.field2601, class226_11.field2602, 256 - (i_15 & 0xff));
                                       } else if (i_21 == class226_11.field2601 && i_22 == class226_11.field2602) {
                                          class335_38.method6121(i_13, i_14);
                                       } else {
                                          class335_38.method6128(i_13, i_14, class226_11.field2601, class226_11.field2602);
                                       }
                                    } else {
                                       class331.method5997(i_13, i_14, i_13 + class226_11.field2601, i_14 + class226_11.field2602);
                                       i_23 = (i_21 - 1 + class226_11.field2601) / i_21;
                                       i_24 = (i_22 - 1 + class226_11.field2602) / i_22;

                                       for (i_34 = 0; i_34 < i_23; i_34++) {
                                          for (i_26 = 0; i_26 < i_24; i_26++) {
                                             if (class226_11.field2627 != 0) {
                                                class335_38.method6145(i_21 / 2 + i_13 + i_21 * i_34, i_22 / 2 + i_14 + i_22 * i_26, class226_11.field2627, 4096);
                                             } else if (i_15 != 0) {
                                                class335_38.method6132(i_13 + i_34 * i_21, i_14 + i_26 * i_22, 256 - (i_15 & 0xff));
                                             } else {
                                                class335_38.method6121(i_13 + i_21 * i_34, i_14 + i_26 * i_22);
                                             }
                                          }
                                       }

                                       class331.method5996(i_2, i_3, i_4, i_5);
                                    }
                                 }
                              }
                           } else {
                              class265 class265_35;
                              if (class226_11.field2588 == 6) {
                                 boolean bool_44 = class8.method91(class226_11, -1209775987);
                                 if (bool_44) {
                                    i_21 = class226_11.field2634;
                                 } else {
                                    i_21 = class226_11.field2633;
                                 }

                                 class136 class136_42 = null;
                                 i_23 = 0;
                                 if (class226_11.field2712 != -1) {
                                    class265_35 = class66.method1223(class226_11.field2712, -1226836834);
                                    if (class265_35 != null) {
                                       class265_35 = class265_35.method4634(class226_11.field2713, -1807857103);
                                       class136_42 = class265_35.method4633(1, 893521551);
                                       if (class136_42 != null) {
                                          class136_42.method2946();
                                          i_23 = class136_42.field1894 / 2;
                                       } else {
                                          class181.method3609(class226_11, 858485882);
                                       }
                                    }
                                 } else if (class226_11.field2629 == 5) {
                                    if (class226_11.field2687 == 0) {
                                       class136_42 = client.field909.method4014((class269) null, -1, (class269) null, -1, -344109414);
                                    } else {
                                       class136_42 = class223.field2562.vmethod3305(-1814248977);
                                    }
                                 } else if (i_21 == -1) {
                                    class136_42 = class226_11.method4052((class269) null, -1, bool_44, class223.field2562.field621, -2117213889);
                                    if (class136_42 == null && class226.field2603) {
                                       class181.method3609(class226_11, -626871809);
                                    }
                                 } else {
                                    class269 class269_48 = class260.method4510(i_21, -2085581000);
                                    class136_42 = class226_11.method4052(class269_48, class226_11.field2718, bool_44, class223.field2562.field621, -1982618151);
                                    if (class136_42 == null && class226.field2603) {
                                       class181.method3609(class226_11, 1293002715);
                                    }
                                 }

                                 class139.method3110(class226_11.field2601 / 2 + i_13, class226_11.field2602 / 2 + i_14);
                                 i_24 = class139.field1797[class226_11.field2583] * class226_11.field2640 >> 16;
                                 i_34 = class139.field1780[class226_11.field2583] * class226_11.field2640 >> 16;
                                 if (class136_42 != null) {
                                    if (!class226_11.field2585) {
                                       class136_42.method2938(0, class226_11.field2638, 0, class226_11.field2583, 0, i_24, i_34);
                                    } else {
                                       class136_42.method2946();
                                       if (class226_11.field2710) {
                                          class136_42.method2939(0, class226_11.field2638, class226_11.field2657, class226_11.field2583, class226_11.field2635, i_23 + i_24 + class226_11.field2680, i_34 + class226_11.field2680, class226_11.field2640);
                                       } else {
                                          class136_42.method2938(0, class226_11.field2638, class226_11.field2657, class226_11.field2583, class226_11.field2635, i_23 + i_24 + class226_11.field2680, i_34 + class226_11.field2680);
                                       }
                                    }
                                 }

                                 class139.method3116();
                              } else {
                                 if (class226_11.field2588 == 7) {
                                    class305_37 = class226_11.method4050(521072072);
                                    if (class305_37 == null) {
                                       if (class226.field2603) {
                                          class181.method3609(class226_11, 790141389);
                                       }
                                       continue;
                                    }

                                    i_21 = 0;

                                    for (i_22 = 0; i_22 < class226_11.field2598; i_22++) {
                                       for (i_23 = 0; i_23 < class226_11.field2597; i_23++) {
                                          if (class226_11.field2704[i_21] > 0) {
                                             class265_35 = class66.method1223(class226_11.field2704[i_21] - 1, -736153009);
                                             String string_25;
                                             if (class265_35.field3455 != 1 && class226_11.field2711[i_21] == 1) {
                                                string_25 = class23.method222(16748608, (byte) 109) + class265_35.field3444 + "</col>";
                                             } else {
                                                string_25 = class23.method222(16748608, (byte) 13) + class265_35.field3444 + "</col>" + " " + 'x' + class55.method895(class226_11.field2711[i_21], 2086769002);
                                             }

                                             i_26 = i_13 + i_23 * (class226_11.field2707 + 115);
                                             i_27 = i_22 * (class226_11.field2654 + 12) + i_14;
                                             if (class226_11.field2650 == 0) {
                                                class305_37.method5335(string_25, i_26, i_27, class226_11.field2611, class226_11.field2693 ? 0 : -1);
                                             } else if (class226_11.field2650 == 1) {
                                                class305_37.method5436(string_25, class226_11.field2601 / 2 + i_26, i_27, class226_11.field2611, class226_11.field2693 ? 0 : -1);
                                             } else {
                                                class305_37.method5347(string_25, i_26 + class226_11.field2601 - 1, i_27, class226_11.field2611, class226_11.field2693 ? 0 : -1);
                                             }
                                          }

                                          ++i_21;
                                       }
                                    }
                                 }

                                 if (class226_11.field2588 == 8 && class226_11 == class181.field2092 && client.field799 == client.field751) {
                                    i_31 = 0;
                                    i_21 = 0;
                                    class305 class305_32 = class26.field137;
                                    String string_33 = class226_11.field2647;

                                    String string_45;
                                    for (string_33 = class234.method4127(string_33, class226_11, -1574592106); string_33.length() > 0; i_21 = i_21 + class305_32.field3727 + 1) {
                                       i_34 = string_33.indexOf("<br>");
                                       if (i_34 != -1) {
                                          string_45 = string_33.substring(0, i_34);
                                          string_33 = string_33.substring(i_34 + 4);
                                       } else {
                                          string_45 = string_33;
                                          string_33 = "";
                                       }

                                       i_26 = class305_32.method5340(string_45);
                                       if (i_26 > i_31) {
                                          i_31 = i_26;
                                       }
                                    }

                                    i_31 += 6;
                                    i_21 += 7;
                                    i_34 = i_13 + class226_11.field2601 - 5 - i_31;
                                    i_26 = i_14 + class226_11.field2602 + 5;
                                    if (i_34 < i_13 + 5) {
                                       i_34 = i_13 + 5;
                                    }

                                    if (i_31 + i_34 > i_4) {
                                       i_34 = i_4 - i_31;
                                    }

                                    if (i_21 + i_26 > i_5) {
                                       i_26 = i_5 - i_21;
                                    }

                                    class331.method6004(i_34, i_26, i_31, i_21, 16777120);
                                    class331.method6008(i_34, i_26, i_31, i_21, 0);
                                    string_33 = class226_11.field2647;
                                    i_27 = i_26 + class305_32.field3727 + 2;

                                    for (string_33 = class234.method4127(string_33, class226_11, -1574592106); string_33.length() > 0; i_27 = i_27 + class305_32.field3727 + 1) {
                                       int i_28 = string_33.indexOf("<br>");
                                       if (i_28 != -1) {
                                          string_45 = string_33.substring(0, i_28);
                                          string_33 = string_33.substring(i_28 + 4);
                                       } else {
                                          string_45 = string_33;
                                          string_33 = "";
                                       }

                                       class305_32.method5335(string_45, i_34 + 3, i_27, 0, -1);
                                    }
                                 }

                                 if (class226_11.field2588 == 9) {
                                    if (class226_11.field2620) {
                                       i_31 = i_13;
                                       i_21 = i_14 + class226_11.field2602;
                                       i_22 = i_13 + class226_11.field2601;
                                       i_23 = i_14;
                                    } else {
                                       i_31 = i_13;
                                       i_21 = i_14;
                                       i_22 = i_13 + class226_11.field2601;
                                       i_23 = i_14 + class226_11.field2602;
                                    }

                                    if (class226_11.field2619 == 1) {
                                       class331.method6014(i_31, i_21, i_22, i_23, class226_11.field2611);
                                    } else {
                                       class7.method87(i_31, i_21, i_22, i_23, class226_11.field2611, class226_11.field2619, 175619167);
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

   }

   static final boolean method5315(int i_0) {
      return class137.field1756;
   }

   public static class302[] method5313(byte b_0) {
      return new class302[] {field3704, field3700, field3701, field3703, field3698, field3699};
   }

   static void method5314(int i_0, short s_1) {
      client.field723 = i_0;
   }

}
