public class class110 {

   class126[] field1367 = new class126[10];
   int field1368;
   int field1366;

   class110(class310 class310_1) {
      for (int i_2 = 0; i_2 < 10; i_2++) {
         int i_3 = class310_1.method5661((byte) -19);
         if (i_3 != 0) {
            --class310_1.field3751;
            this.field1367[i_2] = new class126();
            this.field1367[i_2].method2782(class310_1);
         }
      }

      this.field1368 = class310_1.method5729(-611527491);
      this.field1366 = class310_1.method5729(-56993482);
   }

   final byte[] method2383() {
      int i_1 = 0;

      int i_2;
      for (i_2 = 0; i_2 < 10; i_2++) {
         if (this.field1367[i_2] != null && this.field1367[i_2].field1529 + this.field1367[i_2].field1525 > i_1) {
            i_1 = this.field1367[i_2].field1529 + this.field1367[i_2].field1525;
         }
      }

      if (i_1 == 0) {
         return new byte[0];
      } else {
         i_2 = i_1 * 22050 / 1000;
         byte[] bytes_3 = new byte[i_2];

         for (int i_4 = 0; i_4 < 10; i_4++) {
            if (this.field1367[i_4] != null) {
               int i_5 = this.field1367[i_4].field1529 * 22050 / 1000;
               int i_6 = this.field1367[i_4].field1525 * 22050 / 1000;
               int[] ints_7 = this.field1367[i_4].method2785(i_5, this.field1367[i_4].field1529);

               for (int i_8 = 0; i_8 < i_5; i_8++) {
                  int i_9 = (ints_7[i_8] >> 8) + bytes_3[i_8 + i_6];
                  if ((i_9 + 128 & ~0xff) != 0) {
                     i_9 = i_9 >> 31 ^ 0x7f;
                  }

                  bytes_3[i_8 + i_6] = (byte)i_9;
               }
            }
         }

         return bytes_3;
      }
   }

   public class112 method2387() {
      byte[] bytes_1 = this.method2383();
      return new class112(22050, bytes_1, this.field1368 * 22050 / 1000, this.field1366 * 22050 / 1000);
   }

   public final int method2382() {
      int i_1 = 9999999;

      int i_2;
      for (i_2 = 0; i_2 < 10; i_2++) {
         if (this.field1367[i_2] != null && this.field1367[i_2].field1525 / 20 < i_1) {
            i_1 = this.field1367[i_2].field1525 / 20;
         }
      }

      if (this.field1368 < this.field1366 && this.field1368 / 20 < i_1) {
         i_1 = this.field1368 / 20;
      }

      if (i_1 != 9999999 && i_1 != 0) {
         for (i_2 = 0; i_2 < 10; i_2++) {
            if (this.field1367[i_2] != null) {
               this.field1367[i_2].field1525 -= i_1 * 20;
            }
         }

         if (this.field1368 < this.field1366) {
            this.field1368 -= i_1 * 20;
            this.field1366 -= i_1 * 20;
         }

         return i_1;
      } else {
         return 0;
      }
   }

   public static class110 method2385(class244 class244_0, int i_1, int i_2) {
      byte[] bytes_3 = class244_0.method4160(i_1, i_2, (short) -19618);
      return bytes_3 == null ? null : new class110(new class310(bytes_3));
   }

}
