import java.math.BigInteger;

public class class89 {

   static int field1140;
   static final BigInteger field1147 = new BigInteger("80782894952180643741752986186714059433953886149239752893425047584684715842049");
   static final BigInteger field1142 = new BigInteger("7237300117305667488707183861728052766358166655052137727439795191253340127955075499635575104901523446809299097934591732635674173519120047404024393881551683");

   public static void method2093(String[] arr_0, short[] shorts_1, int i_2, int i_3, int i_4) {
      if (i_2 < i_3) {
         int i_5 = (i_3 + i_2) / 2;
         int i_6 = i_2;
         String string_7 = arr_0[i_5];
         arr_0[i_5] = arr_0[i_3];
         arr_0[i_3] = string_7;
         short s_8 = shorts_1[i_5];
         shorts_1[i_5] = shorts_1[i_3];
         shorts_1[i_3] = s_8;

         for (int i_9 = i_2; i_9 < i_3; i_9++) {
            if (string_7 == null || arr_0[i_9] != null && arr_0[i_9].compareTo(string_7) < (i_9 & 0x1)) {
               String string_10 = arr_0[i_9];
               arr_0[i_9] = arr_0[i_6];
               arr_0[i_6] = string_10;
               short s_11 = shorts_1[i_9];
               shorts_1[i_9] = shorts_1[i_6];
               shorts_1[i_6++] = s_11;
            }
         }

         arr_0[i_3] = arr_0[i_6];
         arr_0[i_6] = string_7;
         shorts_1[i_3] = shorts_1[i_6];
         shorts_1[i_6] = s_8;
         method2093(arr_0, shorts_1, i_2, i_6 - 1, -420934042);
         method2093(arr_0, shorts_1, i_6 + 1, i_3, -2040425004);
      }

   }

   public static class334[] method2095(class244 class244_0, String string_1, String string_2, int i_3) {
      int i_4 = class244_0.method4206(string_1, -1970892017);
      int i_5 = class244_0.method4235(i_4, string_2, 1284626629);
      class334[] arr_6;
      if (!class304.method5323(class244_0, i_4, i_5, 975575420)) {
         arr_6 = null;
      } else {
         class334[] arr_8 = new class334[class336.field3923];

         for (int i_9 = 0; i_9 < class336.field3923; i_9++) {
            class334 class334_10 = arr_8[i_9] = new class334();
            class334_10.field3910 = class215.field2511;
            class334_10.field3906 = class336.field3929;
            class334_10.field3907 = class336.field3925[i_9];
            class334_10.field3908 = class336.field3926[i_9];
            class334_10.field3905 = class336.field3927[i_9];
            class334_10.field3909 = class336.field3928[i_9];
            class334_10.field3904 = class336.field3924;
            class334_10.field3903 = class15.field62[i_9];
         }

         class336.field3925 = null;
         class336.field3926 = null;
         class336.field3927 = null;
         class336.field3928 = null;
         class336.field3924 = null;
         class15.field62 = null;
         arr_6 = arr_8;
      }

      return arr_6;
   }

   static void method2094(int i_0) {
      if (class94.field1204) {
         class50.field381 = null;
         class94.field1183 = null;
         class344.field4050 = null;
         class25.field124 = null;
         class224.field2564 = null;
         class224.field2571 = null;
         class94.field1184 = null;
         class215.field2513 = null;
         class68.field543 = null;
         class301.field3697 = null;
         class90.field1157 = null;
         class29.field168 = null;
         class349.field4076 = null;
         class9.field42 = null;
         class1.field1.method1849(1726461364);
         class66.method1220(2, (byte) 105);
         class116.method2536(true, 1599659854);
         class94.field1204 = false;
      }
   }

   static final void method2092(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5) {
      class70.field564[0].method6091(i_0, i_1);
      class70.field564[1].method6091(i_0, i_3 + i_1 - 16);
      class331.method6004(i_0, i_1 + 16, 16, i_3 - 32, client.field711);
      int i_6 = i_3 * (i_3 - 32) / i_4;
      if (i_6 < 8) {
         i_6 = 8;
      }

      int i_7 = (i_3 - 32 - i_6) * i_2 / (i_4 - i_3);
      class331.method6004(i_0, i_7 + i_1 + 16, 16, i_6, client.field712);
      class331.method6023(i_0, i_7 + i_1 + 16, i_6, client.field714);
      class331.method6023(i_0 + 1, i_7 + i_1 + 16, i_6, client.field714);
      class331.method6052(i_0, i_7 + i_1 + 16, 16, client.field714);
      class331.method6052(i_0, i_7 + i_1 + 17, 16, client.field714);
      class331.method6023(i_0 + 15, i_7 + i_1 + 16, i_6, client.field713);
      class331.method6023(i_0 + 14, i_7 + i_1 + 17, i_6 - 1, client.field713);
      class331.method6052(i_0, i_6 + i_7 + i_1 + 15, 16, client.field713);
      class331.method6052(i_0 + 1, i_7 + i_6 + i_1 + 14, 15, client.field713);
   }

}
