public final class class158 {

   class155 field1984;
   class276 field1980 = new class276();
   int field1983;
   int field1981;
   class326 field1982;

   public class158(int i_1, int i_2) {
      this.field1983 = i_1;
      this.field1981 = i_1;

      int i_3;
      for (i_3 = 1; i_3 + i_3 < i_1 && i_3 < i_2; i_3 += i_3) {
         ;
      }

      this.field1982 = new class326(i_3);
   }

   public void method3416(int i_1) {
      for (class157 class157_2 = (class157) this.field1980.method4945(); class157_2 != null; class157_2 = (class157) this.field1980.method4969()) {
         if (class157_2.vmethod3426()) {
            if (class157_2.vmethod3427() == null) {
               class157_2.method3628();
               class157_2.method3622();
               this.field1981 += class157_2.field1979;
            }
         } else if (++class157_2.field2130 > (long)i_1) {
            class159 class159_3 = new class159(class157_2.vmethod3427(), class157_2.field1979);
            this.field1982.method5928(class159_3, class157_2.field2137);
            class276.method4943(class159_3, class157_2);
            class157_2.method3628();
            class157_2.method3622();
         }
      }

   }

   void method3404(long long_1) {
      class157 class157_3 = (class157) this.field1982.method5919(long_1);
      this.method3405(class157_3);
   }

   void method3405(class157 class157_1) {
      if (class157_1 != null) {
         class157_1.method3628();
         class157_1.method3622();
         this.field1981 += class157_1.field1979;
      }

   }

   public Object method3422(long long_1) {
      class157 class157_3 = (class157) this.field1982.method5919(long_1);
      if (class157_3 == null) {
         return null;
      } else {
         Object object_4 = class157_3.vmethod3427();
         if (object_4 == null) {
            class157_3.method3628();
            class157_3.method3622();
            this.field1981 += class157_3.field1979;
            return null;
         } else {
            if (class157_3.vmethod3426()) {
               class156 class156_5 = new class156(object_4, class157_3.field1979);
               this.field1982.method5928(class156_5, class157_3.field2137);
               this.field1980.method4963(class156_5);
               class156_5.field2130 = 0L;
               class157_3.method3628();
               class157_3.method3622();
            } else {
               this.field1980.method4963(class157_3);
               class157_3.field2130 = 0L;
            }

            return object_4;
         }
      }
   }

   public void method3406(Object object_1, long long_2, int i_4) {
      if (i_4 > this.field1983) {
         throw new IllegalStateException();
      } else {
         this.method3404(long_2);
         this.field1981 -= i_4;

         while (this.field1981 < 0) {
            class157 class157_5 = (class157) this.field1980.method4944();
            if (class157_5 == null) {
               throw new RuntimeException("");
            }

            if (!class157_5.vmethod3426()) {
               ;
            }

            this.method3405(class157_5);
            if (this.field1984 != null) {
               this.field1984.method3383(class157_5.vmethod3427());
            }
         }

         class156 class156_6 = new class156(object_1, i_4);
         this.field1982.method5928(class156_6, long_2);
         this.field1980.method4963(class156_6);
         class156_6.field2130 = 0L;
      }
   }

   public void method3408() {
      this.field1980.method4968();
      this.field1982.method5920();
      this.field1981 = this.field1983;
   }

}
