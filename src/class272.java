public class class272 {

   public class189 field3568 = new class189();
   class189 field3569;

   public class272() {
      this.field3568.field2136 = this.field3568;
      this.field3568.field2138 = this.field3568;
   }

   public void method4878(class189 class189_1) {
      if (class189_1.field2138 != null) {
         class189_1.method3628();
      }

      class189_1.field2138 = this.field3568;
      class189_1.field2136 = this.field3568.field2136;
      class189_1.field2138.field2136 = class189_1;
      class189_1.field2136.field2138 = class189_1;
   }

   public class189 method4879() {
      class189 class189_1 = this.field3568.field2136;
      if (class189_1 == this.field3568) {
         this.field3569 = null;
         return null;
      } else {
         this.field3569 = class189_1.field2136;
         return class189_1;
      }
   }

   public class189 method4884() {
      class189 class189_1 = this.field3569;
      if (class189_1 == this.field3568) {
         this.field3569 = null;
         return null;
      } else {
         this.field3569 = class189_1.field2136;
         return class189_1;
      }
   }

   public void method4888(class189 class189_1) {
      if (class189_1.field2138 != null) {
         class189_1.method3628();
      }

      class189_1.field2138 = this.field3568.field2138;
      class189_1.field2136 = this.field3568;
      class189_1.field2138.field2136 = class189_1;
      class189_1.field2136.field2138 = class189_1;
   }

   public class189 method4880() {
      class189 class189_1 = this.field3568.field2136;
      if (class189_1 == this.field3568) {
         return null;
      } else {
         class189_1.method3628();
         return class189_1;
      }
   }

   public class189 method4883() {
      class189 class189_1 = this.field3568.field2138;
      if (class189_1 == this.field3568) {
         this.field3569 = null;
         return null;
      } else {
         this.field3569 = class189_1.field2138;
         return class189_1;
      }
   }

   public class189 method4881() {
      class189 class189_1 = this.field3568.field2138;
      if (class189_1 == this.field3568) {
         return null;
      } else {
         class189_1.method3628();
         return class189_1;
      }
   }

   public class189 method4893() {
      class189 class189_1 = this.field3569;
      if (class189_1 == this.field3568) {
         this.field3569 = null;
         return null;
      } else {
         this.field3569 = class189_1.field2138;
         return class189_1;
      }
   }

   public void method4876() {
      while (true) {
         class189 class189_1 = this.field3568.field2136;
         if (class189_1 == this.field3568) {
            this.field3569 = null;
            return;
         }

         class189_1.method3628();
      }
   }

   public static void method4885(class189 class189_0, class189 class189_1) {
      if (class189_0.field2138 != null) {
         class189_0.method3628();
      }

      class189_0.field2138 = class189_1.field2138;
      class189_0.field2136 = class189_1;
      class189_0.field2138.field2136 = class189_0;
      class189_0.field2136.field2138 = class189_0;
   }

}
