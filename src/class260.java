public class class260 extends class184 {

   static class244 field3334;
   static class154 field3329 = new class154(64);
   boolean field3333 = true;
   public int field3332;
   char field3331;
   public String field3330;

   void method4496(class310 class310_1, int i_2, int i_3) {
      if (i_2 == 1) {
         byte b_5 = class310_1.method5535((byte) 0);
         int i_6 = b_5 & 0xff;
         if (i_6 == 0) {
            throw new IllegalArgumentException("" + Integer.toString(i_6, 16));
         }

         if (i_6 >= 128 && i_6 < 160) {
            char var_7 = class298.field3688[i_6 - 128];
            if (var_7 == 0) {
               var_7 = 63;
            }

            i_6 = var_7;
         }

         char var_4 = (char)i_6;
         this.field3331 = var_4;
      } else if (i_2 == 2) {
         this.field3332 = class310_1.method5507(1943849293);
      } else if (i_2 == 4) {
         this.field3333 = false;
      } else if (i_2 == 5) {
         this.field3330 = class310_1.method5589(-1241829896);
      }

   }

   void method4494(byte b_1) {
   }

   void method4511(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -27);
         if (i_3 == 0) {
            return;
         }

         this.method4496(class310_1, i_3, 1000544289);
      }
   }

   public boolean method4499(byte b_1) {
      return this.field3331 == 115;
   }

   public static String method4509(long long_0) {
      if (long_0 > 0L && long_0 < 6582952005840035281L) {
         if (long_0 % 37L == 0L) {
            return null;
         } else {
            int i_2 = 0;

            for (long long_3 = long_0; long_3 != 0L; long_3 /= 37L) {
               ++i_2;
            }

            StringBuilder stringbuilder_5;
            char var_8;
            for (stringbuilder_5 = new StringBuilder(i_2); long_0 != 0L; stringbuilder_5.append(var_8)) {
               long long_6 = long_0;
               long_0 /= 37L;
               var_8 = class299.field3689[(int)(long_6 - long_0 * 37L)];
               if (var_8 == 95) {
                  int i_9 = stringbuilder_5.length() - 1;
                  stringbuilder_5.setCharAt(i_9, Character.toUpperCase(stringbuilder_5.charAt(i_9)));
                  var_8 = 160;
               }
            }

            stringbuilder_5.reverse();
            stringbuilder_5.setCharAt(0, Character.toUpperCase(stringbuilder_5.charAt(0)));
            return stringbuilder_5.toString();
         }
      } else {
         return null;
      }
   }

   public static class269 method4510(int i_0, int i_1) {
      class269 class269_2 = (class269) class269.field3562.method3376((long)i_0);
      if (class269_2 != null) {
         return class269_2;
      } else {
         byte[] bytes_3 = class269.field3553.method4160(12, i_0, (short) -1673);
         class269_2 = new class269();
         if (bytes_3 != null) {
            class269_2.method4772(new class310(bytes_3), (byte) -72);
         }

         class269_2.method4757(-1714495810);
         class269.field3562.method3374(class269_2, (long)i_0);
         return class269_2;
      }
   }

   public static void method4493(class244 class244_0, int i_1) {
      class261.field3335 = class244_0;
   }

}
