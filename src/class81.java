public class class81 {

   int[] field1033 = new int[256];
   int field1034 = 0;
   int field1040 = 0;
   int field1047 = 0;
   int field1045 = 0;
   int field1046 = 0;
   int field1032 = 0;
   class334[] field1038;
   int[] field1036;
   int[] field1037;
   int[] field1028;
   int[] field1035;
   int[] field1043;
   int[] field1044;
   int[] field1041;
   int[] field1042;

   class81(class334[] arr_1) {
      this.field1038 = arr_1;
      this.method1859(85812293);
   }

   void method1859(int i_1) {
      this.field1036 = new int[256];

      int i_2;
      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1036[i_2] = i_2 * 262144;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1036[i_2 + 64] = i_2 * 1024 + 16711680;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1036[i_2 + 128] = i_2 * 4 + 16776960;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1036[i_2 + 192] = 16777215;
      }

      this.field1037 = new int[256];

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1037[i_2] = i_2 * 1024;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1037[i_2 + 64] = i_2 * 4 + 65280;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1037[i_2 + 128] = i_2 * 262144 + 65535;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1037[i_2 + 192] = 16777215;
      }

      this.field1028 = new int[256];

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1028[i_2] = i_2 * 4;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1028[i_2 + 64] = i_2 * 262144 + 255;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1028[i_2 + 128] = i_2 * 1024 + 16711935;
      }

      for (i_2 = 0; i_2 < 64; i_2++) {
         this.field1028[i_2 + 192] = 16777215;
      }

      this.field1035 = new int[256];
      this.field1045 = 0;
      this.field1043 = new int[32768];
      this.field1044 = new int[32768];
      this.method1873((class334) null, 1524084760);
      this.field1041 = new int[32768];
      this.field1042 = new int[32768];
   }

   final void method1877(int i_1, int[] ints_2, byte b_3) {
      int i_4 = this.field1035.length;

      for (int i_5 = 0; i_5 < i_4; i_5++) {
         if (i_1 > 768) {
            this.field1035[i_5] = this.method1852(this.field1036[i_5], ints_2[i_5], 1024 - i_1, (byte) 27);
         } else if (i_1 > 256) {
            this.field1035[i_5] = ints_2[i_5];
         } else {
            this.field1035[i_5] = this.method1852(ints_2[i_5], this.field1036[i_5], 256 - i_1, (byte) -27);
         }
      }

   }

   final void method1874(int i_1, int i_2) {
      int i_3 = 0;

      for (int i_4 = 1; i_4 < 255; i_4++) {
         int i_5 = (256 - i_4) * this.field1033[i_4] / 256;
         int i_6 = i_5 + i_1;
         int i_7 = 0;
         int i_8 = 128;
         if (i_6 < 0) {
            i_7 = -i_6;
            i_6 = 0;
         }

         if (i_6 + 128 >= class27.field150.field3892) {
            i_8 = class27.field150.field3892 - i_6;
         }

         int i_9 = i_6 + (i_4 + 8) * class27.field150.field3892;
         i_3 += i_7;

         for (int i_10 = i_7; i_10 < i_8; i_10++) {
            int i_11 = this.field1041[i_3++];
            int i_12 = i_9 % class331.field3889;
            if (i_11 != 0 && i_12 >= class331.field3890 && i_12 < class331.field3884) {
               int i_13 = i_11;
               int i_14 = 256 - i_11;
               i_11 = this.field1035[i_11];
               int i_15 = class27.field150.field3891[i_9];
               class27.field150.field3891[i_9++] = ~0xffffff | ((i_11 & 0xff00ff) * i_13 + (i_15 & 0xff00ff) * i_14 & ~0xff00ff) + (i_14 * (i_15 & 0xff00) + i_13 * (i_11 & 0xff00) & 0xff0000) >> 8;
            } else {
               ++i_9;
            }
         }

         i_3 += 128 - i_8;
      }

   }

   final int method1852(int i_1, int i_2, int i_3, byte b_4) {
      int i_5 = 256 - i_3;
      return (i_5 * (i_1 & 0xff00ff) + i_3 * (i_2 & 0xff00ff) & ~0xff00ff) + (i_5 * (i_1 & 0xff00) + i_3 * (i_2 & 0xff00) & 0xff0000) >> 8;
   }

   final void method1851(int i_1, int i_2) {
      this.field1045 += i_1 * 128;
      int i_3;
      if (this.field1045 > this.field1043.length) {
         this.field1045 -= this.field1043.length;
         i_3 = (int)(Math.random() * 12.0D);
         this.method1873(this.field1038[i_3], 1088508901);
      }

      i_3 = 0;
      int i_4 = i_1 * 128;
      int i_5 = (256 - i_1) * 128;

      int i_7;
      for (int i_6 = 0; i_6 < i_5; i_6++) {
         i_7 = this.field1041[i_3 + i_4] - this.field1043[i_3 + this.field1045 & this.field1043.length - 1] * i_1 / 6;
         if (i_7 < 0) {
            i_7 = 0;
         }

         this.field1041[i_3++] = i_7;
      }

      byte b_16 = 10;
      i_7 = 128 - b_16;

      int i_8;
      int i_11;
      for (i_8 = 256 - i_1; i_8 < 256; i_8++) {
         int i_9 = i_8 * 128;

         for (int i_10 = 0; i_10 < 128; i_10++) {
            i_11 = (int)(Math.random() * 100.0D);
            if (i_11 < 50 && i_10 > b_16 && i_10 < i_7) {
               this.field1041[i_10 + i_9] = 255;
            } else {
               this.field1041[i_10 + i_9] = 0;
            }
         }
      }

      if (this.field1040 > 0) {
         this.field1040 -= i_1 * 4;
      }

      if (this.field1047 > 0) {
         this.field1047 -= i_1 * 4;
      }

      if (this.field1040 == 0 && this.field1047 == 0) {
         i_8 = (int)(Math.random() * (double)(2000 / i_1));
         if (i_8 == 0) {
            this.field1040 = 1024;
         }

         if (i_8 == 1) {
            this.field1047 = 1024;
         }
      }

      for (i_8 = 0; i_8 < 256 - i_1; i_8++) {
         this.field1033[i_8] = this.field1033[i_8 + i_1];
      }

      for (i_8 = 256 - i_1; i_8 < 256; i_8++) {
         this.field1033[i_8] = (int)(Math.sin((double)this.field1034 / 14.0D) * 16.0D + Math.sin((double)this.field1034 / 15.0D) * 14.0D + Math.sin((double)this.field1034 / 16.0D) * 12.0D);
         ++this.field1034;
      }

      this.field1046 += i_1;
      i_8 = ((client.field655 & 0x1) + i_1) / 2;
      if (i_8 > 0) {
         short s_17 = 128;
         byte b_18 = 2;
         i_11 = 128 - b_18 - b_18;

         int i_12;
         int i_13;
         int i_14;
         for (i_12 = 0; i_12 < this.field1046 * 100; i_12++) {
            i_13 = (int)(Math.random() * (double)i_11) + b_18;
            i_14 = (int)(Math.random() * (double)s_17) + s_17;
            this.field1041[i_13 + (i_14 << 7)] = 192;
         }

         this.field1046 = 0;

         int i_15;
         for (i_12 = 0; i_12 < 256; i_12++) {
            i_13 = 0;
            i_14 = i_12 * 128;

            for (i_15 = -i_8; i_15 < 128; i_15++) {
               if (i_8 + i_15 < 128) {
                  i_13 += this.field1041[i_8 + i_15 + i_14];
               }

               if (i_15 - (i_8 + 1) >= 0) {
                  i_13 -= this.field1041[i_15 + i_14 - (i_8 + 1)];
               }

               if (i_15 >= 0) {
                  this.field1042[i_14 + i_15] = i_13 / (i_8 * 2 + 1);
               }
            }
         }

         for (i_12 = 0; i_12 < 128; i_12++) {
            i_13 = 0;

            for (i_14 = -i_8; i_14 < 256; i_14++) {
               i_15 = i_14 * 128;
               if (i_8 + i_14 < 256) {
                  i_13 += this.field1042[i_15 + i_12 + i_8 * 128];
               }

               if (i_14 - (i_8 + 1) >= 0) {
                  i_13 -= this.field1042[i_15 + i_12 - (i_8 + 1) * 128];
               }

               if (i_14 >= 0) {
                  this.field1041[i_15 + i_12] = i_13 / (i_8 * 2 + 1);
               }
            }
         }
      }

   }

   final void method1853(int i_1, byte b_2) {
      int i_3 = this.field1035.length;
      if (this.field1040 > 0) {
         this.method1877(this.field1040, this.field1037, (byte) 60);
      } else if (this.field1047 > 0) {
         this.method1877(this.field1047, this.field1028, (byte) 2);
      } else {
         for (int i_4 = 0; i_4 < i_3; i_4++) {
            this.field1035[i_4] = this.field1036[i_4];
         }
      }

      this.method1874(i_1, 1090951606);
   }

   final void method1873(class334 class334_1, int i_2) {
      int i_3;
      for (i_3 = 0; i_3 < this.field1043.length; i_3++) {
         this.field1043[i_3] = 0;
      }

      int i_4;
      for (i_3 = 0; i_3 < 5000; i_3++) {
         i_4 = (int)(Math.random() * 128.0D * 256.0D);
         this.field1043[i_4] = (int)(Math.random() * 256.0D);
      }

      int i_5;
      int i_6;
      for (i_3 = 0; i_3 < 20; i_3++) {
         for (i_4 = 1; i_4 < 255; i_4++) {
            for (i_5 = 1; i_5 < 127; i_5++) {
               i_6 = i_5 + (i_4 << 7);
               this.field1044[i_6] = (this.field1043[i_6 - 128] + this.field1043[i_6 + 1] + this.field1043[i_6 + 128] + this.field1043[i_6 - 1]) / 4;
            }
         }

         int[] ints_9 = this.field1043;
         this.field1043 = this.field1044;
         this.field1044 = ints_9;
      }

      if (class334_1 != null) {
         i_3 = 0;

         for (i_4 = 0; i_4 < class334_1.field3909; i_4++) {
            for (i_5 = 0; i_5 < class334_1.field3905; i_5++) {
               if (class334_1.field3903[i_3++] != 0) {
                  i_6 = i_5 + class334_1.field3907 + 16;
                  int i_7 = i_4 + class334_1.field3908 + 16;
                  int i_8 = i_6 + (i_7 << 7);
                  this.field1043[i_8] = 0;
               }
            }
         }
      }

   }

   void method1850(int i_1, int i_2, int i_3) {
      if (this.field1041 == null) {
         this.method1859(-1184453059);
      }

      if (this.field1032 == 0) {
         this.field1032 = i_2;
      }

      int i_4 = i_2 - this.field1032;
      if (i_4 >= 256) {
         i_4 = 0;
      }

      this.field1032 = i_2;
      if (i_4 > 0) {
         this.method1851(i_4, -1542043970);
      }

      this.method1853(i_1, (byte) 19);
   }

   void method1849(int i_1) {
      this.field1036 = null;
      this.field1037 = null;
      this.field1028 = null;
      this.field1035 = null;
      this.field1043 = null;
      this.field1044 = null;
      this.field1041 = null;
      this.field1042 = null;
      this.field1045 = 0;
      this.field1046 = 0;
   }

   static final void method1875(int i_0) {
      if (client.field733 > 0) {
         class31.method333(1940481989);
      } else {
         client.field698.method4985(1990101755);
         class44.method663(40, -1110059446);
         class70.field562 = client.field694.method2299(-568018594);
         client.field694.method2293(210420137);
      }
   }

   public static int method1876(int i_0, byte b_1) {
      return class192.method3642(class137.field1768[i_0]);
   }

   public static class250 method1864(int i_0, int i_1) {
      class250 class250_2 = (class250) class250.field3231.method3376((long)i_0);
      if (class250_2 != null) {
         return class250_2;
      } else {
         byte[] bytes_3 = class250.field3232.method4160(5, i_0, (short) -20477);
         class250_2 = new class250();
         if (bytes_3 != null) {
            class250_2.method4336(new class310(bytes_3), 1553227533);
         }

         class250.field3231.method3374(class250_2, (long)i_0);
         return class250_2;
      }
   }

}
