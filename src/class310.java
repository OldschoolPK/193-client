import java.math.BigInteger;

public class class310 extends class189 {

   static int[] field3750 = new int[256];
   static long[] field3753;
   public byte[] field3752;
   public int field3751;

   static {
      int i_2;
      for (int i_1 = 0; i_1 < 256; i_1++) {
         int i_0 = i_1;

         for (i_2 = 0; i_2 < 8; i_2++) {
            if ((i_0 & 0x1) == 1) {
               i_0 = i_0 >>> 1 ^ ~0x12477cdf;
            } else {
               i_0 >>>= 1;
            }
         }

         field3750[i_1] = i_0;
      }

      field3753 = new long[256];

      for (i_2 = 0; i_2 < 256; i_2++) {
         long long_4 = (long)i_2;

         for (int i_3 = 0; i_3 < 8; i_3++) {
            if ((long_4 & 0x1L) == 1L) {
               long_4 = long_4 >>> 1 ^ ~0x3693a86a2878f0bdL;
            } else {
               long_4 >>>= 1;
            }
         }

         field3753[i_2] = long_4;
      }

   }

   public class310(int i_1) {
      this.field3752 = class25.method253(i_1, 123621684);
      this.field3751 = 0;
   }

   public class310(byte[] bytes_1) {
      this.field3752 = bytes_1;
      this.field3751 = 0;
   }

   public long method5508(int i_1) {
      long long_2 = (long)this.method5507(1987818959) & 0xffffffffL;
      long long_4 = (long)this.method5507(117003330) & 0xffffffffL;
      return long_4 + (long_2 << 32);
   }

   public int method5483(int i_1) {
      int i_2 = this.field3752[this.field3751] & 0xff;
      return i_2 < 128 ? this.method5661((byte) -35) : this.method5729(-85179856) - 32768;
   }

   public void method5492(String string_1, int i_2) {
      int i_3 = string_1.indexOf(0);
      if (i_3 >= 0) {
         throw new IllegalArgumentException("");
      } else {
         this.field3751 += class45.method675(string_1, 0, string_1.length(), this.field3752, this.field3751, (byte) -67);
         this.field3752[++this.field3751 - 1] = 0;
      }
   }

   public String method5589(int i_1) {
      int i_2 = this.field3751;

      while (this.field3752[++this.field3751 - 1] != 0) {
         ;
      }

      int i_3 = this.field3751 - i_2 - 1;
      return i_3 == 0 ? "" : class179.method3552(this.field3752, i_2, i_3, (byte) 51);
   }

   public int method5507(int i_1) {
      this.field3751 += 4;
      return ((this.field3752[this.field3751 - 3] & 0xff) << 16) + (this.field3752[this.field3751 - 1] & 0xff) + ((this.field3752[this.field3751 - 2] & 0xff) << 8) + ((this.field3752[this.field3751 - 4] & 0xff) << 24);
   }

   public void method5482(int i_1, byte b_2) {
      this.field3752[++this.field3751 - 1] = (byte)i_1;
   }

   public int method5661(byte b_1) {
      return this.field3752[++this.field3751 - 1] & 0xff;
   }

   public void method5490(long long_1) {
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 56));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 48));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 40));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 32));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 24));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 16));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 8));
      this.field3752[++this.field3751 - 1] = (byte)((int)long_1);
   }

   public int method5506(short s_1) {
      this.field3751 += 3;
      return ((this.field3752[this.field3751 - 3] & 0xff) << 16) + (this.field3752[this.field3751 - 1] & 0xff) + ((this.field3752[this.field3751 - 2] & 0xff) << 8);
   }

   public int method5519(short s_1) {
      if (this.field3752[this.field3751] < 0) {
         return this.method5507(-537411802) & 0x7fffffff;
      } else {
         int i_2 = this.method5729(152229164);
         return i_2 == 32767 ? -1 : i_2;
      }
   }

   public int method5520(byte b_1) {
      byte b_2 = this.field3752[++this.field3751 - 1];

      int i_3;
      for (i_3 = 0; b_2 < 0; b_2 = this.field3752[++this.field3751 - 1]) {
         i_3 = (i_3 | b_2 & 0x7f) << 7;
      }

      return i_3 | b_2;
   }

   public byte method5535(byte b_1) {
      return this.field3752[++this.field3751 - 1];
   }

   public void method5510(int i_1, byte b_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 24);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 16);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)i_1;
   }

   public int method5729(int i_1) {
      this.field3751 += 2;
      return (this.field3752[this.field3751 - 1] & 0xff) + ((this.field3752[this.field3751 - 2] & 0xff) << 8);
   }

   public void method5624(byte[] bytes_1, int i_2, int i_3, int i_4) {
      for (int i_5 = i_2; i_5 < i_3 + i_2; i_5++) {
         this.field3752[++this.field3751 - 1] = bytes_1[i_5];
      }

   }

   public void method5514(byte[] bytes_1, int i_2, int i_3, int i_4) {
      for (int i_5 = i_2; i_5 < i_3 + i_2; i_5++) {
         bytes_1[i_5] = this.field3752[++this.field3751 - 1];
      }

   }

   public void method5486(int i_1, byte b_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)i_1;
   }

   public int method5505(short s_1) {
      this.field3751 += 2;
      int i_2 = (this.field3752[this.field3751 - 1] & 0xff) + ((this.field3752[this.field3751 - 2] & 0xff) << 8);
      if (i_2 > 32767) {
         i_2 -= 65536;
      }

      return i_2;
   }

   public String method5734(int i_1) {
      byte b_2 = this.field3752[++this.field3751 - 1];
      if (b_2 != 0) {
         throw new IllegalStateException("");
      } else {
         int i_3 = this.field3751;

         while (this.field3752[++this.field3751 - 1] != 0) {
            ;
         }

         int i_4 = this.field3751 - i_3 - 1;
         return i_4 == 0 ? "" : class179.method3552(this.field3752, i_3, i_4, (byte) 74);
      }
   }

   public void method5501(int i_1, int i_2) {
      if ((i_1 & ~0x7f) != 0) {
         if ((i_1 & ~0x3fff) != 0) {
            if ((i_1 & ~0x1fffff) != 0) {
               if ((i_1 & ~0xfffffff) != 0) {
                  this.method5482(i_1 >>> 28 | 0x80, (byte) -69);
               }

               this.method5482(i_1 >>> 21 | 0x80, (byte) -8);
            }

            this.method5482(i_1 >>> 14 | 0x80, (byte) -109);
         }

         this.method5482(i_1 >>> 7 | 0x80, (byte) -22);
      }

      this.method5482(i_1 & 0x7f, (byte) -65);
   }

   public int method5518(int i_1) {
      return this.field3752[this.field3751] < 0 ? this.method5507(1407870995) & 0x7fffffff : this.method5729(-1212013623);
   }

   public void method5524(int[] ints_1, int i_2, int i_3, int i_4) {
      int i_5 = this.field3751;
      this.field3751 = i_2;
      int i_6 = (i_3 - i_2) / 8;

      for (int i_7 = 0; i_7 < i_6; i_7++) {
         int i_8 = this.method5507(373500285);
         int i_9 = this.method5507(1161239789);
         int i_10 = -957401312;
         int i_11 = -1640531527;

         for (int i_12 = 32; i_12-- > 0; i_8 -= i_9 + (i_9 << 4 ^ i_9 >>> 5) ^ i_10 + ints_1[i_10 & 0x3]) {
            i_9 -= i_8 + (i_8 << 4 ^ i_8 >>> 5) ^ ints_1[i_10 >>> 11 & 0x3] + i_10;
            i_10 -= i_11;
         }

         this.field3751 -= 8;
         this.method5510(i_8, (byte) -84);
         this.method5510(i_9, (byte) -119);
      }

      this.field3751 = i_5;
   }

   public void method5491(boolean bool_1, byte b_2) {
      this.method5482(bool_1 ? 1 : 0, (byte) -114);
   }

   public void method5512(byte b_1) {
      if (this.field3752 != null) {
         class135.method2917(this.field3752, -1819420058);
      }

      this.field3752 = null;
   }

   public void method5487(int i_1, byte b_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 16);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)i_1;
   }

   public int method5515(int i_1) {
      int i_2 = this.field3752[this.field3751] & 0xff;
      return i_2 < 128 ? this.method5661((byte) -20) - 64 : this.method5729(15665877) - 49152;
   }

   public void method5493(String string_1, byte b_2) {
      int i_3 = string_1.indexOf(0);
      if (i_3 >= 0) {
         throw new IllegalArgumentException("");
      } else {
         this.field3752[++this.field3751 - 1] = 0;
         this.field3751 += class45.method675(string_1, 0, string_1.length(), this.field3752, this.field3751, (byte) -57);
         this.field3752[++this.field3751 - 1] = 0;
      }
   }

   public String method5596(int i_1) {
      if (this.field3752[this.field3751] == 0) {
         ++this.field3751;
         return null;
      } else {
         return this.method5589(52658805);
      }
   }

   public int method5499(int i_1) {
      this.field3751 += 2;
      return (this.field3752[this.field3751 - 1] - 128 & 0xff) + ((this.field3752[this.field3751 - 2] & 0xff) << 8);
   }

   public boolean method5720(int i_1) {
      return (this.method5661((byte) 25) & 0x1) == 1;
   }

   public int method5546(int i_1) {
      this.field3751 += 3;
      return ((this.field3752[this.field3751 - 1] & 0xff) << 8) + ((this.field3752[this.field3751 - 3] & 0xff) << 16) + (this.field3752[this.field3751 - 2] & 0xff);
   }

   public void method5697(int i_1, int i_2) {
      if (i_1 >= 0 && i_1 <= 255) {
         this.field3752[this.field3751 - i_1 - 1] = (byte)i_1;
      } else {
         throw new IllegalArgumentException();
      }
   }

   public void method5547(int i_1, byte b_2) {
      this.field3752[++this.field3751 - 1] = (byte)i_1;
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 16);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 24);
   }

   public void method5699(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)i_1;
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
   }

   public void method5538(int i_1, short s_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 + 128);
   }

   public int method5542(int i_1) {
      this.field3751 += 2;
      return ((this.field3752[this.field3751 - 1] & 0xff) << 8) + (this.field3752[this.field3751 - 2] - 128 & 0xff);
   }

   public int method5531(int i_1) {
      return this.field3752[++this.field3751 - 1] - 128 & 0xff;
   }

   public void method5721(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 + 128);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
   }

   public int method5526(int i_1, int i_2) {
      int i_3 = class31.method334(this.field3752, i_1, this.field3751, 1238862903);
      this.method5510(i_3, (byte) -37);
      return i_3;
   }

   public int method5532(int i_1) {
      return 0 - this.field3752[++this.field3751 - 1] & 0xff;
   }

   public void method5678(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 16);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 24);
      this.field3752[++this.field3751 - 1] = (byte)i_1;
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
   }

   public int method5533(byte b_1) {
      return 128 - this.field3752[++this.field3751 - 1] & 0xff;
   }

   public int method5517(int i_1) {
      int i_2 = 0;

      int i_3;
      for (i_3 = this.method5483(772809583); i_3 == 32767; i_3 = this.method5483(772809583)) {
         i_2 += 32767;
      }

      i_2 += i_3;
      return i_2;
   }

   public int method5603(int i_1) {
      this.field3751 += 4;
      return (this.field3752[this.field3751 - 4] & 0xff) + ((this.field3752[this.field3751 - 3] & 0xff) << 8) + ((this.field3752[this.field3751 - 2] & 0xff) << 16) + ((this.field3752[this.field3751 - 1] & 0xff) << 24);
   }

   public int method5543(byte b_1) {
      this.field3751 += 2;
      int i_2 = (this.field3752[this.field3751 - 1] - 128 & 0xff) + ((this.field3752[this.field3751 - 2] & 0xff) << 8);
      if (i_2 > 32767) {
         i_2 -= 65536;
      }

      return i_2;
   }

   public int method5540(int i_1) {
      this.field3751 += 2;
      return ((this.field3752[this.field3751 - 1] & 0xff) << 8) + (this.field3752[this.field3751 - 2] & 0xff);
   }

   public String method5606(byte b_1) {
      byte b_2 = this.field3752[++this.field3751 - 1];
      if (b_2 != 0) {
         throw new IllegalStateException("");
      } else {
         int i_3 = this.method5520((byte) -120);
         if (i_3 + this.field3751 > this.field3752.length) {
            throw new IllegalStateException("");
         } else {
            String string_4 = class39.method591(this.field3752, this.field3751, i_3, 2030729234);
            this.field3751 += i_3;
            return string_4;
         }
      }
   }

   public void method5509(int i_1, byte b_2) {
      if (i_1 >= 0 && i_1 <= 65535) {
         this.field3752[this.field3751 - i_1 - 2] = (byte)(i_1 >> 8);
         this.field3752[this.field3751 - i_1 - 1] = (byte)i_1;
      } else {
         throw new IllegalArgumentException();
      }
   }

   public void method5545(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 16);
      this.field3752[++this.field3751 - 1] = (byte)i_1;
   }

   public int method5544(int i_1) {
      this.field3751 += 2;
      int i_2 = ((this.field3752[this.field3751 - 1] & 0xff) << 8) + (this.field3752[this.field3751 - 2] - 128 & 0xff);
      if (i_2 > 32767) {
         i_2 -= 65536;
      }

      return i_2;
   }

   public void method5500(BigInteger biginteger_1, BigInteger biginteger_2, int i_3) {
      int i_4 = this.field3751;
      this.field3751 = 0;
      byte[] bytes_5 = new byte[i_4];
      this.method5514(bytes_5, 0, i_4, 1075075180);
      BigInteger biginteger_6 = new BigInteger(bytes_5);
      BigInteger biginteger_7 = biginteger_6.modPow(biginteger_1, biginteger_2);
      byte[] bytes_8 = biginteger_7.toByteArray();
      this.field3751 = 0;
      this.method5486(bytes_8.length, (byte) -39);
      this.method5624(bytes_8, 0, bytes_8.length, 1585895405);
   }

   public void method5530(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)(128 - i_1);
   }

   public byte method5605(int i_1) {
      return (byte)(this.field3752[++this.field3751 - 1] - 128);
   }

   public void method5494(CharSequence charsequence_1, int i_2) {
      int i_3 = class165.method3472(charsequence_1, -1024728099);
      this.field3752[++this.field3751 - 1] = 0;
      this.method5501(i_3, 1932364447);
      int i_4 = this.field3751;
      byte[] bytes_6 = this.field3752;
      int i_7 = this.field3751;
      int i_8 = charsequence_1.length();
      int i_9 = i_7;

      for (int i_10 = 0; i_10 < i_8; i_10++) {
         char var_11 = charsequence_1.charAt(i_10);
         if (var_11 <= 127) {
            bytes_6[i_9++] = (byte)var_11;
         } else if (var_11 <= 2047) {
            bytes_6[i_9++] = (byte)(0xc0 | var_11 >> 6);
            bytes_6[i_9++] = (byte)(0x80 | var_11 & 0x3f);
         } else {
            bytes_6[i_9++] = (byte)(0xe0 | var_11 >> 12);
            bytes_6[i_9++] = (byte)(0x80 | var_11 >> 6 & 0x3f);
            bytes_6[i_9++] = (byte)(0x80 | var_11 & 0x3f);
         }
      }

      int i_5 = i_9 - i_7;
      this.field3751 = i_5 + i_4;
   }

   public int method5551(short s_1) {
      this.field3751 += 4;
      return ((this.field3752[this.field3751 - 2] & 0xff) << 24) + ((this.field3752[this.field3751 - 4] & 0xff) << 8) + (this.field3752[this.field3751 - 3] & 0xff) + ((this.field3752[this.field3751 - 1] & 0xff) << 16);
   }

   public int method5552(int i_1) {
      this.field3751 += 4;
      return ((this.field3752[this.field3751 - 1] & 0xff) << 8) + ((this.field3752[this.field3751 - 4] & 0xff) << 16) + (this.field3752[this.field3751 - 2] & 0xff) + ((this.field3752[this.field3751 - 3] & 0xff) << 24);
   }

   public void method5548(int i_1, byte b_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 8);
      this.field3752[++this.field3751 - 1] = (byte)i_1;
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 24);
      this.field3752[++this.field3751 - 1] = (byte)(i_1 >> 16);
   }

   public void method5496(class310 class310_1, int i_2) {
      this.method5624(class310_1.field3752, 0, class310_1.field3751, 1585895405);
   }

   public void method5591(long long_1) {
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 40));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 32));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 24));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 16));
      this.field3752[++this.field3751 - 1] = (byte)((int)(long_1 >> 8));
      this.field3752[++this.field3751 - 1] = (byte)((int)long_1);
   }

   public void method5643(int i_1, int i_2) {
      if (i_1 < 0) {
         throw new IllegalArgumentException();
      } else {
         this.field3752[this.field3751 - i_1 - 4] = (byte)(i_1 >> 24);
         this.field3752[this.field3751 - i_1 - 3] = (byte)(i_1 >> 16);
         this.field3752[this.field3751 - i_1 - 2] = (byte)(i_1 >> 8);
         this.field3752[this.field3751 - i_1 - 1] = (byte)i_1;
      }
   }

   public void method5529(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)(0 - i_1);
   }

   public void method5528(int i_1, int i_2) {
      this.field3752[++this.field3751 - 1] = (byte)(i_1 + 128);
   }

   public void method5725(int[] ints_1, byte b_2) {
      int i_3 = this.field3751 / 8;
      this.field3751 = 0;

      for (int i_4 = 0; i_4 < i_3; i_4++) {
         int i_5 = this.method5507(914396893);
         int i_6 = this.method5507(1729086227);
         int i_7 = 0;
         int i_8 = -1640531527;

         for (int i_9 = 32; i_9-- > 0; i_6 += i_5 + (i_5 << 4 ^ i_5 >>> 5) ^ ints_1[i_7 >>> 11 & 0x3] + i_7) {
            i_5 += i_6 + (i_6 << 4 ^ i_6 >>> 5) ^ i_7 + ints_1[i_7 & 0x3];
            i_7 += i_8;
         }

         this.field3751 -= 8;
         this.method5510(i_5, (byte) -8);
         this.method5510(i_6, (byte) -43);
      }

   }

   public byte method5625(int i_1) {
      return (byte)(128 - this.field3752[++this.field3751 - 1]);
   }

   public boolean method5527(int i_1) {
      this.field3751 -= 4;
      int i_2 = class31.method334(this.field3752, 0, this.field3751, -738122321);
      int i_3 = this.method5507(-1343045949);
      return i_3 == i_2;
   }

   public byte method5621(byte b_1) {
      return (byte)(0 - this.field3752[++this.field3751 - 1]);
   }

   public void method5575(int[] ints_1, int i_2) {
      int i_3 = this.field3751 / 8;
      this.field3751 = 0;

      for (int i_4 = 0; i_4 < i_3; i_4++) {
         int i_5 = this.method5507(-1363096601);
         int i_6 = this.method5507(-436181648);
         int i_7 = -957401312;
         int i_8 = -1640531527;

         for (int i_9 = 32; i_9-- > 0; i_5 -= i_6 + (i_6 << 4 ^ i_6 >>> 5) ^ i_7 + ints_1[i_7 & 0x3]) {
            i_6 -= i_5 + (i_5 << 4 ^ i_5 >>> 5) ^ ints_1[i_7 >>> 11 & 0x3] + i_7;
            i_7 -= i_8;
         }

         this.field3751 -= 8;
         this.method5510(i_5, (byte) -30);
         this.method5510(i_6, (byte) -34);
      }

   }

   public void method5523(int[] ints_1, int i_2, int i_3, int i_4) {
      int i_5 = this.field3751;
      this.field3751 = i_2;
      int i_6 = (i_3 - i_2) / 8;

      for (int i_7 = 0; i_7 < i_6; i_7++) {
         int i_8 = this.method5507(232711658);
         int i_9 = this.method5507(-1324272521);
         int i_10 = 0;
         int i_11 = -1640531527;

         for (int i_12 = 32; i_12-- > 0; i_9 += i_8 + (i_8 << 4 ^ i_8 >>> 5) ^ ints_1[i_10 >>> 11 & 0x3] + i_10) {
            i_8 += i_9 + (i_9 << 4 ^ i_9 >>> 5) ^ i_10 + ints_1[i_10 & 0x3];
            i_10 += i_11;
         }

         this.field3751 -= 8;
         this.method5510(i_8, (byte) -66);
         this.method5510(i_9, (byte) -121);
      }

      this.field3751 = i_5;
   }

   public void method5693(byte[] bytes_1, int i_2, int i_3, byte b_4) {
      for (int i_5 = i_2; i_5 < i_3 + i_2; i_5++) {
         bytes_1[i_5] = (byte)(this.field3752[++this.field3751 - 1] - 128);
      }

   }

   public void method5553(byte[] bytes_1, int i_2, int i_3, byte b_4) {
      for (int i_5 = i_3 + i_2 - 1; i_5 >= i_2; --i_5) {
         bytes_1[i_5] = this.field3752[++this.field3751 - 1];
      }

   }

   public void method5585(int i_1, int i_2) {
      if (i_1 >= 0 && i_1 < 128) {
         this.method5482(i_1, (byte) -103);
      } else if (i_1 >= 0 && i_1 < 32768) {
         this.method5486(i_1 + 32768, (byte) -40);
      } else {
         throw new IllegalArgumentException();
      }
   }

}
