import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class class343 {

   static final class302 field3996;
   static final class302 field3997;
   static final class302 field4044;
   int field4010 = -1;
   int field4011 = -1;
   int field3998 = -1;
   int field4019 = -1;
   int field4016 = -1;
   int field4017 = -1;
   int field4018 = 3;
   int field4021 = 50;
   boolean field4006 = false;
   HashSet field4002 = null;
   int field4041 = -1;
   int field4023 = -1;
   int field4024 = -1;
   int field4025 = -1;
   int field3990 = -1;
   int field4027 = -1;
   boolean field4031 = true;
   HashSet field4028 = new HashSet();
   HashSet field3988 = new HashSet();
   HashSet field4032 = new HashSet();
   HashSet field4035 = new HashSet();
   boolean field4036 = false;
   int field4037 = 0;
   final int[] field4038 = new int[] {1008, 1009, 1010, 1011, 1012};
   HashSet field4008 = new HashSet();
   class222 field4042 = null;
   public boolean field4043 = false;
   int field4046 = -1;
   int field4047 = -1;
   int field4048 = -1;
   class35 field4014;
   class45 field4020;
   float field4012;
   float field4013;
   List field4039;
   Iterator field4040;
   long field4034;
   int field4015;
   int field4009;
   class335 field4033;
   int field4045;
   class334[] field4022;
   class244 field3993;
   class244 field4003;
   class244 field3995;
   class305 field3999;
   HashMap field4000;
   class342 field4007;
   HashMap field4026;
   class35 field4005;
   class35 field3994;
   int field4029;
   int field4030;

   static {
      field3996 = class302.field3701;
      field3997 = class302.field3703;
      field4044 = class302.field3698;
   }

   boolean method6323(byte b_1) {
      return this.field4010 != -1 && this.field4011 != -1;
   }

   public class35 method6500(int i_1, int i_2, int i_3, int i_4) {
      Iterator iterator_5 = this.field4026.values().iterator();

      class35 class35_6;
      do {
         if (!iterator_5.hasNext()) {
            return null;
         }

         class35_6 = (class35) iterator_5.next();
      } while (!class35_6.method387(i_1, i_2, i_3, -262671603));

      return class35_6;
   }

   public class35 method6343(int i_1, short s_2) {
      Iterator iterator_3 = this.field4026.values().iterator();

      class35 class35_4;
      do {
         if (!iterator_3.hasNext()) {
            return null;
         }

         class35_4 = (class35) iterator_3.next();
      } while (class35_4.method392(-1400045157) != i_1);

      return class35_4;
   }

   float method6344(int i_1, int i_2) {
      return i_1 == 25 ? 1.0F : (i_1 == 37 ? 1.5F : (i_1 == 50 ? 2.0F : (i_1 == 75 ? 3.0F : (i_1 == 100 ? 4.0F : 8.0F))));
   }

   void method6330(class35 class35_1, byte b_2) {
      this.field4014 = class35_1;
      this.field4020 = new class45(this.field4022, this.field4000, this.field4003, this.field3995);
      this.field4007.method6300(this.field4014.method394(1739352402), -1761137181);
   }

   void method6329(class35 class35_1, int i_2) {
      if (this.field4014 == null || class35_1 != this.field4014) {
         this.method6330(class35_1, (byte) 16);
         this.method6332(-1, -1, -1, -423770472);
      }
   }

   public int method6348(int i_1) {
      return this.field4014 == null ? -1 : this.field4015 + this.field4014.method398((byte) -127) * 64;
   }

   void method6392(byte b_1) {
      if (class27.field148 != null) {
         this.field4012 = this.field4013;
      } else {
         if (this.field4012 < this.field4013) {
            this.field4012 = Math.min(this.field4013, this.field4012 / 30.0F + this.field4012);
         }

         if (this.field4012 > this.field4013) {
            this.field4012 = Math.max(this.field4013, this.field4012 - this.field4012 / 30.0F);
         }

      }
   }

   void method6367(int i_1) {
      this.field4035.clear();
      this.field4035.addAll(this.field4028);
      this.field4035.addAll(this.field4032);
   }

   void method6332(int i_1, int i_2, int i_3, int i_4) {
      if (this.field4014 != null) {
         int[] ints_5 = this.field4014.method389(i_1, i_2, i_3, 2087440042);
         if (ints_5 == null) {
            ints_5 = this.field4014.method389(this.field4014.method403(-1105888625), this.field4014.method402(-1989874207), this.field4014.method404(527248188), 2017406858);
         }

         this.method6327(ints_5[0] - this.field4014.method398((byte) -41) * 64, ints_5[1] - this.field4014.method400(8844028) * 64, true, (byte) 0);
         this.field4010 = -1;
         this.field4011 = -1;
         this.field4012 = this.method6344(this.field4014.method457((byte) 39), 1716504753);
         this.field4013 = this.field4012;
         this.field4039 = null;
         this.field4040 = null;
         this.field4020.method667((byte) 54);
      }
   }

   public int method6351(int i_1) {
      return this.field3998;
   }

   public int method6349(int i_1) {
      return this.field4014 == null ? -1 : this.field4009 + this.field4014.method400(-1767237970) * 64;
   }

   void method6320(byte b_1) {
      if (this.method6323((byte) 0)) {
         int i_2 = this.field4010 - this.field4015;
         int i_3 = this.field4011 - this.field4009;
         if (i_2 != 0) {
            i_2 /= Math.min(8, Math.abs(i_2));
         }

         if (i_3 != 0) {
            i_3 /= Math.min(8, Math.abs(i_3));
         }

         this.method6327(i_2 + this.field4015, i_3 + this.field4009, true, (byte) 0);
         if (this.field4015 == this.field4010 && this.field4009 == this.field4011) {
            this.field4010 = -1;
            this.field4011 = -1;
         }

      }
   }

   final void method6322(int i_1) {
      this.field4027 = -1;
      this.field3990 = -1;
      this.field4025 = -1;
      this.field4024 = -1;
   }

   public void method6345(int i_1, int i_2, byte b_3) {
      if (this.field4014 != null) {
         this.method6327(i_1 - this.field4014.method398((byte) -125) * 64, i_2 - this.field4014.method400(-601718895) * 64, true, (byte) 0);
         this.field4010 = -1;
         this.field4011 = -1;
      }
   }

   public void method6410(int i_1, int i_2, short s_3) {
      if (this.field4014 != null && this.field4014.method388(i_1, i_2, -1971067580)) {
         this.field4010 = i_1 - this.field4014.method398((byte) -85) * 64;
         this.field4011 = i_2 - this.field4014.method400(-1886663452) * 64;
      }
   }

   void method6425(int i_1, int i_2, boolean bool_3, long long_4) {
      if (this.field4014 != null) {
         int i_6 = (int)((float)this.field4015 + ((float)(i_1 - this.field4016) - (float)this.method6351(-1867921683) * this.field4012 / 2.0F) / this.field4012);
         int i_7 = (int)((float)this.field4009 - ((float)(i_2 - this.field4017) - (float)this.method6352((byte) 0) * this.field4012 / 2.0F) / this.field4012);
         this.field4042 = this.field4014.method384(i_6 + this.field4014.method398((byte) -13) * 64, i_7 + this.field4014.method400(-2053197051) * 64, (byte) -81);
         if (this.field4042 != null && bool_3) {
            boolean bool_8 = client.field775 >= 2;
            int i_10;
            int i_11;
            if (bool_8 && class54.field416[82] && class54.field416[81]) {
               int i_14 = this.field4042.field2546;
               i_10 = this.field4042.field2547;
               i_11 = this.field4042.field2549;
               class196 class196_12 = class68.method1249(class192.field2269, client.field694.field1328, (byte) 1);
               class196_12.field2360.method5538(i_14, (short) -8820);
               class196_12.field2360.method5547(0, (byte) -29);
               class196_12.field2360.method5699(i_10, 303531121);
               class196_12.field2360.method5482(i_11, (byte) -101);
               client.field694.method2295(class196_12, -412242308);
            } else {
               boolean bool_9 = true;
               if (this.field4031) {
                  i_10 = i_1 - this.field4029;
                  i_11 = i_2 - this.field4030;
                  if (long_4 - this.field4034 > 500L || i_10 < -25 || i_10 > 25 || i_11 < -25 || i_11 > 25) {
                     bool_9 = false;
                  }
               }

               if (bool_9) {
                  class196 class196_13 = class68.method1249(class192.field2328, client.field694.field1328, (byte) 1);
                  class196_13.field2360.method5547(this.field4042.method3994((byte) 31), (byte) -122);
                  client.field694.method2295(class196_13, 1806307111);
                  this.field4034 = 0L;
               }
            }
         }
      } else {
         this.field4042 = null;
      }

   }

   final void method6327(int i_1, int i_2, boolean bool_3, byte b_4) {
      this.field4015 = i_1;
      this.field4009 = i_2;
      class298.method5270(255749540);
      if (bool_3) {
         this.method6322(1129738061);
      }

   }

   public int method6352(byte b_1) {
      return this.field4019;
   }

   public class43 method6372(byte b_1) {
      if (this.field4040 == null) {
         return null;
      } else {
         class43 class43_2;
         do {
            if (!this.field4040.hasNext()) {
               return null;
            }

            class43_2 = (class43) this.field4040.next();
         } while (class43_2.vmethod630(2056145560) == -1);

         return class43_2;
      }
   }

   boolean method6470(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, byte b_7) {
      return this.field4033 == null ? true : (this.field4033.field3913 == i_1 && this.field4033.field3911 == i_2 ? (this.field4020.field347 != this.field4045 ? true : (this.field4048 != client.field915 ? true : (i_3 <= 0 && i_4 <= 0 ? i_3 + i_1 < i_5 || i_2 + i_4 < i_6 : true))) : true);
   }

   void method6368(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      byte b_7 = 20;
      int i_8 = i_3 / 2 + i_1;
      int i_9 = i_4 / 2 + i_2 - 18 - b_7;
      class331.method6004(i_1, i_2, i_3, i_4, -16777216);
      class331.method6008(i_8 - 152, i_9, 304, 34, -65536);
      class331.method6004(i_8 - 150, i_9 + 2, i_5 * 3, 30, -65536);
      this.field3999.method5436("Loading...", i_8, i_9 + b_7, -1, -1);
   }

   public void method6370(int i_1, int i_2, class222 class222_3, class222 class222_4, byte b_5) {
      class71 class71_6 = new class71();
      class50 class50_7 = new class50(i_2, class222_3, class222_4);
      class71_6.method1253(new Object[] {class50_7}, -1890318343);
      switch(i_1) {
      case 1008:
         class71_6.method1254(10, -613486134);
         break;
      case 1009:
         class71_6.method1254(11, -1473756258);
         break;
      case 1010:
         class71_6.method1254(12, -1544784760);
         break;
      case 1011:
         class71_6.method1254(13, -447767886);
         break;
      case 1012:
         class71_6.method1254(14, -1854587815);
      }

      class22.method219(class71_6, -1961709464);
   }

   void method6489(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (class27.field148 != null) {
         int i_8 = 512 / (this.field4020.field347 * 2);
         int i_9 = i_3 + 512;
         int i_10 = i_4 + 512;
         float f_11 = 1.0F;
         i_9 = (int)((float)i_9 / f_11);
         i_10 = (int)((float)i_10 / f_11);
         int i_12 = this.method6348(2021044257) - i_5 / 2 - i_8;
         int i_13 = this.method6349(65408) - i_6 / 2 - i_8;
         int i_14 = i_1 - (i_8 + i_12 - this.field4046) * this.field4020.field347;
         int i_15 = i_2 - this.field4020.field347 * (i_8 - (i_13 - this.field4047));
         if (this.method6470(i_9, i_10, i_14, i_15, i_3, i_4, (byte) -90)) {
            if (this.field4033 != null && this.field4033.field3913 == i_9 && this.field4033.field3911 == i_10) {
               Arrays.fill(this.field4033.field3917, 0);
            } else {
               this.field4033 = new class335(i_9, i_10);
            }

            this.field4046 = this.method6348(820750749) - i_5 / 2 - i_8;
            this.field4047 = this.method6349(65408) - i_6 / 2 - i_8;
            this.field4045 = this.field4020.field347;
            class27.field148.method4313(this.field4046, this.field4047, this.field4033, (float)this.field4045 / f_11, (byte) -62);
            this.field4048 = client.field915;
            i_14 = i_1 - (i_8 + i_12 - this.field4046) * this.field4020.field347;
            i_15 = i_2 - this.field4020.field347 * (i_8 - (i_13 - this.field4047));
         }

         class331.method6003(i_1, i_2, i_3, i_4, 0, 128);
         if (f_11 == 1.0F) {
            this.field4033.method6136(i_14, i_15, 192);
         } else {
            this.field4033.method6139(i_14, i_15, (int)((float)i_9 * f_11), (int)(f_11 * (float)i_10), 192);
         }
      }

   }

   public void method6314(class244 class244_1, class244 class244_2, class244 class244_3, class305 class305_4, HashMap hashmap_5, class334[] arr_6, int i_7) {
      this.field4022 = arr_6;
      this.field3993 = class244_1;
      this.field4003 = class244_2;
      this.field3995 = class244_3;
      this.field3999 = class305_4;
      this.field4000 = new HashMap();
      this.field4000.put(class26.field131, hashmap_5.get(field3996));
      this.field4000.put(class26.field134, hashmap_5.get(field3997));
      this.field4000.put(class26.field130, hashmap_5.get(field4044));
      this.field4007 = new class342(class244_1);
      int i_8 = this.field3993.method4206(class44.field328.field330, -1999824780);
      int[] ints_9 = this.field3993.method4171(i_8, 1201667804);
      this.field4026 = new HashMap(ints_9.length);

      for (int i_10 = 0; i_10 < ints_9.length; i_10++) {
         class310 class310_11 = new class310(this.field3993.method4160(i_8, ints_9[i_10], (short) -20124));
         class35 class35_12 = new class35();
         class35_12.method397(class310_11, ints_9[i_10], 1014419421);
         this.field4026.put(class35_12.method394(1918327624), class35_12);
         if (class35_12.method393(2015329854)) {
            this.field3994 = class35_12;
         }
      }

      this.method6329(this.field3994, -320065457);
      this.field4005 = null;
   }

   public void method6333(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      int[] ints_7 = new int[4];
      class331.method5998(ints_7);
      class331.method5996(i_1, i_2, i_3 + i_1, i_2 + i_4);
      class331.method6004(i_1, i_2, i_3, i_4, -16777216);
      int i_8 = this.field4007.method6303(-1669569125);
      if (i_8 < 100) {
         this.method6368(i_1, i_2, i_3, i_4, i_8, 483495875);
      } else {
         if (!this.field4020.method673(-1914860720)) {
            this.field4020.method694(this.field3993, this.field4014.method394(1457397993), client.field659, 1247485995);
            if (!this.field4020.method673(-2078122304)) {
               return;
            }
         }

         if (this.field4002 != null) {
            ++this.field4023;
            if (this.field4023 % this.field4021 == 0) {
               this.field4023 = 0;
               ++this.field4041;
            }

            if (this.field4041 >= this.field4018 && !this.field4006) {
               this.field4002 = null;
            }
         }

         int i_9 = (int)Math.ceil((double)((float)i_3 / this.field4012));
         int i_10 = (int)Math.ceil((double)((float)i_4 / this.field4012));
         this.field4020.method683(this.field4015 - i_9 / 2, this.field4009 - i_10 / 2, i_9 / 2 + this.field4015, i_10 / 2 + this.field4009, i_1, i_2, i_3 + i_1, i_2 + i_4, 1886974954);
         boolean bool_11;
         if (!this.field4036) {
            bool_11 = false;
            if (i_5 - this.field4037 > 100) {
               this.field4037 = i_5;
               bool_11 = true;
            }

            this.field4020.method669(this.field4015 - i_9 / 2, this.field4009 - i_10 / 2, i_9 / 2 + this.field4015, i_10 / 2 + this.field4009, i_1, i_2, i_3 + i_1, i_2 + i_4, this.field4035, this.field4002, this.field4023, this.field4021, bool_11, 1206614213);
         }

         this.method6489(i_1, i_2, i_3, i_4, i_9, i_10, 1968364271);
         bool_11 = client.field775 >= 2;
         if (bool_11 && this.field4043 && this.field4042 != null) {
            this.field3999.method5335("Coord: " + this.field4042, class331.field3890 + 10, class331.field3887 + 20, 16776960, -1);
         }

         this.field3998 = i_9;
         this.field4019 = i_10;
         this.field4016 = i_1;
         this.field4017 = i_2;
         class331.method6012(ints_7);
      }
   }

   public void method6336(int i_1, int i_2, int i_3, int i_4, int i_5) {
      if (this.field4007.method6308(1070920572)) {
         if (!this.field4020.method673(-2094901335)) {
            this.field4020.method694(this.field3993, this.field4014.method394(1069473486), client.field659, 1247485995);
            if (!this.field4020.method673(-2100985610)) {
               return;
            }
         }

         this.field4020.method670(i_1, i_2, i_3, i_4, this.field4002, this.field4023, this.field4021, (byte) -11);
      }
   }

   public void method6318(int i_1) {
      class38.field273.method3416(5);
   }

   public void method6325(int i_1, int i_2, int i_3, boolean bool_4, byte b_5) {
      class35 class35_6 = this.method6500(i_1, i_2, i_3, 1888096920);
      if (class35_6 == null) {
         if (!bool_4) {
            return;
         }

         class35_6 = this.field3994;
      }

      boolean bool_7 = false;
      if (class35_6 != this.field4005 || bool_4) {
         this.field4005 = class35_6;
         this.method6329(class35_6, -320065457);
         bool_7 = true;
      }

      if (bool_7 || bool_4) {
         this.method6332(i_1, i_2, i_3, 1387321592);
      }

   }

   public void method6471(int i_1) {
      this.field4007.method6301((byte) 106);
   }

   public void method6316(int i_1, int i_2, boolean bool_3, int i_4, int i_5, int i_6, int i_7, int i_8) {
      if (this.field4007.method6308(665938450)) {
         this.method6392((byte) -25);
         this.method6320((byte) -4);
         if (bool_3) {
            int i_9 = (int)Math.ceil((double)((float)i_6 / this.field4012));
            int i_10 = (int)Math.ceil((double)((float)i_7 / this.field4012));
            List list_11 = this.field4020.method707(this.field4015 - i_9 / 2 - 1, this.field4009 - i_10 / 2 - 1, i_9 / 2 + this.field4015 + 1, i_10 / 2 + this.field4009 + 1, i_4, i_5, i_6, i_7, i_1, i_2, 279349552);
            HashSet hashset_12 = new HashSet();

            Iterator iterator_13;
            class43 class43_14;
            class71 class71_15;
            class50 class50_16;
            for (iterator_13 = list_11.iterator(); iterator_13.hasNext(); class22.method219(class71_15, -1743691605)) {
               class43_14 = (class43) iterator_13.next();
               hashset_12.add(class43_14);
               class71_15 = new class71();
               class50_16 = new class50(class43_14.vmethod630(571039693), class43_14.field319, class43_14.field321);
               class71_15.method1253(new Object[] {class50_16, Integer.valueOf(i_1), Integer.valueOf(i_2)}, -1890318343);
               if (this.field4008.contains(class43_14)) {
                  class71_15.method1254(17, -528262263);
               } else {
                  class71_15.method1254(15, -1684298252);
               }
            }

            iterator_13 = this.field4008.iterator();

            while (iterator_13.hasNext()) {
               class43_14 = (class43) iterator_13.next();
               if (!hashset_12.contains(class43_14)) {
                  class71_15 = new class71();
                  class50_16 = new class50(class43_14.vmethod630(-359753179), class43_14.field319, class43_14.field321);
                  class71_15.method1253(new Object[] {class50_16, Integer.valueOf(i_1), Integer.valueOf(i_2)}, -1890318343);
                  class71_15.method1254(16, -2116141310);
                  class22.method219(class71_15, -1449073681);
               }
            }

            this.field4008 = hashset_12;
         }
      }
   }

   public void method6501(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (this.field4007.method6308(1933916597)) {
         int i_8 = (int)Math.ceil((double)((float)i_3 / this.field4012));
         int i_9 = (int)Math.ceil((double)((float)i_4 / this.field4012));
         List list_10 = this.field4020.method707(this.field4015 - i_8 / 2 - 1, this.field4009 - i_9 / 2 - 1, i_8 / 2 + this.field4015 + 1, i_9 / 2 + this.field4009 + 1, i_1, i_2, i_3, i_4, i_5, i_6, -837488387);
         if (!list_10.isEmpty()) {
            Iterator iterator_11 = list_10.iterator();

            boolean bool_14;
            do {
               if (!iterator_11.hasNext()) {
                  return;
               }

               class43 class43_12 = (class43) iterator_11.next();
               class252 class252_13 = class163.method3458(class43_12.vmethod630(515555392), -1881879804);
               bool_14 = false;

               for (int i_15 = this.field4038.length - 1; i_15 >= 0; --i_15) {
                  if (class252_13.field3244[i_15] != null) {
                     class38.method564(class252_13.field3244[i_15], class252_13.field3253, this.field4038[i_15], class43_12.vmethod630(762802848), class43_12.field319.method3994((byte) 2), class43_12.field321.method3994((byte) -59), -1721559841);
                     bool_14 = true;
                  }
               }
            } while (!bool_14);

         }
      }
   }

   public void method6422(int i_1, int i_2) {
      class35 class35_3 = this.method6343(i_1, (short) 14391);
      if (class35_3 != null) {
         this.method6329(class35_3, -320065457);
      }

   }

   public int method6477(byte b_1) {
      return (double)this.field4013 == 1.0D ? 25 : ((double)this.field4013 == 1.5D ? 37 : ((double)this.field4013 == 2.0D ? 50 : ((double)this.field4013 == 3.0D ? 75 : ((double)this.field4013 == 4.0D ? 100 : 200))));
   }

   public void method6337(int i_1, int i_2) {
      this.field4013 = this.method6344(i_1, -857374399);
   }

   public boolean method6371(byte b_1) {
      return this.field4007.method6308(-606809269);
   }

   public void method6317(int i_1, int i_2, boolean bool_3, boolean bool_4, int i_5) {
      long long_6 = class298.method5270(255749540);
      this.method6425(i_1, i_2, bool_4, long_6);
      if (!this.method6323((byte) 0) && (bool_4 || bool_3)) {
         if (bool_4) {
            this.field3990 = i_1;
            this.field4027 = i_2;
            this.field4024 = this.field4015;
            this.field4025 = this.field4009;
         }

         if (this.field4024 != -1) {
            int i_8 = i_1 - this.field3990;
            int i_9 = i_2 - this.field4027;
            this.method6327(this.field4024 - (int)((float)i_8 / this.field4013), (int)((float)i_9 / this.field4013) + this.field4025, false, (byte) 0);
         }
      } else {
         this.method6322(1920245593);
      }

      if (bool_4) {
         this.field4034 = long_6;
         this.field4029 = i_1;
         this.field4030 = i_2;
      }

   }

   public class222 method6350(int i_1) {
      return this.field4014 == null ? null : this.field4014.method384(this.method6348(1460850427), this.method6349(65408), (byte) -85);
   }

   public void method6346(int i_1, int i_2, int i_3, int i_4) {
      if (this.field4014 != null) {
         int[] ints_5 = this.field4014.method389(i_1, i_2, i_3, 2139147886);
         if (ints_5 != null) {
            this.method6410(ints_5[0], ints_5[1], (short) 1536);
         }

      }
   }

   public void method6347(int i_1, int i_2, int i_3, int i_4) {
      if (this.field4014 != null) {
         int[] ints_5 = this.field4014.method389(i_1, i_2, i_3, 2030844208);
         if (ints_5 != null) {
            this.method6345(ints_5[0], ints_5[1], (byte) -3);
         }

      }
   }

   public int method6417(byte b_1) {
      return this.field4014 == null ? -1 : this.field4014.method392(-1008113582);
   }

   public void method6354(int i_1) {
      this.field4018 = 3;
   }

   public class35 method6328(int i_1) {
      return this.field4014;
   }

   public void method6356(byte b_1) {
      this.field4021 = 50;
   }

   public void method6353(int i_1, byte b_2) {
      if (i_1 >= 1) {
         this.field4018 = i_1;
      }

   }

   public void method6360(int i_1) {
      this.field4002 = null;
   }

   public void method6391(int i_1, int i_2) {
      if (i_1 >= 1) {
         this.field4021 = i_1;
      }

   }

   public void method6334(int i_1, int i_2) {
      this.field4002 = new HashSet();
      this.field4002.add(Integer.valueOf(i_1));
      this.field4041 = 0;
      this.field4023 = 0;
   }

   public void method6357(boolean bool_1, byte b_2) {
      this.field4006 = bool_1;
   }

   public void method6359(int i_1, int i_2) {
      this.field4002 = new HashSet();
      this.field4041 = 0;
      this.field4023 = 0;

      for (int i_3 = 0; i_3 < class252.field3262; i_3++) {
         if (class163.method3458(i_3, -1861938304) != null && class163.method3458(i_3, -2096731873).field3252 == i_1) {
            this.field4002.add(Integer.valueOf(class163.method3458(i_3, -2025653759).field3248));
         }
      }

   }

   public class43 method6453(int i_1) {
      if (!this.field4007.method6308(1521791887)) {
         return null;
      } else if (!this.field4020.method673(-2121024240)) {
         return null;
      } else {
         HashMap hashmap_2 = this.field4020.method674(-423043153);
         this.field4039 = new LinkedList();
         Iterator iterator_3 = hashmap_2.values().iterator();

         while (iterator_3.hasNext()) {
            List list_4 = (List) iterator_3.next();
            this.field4039.addAll(list_4);
         }

         this.field4040 = this.field4039.iterator();
         return this.method6372((byte) 71);
      }
   }

   public void method6362(boolean bool_1, int i_2) {
      this.field4036 = !bool_1;
   }

   public boolean method6386(int i_1) {
      return !this.field4036;
   }

   public void method6398(int i_1, boolean bool_2, byte b_3) {
      if (!bool_2) {
         this.field4028.add(Integer.valueOf(i_1));
      } else {
         this.field4028.remove(Integer.valueOf(i_1));
      }

      this.method6367(-1662329798);
   }

   public void method6363(int i_1, boolean bool_2, byte b_3) {
      if (!bool_2) {
         this.field3988.add(Integer.valueOf(i_1));
      } else {
         this.field3988.remove(Integer.valueOf(i_1));
      }

      for (int i_4 = 0; i_4 < class252.field3262; i_4++) {
         if (class163.method3458(i_4, -2117776915) != null && class163.method3458(i_4, -1782742805).field3252 == i_1) {
            int i_5 = class163.method3458(i_4, -1902981548).field3248;
            if (!bool_2) {
               this.field4032.add(Integer.valueOf(i_5));
            } else {
               this.field4032.remove(Integer.valueOf(i_5));
            }
         }
      }

      this.method6367(-258395046);
   }

   public boolean method6355(int i_1, int i_2) {
      return !this.field4028.contains(Integer.valueOf(i_1));
   }

   public void method6440(class35 class35_1, class222 class222_2, class222 class222_3, boolean bool_4, int i_5) {
      if (class35_1 != null) {
         if (this.field4014 == null || class35_1 != this.field4014) {
            this.method6330(class35_1, (byte) 16);
         }

         if (!bool_4 && this.field4014.method387(class222_2.field2549, class222_2.field2546, class222_2.field2547, -262671603)) {
            this.method6332(class222_2.field2549, class222_2.field2546, class222_2.field2547, -864501861);
         } else {
            this.method6332(class222_3.field2549, class222_3.field2546, class222_3.field2547, 2007827156);
         }

      }
   }

   public boolean method6373(int i_1, int i_2) {
      return !this.field3988.contains(Integer.valueOf(i_1));
   }

   public class222 method6369(int i_1, class222 class222_2, int i_3) {
      if (!this.field4007.method6308(787617247)) {
         return null;
      } else if (!this.field4020.method673(-1882300252)) {
         return null;
      } else if (!this.field4014.method388(class222_2.field2546, class222_2.field2547, -1471413083)) {
         return null;
      } else {
         HashMap hashmap_4 = this.field4020.method674(-1074161201);
         List list_5 = (List) hashmap_4.get(Integer.valueOf(i_1));
         if (list_5 != null && !list_5.isEmpty()) {
            class43 class43_6 = null;
            int i_7 = -1;
            Iterator iterator_8 = list_5.iterator();

            while (true) {
               class43 class43_9;
               int i_12;
               do {
                  if (!iterator_8.hasNext()) {
                     return class43_6.field321;
                  }

                  class43_9 = (class43) iterator_8.next();
                  int i_10 = class43_9.field321.field2546 - class222_2.field2546;
                  int i_11 = class43_9.field321.field2547 - class222_2.field2547;
                  i_12 = i_11 * i_11 + i_10 * i_10;
                  if (i_12 == 0) {
                     return class43_9.field321;
                  }
               } while (i_12 >= i_7 && class43_6 != null);

               class43_6 = class43_9;
               i_7 = i_12;
            }
         } else {
            return null;
         }
      }
   }

}
