public class class130 extends class144 {

   static int[] field1598 = new int[10000];
   static int[] field1599 = new int[10000];
   static int field1600 = 0;
   static int[] field1581;
   static int[] field1602;
   int field1575 = 0;
   int field1579 = 0;
   byte field1577 = 0;
   boolean field1592 = false;
   int field1563;
   int[] field1567;
   int[] field1565;
   int[] field1586;
   int[] field1583;
   int[] field1568;
   int[] field1569;
   int[] field1584;
   byte[] field1571;
   byte[] field1572;
   byte[] field1564;
   int[] field1594;
   short[] field1574;
   byte[] field1589;
   short[] field1578;
   byte[] field1570;
   short[] field1580;
   short[] field1590;
   short[] field1582;
   int[][] field1576;
   int[][] field1573;
   class138[] field1588;
   class146[] field1587;
   class138[] field1601;
   public short field1566;
   public short field1591;
   int field1593;
   int field1585;
   int field1595;
   int field1596;
   int field1597;

   static {
      field1581 = class139.field1797;
      field1602 = class139.field1780;
   }

   class130() {
   }

   public class130(class130[] arr_1, int i_2) {
      boolean bool_3 = false;
      boolean bool_4 = false;
      boolean bool_5 = false;
      boolean bool_6 = false;
      boolean bool_7 = false;
      boolean bool_8 = false;
      this.field1575 = 0;
      this.field1579 = 0;
      this.field1563 = 0;
      this.field1577 = -1;

      int i_9;
      class130 class130_10;
      for (i_9 = 0; i_9 < i_2; i_9++) {
         class130_10 = arr_1[i_9];
         if (class130_10 != null) {
            this.field1575 += class130_10.field1575;
            this.field1579 += class130_10.field1579;
            this.field1563 += class130_10.field1563;
            if (class130_10.field1572 != null) {
               bool_4 = true;
            } else {
               if (this.field1577 == -1) {
                  this.field1577 = class130_10.field1577;
               }

               if (this.field1577 != class130_10.field1577) {
                  bool_4 = true;
               }
            }

            bool_3 |= class130_10.field1571 != null;
            bool_5 |= class130_10.field1564 != null;
            bool_6 |= class130_10.field1594 != null;
            bool_7 |= class130_10.field1574 != null;
            bool_8 |= class130_10.field1589 != null;
         }
      }

      this.field1567 = new int[this.field1575];
      this.field1565 = new int[this.field1575];
      this.field1586 = new int[this.field1575];
      this.field1583 = new int[this.field1575];
      this.field1568 = new int[this.field1579];
      this.field1569 = new int[this.field1579];
      this.field1584 = new int[this.field1579];
      if (bool_3) {
         this.field1571 = new byte[this.field1579];
      }

      if (bool_4) {
         this.field1572 = new byte[this.field1579];
      }

      if (bool_5) {
         this.field1564 = new byte[this.field1579];
      }

      if (bool_6) {
         this.field1594 = new int[this.field1579];
      }

      if (bool_7) {
         this.field1574 = new short[this.field1579];
      }

      if (bool_8) {
         this.field1589 = new byte[this.field1579];
      }

      this.field1578 = new short[this.field1579];
      if (this.field1563 > 0) {
         this.field1570 = new byte[this.field1563];
         this.field1580 = new short[this.field1563];
         this.field1590 = new short[this.field1563];
         this.field1582 = new short[this.field1563];
      }

      this.field1575 = 0;
      this.field1579 = 0;
      this.field1563 = 0;

      for (i_9 = 0; i_9 < i_2; i_9++) {
         class130_10 = arr_1[i_9];
         if (class130_10 != null) {
            int i_11;
            for (i_11 = 0; i_11 < class130_10.field1579; i_11++) {
               if (bool_3 && class130_10.field1571 != null) {
                  this.field1571[this.field1579] = class130_10.field1571[i_11];
               }

               if (bool_4) {
                  if (class130_10.field1572 != null) {
                     this.field1572[this.field1579] = class130_10.field1572[i_11];
                  } else {
                     this.field1572[this.field1579] = class130_10.field1577;
                  }
               }

               if (bool_5 && class130_10.field1564 != null) {
                  this.field1564[this.field1579] = class130_10.field1564[i_11];
               }

               if (bool_6 && class130_10.field1594 != null) {
                  this.field1594[this.field1579] = class130_10.field1594[i_11];
               }

               if (bool_7) {
                  if (class130_10.field1574 != null) {
                     this.field1574[this.field1579] = class130_10.field1574[i_11];
                  } else {
                     this.field1574[this.field1579] = -1;
                  }
               }

               if (bool_8) {
                  if (class130_10.field1589 != null && class130_10.field1589[i_11] != -1) {
                     this.field1589[this.field1579] = (byte)(this.field1563 + class130_10.field1589[i_11]);
                  } else {
                     this.field1589[this.field1579] = -1;
                  }
               }

               this.field1578[this.field1579] = class130_10.field1578[i_11];
               this.field1568[this.field1579] = this.method2828(class130_10, class130_10.field1568[i_11]);
               this.field1569[this.field1579] = this.method2828(class130_10, class130_10.field1569[i_11]);
               this.field1584[this.field1579] = this.method2828(class130_10, class130_10.field1584[i_11]);
               ++this.field1579;
            }

            for (i_11 = 0; i_11 < class130_10.field1563; i_11++) {
               byte b_12 = this.field1570[this.field1563] = class130_10.field1570[i_11];
               if (b_12 == 0) {
                  this.field1580[this.field1563] = (short)this.method2828(class130_10, class130_10.field1580[i_11]);
                  this.field1590[this.field1563] = (short)this.method2828(class130_10, class130_10.field1590[i_11]);
                  this.field1582[this.field1563] = (short)this.method2828(class130_10, class130_10.field1582[i_11]);
               }

               ++this.field1563;
            }
         }
      }

   }

   class130(byte[] bytes_1) {
      if (bytes_1[bytes_1.length - 1] == -1 && bytes_1[bytes_1.length - 2] == -1) {
         this.method2825(bytes_1);
      } else {
         this.method2826(bytes_1);
      }

   }

   public class130(class130 class130_1, boolean bool_2, boolean bool_3, boolean bool_4, boolean bool_5) {
      this.field1575 = class130_1.field1575;
      this.field1579 = class130_1.field1579;
      this.field1563 = class130_1.field1563;
      int i_6;
      if (bool_2) {
         this.field1567 = class130_1.field1567;
         this.field1565 = class130_1.field1565;
         this.field1586 = class130_1.field1586;
      } else {
         this.field1567 = new int[this.field1575];
         this.field1565 = new int[this.field1575];
         this.field1586 = new int[this.field1575];

         for (i_6 = 0; i_6 < this.field1575; i_6++) {
            this.field1567[i_6] = class130_1.field1567[i_6];
            this.field1565[i_6] = class130_1.field1565[i_6];
            this.field1586[i_6] = class130_1.field1586[i_6];
         }
      }

      if (bool_3) {
         this.field1578 = class130_1.field1578;
      } else {
         this.field1578 = new short[this.field1579];

         for (i_6 = 0; i_6 < this.field1579; i_6++) {
            this.field1578[i_6] = class130_1.field1578[i_6];
         }
      }

      if (!bool_4 && class130_1.field1574 != null) {
         this.field1574 = new short[this.field1579];

         for (i_6 = 0; i_6 < this.field1579; i_6++) {
            this.field1574[i_6] = class130_1.field1574[i_6];
         }
      } else {
         this.field1574 = class130_1.field1574;
      }

      this.field1564 = class130_1.field1564;
      this.field1568 = class130_1.field1568;
      this.field1569 = class130_1.field1569;
      this.field1584 = class130_1.field1584;
      this.field1571 = class130_1.field1571;
      this.field1572 = class130_1.field1572;
      this.field1589 = class130_1.field1589;
      this.field1577 = class130_1.field1577;
      this.field1570 = class130_1.field1570;
      this.field1580 = class130_1.field1580;
      this.field1590 = class130_1.field1590;
      this.field1582 = class130_1.field1582;
      this.field1583 = class130_1.field1583;
      this.field1594 = class130_1.field1594;
      this.field1576 = class130_1.field1576;
      this.field1573 = class130_1.field1573;
      this.field1588 = class130_1.field1588;
      this.field1587 = class130_1.field1587;
      this.field1601 = class130_1.field1601;
      this.field1566 = class130_1.field1566;
      this.field1591 = class130_1.field1591;
   }

   void method2871() {
      if (!this.field1592) {
         super.field1894 = 0;
         this.field1593 = 0;
         this.field1585 = 999999;
         this.field1595 = -999999;
         this.field1596 = -99999;
         this.field1597 = 99999;

         for (int i_1 = 0; i_1 < this.field1575; i_1++) {
            int i_2 = this.field1567[i_1];
            int i_3 = this.field1565[i_1];
            int i_4 = this.field1586[i_1];
            if (i_2 < this.field1585) {
               this.field1585 = i_2;
            }

            if (i_2 > this.field1595) {
               this.field1595 = i_2;
            }

            if (i_4 < this.field1597) {
               this.field1597 = i_4;
            }

            if (i_4 > this.field1596) {
               this.field1596 = i_4;
            }

            if (-i_3 > super.field1894) {
               super.field1894 = -i_3;
            }

            if (i_3 > this.field1593) {
               this.field1593 = i_3;
            }
         }

         this.field1592 = true;
      }
   }

   public void method2841() {
      if (this.field1588 == null) {
         this.field1588 = new class138[this.field1575];

         int i_1;
         for (i_1 = 0; i_1 < this.field1575; i_1++) {
            this.field1588[i_1] = new class138();
         }

         for (i_1 = 0; i_1 < this.field1579; i_1++) {
            int i_2 = this.field1568[i_1];
            int i_3 = this.field1569[i_1];
            int i_4 = this.field1584[i_1];
            int i_5 = this.field1567[i_3] - this.field1567[i_2];
            int i_6 = this.field1565[i_3] - this.field1565[i_2];
            int i_7 = this.field1586[i_3] - this.field1586[i_2];
            int i_8 = this.field1567[i_4] - this.field1567[i_2];
            int i_9 = this.field1565[i_4] - this.field1565[i_2];
            int i_10 = this.field1586[i_4] - this.field1586[i_2];
            int i_11 = i_6 * i_10 - i_9 * i_7;
            int i_12 = i_7 * i_8 - i_10 * i_5;

            int i_13;
            for (i_13 = i_5 * i_9 - i_8 * i_6; i_11 > 8192 || i_12 > 8192 || i_13 > 8192 || i_11 < -8192 || i_12 < -8192 || i_13 < -8192; i_13 >>= 1) {
               i_11 >>= 1;
               i_12 >>= 1;
            }

            int i_14 = (int)Math.sqrt((double)(i_11 * i_11 + i_12 * i_12 + i_13 * i_13));
            if (i_14 <= 0) {
               i_14 = 1;
            }

            i_11 = i_11 * 256 / i_14;
            i_12 = i_12 * 256 / i_14;
            i_13 = i_13 * 256 / i_14;
            byte b_15;
            if (this.field1571 == null) {
               b_15 = 0;
            } else {
               b_15 = this.field1571[i_1];
            }

            if (b_15 == 0) {
               class138 class138_16 = this.field1588[i_2];
               class138_16.field1772 += i_11;
               class138_16.field1771 += i_12;
               class138_16.field1775 += i_13;
               ++class138_16.field1773;
               class138_16 = this.field1588[i_3];
               class138_16.field1772 += i_11;
               class138_16.field1771 += i_12;
               class138_16.field1775 += i_13;
               ++class138_16.field1773;
               class138_16 = this.field1588[i_4];
               class138_16.field1772 += i_11;
               class138_16.field1771 += i_12;
               class138_16.field1775 += i_13;
               ++class138_16.field1773;
            } else if (b_15 == 1) {
               if (this.field1587 == null) {
                  this.field1587 = new class146[this.field1579];
               }

               class146 class146_17 = this.field1587[i_1] = new class146();
               class146_17.field1905 = i_11;
               class146_17.field1906 = i_12;
               class146_17.field1907 = i_13;
            }
         }

      }
   }

   void method2897() {
      this.field1588 = null;
      this.field1601 = null;
      this.field1587 = null;
      this.field1592 = false;
   }

   public class130 method2829() {
      class130 class130_1 = new class130();
      if (this.field1571 != null) {
         class130_1.field1571 = new byte[this.field1579];

         for (int i_2 = 0; i_2 < this.field1579; i_2++) {
            class130_1.field1571[i_2] = this.field1571[i_2];
         }
      }

      class130_1.field1575 = this.field1575;
      class130_1.field1579 = this.field1579;
      class130_1.field1563 = this.field1563;
      class130_1.field1567 = this.field1567;
      class130_1.field1565 = this.field1565;
      class130_1.field1586 = this.field1586;
      class130_1.field1568 = this.field1568;
      class130_1.field1569 = this.field1569;
      class130_1.field1584 = this.field1584;
      class130_1.field1572 = this.field1572;
      class130_1.field1564 = this.field1564;
      class130_1.field1589 = this.field1589;
      class130_1.field1578 = this.field1578;
      class130_1.field1574 = this.field1574;
      class130_1.field1577 = this.field1577;
      class130_1.field1570 = this.field1570;
      class130_1.field1580 = this.field1580;
      class130_1.field1590 = this.field1590;
      class130_1.field1582 = this.field1582;
      class130_1.field1583 = this.field1583;
      class130_1.field1594 = this.field1594;
      class130_1.field1576 = this.field1576;
      class130_1.field1573 = this.field1573;
      class130_1.field1588 = this.field1588;
      class130_1.field1587 = this.field1587;
      class130_1.field1566 = this.field1566;
      class130_1.field1591 = this.field1591;
      return class130_1;
   }

   public void method2837(short s_1, short s_2) {
      for (int i_3 = 0; i_3 < this.field1579; i_3++) {
         if (this.field1578[i_3] == s_1) {
            this.field1578[i_3] = s_2;
         }
      }

   }

   public void method2840(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < this.field1575; i_4++) {
         this.field1567[i_4] = this.field1567[i_4] * i_1 / 128;
         this.field1565[i_4] = i_2 * this.field1565[i_4] / 128;
         this.field1586[i_4] = i_3 * this.field1586[i_4] / 128;
      }

      this.method2897();
   }

   void method2826(byte[] bytes_1) {
      boolean bool_2 = false;
      boolean bool_3 = false;
      class310 class310_4 = new class310(bytes_1);
      class310 class310_5 = new class310(bytes_1);
      class310 class310_6 = new class310(bytes_1);
      class310 class310_7 = new class310(bytes_1);
      class310 class310_8 = new class310(bytes_1);
      class310_4.field3751 = bytes_1.length - 18;
      int i_9 = class310_4.method5729(921261730);
      int i_10 = class310_4.method5729(-515246874);
      int i_11 = class310_4.method5661((byte) 0);
      int i_12 = class310_4.method5661((byte) 13);
      int i_13 = class310_4.method5661((byte) 62);
      int i_14 = class310_4.method5661((byte) -10);
      int i_15 = class310_4.method5661((byte) -81);
      int i_16 = class310_4.method5661((byte) 56);
      int i_17 = class310_4.method5729(275874104);
      int i_18 = class310_4.method5729(-2043486492);
      int i_19 = class310_4.method5729(-660344774);
      int i_20 = class310_4.method5729(-694065040);
      byte b_21 = 0;
      int i_45 = b_21 + i_9;
      int i_23 = i_45;
      i_45 += i_10;
      int i_24 = i_45;
      if (i_13 == 255) {
         i_45 += i_10;
      }

      int i_25 = i_45;
      if (i_15 == 1) {
         i_45 += i_10;
      }

      int i_26 = i_45;
      if (i_12 == 1) {
         i_45 += i_10;
      }

      int i_27 = i_45;
      if (i_16 == 1) {
         i_45 += i_9;
      }

      int i_28 = i_45;
      if (i_14 == 1) {
         i_45 += i_10;
      }

      int i_29 = i_45;
      i_45 += i_20;
      int i_30 = i_45;
      i_45 += i_10 * 2;
      int i_31 = i_45;
      i_45 += i_11 * 6;
      int i_32 = i_45;
      i_45 += i_17;
      int i_33 = i_45;
      i_45 += i_18;
      int i_10000 = i_45 + i_19;
      this.field1575 = i_9;
      this.field1579 = i_10;
      this.field1563 = i_11;
      this.field1567 = new int[i_9];
      this.field1565 = new int[i_9];
      this.field1586 = new int[i_9];
      this.field1568 = new int[i_10];
      this.field1569 = new int[i_10];
      this.field1584 = new int[i_10];
      if (i_11 > 0) {
         this.field1570 = new byte[i_11];
         this.field1580 = new short[i_11];
         this.field1590 = new short[i_11];
         this.field1582 = new short[i_11];
      }

      if (i_16 == 1) {
         this.field1583 = new int[i_9];
      }

      if (i_12 == 1) {
         this.field1571 = new byte[i_10];
         this.field1589 = new byte[i_10];
         this.field1574 = new short[i_10];
      }

      if (i_13 == 255) {
         this.field1572 = new byte[i_10];
      } else {
         this.field1577 = (byte)i_13;
      }

      if (i_14 == 1) {
         this.field1564 = new byte[i_10];
      }

      if (i_15 == 1) {
         this.field1594 = new int[i_10];
      }

      this.field1578 = new short[i_10];
      class310_4.field3751 = b_21;
      class310_5.field3751 = i_32;
      class310_6.field3751 = i_33;
      class310_7.field3751 = i_45;
      class310_8.field3751 = i_27;
      int i_35 = 0;
      int i_36 = 0;
      int i_37 = 0;

      int i_38;
      int i_39;
      int i_40;
      int i_41;
      int i_42;
      for (i_38 = 0; i_38 < i_9; i_38++) {
         i_39 = class310_4.method5661((byte) 28);
         i_40 = 0;
         if ((i_39 & 0x1) != 0) {
            i_40 = class310_5.method5515(-202250543);
         }

         i_41 = 0;
         if ((i_39 & 0x2) != 0) {
            i_41 = class310_6.method5515(1334112051);
         }

         i_42 = 0;
         if ((i_39 & 0x4) != 0) {
            i_42 = class310_7.method5515(1311677714);
         }

         this.field1567[i_38] = i_35 + i_40;
         this.field1565[i_38] = i_36 + i_41;
         this.field1586[i_38] = i_37 + i_42;
         i_35 = this.field1567[i_38];
         i_36 = this.field1565[i_38];
         i_37 = this.field1586[i_38];
         if (i_16 == 1) {
            this.field1583[i_38] = class310_8.method5661((byte) 2);
         }
      }

      class310_4.field3751 = i_30;
      class310_5.field3751 = i_26;
      class310_6.field3751 = i_24;
      class310_7.field3751 = i_28;
      class310_8.field3751 = i_25;

      for (i_38 = 0; i_38 < i_10; i_38++) {
         this.field1578[i_38] = (short)class310_4.method5729(-553469297);
         if (i_12 == 1) {
            i_39 = class310_5.method5661((byte) 70);
            if ((i_39 & 0x1) == 1) {
               this.field1571[i_38] = 1;
               bool_2 = true;
            } else {
               this.field1571[i_38] = 0;
            }

            if ((i_39 & 0x2) == 2) {
               this.field1589[i_38] = (byte)(i_39 >> 2);
               this.field1574[i_38] = this.field1578[i_38];
               this.field1578[i_38] = 127;
               if (this.field1574[i_38] != -1) {
                  bool_3 = true;
               }
            } else {
               this.field1589[i_38] = -1;
               this.field1574[i_38] = -1;
            }
         }

         if (i_13 == 255) {
            this.field1572[i_38] = class310_6.method5535((byte) 0);
         }

         if (i_14 == 1) {
            this.field1564[i_38] = class310_7.method5535((byte) 0);
         }

         if (i_15 == 1) {
            this.field1594[i_38] = class310_8.method5661((byte) 21);
         }
      }

      class310_4.field3751 = i_29;
      class310_5.field3751 = i_23;
      i_38 = 0;
      i_39 = 0;
      i_40 = 0;
      i_41 = 0;

      int i_43;
      int i_44;
      for (i_42 = 0; i_42 < i_10; i_42++) {
         i_43 = class310_5.method5661((byte) -59);
         if (i_43 == 1) {
            i_38 = class310_4.method5515(135194137) + i_41;
            i_39 = class310_4.method5515(1109195122) + i_38;
            i_40 = class310_4.method5515(67147951) + i_39;
            i_41 = i_40;
            this.field1568[i_42] = i_38;
            this.field1569[i_42] = i_39;
            this.field1584[i_42] = i_40;
         }

         if (i_43 == 2) {
            i_39 = i_40;
            i_40 = class310_4.method5515(1192120162) + i_41;
            i_41 = i_40;
            this.field1568[i_42] = i_38;
            this.field1569[i_42] = i_39;
            this.field1584[i_42] = i_40;
         }

         if (i_43 == 3) {
            i_38 = i_40;
            i_40 = class310_4.method5515(858467806) + i_41;
            i_41 = i_40;
            this.field1568[i_42] = i_38;
            this.field1569[i_42] = i_39;
            this.field1584[i_42] = i_40;
         }

         if (i_43 == 4) {
            i_44 = i_38;
            i_38 = i_39;
            i_39 = i_44;
            i_40 = class310_4.method5515(1307348431) + i_41;
            i_41 = i_40;
            this.field1568[i_42] = i_38;
            this.field1569[i_42] = i_44;
            this.field1584[i_42] = i_40;
         }
      }

      class310_4.field3751 = i_31;

      for (i_42 = 0; i_42 < i_11; i_42++) {
         this.field1570[i_42] = 0;
         this.field1580[i_42] = (short)class310_4.method5729(1420564214);
         this.field1590[i_42] = (short)class310_4.method5729(1673974316);
         this.field1582[i_42] = (short)class310_4.method5729(-648568685);
      }

      if (this.field1589 != null) {
         boolean bool_46 = false;

         for (i_43 = 0; i_43 < i_10; i_43++) {
            i_44 = this.field1589[i_43] & 0xff;
            if (i_44 != 255) {
               if (this.field1568[i_43] == (this.field1580[i_44] & 0xffff) && this.field1569[i_43] == (this.field1590[i_44] & 0xffff) && this.field1584[i_43] == (this.field1582[i_44] & 0xffff)) {
                  this.field1589[i_43] = -1;
               } else {
                  bool_46 = true;
               }
            }
         }

         if (!bool_46) {
            this.field1589 = null;
         }
      }

      if (!bool_3) {
         this.field1574 = null;
      }

      if (!bool_2) {
         this.field1571 = null;
      }

   }

   public void method2833(short s_1, short s_2) {
      if (this.field1574 != null) {
         for (int i_3 = 0; i_3 < this.field1579; i_3++) {
            if (this.field1574[i_3] == s_1) {
               this.field1574[i_3] = s_2;
            }
         }

      }
   }

   public final class136 method2876(int i_1, int i_2, int i_3, int i_4, int i_5) {
      this.method2841();
      int i_6 = (int)Math.sqrt((double)(i_5 * i_5 + i_3 * i_3 + i_4 * i_4));
      int i_7 = i_6 * i_2 >> 8;
      class136 class136_8 = new class136();
      class136_8.field1696 = new int[this.field1579];
      class136_8.field1684 = new int[this.field1579];
      class136_8.field1698 = new int[this.field1579];
      if (this.field1563 > 0 && this.field1589 != null) {
         int[] ints_9 = new int[this.field1563];

         int i_10;
         for (i_10 = 0; i_10 < this.field1579; i_10++) {
            if (this.field1589[i_10] != -1) {
               ++ints_9[this.field1589[i_10] & 0xff];
            }
         }

         class136_8.field1704 = 0;

         for (i_10 = 0; i_10 < this.field1563; i_10++) {
            if (ints_9[i_10] > 0 && this.field1570[i_10] == 0) {
               ++class136_8.field1704;
            }
         }

         class136_8.field1723 = new int[class136_8.field1704];
         class136_8.field1745 = new int[class136_8.field1704];
         class136_8.field1707 = new int[class136_8.field1704];
         i_10 = 0;

         int i_11;
         for (i_11 = 0; i_11 < this.field1563; i_11++) {
            if (ints_9[i_11] > 0 && this.field1570[i_11] == 0) {
               class136_8.field1723[i_10] = this.field1580[i_11] & 0xffff;
               class136_8.field1745[i_10] = this.field1590[i_11] & 0xffff;
               class136_8.field1707[i_10] = this.field1582[i_11] & 0xffff;
               ints_9[i_11] = i_10++;
            } else {
               ints_9[i_11] = -1;
            }
         }

         class136_8.field1701 = new byte[this.field1579];

         for (i_11 = 0; i_11 < this.field1579; i_11++) {
            if (this.field1589[i_11] != -1) {
               class136_8.field1701[i_11] = (byte)ints_9[this.field1589[i_11] & 0xff];
            } else {
               class136_8.field1701[i_11] = -1;
            }
         }
      }

      for (int i_16 = 0; i_16 < this.field1579; i_16++) {
         byte b_17;
         if (this.field1571 == null) {
            b_17 = 0;
         } else {
            b_17 = this.field1571[i_16];
         }

         byte b_18;
         if (this.field1564 == null) {
            b_18 = 0;
         } else {
            b_18 = this.field1564[i_16];
         }

         short s_12;
         if (this.field1574 == null) {
            s_12 = -1;
         } else {
            s_12 = this.field1574[i_16];
         }

         if (b_18 == -2) {
            b_17 = 3;
         }

         if (b_18 == -1) {
            b_17 = 2;
         }

         class138 class138_13;
         int i_14;
         class146 class146_19;
         if (s_12 == -1) {
            if (b_17 != 0) {
               if (b_17 == 1) {
                  class146_19 = this.field1587[i_16];
                  i_14 = (i_4 * class146_19.field1906 + i_5 * class146_19.field1907 + i_3 * class146_19.field1905) / (i_7 / 2 + i_7) + i_1;
                  class136_8.field1696[i_16] = method2846(this.field1578[i_16] & 0xffff, i_14);
                  class136_8.field1698[i_16] = -1;
               } else if (b_17 == 3) {
                  class136_8.field1696[i_16] = 128;
                  class136_8.field1698[i_16] = -1;
               } else {
                  class136_8.field1698[i_16] = -2;
               }
            } else {
               int i_15 = this.field1578[i_16] & 0xffff;
               if (this.field1601 != null && this.field1601[this.field1568[i_16]] != null) {
                  class138_13 = this.field1601[this.field1568[i_16]];
               } else {
                  class138_13 = this.field1588[this.field1568[i_16]];
               }

               i_14 = (i_4 * class138_13.field1771 + i_5 * class138_13.field1775 + i_3 * class138_13.field1772) / (i_7 * class138_13.field1773) + i_1;
               class136_8.field1696[i_16] = method2846(i_15, i_14);
               if (this.field1601 != null && this.field1601[this.field1569[i_16]] != null) {
                  class138_13 = this.field1601[this.field1569[i_16]];
               } else {
                  class138_13 = this.field1588[this.field1569[i_16]];
               }

               i_14 = (i_4 * class138_13.field1771 + i_5 * class138_13.field1775 + i_3 * class138_13.field1772) / (i_7 * class138_13.field1773) + i_1;
               class136_8.field1684[i_16] = method2846(i_15, i_14);
               if (this.field1601 != null && this.field1601[this.field1584[i_16]] != null) {
                  class138_13 = this.field1601[this.field1584[i_16]];
               } else {
                  class138_13 = this.field1588[this.field1584[i_16]];
               }

               i_14 = (i_4 * class138_13.field1771 + i_5 * class138_13.field1775 + i_3 * class138_13.field1772) / (i_7 * class138_13.field1773) + i_1;
               class136_8.field1698[i_16] = method2846(i_15, i_14);
            }
         } else if (b_17 != 0) {
            if (b_17 == 1) {
               class146_19 = this.field1587[i_16];
               i_14 = (i_4 * class146_19.field1906 + i_5 * class146_19.field1907 + i_3 * class146_19.field1905) / (i_7 / 2 + i_7) + i_1;
               class136_8.field1696[i_16] = method2880(i_14);
               class136_8.field1698[i_16] = -1;
            } else {
               class136_8.field1698[i_16] = -2;
            }
         } else {
            if (this.field1601 != null && this.field1601[this.field1568[i_16]] != null) {
               class138_13 = this.field1601[this.field1568[i_16]];
            } else {
               class138_13 = this.field1588[this.field1568[i_16]];
            }

            i_14 = (i_4 * class138_13.field1771 + i_5 * class138_13.field1775 + i_3 * class138_13.field1772) / (i_7 * class138_13.field1773) + i_1;
            class136_8.field1696[i_16] = method2880(i_14);
            if (this.field1601 != null && this.field1601[this.field1569[i_16]] != null) {
               class138_13 = this.field1601[this.field1569[i_16]];
            } else {
               class138_13 = this.field1588[this.field1569[i_16]];
            }

            i_14 = (i_4 * class138_13.field1771 + i_5 * class138_13.field1775 + i_3 * class138_13.field1772) / (i_7 * class138_13.field1773) + i_1;
            class136_8.field1684[i_16] = method2880(i_14);
            if (this.field1601 != null && this.field1601[this.field1584[i_16]] != null) {
               class138_13 = this.field1601[this.field1584[i_16]];
            } else {
               class138_13 = this.field1588[this.field1584[i_16]];
            }

            i_14 = (i_4 * class138_13.field1771 + i_5 * class138_13.field1775 + i_3 * class138_13.field1772) / (i_7 * class138_13.field1773) + i_1;
            class136_8.field1698[i_16] = method2880(i_14);
         }
      }

      this.method2831();
      class136_8.field1688 = this.field1575;
      class136_8.field1716 = this.field1567;
      class136_8.field1690 = this.field1565;
      class136_8.field1691 = this.field1586;
      class136_8.field1748 = this.field1579;
      class136_8.field1740 = this.field1568;
      class136_8.field1694 = this.field1569;
      class136_8.field1739 = this.field1584;
      class136_8.field1699 = this.field1572;
      class136_8.field1693 = this.field1564;
      class136_8.field1722 = this.field1577;
      class136_8.field1708 = this.field1576;
      class136_8.field1709 = this.field1573;
      class136_8.field1746 = this.field1574;
      return class136_8;
   }

   void method2825(byte[] bytes_1) {
      class310 class310_2 = new class310(bytes_1);
      class310 class310_3 = new class310(bytes_1);
      class310 class310_4 = new class310(bytes_1);
      class310 class310_5 = new class310(bytes_1);
      class310 class310_6 = new class310(bytes_1);
      class310 class310_7 = new class310(bytes_1);
      class310 class310_8 = new class310(bytes_1);
      class310_2.field3751 = bytes_1.length - 23;
      int i_9 = class310_2.method5729(-799137562);
      int i_10 = class310_2.method5729(83945246);
      int i_11 = class310_2.method5661((byte) 72);
      int i_12 = class310_2.method5661((byte) 63);
      int i_13 = class310_2.method5661((byte) -11);
      int i_14 = class310_2.method5661((byte) 57);
      int i_15 = class310_2.method5661((byte) 5);
      int i_16 = class310_2.method5661((byte) -73);
      int i_17 = class310_2.method5661((byte) -3);
      int i_18 = class310_2.method5729(1790476112);
      int i_19 = class310_2.method5729(1292716927);
      int i_20 = class310_2.method5729(519003468);
      int i_21 = class310_2.method5729(-252178526);
      int i_22 = class310_2.method5729(851610877);
      int i_23 = 0;
      int i_24 = 0;
      int i_25 = 0;
      int i_26;
      if (i_11 > 0) {
         this.field1570 = new byte[i_11];
         class310_2.field3751 = 0;

         for (i_26 = 0; i_26 < i_11; i_26++) {
            byte b_27 = this.field1570[i_26] = class310_2.method5535((byte) 0);
            if (b_27 == 0) {
               ++i_23;
            }

            if (b_27 >= 1 && b_27 <= 3) {
               ++i_24;
            }

            if (b_27 == 2) {
               ++i_25;
            }
         }
      }

      i_26 = i_11 + i_9;
      int i_28 = i_26;
      if (i_12 == 1) {
         i_26 += i_10;
      }

      int i_29 = i_26;
      i_26 += i_10;
      int i_30 = i_26;
      if (i_13 == 255) {
         i_26 += i_10;
      }

      int i_31 = i_26;
      if (i_15 == 1) {
         i_26 += i_10;
      }

      int i_32 = i_26;
      if (i_17 == 1) {
         i_26 += i_9;
      }

      int i_33 = i_26;
      if (i_14 == 1) {
         i_26 += i_10;
      }

      int i_34 = i_26;
      i_26 += i_21;
      int i_35 = i_26;
      if (i_16 == 1) {
         i_26 += i_10 * 2;
      }

      int i_36 = i_26;
      i_26 += i_22;
      int i_37 = i_26;
      i_26 += i_10 * 2;
      int i_38 = i_26;
      i_26 += i_18;
      int i_39 = i_26;
      i_26 += i_19;
      int i_40 = i_26;
      i_26 += i_20;
      int i_41 = i_26;
      i_26 += i_23 * 6;
      int i_42 = i_26;
      i_26 += i_24 * 6;
      int i_43 = i_26;
      i_26 += i_24 * 6;
      int i_44 = i_26;
      i_26 += i_24 * 2;
      int i_45 = i_26;
      i_26 += i_24;
      int i_46 = i_26;
      i_26 += i_24 * 2 + i_25 * 2;
      this.field1575 = i_9;
      this.field1579 = i_10;
      this.field1563 = i_11;
      this.field1567 = new int[i_9];
      this.field1565 = new int[i_9];
      this.field1586 = new int[i_9];
      this.field1568 = new int[i_10];
      this.field1569 = new int[i_10];
      this.field1584 = new int[i_10];
      if (i_17 == 1) {
         this.field1583 = new int[i_9];
      }

      if (i_12 == 1) {
         this.field1571 = new byte[i_10];
      }

      if (i_13 == 255) {
         this.field1572 = new byte[i_10];
      } else {
         this.field1577 = (byte)i_13;
      }

      if (i_14 == 1) {
         this.field1564 = new byte[i_10];
      }

      if (i_15 == 1) {
         this.field1594 = new int[i_10];
      }

      if (i_16 == 1) {
         this.field1574 = new short[i_10];
      }

      if (i_16 == 1 && i_11 > 0) {
         this.field1589 = new byte[i_10];
      }

      this.field1578 = new short[i_10];
      if (i_11 > 0) {
         this.field1580 = new short[i_11];
         this.field1590 = new short[i_11];
         this.field1582 = new short[i_11];
      }

      class310_2.field3751 = i_11;
      class310_3.field3751 = i_38;
      class310_4.field3751 = i_39;
      class310_5.field3751 = i_40;
      class310_6.field3751 = i_32;
      int i_48 = 0;
      int i_49 = 0;
      int i_50 = 0;

      int i_51;
      int i_52;
      int i_53;
      int i_54;
      int i_55;
      for (i_51 = 0; i_51 < i_9; i_51++) {
         i_52 = class310_2.method5661((byte) 9);
         i_53 = 0;
         if ((i_52 & 0x1) != 0) {
            i_53 = class310_3.method5515(-69436900);
         }

         i_54 = 0;
         if ((i_52 & 0x2) != 0) {
            i_54 = class310_4.method5515(1846969170);
         }

         i_55 = 0;
         if ((i_52 & 0x4) != 0) {
            i_55 = class310_5.method5515(789929058);
         }

         this.field1567[i_51] = i_48 + i_53;
         this.field1565[i_51] = i_49 + i_54;
         this.field1586[i_51] = i_50 + i_55;
         i_48 = this.field1567[i_51];
         i_49 = this.field1565[i_51];
         i_50 = this.field1586[i_51];
         if (i_17 == 1) {
            this.field1583[i_51] = class310_6.method5661((byte) 25);
         }
      }

      class310_2.field3751 = i_37;
      class310_3.field3751 = i_28;
      class310_4.field3751 = i_30;
      class310_5.field3751 = i_33;
      class310_6.field3751 = i_31;
      class310_7.field3751 = i_35;
      class310_8.field3751 = i_36;

      for (i_51 = 0; i_51 < i_10; i_51++) {
         this.field1578[i_51] = (short)class310_2.method5729(862571765);
         if (i_12 == 1) {
            this.field1571[i_51] = class310_3.method5535((byte) 0);
         }

         if (i_13 == 255) {
            this.field1572[i_51] = class310_4.method5535((byte) 0);
         }

         if (i_14 == 1) {
            this.field1564[i_51] = class310_5.method5535((byte) 0);
         }

         if (i_15 == 1) {
            this.field1594[i_51] = class310_6.method5661((byte) -103);
         }

         if (i_16 == 1) {
            this.field1574[i_51] = (short)(class310_7.method5729(143467704) - 1);
         }

         if (this.field1589 != null && this.field1574[i_51] != -1) {
            this.field1589[i_51] = (byte)(class310_8.method5661((byte) -63) - 1);
         }
      }

      class310_2.field3751 = i_34;
      class310_3.field3751 = i_29;
      i_51 = 0;
      i_52 = 0;
      i_53 = 0;
      i_54 = 0;

      int i_56;
      for (i_55 = 0; i_55 < i_10; i_55++) {
         i_56 = class310_3.method5661((byte) 1);
         if (i_56 == 1) {
            i_51 = class310_2.method5515(374891464) + i_54;
            i_52 = class310_2.method5515(175072692) + i_51;
            i_53 = class310_2.method5515(1336716053) + i_52;
            i_54 = i_53;
            this.field1568[i_55] = i_51;
            this.field1569[i_55] = i_52;
            this.field1584[i_55] = i_53;
         }

         if (i_56 == 2) {
            i_52 = i_53;
            i_53 = class310_2.method5515(-217808766) + i_54;
            i_54 = i_53;
            this.field1568[i_55] = i_51;
            this.field1569[i_55] = i_52;
            this.field1584[i_55] = i_53;
         }

         if (i_56 == 3) {
            i_51 = i_53;
            i_53 = class310_2.method5515(944979339) + i_54;
            i_54 = i_53;
            this.field1568[i_55] = i_51;
            this.field1569[i_55] = i_52;
            this.field1584[i_55] = i_53;
         }

         if (i_56 == 4) {
            int i_57 = i_51;
            i_51 = i_52;
            i_52 = i_57;
            i_53 = class310_2.method5515(1179129598) + i_54;
            i_54 = i_53;
            this.field1568[i_55] = i_51;
            this.field1569[i_55] = i_57;
            this.field1584[i_55] = i_53;
         }
      }

      class310_2.field3751 = i_41;
      class310_3.field3751 = i_42;
      class310_4.field3751 = i_43;
      class310_5.field3751 = i_44;
      class310_6.field3751 = i_45;
      class310_7.field3751 = i_46;

      for (i_55 = 0; i_55 < i_11; i_55++) {
         i_56 = this.field1570[i_55] & 0xff;
         if (i_56 == 0) {
            this.field1580[i_55] = (short)class310_2.method5729(700710650);
            this.field1590[i_55] = (short)class310_2.method5729(112455956);
            this.field1582[i_55] = (short)class310_2.method5729(-18715682);
         }
      }

      class310_2.field3751 = i_26;
      i_55 = class310_2.method5661((byte) -92);
      if (i_55 != 0) {
         new class143();
         class310_2.method5729(-49350665);
         class310_2.method5729(276858374);
         class310_2.method5729(-11658071);
         class310_2.method5507(1537099703);
      }

   }

   public class130 method2830(int[][] ints_1, int i_2, int i_3, int i_4, boolean bool_5, int i_6) {
      this.method2871();
      int i_7 = i_2 + this.field1585;
      int i_8 = i_2 + this.field1595;
      int i_9 = i_4 + this.field1597;
      int i_10 = i_4 + this.field1596;
      if (i_7 >= 0 && i_8 + 128 >> 7 < ints_1.length && i_9 >= 0 && i_10 + 128 >> 7 < ints_1[0].length) {
         i_7 >>= 7;
         i_8 = i_8 + 127 >> 7;
         i_9 >>= 7;
         i_10 = i_10 + 127 >> 7;
         if (i_3 == ints_1[i_7][i_9] && i_3 == ints_1[i_8][i_9] && i_3 == ints_1[i_7][i_10] && i_3 == ints_1[i_8][i_10]) {
            return this;
         } else {
            class130 class130_11 = new class130();
            class130_11.field1575 = this.field1575;
            class130_11.field1579 = this.field1579;
            class130_11.field1563 = this.field1563;
            class130_11.field1567 = this.field1567;
            class130_11.field1586 = this.field1586;
            class130_11.field1568 = this.field1568;
            class130_11.field1569 = this.field1569;
            class130_11.field1584 = this.field1584;
            class130_11.field1571 = this.field1571;
            class130_11.field1572 = this.field1572;
            class130_11.field1564 = this.field1564;
            class130_11.field1589 = this.field1589;
            class130_11.field1578 = this.field1578;
            class130_11.field1574 = this.field1574;
            class130_11.field1577 = this.field1577;
            class130_11.field1570 = this.field1570;
            class130_11.field1580 = this.field1580;
            class130_11.field1590 = this.field1590;
            class130_11.field1582 = this.field1582;
            class130_11.field1583 = this.field1583;
            class130_11.field1594 = this.field1594;
            class130_11.field1576 = this.field1576;
            class130_11.field1573 = this.field1573;
            class130_11.field1566 = this.field1566;
            class130_11.field1591 = this.field1591;
            class130_11.field1565 = new int[class130_11.field1575];
            int i_12;
            int i_13;
            int i_14;
            int i_15;
            int i_16;
            int i_17;
            int i_18;
            int i_19;
            int i_20;
            int i_21;
            if (i_6 == 0) {
               for (i_12 = 0; i_12 < class130_11.field1575; i_12++) {
                  i_13 = i_2 + this.field1567[i_12];
                  i_14 = i_4 + this.field1586[i_12];
                  i_15 = i_13 & 0x7f;
                  i_16 = i_14 & 0x7f;
                  i_17 = i_13 >> 7;
                  i_18 = i_14 >> 7;
                  i_19 = ints_1[i_17][i_18] * (128 - i_15) + ints_1[i_17 + 1][i_18] * i_15 >> 7;
                  i_20 = ints_1[i_17][i_18 + 1] * (128 - i_15) + i_15 * ints_1[i_17 + 1][i_18 + 1] >> 7;
                  i_21 = i_19 * (128 - i_16) + i_20 * i_16 >> 7;
                  class130_11.field1565[i_12] = i_21 + this.field1565[i_12] - i_3;
               }
            } else {
               for (i_12 = 0; i_12 < class130_11.field1575; i_12++) {
                  i_13 = (-this.field1565[i_12] << 16) / super.field1894;
                  if (i_13 < i_6) {
                     i_14 = i_2 + this.field1567[i_12];
                     i_15 = i_4 + this.field1586[i_12];
                     i_16 = i_14 & 0x7f;
                     i_17 = i_15 & 0x7f;
                     i_18 = i_14 >> 7;
                     i_19 = i_15 >> 7;
                     i_20 = ints_1[i_18][i_19] * (128 - i_16) + ints_1[i_18 + 1][i_19] * i_16 >> 7;
                     i_21 = ints_1[i_18][i_19 + 1] * (128 - i_16) + i_16 * ints_1[i_18 + 1][i_19 + 1] >> 7;
                     int i_22 = i_20 * (128 - i_17) + i_21 * i_17 >> 7;
                     class130_11.field1565[i_12] = (i_6 - i_13) * (i_22 - i_3) / i_6 + this.field1565[i_12];
                  }
               }
            }

            class130_11.method2897();
            return class130_11;
         }
      } else {
         return this;
      }
   }

   public void method2836(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < this.field1575; i_4++) {
         this.field1567[i_4] += i_1;
         this.field1565[i_4] += i_2;
         this.field1586[i_4] += i_3;
      }

      this.method2897();
   }

   void method2831() {
      int[] ints_1;
      int i_2;
      int i_3;
      int i_4;
      if (this.field1583 != null) {
         ints_1 = new int[256];
         i_2 = 0;

         for (i_3 = 0; i_3 < this.field1575; i_3++) {
            i_4 = this.field1583[i_3];
            ++ints_1[i_4];
            if (i_4 > i_2) {
               i_2 = i_4;
            }
         }

         this.field1576 = new int[i_2 + 1][];

         for (i_3 = 0; i_3 <= i_2; i_3++) {
            this.field1576[i_3] = new int[ints_1[i_3]];
            ints_1[i_3] = 0;
         }

         for (i_3 = 0; i_3 < this.field1575; this.field1576[i_4][ints_1[i_4]++] = i_3++) {
            i_4 = this.field1583[i_3];
         }

         this.field1583 = null;
      }

      if (this.field1594 != null) {
         ints_1 = new int[256];
         i_2 = 0;

         for (i_3 = 0; i_3 < this.field1579; i_3++) {
            i_4 = this.field1594[i_3];
            ++ints_1[i_4];
            if (i_4 > i_2) {
               i_2 = i_4;
            }
         }

         this.field1573 = new int[i_2 + 1][];

         for (i_3 = 0; i_3 <= i_2; i_3++) {
            this.field1573[i_3] = new int[ints_1[i_3]];
            ints_1[i_3] = 0;
         }

         for (i_3 = 0; i_3 < this.field1579; this.field1573[i_4][ints_1[i_4]++] = i_3++) {
            i_4 = this.field1594[i_3];
         }

         this.field1594 = null;
      }

   }

   public void method2839() {
      int i_1;
      for (i_1 = 0; i_1 < this.field1575; i_1++) {
         this.field1586[i_1] = -this.field1586[i_1];
      }

      for (i_1 = 0; i_1 < this.field1579; i_1++) {
         int i_2 = this.field1568[i_1];
         this.field1568[i_1] = this.field1584[i_1];
         this.field1584[i_1] = i_2;
      }

      this.method2897();
   }

   public void method2843(int i_1) {
      int i_2 = field1581[i_1];
      int i_3 = field1602[i_1];

      for (int i_4 = 0; i_4 < this.field1575; i_4++) {
         int i_5 = i_2 * this.field1586[i_4] + i_3 * this.field1567[i_4] >> 16;
         this.field1586[i_4] = i_3 * this.field1586[i_4] - i_2 * this.field1567[i_4] >> 16;
         this.field1567[i_4] = i_5;
      }

      this.method2897();
   }

   public void method2832() {
      for (int i_1 = 0; i_1 < this.field1575; i_1++) {
         int i_2 = this.field1567[i_1];
         this.field1567[i_1] = this.field1586[i_1];
         this.field1586[i_1] = -i_2;
      }

      this.method2897();
   }

   public void method2868() {
      for (int i_1 = 0; i_1 < this.field1575; i_1++) {
         this.field1567[i_1] = -this.field1567[i_1];
         this.field1586[i_1] = -this.field1586[i_1];
      }

      this.method2897();
   }

   public void method2835() {
      for (int i_1 = 0; i_1 < this.field1575; i_1++) {
         int i_2 = this.field1586[i_1];
         this.field1586[i_1] = this.field1567[i_1];
         this.field1567[i_1] = -i_2;
      }

      this.method2897();
   }

   final int method2828(class130 class130_1, int i_2) {
      int i_3 = -1;
      int i_4 = class130_1.field1567[i_2];
      int i_5 = class130_1.field1565[i_2];
      int i_6 = class130_1.field1586[i_2];

      for (int i_7 = 0; i_7 < this.field1575; i_7++) {
         if (i_4 == this.field1567[i_7] && i_5 == this.field1565[i_7] && i_6 == this.field1586[i_7]) {
            i_3 = i_7;
            break;
         }
      }

      if (i_3 == -1) {
         this.field1567[this.field1575] = i_4;
         this.field1565[this.field1575] = i_5;
         this.field1586[this.field1575] = i_6;
         if (class130_1.field1583 != null) {
            this.field1583[this.field1575] = class130_1.field1583[i_2];
         }

         i_3 = this.field1575++;
      }

      return i_3;
   }

   public static class130 method2824(class244 class244_0, int i_1, int i_2) {
      byte[] bytes_3 = class244_0.method4160(i_1, i_2, (short) -19303);
      return bytes_3 == null ? null : new class130(bytes_3);
   }

   static void method2844(class130 class130_0, class130 class130_1, int i_2, int i_3, int i_4, boolean bool_5) {
      class130_0.method2871();
      class130_0.method2841();
      class130_1.method2871();
      class130_1.method2841();
      ++field1600;
      int i_6 = 0;
      int[] ints_7 = class130_1.field1567;
      int i_8 = class130_1.field1575;

      int i_9;
      for (i_9 = 0; i_9 < class130_0.field1575; i_9++) {
         class138 class138_10 = class130_0.field1588[i_9];
         if (class138_10.field1773 != 0) {
            int i_11 = class130_0.field1565[i_9] - i_3;
            if (i_11 <= class130_1.field1593) {
               int i_12 = class130_0.field1567[i_9] - i_2;
               if (i_12 >= class130_1.field1585 && i_12 <= class130_1.field1595) {
                  int i_13 = class130_0.field1586[i_9] - i_4;
                  if (i_13 >= class130_1.field1597 && i_13 <= class130_1.field1596) {
                     for (int i_14 = 0; i_14 < i_8; i_14++) {
                        class138 class138_15 = class130_1.field1588[i_14];
                        if (i_12 == ints_7[i_14] && i_13 == class130_1.field1586[i_14] && i_11 == class130_1.field1565[i_14] && class138_15.field1773 != 0) {
                           if (class130_0.field1601 == null) {
                              class130_0.field1601 = new class138[class130_0.field1575];
                           }

                           if (class130_1.field1601 == null) {
                              class130_1.field1601 = new class138[i_8];
                           }

                           class138 class138_16 = class130_0.field1601[i_9];
                           if (class138_16 == null) {
                              class138_16 = class130_0.field1601[i_9] = new class138(class138_10);
                           }

                           class138 class138_17 = class130_1.field1601[i_14];
                           if (class138_17 == null) {
                              class138_17 = class130_1.field1601[i_14] = new class138(class138_15);
                           }

                           class138_16.field1772 += class138_15.field1772;
                           class138_16.field1771 += class138_15.field1771;
                           class138_16.field1775 += class138_15.field1775;
                           class138_16.field1773 += class138_15.field1773;
                           class138_17.field1772 += class138_10.field1772;
                           class138_17.field1771 += class138_10.field1771;
                           class138_17.field1775 += class138_10.field1775;
                           class138_17.field1773 += class138_10.field1773;
                           ++i_6;
                           field1598[i_9] = field1600;
                           field1599[i_14] = field1600;
                        }
                     }
                  }
               }
            }
         }
      }

      if (i_6 >= 3 && bool_5) {
         for (i_9 = 0; i_9 < class130_0.field1579; i_9++) {
            if (field1598[class130_0.field1568[i_9]] == field1600 && field1598[class130_0.field1569[i_9]] == field1600 && field1598[class130_0.field1584[i_9]] == field1600) {
               if (class130_0.field1571 == null) {
                  class130_0.field1571 = new byte[class130_0.field1579];
               }

               class130_0.field1571[i_9] = 2;
            }
         }

         for (i_9 = 0; i_9 < class130_1.field1579; i_9++) {
            if (field1600 == field1599[class130_1.field1568[i_9]] && field1600 == field1599[class130_1.field1569[i_9]] && field1600 == field1599[class130_1.field1584[i_9]]) {
               if (class130_1.field1571 == null) {
                  class130_1.field1571 = new byte[class130_1.field1579];
               }

               class130_1.field1571[i_9] = 2;
            }
         }

      }
   }

   static final int method2880(int i_0) {
      if (i_0 < 2) {
         i_0 = 2;
      } else if (i_0 > 126) {
         i_0 = 126;
      }

      return i_0;
   }

   static final int method2846(int i_0, int i_1) {
      i_1 = (i_0 & 0x7f) * i_1 >> 7;
      if (i_1 < 2) {
         i_1 = 2;
      } else if (i_1 > 126) {
         i_1 = 126;
      }

      return (i_0 & 0xff80) + i_1;
   }

}
