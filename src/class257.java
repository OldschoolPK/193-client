public class class257 extends class184 {

   static class244 field3315;
   public int field3313;
   static class154 field3308 = new class154(64);
   static class154 field3309 = new class154(64);
   public int field3312 = 255;
   public int field3320 = 255;
   public int field3314 = -1;
   public int field3307 = 1;
   public int field3316 = 70;
   int field3317 = -1;
   int field3318 = -1;
   public int field3319 = 30;
   public int field3310 = 0;

   void method4478(class310 class310_1, int i_2, byte b_3) {
      if (i_2 == 1) {
         class310_1.method5729(-877025018);
      } else if (i_2 == 2) {
         this.field3312 = class310_1.method5661((byte) -5);
      } else if (i_2 == 3) {
         this.field3320 = class310_1.method5661((byte) -61);
      } else if (i_2 == 4) {
         this.field3314 = 0;
      } else if (i_2 == 5) {
         this.field3316 = class310_1.method5729(357570713);
      } else if (i_2 == 6) {
         class310_1.method5661((byte) -67);
      } else if (i_2 == 7) {
         this.field3317 = class310_1.method5519((short) 1005);
      } else if (i_2 == 8) {
         this.field3318 = class310_1.method5519((short) 1005);
      } else if (i_2 == 11) {
         this.field3314 = class310_1.method5729(90613292);
      } else if (i_2 == 14) {
         this.field3319 = class310_1.method5661((byte) 49);
      } else if (i_2 == 15) {
         this.field3310 = class310_1.method5661((byte) -32);
      }

   }

   void method4463(class310 class310_1, byte b_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -30);
         if (i_3 == 0) {
            return;
         }

         this.method4478(class310_1, i_3, (byte) 37);
      }
   }

   public class335 method4459(short s_1) {
      if (this.field3318 < 0) {
         return null;
      } else {
         class335 class335_2 = (class335) field3309.method3376((long)this.field3318);
         if (class335_2 != null) {
            return class335_2;
         } else {
            class335_2 = class230.method4115(field3315, this.field3318, 0, (byte) 26);
            if (class335_2 != null) {
               field3309.method3374(class335_2, (long)this.field3318);
            }

            return class335_2;
         }
      }
   }

   public class335 method4462(int i_1) {
      if (this.field3317 < 0) {
         return null;
      } else {
         class335 class335_2 = (class335) field3309.method3376((long)this.field3317);
         if (class335_2 != null) {
            return class335_2;
         } else {
            class335_2 = class230.method4115(field3315, this.field3317, 0, (byte) 20);
            if (class335_2 != null) {
               field3309.method3374(class335_2, (long)this.field3317);
            }

            return class335_2;
         }
      }
   }

   public static String method4470(int i_0, boolean bool_1, byte b_2) {
      if (bool_1 && i_0 >= 0) {
         int i_4 = i_0;
         String string_3;
         if (bool_1 && i_0 >= 0) {
            int i_5 = 2;

            for (int i_6 = i_0 / 10; i_6 != 0; i_5++) {
               i_6 /= 10;
            }

            char[] arr_7 = new char[i_5];
            arr_7[0] = 43;

            for (int i_8 = i_5 - 1; i_8 > 0; --i_8) {
               int i_9 = i_4;
               i_4 /= 10;
               int i_10 = i_9 - i_4 * 10;
               if (i_10 >= 10) {
                  arr_7[i_8] = (char)(i_10 + 87);
               } else {
                  arr_7[i_8] = (char)(i_10 + 48);
               }
            }

            string_3 = new String(arr_7);
         } else {
            string_3 = Integer.toString(i_0, 10);
         }

         return string_3;
      } else {
         return Integer.toString(i_0);
      }
   }

   static final int method4479(class226 class226_0, int i_1, short s_2) {
      if (class226_0.field2599 != null && i_1 < class226_0.field2599.length) {
         try {
            int[] ints_3 = class226_0.field2599[i_1];
            int i_4 = 0;
            int i_5 = 0;
            byte b_6 = 0;

            while (true) {
               int i_7 = ints_3[i_5++];
               int i_8 = 0;
               byte b_9 = 0;
               if (i_7 == 0) {
                  return i_4;
               }

               if (i_7 == 1) {
                  i_8 = client.field780[ints_3[i_5++]];
               }

               if (i_7 == 2) {
                  i_8 = client.field781[ints_3[i_5++]];
               }

               if (i_7 == 3) {
                  i_8 = client.field675[ints_3[i_5++]];
               }

               int i_10;
               class226 class226_11;
               int i_12;
               int i_13;
               if (i_7 == 4) {
                  i_10 = ints_3[i_5++] << 16;
                  i_10 += ints_3[i_5++];
                  class226_11 = class181.method3610(i_10, 514190901);
                  i_12 = ints_3[i_5++];
                  if (i_12 != -1 && (!class66.method1223(i_12, 132969699).field3441 || client.field659)) {
                     for (i_13 = 0; i_13 < class226_11.field2704.length; i_13++) {
                        if (i_12 + 1 == class226_11.field2704[i_13]) {
                           i_8 += class226_11.field2711[i_13];
                        }
                     }
                  }
               }

               if (i_7 == 5) {
                  i_8 = class221.field2541[ints_3[i_5++]];
               }

               if (i_7 == 6) {
                  i_8 = class230.field2768[client.field781[ints_3[i_5++]] - 1];
               }

               if (i_7 == 7) {
                  i_8 = class221.field2541[ints_3[i_5++]] * 100 / 46875;
               }

               if (i_7 == 8) {
                  i_8 = class223.field2562.field611;
               }

               if (i_7 == 9) {
                  for (i_10 = 0; i_10 < 25; i_10++) {
                     if (class230.field2769[i_10]) {
                        i_8 += client.field781[i_10];
                     }
                  }
               }

               if (i_7 == 10) {
                  i_10 = ints_3[i_5++] << 16;
                  i_10 += ints_3[i_5++];
                  class226_11 = class181.method3610(i_10, -1966056676);
                  i_12 = ints_3[i_5++];
                  if (i_12 != -1 && (!class66.method1223(i_12, -178952540).field3441 || client.field659)) {
                     for (i_13 = 0; i_13 < class226_11.field2704.length; i_13++) {
                        if (i_12 + 1 == class226_11.field2704[i_13]) {
                           i_8 = 999999999;
                           break;
                        }
                     }
                  }
               }

               if (i_7 == 11) {
                  i_8 = client.field815;
               }

               if (i_7 == 12) {
                  i_8 = client.field816;
               }

               if (i_7 == 13) {
                  i_10 = class221.field2541[ints_3[i_5++]];
                  int i_14 = ints_3[i_5++];
                  i_8 = (i_10 & 1 << i_14) != 0 ? 1 : 0;
               }

               if (i_7 == 14) {
                  i_10 = ints_3[i_5++];
                  i_8 = class222.method3999(i_10, -703587276);
               }

               if (i_7 == 15) {
                  b_9 = 1;
               }

               if (i_7 == 16) {
                  b_9 = 2;
               }

               if (i_7 == 17) {
                  b_9 = 3;
               }

               if (i_7 == 18) {
                  i_8 = (class223.field2562.field990 >> 7) + class243.field3156;
               }

               if (i_7 == 19) {
                  i_8 = (class223.field2562.field938 >> 7) + class41.field300;
               }

               if (i_7 == 20) {
                  i_8 = ints_3[i_5++];
               }

               if (b_9 == 0) {
                  if (b_6 == 0) {
                     i_4 += i_8;
                  }

                  if (b_6 == 1) {
                     i_4 -= i_8;
                  }

                  if (b_6 == 2 && i_8 != 0) {
                     i_4 /= i_8;
                  }

                  if (b_6 == 3) {
                     i_4 *= i_8;
                  }

                  b_6 = 0;
               } else {
                  b_6 = b_9;
               }
            }
         } catch (Exception exception_15) {
            return -1;
         }
      } else {
         return -2;
      }
   }

   static void method4477(int i_0, int i_1, int i_2) {
      int[] ints_3 = new int[4];
      int[] ints_4 = new int[4];
      ints_3[0] = i_0;
      ints_4[0] = i_1;
      int i_5 = 1;

      for (int i_6 = 0; i_6 < 4; i_6++) {
         if (class80.field1016[i_6] != i_0) {
            ints_3[i_5] = class80.field1016[i_6];
            ints_4[i_5] = class80.field1015[i_6];
            ++i_5;
         }
      }

      class80.field1016 = ints_3;
      class80.field1015 = ints_4;
      class103.method2287(class80.field1012, 0, class80.field1012.length - 1, class80.field1016, class80.field1015, -740794716);
   }

   public static void method4476(byte b_0) {
      class263.field3356.method3375();
      class263.field3357.method3375();
      class263.field3369.method3375();
   }

}
