import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

public class class319 implements Runnable {

   int field3829 = 0;
   int field3825 = 0;
   InputStream field3826;
   int field3827;
   byte[] field3828;
   Thread field3830;
   IOException field3831;

   class319(InputStream inputstream_1, int i_2) {
      this.field3826 = inputstream_1;
      this.field3827 = i_2 + 1;
      this.field3828 = new byte[this.field3827];
      this.field3830 = new Thread(this);
      this.field3830.setDaemon(true);
      this.field3830.start();
   }

   boolean method5870(int i_1, int i_2) throws IOException {
      if (i_1 == 0) {
         return true;
      } else if (i_1 > 0 && i_1 < this.field3827) {
         synchronized(this) {
            int i_4;
            if (this.field3829 <= this.field3825) {
               i_4 = this.field3825 - this.field3829;
            } else {
               i_4 = this.field3827 - this.field3829 + this.field3825;
            }

            if (i_4 < i_1) {
               if (this.field3831 != null) {
                  throw new IOException(this.field3831.toString());
               } else {
                  this.notifyAll();
                  return false;
               }
            } else {
               return true;
            }
         }
      } else {
         throw new IOException();
      }
   }

   int method5881(int i_1) throws IOException {
      synchronized(this) {
         if (this.field3829 == this.field3825) {
            if (this.field3831 != null) {
               throw new IOException(this.field3831.toString());
            } else {
               return -1;
            }
         } else {
            int i_3 = this.field3828[this.field3829] & 0xff;
            this.field3829 = (this.field3829 + 1) % this.field3827;
            this.notifyAll();
            return i_3;
         }
      }
   }

   int method5873(byte[] bytes_1, int i_2, int i_3, byte b_4) throws IOException {
      if (i_3 >= 0 && i_2 >= 0 && i_3 + i_2 <= bytes_1.length) {
         synchronized(this) {
            int i_6;
            if (this.field3829 <= this.field3825) {
               i_6 = this.field3825 - this.field3829;
            } else {
               i_6 = this.field3827 - this.field3829 + this.field3825;
            }

            if (i_3 > i_6) {
               i_3 = i_6;
            }

            if (i_3 == 0 && this.field3831 != null) {
               throw new IOException(this.field3831.toString());
            } else {
               if (i_3 + this.field3829 <= this.field3827) {
                  System.arraycopy(this.field3828, this.field3829, bytes_1, i_2, i_3);
               } else {
                  int i_7 = this.field3827 - this.field3829;
                  System.arraycopy(this.field3828, this.field3829, bytes_1, i_2, i_7);
                  System.arraycopy(this.field3828, 0, bytes_1, i_7 + i_2, i_3 - i_7);
               }

               this.field3829 = (i_3 + this.field3829) % this.field3827;
               this.notifyAll();
               return i_3;
            }
         }
      } else {
         throw new IOException();
      }
   }

   int method5871(int i_1) throws IOException {
      synchronized(this) {
         int i_3;
         if (this.field3829 <= this.field3825) {
            i_3 = this.field3825 - this.field3829;
         } else {
            i_3 = this.field3827 - this.field3829 + this.field3825;
         }

         if (i_3 <= 0 && this.field3831 != null) {
            throw new IOException(this.field3831.toString());
         } else {
            this.notifyAll();
            return i_3;
         }
      }
   }

   void method5879(short s_1) {
      synchronized(this) {
         if (this.field3831 == null) {
            this.field3831 = new IOException("");
         }

         this.notifyAll();
      }

      try {
         this.field3830.join();
      } catch (InterruptedException interruptedexception_4) {
         ;
      }

   }

   public void run() {
      while (true) {
         int i_1;
         synchronized(this) {
            while (true) {
               if (this.field3831 != null) {
                  return;
               }

               if (this.field3829 == 0) {
                  i_1 = this.field3827 - this.field3825 - 1;
               } else if (this.field3829 <= this.field3825) {
                  i_1 = this.field3827 - this.field3825;
               } else {
                  i_1 = this.field3829 - this.field3825 - 1;
               }

               if (i_1 > 0) {
                  break;
               }

               try {
                  this.wait();
               } catch (InterruptedException interruptedexception_10) {
                  ;
               }
            }
         }

         int i_7;
         try {
            i_7 = this.field3826.read(this.field3828, this.field3825, i_1);
            if (i_7 == -1) {
               throw new EOFException();
            }
         } catch (IOException ioexception_11) {
            IOException ioexception_3 = ioexception_11;
            synchronized(this) {
               this.field3831 = ioexception_3;
               return;
            }
         }

         synchronized(this) {
            this.field3825 = (i_7 + this.field3825) % this.field3827;
         }
      }
   }

}
