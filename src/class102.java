public class class102 extends class144 {

   static class246 field1310;
   static int field1315;
   public static String[] field1318;
   static int field1316;
   int field1317;
   int field1312;
   int field1307;
   int field1309;
   int field1306;
   int field1311;
   class269 field1308;
   int field1313;
   int field1314;

   class102(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, boolean bool_8, class144 class144_9) {
      this.field1317 = i_1;
      this.field1312 = i_2;
      this.field1307 = i_3;
      this.field1309 = i_4;
      this.field1306 = i_5;
      this.field1311 = i_6;
      if (i_7 != -1) {
         this.field1308 = class260.method4510(i_7, -1658368124);
         this.field1313 = 0;
         this.field1314 = client.field655 - 1;
         if (this.field1308.field3548 == 0 && class144_9 != null && class144_9 instanceof class102) {
            class102 class102_10 = (class102) class144_9;
            if (class102_10.field1308 == this.field1308) {
               this.field1313 = class102_10.field1313;
               this.field1314 = class102_10.field1314;
               return;
            }
         }

         if (bool_8 && this.field1308.field3558 != -1) {
            this.field1313 = (int)(Math.random() * (double)this.field1308.field3550.length);
            this.field1314 -= (int)(Math.random() * (double)this.field1308.field3552[this.field1313]);
         }
      }

   }

   protected final class136 vmethod3305(int i_1) {
      if (this.field1308 != null) {
         int i_2 = client.field655 - this.field1314;
         if (i_2 > 100 && this.field1308.field3558 > 0) {
            i_2 = 100;
         }

         label55: {
            do {
               do {
                  if (i_2 <= this.field1308.field3552[this.field1313]) {
                     break label55;
                  }

                  i_2 -= this.field1308.field3552[this.field1313];
                  ++this.field1313;
               } while (this.field1313 < this.field1308.field3550.length);

               this.field1313 -= this.field1308.field3558;
            } while (this.field1313 >= 0 && this.field1313 < this.field1308.field3550.length);

            this.field1308 = null;
         }

         this.field1314 = client.field655 - i_2;
      }

      class264 class264_13 = class34.method383(this.field1317, -643350125);
      if (class264_13.field3421 != null) {
         class264_13 = class264_13.method4599(2033630337);
      }

      if (class264_13 == null) {
         return null;
      } else {
         int i_3;
         int i_4;
         if (this.field1307 != 1 && this.field1307 != 3) {
            i_3 = class264_13.field3418;
            i_4 = class264_13.field3396;
         } else {
            i_3 = class264_13.field3396;
            i_4 = class264_13.field3418;
         }

         int i_5 = (i_3 >> 1) + this.field1306;
         int i_6 = (i_3 + 1 >> 1) + this.field1306;
         int i_7 = (i_4 >> 1) + this.field1311;
         int i_8 = (i_4 + 1 >> 1) + this.field1311;
         int[][] ints_9 = class64.field519[this.field1309];
         int i_10 = ints_9[i_6][i_8] + ints_9[i_5][i_8] + ints_9[i_6][i_7] + ints_9[i_5][i_7] >> 2;
         int i_11 = (this.field1306 << 7) + (i_3 << 6);
         int i_12 = (this.field1311 << 7) + (i_4 << 6);
         return class264_13.method4587(this.field1312, this.field1307, ints_9, i_11, i_10, i_12, this.field1308, this.field1313, -1432537935);
      }
   }

   public static int method2277(byte[] bytes_0, int i_1, byte b_2) {
      return class31.method334(bytes_0, 0, i_1, 1948146007);
   }

   static class36 method2272(byte[] bytes_0, byte b_1) {
      return bytes_0 == null ? new class36() : new class36(class28.method285(bytes_0, 223444851).field3917);
   }

   static int method2276(int i_0) {
      return 12;
   }

   static void method2275(int i_0, int i_1, int i_2) {
      class196 class196_3 = class68.method1249(class192.field2266, client.field694.field1328, (byte) 1);
      class196_3.field2360.method5510(i_0, (byte) -13);
      class196_3.field2360.method5486(i_1, (byte) -8);
      client.field694.method2295(class196_3, -2086623705);
   }

}
