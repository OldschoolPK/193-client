import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class class47 extends class35 {

   List field354;
   HashSet field355;
   HashSet field353;

   void method731(class310 class310_1, boolean bool_2, byte b_3) {
      this.field354 = new LinkedList();
      int i_4 = class310_1.method5729(-670955798);

      for (int i_5 = 0; i_5 < i_4; i_5++) {
         int i_6 = class310_1.method5519((short) 1005);
         class222 class222_7 = new class222(class310_1.method5507(1559744950));
         boolean bool_8 = class310_1.method5661((byte) 16) == 1;
         if (bool_2 || !bool_8) {
            this.field354.add(new class27((class222) null, class222_7, i_6, (class37) null));
         }
      }

   }

   void method735(class310 class310_1, class310 class310_2, int i_3, boolean bool_4, int i_5) {
      this.method397(class310_1, i_3, 1014419421);
      int i_6 = class310_2.method5729(-39720901);
      this.field355 = new HashSet(i_6);

      int i_7;
      for (i_7 = 0; i_7 < i_6; i_7++) {
         class24 class24_8 = new class24();

         try {
            class24_8.method234(class310_2, -804003183);
         } catch (IllegalStateException illegalstateexception_13) {
            continue;
         }

         this.field355.add(class24_8);
      }

      i_7 = class310_2.method5729(1113611396);
      this.field353 = new HashSet(i_7);

      for (int i_11 = 0; i_11 < i_7; i_11++) {
         class48 class48_9 = new class48();

         try {
            class48_9.method737(class310_2, -804130257);
         } catch (IllegalStateException illegalstateexception_12) {
            continue;
         }

         this.field353.add(class48_9);
      }

      this.method731(class310_2, bool_4, (byte) 54);
   }

   static int method734(char var_0, class199 class199_1, int i_2) {
      int i_3 = var_0 << 4;
      if (Character.isUpperCase(var_0) || Character.isTitleCase(var_0)) {
         var_0 = Character.toLowerCase(var_0);
         i_3 = (var_0 << 4) + 1;
      }

      if (var_0 == 241 && class199_1 == class199.field2378) {
         i_3 = 1762;
      }

      return i_3;
   }

}
