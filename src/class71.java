public class class71 extends class189 {

   protected static boolean field573;
   class226 field571;
   int field570;
   int field579;
   class226 field575;
   int field569;
   String field578;
   int field574;
   int field576;
   int field577;
   boolean field581;
   int field580 = 76;
   Object[] field572;

   public void method1253(Object[] arr_1, int i_2) {
      this.field572 = arr_1;
   }

   public void method1254(int i_1, int i_2) {
      this.field580 = i_1;
   }

   static boolean method1260(String string_0, int i_1, byte b_2) {
      return class13.method131(string_0, i_1, "openjs", (byte) -63);
   }

   static final void method1259(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, class142 class142_7, class181 class181_8, byte b_9) {
      class264 class264_10 = class34.method383(i_4, -343246086);
      int i_11;
      int i_12;
      if (i_5 != 1 && i_5 != 3) {
         i_11 = class264_10.field3418;
         i_12 = class264_10.field3396;
      } else {
         i_11 = class264_10.field3396;
         i_12 = class264_10.field3418;
      }

      int i_13;
      int i_14;
      if (i_11 + i_2 <= 104) {
         i_13 = (i_11 >> 1) + i_2;
         i_14 = i_2 + (i_11 + 1 >> 1);
      } else {
         i_13 = i_2;
         i_14 = i_2 + 1;
      }

      int i_15;
      int i_16;
      if (i_3 + i_12 <= 104) {
         i_15 = i_3 + (i_12 >> 1);
         i_16 = i_3 + (i_12 + 1 >> 1);
      } else {
         i_15 = i_3;
         i_16 = i_3 + 1;
      }

      int[][] ints_17 = class64.field519[i_1];
      int i_18 = ints_17[i_13][i_15] + ints_17[i_14][i_15] + ints_17[i_13][i_16] + ints_17[i_14][i_16] >> 2;
      int i_19 = (i_2 << 7) + (i_11 << 6);
      int i_20 = (i_3 << 7) + (i_12 << 6);
      long long_21 = class88.method2085(i_2, i_3, 2, class264_10.field3423 == 0, i_4, (byte) -4);
      int i_23 = (i_5 << 6) + i_6;
      if (class264_10.field3417 == 1) {
         i_23 += 256;
      }

      Object obj_24;
      if (i_6 == 22) {
         if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
            obj_24 = class264_10.method4586(22, i_5, ints_17, i_19, i_18, i_20, -2128450589);
         } else {
            obj_24 = new class102(i_4, 22, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
         }

         class142_7.method3137(i_0, i_2, i_3, i_18, (class144) obj_24, long_21, i_23);
         if (class264_10.field3397 == 1) {
            class181_8.method3596(i_2, i_3, -1399004423);
         }

      } else if (i_6 != 10 && i_6 != 11) {
         if (i_6 >= 12) {
            if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
               obj_24 = class264_10.method4586(i_6, i_5, ints_17, i_19, i_18, i_20, -1757051193);
            } else {
               obj_24 = new class102(i_4, i_6, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
            }

            class142_7.method3132(i_0, i_2, i_3, i_18, 1, 1, (class144) obj_24, 0, long_21, i_23);
            if (class264_10.field3397 != 0) {
               class181_8.method3586(i_2, i_3, i_11, i_12, class264_10.field3398, (byte) -126);
            }

         } else if (i_6 == 0) {
            if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
               obj_24 = class264_10.method4586(0, i_5, ints_17, i_19, i_18, i_20, -1793954024);
            } else {
               obj_24 = new class102(i_4, 0, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
            }

            class142_7.method3258(i_0, i_2, i_3, i_18, (class144) obj_24, (class144) null, class64.field512[i_5], 0, long_21, i_23);
            if (class264_10.field3397 != 0) {
               class181_8.method3582(i_2, i_3, i_6, i_5, class264_10.field3398, (byte) -76);
            }

         } else if (i_6 == 1) {
            if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
               obj_24 = class264_10.method4586(1, i_5, ints_17, i_19, i_18, i_20, -2054664746);
            } else {
               obj_24 = new class102(i_4, 1, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
            }

            class142_7.method3258(i_0, i_2, i_3, i_18, (class144) obj_24, (class144) null, class64.field513[i_5], 0, long_21, i_23);
            if (class264_10.field3397 != 0) {
               class181_8.method3582(i_2, i_3, i_6, i_5, class264_10.field3398, (byte) -77);
            }

         } else {
            int i_30;
            if (i_6 == 2) {
               i_30 = i_5 + 1 & 0x3;
               Object obj_25;
               Object obj_26;
               if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                  obj_25 = class264_10.method4586(2, i_5 + 4, ints_17, i_19, i_18, i_20, -1561317404);
                  obj_26 = class264_10.method4586(2, i_30, ints_17, i_19, i_18, i_20, -1560071668);
               } else {
                  obj_25 = new class102(i_4, 2, i_5 + 4, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
                  obj_26 = new class102(i_4, 2, i_30, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
               }

               class142_7.method3258(i_0, i_2, i_3, i_18, (class144) obj_25, (class144) obj_26, class64.field512[i_5], class64.field512[i_30], long_21, i_23);
               if (class264_10.field3397 != 0) {
                  class181_8.method3582(i_2, i_3, i_6, i_5, class264_10.field3398, (byte) -126);
               }

            } else if (i_6 == 3) {
               if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                  obj_24 = class264_10.method4586(3, i_5, ints_17, i_19, i_18, i_20, -1709079436);
               } else {
                  obj_24 = new class102(i_4, 3, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
               }

               class142_7.method3258(i_0, i_2, i_3, i_18, (class144) obj_24, (class144) null, class64.field513[i_5], 0, long_21, i_23);
               if (class264_10.field3397 != 0) {
                  class181_8.method3582(i_2, i_3, i_6, i_5, class264_10.field3398, (byte) -51);
               }

            } else if (i_6 == 9) {
               if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                  obj_24 = class264_10.method4586(i_6, i_5, ints_17, i_19, i_18, i_20, -2057154315);
               } else {
                  obj_24 = new class102(i_4, i_6, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
               }

               class142_7.method3132(i_0, i_2, i_3, i_18, 1, 1, (class144) obj_24, 0, long_21, i_23);
               if (class264_10.field3397 != 0) {
                  class181_8.method3586(i_2, i_3, i_11, i_12, class264_10.field3398, (byte) -6);
               }

            } else if (i_6 == 4) {
               if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                  obj_24 = class264_10.method4586(4, i_5, ints_17, i_19, i_18, i_20, -1500569538);
               } else {
                  obj_24 = new class102(i_4, 4, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
               }

               class142_7.method3140(i_0, i_2, i_3, i_18, (class144) obj_24, (class144) null, class64.field512[i_5], 0, 0, 0, long_21, i_23);
            } else {
               Object obj_27;
               long long_31;
               if (i_6 == 5) {
                  i_30 = 16;
                  long_31 = class142_7.method3138(i_0, i_2, i_3);
                  if (long_31 != 0L) {
                     i_30 = class34.method383(class149.method3348(long_31), -1067557852).field3404;
                  }

                  if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                     obj_27 = class264_10.method4586(4, i_5, ints_17, i_19, i_18, i_20, -1496791923);
                  } else {
                     obj_27 = new class102(i_4, 4, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
                  }

                  class142_7.method3140(i_0, i_2, i_3, i_18, (class144) obj_27, (class144) null, class64.field512[i_5], 0, i_30 * class64.field518[i_5], i_30 * class64.field515[i_5], long_21, i_23);
               } else if (i_6 == 6) {
                  i_30 = 8;
                  long_31 = class142_7.method3138(i_0, i_2, i_3);
                  if (long_31 != 0L) {
                     i_30 = class34.method383(class149.method3348(long_31), -127309246).field3404 / 2;
                  }

                  if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                     obj_27 = class264_10.method4586(4, i_5 + 4, ints_17, i_19, i_18, i_20, -2090483968);
                  } else {
                     obj_27 = new class102(i_4, 4, i_5 + 4, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
                  }

                  class142_7.method3140(i_0, i_2, i_3, i_18, (class144) obj_27, (class144) null, 256, i_5, i_30 * class64.field509[i_5], i_30 * class64.field517[i_5], long_21, i_23);
               } else if (i_6 == 7) {
                  int i_33 = i_5 + 2 & 0x3;
                  if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                     obj_24 = class264_10.method4586(4, i_33 + 4, ints_17, i_19, i_18, i_20, -2129926871);
                  } else {
                     obj_24 = new class102(i_4, 4, i_33 + 4, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
                  }

                  class142_7.method3140(i_0, i_2, i_3, i_18, (class144) obj_24, (class144) null, 256, i_33, 0, 0, long_21, i_23);
               } else if (i_6 == 8) {
                  i_30 = 8;
                  long_31 = class142_7.method3138(i_0, i_2, i_3);
                  if (long_31 != 0L) {
                     i_30 = class34.method383(class149.method3348(long_31), -1309287861).field3404 / 2;
                  }

                  int i_29 = i_5 + 2 & 0x3;
                  Object obj_28;
                  if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
                     obj_27 = class264_10.method4586(4, i_5 + 4, ints_17, i_19, i_18, i_20, -2047006407);
                     obj_28 = class264_10.method4586(4, i_29 + 4, ints_17, i_19, i_18, i_20, -1817474931);
                  } else {
                     obj_27 = new class102(i_4, 4, i_5 + 4, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
                     obj_28 = new class102(i_4, 4, i_29 + 4, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
                  }

                  class142_7.method3140(i_0, i_2, i_3, i_18, (class144) obj_27, (class144) obj_28, 256, i_5, i_30 * class64.field509[i_5], i_30 * class64.field517[i_5], long_21, i_23);
               }
            }
         }
      } else {
         if (class264_10.field3411 == -1 && class264_10.field3421 == null) {
            obj_24 = class264_10.method4586(10, i_5, ints_17, i_19, i_18, i_20, -1693992720);
         } else {
            obj_24 = new class102(i_4, 10, i_5, i_1, i_2, i_3, class264_10.field3411, class264_10.field3429, (class144) null);
         }

         if (obj_24 != null) {
            class142_7.method3132(i_0, i_2, i_3, i_18, i_11, i_12, (class144) obj_24, i_6 == 11 ? 256 : 0, long_21, i_23);
         }

         if (class264_10.field3397 != 0) {
            class181_8.method3586(i_2, i_3, i_11, i_12, class264_10.field3398, (byte) -77);
         }

      }
   }

}
