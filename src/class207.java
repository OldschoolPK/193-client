public class class207 extends class124 {

   static int field2455;
   int field2429 = 256;
   int field2437 = 1000000;
   int[] field2446 = new int[16];
   int[] field2432 = new int[16];
   int[] field2433 = new int[16];
   int[] field2443 = new int[16];
   int[] field2435 = new int[16];
   int[] field2436 = new int[16];
   int[] field2441 = new int[16];
   int[] field2438 = new int[16];
   int[] field2439 = new int[16];
   int[] field2454 = new int[16];
   int[] field2440 = new int[16];
   int[] field2434 = new int[16];
   int[] field2430 = new int[16];
   int[] field2444 = new int[16];
   int[] field2442 = new int[16];
   class208[][] field2428 = new class208[16][128];
   class208[][] field2447 = new class208[16][128];
   class209 field2445 = new class209();
   class211 field2448 = new class211(this);
   class328 field2431 = new class328(128);
   long field2452;
   boolean field2449;
   int field2450;
   int field2451;
   long field2453;

   public class207() {
      this.method3811((byte) 2);
   }

   void method3721(int i_1, int i_2, short s_3) {
      this.field2443[i_1] = i_2;
      this.field2436[i_1] = i_2 & ~0x7f;
      this.method3730(i_1, i_2, 1712020733);
   }

   void method3783(int i_1, int i_2, int i_3, int i_4) {
      class208 class208_5 = this.field2428[i_1][i_2];
      if (class208_5 != null) {
         this.field2428[i_1][i_2] = null;
         if ((this.field2454[i_1] & 0x2) != 0) {
            for (class208 class208_6 = (class208) this.field2448.field2502.method4879(); class208_6 != null; class208_6 = (class208) this.field2448.field2502.method4884()) {
               if (class208_5.field2477 == class208_6.field2477 && class208_6.field2470 < 0 && class208_5 != class208_6) {
                  class208_5.field2470 = 0;
                  break;
               }
            }
         } else {
            class208_5.field2470 = 0;
         }

      }
   }

   void method3719(int i_1, int i_2) {
      for (class208 class208_3 = (class208) this.field2448.field2502.method4879(); class208_3 != null; class208_3 = (class208) this.field2448.field2502.method4884()) {
         if (i_1 < 0 || class208_3.field2477 == i_1) {
            if (class208_3.field2474 != null) {
               class208_3.field2474.method2592(class114.field1443 / 100);
               if (class208_3.field2474.method2596()) {
                  this.field2448.field2504.method2324(class208_3.field2474);
               }

               class208_3.method3829(2120173978);
            }

            if (class208_3.field2470 < 0) {
               this.field2428[class208_3.field2477][class208_3.field2461] = null;
            }

            class208_3.method3628();
         }
      }

   }

   synchronized void method3799(short s_1) {
      this.field2445.method3837();
      this.method3811((byte) 100);
   }

   void method3738(int i_1, int i_2) {
      if (i_1 >= 0) {
         this.field2446[i_1] = 12800;
         this.field2432[i_1] = 8192;
         this.field2433[i_1] = 16383;
         this.field2441[i_1] = 8192;
         this.field2438[i_1] = 0;
         this.field2439[i_1] = 8192;
         this.method3748(i_1, -907765086);
         this.method3742(i_1, (byte) 0);
         this.field2454[i_1] = 0;
         this.field2440[i_1] = 32767;
         this.field2434[i_1] = 256;
         this.field2430[i_1] = 0;
         this.method3744(i_1, 8192, 1806361942);
      } else {
         for (i_1 = 0; i_1 < 16; i_1++) {
            this.method3738(i_1, 1243220702);
         }

      }
   }

   void method3734(int i_1, int i_2, int i_3, byte b_4) {
   }

   void method3737(int i_1, int i_2, int i_3, byte b_4) {
      this.method3783(i_1, i_2, 64, 1421180601);
      if ((this.field2454[i_1] & 0x2) != 0) {
         for (class208 class208_5 = (class208) this.field2448.field2502.method4883(); class208_5 != null; class208_5 = (class208) this.field2448.field2502.method4893()) {
            if (class208_5.field2477 == i_1 && class208_5.field2470 < 0) {
               this.field2428[i_1][class208_5.field2461] = null;
               this.field2428[i_1][i_2] = class208_5;
               int i_6 = (class208_5.field2466 * class208_5.field2464 >> 12) + class208_5.field2476;
               class208_5.field2476 += i_2 - class208_5.field2461 << 8;
               class208_5.field2464 = i_6 - class208_5.field2476;
               class208_5.field2466 = 4096;
               class208_5.field2461 = i_2;
               return;
            }
         }
      }

      class210 class210_10 = (class210) this.field2431.method5968((long)this.field2435[i_1]);
      if (class210_10 != null) {
         class112 class112_9 = class210_10.field2493[i_2];
         if (class112_9 != null) {
            class208 class208_7 = new class208();
            class208_7.field2477 = i_1;
            class208_7.field2457 = class210_10;
            class208_7.field2458 = class112_9;
            class208_7.field2459 = class210_10.field2492[i_2];
            class208_7.field2460 = class210_10.field2498[i_2];
            class208_7.field2461 = i_2;
            class208_7.field2462 = i_3 * i_3 * class210_10.field2495[i_2] * class210_10.field2499 + 1024 >> 11;
            class208_7.field2463 = class210_10.field2496[i_2] & 0xff;
            class208_7.field2476 = (i_2 << 8) - (class210_10.field2494[i_2] & 0x7fff);
            class208_7.field2467 = 0;
            class208_7.field2465 = 0;
            class208_7.field2472 = 0;
            class208_7.field2470 = -1;
            class208_7.field2471 = 0;
            if (this.field2430[i_1] == 0) {
               class208_7.field2474 = class122.method2602(class112_9, this.method3745(class208_7, -1112663023), this.method3818(class208_7, 976464226), this.method3823(class208_7, (short) -20015));
            } else {
               class208_7.field2474 = class122.method2602(class112_9, this.method3745(class208_7, -542831150), 0, this.method3823(class208_7, (short) -15634));
               this.method3732(class208_7, class210_10.field2494[i_2] < 0, (byte) 1);
            }

            if (class210_10.field2494[i_2] < 0) {
               class208_7.field2474.method2581(-1);
            }

            if (class208_7.field2460 >= 0) {
               class208 class208_8 = this.field2447[i_1][class208_7.field2460];
               if (class208_8 != null && class208_8.field2470 < 0) {
                  this.field2428[i_1][class208_8.field2461] = null;
                  class208_8.field2470 = 0;
               }

               this.field2447[i_1][class208_7.field2460] = class208_7;
            }

            this.field2448.field2502.method4888(class208_7);
            this.field2428[i_1][i_2] = class208_7;
         }
      }
   }

   void method3811(byte b_1) {
      this.method3719(-1, 649437065);
      this.method3738(-1, 178885513);

      int i_2;
      for (i_2 = 0; i_2 < 16; i_2++) {
         this.field2435[i_2] = this.field2443[i_2];
      }

      for (i_2 = 0; i_2 < 16; i_2++) {
         this.field2436[i_2] = this.field2443[i_2] & ~0x7f;
      }

   }

   void method3730(int i_1, int i_2, int i_3) {
      if (i_2 != this.field2435[i_1]) {
         this.field2435[i_1] = i_2;

         for (int i_4 = 0; i_4 < 128; i_4++) {
            this.field2447[i_1][i_4] = null;
         }
      }

   }

   protected synchronized void vmethod3905(int[] ints_1, int i_2, int i_3) {
      if (this.field2445.method3838()) {
         int i_4 = this.field2445.field2487 * this.field2437 / class114.field1443;

         do {
            long long_5 = this.field2452 + (long)i_3 * (long)i_4;
            if (this.field2453 - long_5 >= 0L) {
               this.field2452 = long_5;
               break;
            }

            int i_7 = (int)((this.field2453 - this.field2452 + (long)i_4 - 1L) / (long)i_4);
            this.field2452 += (long)i_7 * (long)i_4;
            this.field2448.vmethod3905(ints_1, i_2, i_7);
            i_2 += i_7;
            i_3 -= i_7;
            this.method3753(1920150271);
         } while (this.field2445.method3838());
      }

      this.field2448.vmethod3905(ints_1, i_2, i_3);
   }

   protected synchronized void vmethod3898(int i_1) {
      if (this.field2445.method3838()) {
         int i_2 = this.field2445.field2487 * this.field2437 / class114.field1443;

         do {
            long long_3 = (long)i_1 * (long)i_2 + this.field2452;
            if (this.field2453 - long_3 >= 0L) {
               this.field2452 = long_3;
               break;
            }

            int i_5 = (int)(((long)i_2 + (this.field2453 - this.field2452) - 1L) / (long)i_2);
            this.field2452 += (long)i_2 * (long)i_5;
            this.field2448.vmethod3898(i_5);
            i_1 -= i_5;
            this.method3753(1360483838);
         } while (this.field2445.method3838());
      }

      this.field2448.vmethod3898(i_1);
   }

   void method3746(int i_1, int i_2, int i_3) {
   }

   void method3736(int i_1, int i_2, byte b_3) {
      this.field2441[i_1] = i_2;
   }

   int method3745(class208 class208_1, int i_2) {
      int i_3 = (class208_1.field2466 * class208_1.field2464 >> 12) + class208_1.field2476;
      i_3 += (this.field2441[class208_1.field2477] - 8192) * this.field2434[class208_1.field2477] >> 12;
      class205 class205_4 = class208_1.field2459;
      int i_5;
      if (class205_4.field2416 > 0 && (class205_4.field2415 > 0 || this.field2438[class208_1.field2477] > 0)) {
         i_5 = class205_4.field2415 << 2;
         int i_6 = class205_4.field2409 << 1;
         if (class208_1.field2468 < i_6) {
            i_5 = i_5 * class208_1.field2468 / i_6;
         }

         i_5 += this.field2438[class208_1.field2477] >> 7;
         double d_7 = Math.sin((double)(class208_1.field2469 & 0x1ff) * 0.01227184630308513D);
         i_3 += (int)((double)i_5 * d_7);
      }

      i_5 = (int)((double)(class208_1.field2458.field1407 * 256) * Math.pow(2.0D, (double)i_3 * 3.255208333333333E-4D) / (double)class114.field1443 + 0.5D);
      return i_5 < 1 ? 1 : i_5;
   }

   protected synchronized class124 vmethod3917() {
      return null;
   }

   synchronized boolean method3722(class212 class212_1, class244 class244_2, class117 class117_3, int i_4, byte b_5) {
      class212_1.method3925();
      boolean bool_6 = true;
      int[] ints_7 = null;
      if (i_4 > 0) {
         ints_7 = new int[] {i_4};
      }

      for (class186 class186_8 = (class186) class212_1.field2508.method5962(); class186_8 != null; class186_8 = (class186) class212_1.field2508.method5957()) {
         int i_9 = (int)class186_8.field2137;
         class210 class210_10 = (class210) this.field2431.method5968((long)i_9);
         if (class210_10 == null) {
            byte[] bytes_12 = class244_2.method4167(i_9, 1882557788);
            class210 class210_11;
            if (bytes_12 == null) {
               class210_11 = null;
            } else {
               class210_11 = new class210(bytes_12);
            }

            class210_10 = class210_11;
            if (class210_11 == null) {
               bool_6 = false;
               continue;
            }

            this.field2431.method5956(class210_11, (long)i_9);
         }

         if (!class210_10.method3889(class117_3, class186_8.field2132, ints_7, 189208991)) {
            bool_6 = false;
         }
      }

      if (bool_6) {
         class212_1.method3926();
      }

      return bool_6;
   }

   boolean method3820(class208 class208_1, int i_2) {
      if (class208_1.field2474 == null) {
         if (class208_1.field2470 >= 0) {
            class208_1.method3628();
            if (class208_1.field2460 > 0 && class208_1 == this.field2447[class208_1.field2477][class208_1.field2460]) {
               this.field2447[class208_1.field2477][class208_1.field2460] = null;
            }
         }

         return true;
      } else {
         return false;
      }
   }

   void method3748(int i_1, int i_2) {
      if ((this.field2454[i_1] & 0x2) != 0) {
         for (class208 class208_3 = (class208) this.field2448.field2502.method4879(); class208_3 != null; class208_3 = (class208) this.field2448.field2502.method4884()) {
            if (class208_3.field2477 == i_1 && this.field2428[i_1][class208_3.field2461] == null && class208_3.field2470 < 0) {
               class208_3.field2470 = 0;
            }
         }
      }

   }

   int method3785(int i_1) {
      return this.field2429;
   }

   void method3742(int i_1, byte b_2) {
      if ((this.field2454[i_1] & 0x4) != 0) {
         for (class208 class208_3 = (class208) this.field2448.field2502.method4879(); class208_3 != null; class208_3 = (class208) this.field2448.field2502.method4884()) {
            if (class208_3.field2477 == i_1) {
               class208_3.field2456 = 0;
            }
         }
      }

   }

   void method3743(int i_1, byte b_2) {
      int i_3 = i_1 & 0xf0;
      int i_4;
      int i_5;
      int i_6;
      if (i_3 == 128) {
         i_4 = i_1 & 0xf;
         i_5 = i_1 >> 8 & 0x7f;
         i_6 = i_1 >> 16 & 0x7f;
         this.method3783(i_4, i_5, i_6, 452314933);
      } else if (i_3 == 144) {
         i_4 = i_1 & 0xf;
         i_5 = i_1 >> 8 & 0x7f;
         i_6 = i_1 >> 16 & 0x7f;
         if (i_6 > 0) {
            this.method3737(i_4, i_5, i_6, (byte) 1);
         } else {
            this.method3783(i_4, i_5, 64, 541244219);
         }

      } else if (i_3 == 160) {
         i_4 = i_1 & 0xf;
         i_5 = i_1 >> 8 & 0x7f;
         i_6 = i_1 >> 16 & 0x7f;
         this.method3734(i_4, i_5, i_6, (byte) -55);
      } else if (i_3 == 176) {
         i_4 = i_1 & 0xf;
         i_5 = i_1 >> 8 & 0x7f;
         i_6 = i_1 >> 16 & 0x7f;
         if (i_5 == 0) {
            this.field2436[i_4] = (i_6 << 14) + (this.field2436[i_4] & ~0x1fc000);
         }

         if (i_5 == 32) {
            this.field2436[i_4] = (i_6 << 7) + (this.field2436[i_4] & ~0x3f80);
         }

         if (i_5 == 1) {
            this.field2438[i_4] = (i_6 << 7) + (this.field2438[i_4] & ~0x3f80);
         }

         if (i_5 == 33) {
            this.field2438[i_4] = i_6 + (this.field2438[i_4] & ~0x7f);
         }

         if (i_5 == 5) {
            this.field2439[i_4] = (i_6 << 7) + (this.field2439[i_4] & ~0x3f80);
         }

         if (i_5 == 37) {
            this.field2439[i_4] = i_6 + (this.field2439[i_4] & ~0x7f);
         }

         if (i_5 == 7) {
            this.field2446[i_4] = (i_6 << 7) + (this.field2446[i_4] & ~0x3f80);
         }

         if (i_5 == 39) {
            this.field2446[i_4] = i_6 + (this.field2446[i_4] & ~0x7f);
         }

         if (i_5 == 10) {
            this.field2432[i_4] = (i_6 << 7) + (this.field2432[i_4] & ~0x3f80);
         }

         if (i_5 == 42) {
            this.field2432[i_4] = i_6 + (this.field2432[i_4] & ~0x7f);
         }

         if (i_5 == 11) {
            this.field2433[i_4] = (i_6 << 7) + (this.field2433[i_4] & ~0x3f80);
         }

         if (i_5 == 43) {
            this.field2433[i_4] = i_6 + (this.field2433[i_4] & ~0x7f);
         }

         if (i_5 == 64) {
            if (i_6 >= 64) {
               this.field2454[i_4] |= 0x1;
            } else {
               this.field2454[i_4] &= ~0x1;
            }
         }

         if (i_5 == 65) {
            if (i_6 >= 64) {
               this.field2454[i_4] |= 0x2;
            } else {
               this.method3748(i_4, -635370566);
               this.field2454[i_4] &= ~0x2;
            }
         }

         if (i_5 == 99) {
            this.field2440[i_4] = (i_6 << 7) + (this.field2440[i_4] & 0x7f);
         }

         if (i_5 == 98) {
            this.field2440[i_4] = (this.field2440[i_4] & 0x3f80) + i_6;
         }

         if (i_5 == 101) {
            this.field2440[i_4] = (i_6 << 7) + (this.field2440[i_4] & 0x7f) + 16384;
         }

         if (i_5 == 100) {
            this.field2440[i_4] = (this.field2440[i_4] & 0x3f80) + i_6 + 16384;
         }

         if (i_5 == 120) {
            this.method3719(i_4, -1449292724);
         }

         if (i_5 == 121) {
            this.method3738(i_4, -526613580);
         }

         if (i_5 == 123) {
            this.method3774(i_4, 971113747);
         }

         int i_7;
         if (i_5 == 6) {
            i_7 = this.field2440[i_4];
            if (i_7 == 16384) {
               this.field2434[i_4] = (i_6 << 7) + (this.field2434[i_4] & ~0x3f80);
            }
         }

         if (i_5 == 38) {
            i_7 = this.field2440[i_4];
            if (i_7 == 16384) {
               this.field2434[i_4] = i_6 + (this.field2434[i_4] & ~0x7f);
            }
         }

         if (i_5 == 16) {
            this.field2430[i_4] = (i_6 << 7) + (this.field2430[i_4] & ~0x3f80);
         }

         if (i_5 == 48) {
            this.field2430[i_4] = i_6 + (this.field2430[i_4] & ~0x7f);
         }

         if (i_5 == 81) {
            if (i_6 >= 64) {
               this.field2454[i_4] |= 0x4;
            } else {
               this.method3742(i_4, (byte) 0);
               this.field2454[i_4] &= ~0x4;
            }
         }

         if (i_5 == 17) {
            this.method3744(i_4, (i_6 << 7) + (this.field2444[i_4] & ~0x3f80), 368361036);
         }

         if (i_5 == 49) {
            this.method3744(i_4, i_6 + (this.field2444[i_4] & ~0x7f), 1274170775);
         }

      } else if (i_3 == 192) {
         i_4 = i_1 & 0xf;
         i_5 = i_1 >> 8 & 0x7f;
         this.method3730(i_4, i_5 + this.field2436[i_4], 1712020733);
      } else if (i_3 == 208) {
         i_4 = i_1 & 0xf;
         i_5 = i_1 >> 8 & 0x7f;
         this.method3746(i_4, i_5, 173552395);
      } else if (i_3 == 224) {
         i_4 = i_1 & 0xf;
         i_5 = (i_1 >> 8 & 0x7f) + (i_1 >> 9 & 0x3f80);
         this.method3736(i_4, i_5, (byte) -30);
      } else {
         i_3 = i_1 & 0xff;
         if (i_3 == 255) {
            this.method3811((byte) 81);
         }
      }
   }

   synchronized boolean method3727(int i_1) {
      return this.field2445.method3838();
   }

   synchronized void method3723(byte b_1) {
      for (class210 class210_2 = (class210) this.field2431.method5962(); class210_2 != null; class210_2 = (class210) this.field2431.method5957()) {
         class210_2.method3890((byte) -118);
      }

   }

   synchronized void method3724(int i_1) {
      for (class210 class210_2 = (class210) this.field2431.method5962(); class210_2 != null; class210_2 = (class210) this.field2431.method5957()) {
         class210_2.method3628();
      }

   }

   boolean method3755(class208 class208_1, int[] ints_2, int i_3, int i_4, int i_5) {
      class208_1.field2475 = class114.field1443 / 100;
      if (class208_1.field2470 < 0 || class208_1.field2474 != null && !class208_1.field2474.method2595()) {
         int i_6 = class208_1.field2466;
         if (i_6 > 0) {
            i_6 -= (int)(16.0D * Math.pow(2.0D, (double)this.field2439[class208_1.field2477] * 4.921259842519685E-4D) + 0.5D);
            if (i_6 < 0) {
               i_6 = 0;
            }

            class208_1.field2466 = i_6;
         }

         class208_1.field2474.method2593(this.method3745(class208_1, -1046287806));
         class205 class205_7 = class208_1.field2459;
         boolean bool_8 = false;
         ++class208_1.field2468;
         class208_1.field2469 += class205_7.field2416;
         double d_9 = 5.086263020833333E-6D * (double)((class208_1.field2461 - 60 << 8) + (class208_1.field2466 * class208_1.field2464 >> 12));
         if (class205_7.field2410 > 0) {
            if (class205_7.field2414 > 0) {
               class208_1.field2467 += (int)(128.0D * Math.pow(2.0D, d_9 * (double)class205_7.field2414) + 0.5D);
            } else {
               class208_1.field2467 += 128;
            }
         }

         if (class205_7.field2411 != null) {
            if (class205_7.field2417 > 0) {
               class208_1.field2465 += (int)(128.0D * Math.pow(2.0D, (double)class205_7.field2417 * d_9) + 0.5D);
            } else {
               class208_1.field2465 += 128;
            }

            while (class208_1.field2472 < class205_7.field2411.length - 2 && class208_1.field2465 > (class205_7.field2411[class208_1.field2472 + 2] & 0xff) << 8) {
               class208_1.field2472 += 2;
            }

            if (class205_7.field2411.length - 2 == class208_1.field2472 && class205_7.field2411[class208_1.field2472 + 1] == 0) {
               bool_8 = true;
            }
         }

         if (class208_1.field2470 >= 0 && class205_7.field2412 != null && (this.field2454[class208_1.field2477] & 0x1) == 0 && (class208_1.field2460 < 0 || class208_1 != this.field2447[class208_1.field2477][class208_1.field2460])) {
            if (class205_7.field2413 > 0) {
               class208_1.field2470 += (int)(128.0D * Math.pow(2.0D, (double)class205_7.field2413 * d_9) + 0.5D);
            } else {
               class208_1.field2470 += 128;
            }

            while (class208_1.field2471 < class205_7.field2412.length - 2 && class208_1.field2470 > (class205_7.field2412[class208_1.field2471 + 2] & 0xff) << 8) {
               class208_1.field2471 += 2;
            }

            if (class205_7.field2412.length - 2 == class208_1.field2471) {
               bool_8 = true;
            }
         }

         if (bool_8) {
            class208_1.field2474.method2592(class208_1.field2475);
            if (ints_2 != null) {
               class208_1.field2474.vmethod3905(ints_2, i_3, i_4);
            } else {
               class208_1.field2474.vmethod3898(i_4);
            }

            if (class208_1.field2474.method2596()) {
               this.field2448.field2504.method2324(class208_1.field2474);
            }

            class208_1.method3829(2108671396);
            if (class208_1.field2470 >= 0) {
               class208_1.method3628();
               if (class208_1.field2460 > 0 && class208_1 == this.field2447[class208_1.field2477][class208_1.field2460]) {
                  this.field2447[class208_1.field2477][class208_1.field2460] = null;
               }
            }

            return true;
         } else {
            class208_1.field2474.method2716(class208_1.field2475, this.method3818(class208_1, 1961596880), this.method3823(class208_1, (short) -9329));
            return false;
         }
      } else {
         class208_1.method3829(2082654889);
         class208_1.method3628();
         if (class208_1.field2460 > 0 && class208_1 == this.field2447[class208_1.field2477][class208_1.field2460]) {
            this.field2447[class208_1.field2477][class208_1.field2460] = null;
         }

         return true;
      }
   }

   public synchronized void method3720(int i_1, short s_2) {
      this.field2429 = i_1;
   }

   protected synchronized class124 vmethod3894() {
      return this.field2448;
   }

   void method3744(int i_1, int i_2, int i_3) {
      this.field2444[i_1] = i_2;
      this.field2442[i_1] = (int)(2097152.0D * Math.pow(2.0D, (double)i_2 * 5.4931640625E-4D) + 0.5D);
   }

   synchronized void method3805(class212 class212_1, boolean bool_2, int i_3) {
      this.method3799((short) 16860);
      this.field2445.method3887(class212_1.field2509);
      this.field2449 = bool_2;
      this.field2452 = 0L;
      int i_4 = this.field2445.method3839();

      for (int i_5 = 0; i_5 < i_4; i_5++) {
         this.field2445.method3840(i_5);
         this.field2445.method3843(i_5);
         this.field2445.method3841(i_5);
      }

      this.field2450 = this.field2445.method3848();
      this.field2451 = this.field2445.field2482[this.field2450];
      this.field2453 = this.field2445.method3847(this.field2451);
   }

   void method3753(int i_1) {
      int i_2 = this.field2450;
      int i_3 = this.field2451;

      long long_4;
      for (long_4 = this.field2453; i_3 == this.field2451; long_4 = this.field2445.method3847(i_3)) {
         while (i_3 == this.field2445.field2482[i_2]) {
            this.field2445.method3840(i_2);
            int i_6 = this.field2445.method3844(i_2);
            if (i_6 == 1) {
               this.field2445.method3842();
               this.field2445.method3841(i_2);
               if (this.field2445.method3860()) {
                  if (!this.field2449 || i_3 == 0) {
                     this.method3811((byte) 98);
                     this.field2445.method3837();
                     return;
                  }

                  this.field2445.method3850(long_4);
               }
               break;
            }

            if ((i_6 & 0x80) != 0) {
               this.method3743(i_6, (byte) 0);
            }

            this.field2445.method3843(i_2);
            this.field2445.method3841(i_2);
         }

         i_2 = this.field2445.method3848();
         i_3 = this.field2445.field2482[i_2];
      }

      this.field2450 = i_2;
      this.field2451 = i_3;
      this.field2453 = long_4;
   }

   void method3732(class208 class208_1, boolean bool_2, byte b_3) {
      int i_4 = class208_1.field2458.field1405.length;
      int i_5;
      if (bool_2 && class208_1.field2458.field1408) {
         int i_6 = i_4 + i_4 - class208_1.field2458.field1404;
         i_5 = (int)((long)this.field2430[class208_1.field2477] * (long)i_6 >> 6);
         i_4 <<= 8;
         if (i_5 >= i_4) {
            i_5 = i_4 + i_4 - 1 - i_5;
            class208_1.field2474.method2588(true);
         }
      } else {
         i_5 = (int)((long)i_4 * (long)this.field2430[class208_1.field2477] >> 6);
      }

      class208_1.field2474.method2587(i_5);
   }

   protected synchronized int vmethod3896() {
      return 0;
   }

   int method3818(class208 class208_1, int i_2) {
      class205 class205_3 = class208_1.field2459;
      int i_4 = this.field2433[class208_1.field2477] * this.field2446[class208_1.field2477] + 4096 >> 13;
      i_4 = i_4 * i_4 + 16384 >> 15;
      i_4 = i_4 * class208_1.field2462 + 16384 >> 15;
      i_4 = i_4 * this.field2429 + 128 >> 8;
      if (class205_3.field2410 > 0) {
         i_4 = (int)((double)i_4 * Math.pow(0.5D, 1.953125E-5D * (double)class208_1.field2467 * (double)class205_3.field2410) + 0.5D);
      }

      int i_5;
      int i_6;
      int i_7;
      int i_8;
      if (class205_3.field2411 != null) {
         i_5 = class208_1.field2465;
         i_6 = class205_3.field2411[class208_1.field2472 + 1];
         if (class208_1.field2472 < class205_3.field2411.length - 2) {
            i_7 = (class205_3.field2411[class208_1.field2472] & 0xff) << 8;
            i_8 = (class205_3.field2411[class208_1.field2472 + 2] & 0xff) << 8;
            i_6 += (class205_3.field2411[class208_1.field2472 + 3] - i_6) * (i_5 - i_7) / (i_8 - i_7);
         }

         i_4 = i_4 * i_6 + 32 >> 6;
      }

      if (class208_1.field2470 > 0 && class205_3.field2412 != null) {
         i_5 = class208_1.field2470;
         i_6 = class205_3.field2412[class208_1.field2471 + 1];
         if (class208_1.field2471 < class205_3.field2412.length - 2) {
            i_7 = (class205_3.field2412[class208_1.field2471] & 0xff) << 8;
            i_8 = (class205_3.field2412[class208_1.field2471 + 2] & 0xff) << 8;
            i_6 += (class205_3.field2412[class208_1.field2471 + 3] - i_6) * (i_5 - i_7) / (i_8 - i_7);
         }

         i_4 = i_4 * i_6 + 32 >> 6;
      }

      return i_4;
   }

   int method3823(class208 class208_1, short s_2) {
      int i_3 = this.field2432[class208_1.field2477];
      return i_3 < 8192 ? i_3 * class208_1.field2463 + 32 >> 6 : 16384 - ((128 - class208_1.field2463) * (16384 - i_3) + 32 >> 6);
   }

   public synchronized void method3728(int i_1, int i_2, byte b_3) {
      this.method3721(i_1, i_2, (short) -15440);
   }

   void method3774(int i_1, int i_2) {
      for (class208 class208_3 = (class208) this.field2448.field2502.method4879(); class208_3 != null; class208_3 = (class208) this.field2448.field2502.method4884()) {
         if ((i_1 < 0 || class208_3.field2477 == i_1) && class208_3.field2470 < 0) {
            this.field2428[class208_3.field2477][class208_3.field2461] = null;
            class208_3.field2470 = 0;
         }
      }

   }

}
