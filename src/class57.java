import java.awt.Canvas;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

public final class class57 extends Canvas {

   static Image field425;
   Component field424;

   class57(Component component_1) {
      this.field424 = component_1;
   }

   public final void update(Graphics graphics_1) {
      this.field424.update(graphics_1);
   }

   public final void paint(Graphics graphics_1) {
      this.field424.paint(graphics_1);
   }

   static void method913(byte b_0) {
      int i_1;
      if (client.field674 == 0) {
         class67.field536 = new class142(4, 104, 104, class64.field519);

         for (i_1 = 0; i_1 < 4; i_1++) {
            client.field831[i_1] = new class181(104, 104);
         }

         class25.field126 = new class335(512, 512);
         class94.field1189 = "Starting game engine...";
         class94.field1187 = 5;
         client.field674 = 20;
      } else if (client.field674 == 20) {
         class94.field1189 = "Prepared visibility map";
         class94.field1187 = 10;
         client.field674 = 30;
      } else if (client.field674 == 30) {
         class140.field1814 = class44.method661(0, false, true, true, (byte) -3);
         class36.field253 = class44.method661(1, false, true, true, (byte) 2);
         class102.field1310 = class44.method661(2, true, false, true, (byte) -65);
         class330.field3883 = class44.method661(3, false, true, true, (byte) 52);
         class227.field2730 = class44.method661(4, false, true, true, (byte) 69);
         class62.field478 = class44.method661(5, true, true, true, (byte) 36);
         class217.field2520 = class44.method661(6, true, true, true, (byte) 52);
         class167.field2010 = class44.method661(7, false, true, true, (byte) -93);
         class100.field1291 = class44.method661(8, false, true, true, (byte) -50);
         class19.field90 = class44.method661(9, false, true, true, (byte) -16);
         class92.field1167 = class44.method661(10, false, true, true, (byte) 23);
         class68.field544 = class44.method661(11, false, true, true, (byte) 1);
         class196.field2361 = class44.method661(12, false, true, true, (byte) 49);
         class19.field89 = class44.method661(13, true, false, true, (byte) 4);
         class302.field3705 = class44.method661(14, false, true, true, (byte) 30);
         client.field677 = class44.method661(15, false, true, true, (byte) -5);
         class169.field2024 = class44.method661(17, true, true, true, (byte) -58);
         class41.field299 = class44.method661(18, false, true, true, (byte) 74);
         class195.field2357 = class44.method661(19, false, true, true, (byte) -13);
         class243.field3157 = class44.method661(20, false, true, true, (byte) -6);
         class94.field1189 = "Connecting to update server";
         class94.field1187 = 20;
         client.field674 = 40;
      } else if (client.field674 == 40) {
         byte b_22 = 0;
         i_1 = b_22 + class140.field1814.method4275(564971922) * 4 / 100;
         i_1 += class36.field253.method4275(1517890733) * 4 / 100;
         i_1 += class102.field1310.method4275(-36405532) * 2 / 100;
         i_1 += class330.field3883.method4275(-494276604) * 2 / 100;
         i_1 += class227.field2730.method4275(788314786) * 6 / 100;
         i_1 += class62.field478.method4275(1907273253) * 4 / 100;
         i_1 += class217.field2520.method4275(-260410912) * 2 / 100;
         i_1 += class167.field2010.method4275(-788810501) * 56 / 100;
         i_1 += class100.field1291.method4275(-583143675) * 2 / 100;
         i_1 += class19.field90.method4275(2097191583) * 2 / 100;
         i_1 += class92.field1167.method4275(386076481) * 2 / 100;
         i_1 += class68.field544.method4275(1541721255) * 2 / 100;
         i_1 += class196.field2361.method4275(1673766517) * 2 / 100;
         i_1 += class19.field89.method4275(1590858650) * 2 / 100;
         i_1 += class302.field3705.method4275(-274937553) * 2 / 100;
         i_1 += client.field677.method4275(1742217181) * 2 / 100;
         i_1 += class195.field2357.method4275(229347529) / 100;
         i_1 += class41.field299.method4275(-284233470) / 100;
         i_1 += class243.field3157.method4275(716632766) / 100;
         i_1 += class169.field2024.method4271((byte) 2) && class169.field2024.method4165(-1853187618) ? 1 : 0;
         if (i_1 != 100) {
            if (i_1 != 0) {
               class94.field1189 = "Checking for updates - " + i_1 + "%";
            }

            class94.field1187 = 30;
         } else {
            class160.method3441(class140.field1814, "Animations", (byte) 0);
            class160.method3441(class36.field253, "Skeletons", (byte) 0);
            class160.method3441(class227.field2730, "Sound FX", (byte) 0);
            class160.method3441(class62.field478, "Maps", (byte) 0);
            class160.method3441(class217.field2520, "Music Tracks", (byte) 0);
            class160.method3441(class167.field2010, "Models", (byte) 0);
            class160.method3441(class100.field1291, "Sprites", (byte) 0);
            class160.method3441(class68.field544, "Music Jingles", (byte) 0);
            class160.method3441(class302.field3705, "Music Samples", (byte) 0);
            class160.method3441(client.field677, "Music Patches", (byte) 0);
            class160.method3441(class195.field2357, "World Map", (byte) 0);
            class160.method3441(class41.field299, "World Map Geography", (byte) 0);
            class160.method3441(class243.field3157, "World Map Ground", (byte) 0);
            class45.field349 = new class322();
            class45.field349.method5897(class169.field2024, (byte) -1);
            class94.field1189 = "Loaded update list";
            class94.field1187 = 30;
            client.field674 = 45;
         }
      } else {
         class246 class246_3;
         class246 class246_4;
         class246 class246_20;
         if (client.field674 == 45) {
            class27.method270(22050, !client.field910, 2, 1525295907);
            class207 class207_24 = new class207();
            class207_24.method3728(9, 128, (byte) 77);
            class30.field192 = class168.method3498(class62.field469, 0, 22050, (byte) 7);
            class30.field192.method2458(class207_24, 2012600293);
            class246_20 = client.field677;
            class246_3 = class302.field3705;
            class246_4 = class227.field2730;
            class206.field2423 = class246_20;
            class206.field2420 = class246_3;
            class206.field2422 = class246_4;
            class206.field2426 = class207_24;
            class296.field3677 = class168.method3498(class62.field469, 1, 2048, (byte) 7);
            class45.field350 = new class107();
            class296.field3677.method2458(class45.field350, -1385388203);
            class129.field1557 = new class121(22050, class114.field1443);
            class94.field1189 = "Prepared sound engine";
            class94.field1187 = 35;
            client.field674 = 50;
            class37.field260 = new class303(class100.field1291, class19.field89);
         } else if (client.field674 == 50) {
            i_1 = class302.method5313((byte) 1).length;
            client.field699 = class37.field260.method5320(class302.method5313((byte) 1), (short) 272);
            if (client.field699.size() < i_1) {
               class94.field1189 = "Loading fonts - " + client.field699.size() * 100 / i_1 + "%";
               class94.field1187 = 40;
            } else {
               class297.field3684 = (class305) client.field699.get(class302.field3704);
               class26.field137 = (class305) client.field699.get(class302.field3699);
               class17.field74 = (class305) client.field699.get(class302.field3700);
               class58.field434 = client.field912.vmethod6289(-1820791637);
               class94.field1189 = "Loaded fonts";
               class94.field1187 = 40;
               client.field674 = 60;
            }
         } else {
            int i_5;
            int i_14;
            if (client.field674 == 60) {
               class246_20 = class92.field1167;
               class246_3 = class100.field1291;
               i_14 = 0;
               if (class246_20.method4182("title.jpg", "", (byte) 53)) {
                  ++i_14;
               }

               if (class246_3.method4182("logo", "", (byte) -37)) {
                  ++i_14;
               }

               if (class246_3.method4182("logo_deadman_mode", "", (byte) -39)) {
                  ++i_14;
               }

               if (class246_3.method4182("logo_seasonal_mode", "", (byte) 105)) {
                  ++i_14;
               }

               if (class246_3.method4182("titlebox", "", (byte) -41)) {
                  ++i_14;
               }

               if (class246_3.method4182("titlebutton", "", (byte) 30)) {
                  ++i_14;
               }

               if (class246_3.method4182("runes", "", (byte) -38)) {
                  ++i_14;
               }

               if (class246_3.method4182("title_mute", "", (byte) -32)) {
                  ++i_14;
               }

               if (class246_3.method4182("options_radio_buttons,0", "", (byte) 80)) {
                  ++i_14;
               }

               if (class246_3.method4182("options_radio_buttons,2", "", (byte) 34)) {
                  ++i_14;
               }

               if (class246_3.method4182("options_radio_buttons,4", "", (byte) 23)) {
                  ++i_14;
               }

               if (class246_3.method4182("options_radio_buttons,6", "", (byte) -122)) {
                  ++i_14;
               }

               class246_3.method4182("sl_back", "", (byte) 53);
               class246_3.method4182("sl_flags", "", (byte) -8);
               class246_3.method4182("sl_arrows", "", (byte) 55);
               class246_3.method4182("sl_stars", "", (byte) 47);
               class246_3.method4182("sl_button", "", (byte) -8);
               i_5 = class102.method2276(1772389683);
               if (i_14 < i_5) {
                  class94.field1189 = "Loading title screen - " + i_14 * 100 / i_5 + "%";
                  class94.field1187 = 50;
               } else {
                  class94.field1189 = "Loaded title screen";
                  class94.field1187 = 50;
                  class44.method663(5, -1045540944);
                  client.field674 = 70;
               }
            } else if (client.field674 == 70) {
               if (!class102.field1310.method4165(-1630069014)) {
                  class94.field1189 = "Loading config - " + class102.field1310.method4273(1590832710) + "%";
                  class94.field1187 = 60;
               } else {
                  class63.method1157(class102.field1310, -1589242946);
                  class246 class246_23 = class102.field1310;
                  class255.field3287 = class246_23;
                  class246_20 = class102.field1310;
                  class246_3 = class167.field2010;
                  class256.field3299 = class246_20;
                  class227.field2732 = class246_3;
                  class256.field3295 = class256.field3299.method4172(3, (byte) 64);
                  class246_4 = class102.field1310;
                  class246 class246_15 = class167.field2010;
                  boolean bool_6 = client.field910;
                  class264.field3422 = class246_4;
                  class264.field3381 = class246_15;
                  class264.field3399 = bool_6;
                  class80.method1821(class102.field1310, class167.field2010, -186964533);
                  class260.method4493(class102.field1310, 1899658312);
                  class78.method1793(class102.field1310, class167.field2010, client.field659, class297.field3684, 411546609);
                  class114.method2510(class102.field1310, class140.field1814, class36.field253, (byte) 47);
                  class143.method3303(class102.field1310, class167.field2010, 402911181);
                  class200.method3681(class102.field1310, (byte) 101);
                  class246 class246_7 = class102.field1310;
                  class1.field3 = class246_7;
                  class251.field3237 = class1.field3.method4172(16, (byte) 97);
                  class246 class246_21 = class330.field3883;
                  class246 class246_9 = class167.field2010;
                  class246 class246_10 = class100.field1291;
                  class246 class246_11 = class19.field89;
                  class58.field427 = class246_21;
                  class14.field54 = class246_9;
                  class1.field2 = class246_10;
                  class329.field3877 = class246_11;
                  class9.field44 = new class226[class58.field427.method4173(1884163104)][];
                  class195.field2355 = new boolean[class58.field427.method4173(2121734300)];
                  class246 class246_12 = class102.field1310;
                  class250.field3232 = class246_12;
                  class261.method4512(class102.field1310, -890013246);
                  class143.method3302(class102.field1310, 2029131503);
                  class13.method129(class102.field1310, (byte) 2);
                  class99.field1270 = new class99();
                  method912(class102.field1310, class100.field1291, class19.field89, 1765160595);
                  class191.method3640(class102.field1310, class100.field1291, -1425205882);
                  class219.method3979(class102.field1310, class100.field1291, 1703173158);
                  class94.field1189 = "Loaded config";
                  class94.field1187 = 60;
                  client.field674 = 80;
               }
            } else if (client.field674 == 80) {
               i_1 = 0;
               if (class152.field1964 == null) {
                  class152.field1964 = class230.method4115(class100.field1291, class45.field349.field3840, 0, (byte) 30);
               } else {
                  ++i_1;
               }

               if (class137.field1769 == null) {
                  class137.field1769 = class230.method4115(class100.field1291, class45.field349.field3845, 0, (byte) 71);
               } else {
                  ++i_1;
               }

               class334[] arr_2;
               class334 class334_8;
               class334[] arr_18;
               int i_19;
               if (class38.field263 == null) {
                  class246_3 = class100.field1291;
                  i_14 = class45.field349.field3842;
                  if (!class304.method5323(class246_3, i_14, 0, 975575420)) {
                     arr_2 = null;
                  } else {
                     arr_18 = new class334[class336.field3923];

                     for (i_19 = 0; i_19 < class336.field3923; i_19++) {
                        class334_8 = arr_18[i_19] = new class334();
                        class334_8.field3910 = class215.field2511;
                        class334_8.field3906 = class336.field3929;
                        class334_8.field3907 = class336.field3925[i_19];
                        class334_8.field3908 = class336.field3926[i_19];
                        class334_8.field3905 = class336.field3927[i_19];
                        class334_8.field3909 = class336.field3928[i_19];
                        class334_8.field3904 = class336.field3924;
                        class334_8.field3903 = class15.field62[i_19];
                     }

                     class336.field3925 = null;
                     class336.field3926 = null;
                     class336.field3927 = null;
                     class336.field3928 = null;
                     class336.field3924 = null;
                     class15.field62 = null;
                     arr_2 = arr_18;
                  }

                  class38.field263 = arr_2;
               } else {
                  ++i_1;
               }

               if (class73.field604 == null) {
                  class73.field604 = class82.method1890(class100.field1291, class45.field349.field3841, 0, (byte) 93);
               } else {
                  ++i_1;
               }

               if (client.field710 == null) {
                  client.field710 = class82.method1890(class100.field1291, class45.field349.field3844, 0, (byte) -76);
               } else {
                  ++i_1;
               }

               if (class43.field318 == null) {
                  class43.field318 = class82.method1890(class100.field1291, class45.field349.field3843, 0, (byte) -5);
               } else {
                  ++i_1;
               }

               if (class19.field87 == null) {
                  class19.field87 = class82.method1890(class100.field1291, class45.field349.field3846, 0, (byte) 80);
               } else {
                  ++i_1;
               }

               if (class21.field98 == null) {
                  class21.field98 = class82.method1890(class100.field1291, class45.field349.field3847, 0, (byte) -14);
               } else {
                  ++i_1;
               }

               if (class78.field998 == null) {
                  class78.field998 = class82.method1890(class100.field1291, class45.field349.field3848, 0, (byte) 15);
               } else {
                  ++i_1;
               }

               if (class70.field564 == null) {
                  class246_3 = class100.field1291;
                  i_14 = class45.field349.field3849;
                  if (!class304.method5323(class246_3, i_14, 0, 975575420)) {
                     arr_2 = null;
                  } else {
                     arr_18 = new class334[class336.field3923];

                     for (i_19 = 0; i_19 < class336.field3923; i_19++) {
                        class334_8 = arr_18[i_19] = new class334();
                        class334_8.field3910 = class215.field2511;
                        class334_8.field3906 = class336.field3929;
                        class334_8.field3907 = class336.field3925[i_19];
                        class334_8.field3908 = class336.field3926[i_19];
                        class334_8.field3905 = class336.field3927[i_19];
                        class334_8.field3909 = class336.field3928[i_19];
                        class334_8.field3904 = class336.field3924;
                        class334_8.field3903 = class15.field62[i_19];
                     }

                     class336.field3925 = null;
                     class336.field3926 = null;
                     class336.field3927 = null;
                     class336.field3928 = null;
                     class336.field3924 = null;
                     class15.field62 = null;
                     arr_2 = arr_18;
                  }

                  class70.field564 = arr_2;
               } else {
                  ++i_1;
               }

               if (class39.field282 == null) {
                  class246_3 = class100.field1291;
                  i_14 = class45.field349.field3850;
                  if (!class304.method5323(class246_3, i_14, 0, 975575420)) {
                     arr_2 = null;
                  } else {
                     arr_18 = new class334[class336.field3923];

                     for (i_19 = 0; i_19 < class336.field3923; i_19++) {
                        class334_8 = arr_18[i_19] = new class334();
                        class334_8.field3910 = class215.field2511;
                        class334_8.field3906 = class336.field3929;
                        class334_8.field3907 = class336.field3925[i_19];
                        class334_8.field3908 = class336.field3926[i_19];
                        class334_8.field3905 = class336.field3927[i_19];
                        class334_8.field3909 = class336.field3928[i_19];
                        class334_8.field3904 = class336.field3924;
                        class334_8.field3903 = class15.field62[i_19];
                     }

                     class336.field3925 = null;
                     class336.field3926 = null;
                     class336.field3927 = null;
                     class336.field3928 = null;
                     class336.field3924 = null;
                     class15.field62 = null;
                     arr_2 = arr_18;
                  }

                  class39.field282 = arr_2;
               } else {
                  ++i_1;
               }

               if (i_1 < 11) {
                  class94.field1189 = "Loading sprites - " + i_1 * 100 / 12 + "%";
                  class94.field1187 = 70;
               } else {
                  class306.field3730 = class39.field282;
                  class137.field1769.method6118();
                  int i_16 = (int)(Math.random() * 21.0D) - 10;
                  int i_17 = (int)(Math.random() * 21.0D) - 10;
                  i_14 = (int)(Math.random() * 21.0D) - 10;
                  i_5 = (int)(Math.random() * 41.0D) - 20;
                  class38.field263[0].method6093(i_16 + i_5, i_17 + i_5, i_5 + i_14);
                  class94.field1189 = "Loaded sprites";
                  class94.field1187 = 70;
                  client.field674 = 90;
               }
            } else if (client.field674 == 90) {
               if (!class19.field90.method4165(-1938904200)) {
                  class94.field1189 = "Loading textures - " + "0%";
                  class94.field1187 = 90;
               } else {
                  class307.field3745 = new class128(class19.field90, class100.field1291, 20, class282.field3617.field1057, client.field910 ? 64 : 128);
                  class139.method3046(class307.field3745);
                  class139.method3047(class282.field3617.field1057);
                  client.field674 = 100;
               }
            } else if (client.field674 == 100) {
               i_1 = class307.field3745.method2788(-1888547862);
               if (i_1 < 100) {
                  class94.field1189 = "Loading textures - " + i_1 + "%";
                  class94.field1187 = 90;
               } else {
                  class94.field1189 = "Loaded textures";
                  class94.field1187 = 90;
                  client.field674 = 110;
               }
            } else if (client.field674 == 110) {
               class117.field1461 = new class72();
               class62.field469.method3521(class117.field1461, 10, 1543300593);
               class94.field1189 = "Loaded input handler";
               class94.field1187 = 92;
               client.field674 = 120;
            } else if (client.field674 == 120) {
               if (!class92.field1167.method4182("huffman", "", (byte) 80)) {
                  class94.field1189 = "Loading wordpack - " + 0 + "%";
                  class94.field1187 = 94;
               } else {
                  class218 class218_13 = new class218(class92.field1167.method4181("huffman", "", 1559839004));
                  class183.method3614(class218_13, 747025369);
                  class94.field1189 = "Loaded wordpack";
                  class94.field1187 = 94;
                  client.field674 = 130;
               }
            } else if (client.field674 == 130) {
               if (!class330.field3883.method4165(-1757174633)) {
                  class94.field1189 = "Loading interfaces - " + class330.field3883.method4273(1520680612) * 4 / 5 + "%";
                  class94.field1187 = 96;
               } else if (!class196.field2361.method4165(-1678437131)) {
                  class94.field1189 = "Loading interfaces - " + (80 + class196.field2361.method4273(1662145042) / 6) + "%";
                  class94.field1187 = 96;
               } else if (!class19.field89.method4165(-1660626077)) {
                  class94.field1189 = "Loading interfaces - " + (96 + class19.field89.method4273(2043403933) / 50) + "%";
                  class94.field1187 = 96;
               } else {
                  class94.field1189 = "Loaded interfaces";
                  class94.field1187 = 98;
                  client.field674 = 140;
               }
            } else if (client.field674 == 140) {
               class94.field1187 = 100;
               if (!class195.field2357.method4183(class44.field328.field330, (byte) 113)) {
                  class94.field1189 = "Loading world map - " + class195.field2357.method4185(class44.field328.field330, (short) -9837) / 10 + "%";
               } else {
                  if (class31.field198 == null) {
                     class31.field198 = new class343();
                     class31.field198.method6314(class195.field2357, class41.field299, class243.field3157, class17.field74, client.field699, class38.field263, -1988514493);
                  }

                  class94.field1189 = "Loaded world map";
                  client.field674 = 150;
               }
            } else if (client.field674 == 150) {
               class44.method663(10, -1642687578);
            }
         }
      }
   }

   public static void method912(class244 class244_0, class244 class244_1, class244 class244_2, int i_3) {
      class263.field3372 = class244_0;
      class263.field3354 = class244_1;
      class263.field3371 = class244_2;
   }

   static boolean method911(int i_0, byte b_1) {
      for (int i_2 = 0; i_2 < client.field870; i_2++) {
         if (client.field693[i_2] == i_0) {
            return true;
         }
      }

      return false;
   }

   static int method907(int i_0, int i_1) {
      class103 class103_2 = (class103) class100.field1288.get(Integer.valueOf(i_0));
      return class103_2 == null ? 0 : class103_2.method2282(-845683507);
   }

}
