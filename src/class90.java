public class class90 {

   static class334[] field1157;

   public static String method2097(CharSequence[] arr_0, int i_1, int i_2, int i_3) {
      if (i_2 == 0) {
         return "";
      } else if (i_2 == 1) {
         CharSequence charsequence_4 = arr_0[i_1];
         return charsequence_4 == null ? "null" : charsequence_4.toString();
      } else {
         int i_9 = i_2 + i_1;
         int i_5 = 0;

         for (int i_6 = i_1; i_6 < i_9; i_6++) {
            CharSequence charsequence_7 = arr_0[i_6];
            if (charsequence_7 == null) {
               i_5 += 4;
            } else {
               i_5 += charsequence_7.length();
            }
         }

         StringBuilder stringbuilder_10 = new StringBuilder(i_5);

         for (int i_11 = i_1; i_11 < i_9; i_11++) {
            CharSequence charsequence_8 = arr_0[i_11];
            if (charsequence_8 == null) {
               stringbuilder_10.append("null");
            } else {
               stringbuilder_10.append(charsequence_8);
            }
         }

         return stringbuilder_10.toString();
      }
   }

}
