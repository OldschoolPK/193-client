public class class181 {

   static class226 field2092;
   public int field2103 = 0;
   public int field2104 = 0;
   int field2106;
   int field2099;
   public int[][] field2107;

   public class181(int i_1, int i_2) {
      this.field2106 = i_1;
      this.field2099 = i_2;
      this.field2107 = new int[this.field2106][this.field2099];
      this.method3581(211407722);
   }

   void method3589(int i_1, int i_2, int i_3, int i_4) {
      this.field2107[i_1][i_2] &= ~i_3;
   }

   void method3607(int i_1, int i_2, int i_3, byte b_4) {
      this.field2107[i_1][i_2] |= i_3;
   }

   public void method3581(int i_1) {
      for (int i_2 = 0; i_2 < this.field2106; i_2++) {
         for (int i_3 = 0; i_3 < this.field2099; i_3++) {
            if (i_2 != 0 && i_3 != 0 && i_2 < this.field2106 - 5 && i_3 < this.field2099 - 5) {
               this.field2107[i_2][i_3] = 16777216;
            } else {
               this.field2107[i_2][i_3] = 16777215;
            }
         }
      }

   }

   public void method3584(int i_1, int i_2, int i_3) {
      i_1 -= this.field2103;
      i_2 -= this.field2104;
      this.field2107[i_1][i_2] |= 0x200000;
   }

   public void method3587(int i_1, int i_2, int i_3, int i_4, boolean bool_5, int i_6) {
      i_1 -= this.field2103;
      i_2 -= this.field2104;
      if (i_3 == 0) {
         if (i_4 == 0) {
            this.method3589(i_1, i_2, 128, 180780669);
            this.method3589(i_1 - 1, i_2, 8, 625203799);
         }

         if (i_4 == 1) {
            this.method3589(i_1, i_2, 2, -178662788);
            this.method3589(i_1, i_2 + 1, 32, 585176085);
         }

         if (i_4 == 2) {
            this.method3589(i_1, i_2, 8, -512037103);
            this.method3589(i_1 + 1, i_2, 128, 810619760);
         }

         if (i_4 == 3) {
            this.method3589(i_1, i_2, 32, 1783776234);
            this.method3589(i_1, i_2 - 1, 2, 271633881);
         }
      }

      if (i_3 == 1 || i_3 == 3) {
         if (i_4 == 0) {
            this.method3589(i_1, i_2, 1, -793355794);
            this.method3589(i_1 - 1, i_2 + 1, 16, 432179695);
         }

         if (i_4 == 1) {
            this.method3589(i_1, i_2, 4, 921248841);
            this.method3589(i_1 + 1, i_2 + 1, 64, 122055419);
         }

         if (i_4 == 2) {
            this.method3589(i_1, i_2, 16, 1548742808);
            this.method3589(i_1 + 1, i_2 - 1, 1, 1786037092);
         }

         if (i_4 == 3) {
            this.method3589(i_1, i_2, 64, -546970516);
            this.method3589(i_1 - 1, i_2 - 1, 4, -1686366392);
         }
      }

      if (i_3 == 2) {
         if (i_4 == 0) {
            this.method3589(i_1, i_2, 130, -1691011086);
            this.method3589(i_1 - 1, i_2, 8, 1674093904);
            this.method3589(i_1, i_2 + 1, 32, -112816709);
         }

         if (i_4 == 1) {
            this.method3589(i_1, i_2, 10, -1783709963);
            this.method3589(i_1, i_2 + 1, 32, 590831968);
            this.method3589(i_1 + 1, i_2, 128, 1350296277);
         }

         if (i_4 == 2) {
            this.method3589(i_1, i_2, 40, -730766351);
            this.method3589(i_1 + 1, i_2, 128, 238000091);
            this.method3589(i_1, i_2 - 1, 2, -251586819);
         }

         if (i_4 == 3) {
            this.method3589(i_1, i_2, 160, 380582897);
            this.method3589(i_1, i_2 - 1, 2, -1569139757);
            this.method3589(i_1 - 1, i_2, 8, 1805951529);
         }
      }

      if (bool_5) {
         if (i_3 == 0) {
            if (i_4 == 0) {
               this.method3589(i_1, i_2, 65536, -127226460);
               this.method3589(i_1 - 1, i_2, 4096, -592222910);
            }

            if (i_4 == 1) {
               this.method3589(i_1, i_2, 1024, 1797039976);
               this.method3589(i_1, i_2 + 1, 16384, -175878909);
            }

            if (i_4 == 2) {
               this.method3589(i_1, i_2, 4096, 2098643006);
               this.method3589(i_1 + 1, i_2, 65536, 277024744);
            }

            if (i_4 == 3) {
               this.method3589(i_1, i_2, 16384, -1351914616);
               this.method3589(i_1, i_2 - 1, 1024, -554195276);
            }
         }

         if (i_3 == 1 || i_3 == 3) {
            if (i_4 == 0) {
               this.method3589(i_1, i_2, 512, 805602052);
               this.method3589(i_1 - 1, i_2 + 1, 8192, 538417089);
            }

            if (i_4 == 1) {
               this.method3589(i_1, i_2, 2048, -1845350922);
               this.method3589(i_1 + 1, i_2 + 1, 32768, 1648197537);
            }

            if (i_4 == 2) {
               this.method3589(i_1, i_2, 8192, -144881610);
               this.method3589(i_1 + 1, i_2 - 1, 512, 1021257536);
            }

            if (i_4 == 3) {
               this.method3589(i_1, i_2, 32768, -1778044036);
               this.method3589(i_1 - 1, i_2 - 1, 2048, 1812590427);
            }
         }

         if (i_3 == 2) {
            if (i_4 == 0) {
               this.method3589(i_1, i_2, 66560, 1200100827);
               this.method3589(i_1 - 1, i_2, 4096, 19142930);
               this.method3589(i_1, i_2 + 1, 16384, 1755757978);
            }

            if (i_4 == 1) {
               this.method3589(i_1, i_2, 5120, -1586593080);
               this.method3589(i_1, i_2 + 1, 16384, -1561529523);
               this.method3589(i_1 + 1, i_2, 65536, -466105297);
            }

            if (i_4 == 2) {
               this.method3589(i_1, i_2, 20480, 1930543644);
               this.method3589(i_1 + 1, i_2, 65536, 929472676);
               this.method3589(i_1, i_2 - 1, 1024, 297161144);
            }

            if (i_4 == 3) {
               this.method3589(i_1, i_2, 81920, 2122187653);
               this.method3589(i_1, i_2 - 1, 1024, -75754499);
               this.method3589(i_1 - 1, i_2, 4096, -763351525);
            }
         }
      }

   }

   public void method3590(int i_1, int i_2, byte b_3) {
      i_1 -= this.field2103;
      i_2 -= this.field2104;
      this.field2107[i_1][i_2] &= ~0x40000;
   }

   public void method3588(int i_1, int i_2, int i_3, int i_4, int i_5, boolean bool_6, byte b_7) {
      int i_8 = 256;
      if (bool_6) {
         i_8 += 131072;
      }

      i_1 -= this.field2103;
      i_2 -= this.field2104;
      int i_9;
      if (i_5 == 1 || i_5 == 3) {
         i_9 = i_3;
         i_3 = i_4;
         i_4 = i_9;
      }

      for (i_9 = i_1; i_9 < i_3 + i_1; i_9++) {
         if (i_9 >= 0 && i_9 < this.field2106) {
            for (int i_10 = i_2; i_10 < i_2 + i_4; i_10++) {
               if (i_10 >= 0 && i_10 < this.field2099) {
                  this.method3589(i_9, i_10, i_8, 1635442704);
               }
            }
         }
      }

   }

   public void method3596(int i_1, int i_2, int i_3) {
      i_1 -= this.field2103;
      i_2 -= this.field2104;
      this.field2107[i_1][i_2] |= 0x40000;
   }

   public void method3586(int i_1, int i_2, int i_3, int i_4, boolean bool_5, byte b_6) {
      int i_7 = 256;
      if (bool_5) {
         i_7 += 131072;
      }

      i_1 -= this.field2103;
      i_2 -= this.field2104;

      for (int i_8 = i_1; i_8 < i_3 + i_1; i_8++) {
         if (i_8 >= 0 && i_8 < this.field2106) {
            for (int i_9 = i_2; i_9 < i_2 + i_4; i_9++) {
               if (i_9 >= 0 && i_9 < this.field2099) {
                  this.method3607(i_8, i_9, i_7, (byte) 4);
               }
            }
         }
      }

   }

   public void method3582(int i_1, int i_2, int i_3, int i_4, boolean bool_5, byte b_6) {
      i_1 -= this.field2103;
      i_2 -= this.field2104;
      if (i_3 == 0) {
         if (i_4 == 0) {
            this.method3607(i_1, i_2, 128, (byte) 4);
            this.method3607(i_1 - 1, i_2, 8, (byte) 4);
         }

         if (i_4 == 1) {
            this.method3607(i_1, i_2, 2, (byte) 4);
            this.method3607(i_1, i_2 + 1, 32, (byte) 4);
         }

         if (i_4 == 2) {
            this.method3607(i_1, i_2, 8, (byte) 4);
            this.method3607(i_1 + 1, i_2, 128, (byte) 4);
         }

         if (i_4 == 3) {
            this.method3607(i_1, i_2, 32, (byte) 4);
            this.method3607(i_1, i_2 - 1, 2, (byte) 4);
         }
      }

      if (i_3 == 1 || i_3 == 3) {
         if (i_4 == 0) {
            this.method3607(i_1, i_2, 1, (byte) 4);
            this.method3607(i_1 - 1, i_2 + 1, 16, (byte) 4);
         }

         if (i_4 == 1) {
            this.method3607(i_1, i_2, 4, (byte) 4);
            this.method3607(i_1 + 1, i_2 + 1, 64, (byte) 4);
         }

         if (i_4 == 2) {
            this.method3607(i_1, i_2, 16, (byte) 4);
            this.method3607(i_1 + 1, i_2 - 1, 1, (byte) 4);
         }

         if (i_4 == 3) {
            this.method3607(i_1, i_2, 64, (byte) 4);
            this.method3607(i_1 - 1, i_2 - 1, 4, (byte) 4);
         }
      }

      if (i_3 == 2) {
         if (i_4 == 0) {
            this.method3607(i_1, i_2, 130, (byte) 4);
            this.method3607(i_1 - 1, i_2, 8, (byte) 4);
            this.method3607(i_1, i_2 + 1, 32, (byte) 4);
         }

         if (i_4 == 1) {
            this.method3607(i_1, i_2, 10, (byte) 4);
            this.method3607(i_1, i_2 + 1, 32, (byte) 4);
            this.method3607(i_1 + 1, i_2, 128, (byte) 4);
         }

         if (i_4 == 2) {
            this.method3607(i_1, i_2, 40, (byte) 4);
            this.method3607(i_1 + 1, i_2, 128, (byte) 4);
            this.method3607(i_1, i_2 - 1, 2, (byte) 4);
         }

         if (i_4 == 3) {
            this.method3607(i_1, i_2, 160, (byte) 4);
            this.method3607(i_1, i_2 - 1, 2, (byte) 4);
            this.method3607(i_1 - 1, i_2, 8, (byte) 4);
         }
      }

      if (bool_5) {
         if (i_3 == 0) {
            if (i_4 == 0) {
               this.method3607(i_1, i_2, 65536, (byte) 4);
               this.method3607(i_1 - 1, i_2, 4096, (byte) 4);
            }

            if (i_4 == 1) {
               this.method3607(i_1, i_2, 1024, (byte) 4);
               this.method3607(i_1, i_2 + 1, 16384, (byte) 4);
            }

            if (i_4 == 2) {
               this.method3607(i_1, i_2, 4096, (byte) 4);
               this.method3607(i_1 + 1, i_2, 65536, (byte) 4);
            }

            if (i_4 == 3) {
               this.method3607(i_1, i_2, 16384, (byte) 4);
               this.method3607(i_1, i_2 - 1, 1024, (byte) 4);
            }
         }

         if (i_3 == 1 || i_3 == 3) {
            if (i_4 == 0) {
               this.method3607(i_1, i_2, 512, (byte) 4);
               this.method3607(i_1 - 1, i_2 + 1, 8192, (byte) 4);
            }

            if (i_4 == 1) {
               this.method3607(i_1, i_2, 2048, (byte) 4);
               this.method3607(i_1 + 1, i_2 + 1, 32768, (byte) 4);
            }

            if (i_4 == 2) {
               this.method3607(i_1, i_2, 8192, (byte) 4);
               this.method3607(i_1 + 1, i_2 - 1, 512, (byte) 4);
            }

            if (i_4 == 3) {
               this.method3607(i_1, i_2, 32768, (byte) 4);
               this.method3607(i_1 - 1, i_2 - 1, 2048, (byte) 4);
            }
         }

         if (i_3 == 2) {
            if (i_4 == 0) {
               this.method3607(i_1, i_2, 66560, (byte) 4);
               this.method3607(i_1 - 1, i_2, 4096, (byte) 4);
               this.method3607(i_1, i_2 + 1, 16384, (byte) 4);
            }

            if (i_4 == 1) {
               this.method3607(i_1, i_2, 5120, (byte) 4);
               this.method3607(i_1, i_2 + 1, 16384, (byte) 4);
               this.method3607(i_1 + 1, i_2, 65536, (byte) 4);
            }

            if (i_4 == 2) {
               this.method3607(i_1, i_2, 20480, (byte) 4);
               this.method3607(i_1 + 1, i_2, 65536, (byte) 4);
               this.method3607(i_1, i_2 - 1, 1024, (byte) 4);
            }

            if (i_4 == 3) {
               this.method3607(i_1, i_2, 81920, (byte) 4);
               this.method3607(i_1, i_2 - 1, 1024, (byte) 4);
               this.method3607(i_1 - 1, i_2, 4096, (byte) 4);
            }
         }
      }

   }

   static void method3609(class226 class226_0, int i_1) {
      if (class226_0.field2596 == client.field852) {
         client.field901[class226_0.field2723] = true;
      }

   }

   public static class226 method3610(int i_0, int i_1) {
      int i_2 = i_0 >> 16;
      int i_3 = i_0 & 0xffff;
      if (class9.field44[i_2] == null || class9.field44[i_2][i_3] == null) {
         boolean bool_4 = class41.method603(i_2, -1462929262);
         if (!bool_4) {
            return null;
         }
      }

      return class9.field44[i_2][i_3];
   }

}
