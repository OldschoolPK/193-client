public class class27 extends class43 {

   public static class248 field148;
   static client field147;
   public static class332 field150;
   static byte[][] field151;
   static int field146;
   final int field149;
   final class37 field144;
   final int field145;
   final int field143;

   class27(class222 class222_1, class222 class222_2, int i_3, class37 class37_4) {
      super(class222_1, class222_2);
      this.field149 = i_3;
      this.field144 = class37_4;
      class252 class252_5 = class163.method3458(this.vmethod630(252709048), -2049952218);
      class335 class335_6 = class252_5.method4374(false, 1847670764);
      if (class335_6 != null) {
         this.field145 = class335_6.field3913;
         this.field143 = class335_6.field3911;
      } else {
         this.field145 = 0;
         this.field143 = 0;
      }

   }

   class37 vmethod633(int i_1) {
      return this.field144;
   }

   public int vmethod630(int i_1) {
      return this.field149;
   }

   int vmethod654(byte b_1) {
      return this.field145;
   }

   int vmethod635(int i_1) {
      return this.field143;
   }

   public static class267 method272(int i_0, int i_1) {
      class267 class267_2 = (class267) class267.field3496.method3376((long)i_0);
      if (class267_2 != null) {
         return class267_2;
      } else {
         byte[] bytes_3 = class267.field3522.method4160(9, i_0, (short) -28068);
         class267_2 = new class267();
         class267_2.field3531 = i_0;
         if (bytes_3 != null) {
            class267_2.method4700(new class310(bytes_3), -174515624);
         }

         class267_2.method4699(1458839985);
         class267.field3496.method3374(class267_2, (long)i_0);
         return class267_2;
      }
   }

   public static final void method270(int i_0, boolean bool_1, int i_2, int i_3) {
      if (i_0 >= 8000 && i_0 <= 48000) {
         class114.field1443 = i_0;
         class114.field1421 = bool_1;
         class114.field1424 = i_2;
      } else {
         throw new IllegalArgumentException();
      }
   }

}
