public class class163 extends class285 {

   final boolean field1997;

   public class163(boolean bool_1) {
      this.field1997 = bool_1;
   }

   int method3459(class289 class289_1, class289 class289_2, int i_3) {
      return client.field645 == class289_1.field3650 && class289_2.field3650 == client.field645 ? (this.field1997 ? class289_1.field3649 - class289_2.field3649 : class289_2.field3649 - class289_1.field3649) : this.method5051(class289_1, class289_2, 490417183);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3459((class289) object_1, (class289) object_2, 620956476);
   }

   public static class252 method3458(int i_0, int i_1) {
      return i_0 >= 0 && i_0 < class252.field3243.length && class252.field3243[i_0] != null ? class252.field3243[i_0] : new class252(i_0);
   }

   static void method3462(int i_0, String string_1, String string_2, String string_3, int i_4) {
      class103 class103_5 = (class103) class100.field1288.get(Integer.valueOf(i_0));
      if (class103_5 == null) {
         class103_5 = new class103();
         class100.field1288.put(Integer.valueOf(i_0), class103_5);
      }

      class73 class73_6 = class103_5.method2280(i_0, string_1, string_2, string_3, 2065978922);
      class100.field1285.method5928(class73_6, (long)class73_6.field598);
      class100.field1284.method4963(class73_6);
      client.field840 = client.field737;
   }

   static char method3464(char var_0, int i_1) {
      switch(var_0) {
      case ' ':
      case '-':
      case '_':
      case ' ':
         return '_';
      case '#':
      case '[':
      case ']':
         return var_0;
      case 'À':
      case 'Á':
      case 'Â':
      case 'Ã':
      case 'Ä':
      case 'à':
      case 'á':
      case 'â':
      case 'ã':
      case 'ä':
         return 'a';
      case 'Ç':
      case 'ç':
         return 'c';
      case 'È':
      case 'É':
      case 'Ê':
      case 'Ë':
      case 'è':
      case 'é':
      case 'ê':
      case 'ë':
         return 'e';
      case 'Í':
      case 'Î':
      case 'Ï':
      case 'í':
      case 'î':
      case 'ï':
         return 'i';
      case 'Ñ':
      case 'ñ':
         return 'n';
      case 'Ò':
      case 'Ó':
      case 'Ô':
      case 'Õ':
      case 'Ö':
      case 'ò':
      case 'ó':
      case 'ô':
      case 'õ':
      case 'ö':
         return 'o';
      case 'Ù':
      case 'Ú':
      case 'Û':
      case 'Ü':
      case 'ù':
      case 'ú':
      case 'û':
      case 'ü':
         return 'u';
      case 'ß':
         return 'b';
      case 'ÿ':
      case 'Ÿ':
         return 'y';
      default:
         return Character.toLowerCase(var_0);
      }
   }

   static final void method3460(class309 class309_0, int i_1, byte b_2) {
      int i_3 = class309_0.field3751;
      class98.field1262 = 0;
      class166.method3487(class309_0, (byte) 10);

      for (int i_4 = 0; i_4 < class98.field1262; i_4++) {
         int i_5 = class98.field1265[i_4];
         class75 class75_6 = client.field764[i_5];
         int i_7 = class309_0.method5661((byte) 30);
         if ((i_7 & 0x40) != 0) {
            i_7 += class309_0.method5661((byte) 2) << 8;
         }

         class45.method709(class309_0, i_5, class75_6, i_7, -2082174377);
      }

      if (class309_0.field3751 - i_3 != i_1) {
         throw new RuntimeException(class309_0.field3751 - i_3 + " " + i_1);
      }
   }

}
