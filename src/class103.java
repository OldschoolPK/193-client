public class class103 {

   static int field1322;
   class73[] field1323 = new class73[100];
   int field1320;

   class73 method2280(int i_1, String string_2, String string_3, String string_4, int i_5) {
      class73 class73_6 = this.field1323[99];

      for (int i_7 = this.field1320; i_7 > 0; --i_7) {
         if (i_7 != 100) {
            this.field1323[i_7] = this.field1323[i_7 - 1];
         }
      }

      if (class73_6 == null) {
         class73_6 = new class73(i_1, string_2, string_4, string_3);
      } else {
         class73_6.method3628();
         class73_6.method3622();
         class73_6.method1268(i_1, string_2, string_4, string_3, 914812055);
      }

      this.field1323[0] = class73_6;
      if (this.field1320 < 100) {
         ++this.field1320;
      }

      return class73_6;
   }

   class73 method2281(int i_1, int i_2) {
      return i_1 >= 0 && i_1 < this.field1320 ? this.field1323[i_1] : null;
   }

   int method2282(int i_1) {
      return this.field1320;
   }

   public static class260 method2288(int i_0, int i_1) {
      class260 class260_2 = (class260) class260.field3329.method3376((long)i_0);
      if (class260_2 != null) {
         return class260_2;
      } else {
         byte[] bytes_3 = class260.field3334.method4160(11, i_0, (short) -2504);
         class260_2 = new class260();
         if (bytes_3 != null) {
            class260_2.method4511(new class310(bytes_3), 65535);
         }

         class260_2.method4494((byte) 10);
         class260.field3329.method3374(class260_2, (long)i_0);
         return class260_2;
      }
   }

   static void method2287(class80[] arr_0, int i_1, int i_2, int[] ints_3, int[] ints_4, int i_5) {
      if (i_1 < i_2) {
         int i_6 = i_1 - 1;
         int i_7 = i_2 + 1;
         int i_8 = (i_2 + i_1) / 2;
         class80 class80_9 = arr_0[i_8];
         arr_0[i_8] = arr_0[i_1];
         arr_0[i_1] = class80_9;

         while (i_6 < i_7) {
            boolean bool_10 = true;

            int i_11;
            int i_12;
            int i_13;
            do {
               --i_7;

               for (i_11 = 0; i_11 < 4; i_11++) {
                  if (ints_3[i_11] == 2) {
                     i_12 = arr_0[i_7].field1024;
                     i_13 = class80_9.field1024;
                  } else if (ints_3[i_11] == 1) {
                     i_12 = arr_0[i_7].field1020;
                     i_13 = class80_9.field1020;
                     if (i_12 == -1 && ints_4[i_11] == 1) {
                        i_12 = 2001;
                     }

                     if (i_13 == -1 && ints_4[i_11] == 1) {
                        i_13 = 2001;
                     }
                  } else if (ints_3[i_11] == 3) {
                     i_12 = arr_0[i_7].method1800((byte) 1) ? 1 : 0;
                     i_13 = class80_9.method1800((byte) 1) ? 1 : 0;
                  } else {
                     i_12 = arr_0[i_7].field1018;
                     i_13 = class80_9.field1018;
                  }

                  if (i_12 != i_13) {
                     if ((ints_4[i_11] != 1 || i_12 <= i_13) && (ints_4[i_11] != 0 || i_12 >= i_13)) {
                        bool_10 = false;
                     }
                     break;
                  }

                  if (i_11 == 3) {
                     bool_10 = false;
                  }
               }
            } while (bool_10);

            bool_10 = true;

            do {
               ++i_6;

               for (i_11 = 0; i_11 < 4; i_11++) {
                  if (ints_3[i_11] == 2) {
                     i_12 = arr_0[i_6].field1024;
                     i_13 = class80_9.field1024;
                  } else if (ints_3[i_11] == 1) {
                     i_12 = arr_0[i_6].field1020;
                     i_13 = class80_9.field1020;
                     if (i_12 == -1 && ints_4[i_11] == 1) {
                        i_12 = 2001;
                     }

                     if (i_13 == -1 && ints_4[i_11] == 1) {
                        i_13 = 2001;
                     }
                  } else if (ints_3[i_11] == 3) {
                     i_12 = arr_0[i_6].method1800((byte) 1) ? 1 : 0;
                     i_13 = class80_9.method1800((byte) 1) ? 1 : 0;
                  } else {
                     i_12 = arr_0[i_6].field1018;
                     i_13 = class80_9.field1018;
                  }

                  if (i_12 != i_13) {
                     if ((ints_4[i_11] != 1 || i_12 >= i_13) && (ints_4[i_11] != 0 || i_12 <= i_13)) {
                        bool_10 = false;
                     }
                     break;
                  }

                  if (i_11 == 3) {
                     bool_10 = false;
                  }
               }
            } while (bool_10);

            if (i_6 < i_7) {
               class80 class80_14 = arr_0[i_6];
               arr_0[i_6] = arr_0[i_7];
               arr_0[i_7] = class80_14;
            }
         }

         method2287(arr_0, i_1, i_7, ints_3, ints_4, -156492975);
         method2287(arr_0, i_7 + 1, i_2, ints_3, ints_4, -930013020);
      }

   }

   static class101 method2290(int i_0, int i_1) {
      class101 class101_2 = (class101) class101.field1299.method3376((long)i_0);
      if (class101_2 != null) {
         return class101_2;
      } else {
         byte[] bytes_3 = class196.field2361.method4160(i_0, 0, (short) -23689);
         if (bytes_3 == null) {
            return null;
         } else {
            class101_2 = class78.method1788(bytes_3, (byte) 0);
            class101.field1299.method3374(class101_2, (long)i_0);
            return class101_2;
         }
      }
   }

   public static void method2289(int i_0, byte b_1) {
      class63.field486 = i_0;
   }

}
