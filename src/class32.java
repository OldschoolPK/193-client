public class class32 extends class43 {

   public static char field200;
   static boolean field207;
   final int field204;
   final class38 field201;
   int field202;
   class37 field203;
   int field205;
   int field208;

   class32(class222 class222_1, class222 class222_2, int i_3, class38 class38_4) {
      super(class222_1, class222_2);
      this.field204 = i_3;
      this.field201 = class38_4;
      this.method349(19573601);
   }

   class37 vmethod633(int i_1) {
      return this.field203;
   }

   public int vmethod630(int i_1) {
      return this.field202;
   }

   void method349(int i_1) {
      this.field202 = class34.method383(this.field204, 1515842788).method4599(2042384515).field3414;
      this.field203 = this.field201.method500(class163.method3458(this.field202, -2054168716), -1657578860);
      class252 class252_2 = class163.method3458(this.vmethod630(90842686), -2105424128);
      class335 class335_3 = class252_2.method4374(false, 610555608);
      if (class335_3 != null) {
         this.field205 = class335_3.field3913;
         this.field208 = class335_3.field3911;
      } else {
         this.field205 = 0;
         this.field208 = 0;
      }

   }

   int vmethod654(byte b_1) {
      return this.field205;
   }

   int vmethod635(int i_1) {
      return this.field208;
   }

   public static boolean method355(int i_0) {
      try {
         if (class206.field2421 == 2) {
            if (class178.field2071 == null) {
               class178.field2071 = class212.method3929(class221.field2538, class79.field1002, class206.field2425);
               if (class178.field2071 == null) {
                  return false;
               }
            }

            if (class206.field2424 == null) {
               class206.field2424 = new class117(class206.field2422, class206.field2420);
            }

            if (class206.field2426.method3722(class178.field2071, class206.field2423, class206.field2424, 22050, (byte) 3)) {
               class206.field2426.method3723((byte) 51);
               class206.field2426.method3720(class16.field71, (short) 3410);
               class206.field2426.method3805(class178.field2071, class231.field2772, 614971049);
               class206.field2421 = 0;
               class178.field2071 = null;
               class206.field2424 = null;
               class221.field2538 = null;
               return true;
            }
         }
      } catch (Exception exception_2) {
         exception_2.printStackTrace();
         class206.field2426.method3799((short) 6203);
         class206.field2421 = 0;
         class178.field2071 = null;
         class206.field2424 = null;
         class221.field2538 = null;
      }

      return false;
   }

   static void method354(byte b_0) {
      if (class175.field2052.toLowerCase().indexOf("microsoft") != -1) {
         class54.field398[186] = 57;
         class54.field398[187] = 27;
         class54.field398[188] = 71;
         class54.field398[189] = 26;
         class54.field398[190] = 72;
         class54.field398[191] = 73;
         class54.field398[192] = 58;
         class54.field398[219] = 42;
         class54.field398[220] = 74;
         class54.field398[221] = 43;
         class54.field398[222] = 59;
         class54.field398[223] = 28;
      } else {
         class54.field398[44] = 71;
         class54.field398[45] = 26;
         class54.field398[46] = 72;
         class54.field398[47] = 73;
         class54.field398[59] = 57;
         class54.field398[61] = 27;
         class54.field398[91] = 42;
         class54.field398[92] = 74;
         class54.field398[93] = 43;
         class54.field398[192] = 28;
         class54.field398[222] = 58;
         class54.field398[520] = 59;
      }

   }

   public static void method339(int i_0) {
      class267.field3496.method3375();
      class267.field3497.method3375();
   }

   static int method336(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      class226 class226_4 = class181.method3610(class85.field1095[--class253.field3267], 245109549);
      if (i_0 == 2600) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2607;
         return 1;
      } else if (i_0 == 2601) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2608;
         return 1;
      } else if (i_0 == 2602) {
         class85.field1096[++class85.field1105 - 1] = class226_4.field2647;
         return 1;
      } else if (i_0 == 2603) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2609;
         return 1;
      } else if (i_0 == 2604) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2639;
         return 1;
      } else if (i_0 == 2605) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2640;
         return 1;
      } else if (i_0 == 2606) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2583;
         return 1;
      } else if (i_0 == 2607) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2657;
         return 1;
      } else if (i_0 == 2608) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2638;
         return 1;
      } else if (i_0 == 2609) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2652;
         return 1;
      } else if (i_0 == 2610) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2636;
         return 1;
      } else if (i_0 == 2611) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2611;
         return 1;
      } else if (i_0 == 2612) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2612;
         return 1;
      } else if (i_0 == 2613) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2616.vmethod6086(-2022540287);
         return 1;
      } else if (i_0 == 2614) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2673 ? 1 : 0;
         return 1;
      } else {
         return 2;
      }
   }

   static int method345(int i_0, class101 class101_1, boolean bool_2, short s_3) {
      class226 class226_4;
      if (i_0 == 2700) {
         class226_4 = class181.method3610(class85.field1095[--class253.field3267], -1745800759);
         class85.field1095[++class253.field3267 - 1] = class226_4.field2712;
         return 1;
      } else if (i_0 == 2701) {
         class226_4 = class181.method3610(class85.field1095[--class253.field3267], -1593654051);
         if (class226_4.field2712 != -1) {
            class85.field1095[++class253.field3267 - 1] = class226_4.field2713;
         } else {
            class85.field1095[++class253.field3267 - 1] = 0;
         }

         return 1;
      } else if (i_0 == 2702) {
         int i_6 = class85.field1095[--class253.field3267];
         class70 class70_5 = (class70) client.field833.method5968((long)i_6);
         if (class70_5 != null) {
            class85.field1095[++class253.field3267 - 1] = 1;
         } else {
            class85.field1095[++class253.field3267 - 1] = 0;
         }

         return 1;
      } else if (i_0 == 2706) {
         class85.field1095[++class253.field3267 - 1] = client.field873;
         return 1;
      } else {
         return 2;
      }
   }

}
