import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

public class class205 {

   int field2416;
   int field2410;
   int field2415;
   byte[] field2411;
   int field2409;
   int field2414;
   byte[] field2412;
   int field2417;
   int field2413;

   public static int method3692(int i_0, int i_1, byte b_2) {
      int i_3;
      for (i_3 = 0; i_1 > 0; --i_1) {
         i_3 = i_3 << 1 | i_0 & 0x1;
         i_0 >>>= 1;
      }

      return i_3;
   }

   static final int method3693(long long_0, String string_2, short s_3) {
      Random random_4 = new Random();
      class310 class310_5 = new class310(128);
      class310 class310_6 = new class310(128);
      int[] ints_7 = new int[] {random_4.nextInt(), random_4.nextInt(), (int)(long_0 >> 32), (int)long_0};
      class310_5.method5482(10, (byte) -106);

      int i_8;
      for (i_8 = 0; i_8 < 4; i_8++) {
         class310_5.method5510(random_4.nextInt(), (byte) -14);
      }

      class310_5.method5510(ints_7[0], (byte) -55);
      class310_5.method5510(ints_7[1], (byte) -53);
      class310_5.method5490(long_0);
      class310_5.method5490(0L);

      for (i_8 = 0; i_8 < 4; i_8++) {
         class310_5.method5510(random_4.nextInt(), (byte) -108);
      }

      class310_5.method5500(class89.field1147, class89.field1142, 1943723857);
      class310_6.method5482(10, (byte) -100);

      for (i_8 = 0; i_8 < 3; i_8++) {
         class310_6.method5510(random_4.nextInt(), (byte) -65);
      }

      class310_6.method5490(random_4.nextLong());
      class310_6.method5591(random_4.nextLong());
      class178.method3545(class310_6, -1428614163);
      class310_6.method5490(random_4.nextLong());
      class310_6.method5500(class89.field1147, class89.field1142, -1516127872);
      i_8 = class132.method2906(string_2, (byte) 46);
      if (i_8 % 8 != 0) {
         i_8 += 8 - i_8 % 8;
      }

      class310 class310_9 = new class310(i_8);
      class310_9.method5492(string_2, -1748540705);
      class310_9.field3751 = i_8;
      class310_9.method5725(ints_7, (byte) 0);
      class310 class310_10 = new class310(class310_9.field3751 + class310_6.field3751 + class310_5.field3751 + 5);
      class310_10.method5482(2, (byte) -104);
      class310_10.method5482(class310_5.field3751, (byte) -108);
      class310_10.method5624(class310_5.field3752, 0, class310_5.field3751, 1585895405);
      class310_10.method5482(class310_6.field3751, (byte) -105);
      class310_10.method5624(class310_6.field3752, 0, class310_6.field3751, 1585895405);
      class310_10.method5486(class310_9.field3751, (byte) -99);
      class310_10.method5624(class310_9.field3752, 0, class310_9.field3751, 1585895405);
      byte[] bytes_12 = class310_10.field3752;
      String string_11 = class67.method1229(bytes_12, 0, bytes_12.length, 919177129);
      String string_13 = string_11;

      try {
         URL url_14 = new URL(class43.method632("services", false, (byte) 104) + "m=accountappeal/login.ws");
         URLConnection urlconnection_15 = url_14.openConnection();
         urlconnection_15.setDoInput(true);
         urlconnection_15.setDoOutput(true);
         urlconnection_15.setConnectTimeout(5000);
         OutputStreamWriter outputstreamwriter_16 = new OutputStreamWriter(urlconnection_15.getOutputStream());
         outputstreamwriter_16.write("data2=" + class145.method3315(string_13, -2018740830) + "&dest=" + class145.method3315("passwordchoice.ws", 14163706));
         outputstreamwriter_16.flush();
         InputStream inputstream_17 = urlconnection_15.getInputStream();
         class310_10 = new class310(new byte[1000]);

         do {
            int i_18 = inputstream_17.read(class310_10.field3752, class310_10.field3751, 1000 - class310_10.field3751);
            if (i_18 == -1) {
               outputstreamwriter_16.close();
               inputstream_17.close();
               String string_21 = new String(class310_10.field3752);
               if (string_21.startsWith("OFFLINE")) {
                  return 4;
               } else if (string_21.startsWith("WRONG")) {
                  return 7;
               } else if (string_21.startsWith("RELOAD")) {
                  return 3;
               } else if (string_21.startsWith("Not permitted for social network accounts.")) {
                  return 6;
               } else {
                  class310_10.method5575(ints_7, 847845555);

                  while (class310_10.field3751 > 0 && class310_10.field3752[class310_10.field3751 - 1] == 0) {
                     --class310_10.field3751;
                  }

                  string_21 = new String(class310_10.field3752, 0, class310_10.field3751);
                  boolean bool_19;
                  if (string_21 == null) {
                     bool_19 = false;
                  } else {
                     label85: {
                        try {
                           new URL(string_21);
                        } catch (MalformedURLException malformedurlexception_22) {
                           bool_19 = false;
                           break label85;
                        }

                        bool_19 = true;
                     }
                  }

                  if (bool_19) {
                     class93.method2109(string_21, true, false, 1472993043);
                     return 2;
                  } else {
                     return 5;
                  }
               }
            }

            class310_10.field3751 += i_18;
         } while (class310_10.field3751 < 1000);

         return 5;
      } catch (Throwable throwable_23) {
         throwable_23.printStackTrace();
         return 5;
      }
   }

}
