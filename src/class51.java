public class class51 implements class46 {

   int field391;
   int field383;
   int field384;
   int field388;
   int field385;
   int field389;
   int field392;
   int field390;
   int field387;
   int field393;

   public boolean vmethod814(int i_1, int i_2, int i_3) {
      return i_1 >= (this.field392 << 6) + (this.field390 << 3) && i_1 <= (this.field392 << 6) + (this.field390 << 3) + 7 && i_2 >= (this.field387 << 6) + (this.field393 << 3) && i_2 <= (this.field387 << 6) + (this.field393 << 3) + 7;
   }

   public boolean vmethod813(int i_1, int i_2, int i_3, int i_4) {
      return i_1 >= this.field391 && i_1 < this.field391 + this.field383 ? i_2 >= (this.field384 << 6) + (this.field388 << 3) && i_2 <= (this.field384 << 6) + (this.field388 << 3) + 7 && i_3 >= (this.field385 << 6) + (this.field389 << 3) && i_3 <= (this.field385 << 6) + (this.field389 << 3) + 7 : false;
   }

   public void vmethod812(class35 class35_1, short s_2) {
      if (class35_1.field234 > this.field392) {
         class35_1.field234 = this.field392;
      }

      if (class35_1.field228 < this.field392) {
         class35_1.field228 = this.field392;
      }

      if (class35_1.field236 > this.field387) {
         class35_1.field236 = this.field387;
      }

      if (class35_1.field239 < this.field387) {
         class35_1.field239 = this.field387;
      }

   }

   public class222 vmethod835(int i_1, int i_2, byte b_3) {
      if (!this.vmethod814(i_1, i_2, 639074408)) {
         return null;
      } else {
         int i_4 = this.field384 * 64 - this.field392 * 64 + (this.field388 * 8 - this.field390 * 8) + i_1;
         int i_5 = this.field385 * 64 - this.field387 * 64 + i_2 + (this.field389 * 8 - this.field393 * 8);
         return new class222(this.field391, i_4, i_5);
      }
   }

   public int[] vmethod819(int i_1, int i_2, int i_3, int i_4) {
      if (!this.vmethod813(i_1, i_2, i_3, -73664065)) {
         return null;
      } else {
         int[] ints_5 = new int[] {this.field392 * 64 - this.field384 * 64 + i_2 + (this.field390 * 8 - this.field388 * 8), i_3 + (this.field387 * 64 - this.field385 * 64) + (this.field393 * 8 - this.field389 * 8)};
         return ints_5;
      }
   }

   public void vmethod825(class310 class310_1, byte b_2) {
      this.field391 = class310_1.method5661((byte) -81);
      this.field383 = class310_1.method5661((byte) -28);
      this.field384 = class310_1.method5729(-1165874062);
      this.field388 = class310_1.method5661((byte) -18);
      this.field385 = class310_1.method5729(-435213647);
      this.field389 = class310_1.method5661((byte) -39);
      this.field392 = class310_1.method5729(-1707280945);
      this.field390 = class310_1.method5661((byte) 5);
      this.field387 = class310_1.method5729(572544054);
      this.field393 = class310_1.method5661((byte) -19);
      this.method817((byte) -70);
   }

   void method817(byte b_1) {
   }

   public static class334 method836(class244 class244_0, String string_1, String string_2, int i_3) {
      int i_4 = class244_0.method4206(string_1, -1966646196);
      int i_5 = class244_0.method4235(i_4, string_2, 1284626629);
      class334 class334_6;
      if (!class304.method5323(class244_0, i_4, i_5, 975575420)) {
         class334_6 = null;
      } else {
         class334_6 = class320.method5894((byte) 64);
      }

      return class334_6;
   }

   static void method832(int i_0) {
      if (client.field725) {
         class82.method1879(class223.field2562, false, 120078652);
      }

   }

   static final void method829(class226[] arr_0, int i_1, int i_2) {
      for (int i_3 = 0; i_3 < arr_0.length; i_3++) {
         class226 class226_4 = arr_0[i_3];
         if (class226_4 != null) {
            if (class226_4.field2588 == 0) {
               if (class226_4.field2672 != null) {
                  method829(class226_4.field2672, i_1, 2035506733);
               }

               class70 class70_5 = (class70) client.field833.method5968((long)class226_4.field2586);
               if (class70_5 != null) {
                  class31.method330(class70_5.field563, i_1, -1776360801);
               }
            }

            class71 class71_6;
            if (i_1 == 0 && class226_4.field2699 != null) {
               class71_6 = new class71();
               class71_6.field571 = class226_4;
               class71_6.field572 = class226_4.field2699;
               class22.method219(class71_6, -1542708065);
            }

            if (i_1 == 1 && class226_4.field2610 != null) {
               if (class226_4.field2587 >= 0) {
                  class226 class226_7 = class181.method3610(class226_4.field2586, -1080109171);
                  if (class226_7 == null || class226_7.field2672 == null || class226_4.field2587 >= class226_7.field2672.length || class226_4 != class226_7.field2672[class226_4.field2587]) {
                     continue;
                  }
               }

               class71_6 = new class71();
               class71_6.field571 = class226_4;
               class71_6.field572 = class226_4.field2610;
               class22.method219(class71_6, -1238818938);
            }
         }
      }

   }

   public static int method815(int i_0, int i_1, int i_2, int i_3) {
      i_2 &= 0x3;
      return i_2 == 0 ? i_0 : (i_2 == 1 ? i_1 : (i_2 == 2 ? 7 - i_0 : 7 - i_1));
   }

   static final void method831(class78 class78_0, int i_1) {
      class78_0.field967 = class78_0.field943;
      if (class78_0.field985 == 0) {
         class78_0.field963 = 0;
      } else {
         if (class78_0.field970 != -1 && class78_0.field937 == 0) {
            class269 class269_2 = class260.method4510(class78_0.field970, -1358760869);
            if (class78_0.field996 > 0 && class269_2.field3560 == 0) {
               ++class78_0.field963;
               return;
            }

            if (class78_0.field996 <= 0 && class269_2.field3555 == 0) {
               ++class78_0.field963;
               return;
            }
         }

         int i_11 = class78_0.field990;
         int i_3 = class78_0.field938;
         int i_4 = class78_0.field993[class78_0.field985 - 1] * 128 + class78_0.field941 * 64;
         int i_5 = class78_0.field968[class78_0.field985 - 1] * 128 + class78_0.field941 * 64;
         if (i_11 < i_4) {
            if (i_3 < i_5) {
               class78_0.field989 = 1280;
            } else if (i_3 > i_5) {
               class78_0.field989 = 1792;
            } else {
               class78_0.field989 = 1536;
            }
         } else if (i_11 > i_4) {
            if (i_3 < i_5) {
               class78_0.field989 = 768;
            } else if (i_3 > i_5) {
               class78_0.field989 = 256;
            } else {
               class78_0.field989 = 512;
            }
         } else if (i_3 < i_5) {
            class78_0.field989 = 1024;
         } else if (i_3 > i_5) {
            class78_0.field989 = 0;
         }

         byte b_6 = class78_0.field995[class78_0.field985 - 1];
         if (i_4 - i_11 <= 256 && i_4 - i_11 >= -256 && i_5 - i_3 <= 256 && i_5 - i_3 >= -256) {
            int i_7 = class78_0.field989 - class78_0.field939 & 0x7ff;
            if (i_7 > 1024) {
               i_7 -= 2048;
            }

            int i_8 = class78_0.field947;
            if (i_7 >= -256 && i_7 <= 256) {
               i_8 = class78_0.field946;
            } else if (i_7 >= 256 && i_7 < 768) {
               i_8 = class78_0.field949;
            } else if (i_7 >= -768 && i_7 <= -256) {
               i_8 = class78_0.field948;
            }

            if (i_8 == -1) {
               i_8 = class78_0.field946;
            }

            class78_0.field967 = i_8;
            int i_9 = 4;
            boolean bool_10 = true;
            if (class78_0 instanceof class88) {
               bool_10 = ((class88) class78_0).field1135.field3513;
            }

            if (bool_10) {
               if (class78_0.field989 != class78_0.field939 && class78_0.field980 == -1 && class78_0.field940 != 0) {
                  i_9 = 2;
               }

               if (class78_0.field985 > 2) {
                  i_9 = 6;
               }

               if (class78_0.field985 > 3) {
                  i_9 = 8;
               }

               if (class78_0.field963 > 0 && class78_0.field985 > 1) {
                  i_9 = 8;
                  --class78_0.field963;
               }
            } else {
               if (class78_0.field985 > 1) {
                  i_9 = 6;
               }

               if (class78_0.field985 > 2) {
                  i_9 = 8;
               }

               if (class78_0.field963 > 0 && class78_0.field985 > 1) {
                  i_9 = 8;
                  --class78_0.field963;
               }
            }

            if (b_6 == 2) {
               i_9 <<= 1;
            }

            if (i_9 >= 8 && class78_0.field946 == class78_0.field967 && class78_0.field942 != -1) {
               class78_0.field967 = class78_0.field942;
            }

            if (i_11 != i_4 || i_5 != i_3) {
               if (i_11 < i_4) {
                  class78_0.field990 += i_9;
                  if (class78_0.field990 > i_4) {
                     class78_0.field990 = i_4;
                  }
               } else if (i_11 > i_4) {
                  class78_0.field990 -= i_9;
                  if (class78_0.field990 < i_4) {
                     class78_0.field990 = i_4;
                  }
               }

               if (i_3 < i_5) {
                  class78_0.field938 += i_9;
                  if (class78_0.field938 > i_5) {
                     class78_0.field938 = i_5;
                  }
               } else if (i_3 > i_5) {
                  class78_0.field938 -= i_9;
                  if (class78_0.field938 < i_5) {
                     class78_0.field938 = i_5;
                  }
               }
            }

            if (i_4 == class78_0.field990 && i_5 == class78_0.field938) {
               --class78_0.field985;
               if (class78_0.field996 > 0) {
                  --class78_0.field996;
               }
            }

         } else {
            class78_0.field990 = i_4;
            class78_0.field938 = i_5;
            --class78_0.field985;
            if (class78_0.field996 > 0) {
               --class78_0.field996;
            }

         }
      }
   }

   static final void method816(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, byte b_7) {
      if (class41.method603(i_0, -335068840)) {
         class98.method2182(class9.field44[i_0], -1, i_1, i_2, i_3, i_4, i_5, i_6, -2127436431);
      }
   }

}
