public final class class86 extends class144 {

   public static String[] field1117;
   int field1110 = 0;
   int field1118 = 0;
   boolean field1120 = false;
   int field1115;
   int field1112;
   int field1113;
   int field1114;
   int field1111;
   int field1119;
   class269 field1116;

   class86(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      this.field1115 = i_1;
      this.field1112 = i_2;
      this.field1113 = i_3;
      this.field1114 = i_4;
      this.field1111 = i_5;
      this.field1119 = i_7 + i_6;
      int i_8 = class96.method2160(this.field1115, (byte) 63).field3284;
      if (i_8 != -1) {
         this.field1120 = false;
         this.field1116 = class260.method4510(i_8, -1863702242);
      } else {
         this.field1120 = true;
      }

   }

   protected final class136 vmethod3305(int i_1) {
      class254 class254_2 = class96.method2160(this.field1115, (byte) 36);
      class136 class136_3;
      if (!this.field1120) {
         class136_3 = class254_2.method4411(this.field1110, -1387550143);
      } else {
         class136_3 = class254_2.method4411(-1, 2016370935);
      }

      return class136_3 == null ? null : class136_3;
   }

   final void method2061(int i_1, int i_2) {
      if (!this.field1120) {
         this.field1118 += i_1;

         while (this.field1118 > this.field1116.field3552[this.field1110]) {
            this.field1118 -= this.field1116.field3552[this.field1110];
            ++this.field1110;
            if (this.field1110 >= this.field1116.field3550.length) {
               this.field1120 = true;
               break;
            }
         }

      }
   }

   public static void method2062(byte b_0) {
      class54 class54_1 = class54.field407;
      synchronized(class54.field407) {
         ++class54.field418;
         class54.field415 = class54.field417;
         class54.field414 = 0;
         int i_2;
         if (class54.field410 < 0) {
            for (i_2 = 0; i_2 < 112; i_2++) {
               class54.field416[i_2] = false;
            }

            class54.field410 = class54.field401;
         } else {
            while (class54.field401 != class54.field410) {
               i_2 = class54.field408[class54.field401];
               class54.field401 = class54.field401 + 1 & 0x7f;
               if (i_2 < 0) {
                  class54.field416[~i_2] = false;
               } else {
                  if (!class54.field416[i_2] && class54.field414 < class54.field413.length - 1) {
                     class54.field413[++class54.field414 - 1] = i_2;
                  }

                  class54.field416[i_2] = true;
               }
            }
         }

         if (class54.field414 > 0) {
            class54.field418 = 0;
         }

         class54.field417 = class54.field419;
      }
   }

}
