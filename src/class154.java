public final class class154 {

   class184 field1977 = new class184();
   class276 field1973 = new class276();
   int field1974;
   int field1975;
   class326 field1976;

   public class154(int i_1) {
      this.field1974 = i_1;
      this.field1975 = i_1;

      int i_2;
      for (i_2 = 1; i_2 + i_2 < i_1; i_2 += i_2) {
         ;
      }

      this.field1976 = new class326(i_2);
   }

   public class184 method3376(long long_1) {
      class184 class184_3 = (class184) this.field1976.method5919(long_1);
      if (class184_3 != null) {
         this.field1973.method4963(class184_3);
      }

      return class184_3;
   }

   public void method3374(class184 class184_1, long long_2) {
      if (this.field1975 == 0) {
         class184 class184_4 = this.field1973.method4944();
         class184_4.method3628();
         class184_4.method3622();
         if (class184_4 == this.field1977) {
            class184_4 = this.field1973.method4944();
            class184_4.method3628();
            class184_4.method3622();
         }
      } else {
         --this.field1975;
      }

      this.field1976.method5928(class184_1, long_2);
      this.field1973.method4963(class184_1);
   }

   public void method3375() {
      this.field1973.method4968();
      this.field1976.method5920();
      this.field1977 = new class184();
      this.field1975 = this.field1974;
   }

   public void method3378(long long_1) {
      class184 class184_3 = (class184) this.field1976.method5919(long_1);
      if (class184_3 != null) {
         class184_3.method3628();
         class184_3.method3622();
         ++this.field1975;
      }

   }

}
