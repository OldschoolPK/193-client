public class class221 {

   static class244 field2538;
   static class108 field2543;
   static int[] field2540 = new int[32];
   public static int[] field2539;
   public static int[] field2541;

   static {
      int i_0 = 2;

      for (int i_1 = 0; i_1 < 32; i_1++) {
         field2540[i_1] = i_0 - 1;
         i_0 += i_0;
      }

      field2539 = new int[4000];
      field2541 = new int[4000];
   }

   public static byte method3992(char var_0, int i_1) {
      byte b_2;
      if (var_0 > 0 && var_0 < 128 || var_0 >= 160 && var_0 <= 255) {
         b_2 = (byte)var_0;
      } else if (var_0 == 8364) {
         b_2 = -128;
      } else if (var_0 == 8218) {
         b_2 = -126;
      } else if (var_0 == 402) {
         b_2 = -125;
      } else if (var_0 == 8222) {
         b_2 = -124;
      } else if (var_0 == 8230) {
         b_2 = -123;
      } else if (var_0 == 8224) {
         b_2 = -122;
      } else if (var_0 == 8225) {
         b_2 = -121;
      } else if (var_0 == 710) {
         b_2 = -120;
      } else if (var_0 == 8240) {
         b_2 = -119;
      } else if (var_0 == 352) {
         b_2 = -118;
      } else if (var_0 == 8249) {
         b_2 = -117;
      } else if (var_0 == 338) {
         b_2 = -116;
      } else if (var_0 == 381) {
         b_2 = -114;
      } else if (var_0 == 8216) {
         b_2 = -111;
      } else if (var_0 == 8217) {
         b_2 = -110;
      } else if (var_0 == 8220) {
         b_2 = -109;
      } else if (var_0 == 8221) {
         b_2 = -108;
      } else if (var_0 == 8226) {
         b_2 = -107;
      } else if (var_0 == 8211) {
         b_2 = -106;
      } else if (var_0 == 8212) {
         b_2 = -105;
      } else if (var_0 == 732) {
         b_2 = -104;
      } else if (var_0 == 8482) {
         b_2 = -103;
      } else if (var_0 == 353) {
         b_2 = -102;
      } else if (var_0 == 8250) {
         b_2 = -101;
      } else if (var_0 == 339) {
         b_2 = -100;
      } else if (var_0 == 382) {
         b_2 = -98;
      } else if (var_0 == 376) {
         b_2 = -97;
      } else {
         b_2 = 63;
      }

      return b_2;
   }

   static final void method3988(String string_0, boolean bool_1, byte b_2) {
      if (client.field763) {
         byte b_3 = 4;
         int i_4 = b_3 + 6;
         int i_5 = b_3 + 6;
         int i_6 = class26.field137.method5342(string_0, 250);
         int i_7 = class26.field137.method5398(string_0, 250) * 13;
         class331.method6004(i_4 - b_3, i_5 - b_3, i_6 + b_3 + b_3, i_7 + b_3 + b_3, 0);
         class331.method6008(i_4 - b_3, i_5 - b_3, b_3 + i_6 + b_3, b_3 + i_7 + b_3, 16777215);
         class26.field137.method5349(string_0, i_4, i_5, i_6, i_7, 16777215, -1, 1, 1, 0);
         class147.method3324(i_4 - b_3, i_5 - b_3, i_6 + b_3 + b_3, b_3 + i_7 + b_3, (byte) -41);
         if (bool_1) {
            class27.field150.vmethod6077(0, 0, 1744405056);
         } else {
            class233.method4124(i_4, i_5, i_6, i_7, -851150981);
         }

      }
   }

}
