public class class80 {

   static class80[] field1012;
   static class153 field1026;
   int field1019;
   int field1024;
   int field1020;
   int field1018;
   String field1021;
   String field1022;
   int field1023;
   static int field1017 = 0;
   static int field1014 = 0;
   static int[] field1015 = new int[] {1, 1, 1, 1};
   static int[] field1016 = new int[] {0, 1, 2, 3};

   boolean method1800(byte b_1) {
      return (0x1 & this.field1019) != 0;
   }

   boolean method1805(int i_1) {
      return (0x2000000 & this.field1019) != 0;
   }

   boolean method1804(byte b_1) {
      return (0x20000000 & this.field1019) != 0;
   }

   boolean method1817(byte b_1) {
      return (0x40000000 & this.field1019) != 0;
   }

   boolean method1802(int i_1) {
      return (0x4 & this.field1019) != 0;
   }

   boolean method1803(int i_1) {
      return (0x8 & this.field1019) != 0;
   }

   boolean method1801(int i_1) {
      return (0x2 & this.field1019) != 0;
   }

   public static int method1845(CharSequence charsequence_0, int i_1, boolean bool_2, int i_3) {
      if (i_1 >= 2 && i_1 <= 36) {
         boolean bool_4 = false;
         boolean bool_5 = false;
         int i_6 = 0;
         int i_7 = charsequence_0.length();

         for (int i_8 = 0; i_8 < i_7; i_8++) {
            char var_9 = charsequence_0.charAt(i_8);
            if (i_8 == 0) {
               if (var_9 == 45) {
                  bool_4 = true;
                  continue;
               }

               if (var_9 == 43) {
                  continue;
               }
            }

            int i_11;
            if (var_9 >= 48 && var_9 <= 57) {
               i_11 = var_9 - 48;
            } else if (var_9 >= 65 && var_9 <= 90) {
               i_11 = var_9 - 55;
            } else {
               if (var_9 < 97 || var_9 > 122) {
                  throw new NumberFormatException();
               }

               i_11 = var_9 - 87;
            }

            if (i_11 >= i_1) {
               throw new NumberFormatException();
            }

            if (bool_4) {
               i_11 = -i_11;
            }

            int i_10 = i_6 * i_1 + i_11;
            if (i_10 / i_1 != i_6) {
               throw new NumberFormatException();
            }

            i_6 = i_10;
            bool_5 = true;
         }

         if (!bool_5) {
            throw new NumberFormatException();
         } else {
            return i_6;
         }
      } else {
         throw new IllegalArgumentException("" + i_1);
      }
   }

   public static int method1842(int i_0, int i_1) {
      --i_0;
      i_0 |= i_0 >>> 1;
      i_0 |= i_0 >>> 2;
      i_0 |= i_0 >>> 4;
      i_0 |= i_0 >>> 8;
      i_0 |= i_0 >>> 16;
      return i_0 + 1;
   }

   public static void method1821(class244 class244_0, class244 class244_1, int i_2) {
      class267.field3522 = class244_0;
      class267.field3495 = class244_1;
   }

   public static int method1843(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      if ((i_5 & 0x1) == 1) {
         int i_7 = i_3;
         i_3 = i_4;
         i_4 = i_7;
      }

      i_2 &= 0x3;
      return i_2 == 0 ? i_1 : (i_2 == 1 ? 7 - i_0 - (i_3 - 1) : (i_2 == 2 ? 7 - i_1 - (i_4 - 1) : i_0));
   }

   public static int method1846(int i_0, int i_1) {
      return i_0 > 0 ? 1 : (i_0 < 0 ? -1 : 0);
   }

}
