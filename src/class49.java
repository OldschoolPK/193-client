public class class49 {

   public static short[][] field366;
   static class199 field372;
   public static String field365;
   int field371;
   byte[][][] field368;

   class49(int i_1) {
      this.field371 = i_1;
   }

   void method772(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      int i_3 = 0;

      int i_4;
      int i_5;
      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[0][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[0][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[0][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[0][3] = bytes_2;
   }

   int method804(int i_1, int i_2, int i_3) {
      if (i_2 == 9) {
         i_1 = i_1 + 1 & 0x3;
      }

      if (i_2 == 10) {
         i_1 = i_1 + 3 & 0x3;
      }

      if (i_2 == 11) {
         i_1 = i_1 + 3 & 0x3;
      }

      return i_1;
   }

   void method773(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      int i_3 = 0;

      int i_4;
      int i_5;
      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[1][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_3 >= 0 && i_3 < bytes_2.length) {
               if (i_5 >= i_4 << 1) {
                  bytes_2[i_3] = -1;
               }

               ++i_3;
            } else {
               ++i_3;
            }
         }
      }

      this.field368[1][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 <= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[1][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 >= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[1][3] = bytes_2;
   }

   int method770(int i_1, byte b_2) {
      return i_1 != 9 && i_1 != 10 ? (i_1 == 11 ? 8 : i_1) : 1;
   }

   void method774(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      int i_3 = 0;

      int i_4;
      int i_5;
      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 <= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[2][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[2][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[2][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 >= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[2][3] = bytes_2;
   }

   void method791(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      int i_3 = 0;

      int i_4;
      int i_5;
      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[3][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[3][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 >= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[3][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 <= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[3][3] = bytes_2;
   }

   void method776(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      int i_3 = 0;

      int i_4;
      int i_5;
      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 >= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[4][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[4][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4 >> 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[4][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_3 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 <= i_4 << 1) {
               bytes_2[i_3] = -1;
            }

            ++i_3;
         }
      }

      this.field368[4][3] = bytes_2;
   }

   void method777(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      boolean bool_3 = false;
      bytes_2 = new byte[this.field371 * this.field371];
      int i_6 = 0;

      int i_4;
      int i_5;
      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[5][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_4 <= this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[5][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[5][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_4 >= this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[5][3] = bytes_2;
   }

   void method778(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      boolean bool_3 = false;
      bytes_2 = new byte[this.field371 * this.field371];
      int i_6 = 0;

      int i_4;
      int i_5;
      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[6][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 <= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[6][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 <= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[6][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 <= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[6][3] = bytes_2;
   }

   void method779(int i_1) {
      byte[] bytes_2 = new byte[this.field371 * this.field371];
      boolean bool_3 = false;
      bytes_2 = new byte[this.field371 * this.field371];
      int i_6 = 0;

      int i_4;
      int i_5;
      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[7][0] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = 0; i_5 < this.field371; i_5++) {
            if (i_5 >= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[7][1] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = this.field371 - 1; i_4 >= 0; --i_4) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 >= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[7][2] = bytes_2;
      bytes_2 = new byte[this.field371 * this.field371];
      i_6 = 0;

      for (i_4 = 0; i_4 < this.field371; i_4++) {
         for (i_5 = this.field371 - 1; i_5 >= 0; --i_5) {
            if (i_5 >= i_4 - this.field371 / 2) {
               bytes_2[i_6] = -1;
            }

            ++i_6;
         }
      }

      this.field368[7][3] = bytes_2;
   }

   void method793(int i_1) {
      if (this.field368 == null) {
         this.field368 = new byte[8][4][];
         this.method772(1025982121);
         this.method773(-259199833);
         this.method774(-1200992206);
         this.method791(-482599020);
         this.method776(1069813935);
         this.method777(2128445044);
         this.method778(-1547237879);
         this.method779(-299127223);
      }
   }

   void method780(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, byte b_9) {
      if (i_7 != 0 && this.field371 != 0 && this.field368 != null) {
         i_8 = this.method804(i_8, i_7, 1815256421);
         i_7 = this.method770(i_7, (byte) 77);
         class331.method5994(i_1, i_2, i_5, i_6, i_3, i_4, this.field368[i_7 - 1][i_8], this.field371, true);
      }
   }

}
