import java.util.Comparator;

public class class76 implements Comparator {

   static class329 field643;
   boolean field639;

   int method1353(class16 class16_1, class16 class16_2, byte b_3) {
      if (class16_2.field68 == class16_1.field68) {
         return 0;
      } else {
         if (this.field639) {
            if (client.field645 == class16_1.field68) {
               return -1;
            }

            if (class16_2.field68 == client.field645) {
               return 1;
            }
         }

         return class16_1.field68 < class16_2.field68 ? -1 : 1;
      }
   }

   public int compare(Object object_1, Object object_2) {
      return this.method1353((class16) object_1, (class16) object_2, (byte) -7);
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   static final void method1359(String string_0, int i_1, int i_2) {
      class196 class196_3 = class68.method1249(class192.field2292, client.field694.field1328, (byte) 1);
      class196_3.field2360.method5482(class132.method2906(string_0, (byte) -6) + 1, (byte) -74);
      class196_3.field2360.method5492(string_0, -1748540705);
      class196_3.field2360.method5529(i_1, -1644143192);
      client.field694.method2295(class196_3, -183156542);
   }

}
