public final class class270 {

   class184 field3565 = new class184();

   public class270() {
      this.field3565.field2129 = this.field3565;
      this.field3565.field2128 = this.field3565;
   }

   public void method4793(class184 class184_1) {
      if (class184_1.field2128 != null) {
         class184_1.method3622();
      }

      class184_1.field2128 = this.field3565;
      class184_1.field2129 = this.field3565.field2129;
      class184_1.field2128.field2129 = class184_1;
      class184_1.field2129.field2128 = class184_1;
   }

   public void method4792(class184 class184_1) {
      if (class184_1.field2128 != null) {
         class184_1.method3622();
      }

      class184_1.field2128 = this.field3565.field2128;
      class184_1.field2129 = this.field3565;
      class184_1.field2128.field2129 = class184_1;
      class184_1.field2129.field2128 = class184_1;
   }

   public class184 method4795() {
      class184 class184_1 = this.field3565.field2129;
      return class184_1 == this.field3565 ? null : class184_1;
   }

}
