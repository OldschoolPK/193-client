public class class255 extends class184 {

   public static class244 field3287;
   static class154 field3288 = new class154(64);
   int field3289 = 0;
   public int field3292;
   public int field3291;
   public int field3293;
   public int field3290;

   void method4417(int i_1, int i_2) {
      double d_3 = (double)(i_1 >> 16 & 0xff) / 256.0D;
      double d_5 = (double)(i_1 >> 8 & 0xff) / 256.0D;
      double d_7 = (double)(i_1 & 0xff) / 256.0D;
      double d_9 = d_3;
      if (d_5 < d_3) {
         d_9 = d_5;
      }

      if (d_7 < d_9) {
         d_9 = d_7;
      }

      double d_11 = d_3;
      if (d_5 > d_3) {
         d_11 = d_5;
      }

      if (d_7 > d_11) {
         d_11 = d_7;
      }

      double d_13 = 0.0D;
      double d_15 = 0.0D;
      double d_17 = (d_9 + d_11) / 2.0D;
      if (d_9 != d_11) {
         if (d_17 < 0.5D) {
            d_15 = (d_11 - d_9) / (d_9 + d_11);
         }

         if (d_17 >= 0.5D) {
            d_15 = (d_11 - d_9) / (2.0D - d_11 - d_9);
         }

         if (d_3 == d_11) {
            d_13 = (d_5 - d_7) / (d_11 - d_9);
         } else if (d_11 == d_5) {
            d_13 = 2.0D + (d_7 - d_3) / (d_11 - d_9);
         } else if (d_7 == d_11) {
            d_13 = 4.0D + (d_3 - d_5) / (d_11 - d_9);
         }
      }

      d_13 /= 6.0D;
      this.field3292 = (int)(d_15 * 256.0D);
      this.field3291 = (int)(256.0D * d_17);
      if (this.field3292 < 0) {
         this.field3292 = 0;
      } else if (this.field3292 > 255) {
         this.field3292 = 255;
      }

      if (this.field3291 < 0) {
         this.field3291 = 0;
      } else if (this.field3291 > 255) {
         this.field3291 = 255;
      }

      if (d_17 > 0.5D) {
         this.field3293 = (int)(d_15 * (1.0D - d_17) * 512.0D);
      } else {
         this.field3293 = (int)(d_17 * d_15 * 512.0D);
      }

      if (this.field3293 < 1) {
         this.field3293 = 1;
      }

      this.field3290 = (int)((double)this.field3293 * d_13);
   }

   void method4416(class310 class310_1, int i_2, int i_3, byte b_4) {
      if (i_2 == 1) {
         this.field3289 = class310_1.method5506((short) 239);
      }

   }

   void method4414(int i_1) {
      this.method4417(this.field3289, 1248052268);
   }

   void method4415(class310 class310_1, int i_2, int i_3) {
      while (true) {
         int i_4 = class310_1.method5661((byte) 32);
         if (i_4 == 0) {
            return;
         }

         this.method4416(class310_1, i_4, i_2, (byte) 5);
      }
   }

   static int method4427(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      int i_4;
      if (i_0 == 5504) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         int i_5 = class85.field1095[class253.field3267 + 1];
         if (!client.field890) {
            client.field777 = i_4;
            client.field830 = i_5;
         }

         return 1;
      } else if (i_0 == 5505) {
         class85.field1095[++class253.field3267 - 1] = client.field777;
         return 1;
      } else if (i_0 == 5506) {
         class85.field1095[++class253.field3267 - 1] = client.field830;
         return 1;
      } else if (i_0 == 5530) {
         i_4 = class85.field1095[--class253.field3267];
         if (i_4 < 0) {
            i_4 = 0;
         }

         client.field867 = i_4;
         return 1;
      } else if (i_0 == 5531) {
         class85.field1095[++class253.field3267 - 1] = client.field867;
         return 1;
      } else {
         return 2;
      }
   }

}
