public class class245 implements Runnable {

   static int[] field3181;
   static class272 field3177 = new class272();
   static class272 field3179 = new class272();
   public static int field3178 = 0;
   public static Object field3180 = new Object();

   public void run() {
      try {
         while (true) {
            class272 class272_2 = field3177;
            class242 class242_1;
            synchronized(field3177) {
               class242_1 = (class242) field3177.method4879();
            }

            Object object_14;
            if (class242_1 != null) {
               if (class242_1.field3149 == 0) {
                  class242_1.field3146.method5860((int)class242_1.field2137, class242_1.field3145, class242_1.field3145.length, -315140896);
                  class272_2 = field3177;
                  synchronized(field3177) {
                     class242_1.method3628();
                  }
               } else if (class242_1.field3149 == 1) {
                  class242_1.field3145 = class242_1.field3146.method5864((int)class242_1.field2137, 361670705);
                  class272_2 = field3177;
                  synchronized(field3177) {
                     field3179.method4888(class242_1);
                  }
               }

               object_14 = field3180;
               synchronized(field3180) {
                  if (field3178 <= 1) {
                     field3178 = 0;
                     field3180.notifyAll();
                     return;
                  }

                  field3178 = 600;
               }
            } else {
               class236.method4132(100L);
               object_14 = field3180;
               synchronized(field3180) {
                  if (field3178 <= 1) {
                     field3178 = 0;
                     field3180.notifyAll();
                     return;
                  }

                  --field3178;
               }
            }
         }
      } catch (Exception exception_13) {
         class223.method4011((String) null, exception_13, 144469815);
      }
   }

   public static double[] method4261(double d_0, double d_2, int i_4, int i_5) {
      int i_6 = i_4 * 2 + 1;
      double[] doubles_7 = new double[i_6];
      int i_8 = -i_4;

      for (int i_9 = 0; i_8 <= i_4; i_9++) {
         double d_12 = class83.method1944(((double)i_8 - d_0) / d_2) / d_2;
         doubles_7[i_9] = d_12;
         ++i_8;
      }

      return doubles_7;
   }

}
