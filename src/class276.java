import java.util.Iterator;

public class class276 implements Iterable {

   public class184 field3579 = new class184();
   class184 field3578;

   public class276() {
      this.field3579.field2129 = this.field3579;
      this.field3579.field2128 = this.field3579;
   }

   class184 method4947(class184 class184_1) {
      class184 class184_2;
      if (class184_1 == null) {
         class184_2 = this.field3579.field2129;
      } else {
         class184_2 = class184_1;
      }

      if (class184_2 == this.field3579) {
         this.field3578 = null;
         return null;
      } else {
         this.field3578 = class184_2.field2129;
         return class184_2;
      }
   }

   public class184 method4945() {
      return this.method4947((class184) null);
   }

   public void method4968() {
      while (this.field3579.field2129 != this.field3579) {
         this.field3579.field2129.method3622();
      }

   }

   public class184 method4944() {
      class184 class184_1 = this.field3579.field2129;
      if (class184_1 == this.field3579) {
         return null;
      } else {
         class184_1.method3622();
         return class184_1;
      }
   }

   public void method4963(class184 class184_1) {
      if (class184_1.field2128 != null) {
         class184_1.method3622();
      }

      class184_1.field2128 = this.field3579.field2128;
      class184_1.field2129 = this.field3579;
      class184_1.field2128.field2129 = class184_1;
      class184_1.field2129.field2128 = class184_1;
   }

   public class184 method4969() {
      class184 class184_1 = this.field3578;
      if (class184_1 == this.field3579) {
         this.field3578 = null;
         return null;
      } else {
         this.field3578 = class184_1.field2129;
         return class184_1;
      }
   }

   public Iterator iterator() {
      return new class274(this);
   }

   public static void method4943(class184 class184_0, class184 class184_1) {
      if (class184_0.field2128 != null) {
         class184_0.method3622();
      }

      class184_0.field2128 = class184_1;
      class184_0.field2129 = class184_1.field2129;
      class184_0.field2128.field2129 = class184_0;
      class184_0.field2129.field2128 = class184_0;
   }

}
