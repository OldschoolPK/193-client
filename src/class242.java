public class class242 extends class189 {

   static int field3148;
   int field3149;
   class318 field3146;
   byte[] field3145;
   class246 field3147;

   static class147 method4154(int i_0, byte b_1) {
      class147 class147_2 = (class147) class269.field3549.method3376((long)i_0);
      if (class147_2 != null) {
         return class147_2;
      } else {
         class147_2 = class160.method3442(class269.field3546, class269.field3547, i_0, false, 1436051671);
         if (class147_2 != null) {
            class269.field3549.method3374(class147_2, (long)i_0);
         }

         return class147_2;
      }
   }

   public static boolean method4155(CharSequence charsequence_0, int i_1) {
      boolean bool_3 = false;
      boolean bool_4 = false;
      int i_5 = 0;
      int i_6 = charsequence_0.length();
      int i_7 = 0;

      boolean bool_2;
      while (true) {
         if (i_7 >= i_6) {
            bool_2 = bool_4;
            break;
         }

         label84: {
            char var_8 = charsequence_0.charAt(i_7);
            if (i_7 == 0) {
               if (var_8 == 45) {
                  bool_3 = true;
                  break label84;
               }

               if (var_8 == 43) {
                  break label84;
               }
            }

            int i_10;
            if (var_8 >= 48 && var_8 <= 57) {
               i_10 = var_8 - 48;
            } else if (var_8 >= 65 && var_8 <= 90) {
               i_10 = var_8 - 55;
            } else {
               if (var_8 < 97 || var_8 > 122) {
                  bool_2 = false;
                  break;
               }

               i_10 = var_8 - 87;
            }

            if (i_10 >= 10) {
               bool_2 = false;
               break;
            }

            if (bool_3) {
               i_10 = -i_10;
            }

            int i_9 = i_10 + i_5 * 10;
            if (i_5 != i_9 / 10) {
               bool_2 = false;
               break;
            }

            i_5 = i_9;
            bool_4 = true;
         }

         ++i_7;
      }

      return bool_2;
   }

}
