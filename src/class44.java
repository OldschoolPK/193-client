public class class44 {

   public static short[][] field331;
   public static final class44 field328 = new class44("details");
   public static final class44 field326 = new class44("compositemap");
   public static final class44 field327 = new class44("compositetexture");
   static final class44 field329 = new class44("area");
   public static final class44 field325 = new class44("labels");
   public final String field330;

   class44(String string_1) {
      this.field330 = string_1;
   }

   static void method663(int i_0, int i_1) {
      if (i_0 != client.field653) {
         if (client.field653 == 0) {
            class27.field147.method1050(-1779228195);
         }

         if (i_0 == 20 || i_0 == 40 || i_0 == 45) {
            client.field648 = 0;
            client.field892 = 0;
            client.field681 = 0;
            client.field698.method5000(i_0, (byte) 11);
            if (i_0 != 20) {
               class34.method380(false, 381809212);
            }
         }

         if (i_0 != 20 && i_0 != 40 && class70.field562 != null) {
            class70.field562.vmethod5831((byte) 88);
            class70.field562 = null;
         }

         if (client.field653 == 25) {
            client.field704 = 0;
            client.field700 = 0;
            client.field701 = 1;
            client.field702 = 0;
            client.field703 = 1;
         }

         if (i_0 != 5 && i_0 != 10) {
            if (i_0 == 20) {
               class64.method1208(class92.field1167, class100.field1291, true, client.field653 == 11 ? 4 : 0, 1669838016);
            } else if (i_0 == 11) {
               class64.method1208(class92.field1167, class100.field1291, false, 4, -282067517);
            } else {
               class89.method2094(-1990775275);
            }
         } else {
            class64.method1208(class92.field1167, class100.field1291, true, 0, 1438840019);
         }

         client.field653 = i_0;
      }
   }

   static class246 method661(int i_0, boolean bool_1, boolean bool_2, boolean bool_3, byte b_4) {
      class318 class318_5 = null;
      if (class176.field2066 != null) {
         class318_5 = new class318(i_0, class176.field2066, class223.field2556[i_0], 1000000);
      }

      return new class246(class318_5, class7.field30, i_0, bool_1, bool_2, bool_3);
   }

   static final void method662(int i_0, int i_1) {
      i_0 = Math.min(Math.max(i_0, 0), 127);
      class282.field3617.field1059 = i_0;
      class18.method187(2123398845);
   }

   static void method664(byte b_0) {
      if (client.field723 == 1) {
         client.field649 = true;
      }

   }

}
