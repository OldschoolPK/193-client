import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class class15 {

   public static byte[][] field62;
   public static Comparator field57 = new class21();
   public static Comparator field58;
   public static Comparator field56;
   public static Comparator field59;
   public final List field60;

   static {
      new class14();
      field58 = new class19();
      field56 = new class20();
      field59 = new class17();
   }

   public class15(class310 class310_1, boolean bool_2) {
      int i_3 = class310_1.method5729(322693105);
      boolean bool_4 = class310_1.method5661((byte) -92) == 1;
      byte b_5;
      if (bool_4) {
         b_5 = 1;
      } else {
         b_5 = 0;
      }

      int i_6 = class310_1.method5729(1360599865);
      this.field60 = new ArrayList(i_6);

      for (int i_7 = 0; i_7 < i_6; i_7++) {
         this.field60.add(new class16(class310_1, b_5, i_3));
      }

   }

   public void method140(Comparator comparator_1, boolean bool_2, int i_3) {
      if (bool_2) {
         Collections.sort(this.field60, comparator_1);
      } else {
         Collections.sort(this.field60, Collections.reverseOrder(comparator_1));
      }

   }

   static int method146(int i_0, class101 class101_1, boolean bool_2, byte b_3) {
      int i_4;
      if (i_0 == 3903) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = client.field913[i_4].method170(1433662389);
         return 1;
      } else if (i_0 == 3904) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = client.field913[i_4].field77;
         return 1;
      } else if (i_0 == 3905) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = client.field913[i_4].field79;
         return 1;
      } else if (i_0 == 3906) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = client.field913[i_4].field78;
         return 1;
      } else if (i_0 == 3907) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = client.field913[i_4].field81;
         return 1;
      } else if (i_0 == 3908) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = client.field913[i_4].field82;
         return 1;
      } else {
         int i_13;
         if (i_0 == 3910) {
            i_4 = class85.field1095[--class253.field3267];
            i_13 = client.field913[i_4].method169(-9146614);
            class85.field1095[++class253.field3267 - 1] = i_13 == 0 ? 1 : 0;
            return 1;
         } else if (i_0 == 3911) {
            i_4 = class85.field1095[--class253.field3267];
            i_13 = client.field913[i_4].method169(-795605732);
            class85.field1095[++class253.field3267 - 1] = i_13 == 2 ? 1 : 0;
            return 1;
         } else if (i_0 == 3912) {
            i_4 = class85.field1095[--class253.field3267];
            i_13 = client.field913[i_4].method169(1240913708);
            class85.field1095[++class253.field3267 - 1] = i_13 == 5 ? 1 : 0;
            return 1;
         } else if (i_0 == 3913) {
            i_4 = class85.field1095[--class253.field3267];
            i_13 = client.field913[i_4].method169(-1584268632);
            class85.field1095[++class253.field3267 - 1] = i_13 == 1 ? 1 : 0;
            return 1;
         } else {
            boolean bool_14;
            if (i_0 == 3914) {
               bool_14 = class85.field1095[--class253.field3267] == 1;
               if (class285.field3634 != null) {
                  class285.field3634.method140(field56, bool_14, 1883251258);
               }

               return 1;
            } else if (i_0 == 3915) {
               bool_14 = class85.field1095[--class253.field3267] == 1;
               if (class285.field3634 != null) {
                  class285.field3634.method140(field58, bool_14, 641311806);
               }

               return 1;
            } else if (i_0 == 3916) {
               class253.field3267 -= 2;
               bool_14 = class85.field1095[class253.field3267] == 1;
               boolean bool_5 = class85.field1095[class253.field3267 + 1] == 1;
               if (class285.field3634 != null) {
                  client.field802.field639 = bool_5;
                  class285.field3634.method140(client.field802, bool_14, 1445292612);
               }

               return 1;
            } else if (i_0 == 3917) {
               bool_14 = class85.field1095[--class253.field3267] == 1;
               if (class285.field3634 != null) {
                  class285.field3634.method140(field57, bool_14, 681966863);
               }

               return 1;
            } else if (i_0 == 3918) {
               bool_14 = class85.field1095[--class253.field3267] == 1;
               if (class285.field3634 != null) {
                  class285.field3634.method140(field59, bool_14, 1261394651);
               }

               return 1;
            } else if (i_0 == 3919) {
               class85.field1095[++class253.field3267 - 1] = class285.field3634 == null ? 0 : class285.field3634.field60.size();
               return 1;
            } else {
               class16 class16_12;
               if (i_0 == 3920) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  class85.field1095[++class253.field3267 - 1] = class16_12.field68;
                  return 1;
               } else if (i_0 == 3921) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  class85.field1096[++class85.field1105 - 1] = class16_12.method156(-819048279);
                  return 1;
               } else if (i_0 == 3922) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  class85.field1096[++class85.field1105 - 1] = class16_12.method151((byte) 5);
                  return 1;
               } else if (i_0 == 3923) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  long long_6 = class298.method5270(255749540) - class4.field17 - class16_12.field69;
                  int i_8 = (int)(long_6 / 3600000L);
                  int i_9 = (int)((long_6 - (long)(i_8 * 3600000)) / 60000L);
                  int i_10 = (int)((long_6 - (long)(i_8 * 3600000) - (long)(i_9 * 60000)) / 1000L);
                  String string_11 = i_8 + ":" + i_9 / 10 + i_9 % 10 + ":" + i_10 / 10 + i_10 % 10;
                  class85.field1096[++class85.field1105 - 1] = string_11;
                  return 1;
               } else if (i_0 == 3924) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  class85.field1095[++class253.field3267 - 1] = class16_12.field72.field78;
                  return 1;
               } else if (i_0 == 3925) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  class85.field1095[++class253.field3267 - 1] = class16_12.field72.field79;
                  return 1;
               } else if (i_0 == 3926) {
                  i_4 = class85.field1095[--class253.field3267];
                  class16_12 = (class16) class285.field3634.field60.get(i_4);
                  class85.field1095[++class253.field3267 - 1] = class16_12.field72.field77;
                  return 1;
               } else {
                  return 2;
               }
            }
         }
      }
   }

   static class80 method145(int i_0) {
      return class80.field1014 < class80.field1017 ? class80.field1012[++class80.field1014 - 1] : null;
   }

   static final void method144(class78 class78_0, int i_1, int i_2) {
      class18.method175(class78_0.field990, class78_0.field938, i_1, -18578608);
   }

}
