public class class73 extends class184 {

   protected static String field603;
   static class335[] field604;
   class291 field599;
   class291 field600;
   int field598;
   int field595;
   int field596;
   String field597;
   String field594;
   String field602;
   class293 field601;

   class73(int i_1, String string_2, String string_3, String string_4) {
      this.field599 = class291.field3653;
      this.field600 = class291.field3653;
      this.method1268(i_1, string_2, string_3, string_4, -290000272);
   }

   void method1271(byte b_1) {
      this.field599 = class173.field2036.field1067.method5073(this.field601, (byte) 1) ? class291.field3652 : class291.field3654;
   }

   void method1274(byte b_1) {
      this.field600 = class173.field2036.field1068.method5073(this.field601, (byte) 1) ? class291.field3652 : class291.field3654;
   }

   void method1268(int i_1, String string_2, String string_3, String string_4, int i_5) {
      this.field598 = class94.method2124(-1905829152);
      this.field595 = client.field655;
      this.field596 = i_1;
      this.field597 = string_2;
      this.method1278(1096024764);
      this.field594 = string_3;
      this.field602 = string_4;
      this.method1269((byte) 4);
      this.method1272(-343882124);
   }

   final void method1278(int i_1) {
      if (this.field597 != null) {
         this.field601 = new class293(class22.method220(this.field597, -859465927), class19.field92);
      } else {
         this.field601 = null;
      }

   }

   void method1269(byte b_1) {
      this.field599 = class291.field3653;
   }

   void method1272(int i_1) {
      this.field600 = class291.field3653;
   }

   final boolean method1270(int i_1) {
      if (this.field599 == class291.field3653) {
         this.method1271((byte) -50);
      }

      return this.field599 == class291.field3652;
   }

   final boolean method1273(int i_1) {
      if (this.field600 == class291.field3653) {
         this.method1274((byte) -53);
      }

      return this.field600 == class291.field3652;
   }

}
