public abstract class class78 extends class144 {

   static class335[] field998;
   int field990;
   int field938;
   int field979;
   int field989;
   int field978;
   int field939;
   int field981;
   int field983;
   int field982;
   int field958;
   int field956;
   int field984;
   int field973;
   int field986;
   boolean field952;
   boolean field950 = false;
   int field941 = 1;
   int field943 = -1;
   int field944 = -1;
   int field945 = -1;
   int field946 = -1;
   int field947 = -1;
   int field948 = -1;
   int field949 = -1;
   int field942 = -1;
   String field951 = null;
   boolean field953 = false;
   int field991 = 100;
   int field955 = 0;
   int field994 = 0;
   byte field997 = 0;
   int[] field966 = new int[4];
   int[] field992 = new int[4];
   int[] field960 = new int[4];
   int[] field961 = new int[4];
   int[] field962 = new int[4];
   class271 field957 = new class271();
   int field980 = -1;
   boolean field965 = false;
   int field959 = -1;
   int field967 = -1;
   int field964 = 0;
   int field969 = 0;
   int field970 = -1;
   int field971 = 0;
   int field972 = 0;
   int field937 = 0;
   int field974 = 0;
   int field975 = -1;
   int field976 = 0;
   int field977 = 0;
   int field987 = 0;
   int field988 = 200;
   int field954 = 0;
   int field940 = 32;
   int field985 = 0;
   int[] field993 = new int[10];
   int[] field968 = new int[10];
   byte[] field995 = new byte[10];
   int field963 = 0;
   int field996 = 0;

   boolean vmethod2080(int i_1) {
      return false;
   }

   final void method1778(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      boolean bool_8 = true;
      boolean bool_9 = true;

      int i_10;
      for (i_10 = 0; i_10 < 4; i_10++) {
         if (this.field960[i_10] > i_5) {
            bool_8 = false;
         } else {
            bool_9 = false;
         }
      }

      i_10 = -1;
      int i_11 = -1;
      int i_12 = 0;
      if (i_1 >= 0) {
         class263 class263_13 = class198.method3668(i_1, 1390109728);
         i_11 = class263_13.field3373;
         i_12 = class263_13.field3364;
      }

      int i_15;
      if (bool_9) {
         if (i_11 == -1) {
            return;
         }

         i_10 = 0;
         i_15 = 0;
         if (i_11 == 0) {
            i_15 = this.field960[0];
         } else if (i_11 == 1) {
            i_15 = this.field992[0];
         }

         for (int i_14 = 1; i_14 < 4; i_14++) {
            if (i_11 == 0) {
               if (this.field960[i_14] < i_15) {
                  i_10 = i_14;
                  i_15 = this.field960[i_14];
               }
            } else if (i_11 == 1 && this.field992[i_14] < i_15) {
               i_10 = i_14;
               i_15 = this.field992[i_14];
            }
         }

         if (i_11 == 1 && i_15 >= i_2) {
            return;
         }
      } else {
         if (bool_8) {
            this.field997 = 0;
         }

         for (i_15 = 0; i_15 < 4; i_15++) {
            byte b_16 = this.field997;
            this.field997 = (byte)((this.field997 + 1) % 4);
            if (this.field960[b_16] <= i_5) {
               i_10 = b_16;
               break;
            }
         }
      }

      if (i_10 >= 0) {
         this.field966[i_10] = i_1;
         this.field992[i_10] = i_2;
         this.field961[i_10] = i_3;
         this.field962[i_10] = i_4;
         this.field960[i_10] = i_5 + i_12 + i_6;
      }
   }

   final void method1779(int i_1, int i_2) {
      class257 class257_3 = class79.method1798(i_1, (byte) -68);

      for (class87 class87_4 = (class87) this.field957.method4806(); class87_4 != null; class87_4 = (class87) this.field957.method4805()) {
         if (class257_3 == class87_4.field1126) {
            class87_4.method3628();
            return;
         }
      }

   }

   final void method1787(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      class257 class257_8 = class79.method1798(i_1, (byte) -44);
      class87 class87_9 = null;
      class87 class87_10 = null;
      int i_11 = class257_8.field3320;
      int i_12 = 0;

      class87 class87_13;
      for (class87_13 = (class87) this.field957.method4806(); class87_13 != null; class87_13 = (class87) this.field957.method4805()) {
         ++i_12;
         if (class87_13.field1126.field3313 == class257_8.field3313) {
            class87_13.method2071(i_2 + i_4, i_5, i_6, i_3, -1617546911);
            return;
         }

         if (class87_13.field1126.field3312 <= class257_8.field3312) {
            class87_9 = class87_13;
         }

         if (class87_13.field1126.field3320 > i_11) {
            class87_10 = class87_13;
            i_11 = class87_13.field1126.field3320;
         }
      }

      if (class87_10 != null || i_12 < 4) {
         class87_13 = new class87(class257_8);
         if (class87_9 == null) {
            this.field957.method4801(class87_13);
         } else {
            class271.method4802(class87_13, class87_9);
         }

         class87_13.method2071(i_2 + i_4, i_5, i_6, i_3, -1481890764);
         if (i_12 >= 4) {
            class87_10.method3628();
         }

      }
   }

   final void method1775(int i_1) {
      this.field985 = 0;
      this.field996 = 0;
   }

   public static void method1793(class244 class244_0, class244 class244_1, boolean bool_2, class305 class305_3, int i_4) {
      class88.field1136 = class244_0;
      class138.field1778 = class244_1;
      class265.field3466 = bool_2;
      class99.field1278 = class88.field1136.method4172(10, (byte) 99);
      class265.field3446 = class305_3;
   }

   static class101 method1788(byte[] bytes_0, byte b_1) {
      class101 class101_2 = new class101();
      class310 class310_3 = new class310(bytes_0);
      class310_3.field3751 = class310_3.field3752.length - 2;
      int i_4 = class310_3.method5729(-1299623639);
      int i_5 = class310_3.field3752.length - 2 - i_4 - 12;
      class310_3.field3751 = i_5;
      int i_6 = class310_3.method5507(1806289637);
      class101_2.field1296 = class310_3.method5729(1418133846);
      class101_2.field1297 = class310_3.method5729(-118353550);
      class101_2.field1298 = class310_3.method5729(-1667128373);
      class101_2.field1292 = class310_3.method5729(-936272567);
      int i_7 = class310_3.method5661((byte) -98);
      int i_8;
      int i_9;
      if (i_7 > 0) {
         class101_2.field1305 = class101_2.method2259(i_7, 557069999);

         for (i_8 = 0; i_8 < i_7; i_8++) {
            i_9 = class310_3.method5729(-1607356868);
            class326 class326_10 = new class326(i_9 > 0 ? class80.method1842(i_9, 1852622369) : 1);
            class101_2.field1305[i_8] = class326_10;

            while (i_9-- > 0) {
               int i_11 = class310_3.method5507(-792412089);
               int i_12 = class310_3.method5507(1759597180);
               class326_10.method5928(new class188(i_12), (long)i_11);
            }
         }
      }

      class310_3.field3751 = 0;
      class310_3.method5596(-1363626188);
      class101_2.field1300 = new int[i_6];
      class101_2.field1294 = new int[i_6];
      class101_2.field1295 = new String[i_6];

      for (i_8 = 0; class310_3.field3751 < i_5; class101_2.field1300[i_8++] = i_9) {
         i_9 = class310_3.method5729(-566755317);
         if (i_9 == 3) {
            class101_2.field1295[i_8] = class310_3.method5589(1687087733);
         } else if (i_9 < 100 && i_9 != 21 && i_9 != 38 && i_9 != 39) {
            class101_2.field1294[i_8] = class310_3.method5507(644786749);
         } else {
            class101_2.field1294[i_8] = class310_3.method5661((byte) 20);
         }
      }

      return class101_2;
   }

   static void method1791(int i_0, int i_1) {
      class68 class68_2 = (class68) class68.field546.method5968((long)i_0);
      if (class68_2 != null) {
         class68_2.method3628();
      }
   }

   static class343 method1792(int i_0) {
      return class31.field198;
   }

}
