import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;

public class class223 {

   static class75 field2562;
   public static short[] field2551;
   public static short[] field2557;
   public static class352[] field2556;
   static class226 field2561;
   static final int[] field2558 = new int[] {8, 11, 4, 6, 9, 7, 10};
   public static class154 field2559 = new class154(260);
   int[] field2560;
   int[] field2550;
   public boolean field2552;
   public int field2553;
   long field2554;
   long field2555;

   public void method4008(int[] ints_1, int[] ints_2, boolean bool_3, int i_4, int i_5) {
      if (ints_1 == null) {
         ints_1 = new int[12];

         for (int i_6 = 0; i_6 < 7; i_6++) {
            for (int i_7 = 0; i_7 < class256.field3295; i_7++) {
               class256 class256_8 = class48.method766(i_7, 1792117497);
               if (class256_8 != null && !class256_8.field3304 && class256_8.field3301 == i_6 + (bool_3 ? 7 : 0)) {
                  ints_1[field2558[i_6]] = i_7 + 256;
                  break;
               }
            }
         }
      }

      this.field2560 = ints_1;
      this.field2550 = ints_2;
      this.field2552 = bool_3;
      this.field2553 = i_4;
      this.method4013(-1623503155);
   }

   void method4013(int i_1) {
      long long_2 = this.field2554;
      int i_4 = this.field2560[5];
      int i_5 = this.field2560[9];
      this.field2560[5] = i_5;
      this.field2560[9] = i_4;
      this.field2554 = 0L;

      int i_6;
      for (i_6 = 0; i_6 < 12; i_6++) {
         this.field2554 <<= 4;
         if (this.field2560[i_6] >= 256) {
            this.field2554 += (long)(this.field2560[i_6] - 256);
         }
      }

      if (this.field2560[0] >= 256) {
         this.field2554 += (long)(this.field2560[0] - 256 >> 4);
      }

      if (this.field2560[1] >= 256) {
         this.field2554 += (long)(this.field2560[1] - 256 >> 8);
      }

      for (i_6 = 0; i_6 < 5; i_6++) {
         this.field2554 <<= 3;
         this.field2554 += (long)this.field2550[i_6];
      }

      this.field2554 <<= 1;
      this.field2554 += (long)(this.field2552 ? 1 : 0);
      this.field2560[5] = i_4;
      this.field2560[9] = i_5;
      if (long_2 != 0L && this.field2554 != long_2) {
         field2559.method3378(long_2);
      }

   }

   public class136 method4014(class269 class269_1, int i_2, class269 class269_3, int i_4, int i_5) {
      if (this.field2553 != -1) {
         return class27.method272(this.field2553, -477028527).method4704(class269_1, i_2, class269_3, i_4, (byte) 86);
      } else {
         long long_6 = this.field2554;
         int[] ints_8 = this.field2560;
         if (class269_1 != null && (class269_1.field3563 >= 0 || class269_1.field3559 >= 0)) {
            ints_8 = new int[12];

            for (int i_19 = 0; i_19 < 12; i_19++) {
               ints_8[i_19] = this.field2560[i_19];
            }

            if (class269_1.field3563 >= 0) {
               long_6 += (long)(class269_1.field3563 - this.field2560[5] << 40);
               ints_8[5] = class269_1.field3563;
            }

            if (class269_1.field3559 >= 0) {
               long_6 += (long)(class269_1.field3559 - this.field2560[3] << 48);
               ints_8[3] = class269_1.field3559;
            }
         }

         class136 class136_9 = (class136) field2559.method3376(long_6);
         if (class136_9 == null) {
            boolean bool_10 = false;

            int i_12;
            for (int i_11 = 0; i_11 < 12; i_11++) {
               i_12 = ints_8[i_11];
               if (i_12 >= 256 && i_12 < 512 && !class48.method766(i_12 - 256, 1792117497).method4431(-1129169926)) {
                  bool_10 = true;
               }

               if (i_12 >= 512 && !class66.method1223(i_12 - 512, -2081518521).method4635(this.field2552, (short) 30323)) {
                  bool_10 = true;
               }
            }

            if (bool_10) {
               if (this.field2555 != -1L) {
                  class136_9 = (class136) field2559.method3376(this.field2555);
               }

               if (class136_9 == null) {
                  return null;
               }
            }

            if (class136_9 == null) {
               class130[] arr_16 = new class130[12];
               i_12 = 0;

               int i_14;
               for (int i_13 = 0; i_13 < 12; i_13++) {
                  i_14 = ints_8[i_13];
                  class130 class130_15;
                  if (i_14 >= 256 && i_14 < 512) {
                     class130_15 = class48.method766(i_14 - 256, 1792117497).method4432(-1617446869);
                     if (class130_15 != null) {
                        arr_16[i_12++] = class130_15;
                     }
                  }

                  if (i_14 >= 512) {
                     class130_15 = class66.method1223(i_14 - 512, -1403183737).method4636(this.field2552, -496299107);
                     if (class130_15 != null) {
                        arr_16[i_12++] = class130_15;
                     }
                  }
               }

               class130 class130_18 = new class130(arr_16, i_12);

               for (i_14 = 0; i_14 < 5; i_14++) {
                  if (this.field2550[i_14] < class49.field366[i_14].length) {
                     class130_18.method2837(field2551[i_14], class49.field366[i_14][this.field2550[i_14]]);
                  }

                  if (this.field2550[i_14] < class44.field331[i_14].length) {
                     class130_18.method2837(field2557[i_14], class44.field331[i_14][this.field2550[i_14]]);
                  }
               }

               class136_9 = class130_18.method2876(64, 850, -30, -50, -30);
               field2559.method3374(class136_9, long_6);
               this.field2555 = long_6;
            }
         }

         if (class269_1 == null && class269_3 == null) {
            return class136_9;
         } else {
            class136 class136_17;
            if (class269_1 != null && class269_3 != null) {
               class136_17 = class269_1.method4781(class136_9, i_2, class269_3, i_4, (byte) -74);
            } else if (class269_1 != null) {
               class136_17 = class269_1.method4761(class136_9, i_2, (byte) 17);
            } else {
               class136_17 = class269_3.method4761(class136_9, i_4, (byte) -41);
            }

            return class136_17;
         }
      }
   }

   class130 method4015(int i_1) {
      if (this.field2553 != -1) {
         return class27.method272(this.field2553, -2062926306).method4703(-2109458665);
      } else {
         boolean bool_2 = false;

         int i_4;
         for (int i_3 = 0; i_3 < 12; i_3++) {
            i_4 = this.field2560[i_3];
            if (i_4 >= 256 && i_4 < 512 && !class48.method766(i_4 - 256, 1792117497).method4455(-734430766)) {
               bool_2 = true;
            }

            if (i_4 >= 512 && !class66.method1223(i_4 - 512, 590506751).method4627(this.field2552, (byte) -128)) {
               bool_2 = true;
            }
         }

         if (bool_2) {
            return null;
         } else {
            class130[] arr_8 = new class130[12];
            i_4 = 0;

            int i_6;
            for (int i_5 = 0; i_5 < 12; i_5++) {
               i_6 = this.field2560[i_5];
               class130 class130_7;
               if (i_6 >= 256 && i_6 < 512) {
                  class130_7 = class48.method766(i_6 - 256, 1792117497).method4434(148590883);
                  if (class130_7 != null) {
                     arr_8[i_4++] = class130_7;
                  }
               }

               if (i_6 >= 512) {
                  class130_7 = class66.method1223(i_6 - 512, -1294848899).method4638(this.field2552, 1268940833);
                  if (class130_7 != null) {
                     arr_8[i_4++] = class130_7;
                  }
               }
            }

            class130 class130_9 = new class130(arr_8, i_4);

            for (i_6 = 0; i_6 < 5; i_6++) {
               if (this.field2550[i_6] < class49.field366[i_6].length) {
                  class130_9.method2837(field2551[i_6], class49.field366[i_6][this.field2550[i_6]]);
               }

               if (this.field2550[i_6] < class44.field331[i_6].length) {
                  class130_9.method2837(field2557[i_6], class44.field331[i_6][this.field2550[i_6]]);
               }
            }

            return class130_9;
         }
      }
   }

   public int method4016(int i_1) {
      return this.field2553 == -1 ? (this.field2560[0] << 15) + this.field2560[1] + (this.field2560[11] << 5) + (this.field2560[8] << 10) + (this.field2550[0] << 25) + (this.field2550[4] << 20) : 305419896 + class27.method272(this.field2553, 355915340).field3531;
   }

   public void method4019(int i_1, boolean bool_2, int i_3) {
      if (i_1 != 1 || !this.field2552) {
         int i_4 = this.field2560[field2558[i_1]];
         if (i_4 != 0) {
            i_4 -= 256;

            class256 class256_5;
            do {
               if (!bool_2) {
                  --i_4;
                  if (i_4 < 0) {
                     i_4 = class256.field3295 - 1;
                  }
               } else {
                  ++i_4;
                  if (i_4 >= class256.field3295) {
                     i_4 = 0;
                  }
               }

               class256_5 = class48.method766(i_4, 1792117497);
            } while (class256_5 == null || class256_5.field3304 || class256_5.field3301 != (this.field2552 ? 7 : 0) + i_1);

            this.field2560[field2558[i_1]] = i_4 + 256;
            this.method4013(-1623503155);
         }
      }
   }

   public void method4010(int i_1, boolean bool_2, int i_3) {
      int i_4 = this.field2550[i_1];
      if (!bool_2) {
         do {
            --i_4;
            if (i_4 < 0) {
               i_4 = class49.field366[i_1].length - 1;
            }
         } while (!class171.method3507(i_1, i_4, -1627436603));
      } else {
         do {
            ++i_4;
            if (i_4 >= class49.field366[i_1].length) {
               i_4 = 0;
            }
         } while (!class171.method3507(i_1, i_4, -1627436603));
      }

      this.field2550[i_1] = i_4;
      this.method4013(-1623503155);
   }

   public void method4021(boolean bool_1, byte b_2) {
      if (this.field2552 != bool_1) {
         this.method4008((int[]) null, this.field2550, bool_1, -1, 1899379562);
      }
   }

   public void method4012(class310 class310_1, int i_2) {
      class310_1.method5482(this.field2552 ? 1 : 0, (byte) -4);

      int i_3;
      for (i_3 = 0; i_3 < 7; i_3++) {
         int i_4 = this.field2560[field2558[i_3]];
         if (i_4 == 0) {
            class310_1.method5482(-1, (byte) -38);
         } else {
            class310_1.method5482(i_4 - 256, (byte) -5);
         }
      }

      for (i_3 = 0; i_3 < 5; i_3++) {
         class310_1.method5482(this.field2550[i_3], (byte) -2);
      }

   }

   public static void method4011(String string_0, Throwable throwable_1, int i_2) {
      try {
         String str_3 = "";
         if (throwable_1 != null) {
            Throwable throwable_5 = throwable_1;
            String str_6;
            if (throwable_1 instanceof class351) {
               class351 class351_7 = (class351) throwable_1;
               str_6 = class351_7.field4081 + " | ";
               throwable_5 = class351_7.field4083;
            } else {
               str_6 = "";
            }

            StringWriter stringwriter_19 = new StringWriter();
            PrintWriter printwriter_8 = new PrintWriter(stringwriter_19);
            throwable_5.printStackTrace(printwriter_8);
            printwriter_8.close();
            String string_9 = stringwriter_19.toString();
            BufferedReader bufferedreader_10 = new BufferedReader(new StringReader(string_9));
            String string_11 = bufferedreader_10.readLine();

            while (true) {
               String string_12 = bufferedreader_10.readLine();
               if (string_12 == null) {
                  str_6 = str_6 + "| " + string_11;
                  str_3 = str_6;
                  break;
               }

               int i_13 = string_12.indexOf(40);
               int i_14 = string_12.indexOf(41, i_13 + 1);
               if (i_13 >= 0 && i_14 >= 0) {
                  String string_15 = string_12.substring(i_13 + 1, i_14);
                  int i_16 = string_15.indexOf(".java:");
                  if (i_16 >= 0) {
                     string_15 = string_15.substring(0, i_16) + string_15.substring(i_16 + 5);
                     str_6 = str_6 + string_15 + ' ';
                     continue;
                  }

                  string_12 = string_12.substring(0, i_13);
               }

               string_12 = string_12.trim();
               string_12 = string_12.substring(string_12.lastIndexOf(32) + 1);
               string_12 = string_12.substring(string_12.lastIndexOf(9) + 1);
               str_6 = str_6 + string_12 + ' ';
            }
         }

         if (string_0 != null) {
            if (throwable_1 != null) {
               str_3 = str_3 + " | ";
            }

            str_3 = str_3 + string_0;
         }

         System.out.println("Error: " + str_3);
         str_3 = str_3.replace(':', '.');
         str_3 = str_3.replace('@', '_');
         str_3 = str_3.replace('&', '_');
         str_3 = str_3.replace('#', '_');
         if (class351.field4085 == null) {
            return;
         }

         URL url_4 = new URL(class351.field4085.getCodeBase(), "clienterror.ws?c=" + class351.field4084 + "&u=" + class351.field4082 + "&v1=" + class175.field2052 + "&v2=" + class175.field2051 + "&ct=" + class39.field288 + "&e=" + str_3);
         DataInputStream datainputstream_18 = new DataInputStream(url_4.openStream());
         datainputstream_18.read();
         datainputstream_18.close();
      } catch (Exception exception_17) {
         ;
      }

   }

}
