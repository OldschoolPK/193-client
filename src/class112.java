public class class112 extends class127 {

   public int field1407;
   public byte[] field1405;
   public int field1404;
   int field1406;
   public boolean field1408;

   class112(int i_1, byte[] bytes_2, int i_3, int i_4) {
      this.field1407 = i_1;
      this.field1405 = bytes_2;
      this.field1404 = i_3;
      this.field1406 = i_4;
   }

   class112(int i_1, byte[] bytes_2, int i_3, int i_4, boolean bool_5) {
      this.field1407 = i_1;
      this.field1405 = bytes_2;
      this.field1404 = i_3;
      this.field1406 = i_4;
      this.field1408 = bool_5;
   }

   public class112 method2428(class121 class121_1) {
      this.field1405 = class121_1.method2560(this.field1405, -77952931);
      this.field1407 = class121_1.method2562(this.field1407, (byte) -4);
      if (this.field1404 == this.field1406) {
         this.field1404 = this.field1406 = class121_1.method2567(this.field1404, (byte) -38);
      } else {
         this.field1404 = class121_1.method2567(this.field1404, (byte) -109);
         this.field1406 = class121_1.method2567(this.field1406, (byte) 51);
         if (this.field1404 == this.field1406) {
            --this.field1404;
         }
      }

      return this;
   }

}
