import java.security.SecureRandom;
import java.util.concurrent.Callable;

public class class66 implements Callable {

   static String field528;

   public Object call() {
      SecureRandom securerandom_2 = new SecureRandom();
      securerandom_2.nextInt();
      return securerandom_2;
   }

   public static class265 method1223(int i_0, int i_1) {
      class265 class265_2 = (class265) class265.field3437.method3376((long)i_0);
      if (class265_2 != null) {
         return class265_2;
      } else {
         byte[] bytes_3 = class88.field1136.method4160(10, i_0, (short) -16311);
         class265_2 = new class265();
         class265_2.field3480 = i_0;
         if (bytes_3 != null) {
            class265_2.method4674(new class310(bytes_3), 2011141659);
         }

         class265_2.method4626(-617836008);
         if (class265_2.field3432 * -582464211 != -1) {
            class265_2.method4629(method1223(class265_2.field3432 * -582464211, -358655401), method1223(class265_2.field3475, -61968736), 1371211432);
         }

         if (class265_2.field3486 != -1) {
            class265_2.method4630(method1223(class265_2.field3486, -1807921353), method1223(class265_2.field3485, -1834226703), -1959177502);
         }

         if (class265_2.field3488 != -1) {
            class265_2.method4631(method1223(class265_2.field3488, -1156814703), method1223(class265_2.field3487, -1932166078), 312275833);
         }

         if (!class265.field3466 && class265_2.field3441) {
            class265_2.field3444 = "Members object";
            class265_2.field3484 = false;
            class265_2.field3458 = null;
            class265_2.field3459 = null;
            class265_2.field3447 = -1;
            class265_2.field3482 = 0;
            if (class265_2.field3483 != null) {
               boolean bool_4 = false;

               for (class189 class189_5 = class265_2.field3483.method5933(); class189_5 != null; class189_5 = class265_2.field3483.method5922()) {
                  class260 class260_6 = class103.method2288((int)class189_5.field2137, 1207819240);
                  if (class260_6.field3333) {
                     class189_5.method3628();
                  } else {
                     bool_4 = true;
                  }
               }

               if (!bool_4) {
                  class265_2.field3483 = null;
               }
            }
         }

         class265.field3437.method3374(class265_2, (long)i_0);
         return class265_2;
      }
   }

   public static String method1218(long long_0) {
      if (long_0 > 0L && long_0 < 6582952005840035281L) {
         if (long_0 % 37L == 0L) {
            return null;
         } else {
            int i_2 = 0;

            for (long long_3 = long_0; long_3 != 0L; long_3 /= 37L) {
               ++i_2;
            }

            StringBuilder stringbuilder_5 = new StringBuilder(i_2);

            while (long_0 != 0L) {
               long long_6 = long_0;
               long_0 /= 37L;
               stringbuilder_5.append(class299.field3689[(int)(long_6 - 37L * long_0)]);
            }

            return stringbuilder_5.reverse().toString();
         }
      } else {
         return null;
      }
   }

   public static void method1221(int i_0, class244 class244_1, int i_2, int i_3, int i_4, boolean bool_5, int i_6) {
      class206.field2421 = 1;
      class221.field2538 = class244_1;
      class79.field1002 = i_2;
      class206.field2425 = i_3;
      class16.field71 = i_4;
      class231.field2772 = bool_5;
      class42.field312 = i_0;
   }

   static void method1222(class80 class80_0, int i_1) {
      if (class80_0.method1800((byte) 1) != client.field659) {
         client.field659 = class80_0.method1800((byte) 1);
         class97.method2177(class80_0.method1800((byte) 1), 109574038);
      }

      if (class80_0.field1019 != client.field646) {
         class246 class246_2 = class100.field1291;
         int i_3 = class80_0.field1019;
         if ((i_3 & 0x20000000) != 0) {
            class224.field2571 = class51.method836(class246_2, "logo_deadman_mode", "", -1212098032);
         } else if ((i_3 & 0x40000000) != 0) {
            class224.field2571 = class51.method836(class246_2, "logo_seasonal_mode", "", -917544304);
         } else {
            class224.field2571 = class51.method836(class246_2, "logo", "", -562691664);
         }
      }

      class36.field252 = class80_0.field1021;
      client.field645 = class80_0.field1018;
      client.field646 = class80_0.field1019;
      class278.field3583 = client.field647 == 0 ? 43594 : class80_0.field1018 + 40000;
      class98.field1266 = client.field647 == 0 ? 443 : class80_0.field1018 + 50000;
      class242.field3148 = class278.field3583;
   }

   static final void method1217(int i_0) {
      for (int i_1 = 0; i_1 < class98.field1263; i_1++) {
         class75 class75_2 = client.field764[class98.field1268[i_1]];
         class75_2.method1309(1880119871);
      }

   }

   public static void method1220(int i_0, byte b_1) {
      class206.field2421 = 1;
      class221.field2538 = null;
      class79.field1002 = -1;
      class206.field2425 = -1;
      class16.field71 = 0;
      class231.field2772 = false;
      class42.field312 = i_0;
   }

   static final void method1219(String string_0, byte b_1) {
      if (string_0.equalsIgnoreCase("toggleroof")) {
         class282.field3617.field1052 = !class282.field3617.field1052;
         class18.method187(-1921611751);
         if (class282.field3617.field1052) {
            class234.method4129(99, "", "Roofs are now all hidden", (byte) 46);
         } else {
            class234.method4129(99, "", "Roofs will only be removed selectively", (byte) -89);
         }
      }

      if (string_0.equalsIgnoreCase("displayfps")) {
         client.field782 = !client.field782;
      }

      if (string_0.equalsIgnoreCase("renderself")) {
         client.field725 = !client.field725;
      }

      if (string_0.equalsIgnoreCase("mouseovertext")) {
         client.field796 = !client.field796;
      }

      if (client.field775 >= 2) {
         if (string_0.equalsIgnoreCase("errortest")) {
            throw new RuntimeException();
         }

         if (string_0.equalsIgnoreCase("showcoord")) {
            class31.field198.field4043 = !class31.field198.field4043;
         }

         if (string_0.equalsIgnoreCase("fpson")) {
            client.field782 = true;
         }

         if (string_0.equalsIgnoreCase("fpsoff")) {
            client.field782 = false;
         }

         if (string_0.equalsIgnoreCase("gc")) {
            System.gc();
         }

         if (string_0.equalsIgnoreCase("clientdrop")) {
            class81.method1875(777145889);
         }
      }

      class196 class196_2 = class68.method1249(class192.field2243, client.field694.field1328, (byte) 1);
      class196_2.field2360.method5482(string_0.length() + 1, (byte) -67);
      class196_2.field2360.method5492(string_0, -1748540705);
      client.field694.method2295(class196_2, -1127684037);
   }

   public static boolean method1216(int i_0, int i_1) {
      return (i_0 >> 20 & 0x1) != 0;
   }

}
