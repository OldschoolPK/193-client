public final class class140 {

   static class246 field1814;
   boolean field1808 = true;
   int field1811;
   int field1807;
   int field1806;
   int field1809;
   int field1810;
   int field1812;

   class140(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, boolean bool_7) {
      this.field1811 = i_1;
      this.field1807 = i_2;
      this.field1806 = i_3;
      this.field1809 = i_4;
      this.field1810 = i_5;
      this.field1812 = i_6;
      this.field1808 = bool_7;
   }

   static final void method3126(class78 class78_0, int i_1) {
      class78_0.field950 = false;
      class269 class269_2;
      if (class78_0.field967 != -1) {
         class269_2 = class260.method4510(class78_0.field967, -2079832421);
         if (class269_2 != null && class269_2.field3550 != null) {
            ++class78_0.field969;
            if (class78_0.field964 < class269_2.field3550.length && class78_0.field969 > class269_2.field3552[class78_0.field964]) {
               class78_0.field969 = 1;
               ++class78_0.field964;
               class225.method4042(class269_2, class78_0.field964, class78_0.field990, class78_0.field938, (byte) 26);
            }

            if (class78_0.field964 >= class269_2.field3550.length) {
               class78_0.field969 = 0;
               class78_0.field964 = 0;
               class225.method4042(class269_2, class78_0.field964, class78_0.field990, class78_0.field938, (byte) 35);
            }
         } else {
            class78_0.field967 = -1;
         }
      }

      if (class78_0.field975 != -1 && client.field655 >= class78_0.field978) {
         if (class78_0.field976 < 0) {
            class78_0.field976 = 0;
         }

         int i_4 = class96.method2160(class78_0.field975, (byte) 93).field3284;
         if (i_4 != -1) {
            class269 class269_3 = class260.method4510(i_4, -1915579695);
            if (class269_3 != null && class269_3.field3550 != null) {
               ++class78_0.field977;
               if (class78_0.field976 < class269_3.field3550.length && class78_0.field977 > class269_3.field3552[class78_0.field976]) {
                  class78_0.field977 = 1;
                  ++class78_0.field976;
                  class225.method4042(class269_3, class78_0.field976, class78_0.field990, class78_0.field938, (byte) -118);
               }

               if (class78_0.field976 >= class269_3.field3550.length && (class78_0.field976 < 0 || class78_0.field976 >= class269_3.field3550.length)) {
                  class78_0.field975 = -1;
               }
            } else {
               class78_0.field975 = -1;
            }
         } else {
            class78_0.field975 = -1;
         }
      }

      if (class78_0.field970 != -1 && class78_0.field937 <= 1) {
         class269_2 = class260.method4510(class78_0.field970, -1696878583);
         if (class269_2.field3560 == 1 && class78_0.field996 > 0 && class78_0.field984 <= client.field655 && class78_0.field973 < client.field655) {
            class78_0.field937 = 1;
            return;
         }
      }

      if (class78_0.field970 != -1 && class78_0.field937 == 0) {
         class269_2 = class260.method4510(class78_0.field970, -1970109493);
         if (class269_2 != null && class269_2.field3550 != null) {
            ++class78_0.field972;
            if (class78_0.field971 < class269_2.field3550.length && class78_0.field972 > class269_2.field3552[class78_0.field971]) {
               class78_0.field972 = 1;
               ++class78_0.field971;
               class225.method4042(class269_2, class78_0.field971, class78_0.field990, class78_0.field938, (byte) -62);
            }

            if (class78_0.field971 >= class269_2.field3550.length) {
               class78_0.field971 -= class269_2.field3558;
               ++class78_0.field974;
               if (class78_0.field974 >= class269_2.field3554) {
                  class78_0.field970 = -1;
               } else if (class78_0.field971 >= 0 && class78_0.field971 < class269_2.field3550.length) {
                  class225.method4042(class269_2, class78_0.field971, class78_0.field990, class78_0.field938, (byte) 80);
               } else {
                  class78_0.field970 = -1;
               }
            }

            class78_0.field950 = class269_2.field3556;
         } else {
            class78_0.field970 = -1;
         }
      }

      if (class78_0.field937 > 0) {
         --class78_0.field937;
      }

   }

}
