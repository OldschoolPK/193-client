import java.util.Iterator;

public class class275 implements Iterator {

   class189 field3577 = null;
   class189 field3575;
   class271 field3576;

   class275(class271 class271_1) {
      this.method4935(class271_1);
   }

   void method4930() {
      this.field3575 = this.field3576 != null ? this.field3576.field3566.field2136 : null;
      this.field3577 = null;
   }

   void method4935(class271 class271_1) {
      this.field3576 = class271_1;
      this.method4930();
   }

   public Object next() {
      class189 class189_1 = this.field3575;
      if (class189_1 == this.field3576.field3566) {
         class189_1 = null;
         this.field3575 = null;
      } else {
         this.field3575 = class189_1.field2136;
      }

      this.field3577 = class189_1;
      return class189_1;
   }

   public void remove() {
      if (this.field3577 == null) {
         throw new IllegalStateException();
      } else {
         this.field3577.method3628();
         this.field3577 = null;
      }
   }

   public boolean hasNext() {
      return this.field3576.field3566 != this.field3575;
   }

}
