import java.awt.Component;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

public final class class53 implements class177, MouseWheelListener {

   int field397 = 0;

   void method847(Component component_1, byte b_2) {
      component_1.addMouseWheelListener(this);
   }

   void method853(Component component_1, int i_2) {
      component_1.removeMouseWheelListener(this);
   }

   public synchronized int vmethod3539(byte b_1) {
      int i_2 = this.field397;
      this.field397 = 0;
      return i_2;
   }

   public synchronized void mouseWheelMoved(MouseWheelEvent mousewheelevent_1) {
      this.field397 += mousewheelevent_1.getWheelRotation();
   }

}
