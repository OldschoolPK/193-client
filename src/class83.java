public class class83 {

   static int field1065;
   static int field1071;
   int field1069 = 0;
   final class348 field1070;
   public final class292 field1067;
   public final class286 field1068;

   class83(class348 class348_1) {
      this.field1070 = class348_1;
      this.field1067 = new class292(class348_1);
      this.field1068 = new class286(class348_1);
   }

   final boolean method1905(class293 class293_1, int i_2) {
      class294 class294_3 = (class294) this.field1067.method5074(class293_1, -171795868);
      return class294_3 != null && class294_3.method5157(323903236);
   }

   final boolean method1900(int i_1) {
      return this.field1067.method5122((byte) 1) || this.field1067.method5071(1366420598) >= 200 && client.field766 != 1;
   }

   final boolean method1910(int i_1) {
      return this.field1068.method5122((byte) 1) || this.field1068.method5071(2116280939) >= 100 && client.field766 != 1;
   }

   final boolean method1897(class293 class293_1, boolean bool_2, byte b_3) {
      return class293_1 == null ? false : (class293_1.equals(class223.field2562.field619) ? true : this.field1067.method5170(class293_1, bool_2, (byte) -6));
   }

   final boolean method1936(class293 class293_1, int i_2) {
      return class293_1 == null ? false : this.field1068.method5073(class293_1, (byte) 1);
   }

   final void method1894(class310 class310_1, int i_2, int i_3) {
      this.field1067.method5171(class310_1, i_2, -1122558022);
      this.field1069 = 2;

      for (int i_4 = 0; i_4 < class98.field1263; i_4++) {
         class75 class75_5 = client.field764[class98.field1268[i_4]];
         class75_5.method1305(-730921547);
      }

      class96.method2161(-263304274);
      if (class35.field244 != null) {
         class35.field244.method5218((byte) 29);
      }

   }

   final void method1891(int i_1) {
      this.field1069 = 1;
   }

   boolean method1911(int i_1) {
      return this.field1069 == 2;
   }

   final void method1899(String string_1, int i_2) {
      if (string_1 != null) {
         class293 class293_3 = new class293(string_1, this.field1070);
         if (class293_3.method5187((byte) -33)) {
            if (this.method1900(1820755315)) {
               class216.method3954(-1496948542);
            } else if (class223.field2562.field619.equals(class293_3)) {
               class191.method3639(-1225574522);
            } else if (this.method1897(class293_3, false, (byte) 69)) {
               class219.method3972(string_1, -317727770);
            } else if (this.method1936(class293_3, 1551667153)) {
               StringBuilder stringbuilder_10000 = new StringBuilder();
               Object obj_10001 = null;
               stringbuilder_10000 = stringbuilder_10000.append("Please remove ").append(string_1);
               obj_10001 = null;
               String string_4 = stringbuilder_10000.append(" from your ignore list first").toString();
               class234.method4129(30, "", string_4, (byte) -12);
            } else {
               class132.method2904(string_1, 390219432);
            }
         }
      }
   }

   final void method1903(String string_1, int i_2) {
      if (string_1 != null) {
         class293 class293_3 = new class293(string_1, this.field1070);
         if (class293_3.method5187((byte) -123)) {
            if (this.field1067.method5077(class293_3, (byte) 0)) {
               client.field879 = client.field737;
               class196 class196_4 = class68.method1249(class192.field2290, client.field694.field1328, (byte) 1);
               class196_4.field2360.method5482(class132.method2906(string_1, (byte) -103), (byte) -124);
               class196_4.field2360.method5492(string_1, -1748540705);
               client.field694.method2295(class196_4, -496313838);
            }

            for (int i_6 = 0; i_6 < class98.field1263; i_6++) {
               class75 class75_5 = client.field764[class98.field1268[i_6]];
               class75_5.method1305(-730921547);
            }

            class96.method2161(-263304274);
            if (class35.field244 != null) {
               class35.field244.method5218((byte) 54);
            }

         }
      }
   }

   final void method1901(String string_1, int i_2) {
      if (string_1 != null) {
         class293 class293_3 = new class293(string_1, this.field1070);
         if (class293_3.method5187((byte) -40)) {
            StringBuilder stringbuilder_10000;
            String string_4;
            if (this.method1910(-1353584964)) {
               stringbuilder_10000 = null;
               string_4 = "Your ignore list is full. Max of 100 for free users, and 400 for members";
               class234.method4129(30, "", string_4, (byte) 27);
            } else if (class223.field2562.field619.equals(class293_3)) {
               stringbuilder_10000 = null;
               string_4 = "You can\'t add yourself to your own ignore list";
               class234.method4129(30, "", string_4, (byte) -40);
            } else if (this.method1936(class293_3, -1374941040)) {
               stringbuilder_10000 = (new StringBuilder()).append(string_1);
               Object obj_10001 = null;
               string_4 = stringbuilder_10000.append(" is already on your ignore list").toString();
               class234.method4129(30, "", string_4, (byte) -10);
            } else if (this.method1897(class293_3, false, (byte) 50)) {
               class17.method165(string_1, -1658797049);
            } else {
               class141.method3127(string_1, (byte) -5);
            }
         }
      }
   }

   final void method1904(String string_1, byte b_2) {
      if (string_1 != null) {
         class293 class293_3 = new class293(string_1, this.field1070);
         if (class293_3.method5187((byte) -96)) {
            if (this.field1068.method5077(class293_3, (byte) 0)) {
               client.field879 = client.field737;
               class196 class196_4 = class68.method1249(class192.field2318, client.field694.field1328, (byte) 1);
               class196_4.field2360.method5482(class132.method2906(string_1, (byte) 26), (byte) -36);
               class196_4.field2360.method5492(string_1, -1748540705);
               client.field694.method2295(class196_4, 1922867518);
            }

            class9.method107((byte) -76);
         }
      }
   }

   final void method1896(int i_1) {
      this.field1069 = 0;
      this.field1067.method5101((byte) 0);
      this.field1068.method5101((byte) -94);
   }

   final void method1895(int i_1) {
      for (class296 class296_2 = (class296) this.field1067.field3656.method4909(); class296_2 != null; class296_2 = (class296) this.field1067.field3656.method4912()) {
         if ((long)class296_2.field3680 < class298.method5270(255749540) / 1000L - 5L) {
            if (class296_2.field3679 > 0) {
               class234.method4129(5, "", class296_2.field3678 + " has logged in.", (byte) -33);
            }

            if (class296_2.field3679 == 0) {
               class234.method4129(5, "", class296_2.field3678 + " has logged out.", (byte) -56);
            }

            class296_2.method3626();
         }
      }

   }

   static double method1944(double d_0) {
      return Math.exp(d_0 * -d_0 / 2.0D) / Math.sqrt(6.283185307179586D);
   }

   public static boolean method1915(long long_0) {
      return long_0 != 0L && !class43.method657(long_0);
   }

}
