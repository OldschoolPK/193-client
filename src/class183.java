public abstract class class183 {

   static int field2124;
   public int field2125;
   public int field2123;
   public int field2122;
   public int field2127;

   public abstract boolean vmethod3613(int var1, int var2, int var3, class181 var4, byte var5);

   public static class268 method3619(int i_0, int i_1) {
      class268 class268_2 = (class268) class268.field3533.method3376((long)i_0);
      if (class268_2 != null) {
         return class268_2;
      } else {
         byte[] bytes_3 = class268.field3543.method4160(4, i_0, (short) -3925);
         class268_2 = new class268();
         if (bytes_3 != null) {
            class268_2.method4741(new class310(bytes_3), i_0, 876279839);
         }

         class268_2.method4740(-456917444);
         class268.field3533.method3374(class268_2, (long)i_0);
         return class268_2;
      }
   }

   static final void method3618(int i_0, int i_1, int i_2, int i_3, String string_4, String string_5, int i_6, int i_7, int i_8) {
      if (i_2 >= 2000) {
         i_2 -= 2000;
      }

      class196 class196_9;
      if (i_2 == 1) {
         client.field749 = i_6;
         client.field767 = i_7;
         client.field752 = 2;
         client.field750 = 0;
         client.field774 = i_0;
         client.field880 = i_1;
         class196_9 = class68.method1249(class192.field2279, client.field694.field1328, (byte) 1);
         class196_9.field2360.method5538(class50.field379, (short) -16077);
         class196_9.field2360.method5538(class41.field300 + i_1, (short) -22354);
         class196_9.field2360.method5538(class103.field1322, (short) -14585);
         class196_9.field2360.method5721(i_0 + class243.field3156, 1133202553);
         class196_9.field2360.method5510(class26.field140, (byte) -10);
         class196_9.field2360.method5486(i_3, (byte) -95);
         class196_9.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
         client.field694.method2295(class196_9, -1471274894);
      } else if (i_2 == 2) {
         client.field749 = i_6;
         client.field767 = i_7;
         client.field752 = 2;
         client.field750 = 0;
         client.field774 = i_0;
         client.field880 = i_1;
         class196_9 = class68.method1249(class192.field2237, client.field694.field1328, (byte) 1);
         class196_9.field2360.method5699(i_0 + class243.field3156, -1675364431);
         class196_9.field2360.method5486(client.field804, (byte) -117);
         class196_9.field2360.method5486(class41.field300 + i_1, (byte) -60);
         class196_9.field2360.method5510(class63.field493, (byte) -65);
         class196_9.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
         class196_9.field2360.method5486(i_3, (byte) -79);
         client.field694.method2295(class196_9, 1101252398);
      } else if (i_2 == 3) {
         client.field749 = i_6;
         client.field767 = i_7;
         client.field752 = 2;
         client.field750 = 0;
         client.field774 = i_0;
         client.field880 = i_1;
         class196_9 = class68.method1249(class192.field2293, client.field694.field1328, (byte) 1);
         class196_9.field2360.method5721(i_0 + class243.field3156, 1133202553);
         class196_9.field2360.method5482(class54.field416[82] ? 1 : 0, (byte) -58);
         class196_9.field2360.method5486(class41.field300 + i_1, (byte) -56);
         class196_9.field2360.method5721(i_3, 1133202553);
         client.field694.method2295(class196_9, -1438570563);
      } else if (i_2 == 4) {
         client.field749 = i_6;
         client.field767 = i_7;
         client.field752 = 2;
         client.field750 = 0;
         client.field774 = i_0;
         client.field880 = i_1;
         class196_9 = class68.method1249(class192.field2310, client.field694.field1328, (byte) 1);
         class196_9.field2360.method5486(i_3, (byte) -120);
         class196_9.field2360.method5699(i_0 + class243.field3156, -1454775585);
         class196_9.field2360.method5529(class54.field416[82] ? 1 : 0, 1710257653);
         class196_9.field2360.method5538(class41.field300 + i_1, (short) -31615);
         client.field694.method2295(class196_9, -5895220);
      } else if (i_2 == 5) {
         client.field749 = i_6;
         client.field767 = i_7;
         client.field752 = 2;
         client.field750 = 0;
         client.field774 = i_0;
         client.field880 = i_1;
         class196_9 = class68.method1249(class192.field2267, client.field694.field1328, (byte) 1);
         class196_9.field2360.method5721(class41.field300 + i_1, 1133202553);
         class196_9.field2360.method5529(class54.field416[82] ? 1 : 0, 375915419);
         class196_9.field2360.method5699(i_3, 359530582);
         class196_9.field2360.method5538(i_0 + class243.field3156, (short) -20490);
         client.field694.method2295(class196_9, -1186880726);
      } else if (i_2 == 6) {
         client.field749 = i_6;
         client.field767 = i_7;
         client.field752 = 2;
         client.field750 = 0;
         client.field774 = i_0;
         client.field880 = i_1;
         class196_9 = class68.method1249(class192.field2324, client.field694.field1328, (byte) 1);
         class196_9.field2360.method5538(class41.field300 + i_1, (short) -25240);
         class196_9.field2360.method5538(i_3, (short) -5928);
         class196_9.field2360.method5538(i_0 + class243.field3156, (short) -31019);
         class196_9.field2360.method5529(class54.field416[82] ? 1 : 0, 1573097231);
         client.field694.method2295(class196_9, -37282365);
      } else {
         class196 class196_10;
         class88 class88_14;
         if (i_2 == 7) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2258, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5548(class26.field140, (byte) 0);
               class196_10.field2360.method5721(i_3, 1133202553);
               class196_10.field2360.method5699(class103.field1322, -1114078509);
               class196_10.field2360.method5529(class54.field416[82] ? 1 : 0, 2052478415);
               class196_10.field2360.method5486(class50.field379, (byte) -99);
               client.field694.method2295(class196_10, 173046669);
            }
         } else if (i_2 == 8) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2274, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5699(i_3, 197666706);
               class196_10.field2360.method5721(client.field804, 1133202553);
               class196_10.field2360.method5547(class63.field493, (byte) -1);
               class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
               client.field694.method2295(class196_10, 278195686);
            }
         } else if (i_2 == 9) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2262, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5538(i_3, (short) -12140);
               class196_10.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
               client.field694.method2295(class196_10, 1781520933);
            }
         } else if (i_2 == 10) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2246, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5721(i_3, 1133202553);
               class196_10.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
               client.field694.method2295(class196_10, 1178665058);
            }
         } else if (i_2 == 11) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2236, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
               class196_10.field2360.method5486(i_3, (byte) -2);
               client.field694.method2295(class196_10, 1763344964);
            }
         } else if (i_2 == 12) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2309, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
               class196_10.field2360.method5538(i_3, (short) -28197);
               client.field694.method2295(class196_10, -1223851976);
            }
         } else if (i_2 == 13) {
            class88_14 = client.field689[i_3];
            if (class88_14 != null) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_10 = class68.method1249(class192.field2276, client.field694.field1328, (byte) 1);
               class196_10.field2360.method5529(class54.field416[82] ? 1 : 0, 1624993343);
               class196_10.field2360.method5699(i_3, -29894253);
               client.field694.method2295(class196_10, -1246242324);
            }
         } else {
            class75 class75_16;
            if (i_2 == 14) {
               class75_16 = client.field764[i_3];
               if (class75_16 != null) {
                  client.field749 = i_6;
                  client.field767 = i_7;
                  client.field752 = 2;
                  client.field750 = 0;
                  client.field774 = i_0;
                  client.field880 = i_1;
                  class196_10 = class68.method1249(class192.field2283, client.field694.field1328, (byte) 1);
                  class196_10.field2360.method5529(class54.field416[82] ? 1 : 0, -1405704466);
                  class196_10.field2360.method5548(class26.field140, (byte) 0);
                  class196_10.field2360.method5721(class103.field1322, 1133202553);
                  class196_10.field2360.method5699(i_3, -588457850);
                  class196_10.field2360.method5486(class50.field379, (byte) -44);
                  client.field694.method2295(class196_10, -1350018624);
               }
            } else if (i_2 == 15) {
               class75_16 = client.field764[i_3];
               if (class75_16 != null) {
                  client.field749 = i_6;
                  client.field767 = i_7;
                  client.field752 = 2;
                  client.field750 = 0;
                  client.field774 = i_0;
                  client.field880 = i_1;
                  class196_10 = class68.method1249(class192.field2256, client.field694.field1328, (byte) 1);
                  class196_10.field2360.method5486(i_3, (byte) -123);
                  class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
                  class196_10.field2360.method5699(client.field804, 670444296);
                  class196_10.field2360.method5510(class63.field493, (byte) -3);
                  client.field694.method2295(class196_10, 560173750);
               }
            } else if (i_2 == 16) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2263, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5547(class26.field140, (byte) -70);
               class196_9.field2360.method5721(i_0 + class243.field3156, 1133202553);
               class196_9.field2360.method5699(class50.field379, 1994125407);
               class196_9.field2360.method5699(class41.field300 + i_1, -1610778599);
               class196_9.field2360.method5538(class103.field1322, (short) -4112);
               class196_9.field2360.method5721(i_3, 1133202553);
               class196_9.field2360.method5482(class54.field416[82] ? 1 : 0, (byte) -9);
               client.field694.method2295(class196_9, 2113672762);
            } else if (i_2 == 17) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2281, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5721(class41.field300 + i_1, 1133202553);
               class196_9.field2360.method5721(i_0 + class243.field3156, 1133202553);
               class196_9.field2360.method5721(client.field804, 1133202553);
               class196_9.field2360.method5538(i_3, (short) -470);
               class196_9.field2360.method5482(class54.field416[82] ? 1 : 0, (byte) -2);
               class196_9.field2360.method5548(class63.field493, (byte) 0);
               client.field694.method2295(class196_9, -870085308);
            } else if (i_2 == 18) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2311, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5486(i_0 + class243.field3156, (byte) -55);
               class196_9.field2360.method5486(i_3, (byte) -43);
               class196_9.field2360.method5486(class41.field300 + i_1, (byte) -28);
               class196_9.field2360.method5529(class54.field416[82] ? 1 : 0, -1275628591);
               client.field694.method2295(class196_9, -2133782118);
            } else if (i_2 == 19) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2317, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5486(class41.field300 + i_1, (byte) -65);
               class196_9.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
               class196_9.field2360.method5699(i_3, 1104139165);
               class196_9.field2360.method5486(i_0 + class243.field3156, (byte) -122);
               client.field694.method2295(class196_9, -147939444);
            } else if (i_2 == 20) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2254, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5538(class41.field300 + i_1, (short) -3131);
               class196_9.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
               class196_9.field2360.method5538(i_0 + class243.field3156, (short) -29001);
               class196_9.field2360.method5699(i_3, -873588584);
               client.field694.method2295(class196_9, -1395025649);
            } else if (i_2 == 21) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2248, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5486(i_0 + class243.field3156, (byte) -5);
               class196_9.field2360.method5538(i_3, (short) -31835);
               class196_9.field2360.method5721(class41.field300 + i_1, 1133202553);
               class196_9.field2360.method5482(class54.field416[82] ? 1 : 0, (byte) -53);
               client.field694.method2295(class196_9, 1652546177);
            } else if (i_2 == 22) {
               client.field749 = i_6;
               client.field767 = i_7;
               client.field752 = 2;
               client.field750 = 0;
               client.field774 = i_0;
               client.field880 = i_1;
               class196_9 = class68.method1249(class192.field2305, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5486(i_3, (byte) -53);
               class196_9.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
               class196_9.field2360.method5486(class41.field300 + i_1, (byte) -121);
               class196_9.field2360.method5721(i_0 + class243.field3156, 1133202553);
               client.field694.method2295(class196_9, 1573156678);
            } else if (i_2 == 23) {
               if (client.field784) {
                  class67.field536.method3264();
               } else {
                  class67.field536.method3168(class151.field1949, i_0, i_1, true);
               }
            } else {
               class196 class196_13;
               class226 class226_17;
               if (i_2 == 24) {
                  class226_17 = class181.method3610(i_1, 92357938);
                  boolean bool_12 = true;
                  if (class226_17.field2695 > 0) {
                     bool_12 = class79.method1797(class226_17, -1439133446);
                  }

                  if (bool_12) {
                     class196_13 = class68.method1249(class192.field2287, client.field694.field1328, (byte) 1);
                     class196_13.field2360.method5510(i_1, (byte) -36);
                     client.field694.method2295(class196_13, 745433907);
                  }
               } else {
                  if (i_2 == 25) {
                     class226_17 = class92.method2103(i_1, i_0, -100010964);
                     if (class226_17 != null) {
                        class45.method706(409732903);
                        class194.method3648(i_1, i_0, class191.method3638(class146.method3318(class226_17, -443489325), -1847073400), class226_17.field2712, 1832096087);
                        client.field801 = 0;
                        client.field851 = class241.method4150(class226_17, (byte) -1);
                        if (client.field851 == null) {
                           client.field851 = "null";
                        }

                        if (class226_17.field2585) {
                           client.field807 = class226_17.field2665 + class23.method222(16777215, (byte) 80);
                        } else {
                           client.field807 = class23.method222(65280, (byte) 78) + class226_17.field2708 + class23.method222(16777215, (byte) 17);
                        }
                     }

                     return;
                  }

                  if (i_2 == 26) {
                     class191.method3635((byte) -123);
                  } else {
                     int i_11;
                     class226 class226_15;
                     if (i_2 == 28) {
                        class196_9 = class68.method1249(class192.field2287, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5510(i_1, (byte) -107);
                        client.field694.method2295(class196_9, -1604634720);
                        class226_15 = class181.method3610(i_1, -1449206406);
                        if (class226_15.field2599 != null && class226_15.field2599[0][0] == 5) {
                           i_11 = class226_15.field2599[0][1];
                           class221.field2541[i_11] = 1 - class221.field2541[i_11];
                           class101.method2271(i_11, -43315102);
                        }
                     } else if (i_2 == 29) {
                        class196_9 = class68.method1249(class192.field2287, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5510(i_1, (byte) -32);
                        client.field694.method2295(class196_9, -1353332616);
                        class226_15 = class181.method3610(i_1, -1634750640);
                        if (class226_15.field2599 != null && class226_15.field2599[0][0] == 5) {
                           i_11 = class226_15.field2599[0][1];
                           if (class221.field2541[i_11] != class226_15.field2706[0]) {
                              class221.field2541[i_11] = class226_15.field2706[0];
                              class101.method2271(i_11, 481151652);
                           }
                        }
                     } else if (i_2 == 30) {
                        if (client.field822 == null) {
                           class102.method2275(i_1, i_0, -1991750086);
                           client.field822 = class92.method2103(i_1, i_0, -100010964);
                           class181.method3609(client.field822, 1215355334);
                        }
                     } else if (i_2 == 31) {
                        class196_9 = class68.method1249(class192.field2295, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5486(class50.field379, (byte) -54);
                        class196_9.field2360.method5486(i_3, (byte) -58);
                        class196_9.field2360.method5486(class103.field1322, (byte) -13);
                        class196_9.field2360.method5548(class26.field140, (byte) 0);
                        class196_9.field2360.method5486(i_0, (byte) -68);
                        class196_9.field2360.method5510(i_1, (byte) -46);
                        client.field694.method2295(class196_9, -15242434);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -320853129);
                        client.field881 = i_0;
                     } else if (i_2 == 32) {
                        class196_9 = class68.method1249(class192.field2327, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5486(i_3, (byte) -4);
                        class196_9.field2360.method5486(i_0, (byte) -84);
                        class196_9.field2360.method5538(client.field804, (short) -5946);
                        class196_9.field2360.method5548(class63.field493, (byte) 0);
                        class196_9.field2360.method5547(i_1, (byte) -24);
                        client.field694.method2295(class196_9, 1238429165);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -156740754);
                        client.field881 = i_0;
                     } else if (i_2 == 33) {
                        class196_9 = class68.method1249(class192.field2284, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5486(i_0, (byte) -92);
                        class196_9.field2360.method5547(i_1, (byte) -10);
                        class196_9.field2360.method5538(i_3, (short) -29337);
                        client.field694.method2295(class196_9, -1562140656);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -1147486231);
                        client.field881 = i_0;
                     } else if (i_2 == 34) {
                        class196_9 = class68.method1249(class192.field2325, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5486(i_0, (byte) -17);
                        class196_9.field2360.method5699(i_3, -1812522172);
                        class196_9.field2360.method5547(i_1, (byte) -124);
                        client.field694.method2295(class196_9, -1652904134);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -1729600139);
                        client.field881 = i_0;
                     } else if (i_2 == 35) {
                        class196_9 = class68.method1249(class192.field2259, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5699(i_0, -558558923);
                        class196_9.field2360.method5538(i_3, (short) -26849);
                        class196_9.field2360.method5547(i_1, (byte) -46);
                        client.field694.method2295(class196_9, 358109068);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -287627677);
                        client.field881 = i_0;
                     } else if (i_2 == 36) {
                        class196_9 = class68.method1249(class192.field2265, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5721(i_0, 1133202553);
                        class196_9.field2360.method5548(i_1, (byte) 0);
                        class196_9.field2360.method5486(i_3, (byte) -41);
                        client.field694.method2295(class196_9, -56665747);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -418427326);
                        client.field881 = i_0;
                     } else if (i_2 == 37) {
                        class196_9 = class68.method1249(class192.field2229, client.field694.field1328, (byte) 1);
                        class196_9.field2360.method5678(i_1, 343944905);
                        class196_9.field2360.method5699(i_0, -1866403527);
                        class196_9.field2360.method5486(i_3, (byte) -94);
                        client.field694.method2295(class196_9, -873781760);
                        client.field754 = 0;
                        class88.field1131 = class181.method3610(i_1, -2016744924);
                        client.field881 = i_0;
                     } else {
                        if (i_2 == 38) {
                           class45.method706(1575459612);
                           class226_17 = class181.method3610(i_1, -1507484938);
                           client.field801 = 1;
                           class103.field1322 = i_0;
                           class26.field140 = i_1;
                           class50.field379 = i_3;
                           class181.method3609(class226_17, 1370682300);
                           client.field744 = class23.method222(16748608, (byte) 7) + class66.method1223(i_3, -471339893).field3444 + class23.method222(16777215, (byte) 117);
                           if (client.field744 == null) {
                              client.field744 = "null";
                           }

                           return;
                        }

                        if (i_2 == 39) {
                           class196_9 = class68.method1249(class192.field2330, client.field694.field1328, (byte) 1);
                           class196_9.field2360.method5538(i_3, (short) -1976);
                           class196_9.field2360.method5678(i_1, 1918102063);
                           class196_9.field2360.method5538(i_0, (short) -10276);
                           client.field694.method2295(class196_9, 2042515779);
                           client.field754 = 0;
                           class88.field1131 = class181.method3610(i_1, -81543627);
                           client.field881 = i_0;
                        } else if (i_2 == 40) {
                           class196_9 = class68.method1249(class192.field2264, client.field694.field1328, (byte) 1);
                           class196_9.field2360.method5486(i_3, (byte) -53);
                           class196_9.field2360.method5699(i_0, 1738254460);
                           class196_9.field2360.method5510(i_1, (byte) -40);
                           client.field694.method2295(class196_9, 1602370338);
                           client.field754 = 0;
                           class88.field1131 = class181.method3610(i_1, -620793201);
                           client.field881 = i_0;
                        } else if (i_2 == 41) {
                           class196_9 = class68.method1249(class192.field2294, client.field694.field1328, (byte) 1);
                           class196_9.field2360.method5538(i_3, (short) -7868);
                           class196_9.field2360.method5510(i_1, (byte) -46);
                           class196_9.field2360.method5699(i_0, -459908386);
                           client.field694.method2295(class196_9, 1487327829);
                           client.field754 = 0;
                           class88.field1131 = class181.method3610(i_1, 803985384);
                           client.field881 = i_0;
                        } else if (i_2 == 42) {
                           class196_9 = class68.method1249(class192.field2315, client.field694.field1328, (byte) 1);
                           class196_9.field2360.method5721(i_0, 1133202553);
                           class196_9.field2360.method5721(i_3, 1133202553);
                           class196_9.field2360.method5547(i_1, (byte) -120);
                           client.field694.method2295(class196_9, -1978446973);
                           client.field754 = 0;
                           class88.field1131 = class181.method3610(i_1, -580178968);
                           client.field881 = i_0;
                        } else if (i_2 == 43) {
                           class196_9 = class68.method1249(class192.field2257, client.field694.field1328, (byte) 1);
                           class196_9.field2360.method5721(i_3, 1133202553);
                           class196_9.field2360.method5721(i_0, 1133202553);
                           class196_9.field2360.method5548(i_1, (byte) 0);
                           client.field694.method2295(class196_9, -1350423583);
                           client.field754 = 0;
                           class88.field1131 = class181.method3610(i_1, -48106974);
                           client.field881 = i_0;
                        } else if (i_2 == 44) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2260, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
                              class196_10.field2360.method5721(i_3, 1133202553);
                              client.field694.method2295(class196_10, -2044212888);
                           }
                        } else if (i_2 == 45) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2238, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5721(i_3, 1133202553);
                              class196_10.field2360.method5529(class54.field416[82] ? 1 : 0, 1737369613);
                              client.field694.method2295(class196_10, -972629042);
                           }
                        } else if (i_2 == 46) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2278, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5486(i_3, (byte) -76);
                              class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
                              client.field694.method2295(class196_10, -1547546430);
                           }
                        } else if (i_2 == 47) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2312, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5721(i_3, 1133202553);
                              class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
                              client.field694.method2295(class196_10, -202672567);
                           }
                        } else if (i_2 == 48) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2253, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5529(class54.field416[82] ? 1 : 0, 161209694);
                              class196_10.field2360.method5538(i_3, (short) -27488);
                              client.field694.method2295(class196_10, 2140705099);
                           }
                        } else if (i_2 == 49) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2242, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5528(class54.field416[82] ? 1 : 0, 1712020733);
                              class196_10.field2360.method5538(i_3, (short) -16092);
                              client.field694.method2295(class196_10, 229736991);
                           }
                        } else if (i_2 == 50) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2268, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5538(i_3, (short) -14705);
                              class196_10.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
                              client.field694.method2295(class196_10, -217380005);
                           }
                        } else if (i_2 == 51) {
                           class75_16 = client.field764[i_3];
                           if (class75_16 != null) {
                              client.field749 = i_6;
                              client.field767 = i_7;
                              client.field752 = 2;
                              client.field750 = 0;
                              client.field774 = i_0;
                              client.field880 = i_1;
                              class196_10 = class68.method1249(class192.field2282, client.field694.field1328, (byte) 1);
                              class196_10.field2360.method5530(class54.field416[82] ? 1 : 0, -1493586164);
                              class196_10.field2360.method5486(i_3, (byte) -7);
                              client.field694.method2295(class196_10, -803796631);
                           }
                        } else {
                           label976: {
                              if (i_2 != 57) {
                                 if (i_2 == 58) {
                                    class226_17 = class92.method2103(i_1, i_0, -100010964);
                                    if (class226_17 != null) {
                                       class196_10 = class68.method1249(class192.field2296, client.field694.field1328, (byte) 1);
                                       class196_10.field2360.method5510(class63.field493, (byte) -21);
                                       class196_10.field2360.method5486(i_0, (byte) -55);
                                       class196_10.field2360.method5538(client.field836, (short) -8083);
                                       class196_10.field2360.method5721(client.field804, 1133202553);
                                       class196_10.field2360.method5699(class226_17.field2712, -33012958);
                                       class196_10.field2360.method5678(i_1, -280742527);
                                       client.field694.method2295(class196_10, -1714098441);
                                    }
                                    break label976;
                                 }

                                 if (i_2 == 1001) {
                                    client.field749 = i_6;
                                    client.field767 = i_7;
                                    client.field752 = 2;
                                    client.field750 = 0;
                                    client.field774 = i_0;
                                    client.field880 = i_1;
                                    class196_9 = class68.method1249(class192.field2271, client.field694.field1328, (byte) 1);
                                    class196_9.field2360.method5529(class54.field416[82] ? 1 : 0, -1861433837);
                                    class196_9.field2360.method5699(class41.field300 + i_1, -396027908);
                                    class196_9.field2360.method5699(i_0 + class243.field3156, 896850895);
                                    class196_9.field2360.method5486(i_3, (byte) -124);
                                    client.field694.method2295(class196_9, -34791989);
                                    break label976;
                                 }

                                 if (i_2 == 1002) {
                                    client.field749 = i_6;
                                    client.field767 = i_7;
                                    client.field752 = 2;
                                    client.field750 = 0;
                                    class196_9 = class68.method1249(class192.field2321, client.field694.field1328, (byte) 1);
                                    class196_9.field2360.method5699(i_3, -336838598);
                                    client.field694.method2295(class196_9, -538753229);
                                    break label976;
                                 }

                                 if (i_2 == 1003) {
                                    client.field749 = i_6;
                                    client.field767 = i_7;
                                    client.field752 = 2;
                                    client.field750 = 0;
                                    class88_14 = client.field689[i_3];
                                    if (class88_14 != null) {
                                       class267 class267_18 = class88_14.field1135;
                                       if (class267_18.field3524 != null) {
                                          class267_18 = class267_18.method4731((byte) -51);
                                       }

                                       if (class267_18 != null) {
                                          class196_13 = class68.method1249(class192.field2288, client.field694.field1328, (byte) 1);
                                          class196_13.field2360.method5699(class267_18.field3531, -1085456126);
                                          client.field694.method2295(class196_13, -2123318570);
                                       }
                                    }
                                    break label976;
                                 }

                                 if (i_2 == 1004) {
                                    client.field749 = i_6;
                                    client.field767 = i_7;
                                    client.field752 = 2;
                                    client.field750 = 0;
                                    class196_9 = class68.method1249(class192.field2313, client.field694.field1328, (byte) 1);
                                    class196_9.field2360.method5538(i_3, (short) -31604);
                                    client.field694.method2295(class196_9, -810463665);
                                    break label976;
                                 }

                                 if (i_2 == 1005) {
                                    class226_17 = class181.method3610(i_1, -1673985891);
                                    if (class226_17 != null && class226_17.field2711[i_0] >= 100000) {
                                       class234.method4129(27, "", class226_17.field2711[i_0] + " x " + class66.method1223(i_3, -998769001).field3444, (byte) 28);
                                    } else {
                                       class196_10 = class68.method1249(class192.field2313, client.field694.field1328, (byte) 1);
                                       class196_10.field2360.method5538(i_3, (short) -7358);
                                       client.field694.method2295(class196_10, 2122540309);
                                    }

                                    client.field754 = 0;
                                    class88.field1131 = class181.method3610(i_1, -554240003);
                                    client.field881 = i_0;
                                    break label976;
                                 }

                                 if (i_2 != 1007) {
                                    if (i_2 == 1010 || i_2 == 1011 || i_2 == 1008 || i_2 == 1012 || i_2 == 1009) {
                                       class31.field198.method6370(i_2, i_3, new class222(i_0), new class222(i_1), (byte) 123);
                                    }
                                    break label976;
                                 }
                              }

                              class226_17 = class92.method2103(i_1, i_0, -100010964);
                              if (class226_17 != null) {
                                 class3.method31(i_3, i_1, i_0, class226_17.field2712, string_5, 2116806066);
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }

      if (client.field801 != 0) {
         client.field801 = 0;
         class181.method3609(class181.method3610(class26.field140, -1249866742), -976806232);
      }

      if (client.field803) {
         class45.method706(1096369235);
      }

      if (class88.field1131 != null && client.field754 == 0) {
         class181.method3609(class88.field1131, 1070335569);
      }

   }

   public static void method3614(class218 class218_0, int i_1) {
      class219.field2528 = class218_0;
   }

   static boolean method3620(byte b_0) {
      return (client.field768 & 0x2) != 0;
   }

}
