public class class128 implements class148 {

   class272 field1549 = new class272();
   int field1544 = 0;
   double field1545 = 1.0D;
   int field1543 = 128;
   class244 field1547;
   int field1542;
   class133[] field1546;

   public class128(class244 class244_1, class244 class244_2, int i_3, double d_4, int i_6) {
      this.field1547 = class244_2;
      this.field1542 = i_3;
      this.field1544 = this.field1542;
      this.field1545 = d_4;
      this.field1543 = i_6;
      int[] ints_7 = class244_1.method4171(0, -1130356360);
      int i_8 = ints_7.length;
      this.field1546 = new class133[class244_1.method4172(0, (byte) 31)];

      for (int i_9 = 0; i_9 < i_8; i_9++) {
         class310 class310_10 = new class310(class244_1.method4160(0, ints_7[i_9], (short) -12408));
         this.field1546[ints_7[i_9]] = new class133(class310_10);
      }

   }

   public void method2793(byte b_1) {
      for (int i_2 = 0; i_2 < this.field1546.length; i_2++) {
         if (this.field1546[i_2] != null) {
            this.field1546[i_2].method2909();
         }
      }

      this.field1549 = new class272();
      this.field1544 = this.field1542;
   }

   public int vmethod3330(int i_1, int i_2) {
      return this.field1546[i_1] != null ? this.field1546[i_1].field1638 : 0;
   }

   public int method2788(int i_1) {
      int i_2 = 0;
      int i_3 = 0;
      class133[] arr_4 = this.field1546;

      for (int i_5 = 0; i_5 < arr_4.length; i_5++) {
         class133 class133_6 = arr_4[i_5];
         if (class133_6 != null && class133_6.field1641 != null) {
            i_2 += class133_6.field1641.length;
            int[] ints_7 = class133_6.field1641;

            for (int i_8 = 0; i_8 < ints_7.length; i_8++) {
               int i_9 = ints_7[i_8];
               if (this.field1547.method4163(i_9, (short) 256)) {
                  ++i_3;
               }
            }
         }
      }

      if (i_2 == 0) {
         return 0;
      } else {
         return i_3 * 100 / i_2;
      }
   }

   public int[] vmethod3341(int i_1, int i_2) {
      class133 class133_3 = this.field1546[i_1];
      if (class133_3 != null) {
         if (class133_3.field1647 != null) {
            this.field1549.method4878(class133_3);
            class133_3.field1635 = true;
            return class133_3.field1647;
         }

         boolean bool_4 = class133_3.method2908(this.field1545, this.field1543, this.field1547);
         if (bool_4) {
            if (this.field1544 == 0) {
               class133 class133_5 = (class133) this.field1549.method4881();
               class133_5.method2909();
            } else {
               --this.field1544;
            }

            this.field1549.method4878(class133_3);
            class133_3.field1635 = true;
            return class133_3.field1647;
         }
      }

      return null;
   }

   public boolean vmethod3332(int i_1, int i_2) {
      return this.field1543 == 64;
   }

   public boolean vmethod3331(int i_1, int i_2) {
      return this.field1546[i_1].field1640;
   }

   public void method2789(double d_1) {
      this.field1545 = d_1;
      this.method2793((byte) 24);
   }

   public void method2794(int i_1, int i_2) {
      for (int i_3 = 0; i_3 < this.field1546.length; i_3++) {
         class133 class133_4 = this.field1546[i_3];
         if (class133_4 != null && class133_4.field1645 != 0 && class133_4.field1635) {
            class133_4.method2910(i_1);
            class133_4.field1635 = false;
         }
      }

   }

   public static class253 method2815(int i_0, int i_1) {
      class253 class253_2 = (class253) class253.field3265.method3376((long)i_0);
      if (class253_2 != null) {
         return class253_2;
      } else {
         byte[] bytes_3 = class253.field3266.method4160(19, i_0, (short) -5088);
         class253_2 = new class253();
         if (bytes_3 != null) {
            class253_2.method4383(new class310(bytes_3), (short) 12817);
         }

         class253.field3265.method3374(class253_2, (long)i_0);
         return class253_2;
      }
   }

   static void method2820(int i_0) {
      if (class27.field148 != null) {
         client.field915 = client.field655;
         class27.field148.method4312(542816734);

         for (int i_1 = 0; i_1 < client.field764.length; i_1++) {
            if (client.field764[i_1] != null) {
               class27.field148.method4311((client.field764[i_1].field990 >> 7) + class243.field3156, (client.field764[i_1].field938 >> 7) + class41.field300, 2038204538);
            }
         }
      }

   }

}
