import java.util.LinkedList;

public abstract class class30 {

   static class114 field192;
   int field180;
   int field188;
   int field189;
   int field181;
   int field187;
   int field182;
   int field183 = -1;
   int field184 = -1;
   boolean field190;
   boolean field191;
   short[][][] field185;
   short[][][] field186;
   byte[][][] field178;
   byte[][][] field177;
   class34[][][][] field179;

   class30() {
      new LinkedList();
      this.field190 = false;
      this.field191 = false;
   }

   boolean method307(int i_1) {
      return this.field190 && this.field191;
   }

   void method304(byte b_1) {
      this.field185 = null;
      this.field186 = null;
      this.field178 = null;
      this.field177 = null;
      this.field179 = null;
      this.field190 = false;
      this.field191 = false;
   }

   void method306(int i_1, int i_2, class310 class310_3, int i_4, int i_5) {
      boolean bool_6 = (i_4 & 0x2) != 0;
      if (bool_6) {
         this.field186[0][i_1][i_2] = (short)class310_3.method5661((byte) -12);
      }

      this.field185[0][i_1][i_2] = (short)class310_3.method5661((byte) 43);
   }

   void method314(int i_1, int i_2, class310 class310_3, int i_4, int i_5) {
      int i_6 = ((i_4 & 0x18) >> 3) + 1;
      boolean bool_7 = (i_4 & 0x2) != 0;
      boolean bool_8 = (i_4 & 0x4) != 0;
      this.field185[0][i_1][i_2] = (short)class310_3.method5661((byte) -35);
      int i_9;
      int i_10;
      int i_12;
      if (bool_7) {
         i_9 = class310_3.method5661((byte) 29);

         for (i_10 = 0; i_10 < i_9; i_10++) {
            int i_11 = class310_3.method5661((byte) 4);
            if (i_11 != 0) {
               this.field186[i_10][i_1][i_2] = (short)i_11;
               i_12 = class310_3.method5661((byte) -106);
               this.field178[i_10][i_1][i_2] = (byte)(i_12 >> 2);
               this.field177[i_10][i_1][i_2] = (byte)(i_12 & 0x3);
            }
         }
      }

      if (bool_8) {
         for (i_9 = 0; i_9 < i_6; i_9++) {
            i_10 = class310_3.method5661((byte) -48);
            if (i_10 != 0) {
               class34[] arr_15 = this.field179[i_9][i_1][i_2] = new class34[i_10];

               for (i_12 = 0; i_12 < i_10; i_12++) {
                  int i_13 = class310_3.method5519((short) 1005);
                  int i_14 = class310_3.method5661((byte) -37);
                  arr_15[i_12] = new class34(i_13, i_14 >> 2, i_14 & 0x3);
               }
            }
         }
      }

   }

   void method318(class244 class244_1, int i_2) {
      if (!this.method307(-1303854745)) {
         byte[] bytes_3 = class244_1.method4160(this.field183, this.field184, (short) -10440);
         if (bytes_3 != null) {
            this.vmethod745(new class310(bytes_3), 1878970684);
            this.field190 = true;
            this.field191 = true;
         }

      }
   }

   int method308(int i_1) {
      return this.field180;
   }

   int method309(short s_1) {
      return this.field188;
   }

   abstract void vmethod745(class310 var1, int var2);

   void method305(int i_1, int i_2, class310 class310_3, int i_4) {
      int i_5 = class310_3.method5661((byte) 100);
      if (i_5 != 0) {
         if ((i_5 & 0x1) != 0) {
            this.method306(i_1, i_2, class310_3, i_5, 732495103);
         } else {
            this.method314(i_1, i_2, class310_3, i_5, -550064835);
         }

      }
   }

   static final void method320(int i_0, int i_1, int i_2, int i_3, class335 class335_4, class220 class220_5, int i_6) {
      int i_7 = i_3 * i_3 + i_2 * i_2;
      if (i_7 > 4225 && i_7 < 90000) {
         int i_8 = client.field830 & 0x7ff;
         int i_9 = class139.field1797[i_8];
         int i_10 = class139.field1780[i_8];
         int i_11 = i_10 * i_2 + i_3 * i_9 >> 16;
         int i_12 = i_3 * i_10 - i_9 * i_2 >> 16;
         double d_13 = Math.atan2((double)i_11, (double)i_12);
         int i_15 = class220_5.field2537 / 2 - 25;
         int i_16 = (int)(Math.sin(d_13) * (double)i_15);
         int i_17 = (int)(Math.cos(d_13) * (double)i_15);
         byte b_18 = 20;
         class137.field1769.method6144(i_16 + (i_0 + class220_5.field2537 / 2 - b_18 / 2), class220_5.field2532 / 2 + i_1 - b_18 / 2 - i_17 - 10, b_18, b_18, 15, 15, d_13, 256);
      } else {
         class25.method250(i_0, i_1, i_2, i_3, class335_4, class220_5, -1717829494);
      }

   }

   static int method328(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      class226 class226_4 = class181.method3610(class85.field1095[--class253.field3267], 94092514);
      if (i_0 == 2800) {
         class85.field1095[++class253.field3267 - 1] = class191.method3638(class146.method3318(class226_4, 1333445423), -1857340105);
         return 1;
      } else if (i_0 != 2801) {
         if (i_0 == 2802) {
            if (class226_4.field2665 == null) {
               class85.field1096[++class85.field1105 - 1] = "";
            } else {
               class85.field1096[++class85.field1105 - 1] = class226_4.field2665;
            }

            return 1;
         } else {
            return 2;
         }
      } else {
         int i_5 = class85.field1095[--class253.field3267];
         --i_5;
         if (class226_4.field2662 != null && i_5 < class226_4.field2662.length && class226_4.field2662[i_5] != null) {
            class85.field1096[++class85.field1105 - 1] = class226_4.field2662[i_5];
         } else {
            class85.field1096[++class85.field1105 - 1] = "";
         }

         return 1;
      }
   }

}
