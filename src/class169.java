public class class169 implements class203 {

   static class246 field2024;
   public static final class169 field2025 = new class169(0, 0);
   public static final class169 field2019 = new class169(3, 1);
   public static final class169 field2021 = new class169(1, 2);
   public static final class169 field2020 = new class169(2, 3);
   public final int field2022;
   final int field2023;

   class169(int i_1, int i_2) {
      this.field2022 = i_1;
      this.field2023 = i_2;
   }

   public int vmethod6086(int i_1) {
      return this.field2023;
   }

   public static class305 method3506(class244 class244_0, class244 class244_1, String string_2, String string_3, byte b_4) {
      int i_5 = class244_0.method4206(string_2, -2112673296);
      int i_6 = class244_0.method4235(i_5, string_3, 1284626629);
      return class320.method5891(class244_0, class244_1, i_5, i_6, 1224864347);
   }

   static final void method3505(byte[] bytes_0, int i_1, int i_2, class142 class142_3, class181[] arr_4, int i_5) {
      class310 class310_6 = new class310(bytes_0);
      int i_7 = -1;

      while (true) {
         int i_8 = class310_6.method5517(-1254206875);
         if (i_8 == 0) {
            return;
         }

         i_7 += i_8;
         int i_9 = 0;

         while (true) {
            int i_10 = class310_6.method5483(772809583);
            if (i_10 == 0) {
               break;
            }

            i_9 += i_10 - 1;
            int i_11 = i_9 & 0x3f;
            int i_12 = i_9 >> 6 & 0x3f;
            int i_13 = i_9 >> 12;
            int i_14 = class310_6.method5661((byte) -13);
            int i_15 = i_14 >> 2;
            int i_16 = i_14 & 0x3;
            int i_17 = i_12 + i_1;
            int i_18 = i_11 + i_2;
            if (i_17 > 0 && i_18 > 0 && i_17 < 103 && i_18 < 103) {
               int i_19 = i_13;
               if ((class64.field507[1][i_17][i_18] & 0x2) == 2) {
                  i_19 = i_13 - 1;
               }

               class181 class181_20 = null;
               if (i_19 >= 0) {
                  class181_20 = arr_4[i_19];
               }

               class7.method90(i_13, i_17, i_18, i_7, i_16, i_15, class142_3, class181_20, (byte) 14);
            }
         }
      }
   }

   static boolean method3501(int i_0) {
      return client.field724 || class54.field416[81];
   }

}
