public class class117 {

   static class72 field1461;
   public static int[] field1460;
   class328 field1459 = new class328(256);
   class328 field1462 = new class328(256);
   class244 field1458;
   class244 field1457;

   public class117(class244 class244_1, class244 class244_2) {
      this.field1458 = class244_1;
      this.field1457 = class244_2;
   }

   class112 method2540(int i_1, int i_2, int[] ints_3, int i_4) {
      int i_5 = i_2 ^ (i_1 << 4 & 0xffff | i_1 >>> 12);
      i_5 |= i_1 << 16;
      long long_6 = (long)i_5;
      class112 class112_8 = (class112) this.field1462.method5968(long_6);
      if (class112_8 != null) {
         return class112_8;
      } else if (ints_3 != null && ints_3[0] <= 0) {
         return null;
      } else {
         class110 class110_9 = class110.method2385(this.field1458, i_1, i_2);
         if (class110_9 == null) {
            return null;
         } else {
            class112_8 = class110_9.method2387();
            this.field1462.method5956(class112_8, long_6);
            if (ints_3 != null) {
               ints_3[0] -= class112_8.field1405.length;
            }

            return class112_8;
         }
      }
   }

   class112 method2541(int i_1, int i_2, int[] ints_3, int i_4) {
      int i_5 = i_2 ^ (i_1 << 4 & 0xffff | i_1 >>> 12);
      i_5 |= i_1 << 16;
      long long_6 = (long)i_5 ^ 0x100000000L;
      class112 class112_8 = (class112) this.field1462.method5968(long_6);
      if (class112_8 != null) {
         return class112_8;
      } else if (ints_3 != null && ints_3[0] <= 0) {
         return null;
      } else {
         class111 class111_9 = (class111) this.field1459.method5968(long_6);
         if (class111_9 == null) {
            class111_9 = class111.method2396(this.field1457, i_1, i_2);
            if (class111_9 == null) {
               return null;
            }

            this.field1459.method5956(class111_9, long_6);
         }

         class112_8 = class111_9.method2401(ints_3);
         if (class112_8 == null) {
            return null;
         } else {
            class111_9.method3628();
            this.field1462.method5956(class112_8, long_6);
            return class112_8;
         }
      }
   }

   public class112 method2542(int i_1, int[] ints_2, int i_3) {
      if (this.field1458.method4173(939845919) == 1) {
         return this.method2540(0, i_1, ints_2, -2076179570);
      } else if (this.field1458.method4172(i_1, (byte) 124) == 1) {
         return this.method2540(i_1, 0, ints_2, -2133307689);
      } else {
         throw new RuntimeException();
      }
   }

   public class112 method2545(int i_1, int[] ints_2, int i_3) {
      if (this.field1457.method4173(1511641592) == 1) {
         return this.method2541(0, i_1, ints_2, 1195097760);
      } else if (this.field1457.method4172(i_1, (byte) 7) == 1) {
         return this.method2541(i_1, 0, ints_2, -1227277117);
      } else {
         throw new RuntimeException();
      }
   }

}
