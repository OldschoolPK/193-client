public class class74 extends class183 {

   public boolean vmethod3613(int i_1, int i_2, int i_3, class181 class181_4, byte b_5) {
      return i_2 == super.field2125 && i_3 == super.field2123;
   }

   static void method1301(int i_0) {
      client.field785 = 0;
      client.field784 = false;
      client.field790[0] = "Cancel";
      client.field791[0] = "";
      client.field844[0] = 1006;
      client.field792[0] = false;
      client.field785 = 1;
   }

   static int method1302(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      if (i_0 == 3600) {
         if (class173.field2036.field1069 == 0) {
            class85.field1095[++class253.field3267 - 1] = -2;
         } else if (class173.field2036.field1069 == 1) {
            class85.field1095[++class253.field3267 - 1] = -1;
         } else {
            class85.field1095[++class253.field3267 - 1] = class173.field2036.field1067.method5071(1972857836);
         }

         return 1;
      } else {
         int i_4;
         if (i_0 == 3601) {
            i_4 = class85.field1095[--class253.field3267];
            if (class173.field2036.method1911(-413073532) && i_4 >= 0 && i_4 < class173.field2036.field1067.method5071(1679671897)) {
               class294 class294_9 = (class294) class173.field2036.field1067.method5081(i_4, -1943833637);
               class85.field1096[++class85.field1105 - 1] = class294_9.method5025((short) 2928);
               class85.field1096[++class85.field1105 - 1] = class294_9.method5026(1728130643);
            } else {
               class85.field1096[++class85.field1105 - 1] = "";
               class85.field1096[++class85.field1105 - 1] = "";
            }

            return 1;
         } else if (i_0 == 3602) {
            i_4 = class85.field1095[--class253.field3267];
            if (class173.field2036.method1911(-779626498) && i_4 >= 0 && i_4 < class173.field2036.field1067.method5071(1366781508)) {
               class85.field1095[++class253.field3267 - 1] = ((class289) class173.field2036.field1067.method5081(i_4, -1052647042)).field3650;
            } else {
               class85.field1095[++class253.field3267 - 1] = 0;
            }

            return 1;
         } else if (i_0 == 3603) {
            i_4 = class85.field1095[--class253.field3267];
            if (class173.field2036.method1911(-1979797465) && i_4 >= 0 && i_4 < class173.field2036.field1067.method5071(2051278630)) {
               class85.field1095[++class253.field3267 - 1] = ((class289) class173.field2036.field1067.method5081(i_4, -900600114)).field3651;
            } else {
               class85.field1095[++class253.field3267 - 1] = 0;
            }

            return 1;
         } else {
            String string_6;
            if (i_0 == 3604) {
               string_6 = class85.field1096[--class85.field1105];
               int i_7 = class85.field1095[--class253.field3267];
               class76.method1359(string_6, i_7, -812111829);
               return 1;
            } else if (i_0 == 3605) {
               string_6 = class85.field1096[--class85.field1105];
               class173.field2036.method1899(string_6, -262000374);
               return 1;
            } else if (i_0 == 3606) {
               string_6 = class85.field1096[--class85.field1105];
               class173.field2036.method1903(string_6, 92133268);
               return 1;
            } else if (i_0 == 3607) {
               string_6 = class85.field1096[--class85.field1105];
               class173.field2036.method1901(string_6, -1480172940);
               return 1;
            } else if (i_0 == 3608) {
               string_6 = class85.field1096[--class85.field1105];
               class173.field2036.method1904(string_6, (byte) 40);
               return 1;
            } else if (i_0 == 3609) {
               string_6 = class85.field1096[--class85.field1105];
               string_6 = class22.method220(string_6, -1865855416);
               class85.field1095[++class253.field3267 - 1] = class173.field2036.method1897(new class293(string_6, class19.field92), false, (byte) 66) ? 1 : 0;
               return 1;
            } else if (i_0 == 3611) {
               if (class35.field244 != null) {
                  class85.field1096[++class85.field1105 - 1] = class35.field244.field3669;
               } else {
                  class85.field1096[++class85.field1105 - 1] = "";
               }

               return 1;
            } else if (i_0 == 3612) {
               if (class35.field244 != null) {
                  class85.field1095[++class253.field3267 - 1] = class35.field244.method5071(1469511807);
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 3613) {
               i_4 = class85.field1095[--class253.field3267];
               if (class35.field244 != null && i_4 < class35.field244.method5071(2009853718)) {
                  class85.field1096[++class85.field1105 - 1] = class35.field244.method5081(i_4, -2015369298).method5038(1670678120).method5186(-1648120702);
               } else {
                  class85.field1096[++class85.field1105 - 1] = "";
               }

               return 1;
            } else if (i_0 == 3614) {
               i_4 = class85.field1095[--class253.field3267];
               if (class35.field244 != null && i_4 < class35.field244.method5071(1825150249)) {
                  class85.field1095[++class253.field3267 - 1] = ((class289) class35.field244.method5081(i_4, 291019743)).method5155(-2024545535);
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 3615) {
               i_4 = class85.field1095[--class253.field3267];
               if (class35.field244 != null && i_4 < class35.field244.method5071(1438205515)) {
                  class85.field1095[++class253.field3267 - 1] = ((class289) class35.field244.method5081(i_4, 541297568)).field3651;
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 3616) {
               class85.field1095[++class253.field3267 - 1] = class35.field244 != null ? class35.field244.field3674 : 0;
               return 1;
            } else if (i_0 == 3617) {
               string_6 = class85.field1096[--class85.field1105];
               class64.method1207(string_6, (short) 2048);
               return 1;
            } else if (i_0 == 3618) {
               class85.field1095[++class253.field3267 - 1] = class35.field244 != null ? class35.field244.field3672 : 0;
               return 1;
            } else if (i_0 == 3619) {
               string_6 = class85.field1096[--class85.field1105];
               class99.method2234(string_6, 1893534902);
               return 1;
            } else if (i_0 == 3620) {
               class313.method5785(-244499356);
               return 1;
            } else if (i_0 == 3621) {
               if (!class173.field2036.method1911(-105683808)) {
                  class85.field1095[++class253.field3267 - 1] = -1;
               } else {
                  class85.field1095[++class253.field3267 - 1] = class173.field2036.field1068.method5071(1350870585);
               }

               return 1;
            } else if (i_0 == 3622) {
               i_4 = class85.field1095[--class253.field3267];
               if (class173.field2036.method1911(1325571435) && i_4 >= 0 && i_4 < class173.field2036.field1068.method5071(2021463436)) {
                  class288 class288_5 = (class288) class173.field2036.field1068.method5081(i_4, -418547073);
                  class85.field1096[++class85.field1105 - 1] = class288_5.method5025((short) 2928);
                  class85.field1096[++class85.field1105 - 1] = class288_5.method5026(-919941774);
               } else {
                  class85.field1096[++class85.field1105 - 1] = "";
                  class85.field1096[++class85.field1105 - 1] = "";
               }

               return 1;
            } else if (i_0 == 3623) {
               string_6 = class85.field1096[--class85.field1105];
               string_6 = class22.method220(string_6, -665850396);
               class85.field1095[++class253.field3267 - 1] = class173.field2036.method1936(new class293(string_6, class19.field92), -454135084) ? 1 : 0;
               return 1;
            } else if (i_0 == 3624) {
               i_4 = class85.field1095[--class253.field3267];
               if (class35.field244 != null && i_4 < class35.field244.method5071(1828138503) && class35.field244.method5081(i_4, -1920273019).method5038(1670678120).equals(class223.field2562.field619)) {
                  class85.field1095[++class253.field3267 - 1] = 1;
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 == 3625) {
               if (class35.field244 != null && class35.field244.field3673 != null) {
                  class85.field1096[++class85.field1105 - 1] = class35.field244.field3673;
               } else {
                  class85.field1096[++class85.field1105 - 1] = "";
               }

               return 1;
            } else if (i_0 == 3626) {
               i_4 = class85.field1095[--class253.field3267];
               if (class35.field244 != null && i_4 < class35.field244.method5071(1895660439) && ((class283) class35.field244.method5081(i_4, 619638482)).method5015(-1638349091)) {
                  class85.field1095[++class253.field3267 - 1] = 1;
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else if (i_0 != 3627) {
               if (i_0 == 3628) {
                  class173.field2036.field1067.method5069(1918060687);
                  return 1;
               } else {
                  boolean bool_8;
                  if (i_0 == 3629) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class324(bool_8), 345387098);
                     return 1;
                  } else if (i_0 == 3630) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class325(bool_8), -443946675);
                     return 1;
                  } else if (i_0 == 3631) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class166(bool_8), -953682122);
                     return 1;
                  } else if (i_0 == 3632) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class160(bool_8), -852907174);
                     return 1;
                  } else if (i_0 == 3633) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class165(bool_8), 151172076);
                     return 1;
                  } else if (i_0 == 3634) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class168(bool_8), -1637200798);
                     return 1;
                  } else if (i_0 == 3635) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class164(bool_8), -604005466);
                     return 1;
                  } else if (i_0 == 3636) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class162(bool_8), -1690778580);
                     return 1;
                  } else if (i_0 == 3637) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class161(bool_8), -1683674904);
                     return 1;
                  } else if (i_0 == 3638) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class163(bool_8), 146936524);
                     return 1;
                  } else if (i_0 == 3639) {
                     class173.field2036.field1067.method5082(1917063500);
                     return 1;
                  } else if (i_0 == 3640) {
                     class173.field2036.field1068.method5069(1897236335);
                     return 1;
                  } else if (i_0 == 3641) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1068.method5139(new class324(bool_8), 355276768);
                     return 1;
                  } else if (i_0 == 3642) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1068.method5139(new class325(bool_8), -244102223);
                     return 1;
                  } else if (i_0 == 3643) {
                     class173.field2036.field1068.method5082(1929586782);
                     return 1;
                  } else if (i_0 == 3644) {
                     if (class35.field244 != null) {
                        class35.field244.method5069(949323751);
                     }

                     return 1;
                  } else if (i_0 == 3645) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class324(bool_8), -1936802834);
                     }

                     return 1;
                  } else if (i_0 == 3646) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class325(bool_8), -319059070);
                     }

                     return 1;
                  } else if (i_0 == 3647) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class166(bool_8), -524279034);
                     }

                     return 1;
                  } else if (i_0 == 3648) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class160(bool_8), -245575436);
                     }

                     return 1;
                  } else if (i_0 == 3649) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class165(bool_8), 395847525);
                     }

                     return 1;
                  } else if (i_0 == 3650) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class168(bool_8), 284692852);
                     }

                     return 1;
                  } else if (i_0 == 3651) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class164(bool_8), -2116808317);
                     }

                     return 1;
                  } else if (i_0 == 3652) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class162(bool_8), -368731425);
                     }

                     return 1;
                  } else if (i_0 == 3653) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class161(bool_8), -2143527633);
                     }

                     return 1;
                  } else if (i_0 == 3654) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class163(bool_8), -456059258);
                     }

                     return 1;
                  } else if (i_0 == 3655) {
                     if (class35.field244 != null) {
                        class35.field244.method5082(1989546643);
                     }

                     return 1;
                  } else if (i_0 == 3656) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     class173.field2036.field1067.method5139(new class167(bool_8), -173580394);
                     return 1;
                  } else if (i_0 == 3657) {
                     bool_8 = class85.field1095[--class253.field3267] == 1;
                     if (class35.field244 != null) {
                        class35.field244.method5139(new class167(bool_8), 233736602);
                     }

                     return 1;
                  } else {
                     return 2;
                  }
               }
            } else {
               i_4 = class85.field1095[--class253.field3267];
               if (class35.field244 != null && i_4 < class35.field244.method5071(2084285395) && ((class283) class35.field244.method5081(i_4, -1095455283)).method5006(-842680773)) {
                  class85.field1095[++class253.field3267 - 1] = 1;
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            }
         }
      }
   }

}
