import java.util.Comparator;

final class class17 implements Comparator {

   static class305 field74;
   static String field75;
   static int[] field76;

   int method159(class16 class16_1, class16 class16_2, byte b_3) {
      return class16_1.field72.field78 < class16_2.field72.field78 ? -1 : (class16_2.field72.field78 == class16_1.field72.field78 ? 0 : 1);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method159((class16) object_1, (class16) object_2, (byte) 5);
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   static final void method165(String string_0, int i_1) {
      StringBuilder stringbuilder_10000 = new StringBuilder();
      Object obj_10001 = null;
      stringbuilder_10000 = stringbuilder_10000.append("Please remove ").append(string_0);
      obj_10001 = null;
      String string_2 = stringbuilder_10000.append(" from your friend list first").toString();
      class234.method4129(30, "", string_2, (byte) 66);
   }

}
