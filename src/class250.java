public class class250 extends class184 {

   public static class244 field3232;
   static class154 field3231 = new class154(64);
   public int field3234 = 0;

   void method4337(class310 class310_1, int i_2, byte b_3) {
      if (i_2 == 2) {
         this.field3234 = class310_1.method5729(600189760);
      }

   }

   void method4336(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) 44);
         if (i_3 == 0) {
            return;
         }

         this.method4337(class310_1, i_3, (byte) 127);
      }
   }

   public static class194[] method4338(int i_0) {
      return new class194[] {class194.field2348, class194.field2342, class194.field2344, class194.field2350, class194.field2346, class194.field2345, class194.field2343, class194.field2349, class194.field2347, class194.field2351};
   }

   static final void method4335(int i_0, int i_1, int i_2, int i_3, int i_4) {
      if (client.field801 == 0 && !client.field803) {
         class38.method564("Walk here", "", 23, 0, i_0 - i_2, i_1 - i_3, -1426103660);
      }

      long long_5 = -1L;
      long long_7 = -1L;
      int i_9 = 0;

      while (true) {
         int i_11 = class137.field1763;
         if (i_9 >= i_11) {
            if (long_5 != -1L) {
               i_9 = class192.method3642(long_5);
               int i_10 = (int)(long_5 >>> 7 & 0x7fL);
               class75 class75_12 = client.field764[client.field814];
               class132.method2905(class75_12, client.field814, i_9, i_10, -1914334357);
            }

            return;
         }

         long long_28 = class137.field1768[i_9];
         if (long_28 != long_7) {
            label332: {
               long_7 = long_28;
               int i_16 = class81.method1876(i_9, (byte) -124);
               int i_17 = class64.method1170(i_9, (byte) 5);
               int i_18 = class226.method4044(class137.field1768[i_9]);
               int i_20 = class149.method3348(class137.field1768[i_9]);
               int i_21 = i_20;
               if (i_18 == 2 && class67.field536.method3139(class151.field1949, i_16, i_17, long_28) >= 0) {
                  class264 class264_22 = class34.method383(i_20, 541698938);
                  if (class264_22.field3421 != null) {
                     class264_22 = class264_22.method4599(2034714891);
                  }

                  if (class264_22 == null) {
                     break label332;
                  }

                  if (client.field801 == 1) {
                     class38.method564("Use", client.field744 + " " + "->" + " " + class23.method222(65535, (byte) 126) + class264_22.field3394, 1, i_20, i_16, i_17, -1983438092);
                  } else if (client.field803) {
                     if ((class75.field637 & 0x4) == 4) {
                        class38.method564(client.field851, client.field807 + " " + "->" + " " + class23.method222(65535, (byte) 48) + class264_22.field3394, 2, i_20, i_16, i_17, -1366970230);
                     }
                  } else {
                     String[] arr_30 = class264_22.field3407;
                     if (arr_30 != null) {
                        for (int i_31 = 4; i_31 >= 0; --i_31) {
                           if (arr_30[i_31] != null) {
                              short s_25 = 0;
                              if (i_31 == 0) {
                                 s_25 = 3;
                              }

                              if (i_31 == 1) {
                                 s_25 = 4;
                              }

                              if (i_31 == 2) {
                                 s_25 = 5;
                              }

                              if (i_31 == 3) {
                                 s_25 = 6;
                              }

                              if (i_31 == 4) {
                                 s_25 = 1001;
                              }

                              class38.method564(arr_30[i_31], class23.method222(65535, (byte) 119) + class264_22.field3394, s_25, i_21, i_16, i_17, -1443617858);
                           }
                        }
                     }

                     class38.method564("Examine", class23.method222(65535, (byte) 124) + class264_22.field3394, 1002, class264_22.field3387, i_16, i_17, -2087404181);
                  }
               }

               int i_23;
               class88 class88_24;
               class75 class75_33;
               int[] ints_37;
               int i_39;
               if (i_18 == 1) {
                  class88 class88_34 = client.field689[i_21];
                  if (class88_34 == null) {
                     break label332;
                  }

                  if (class88_34.field1135.field3500 == 1 && (class88_34.field990 & 0x7f) == 64 && (class88_34.field938 & 0x7f) == 64) {
                     for (i_23 = 0; i_23 < client.field690; i_23++) {
                        class88_24 = client.field689[client.field691[i_23]];
                        if (class88_24 != null && class88_24 != class88_34 && class88_24.field1135.field3500 == 1 && class88_34.field990 == class88_24.field990 && class88_34.field938 == class88_24.field938) {
                           class96.method2163(class88_24.field1135, client.field691[i_23], i_16, i_17, (short) 13307);
                        }
                     }

                     i_23 = class98.field1263;
                     ints_37 = class98.field1268;

                     for (i_39 = 0; i_39 < i_23; i_39++) {
                        class75_33 = client.field764[ints_37[i_39]];
                        if (class75_33 != null && class88_34.field990 == class75_33.field990 && class75_33.field938 == class88_34.field938) {
                           class132.method2905(class75_33, ints_37[i_39], i_16, i_17, -1886391886);
                        }
                     }
                  }

                  class96.method2163(class88_34.field1135, i_21, i_16, i_17, (short) -32723);
               }

               if (i_18 == 0) {
                  class75 class75_35 = client.field764[i_21];
                  if (class75_35 == null) {
                     break label332;
                  }

                  if ((class75_35.field990 & 0x7f) == 64 && (class75_35.field938 & 0x7f) == 64) {
                     for (i_23 = 0; i_23 < client.field690; i_23++) {
                        class88_24 = client.field689[client.field691[i_23]];
                        if (class88_24 != null && class88_24.field1135.field3500 == 1 && class88_24.field990 == class75_35.field990 && class75_35.field938 == class88_24.field938) {
                           class96.method2163(class88_24.field1135, client.field691[i_23], i_16, i_17, (short) 9349);
                        }
                     }

                     i_23 = class98.field1263;
                     ints_37 = class98.field1268;

                     for (i_39 = 0; i_39 < i_23; i_39++) {
                        class75_33 = client.field764[ints_37[i_39]];
                        if (class75_33 != null && class75_35 != class75_33 && class75_35.field990 == class75_33.field990 && class75_35.field938 == class75_33.field938) {
                           class132.method2905(class75_33, ints_37[i_39], i_16, i_17, -1861673723);
                        }
                     }
                  }

                  if (i_21 != client.field814) {
                     class132.method2905(class75_35, i_21, i_16, i_17, -1893006196);
                  } else {
                     long_5 = long_28;
                  }
               }

               if (i_18 == 3) {
                  class272 class272_36 = client.field776[class151.field1949][i_16][i_17];
                  if (class272_36 != null) {
                     for (class96 class96_40 = (class96) class272_36.method4883(); class96_40 != null; class96_40 = (class96) class272_36.method4893()) {
                        class265 class265_38 = class66.method1223(class96_40.field1238, -1601826115);
                        if (client.field801 == 1) {
                           class38.method564("Use", client.field744 + " " + "->" + " " + class23.method222(16748608, (byte) 56) + class265_38.field3444, 16, class96_40.field1238, i_16, i_17, -1632347980);
                        } else if (client.field803) {
                           if ((class75.field637 & 0x1) == 1) {
                              class38.method564(client.field851, client.field807 + " " + "->" + " " + class23.method222(16748608, (byte) 45) + class265_38.field3444, 17, class96_40.field1238, i_16, i_17, -2003609349);
                           }
                        } else {
                           String[] arr_32 = class265_38.field3458;

                           for (int i_26 = 4; i_26 >= 0; --i_26) {
                              if (arr_32 != null && arr_32[i_26] != null) {
                                 byte b_27 = 0;
                                 if (i_26 == 0) {
                                    b_27 = 18;
                                 }

                                 if (i_26 == 1) {
                                    b_27 = 19;
                                 }

                                 if (i_26 == 2) {
                                    b_27 = 20;
                                 }

                                 if (i_26 == 3) {
                                    b_27 = 21;
                                 }

                                 if (i_26 == 4) {
                                    b_27 = 22;
                                 }

                                 class38.method564(arr_32[i_26], class23.method222(16748608, (byte) 64) + class265_38.field3444, b_27, class96_40.field1238, i_16, i_17, -1864182741);
                              } else if (i_26 == 2) {
                                 class38.method564("Take", class23.method222(16748608, (byte) 15) + class265_38.field3444, 20, class96_40.field1238, i_16, i_17, -2098979429);
                              }
                           }

                           class38.method564("Examine", class23.method222(16748608, (byte) 99) + class265_38.field3444, 1004, class96_40.field1238, i_16, i_17, -1571486290);
                        }
                     }
                  }
               }
            }
         }

         ++i_9;
      }
   }

}
