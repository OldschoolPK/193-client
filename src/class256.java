public class class256 extends class184 {

   public static int field3295;
   public static class244 field3299;
   static class177 field3296;
   static class154 field3294 = new class154(64);
   public int field3301 = -1;
   int[] field3303 = new int[] {-1, -1, -1, -1, -1};
   public boolean field3304 = false;
   int[] field3298;
   short[] field3297;
   short[] field3300;
   short[] field3306;
   short[] field3302;

   void method4446(class310 class310_1, int i_2, byte b_3) {
      if (i_2 == 1) {
         this.field3301 = class310_1.method5661((byte) -43);
      } else {
         int i_4;
         int i_5;
         if (i_2 == 2) {
            i_4 = class310_1.method5661((byte) 92);
            this.field3298 = new int[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3298[i_5] = class310_1.method5729(-2114958668);
            }
         } else if (i_2 == 3) {
            this.field3304 = true;
         } else if (i_2 == 40) {
            i_4 = class310_1.method5661((byte) 16);
            this.field3297 = new short[i_4];
            this.field3300 = new short[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3297[i_5] = (short)class310_1.method5729(1543579078);
               this.field3300[i_5] = (short)class310_1.method5729(50484743);
            }
         } else if (i_2 == 41) {
            i_4 = class310_1.method5661((byte) 87);
            this.field3306 = new short[i_4];
            this.field3302 = new short[i_4];

            for (i_5 = 0; i_5 < i_4; i_5++) {
               this.field3306[i_5] = (short)class310_1.method5729(-1851725994);
               this.field3302[i_5] = (short)class310_1.method5729(-1351932387);
            }
         } else if (i_2 >= 60 && i_2 < 70) {
            this.field3303[i_2 - 60] = class310_1.method5729(-294761566);
         }
      }

   }

   public boolean method4455(int i_1) {
      boolean bool_2 = true;

      for (int i_3 = 0; i_3 < 5; i_3++) {
         if (this.field3303[i_3] != -1 && !class227.field2732.method4162(this.field3303[i_3], 0, 1281141660)) {
            bool_2 = false;
         }
      }

      return bool_2;
   }

   public class130 method4434(int i_1) {
      class130[] arr_2 = new class130[5];
      int i_3 = 0;

      for (int i_4 = 0; i_4 < 5; i_4++) {
         if (this.field3303[i_4] != -1) {
            arr_2[i_3++] = class130.method2824(class227.field2732, this.field3303[i_4], 0);
         }
      }

      class130 class130_6 = new class130(arr_2, i_3);
      int i_5;
      if (this.field3297 != null) {
         for (i_5 = 0; i_5 < this.field3297.length; i_5++) {
            class130_6.method2837(this.field3297[i_5], this.field3300[i_5]);
         }
      }

      if (this.field3306 != null) {
         for (i_5 = 0; i_5 < this.field3306.length; i_5++) {
            class130_6.method2833(this.field3306[i_5], this.field3302[i_5]);
         }
      }

      return class130_6;
   }

   public boolean method4431(int i_1) {
      if (this.field3298 == null) {
         return true;
      } else {
         boolean bool_2 = true;

         for (int i_3 = 0; i_3 < this.field3298.length; i_3++) {
            if (!class227.field2732.method4162(this.field3298[i_3], 0, -1907377702)) {
               bool_2 = false;
            }
         }

         return bool_2;
      }
   }

   void method4429(class310 class310_1, short s_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) 81);
         if (i_3 == 0) {
            return;
         }

         this.method4446(class310_1, i_3, (byte) -62);
      }
   }

   public class130 method4432(int i_1) {
      if (this.field3298 == null) {
         return null;
      } else {
         class130[] arr_2 = new class130[this.field3298.length];

         for (int i_3 = 0; i_3 < this.field3298.length; i_3++) {
            arr_2[i_3] = class130.method2824(class227.field2732, this.field3298[i_3], 0);
         }

         class130 class130_5;
         if (arr_2.length == 1) {
            class130_5 = arr_2[0];
         } else {
            class130_5 = new class130(arr_2, arr_2.length);
         }

         int i_4;
         if (this.field3297 != null) {
            for (i_4 = 0; i_4 < this.field3297.length; i_4++) {
               class130_5.method2837(this.field3297[i_4], this.field3300[i_4]);
            }
         }

         if (this.field3306 != null) {
            for (i_4 = 0; i_4 < this.field3306.length; i_4++) {
               class130_5.method2833(this.field3306[i_4], this.field3302[i_4]);
            }
         }

         return class130_5;
      }
   }

   public static void method4458(int i_0, int i_1, int i_2) {
      class259 class259_4 = (class259) class259.field3324.method3376((long)i_0);
      class259 class259_3;
      if (class259_4 != null) {
         class259_3 = class259_4;
      } else {
         byte[] bytes_9 = class259.field3323.method4160(14, i_0, (short) -10613);
         class259_4 = new class259();
         if (bytes_9 != null) {
            class259_4.method4484(new class310(bytes_9), 1196473375);
         }

         class259.field3324.method3374(class259_4, (long)i_0);
         class259_3 = class259_4;
      }

      int i_5 = class259_3.field3326;
      int i_6 = class259_3.field3328;
      int i_7 = class259_3.field3327;
      int i_8 = class221.field2540[i_7 - i_6];
      if (i_1 < 0 || i_1 > i_8) {
         i_1 = 0;
      }

      i_8 <<= i_6;
      class221.field2541[i_5] = class221.field2541[i_5] & ~i_8 | i_1 << i_6 & i_8;
   }

}
