import java.io.DataInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.Queue;

public class class152 implements Runnable {

   static class335 field1964;
   static int field1963;
   Queue field1961 = new LinkedList();
   final Thread field1965 = new Thread(this);
   volatile boolean field1962;

   public class152() {
      this.field1965.setPriority(1);
      this.field1965.start();
   }

   public void method3356(byte b_1) {
      this.field1962 = true;

      try {
         synchronized(this) {
            this.notify();
         }

         this.field1965.join();
      } catch (InterruptedException interruptedexception_5) {
         ;
      }

   }

   public class153 method3354(URL url_1, int i_2) {
      class153 class153_3 = new class153(url_1);
      synchronized(this) {
         this.field1961.add(class153_3);
         this.notify();
         return class153_3;
      }
   }

   public void run() {
      while (!this.field1962) {
         try {
            class153 class153_1;
            synchronized(this) {
               class153_1 = (class153) this.field1961.poll();
               if (class153_1 == null) {
                  try {
                     this.wait();
                  } catch (InterruptedException interruptedexception_13) {
                     ;
                  }
                  continue;
               }
            }

            DataInputStream datainputstream_2 = null;
            URLConnection urlconnection_3 = null;

            try {
               urlconnection_3 = class153_1.field1968.openConnection();
               urlconnection_3.setConnectTimeout(5000);
               urlconnection_3.setReadTimeout(5000);
               urlconnection_3.setUseCaches(false);
               urlconnection_3.setRequestProperty("Connection", "close");
               int i_7 = urlconnection_3.getContentLength();
               if (i_7 >= 0) {
                  byte[] bytes_5 = new byte[i_7];
                  datainputstream_2 = new DataInputStream(urlconnection_3.getInputStream());
                  datainputstream_2.readFully(bytes_5);
                  class153_1.field1972 = bytes_5;
               }

               class153_1.field1969 = true;
            } catch (IOException ioexception_14) {
               class153_1.field1969 = true;
            } finally {
               if (datainputstream_2 != null) {
                  datainputstream_2.close();
               }

               if (urlconnection_3 != null && urlconnection_3 instanceof HttpURLConnection) {
                  ((HttpURLConnection) urlconnection_3).disconnect();
               }

            }
         } catch (Exception exception_17) {
            class223.method4011((String) null, exception_17, 2027156074);
         }
      }

   }

   public static void method3353(byte b_0) {
      try {
         if (class206.field2421 == 1) {
            int i_1 = class206.field2426.method3785(643767530);
            if (i_1 > 0 && class206.field2426.method3727(-185002283)) {
               i_1 -= class42.field312;
               if (i_1 < 0) {
                  i_1 = 0;
               }

               class206.field2426.method3720(i_1, (short) 1513);
               return;
            }

            class206.field2426.method3799((short) 490);
            class206.field2426.method3724(1115530445);
            if (class221.field2538 != null) {
               class206.field2421 = 2;
            } else {
               class206.field2421 = 0;
            }

            class178.field2071 = null;
            class206.field2424 = null;
         }
      } catch (Exception exception_3) {
         exception_3.printStackTrace();
         class206.field2426.method3799((short) -15313);
         class206.field2421 = 0;
         class178.field2071 = null;
         class206.field2424 = null;
         class221.field2538 = null;
      }

   }

   public static class203 method3363(class203[] arr_0, int i_1, int i_2) {
      class203[] arr_3 = arr_0;

      for (int i_4 = 0; i_4 < arr_3.length; i_4++) {
         class203 class203_5 = arr_3[i_4];
         if (i_1 == class203_5.vmethod6086(-275768542)) {
            return class203_5;
         }
      }

      return null;
   }

   public static int method3365(CharSequence charsequence_0, CharSequence charsequence_1, class199 class199_2, int i_3) {
      int i_4 = charsequence_0.length();
      int i_5 = charsequence_1.length();
      int i_6 = 0;
      int i_7 = 0;
      byte b_8 = 0;
      byte b_9 = 0;

      while (i_6 - b_8 < i_4 || i_7 - b_9 < i_5) {
         if (i_6 - b_8 >= i_4) {
            return -1;
         }

         if (i_7 - b_9 >= i_5) {
            return 1;
         }

         char var_10;
         if (b_8 != 0) {
            var_10 = (char) b_8;
            boolean bool_15 = false;
         } else {
            var_10 = charsequence_0.charAt(i_6++);
         }

         char var_11;
         if (b_9 != 0) {
            var_11 = (char) b_9;
            boolean bool_16 = false;
         } else {
            var_11 = charsequence_1.charAt(i_7++);
         }

         byte b_12;
         if (var_10 == 198) {
            b_12 = 69;
         } else if (var_10 == 230) {
            b_12 = 101;
         } else if (var_10 == 223) {
            b_12 = 115;
         } else if (var_10 == 338) {
            b_12 = 69;
         } else if (var_10 == 339) {
            b_12 = 101;
         } else {
            b_12 = 0;
         }

         b_8 = b_12;
         byte b_13;
         if (var_11 == 198) {
            b_13 = 69;
         } else if (var_11 == 230) {
            b_13 = 101;
         } else if (var_11 == 223) {
            b_13 = 115;
         } else if (var_11 == 338) {
            b_13 = 69;
         } else if (var_11 == 339) {
            b_13 = 101;
         } else {
            b_13 = 0;
         }

         b_9 = b_13;
         var_10 = class165.method3481(var_10, class199_2, -531904522);
         var_11 = class165.method3481(var_11, class199_2, -531904522);
         if (var_10 != var_11 && Character.toUpperCase(var_10) != Character.toUpperCase(var_11)) {
            var_10 = Character.toLowerCase(var_10);
            var_11 = Character.toLowerCase(var_11);
            if (var_11 != var_10) {
               return class47.method734(var_10, class199_2, -2123466644) - class47.method734(var_11, class199_2, -2126592731);
            }
         }
      }

      int i_17 = Math.min(i_4, i_5);

      int i_18;
      char var_21;
      for (i_18 = 0; i_18 < i_17; i_18++) {
         if (class199_2 == class199.field2375) {
            i_6 = i_4 - 1 - i_18;
            i_7 = i_5 - 1 - i_18;
         } else {
            i_7 = i_18;
            i_6 = i_18;
         }

         char var_19 = charsequence_0.charAt(i_6);
         var_21 = charsequence_1.charAt(i_7);
         if (var_21 != var_19 && Character.toUpperCase(var_19) != Character.toUpperCase(var_21)) {
            var_19 = Character.toLowerCase(var_19);
            var_21 = Character.toLowerCase(var_21);
            if (var_19 != var_21) {
               return class47.method734(var_19, class199_2, -2004067316) - class47.method734(var_21, class199_2, -2088325046);
            }
         }
      }

      i_18 = i_4 - i_5;
      if (i_18 != 0) {
         return i_18;
      } else {
         for (int i_20 = 0; i_20 < i_17; i_20++) {
            var_21 = charsequence_0.charAt(i_20);
            char var_14 = charsequence_1.charAt(i_20);
            if (var_21 != var_14) {
               return class47.method734(var_21, class199_2, -1980215822) - class47.method734(var_14, class199_2, -2053875232);
            }
         }

         return 0;
      }
   }

   public static int method3355(class310 class310_0, String string_1, int i_2) {
      int i_3 = class310_0.field3751;
      byte[] bytes_4 = class95.method2143(string_1, 251926976);
      class310_0.method5585(bytes_4.length, 2062071032);
      class310_0.field3751 += class219.field2528.method3960(bytes_4, 0, bytes_4.length, class310_0.field3752, class310_0.field3751, 2127333577);
      return class310_0.field3751 - i_3;
   }

}
