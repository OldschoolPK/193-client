import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public final class class54 implements KeyListener, FocusListener {

   static int field421;
   static class54 field407 = new class54();
   public static boolean[] field416 = new boolean[112];
   static int[] field408 = new int[128];
   static int field401 = 0;
   static int field410 = 0;
   static char[] field409 = new char[128];
   static int[] field412 = new int[128];
   public static int[] field413 = new int[128];
   public static int field414 = 0;
   static int field415 = 0;
   static int field419 = 0;
   static int field417 = 0;
   static volatile int field418 = 0;
   static int[] field398 = new int[] {-1, -1, -1, -1, -1, -1, -1, -1, 85, 80, 84, -1, 91, -1, -1, -1, 81, 82, 86, -1, -1, -1, -1, -1, -1, -1, -1, 13, -1, -1, -1, -1, 83, 104, 105, 103, 102, 96, 98, 97, 99, -1, -1, -1, -1, -1, -1, -1, 25, 16, 17, 18, 19, 20, 21, 22, 23, 24, -1, -1, -1, -1, -1, -1, -1, 48, 68, 66, 50, 34, 51, 52, 53, 39, 54, 55, 56, 70, 69, 40, 41, 32, 35, 49, 36, 38, 67, 33, 65, 37, 64, -1, -1, -1, -1, -1, 228, 231, 227, 233, 224, 219, 225, 230, 226, 232, 89, 87, -1, 88, 229, 90, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -1, -1, 101, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 100, -1, 87, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};

   public final synchronized void keyPressed(KeyEvent keyevent_1) {
      if (field407 != null) {
         int i_2 = keyevent_1.getKeyCode();
         if (i_2 >= 0 && i_2 < field398.length) {
            i_2 = field398[i_2];
            if ((i_2 & 0x80) != 0) {
               i_2 = -1;
            }
         } else {
            i_2 = -1;
         }

         if (field410 >= 0 && i_2 >= 0) {
            field408[field410] = i_2;
            field410 = field410 + 1 & 0x7f;
            if (field401 == field410) {
               field410 = -1;
            }
         }

         int i_3;
         if (i_2 >= 0) {
            i_3 = field419 + 1 & 0x7f;
            if (i_3 != field415) {
               field412[field419] = i_2;
               field409[field419] = 0;
               field419 = i_3;
            }
         }

         i_3 = keyevent_1.getModifiers();
         if ((i_3 & 0xa) != 0 || i_2 == 85 || i_2 == 10) {
            keyevent_1.consume();
         }
      }

   }

   public final void focusGained(FocusEvent focusevent_1) {
   }

   public final synchronized void focusLost(FocusEvent focusevent_1) {
      if (field407 != null) {
         field410 = -1;
      }

   }

   public final synchronized void keyReleased(KeyEvent keyevent_1) {
      if (field407 != null) {
         int i_2 = keyevent_1.getKeyCode();
         if (i_2 >= 0 && i_2 < field398.length) {
            i_2 = field398[i_2] & ~0x80;
         } else {
            i_2 = -1;
         }

         if (field410 >= 0 && i_2 >= 0) {
            field408[field410] = ~i_2;
            field410 = field410 + 1 & 0x7f;
            if (field410 == field401) {
               field410 = -1;
            }
         }
      }

      keyevent_1.consume();
   }

   public final void keyTyped(KeyEvent keyevent_1) {
      if (field407 != null) {
         char var_2 = keyevent_1.getKeyChar();
         if (var_2 != 0 && var_2 != 65535 && class24.method247(var_2, 69015458)) {
            int i_3 = field419 + 1 & 0x7f;
            if (i_3 != field415) {
               field412[field419] = -1;
               field409[field419] = var_2;
               field419 = i_3;
            }
         }
      }

      keyevent_1.consume();
   }

   static void method872(int i_0, byte[] bytes_1, class318 class318_2, int i_3) {
      class242 class242_4 = new class242();
      class242_4.field3149 = 0;
      class242_4.field2137 = (long)i_0;
      class242_4.field3145 = bytes_1;
      class242_4.field3146 = class318_2;
      class272 class272_5 = class245.field3177;
      synchronized(class245.field3177) {
         class245.field3177.method4888(class242_4);
      }

      Object object_10 = class245.field3180;
      synchronized(class245.field3180) {
         if (class245.field3178 == 0) {
            class97.field1247 = new Thread(new class245());
            class97.field1247.setDaemon(true);
            class97.field1247.start();
            class97.field1247.setPriority(5);
         }

         class245.field3178 = 600;
      }
   }

   static final void method866(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, byte b_8) {
      if (class41.method603(i_0, -1487226954)) {
         class200.field2390 = null;
         class302.method5317(class9.field44[i_0], -1, i_1, i_2, i_3, i_4, i_5, i_6, i_7, (byte) 105);
         if (class200.field2390 != null) {
            class302.method5317(class200.field2390, -1412584499, i_1, i_2, i_3, i_4, client.field923, class144.field1893, i_7, (byte) 52);
            class200.field2390 = null;
         }

      } else {
         if (i_7 != -1) {
            client.field901[i_7] = true;
         } else {
            for (int i_9 = 0; i_9 < 100; i_9++) {
               client.field901[i_9] = true;
            }
         }

      }
   }

}
