import java.awt.Desktop;
import java.awt.Desktop.Action;
import java.net.URI;

public class class93 implements class203 {

   static class241 field1174;
   static final class93 field1176 = new class93(0);
   static final class93 field1175 = new class93(1);
   static final class93 field1179 = new class93(2);
   static final class93 field1177 = new class93(3);
   final int field1178;

   class93(int i_1) {
      this.field1178 = i_1;
   }

   public int vmethod6086(int i_1) {
      return this.field1178;
   }

   static void method2108(class71 class71_0, int i_1, int i_2) {
      Object[] arr_3 = class71_0.field572;
      class101 class101_4;
      int i_18;
      if (class246.method4296(class71_0.field580, -1996112671)) {
         class101.field1302 = (class50) arr_3[0];
         class252 class252_5 = class163.method3458(class101.field1302.field374, -1966052617);
         class101_4 = class96.method2158(class71_0.field580, class252_5.field3248, class252_5.field3252, (short) -4905);
      } else {
         i_18 = ((Integer) arr_3[0]).intValue();
         class101_4 = class103.method2290(i_18, 1674386327);
      }

      if (class101_4 != null) {
         class253.field3267 = 0;
         class85.field1105 = 0;
         i_18 = -1;
         int[] ints_6 = class101_4.field1300;
         int[] ints_7 = class101_4.field1294;
         byte b_8 = -1;
         class85.field1098 = 0;
         class85.field1104 = false;
         boolean bool_28 = false;

         label794: {
            label795: {
               try {
                  int i_11;
                  try {
                     bool_28 = true;
                     class70.field565 = new int[class101_4.field1296];
                     int i_9 = 0;
                     class293.field3665 = new String[class101_4.field1297];
                     int i_10 = 0;

                     int i_12;
                     String string_19;
                     for (i_11 = 1; i_11 < arr_3.length; i_11++) {
                        if (arr_3[i_11] instanceof Integer) {
                           i_12 = ((Integer) arr_3[i_11]).intValue();
                           if (i_12 == -2147483647) {
                              i_12 = class71_0.field570;
                           }

                           if (i_12 == -2147483646) {
                              i_12 = class71_0.field579;
                           }

                           if (i_12 == -2147483645) {
                              i_12 = class71_0.field571 != null ? class71_0.field571.field2586 : -1;
                           }

                           if (i_12 == -2147483644) {
                              i_12 = class71_0.field574;
                           }

                           if (i_12 == -2147483643) {
                              i_12 = class71_0.field571 != null ? class71_0.field571.field2587 : -1;
                           }

                           if (i_12 == -2147483642) {
                              i_12 = class71_0.field575 != null ? class71_0.field575.field2586 : -1;
                           }

                           if (i_12 == -2147483641) {
                              i_12 = class71_0.field575 != null ? class71_0.field575.field2587 : -1;
                           }

                           if (i_12 == -2147483640) {
                              i_12 = class71_0.field576;
                           }

                           if (i_12 == -2147483639) {
                              i_12 = class71_0.field577;
                           }

                           class70.field565[i_9++] = i_12;
                        } else if (arr_3[i_11] instanceof String) {
                           string_19 = (String) arr_3[i_11];
                           if (string_19.equals("event_opbase")) {
                              string_19 = class71_0.field578;
                           }

                           class293.field3665[i_10++] = string_19;
                        }
                     }

                     i_11 = 0;
                     class85.field1097 = class71_0.field569;

                     label778:
                     while (true) {
                        ++i_11;
                        if (i_11 > i_1) {
                           throw new RuntimeException();
                        }

                        ++i_18;
                        int i_31 = ints_6[i_18];
                        int i_21;
                        if (i_31 >= 100) {
                           boolean bool_34;
                           if (class101_4.field1294[i_18] == 1) {
                              bool_34 = true;
                           } else {
                              bool_34 = false;
                           }

                           i_21 = class146.method3317(i_31, class101_4, bool_34, 1343156262);
                           switch(i_21) {
                           case 0:
                              bool_28 = false;
                              break label778;
                           case 1:
                           default:
                              break;
                           case 2:
                              throw new IllegalStateException();
                           }
                        } else if (i_31 == 0) {
                           class85.field1095[++class253.field3267 - 1] = ints_7[i_18];
                        } else if (i_31 == 1) {
                           i_12 = ints_7[i_18];
                           class85.field1095[++class253.field3267 - 1] = class221.field2541[i_12];
                        } else if (i_31 == 2) {
                           i_12 = ints_7[i_18];
                           class221.field2541[i_12] = class85.field1095[--class253.field3267];
                           class101.method2271(i_12, 947039131);
                        } else if (i_31 == 3) {
                           class85.field1096[++class85.field1105 - 1] = class101_4.field1295[i_18];
                        } else if (i_31 == 6) {
                           i_18 += ints_7[i_18];
                        } else if (i_31 == 7) {
                           class253.field3267 -= 2;
                           if (class85.field1095[class253.field3267] != class85.field1095[class253.field3267 + 1]) {
                              i_18 += ints_7[i_18];
                           }
                        } else if (i_31 == 8) {
                           class253.field3267 -= 2;
                           if (class85.field1095[class253.field3267] == class85.field1095[class253.field3267 + 1]) {
                              i_18 += ints_7[i_18];
                           }
                        } else if (i_31 == 9) {
                           class253.field3267 -= 2;
                           if (class85.field1095[class253.field3267] < class85.field1095[class253.field3267 + 1]) {
                              i_18 += ints_7[i_18];
                           }
                        } else if (i_31 == 10) {
                           class253.field3267 -= 2;
                           if (class85.field1095[class253.field3267] > class85.field1095[class253.field3267 + 1]) {
                              i_18 += ints_7[i_18];
                           }
                        } else if (i_31 == 21) {
                           if (class85.field1098 == 0) {
                              bool_28 = false;
                              break label795;
                           }

                           class65 class65_36 = class85.field1099[--class85.field1098];
                           class101_4 = class65_36.field522;
                           ints_6 = class101_4.field1300;
                           ints_7 = class101_4.field1294;
                           i_18 = class65_36.field525;
                           class70.field565 = class65_36.field523;
                           class293.field3665 = class65_36.field524;
                        } else if (i_31 == 25) {
                           i_12 = ints_7[i_18];
                           class85.field1095[++class253.field3267 - 1] = class222.method3999(i_12, -676384531);
                        } else if (i_31 == 27) {
                           i_12 = ints_7[i_18];
                           class256.method4458(i_12, class85.field1095[--class253.field3267], -1929348115);
                        } else if (i_31 == 31) {
                           class253.field3267 -= 2;
                           if (class85.field1095[class253.field3267] <= class85.field1095[class253.field3267 + 1]) {
                              i_18 += ints_7[i_18];
                           }
                        } else if (i_31 == 32) {
                           class253.field3267 -= 2;
                           if (class85.field1095[class253.field3267] >= class85.field1095[class253.field3267 + 1]) {
                              i_18 += ints_7[i_18];
                           }
                        } else if (i_31 == 33) {
                           class85.field1095[++class253.field3267 - 1] = class70.field565[ints_7[i_18]];
                        } else if (i_31 == 34) {
                           class70.field565[ints_7[i_18]] = class85.field1095[--class253.field3267];
                        } else if (i_31 == 35) {
                           class85.field1096[++class85.field1105 - 1] = class293.field3665[ints_7[i_18]];
                        } else if (i_31 == 36) {
                           class293.field3665[ints_7[i_18]] = class85.field1096[--class85.field1105];
                        } else if (i_31 == 37) {
                           i_12 = ints_7[i_18];
                           class85.field1105 -= i_12;
                           String string_33 = class90.method2097(class85.field1096, class85.field1105, i_12, -540842250);
                           class85.field1096[++class85.field1105 - 1] = string_33;
                        } else if (i_31 == 38) {
                           --class253.field3267;
                        } else if (i_31 == 39) {
                           --class85.field1105;
                        } else {
                           int i_16;
                           if (i_31 != 40) {
                              if (i_31 == 42) {
                                 class85.field1095[++class253.field3267 - 1] = class99.field1270.method2200(ints_7[i_18], -2112992813);
                              } else if (i_31 == 43) {
                                 class99.field1270.method2202(ints_7[i_18], class85.field1095[--class253.field3267], -298220081);
                              } else if (i_31 == 44) {
                                 i_12 = ints_7[i_18] >> 16;
                                 i_21 = ints_7[i_18] & 0xffff;
                                 int i_22 = class85.field1095[--class253.field3267];
                                 if (i_22 < 0 || i_22 > 5000) {
                                    throw new RuntimeException();
                                 }

                                 class85.field1090[i_12] = i_22;
                                 byte b_23 = -1;
                                 if (i_21 == 105) {
                                    b_23 = 0;
                                 }

                                 for (i_16 = 0; i_16 < i_22; i_16++) {
                                    class85.field1094[i_12][i_16] = b_23;
                                 }
                              } else if (i_31 == 45) {
                                 i_12 = ints_7[i_18];
                                 i_21 = class85.field1095[--class253.field3267];
                                 if (i_21 < 0 || i_21 >= class85.field1090[i_12]) {
                                    throw new RuntimeException();
                                 }

                                 class85.field1095[++class253.field3267 - 1] = class85.field1094[i_12][i_21];
                              } else if (i_31 == 46) {
                                 i_12 = ints_7[i_18];
                                 class253.field3267 -= 2;
                                 i_21 = class85.field1095[class253.field3267];
                                 if (i_21 < 0 || i_21 >= class85.field1090[i_12]) {
                                    throw new RuntimeException();
                                 }

                                 class85.field1094[i_12][i_21] = class85.field1095[class253.field3267 + 1];
                              } else if (i_31 == 47) {
                                 string_19 = class99.field1270.method2204(ints_7[i_18], 343264324);
                                 if (string_19 == null) {
                                    string_19 = "null";
                                 }

                                 class85.field1096[++class85.field1105 - 1] = string_19;
                              } else if (i_31 == 48) {
                                 class99.field1270.method2203(ints_7[i_18], class85.field1096[--class85.field1105], (byte) 0);
                              } else if (i_31 == 49) {
                                 string_19 = class99.field1270.method2205(ints_7[i_18], (byte) -99);
                                 class85.field1096[++class85.field1105 - 1] = string_19;
                              } else if (i_31 == 50) {
                                 class99.field1270.method2201(ints_7[i_18], class85.field1096[--class85.field1105], -387665729);
                              } else {
                                 if (i_31 != 60) {
                                    throw new IllegalStateException();
                                 }

                                 class326 class326_35 = class101_4.field1305[ints_7[i_18]];
                                 class188 class188_32 = (class188) class326_35.method5919((long)class85.field1095[--class253.field3267]);
                                 if (class188_32 != null) {
                                    i_18 += class188_32.field2135;
                                 }
                              }
                           } else {
                              i_12 = ints_7[i_18];
                              class101 class101_13 = class103.method2290(i_12, 1402468896);
                              int[] ints_14 = new int[class101_13.field1296];
                              String[] arr_15 = new String[class101_13.field1297];

                              for (i_16 = 0; i_16 < class101_13.field1298; i_16++) {
                                 ints_14[i_16] = class85.field1095[i_16 + (class253.field3267 - class101_13.field1298)];
                              }

                              for (i_16 = 0; i_16 < class101_13.field1292; i_16++) {
                                 arr_15[i_16] = class85.field1096[i_16 + (class85.field1105 - class101_13.field1292)];
                              }

                              class253.field3267 -= class101_13.field1298;
                              class85.field1105 -= class101_13.field1292;
                              class65 class65_20 = new class65();
                              class65_20.field522 = class101_4;
                              class65_20.field525 = i_18;
                              class65_20.field523 = class70.field565;
                              class65_20.field524 = class293.field3665;
                              class85.field1099[++class85.field1098 - 1] = class65_20;
                              class101_4 = class101_13;
                              ints_6 = class101_13.field1300;
                              ints_7 = class101_13.field1294;
                              i_18 = -1;
                              class70.field565 = ints_14;
                              class293.field3665 = arr_15;
                           }
                        }
                     }
                  } catch (Exception exception_29) {
                     StringBuilder stringbuilder_25 = new StringBuilder(30);
                     stringbuilder_25.append("").append(class101_4.field2137).append(" ");

                     for (i_11 = class85.field1098 - 1; i_11 >= 0; --i_11) {
                        stringbuilder_25.append("").append(class85.field1099[i_11].field522.field2137).append(" ");
                     }

                     stringbuilder_25.append("").append(b_8);
                     class223.method4011(stringbuilder_25.toString(), exception_29, 1378173743);
                     bool_28 = false;
                     break label794;
                  }
               } finally {
                  if(bool_28) {
                     if (class85.field1104) {
                        class85.field1103 = true;
                        class191.method3635((byte) 36);
                        class85.field1103 = false;
                        class85.field1104 = false;
                     }

                  }
               }

               if (class85.field1104) {
                  class85.field1103 = true;
                  class191.method3635((byte) 37);
                  class85.field1103 = false;
                  class85.field1104 = false;
               }

               return;
            }

            if (class85.field1104) {
               class85.field1103 = true;
               class191.method3635((byte) 51);
               class85.field1103 = false;
               class85.field1104 = false;
            }

            return;
         }

         if (class85.field1104) {
            class85.field1103 = true;
            class191.method3635((byte) 18);
            class85.field1103 = false;
            class85.field1104 = false;
         }

      }
   }

   static void method2110(int i_0) {
      if (class223.field2562.field990 >> 7 == client.field774 && class223.field2562.field938 >> 7 == client.field880) {
         client.field774 = 0;
      }

   }

   public static void method2109(String string_0, boolean bool_1, boolean bool_2, int i_3) {
      if (bool_1) {
         if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Action.BROWSE)) {
            try {
               Desktop.getDesktop().browse(new URI(string_0));
               return;
            } catch (Exception exception_5) {
               ;
            }
         }

         if (class60.field439.startsWith("win")) {
            class71.method1260(string_0, 0, (byte) -1);
         } else if (class60.field439.startsWith("mac")) {
            class13.method131(string_0, 1, "openjs", (byte) -107);
         } else {
            class71.method1260(string_0, 2, (byte) -85);
         }
      } else {
         class71.method1260(string_0, 3, (byte) -119);
      }

   }

   public static String method2107(class310 class310_0, byte b_1) {
      String str_2;
      try {
         int i_3 = class310_0.method5483(772809583);
         if (i_3 > 32767) {
            i_3 = 32767;
         }

         byte[] bytes_4 = new byte[i_3];
         class310_0.field3751 += class219.field2528.method3964(class310_0.field3752, class310_0.field3751, bytes_4, 0, i_3, -556309350);
         String string_5 = class179.method3552(bytes_4, 0, i_3, (byte) 99);
         str_2 = string_5;
      } catch (Exception exception_7) {
         str_2 = "Cabbage";
      }

      return str_2;
   }

}
