import java.util.Iterator;
import java.util.LinkedList;

public class class35 {

   static class295 field244;
   int field237 = -1;
   int field231 = -1;
   int field232 = -1;
   class222 field233 = null;
   int field234 = Integer.MAX_VALUE;
   int field228 = 0;
   int field236 = Integer.MAX_VALUE;
   int field239 = 0;
   boolean field238 = false;
   String field245;
   String field230;
   LinkedList field235;

   public void method397(class310 class310_1, int i_2, int i_3) {
      this.field237 = i_2;
      this.field245 = class310_1.method5589(-2124668922);
      this.field230 = class310_1.method5589(24340297);
      this.field233 = new class222(class310_1.method5507(1858705182));
      this.field231 = class310_1.method5507(750341052);
      class310_1.method5661((byte) -67);
      this.field238 = class310_1.method5661((byte) 83) == 1;
      this.field232 = class310_1.method5661((byte) -35);
      int i_4 = class310_1.method5661((byte) 108);
      this.field235 = new LinkedList();

      for (int i_5 = 0; i_5 < i_4; i_5++) {
         this.field235.add(this.method386(class310_1, 1162272573));
      }

      this.method391(-777365291);
   }

   public boolean method388(int i_1, int i_2, int i_3) {
      int i_4 = i_1 / 64;
      int i_5 = i_2 / 64;
      if (i_4 >= this.field234 && i_4 <= this.field228) {
         if (i_5 >= this.field236 && i_5 <= this.field239) {
            Iterator iterator_6 = this.field235.iterator();

            class46 class46_7;
            do {
               if (!iterator_6.hasNext()) {
                  return false;
               }

               class46_7 = (class46) iterator_6.next();
            } while (!class46_7.vmethod814(i_1, i_2, 639074408));

            return true;
         } else {
            return false;
         }
      } else {
         return false;
      }
   }

   public int[] method389(int i_1, int i_2, int i_3, int i_4) {
      Iterator iterator_5 = this.field235.iterator();

      class46 class46_6;
      do {
         if (!iterator_5.hasNext()) {
            return null;
         }

         class46_6 = (class46) iterator_5.next();
      } while (!class46_6.vmethod813(i_1, i_2, i_3, 1743210337));

      return class46_6.vmethod819(i_1, i_2, i_3, 1868308301);
   }

   public int method392(int i_1) {
      return this.field237;
   }

   public int method398(byte b_1) {
      return this.field234;
   }

   public int method400(int i_1) {
      return this.field236;
   }

   public boolean method387(int i_1, int i_2, int i_3, int i_4) {
      Iterator iterator_5 = this.field235.iterator();

      class46 class46_6;
      do {
         if (!iterator_5.hasNext()) {
            return false;
         }

         class46_6 = (class46) iterator_5.next();
      } while (!class46_6.vmethod813(i_1, i_2, i_3, 241939554));

      return true;
   }

   public class222 method384(int i_1, int i_2, byte b_3) {
      Iterator iterator_4 = this.field235.iterator();

      class46 class46_5;
      do {
         if (!iterator_4.hasNext()) {
            return null;
         }

         class46_5 = (class46) iterator_4.next();
      } while (!class46_5.vmethod814(i_1, i_2, 639074408));

      return class46_5.vmethod835(i_1, i_2, (byte) 0);
   }

   public int method403(int i_1) {
      return this.field233.field2549;
   }

   public String method394(int i_1) {
      return this.field245;
   }

   public int method402(int i_1) {
      return this.field233.field2546;
   }

   public int method404(int i_1) {
      return this.field233.field2547;
   }

   public int method457(byte b_1) {
      return this.field232;
   }

   void method391(int i_1) {
      Iterator iterator_2 = this.field235.iterator();

      while (iterator_2.hasNext()) {
         class46 class46_3 = (class46) iterator_2.next();
         class46_3.vmethod812(this, (short) 19050);
      }

   }

   class46 method386(class310 class310_1, int i_2) {
      int i_3 = class310_1.method5661((byte) 9);
      class29 class29_4 = (class29) class152.method3363(class29.method295((byte) 116), i_3, -1230720169);
      Object obj_5 = null;
      switch(class29_4.field170) {
      case 0:
         obj_5 = new class42();
         break;
      case 1:
         obj_5 = new class33();
         break;
      case 2:
         obj_5 = new class51();
         break;
      case 3:
         obj_5 = new class28();
         break;
      default:
         throw new IllegalStateException("");
      }

      ((class46) obj_5).vmethod825(class310_1, (byte) 125);
      return (class46) obj_5;
   }

   public int method425(int i_1) {
      return this.field228;
   }

   public boolean method393(int i_1) {
      return this.field238;
   }

   public int method446(int i_1) {
      return this.field239;
   }

   int method450(int i_1) {
      return this.field231;
   }

   public String method395(int i_1) {
      return this.field230;
   }

   public class222 method455(int i_1) {
      return new class222(this.field233);
   }

}
