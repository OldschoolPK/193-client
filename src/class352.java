import java.io.EOFException;
import java.io.IOException;

public class class352 {

   long field4089 = -1L;
   long field4092 = -1L;
   int field4096 = 0;
   class353 field4087;
   long field4093;
   long field4097;
   byte[] field4086;
   byte[] field4091;
   long field4095;
   long field4088;
   int field4090;

   public class352(class353 class353_1, int i_2, int i_3) throws IOException {
      this.field4087 = class353_1;
      this.field4097 = this.field4093 = class353_1.method6568(-200232161);
      this.field4086 = new byte[i_2];
      this.field4091 = new byte[i_3];
      this.field4095 = 0L;
   }

   public void method6539(byte[] bytes_1, int i_2, int i_3, int i_4) throws IOException {
      try {
         if (i_3 + i_2 > bytes_1.length) {
            throw new ArrayIndexOutOfBoundsException(i_3 + i_2 - bytes_1.length);
         }

         if (this.field4092 != -1L && this.field4095 >= this.field4092 && (long)i_3 + this.field4095 <= (long)this.field4096 + this.field4092) {
            System.arraycopy(this.field4091, (int)(this.field4095 - this.field4092), bytes_1, i_2, i_3);
            this.field4095 += (long)i_3;
            return;
         }

         long long_5 = this.field4095;
         int i_8 = i_3;
         int i_9;
         if (this.field4095 >= this.field4089 && this.field4095 < this.field4089 + (long)this.field4090) {
            i_9 = (int)((long)this.field4090 - (this.field4095 - this.field4089));
            if (i_9 > i_3) {
               i_9 = i_3;
            }

            System.arraycopy(this.field4086, (int)(this.field4095 - this.field4089), bytes_1, i_2, i_9);
            this.field4095 += (long)i_9;
            i_2 += i_9;
            i_3 -= i_9;
         }

         if (i_3 > this.field4086.length) {
            this.field4087.method6564(this.field4095);

            for (this.field4088 = this.field4095; i_3 > 0; i_3 -= i_9) {
               i_9 = this.field4087.method6567(bytes_1, i_2, i_3, 1925359098);
               if (i_9 == -1) {
                  break;
               }

               this.field4088 += (long)i_9;
               this.field4095 += (long)i_9;
               i_2 += i_9;
            }
         } else if (i_3 > 0) {
            this.method6540(1871269663);
            i_9 = i_3;
            if (i_3 > this.field4090) {
               i_9 = this.field4090;
            }

            System.arraycopy(this.field4086, 0, bytes_1, i_2, i_9);
            i_2 += i_9;
            i_3 -= i_9;
            this.field4095 += (long)i_9;
         }

         if (this.field4092 != -1L) {
            if (this.field4092 > this.field4095 && i_3 > 0) {
               i_9 = i_2 + (int)(this.field4092 - this.field4095);
               if (i_9 > i_3 + i_2) {
                  i_9 = i_3 + i_2;
               }

               while (i_2 < i_9) {
                  bytes_1[i_2++] = 0;
                  --i_3;
                  ++this.field4095;
               }
            }

            long long_14 = -1L;
            long long_11 = -1L;
            if (this.field4092 >= long_5 && this.field4092 < long_5 + (long)i_8) {
               long_14 = this.field4092;
            } else if (long_5 >= this.field4092 && long_5 < this.field4092 + (long)this.field4096) {
               long_14 = long_5;
            }

            if (this.field4092 + (long)this.field4096 > long_5 && this.field4092 + (long)this.field4096 <= long_5 + (long)i_8) {
               long_11 = (long)this.field4096 + this.field4092;
            } else if (long_5 + (long)i_8 > this.field4092 && (long)i_8 + long_5 <= (long)this.field4096 + this.field4092) {
               long_11 = (long)i_8 + long_5;
            }

            if (long_14 > -1L && long_11 > long_14) {
               int i_13 = (int)(long_11 - long_14);
               System.arraycopy(this.field4091, (int)(long_14 - this.field4092), bytes_1, (int)(long_14 - long_5) + i_2, i_13);
               if (long_11 > this.field4095) {
                  i_3 = (int)((long)i_3 - (long_11 - this.field4095));
                  this.field4095 = long_11;
               }
            }
         }
      } catch (IOException ioexception_17) {
         this.field4088 = -1L;
         throw ioexception_17;
      }

      if (i_3 > 0) {
         throw new EOFException();
      }
   }

   void method6542(int i_1) throws IOException {
      if (this.field4092 != -1L) {
         if (this.field4092 != this.field4088) {
            this.field4087.method6564(this.field4092);
            this.field4088 = this.field4092;
         }

         this.field4087.method6565(this.field4091, 0, this.field4096, 376046119);
         this.field4088 += 958797773L * (long)(this.field4096 * 1893510405);
         if (this.field4088 > this.field4093) {
            this.field4093 = this.field4088;
         }

         long long_2 = -1L;
         long long_4 = -1L;
         if (this.field4092 >= this.field4089 && this.field4092 < (long)this.field4090 + this.field4089) {
            long_2 = this.field4092;
         } else if (this.field4089 >= this.field4092 && this.field4089 < (long)this.field4096 + this.field4092) {
            long_2 = this.field4089;
         }

         if ((long)this.field4096 + this.field4092 > this.field4089 && this.field4092 + (long)this.field4096 <= (long)this.field4090 + this.field4089) {
            long_4 = this.field4092 + (long)this.field4096;
         } else if (this.field4089 + (long)this.field4090 > this.field4092 && this.field4089 + (long)this.field4090 <= (long)this.field4096 + this.field4092) {
            long_4 = this.field4089 + (long)this.field4090;
         }

         if (long_2 > -1L && long_4 > long_2) {
            int i_6 = (int)(long_4 - long_2);
            System.arraycopy(this.field4091, (int)(long_2 - this.field4092), this.field4086, (int)(long_2 - this.field4089), i_6);
         }

         this.field4092 = -1L;
         this.field4096 = 0;
      }

   }

   public long method6537(int i_1) {
      return this.field4097;
   }

   public void method6561(long long_1) throws IOException {
      if (long_1 < 0L) {
         throw new IOException("");
      } else {
         this.field4095 = long_1;
      }
   }

   public void method6538(byte[] bytes_1, byte b_2) throws IOException {
      this.method6539(bytes_1, 0, bytes_1.length, 688300082);
   }

   public void method6541(byte[] bytes_1, int i_2, int i_3, byte b_4) throws IOException {
      try {
         if ((long)i_3 + this.field4095 > this.field4097) {
            this.field4097 = this.field4095 + (long)i_3;
         }

         if (this.field4092 != -1L && (this.field4095 < this.field4092 || this.field4095 > this.field4092 + (long)this.field4096)) {
            this.method6542(907719401);
         }

         if (this.field4092 != -1L && (long)i_3 + this.field4095 > this.field4092 + (long)this.field4091.length) {
            int i_5 = (int)((long)this.field4091.length - (this.field4095 - this.field4092));
            System.arraycopy(bytes_1, i_2, this.field4091, (int)(this.field4095 - this.field4092), i_5);
            this.field4095 += (long)i_5;
            i_2 += i_5;
            i_3 -= i_5;
            this.field4096 = this.field4091.length;
            this.method6542(1686189651);
         }

         if (i_3 <= this.field4091.length) {
            if (i_3 > 0) {
               if (this.field4092 == -1L) {
                  this.field4092 = this.field4095;
               }

               System.arraycopy(bytes_1, i_2, this.field4091, (int)(this.field4095 - this.field4092), i_3);
               this.field4095 += (long)i_3;
               if (this.field4095 - this.field4092 > (long)this.field4096) {
                  this.field4096 = (int)(this.field4095 - this.field4092);
               }

            }
         } else {
            if (this.field4095 != this.field4088) {
               this.field4087.method6564(this.field4095);
               this.field4088 = this.field4095;
            }

            this.field4087.method6565(bytes_1, i_2, i_3, 1349397429);
            this.field4088 += (long)i_3;
            if (this.field4088 > this.field4093) {
               this.field4093 = this.field4088;
            }

            long long_10 = -1L;
            long long_7 = -1L;
            if (this.field4095 >= this.field4089 && this.field4095 < this.field4089 + (long)this.field4090) {
               long_10 = this.field4095;
            } else if (this.field4089 >= this.field4095 && this.field4089 < this.field4095 + (long)i_3) {
               long_10 = this.field4089;
            }

            if (this.field4095 + (long)i_3 > this.field4089 && (long)i_3 + this.field4095 <= this.field4089 + (long)this.field4090) {
               long_7 = this.field4095 + (long)i_3;
            } else if ((long)this.field4090 + this.field4089 > this.field4095 && (long)this.field4090 + this.field4089 <= this.field4095 + (long)i_3) {
               long_7 = (long)this.field4090 + this.field4089;
            }

            if (long_10 > -1L && long_7 > long_10) {
               int i_9 = (int)(long_7 - long_10);
               System.arraycopy(bytes_1, (int)(long_10 + (long)i_2 - this.field4095), this.field4086, (int)(long_10 - this.field4089), i_9);
            }

            this.field4095 += (long)i_3;
         }
      } catch (IOException ioexception_13) {
         this.field4088 = -1L;
         throw ioexception_13;
      }
   }

   void method6540(int i_1) throws IOException {
      this.field4090 = 0;
      if (this.field4088 != this.field4095) {
         this.field4087.method6564(this.field4095);
         this.field4088 = this.field4095;
      }

      int i_3;
      for (this.field4089 = this.field4095; this.field4090 < this.field4086.length; this.field4090 += i_3) {
         int i_2 = this.field4086.length - this.field4090;
         if (i_2 > 200000000) {
            i_2 = 200000000;
         }

         i_3 = this.field4087.method6567(this.field4086, this.field4090, i_2, 543995116);
         if (i_3 == -1) {
            break;
         }

         this.field4088 += (long)i_3;
      }

   }

   public void method6552(int i_1) throws IOException {
      this.method6542(-1956599640);
      this.field4087.method6569((byte) 0);
   }

}
