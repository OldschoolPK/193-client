public class class120 {

   int field1468 = class111.method2394(16);
   int field1466 = class111.method2394(24);
   int field1467 = class111.method2394(24);
   int field1470 = class111.method2394(24) + 1;
   int field1469 = class111.method2394(6) + 1;
   int field1465 = class111.method2394(8);
   int[] field1471;

   class120() {
      int[] ints_1 = new int[this.field1469];

      int i_2;
      for (i_2 = 0; i_2 < this.field1469; i_2++) {
         int i_3 = 0;
         int i_4 = class111.method2394(3);
         boolean bool_5 = class111.method2393() != 0;
         if (bool_5) {
            i_3 = class111.method2394(5);
         }

         ints_1[i_2] = i_3 << 3 | i_4;
      }

      this.field1471 = new int[this.field1469 * 8];

      for (i_2 = 0; i_2 < this.field1469 * 8; i_2++) {
         this.field1471[i_2] = (ints_1[i_2 >> 3] & 1 << (i_2 & 0x7)) != 0 ? class111.method2394(8) : -1;
      }

   }

   void method2558(float[] floats_1, int i_2, boolean bool_3) {
      int i_4;
      for (i_4 = 0; i_4 < i_2; i_4++) {
         floats_1[i_4] = 0.0F;
      }

      if (!bool_3) {
         i_4 = class111.field1373[this.field1465].field1363;
         int i_5 = this.field1467 - this.field1466;
         int i_6 = i_5 / this.field1470;
         int[] ints_7 = new int[i_6];

         for (int i_8 = 0; i_8 < 8; i_8++) {
            int i_9 = 0;

            while (i_9 < i_6) {
               int i_10;
               int i_11;
               if (i_8 == 0) {
                  i_10 = class111.field1373[this.field1465].method2374();

                  for (i_11 = i_4 - 1; i_11 >= 0; --i_11) {
                     if (i_9 + i_11 < i_6) {
                        ints_7[i_9 + i_11] = i_10 % this.field1469;
                     }

                     i_10 /= this.field1469;
                  }
               }

               for (i_10 = 0; i_10 < i_4; i_10++) {
                  i_11 = ints_7[i_9];
                  int i_12 = this.field1471[i_8 + i_11 * 8];
                  if (i_12 >= 0) {
                     int i_13 = i_9 * this.field1470 + this.field1466;
                     class109 class109_14 = class111.field1373[i_12];
                     int i_15;
                     if (this.field1468 == 0) {
                        i_15 = this.field1470 / class109_14.field1363;

                        for (int i_16 = 0; i_16 < i_15; i_16++) {
                           float[] floats_17 = class109_14.method2372();

                           for (int i_18 = 0; i_18 < class109_14.field1363; i_18++) {
                              floats_1[i_13 + i_16 + i_18 * i_15] += floats_17[i_18];
                           }
                        }
                     } else {
                        i_15 = 0;

                        while (i_15 < this.field1470) {
                           float[] floats_19 = class109_14.method2372();

                           for (int i_20 = 0; i_20 < class109_14.field1363; i_20++) {
                              floats_1[i_13 + i_15] += floats_19[i_20];
                              ++i_15;
                           }
                        }
                     }
                  }

                  ++i_9;
                  if (i_9 >= i_6) {
                     break;
                  }
               }
            }
         }

      }
   }

}
