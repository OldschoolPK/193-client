import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.Comparator;
import java.util.Iterator;

final class class20 implements Comparator {

   int method206(class16 class16_1, class16 class16_2, int i_3) {
      return class16_1.method156(-819048279).compareTo(class16_2.method156(-819048279));
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method206((class16) object_1, (class16) object_2, 1347597174);
   }

   public static final boolean method202(int i_0) {
      class54 class54_1 = class54.field407;
      synchronized(class54.field407) {
         if (class54.field415 == class54.field417) {
            return false;
         } else {
            class241.field3135 = class54.field412[class54.field415];
            class32.field200 = class54.field409[class54.field415];
            class54.field415 = class54.field415 + 1 & 0x7f;
            return true;
         }
      }
   }

   static void method201(byte b_0) {
      class38.field273.method3408();
   }

   static final void method209(int i_0) {
      if (class32.field207) {
         if (class35.field244 != null) {
            class35.field244.method5082(2040277695);
         }

         class66.method1217(-1703246278);
         class32.field207 = false;
      }

   }

   static void method210(int i_0, int i_1, int i_2, int i_3, int i_4) {
      class68 class68_5 = (class68) class68.field546.method5968((long)i_0);
      if (class68_5 == null) {
         class68_5 = new class68();
         class68.field546.method5956(class68_5, (long)i_0);
      }

      if (class68_5.field540.length <= i_1) {
         int[] ints_6 = new int[i_1 + 1];
         int[] ints_7 = new int[i_1 + 1];

         int i_8;
         for (i_8 = 0; i_8 < class68_5.field540.length; i_8++) {
            ints_6[i_8] = class68_5.field540[i_8];
            ints_7[i_8] = class68_5.field539[i_8];
         }

         for (i_8 = class68_5.field540.length; i_8 < i_1; i_8++) {
            ints_6[i_8] = -1;
            ints_7[i_8] = 0;
         }

         class68_5.field540 = ints_6;
         class68_5.field539 = ints_7;
      }

      class68_5.field540[i_1] = i_2;
      class68_5.field539[i_1] = i_3;
   }

   protected static int method207(byte b_0) {
      int i_1 = 0;
      if (class25.field121 == null || !class25.field121.isValid()) {
         try {
            Iterator iterator_2 = ManagementFactory.getGarbageCollectorMXBeans().iterator();

            while (iterator_2.hasNext()) {
               GarbageCollectorMXBean garbagecollectormxbean_3 = (GarbageCollectorMXBean) iterator_2.next();
               if (garbagecollectormxbean_3.isValid()) {
                  class25.field121 = garbagecollectormxbean_3;
                  class62.field480 = -1L;
                  class62.field479 = -1L;
               }
            }
         } catch (Throwable throwable_12) {
            ;
         }
      }

      if (class25.field121 != null) {
         long long_10 = class298.method5270(255749540);
         long long_4 = class25.field121.getCollectionTime();
         if (class62.field479 != -1L) {
            long long_6 = long_4 - class62.field479;
            long long_8 = long_10 - class62.field480;
            if (long_8 != 0L) {
               i_1 = (int)(100L * long_6 / long_8);
            }
         }

         class62.field479 = long_4;
         class62.field480 = long_10;
      }

      return i_1;
   }

}
