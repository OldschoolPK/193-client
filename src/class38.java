import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

public class class38 {

   static class334[] field263;
   static int field271;
   public static class158 field273 = new class158(37748736, 256);
   int field265;
   int field266;
   LinkedList field268;
   List field276;
   HashMap field272;
   int field269;
   final HashMap field261;
   int field270;
   class24 field267;

   class38(int i_1, int i_2, int i_3, HashMap hashmap_4) {
      this.field265 = i_1;
      this.field266 = i_2;
      this.field268 = new LinkedList();
      this.field276 = new LinkedList();
      this.field272 = new HashMap();
      this.field269 = i_3 | ~0xffffff;
      this.field261 = hashmap_4;
   }

   void method548(int i_1, int i_2, class30 class30_3, int i_4) {
      for (int i_5 = 0; i_5 < class30_3.field189; i_5++) {
         class34[] arr_6 = class30_3.field179[i_5][i_1][i_2];
         if (arr_6 != null && arr_6.length != 0) {
            class34[] arr_7 = arr_6;

            for (int i_8 = 0; i_8 < arr_7.length; i_8++) {
               class34 class34_9 = arr_7[i_8];
               if (class296.method5250(class34_9.field222, (byte) -1)) {
                  class264 class264_10 = class34.method383(class34_9.field227, -1352215239);
                  int i_11 = class264_10.field3423 != 0 ? -3407872 : -3355444;
                  if (class34_9.field222 == class229.field2761.field2767) {
                     this.method503(i_1, i_2, class34_9.field223, i_11, -660950240);
                  }

                  if (class34_9.field222 == class229.field2746.field2767) {
                     this.method503(i_1, i_2, class34_9.field223, -3355444, -1441629885);
                     this.method503(i_1, i_2, class34_9.field223 + 1, i_11, 2079452374);
                  }

                  if (class34_9.field222 == class229.field2747.field2767) {
                     if (class34_9.field223 == 0) {
                        class331.method6052(this.field270 * i_1, this.field270 * (63 - i_2), 1, i_11);
                     }

                     if (class34_9.field223 == 1) {
                        class331.method6052(this.field270 * i_1 + this.field270 - 1, this.field270 * (63 - i_2), 1, i_11);
                     }

                     if (class34_9.field223 == 2) {
                        class331.method6052(this.field270 + this.field270 * i_1 - 1, this.field270 * (63 - i_2) + this.field270 - 1, 1, i_11);
                     }

                     if (class34_9.field223 == 3) {
                        class331.method6052(this.field270 * i_1, this.field270 * (63 - i_2) + this.field270 - 1, 1, i_11);
                     }
                  }

                  if (class34_9.field222 == class229.field2748.field2767) {
                     int i_12 = class34_9.field223 % 2;
                     int i_13;
                     if (i_12 == 0) {
                        for (i_13 = 0; i_13 < this.field270; i_13++) {
                           class331.method6052(i_13 + this.field270 * i_1, (64 - i_2) * this.field270 - 1 - i_13, 1, i_11);
                        }
                     } else {
                        for (i_13 = 0; i_13 < this.field270; i_13++) {
                           class331.method6052(i_13 + this.field270 * i_1, i_13 + this.field270 * (63 - i_2), 1, i_11);
                        }
                     }
                  }
               }
            }
         }
      }

   }

   void method543(int i_1, int i_2, HashSet hashset_3, int i_4, int i_5) {
      float f_6 = (float)i_4 / 64.0F;
      float f_7 = f_6 / 2.0F;
      Iterator iterator_8 = this.field272.entrySet().iterator();

      while (iterator_8.hasNext()) {
         Entry map$entry_9 = (Entry) iterator_8.next();
         class222 class222_10 = (class222) map$entry_9.getKey();
         int i_11 = (int)((float)class222_10.field2546 * f_6 + (float)i_1 - f_7);
         int i_12 = (int)((float)(i_2 + i_4) - f_6 * (float)class222_10.field2547 - f_7);
         class43 class43_13 = (class43) map$entry_9.getValue();
         if (class43_13 != null && class43_13.method634(1161567228)) {
            class43_13.field320 = i_11;
            class43_13.field323 = i_12;
            class252 class252_14 = class163.method3458(class43_13.vmethod630(496659420), -1833325301);
            if (!hashset_3.contains(Integer.valueOf(class252_14.method4361(-2018302724)))) {
               this.method493(class43_13, i_11, i_12, f_6, 66792937);
            }
         }
      }

   }

   void method552(int i_1, int i_2, class30 class30_3, class334[] arr_4, byte b_5) {
      for (int i_6 = 0; i_6 < class30_3.field189; i_6++) {
         class34[] arr_7 = class30_3.field179[i_6][i_1][i_2];
         if (arr_7 != null && arr_7.length != 0) {
            class34[] arr_8 = arr_7;

            for (int i_9 = 0; i_9 < arr_8.length; i_9++) {
               class34 class34_10 = arr_8[i_9];
               int i_12 = class34_10.field222;
               boolean bool_11 = i_12 >= class229.field2764.field2767 && i_12 <= class229.field2755.field2767;
               if (bool_11 || class145.method3314(class34_10.field222, (byte) -57)) {
                  class264 class264_13 = class34.method383(class34_10.field227, -22372934);
                  if (class264_13.field3382 != -1) {
                     if (class264_13.field3382 != 46 && class264_13.field3382 != 52) {
                        arr_4[class264_13.field3382].method6096(this.field270 * i_1, this.field270 * (63 - i_2), this.field270 * 2, this.field270 * 2);
                     } else {
                        arr_4[class264_13.field3382].method6096(this.field270 * i_1, this.field270 * (63 - i_2), this.field270 * 2 + 1, this.field270 * 2 + 1);
                     }
                  }
               }
            }
         }
      }

   }

   void method496(int i_1, int i_2, HashSet hashset_3, int i_4, int i_5) {
      float f_6 = (float)i_4 / 64.0F;
      Iterator iterator_7 = this.field276.iterator();

      while (iterator_7.hasNext()) {
         class43 class43_8 = (class43) iterator_7.next();
         if (class43_8.method634(1161567228)) {
            int i_9 = class43_8.field321.field2546 % 64;
            int i_10 = class43_8.field321.field2547 % 64;
            class43_8.field320 = (int)(f_6 * (float)i_9 + (float)i_1);
            class43_8.field323 = (int)(f_6 * (float)(63 - i_10) + (float)i_2);
            if (!hashset_3.contains(Integer.valueOf(class43_8.vmethod630(-170225365)))) {
               this.method493(class43_8, class43_8.field320, class43_8.field323, f_6, 66792937);
            }
         }
      }

   }

   void method581(int i_1, int i_2, class30 class30_3, class49 class49_4, class36 class36_5, int i_6) {
      int i_7 = class30_3.field185[0][i_1][i_2] - 1;
      int i_8 = class30_3.field186[0][i_1][i_2] - 1;
      if (i_7 == -1 && i_8 == -1) {
         class331.method6004(this.field270 * i_1, this.field270 * (63 - i_2), this.field270, this.field270, this.field269);
      }

      int i_9 = 16711935;
      if (i_8 != -1) {
         i_9 = class18.method186(i_8, this.field269, (byte) 32);
      }

      if (i_8 > -1 && class30_3.field178[0][i_1][i_2] == 0) {
         class331.method6004(this.field270 * i_1, this.field270 * (63 - i_2), this.field270, this.field270, i_9);
      } else {
         int i_10 = this.method488(i_1, i_2, class30_3, class36_5, 372915391);
         if (i_8 == -1) {
            class331.method6004(this.field270 * i_1, this.field270 * (63 - i_2), this.field270, this.field270, i_10);
         } else {
            class49_4.method780(this.field270 * i_1, this.field270 * (63 - i_2), i_10, i_9, this.field270, this.field270, class30_3.field178[0][i_1][i_2], class30_3.field177[0][i_1][i_2], (byte) 15);
         }
      }
   }

   int method497(class335 class335_1, class266 class266_2, int i_3) {
      switch(class266_2.field3492) {
      case 1:
         return -class335_1.field3913 / 2;
      case 2:
         return 0;
      default:
         return -class335_1.field3913;
      }
   }

   void method477(List list_1, int i_2) {
      Iterator iterator_3 = list_1.iterator();

      while (iterator_3.hasNext()) {
         class27 class27_4 = (class27) iterator_3.next();
         if (class163.method3458(class27_4.field149, -1875216585).field3250 && class27_4.field321.field2546 >> 6 == this.field265 && class27_4.field321.field2547 >> 6 == this.field266) {
            class27 class27_5 = new class27(class27_4.field321, class27_4.field321, class27_4.field149, this.method474(class27_4.field149, -951001387));
            this.field276.add(class27_5);
         }
      }

   }

   void method485(int i_1, int i_2, class30 class30_3, class49 class49_4, class334[] arr_5, byte b_6) {
      this.method548(i_1, i_2, class30_3, 160900359);
      this.method552(i_1, i_2, class30_3, arr_5, (byte) 109);
   }

   class37 method500(class252 class252_1, int i_2) {
      if (class252_1.field3247 != null && this.field261 != null && this.field261.get(class26.field131) != null) {
         int i_4 = class252_1.field3249;
         class26[] arr_5 = new class26[] {class26.field134, class26.field130, class26.field131};
         class26[] arr_6 = arr_5;
         int i_7 = 0;

         class26 class26_3;
         while (true) {
            if (i_7 >= arr_6.length) {
               class26_3 = null;
               break;
            }

            class26 class26_8 = arr_6[i_7];
            if (i_4 == class26_8.field133) {
               class26_3 = class26_8;
               break;
            }

            ++i_7;
         }

         if (class26_3 == null) {
            return null;
         } else {
            class305 class305_15 = (class305) this.field261.get(class26_3);
            if (class305_15 == null) {
               return null;
            } else {
               i_7 = class305_15.method5398(class252_1.field3247, 1000000);
               String[] arr_16 = new String[i_7];
               class305_15.method5341(class252_1.field3247, (int[]) null, arr_16);
               int i_9 = arr_16.length * class305_15.field3727 / 2;
               int i_10 = 0;
               String[] arr_11 = arr_16;

               for (int i_12 = 0; i_12 < arr_11.length; i_12++) {
                  String string_13 = arr_11[i_12];
                  int i_14 = class305_15.method5340(string_13);
                  if (i_14 > i_10) {
                     i_10 = i_14;
                  }
               }

               return new class37(class252_1.field3247, i_10, i_9, class26_3);
            }
         }
      } else {
         return null;
      }
   }

   void method491(HashSet hashset_1, int i_2, int i_3, int i_4) {
      Iterator iterator_5 = this.field276.iterator();

      while (iterator_5.hasNext()) {
         class43 class43_6 = (class43) iterator_5.next();
         if (class43_6.method634(1161567228)) {
            class252 class252_7 = class163.method3458(class43_6.vmethod630(441219917), -1999740602);
            if (class252_7 != null && hashset_1.contains(Integer.valueOf(class252_7.method4361(-1401841512)))) {
               this.method483(class252_7, class43_6.field320, class43_6.field323, i_2, i_3, 914630911);
            }
         }
      }

   }

   void method487(int i_1, int i_2, class30 class30_3, class49 class49_4, int i_5) {
      for (int i_6 = 1; i_6 < class30_3.field189; i_6++) {
         int i_7 = class30_3.field186[i_6][i_1][i_2] - 1;
         if (i_7 > -1) {
            int i_8 = class18.method186(i_7, this.field269, (byte) 21);
            if (class30_3.field178[i_6][i_1][i_2] == 0) {
               class331.method6004(this.field270 * i_1, this.field270 * (63 - i_2), this.field270, this.field270, i_8);
            } else {
               class49_4.method780(this.field270 * i_1, this.field270 * (63 - i_2), 0, i_8, this.field270, this.field270, class30_3.field178[i_6][i_1][i_2], class30_3.field177[i_6][i_1][i_2], (byte) 15);
            }
         }
      }

   }

   void method494(class252 class252_1, int i_2, int i_3, byte b_4) {
      class335 class335_5 = class252_1.method4374(false, -510835989);
      if (class335_5 != null) {
         int i_6 = this.method497(class335_5, class252_1.field3259, -1268952607);
         int i_7 = this.method498(class335_5, class252_1.field3260, (short) -11626);
         class335_5.method6121(i_6 + i_2, i_3 + i_7);
      }

   }

   int method498(class335 class335_1, class249 class249_2, short s_3) {
      switch(class249_2.field3227) {
      case 1:
         return 0;
      case 2:
         return -class335_1.field3911 / 2;
      default:
         return -class335_1.field3911;
      }
   }

   void method580(class43 class43_1, class252 class252_2, int i_3, int i_4, float f_5, byte b_6) {
      class37 class37_7 = class43_1.vmethod633(111822312);
      if (class37_7 != null) {
         if (class37_7.field254.method254(f_5, (byte) -7)) {
            class305 class305_8 = (class305) this.field261.get(class37_7.field254);
            class305_8.method5349(class37_7.field257, i_3 - class37_7.field255 / 2, i_4, class37_7.field255, class37_7.field256, ~0xffffff | class252_2.field3251, 0, 1, 0, class305_8.field3727 / 2);
         }
      }
   }

   boolean method586(class244 class244_1, int i_2) {
      this.field272.clear();
      if (this.field267 != null) {
         this.field267.method318(class244_1, -1133283231);
         if (this.field267.method307(-1422551454)) {
            this.method568(0, 0, 64, 64, this.field267, -1057419535);
            return true;
         } else {
            return false;
         }
      } else {
         boolean bool_3 = true;

         Iterator iterator_4;
         class48 class48_5;
         for (iterator_4 = this.field268.iterator(); iterator_4.hasNext(); bool_3 &= class48_5.method307(-1660445382)) {
            class48_5 = (class48) iterator_4.next();
            class48_5.method318(class244_1, -1762326043);
         }

         if (bool_3) {
            iterator_4 = this.field268.iterator();

            while (iterator_4.hasNext()) {
               class48_5 = (class48) iterator_4.next();
               this.method568(class48_5.method741((byte) 3) * 8, class48_5.method742(1372566315) * 8, 8, 8, class48_5, -373614796);
            }
         }

         return bool_3;
      }
   }

   void method568(int i_1, int i_2, int i_3, int i_4, class30 class30_5, int i_6) {
      for (int i_7 = i_1; i_7 < i_3 + i_1; i_7++) {
         label75:
         for (int i_8 = i_2; i_8 < i_2 + i_4; i_8++) {
            for (int i_9 = 0; i_9 < class30_5.field189; i_9++) {
               class34[] arr_10 = class30_5.field179[i_9][i_7][i_8];
               if (arr_10 != null && arr_10.length != 0) {
                  class34[] arr_11 = arr_10;

                  for (int i_12 = 0; i_12 < arr_11.length; i_12++) {
                     class264 class264_14;
                     boolean bool_15;
                     label66: {
                        class34 class34_13 = arr_11[i_12];
                        class264_14 = class34.method383(class34_13.field227, -1751895721);
                        if (class264_14.field3421 != null) {
                           int[] ints_16 = class264_14.field3421;

                           for (int i_17 = 0; i_17 < ints_16.length; i_17++) {
                              int i_18 = ints_16[i_17];
                              class264 class264_19 = class34.method383(i_18, -1886639159);
                              if (class264_19.field3414 != -1) {
                                 bool_15 = true;
                                 break label66;
                              }
                           }
                        } else if (class264_14.field3414 != -1) {
                           bool_15 = true;
                           break label66;
                        }

                        bool_15 = false;
                     }

                     if (bool_15) {
                        this.method475(class264_14, i_9, i_7, i_8, class30_5, 1801137136);
                        continue label75;
                     }
                  }
               }
            }
         }
      }

   }

   int method488(int i_1, int i_2, class30 class30_3, class36 class36_4, int i_5) {
      return class30_3.field185[0][i_1][i_2] == 0 ? this.field269 : class36_4.method462(i_1, i_2, -323058581);
   }

   List method502(int i_1) {
      LinkedList linkedlist_2 = new LinkedList();
      linkedlist_2.addAll(this.field276);
      linkedlist_2.addAll(this.field272.values());
      return linkedlist_2;
   }

   class37 method474(int i_1, int i_2) {
      class252 class252_3 = class163.method3458(i_1, -1976952613);
      return this.method500(class252_3, -1657578860);
   }

   void method475(class264 class264_1, int i_2, int i_3, int i_4, class30 class30_5, int i_6) {
      class222 class222_7 = new class222(i_2, i_3 + this.field265 * 64, this.field266 * 64 + i_4);
      class222 class222_8 = null;
      if (this.field267 != null) {
         class222_8 = new class222(this.field267.field181 + i_2, i_3 + this.field267.field187 * 64, i_4 + this.field267.field182 * 64);
      } else {
         class48 class48_9 = (class48) class30_5;
         class222_8 = new class222(i_2 + class48_9.field181, i_3 + class48_9.field187 * 64 + class48_9.method739(-915979405) * 8, class48_9.field182 * 64 + i_4 + class48_9.method740(-1034896406) * 8);
      }

      class252 class252_10;
      Object obj_11;
      if (class264_1.field3421 != null) {
         obj_11 = new class32(class222_8, class222_7, class264_1.field3387, this);
      } else {
         class252_10 = class163.method3458(class264_1.field3414, -1952441450);
         obj_11 = new class27(class222_8, class222_7, class252_10.field3248, this.method500(class252_10, -1657578860));
      }

      class252_10 = class163.method3458(((class43) obj_11).vmethod630(280385965), -1849989189);
      if (class252_10.field3250) {
         this.field272.put(new class222(0, i_3, i_4), obj_11);
      }

   }

   void method483(class252 class252_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      class335 class335_7 = class252_1.method4374(false, -1389474758);
      if (class335_7 != null) {
         class335_7.method6121(i_2 - class335_7.field3913 / 2, i_3 - class335_7.field3911 / 2);
         if (i_4 % i_5 < i_5 / 2) {
            class331.method6044(i_2, i_3, 15, 16776960, 128);
            class331.method6044(i_2, i_3, 7, 16777215, 256);
         }

      }
   }

   void method476(int i_1) {
      Iterator iterator_2 = this.field272.values().iterator();

      while (iterator_2.hasNext()) {
         class43 class43_3 = (class43) iterator_2.next();
         if (class43_3 instanceof class32) {
            ((class32) class43_3).method349(1102738521);
         }
      }

   }

   void method493(class43 class43_1, int i_2, int i_3, float f_4, int i_5) {
      class252 class252_6 = class163.method3458(class43_1.vmethod630(-397502114), -1971630454);
      this.method494(class252_6, i_2, i_3, (byte) 0);
      this.method580(class43_1, class252_6, i_2, i_3, f_4, (byte) 19);
   }

   void method503(int i_1, int i_2, int i_3, int i_4, int i_5) {
      i_3 %= 4;
      if (i_3 == 0) {
         class331.method6023(this.field270 * i_1, this.field270 * (63 - i_2), this.field270, i_4);
      }

      if (i_3 == 1) {
         class331.method6052(this.field270 * i_1, this.field270 * (63 - i_2), this.field270, i_4);
      }

      if (i_3 == 2) {
         class331.method6023(this.field270 + this.field270 * i_1 - 1, this.field270 * (63 - i_2), this.field270, i_4);
      }

      if (i_3 == 3) {
         class331.method6052(this.field270 * i_1, this.field270 * (63 - i_2) + this.field270 - 1, this.field270, i_4);
      }

   }

   void method486(HashSet hashset_1, int i_2, int i_3, int i_4) {
      Iterator iterator_5 = this.field272.values().iterator();

      while (iterator_5.hasNext()) {
         class43 class43_6 = (class43) iterator_5.next();
         if (class43_6.method634(1161567228)) {
            int i_7 = class43_6.vmethod630(403076847);
            if (hashset_1.contains(Integer.valueOf(i_7))) {
               class252 class252_8 = class163.method3458(i_7, -2076152840);
               this.method483(class252_8, class43_6.field320, class43_6.field323, i_2, i_3, 1953515805);
            }
         }
      }

      this.method491(hashset_1, i_2, i_3, -1282937551);
   }

   void method484(class49 class49_1, class334[] arr_2, class36 class36_3, int i_4) {
      Iterator iterator_5 = this.field268.iterator();

      class48 class48_6;
      int i_7;
      int i_8;
      while (iterator_5.hasNext()) {
         class48_6 = (class48) iterator_5.next();

         for (i_7 = class48_6.method741((byte) 3) * 8; i_7 < class48_6.method741((byte) 3) * 8 + 8; i_7++) {
            for (i_8 = class48_6.method742(848726931) * 8; i_8 < class48_6.method742(1914398721) * 8 + 8; i_8++) {
               this.method581(i_7, i_8, class48_6, class49_1, class36_3, -1713222092);
               this.method487(i_7, i_8, class48_6, class49_1, 1417048087);
            }
         }
      }

      iterator_5 = this.field268.iterator();

      while (iterator_5.hasNext()) {
         class48_6 = (class48) iterator_5.next();

         for (i_7 = class48_6.method741((byte) 3) * 8; i_7 < class48_6.method741((byte) 3) * 8 + 8; i_7++) {
            for (i_8 = class48_6.method742(1812224951) * 8; i_8 < class48_6.method742(1481667830) * 8 + 8; i_8++) {
               this.method485(i_7, i_8, class48_6, class49_1, arr_2, (byte) -18);
            }
         }
      }

   }

   void method471(class49 class49_1, class334[] arr_2, class36 class36_3, int i_4) {
      int i_5;
      int i_6;
      for (i_5 = 0; i_5 < 64; i_5++) {
         for (i_6 = 0; i_6 < 64; i_6++) {
            this.method581(i_5, i_6, this.field267, class49_1, class36_3, 143170882);
            this.method487(i_5, i_6, this.field267, class49_1, 1236225695);
         }
      }

      for (i_5 = 0; i_5 < 64; i_5++) {
         for (i_6 = 0; i_6 < 64; i_6++) {
            this.method485(i_5, i_6, this.field267, class49_1, arr_2, (byte) -14);
         }
      }

   }

   void method481(int i_1, int i_2, int i_3, HashSet hashset_4, int i_5) {
      if (hashset_4 == null) {
         hashset_4 = new HashSet();
      }

      this.method543(i_1, i_2, hashset_4, i_3, 1579796963);
      this.method496(i_1, i_2, hashset_4, i_3, -907563765);
   }

   List method501(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      LinkedList linkedlist_7 = new LinkedList();
      if (i_4 >= i_1 && i_5 >= i_2) {
         if (i_4 < i_3 + i_1 && i_5 < i_3 + i_2) {
            Iterator iterator_8 = this.field272.values().iterator();

            class43 class43_9;
            while (iterator_8.hasNext()) {
               class43_9 = (class43) iterator_8.next();
               if (class43_9.method634(1161567228) && class43_9.method642(i_4, i_5, 8588195)) {
                  linkedlist_7.add(class43_9);
               }
            }

            iterator_8 = this.field276.iterator();

            while (iterator_8.hasNext()) {
               class43_9 = (class43) iterator_8.next();
               if (class43_9.method634(1161567228) && class43_9.method642(i_4, i_5, 8588195)) {
                  linkedlist_7.add(class43_9);
               }
            }

            return linkedlist_7;
         } else {
            return linkedlist_7;
         }
      } else {
         return linkedlist_7;
      }
   }

   void method530(short s_1) {
      if (this.field267 != null) {
         this.field267.method304((byte) 8);
      } else {
         Iterator iterator_2 = this.field268.iterator();

         while (iterator_2.hasNext()) {
            class48 class48_3 = (class48) iterator_2.next();
            class48_3.method304((byte) 8);
         }
      }

   }

   void method480(int i_1, class49 class49_2, class334[] arr_3, class244 class244_4, class244 class244_5, byte b_6) {
      this.field270 = i_1;
      if (this.field267 != null || !this.field268.isEmpty()) {
         if (class239.method4137(this.field265, this.field266, i_1, -320192439) == null) {
            boolean bool_7 = true;
            bool_7 &= this.method586(class244_4, -1344110120);
            int i_9;
            if (this.field267 != null) {
               i_9 = this.field267.field183;
            } else {
               i_9 = ((class30) this.field268.getFirst()).field183;
            }

            bool_7 &= class244_5.method4164(i_9, -1344658715);
            if (bool_7) {
               byte[] bytes_8 = class244_5.method4167(i_9, 589315752);
               class36 class36_10 = class102.method2272(bytes_8, (byte) 92);
               class335 class335_11 = new class335(this.field270 * 64, this.field270 * 64);
               class335_11.method6117();
               if (this.field267 != null) {
                  this.method471(class49_2, arr_3, class36_10, -830543594);
               } else {
                  this.method484(class49_2, arr_3, class36_10, 574283591);
               }

               int i_12 = this.field265;
               int i_13 = this.field266;
               int i_14 = this.field270;
               class158 class158_15 = field273;
               long long_17 = (long)(i_14 << 16 | i_12 << 8 | i_13);
               class158_15.method3406(class335_11, long_17, class335_11.field3917.length * 4);
               this.method530((short) 1700);
            }
         }
      }
   }

   void method534(int i_1, int i_2, int i_3, int i_4) {
      class335 class335_5 = class239.method4137(this.field265, this.field266, this.field270, -320192439);
      if (class335_5 != null) {
         if (i_3 == this.field270 * 64) {
            class335_5.method6124(i_1, i_2);
         } else {
            class335_5.method6135(i_1, i_2, i_3, i_3);
         }

      }
   }

   void method472(class24 class24_1, List list_2, byte b_3) {
      this.field272.clear();
      this.field267 = class24_1;
      this.method477(list_2, -1315662131);
   }

   void method506(HashSet hashset_1, List list_2, int i_3) {
      this.field272.clear();
      Iterator iterator_4 = hashset_1.iterator();

      while (iterator_4.hasNext()) {
         class48 class48_5 = (class48) iterator_4.next();
         if (class48_5.method308(1451993074) == this.field265 && class48_5.method309((short) 11679) == this.field266) {
            this.field268.add(class48_5);
         }
      }

      this.method477(list_2, -1315662131);
   }

   public static class353 method587(String string_0, String string_1, boolean bool_2, int i_3) {
      File file_4 = new File(class176.field2062, "preferences" + string_0 + ".dat");
      if (file_4.exists()) {
         try {
            class353 class353_11 = new class353(file_4, "rw", 10000L);
            return class353_11;
         } catch (IOException ioexception_10) {
            ;
         }
      }

      String str_5 = "";
      if (class176.field2068 == 33) {
         str_5 = "_rc";
      } else if (class176.field2068 == 34) {
         str_5 = "_wip";
      }

      File file_6 = new File(class129.field1559, "jagex_" + string_1 + "_preferences" + string_0 + str_5 + ".dat");
      class353 class353_7;
      if (!bool_2 && file_6.exists()) {
         try {
            class353_7 = new class353(file_6, "rw", 10000L);
            return class353_7;
         } catch (IOException ioexception_9) {
            ;
         }
      }

      try {
         class353_7 = new class353(file_4, "rw", 10000L);
         return class353_7;
      } catch (IOException ioexception_8) {
         throw new RuntimeException();
      }
   }

   public static void method588(int i_0) {
      class206.field2426.method3799((short) -3257);
      class206.field2421 = 1;
      class221.field2538 = null;
   }

   public static final void method564(String string_0, String string_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      class40.method597(string_0, string_1, i_2, i_3, i_4, i_5, false, -1609914367);
   }

   static void method589(int i_0, int i_1) {
      for (class188 class188_2 = (class188) client.field891.method5962(); class188_2 != null; class188_2 = (class188) client.field891.method5957()) {
         if ((class188_2.field2137 >> 48 & 0xffffL) == (long)i_0) {
            class188_2.method3628();
         }
      }

   }

   static final void method499(class226 class226_0, int i_1, int i_2, byte b_3) {
      if (class226_0.field2589 == 1) {
         method564(class226_0.field2709, "", 24, 0, 0, class226_0.field2586, -1555696948);
      }

      String string_4;
      if (class226_0.field2589 == 2 && !client.field803) {
         string_4 = class241.method4150(class226_0, (byte) -47);
         if (string_4 != null) {
            method564(string_4, class23.method222(65280, (byte) 92) + class226_0.field2708, 25, 0, -1, class226_0.field2586, -1963358645);
         }
      }

      if (class226_0.field2589 == 3) {
         method564("Close", "", 26, 0, 0, class226_0.field2586, -1914667761);
      }

      if (class226_0.field2589 == 4) {
         method564(class226_0.field2709, "", 28, 0, 0, class226_0.field2586, -1658168486);
      }

      if (class226_0.field2589 == 5) {
         method564(class226_0.field2709, "", 29, 0, 0, class226_0.field2586, -1663269854);
      }

      if (class226_0.field2589 == 6 && client.field822 == null) {
         method564(class226_0.field2709, "", 30, 0, -1, class226_0.field2586, -2004455110);
      }

      int i_5;
      int i_6;
      int i_7;
      int i_8;
      int i_16;
      if (class226_0.field2588 == 2) {
         i_16 = 0;

         for (i_5 = 0; i_5 < class226_0.field2602; i_5++) {
            for (i_6 = 0; i_6 < class226_0.field2601; i_6++) {
               i_7 = (class226_0.field2707 + 32) * i_6;
               i_8 = (class226_0.field2654 + 32) * i_5;
               if (i_16 < 20) {
                  i_7 += class226_0.field2655[i_16];
                  i_8 += class226_0.field2656[i_16];
               }

               if (i_1 >= i_7 && i_2 >= i_8 && i_1 < i_7 + 32 && i_2 < i_8 + 32) {
                  client.field759 = i_16;
                  class149.field1922 = class226_0;
                  if (class226_0.field2704[i_16] > 0) {
                     class265 class265_9 = class66.method1223(class226_0.field2704[i_16] - 1, 335666244);
                     if (client.field801 == 1 && class52.method844(class146.method3318(class226_0, 2105769138), -677680032)) {
                        if (class226_0.field2586 != class26.field140 || i_16 != class103.field1322) {
                           method564("Use", client.field744 + " " + "->" + " " + class23.method222(16748608, (byte) 107) + class265_9.field3444, 31, class265_9.field3480, i_16, class226_0.field2586, -1352586281);
                        }
                     } else if (client.field803 && class52.method844(class146.method3318(class226_0, 1797604093), -2068236954)) {
                        if ((class75.field637 & 0x10) == 16) {
                           method564(client.field851, client.field807 + " " + "->" + " " + class23.method222(16748608, (byte) 67) + class265_9.field3444, 32, class265_9.field3480, i_16, class226_0.field2586, -2090612616);
                        }
                     } else {
                        String[] arr_10 = class265_9.field3459;
                        int i_11 = -1;
                        if (client.field663 && class169.method3501(725246299)) {
                           i_11 = class265_9.method4690((short) 32767);
                        }

                        if (class52.method844(class146.method3318(class226_0, -168115217), -618294000)) {
                           for (int i_12 = 4; i_12 >= 3; --i_12) {
                              if (i_12 != i_11) {
                                 class7.method88(class226_0, class265_9, i_16, i_12, false, 1516995220);
                              }
                           }
                        }

                        int i_13 = class146.method3318(class226_0, 424193357);
                        boolean bool_23 = (i_13 >> 31 & 0x1) != 0;
                        if (bool_23) {
                           method564("Use", class23.method222(16748608, (byte) 76) + class265_9.field3444, 38, class265_9.field3480, i_16, class226_0.field2586, -1323738363);
                        }

                        Object obj_10000 = null;
                        int i_14;
                        if (class52.method844(class146.method3318(class226_0, -391785271), -1377396619)) {
                           for (i_14 = 2; i_14 >= 0; --i_14) {
                              if (i_11 != i_14) {
                                 class7.method88(class226_0, class265_9, i_16, i_14, false, 567877141);
                              }
                           }

                           if (i_11 >= 0) {
                              class7.method88(class226_0, class265_9, i_16, i_11, true, 479501327);
                           }
                        }

                        arr_10 = class226_0.field2658;
                        if (arr_10 != null) {
                           for (i_14 = 4; i_14 >= 0; --i_14) {
                              if (arr_10[i_14] != null) {
                                 byte b_15 = 0;
                                 if (i_14 == 0) {
                                    b_15 = 39;
                                 }

                                 if (i_14 == 1) {
                                    b_15 = 40;
                                 }

                                 if (i_14 == 2) {
                                    b_15 = 41;
                                 }

                                 if (i_14 == 3) {
                                    b_15 = 42;
                                 }

                                 if (i_14 == 4) {
                                    b_15 = 43;
                                 }

                                 method564(arr_10[i_14], class23.method222(16748608, (byte) 109) + class265_9.field3444, b_15, class265_9.field3480, i_16, class226_0.field2586, -1459918751);
                              }
                           }
                        }

                        method564("Examine", class23.method222(16748608, (byte) 14) + class265_9.field3444, 1005, class265_9.field3480, i_16, class226_0.field2586, -1382426216);
                     }
                  }
               }

               ++i_16;
            }
         }
      }

      if (class226_0.field2585) {
         if (client.field803) {
            if (class121.method2570(class146.method3318(class226_0, 487049411), 319524550) && (class75.field637 & 0x20) == 32) {
               method564(client.field851, client.field807 + " " + "->" + " " + class226_0.field2665, 58, 0, class226_0.field2587, class226_0.field2586, -1880204871);
            }
         } else {
            for (i_16 = 9; i_16 >= 5; --i_16) {
               i_7 = class146.method3318(class226_0, 1588000750);
               boolean bool_21 = (i_7 >> i_16 + 1 & 0x1) != 0;
               String string_17;
               if (!bool_21 && class226_0.field2613 == null) {
                  string_17 = null;
               } else if (class226_0.field2662 != null && class226_0.field2662.length > i_16 && class226_0.field2662[i_16] != null && class226_0.field2662[i_16].trim().length() != 0) {
                  string_17 = class226_0.field2662[i_16];
               } else {
                  string_17 = null;
               }

               if (string_17 != null) {
                  method564(string_17, class226_0.field2665, 1007, i_16 + 1, class226_0.field2587, class226_0.field2586, -1728335151);
               }
            }

            string_4 = class241.method4150(class226_0, (byte) -54);
            if (string_4 != null) {
               method564(string_4, class226_0.field2665, 25, 0, class226_0.field2587, class226_0.field2586, -2077814594);
            }

            for (i_5 = 4; i_5 >= 0; --i_5) {
               i_8 = class146.method3318(class226_0, 1144231581);
               boolean bool_22 = (i_8 >> i_5 + 1 & 0x1) != 0;
               String string_18;
               if (!bool_22 && class226_0.field2613 == null) {
                  string_18 = null;
               } else if (class226_0.field2662 != null && class226_0.field2662.length > i_5 && class226_0.field2662[i_5] != null && class226_0.field2662[i_5].trim().length() != 0) {
                  string_18 = class226_0.field2662[i_5];
               } else {
                  string_18 = null;
               }

               if (string_18 != null) {
                  class40.method597(string_18, class226_0.field2665, 57, i_5 + 1, class226_0.field2587, class226_0.field2586, class226_0.field2623, -1609914367);
               }
            }

            i_6 = class146.method3318(class226_0, 794788291);
            boolean bool_20 = (i_6 & 0x1) != 0;
            if (bool_20) {
               method564("Continue", "", 30, 0, class226_0.field2587, class226_0.field2586, -1476604039);
            }
         }
      }

   }

}
