import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class class7 implements class203 {

   static class318 field30;
   static final class7 field25 = new class7(0, 0);
   final int field26;
   final int field24;

   class7(int i_1, int i_2) {
      this.field26 = i_1;
      this.field24 = i_2;
   }

   public int vmethod6086(int i_1) {
      return this.field24;
   }

   static void method89(class226 class226_0, int i_1, int i_2, int i_3) {
      if (class226_0.field2591 == 0) {
         class226_0.field2694 = class226_0.field2595;
      } else if (class226_0.field2591 == 1) {
         class226_0.field2694 = class226_0.field2595 + (i_1 - class226_0.field2601) / 2;
      } else if (class226_0.field2591 == 2) {
         class226_0.field2694 = i_1 - class226_0.field2601 - class226_0.field2595;
      } else if (class226_0.field2591 == 3) {
         class226_0.field2694 = class226_0.field2595 * i_1 >> 14;
      } else if (class226_0.field2591 == 4) {
         class226_0.field2694 = (class226_0.field2595 * i_1 >> 14) + (i_1 - class226_0.field2601) / 2;
      } else {
         class226_0.field2694 = i_1 - class226_0.field2601 - (class226_0.field2595 * i_1 >> 14);
      }

      if (class226_0.field2692 == 0) {
         class226_0.field2600 = class226_0.field2579;
      } else if (class226_0.field2692 == 1) {
         class226_0.field2600 = (i_2 - class226_0.field2602) / 2 + class226_0.field2579;
      } else if (class226_0.field2692 == 2) {
         class226_0.field2600 = i_2 - class226_0.field2602 - class226_0.field2579;
      } else if (class226_0.field2692 == 3) {
         class226_0.field2600 = i_2 * class226_0.field2579 >> 14;
      } else if (class226_0.field2692 == 4) {
         class226_0.field2600 = (i_2 * class226_0.field2579 >> 14) + (i_2 - class226_0.field2602) / 2;
      } else {
         class226_0.field2600 = i_2 - class226_0.field2602 - (i_2 * class226_0.field2579 >> 14);
      }

   }

   static void method83(int i_0) {
      class94.field1196 = class94.field1196.trim();
      if (class94.field1196.length() == 0) {
         class62.method1087("Please enter your username.", "If you created your account after November", "2010, this will be the creation email address.", -1900063983);
      } else {
         long long_2;
         try {
            URL url_4 = new URL(class43.method632("services", false, (byte) 127) + "m=accountappeal/login.ws");
            URLConnection urlconnection_5 = url_4.openConnection();
            urlconnection_5.setRequestProperty("connection", "close");
            urlconnection_5.setDoInput(true);
            urlconnection_5.setDoOutput(true);
            urlconnection_5.setConnectTimeout(5000);
            OutputStreamWriter outputstreamwriter_6 = new OutputStreamWriter(urlconnection_5.getOutputStream());
            outputstreamwriter_6.write("data1=req");
            outputstreamwriter_6.flush();
            InputStream inputstream_7 = urlconnection_5.getInputStream();
            class310 class310_8 = new class310(new byte[1000]);

            while (true) {
               int i_9 = inputstream_7.read(class310_8.field3752, class310_8.field3751, 1000 - class310_8.field3751);
               if (i_9 == -1) {
                  class310_8.field3751 = 0;
                  long long_11 = class310_8.method5508(-1686926914);
                  long_2 = long_11;
                  break;
               }

               class310_8.field3751 += i_9;
               if (class310_8.field3751 >= 1000) {
                  long_2 = 0L;
                  break;
               }
            }
         } catch (Exception exception_15) {
            long_2 = 0L;
         }

         int i_1;
         if (long_2 == 0L) {
            i_1 = 5;
         } else {
            i_1 = class205.method3693(long_2, class94.field1196, (short) 765);
         }

         switch(i_1) {
         case 2:
            class62.method1087(class233.field3053, class233.field3054, class233.field3055, -934383986);
            class94.field1191 = 6;
            break;
         case 3:
            class62.method1087("", "Error connecting to server.", "", -1072098411);
            break;
         case 4:
            class62.method1087("The part of the website you are trying", "to connect to is offline at the moment.", "Please try again later.", -1655770050);
            break;
         case 5:
            class62.method1087("Sorry, there was an error trying to", "log you in to this part of the website.", "Please try again later.", -1687422668);
            break;
         case 6:
            class62.method1087("", "Error connecting to server.", "", -1436802930);
            break;
         case 7:
            class62.method1087("You must enter a valid login to proceed. For accounts", "created after 24th November 2010, please use your", "email address. Otherwise please use your username.", -1407578923);
         }

      }
   }

   static final void method87(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      int i_7 = i_2 - i_0;
      int i_8 = i_3 - i_1;
      int i_9 = i_7 >= 0 ? i_7 : -i_7;
      int i_10 = i_8 >= 0 ? i_8 : -i_8;
      int i_11 = i_9;
      if (i_9 < i_10) {
         i_11 = i_10;
      }

      if (i_11 != 0) {
         int i_12 = (i_7 << 16) / i_11;
         int i_13 = (i_8 << 16) / i_11;
         if (i_13 <= i_12) {
            i_12 = -i_12;
         } else {
            i_13 = -i_13;
         }

         int i_14 = i_5 * i_13 >> 17;
         int i_15 = i_5 * i_13 + 1 >> 17;
         int i_16 = i_5 * i_12 >> 17;
         int i_17 = i_5 * i_12 + 1 >> 17;
         i_0 -= class331.field3890;
         i_1 -= class331.field3887;
         int i_18 = i_0 + i_14;
         int i_19 = i_0 - i_15;
         int i_20 = i_0 + i_7 - i_15;
         int i_21 = i_0 + i_14 + i_7;
         int i_22 = i_16 + i_1;
         int i_23 = i_1 - i_17;
         int i_24 = i_8 + i_1 - i_17;
         int i_25 = i_16 + i_8 + i_1;
         class139.method3050(i_18, i_19, i_20);
         class139.method3053(i_22, i_23, i_24, i_18, i_19, i_20, i_4);
         class139.method3050(i_18, i_20, i_21);
         class139.method3053(i_22, i_24, i_25, i_18, i_20, i_21, i_4);
      }
   }

   static final void method86(class77 class77_0, int i_1) {
      long long_2 = 0L;
      int i_4 = -1;
      int i_5 = 0;
      int i_6 = 0;
      if (class77_0.field925 == 0) {
         long_2 = class67.field536.method3138(class77_0.field924, class77_0.field926, class77_0.field927);
      }

      if (class77_0.field925 == 1) {
         long_2 = class67.field536.method3158(class77_0.field924, class77_0.field926, class77_0.field927);
      }

      if (class77_0.field925 == 2) {
         long_2 = class67.field536.method3159(class77_0.field924, class77_0.field926, class77_0.field927);
      }

      if (class77_0.field925 == 3) {
         long_2 = class67.field536.method3160(class77_0.field924, class77_0.field926, class77_0.field927);
      }

      if (long_2 != 0L) {
         int i_7 = class67.field536.method3139(class77_0.field924, class77_0.field926, class77_0.field927, long_2);
         i_4 = class149.method3348(long_2);
         i_5 = i_7 & 0x1f;
         i_6 = i_7 >> 6 & 0x3;
      }

      class77_0.field928 = i_4;
      class77_0.field935 = i_5;
      class77_0.field929 = i_6;
   }

   static final void method90(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, class142 class142_6, class181 class181_7, byte b_8) {
      if (!client.field910 || (class64.field507[0][i_1][i_2] & 0x2) != 0 || (class64.field507[i_0][i_1][i_2] & 0x10) == 0) {
         if (i_0 < class64.field506) {
            class64.field506 = i_0;
         }

         class264 class264_9 = class34.method383(i_3, -1997028613);
         int i_10;
         int i_11;
         if (i_4 != 1 && i_4 != 3) {
            i_10 = class264_9.field3418;
            i_11 = class264_9.field3396;
         } else {
            i_10 = class264_9.field3396;
            i_11 = class264_9.field3418;
         }

         int i_12;
         int i_13;
         if (i_10 + i_1 <= 104) {
            i_12 = (i_10 >> 1) + i_1;
            i_13 = (i_10 + 1 >> 1) + i_1;
         } else {
            i_12 = i_1;
            i_13 = i_1 + 1;
         }

         int i_14;
         int i_15;
         if (i_11 + i_2 <= 104) {
            i_14 = (i_11 >> 1) + i_2;
            i_15 = i_2 + (i_11 + 1 >> 1);
         } else {
            i_14 = i_2;
            i_15 = i_2 + 1;
         }

         int[][] ints_16 = class64.field519[i_0];
         int i_17 = ints_16[i_13][i_15] + ints_16[i_13][i_14] + ints_16[i_12][i_14] + ints_16[i_12][i_15] >> 2;
         int i_18 = (i_1 << 7) + (i_10 << 6);
         int i_19 = (i_2 << 7) + (i_11 << 6);
         long long_20 = class88.method2085(i_1, i_2, 2, class264_9.field3423 == 0, i_3, (byte) -116);
         int i_22 = i_5 + (i_4 << 6);
         if (class264_9.field3417 == 1) {
            i_22 += 256;
         }

         int i_24;
         int i_25;
         if (class264_9.method4591(-2120899422)) {
            class84 class84_23 = new class84();
            class84_23.field1074 = i_0;
            class84_23.field1079 = i_1 * 128;
            class84_23.field1076 = i_2 * 128;
            i_24 = class264_9.field3418;
            i_25 = class264_9.field3396;
            if (i_4 == 1 || i_4 == 3) {
               i_24 = class264_9.field3396;
               i_25 = class264_9.field3418;
            }

            class84_23.field1073 = (i_24 + i_1) * 128;
            class84_23.field1078 = (i_25 + i_2) * 128;
            class84_23.field1080 = class264_9.field3424;
            class84_23.field1086 = class264_9.field3408 * 128;
            class84_23.field1085 = class264_9.field3426;
            class84_23.field1083 = class264_9.field3388;
            class84_23.field1084 = class264_9.field3428;
            if (class264_9.field3421 != null) {
               class84_23.field1087 = class264_9;
               class84_23.method1959(401252200);
            }

            class84.field1075.method4888(class84_23);
            if (class84_23.field1084 != null) {
               class84_23.field1081 = class84_23.field1085 + (int)(Math.random() * (double)(class84_23.field1083 - class84_23.field1085));
            }
         }

         Object obj_35;
         if (i_5 == 22) {
            if (!client.field910 || class264_9.field3423 != 0 || class264_9.field3397 == 1 || class264_9.field3380) {
               if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                  obj_35 = class264_9.method4623(22, i_4, ints_16, i_18, i_17, i_19, (short) 6726);
               } else {
                  obj_35 = new class102(i_3, 22, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
               }

               class142_6.method3137(i_0, i_1, i_2, i_17, (class144) obj_35, long_20, i_22);
               if (class264_9.field3397 == 1 && class181_7 != null) {
                  class181_7.method3596(i_1, i_2, -1697769054);
               }

            }
         } else if (i_5 != 10 && i_5 != 11) {
            if (i_5 >= 12) {
               if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                  obj_35 = class264_9.method4623(i_5, i_4, ints_16, i_18, i_17, i_19, (short) 13551);
               } else {
                  obj_35 = new class102(i_3, i_5, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
               }

               class142_6.method3132(i_0, i_1, i_2, i_17, 1, 1, (class144) obj_35, 0, long_20, i_22);
               if (i_5 >= 12 && i_5 <= 17 && i_5 != 13 && i_0 > 0) {
                  class14.field52[i_0][i_1][i_2] |= 0x924;
               }

               if (class264_9.field3397 != 0 && class181_7 != null) {
                  class181_7.method3586(i_1, i_2, i_10, i_11, class264_9.field3398, (byte) -10);
               }

            } else if (i_5 == 0) {
               if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                  obj_35 = class264_9.method4623(0, i_4, ints_16, i_18, i_17, i_19, (short) -1444);
               } else {
                  obj_35 = new class102(i_3, 0, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
               }

               class142_6.method3258(i_0, i_1, i_2, i_17, (class144) obj_35, (class144) null, class64.field512[i_4], 0, long_20, i_22);
               if (i_4 == 0) {
                  if (class264_9.field3425) {
                     class64.field511[i_0][i_1][i_2] = 50;
                     class64.field511[i_0][i_1][i_2 + 1] = 50;
                  }

                  if (class264_9.field3402) {
                     class14.field52[i_0][i_1][i_2] |= 0x249;
                  }
               } else if (i_4 == 1) {
                  if (class264_9.field3425) {
                     class64.field511[i_0][i_1][i_2 + 1] = 50;
                     class64.field511[i_0][i_1 + 1][i_2 + 1] = 50;
                  }

                  if (class264_9.field3402) {
                     class14.field52[i_0][i_1][1 + i_2] |= 0x492;
                  }
               } else if (i_4 == 2) {
                  if (class264_9.field3425) {
                     class64.field511[i_0][i_1 + 1][i_2] = 50;
                     class64.field511[i_0][i_1 + 1][i_2 + 1] = 50;
                  }

                  if (class264_9.field3402) {
                     class14.field52[i_0][i_1 + 1][i_2] |= 0x249;
                  }
               } else if (i_4 == 3) {
                  if (class264_9.field3425) {
                     class64.field511[i_0][i_1][i_2] = 50;
                     class64.field511[i_0][i_1 + 1][i_2] = 50;
                  }

                  if (class264_9.field3402) {
                     class14.field52[i_0][i_1][i_2] |= 0x492;
                  }
               }

               if (class264_9.field3397 != 0 && class181_7 != null) {
                  class181_7.method3582(i_1, i_2, i_5, i_4, class264_9.field3398, (byte) -49);
               }

               if (class264_9.field3404 != 16) {
                  class142_6.method3147(i_0, i_1, i_2, class264_9.field3404);
               }

            } else if (i_5 == 1) {
               if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                  obj_35 = class264_9.method4623(1, i_4, ints_16, i_18, i_17, i_19, (short) 11872);
               } else {
                  obj_35 = new class102(i_3, 1, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
               }

               class142_6.method3258(i_0, i_1, i_2, i_17, (class144) obj_35, (class144) null, class64.field513[i_4], 0, long_20, i_22);
               if (class264_9.field3425) {
                  if (i_4 == 0) {
                     class64.field511[i_0][i_1][i_2 + 1] = 50;
                  } else if (i_4 == 1) {
                     class64.field511[i_0][i_1 + 1][i_2 + 1] = 50;
                  } else if (i_4 == 2) {
                     class64.field511[i_0][i_1 + 1][i_2] = 50;
                  } else if (i_4 == 3) {
                     class64.field511[i_0][i_1][i_2] = 50;
                  }
               }

               if (class264_9.field3397 != 0 && class181_7 != null) {
                  class181_7.method3582(i_1, i_2, i_5, i_4, class264_9.field3398, (byte) -12);
               }

            } else {
               int i_29;
               if (i_5 == 2) {
                  i_29 = i_4 + 1 & 0x3;
                  Object obj_30;
                  Object obj_31;
                  if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                     obj_30 = class264_9.method4623(2, i_4 + 4, ints_16, i_18, i_17, i_19, (short) 6745);
                     obj_31 = class264_9.method4623(2, i_29, ints_16, i_18, i_17, i_19, (short) -2208);
                  } else {
                     obj_30 = new class102(i_3, 2, i_4 + 4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                     obj_31 = new class102(i_3, 2, i_29, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                  }

                  class142_6.method3258(i_0, i_1, i_2, i_17, (class144) obj_30, (class144) obj_31, class64.field512[i_4], class64.field512[i_29], long_20, i_22);
                  if (class264_9.field3402) {
                     if (i_4 == 0) {
                        class14.field52[i_0][i_1][i_2] |= 0x249;
                        class14.field52[i_0][i_1][1 + i_2] |= 0x492;
                     } else if (i_4 == 1) {
                        class14.field52[i_0][i_1][i_2 + 1] |= 0x492;
                        class14.field52[i_0][i_1 + 1][i_2] |= 0x249;
                     } else if (i_4 == 2) {
                        class14.field52[i_0][i_1 + 1][i_2] |= 0x249;
                        class14.field52[i_0][i_1][i_2] |= 0x492;
                     } else if (i_4 == 3) {
                        class14.field52[i_0][i_1][i_2] |= 0x492;
                        class14.field52[i_0][i_1][i_2] |= 0x249;
                     }
                  }

                  if (class264_9.field3397 != 0 && class181_7 != null) {
                     class181_7.method3582(i_1, i_2, i_5, i_4, class264_9.field3398, (byte) -63);
                  }

                  if (class264_9.field3404 != 16) {
                     class142_6.method3147(i_0, i_1, i_2, class264_9.field3404);
                  }

               } else if (i_5 == 3) {
                  if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                     obj_35 = class264_9.method4623(3, i_4, ints_16, i_18, i_17, i_19, (short) -9359);
                  } else {
                     obj_35 = new class102(i_3, 3, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                  }

                  class142_6.method3258(i_0, i_1, i_2, i_17, (class144) obj_35, (class144) null, class64.field513[i_4], 0, long_20, i_22);
                  if (class264_9.field3425) {
                     if (i_4 == 0) {
                        class64.field511[i_0][i_1][i_2 + 1] = 50;
                     } else if (i_4 == 1) {
                        class64.field511[i_0][i_1 + 1][i_2 + 1] = 50;
                     } else if (i_4 == 2) {
                        class64.field511[i_0][i_1 + 1][i_2] = 50;
                     } else if (i_4 == 3) {
                        class64.field511[i_0][i_1][i_2] = 50;
                     }
                  }

                  if (class264_9.field3397 != 0 && class181_7 != null) {
                     class181_7.method3582(i_1, i_2, i_5, i_4, class264_9.field3398, (byte) 2);
                  }

               } else if (i_5 == 9) {
                  if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                     obj_35 = class264_9.method4623(i_5, i_4, ints_16, i_18, i_17, i_19, (short) -13372);
                  } else {
                     obj_35 = new class102(i_3, i_5, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                  }

                  class142_6.method3132(i_0, i_1, i_2, i_17, 1, 1, (class144) obj_35, 0, long_20, i_22);
                  if (class264_9.field3397 != 0 && class181_7 != null) {
                     class181_7.method3586(i_1, i_2, i_10, i_11, class264_9.field3398, (byte) -18);
                  }

                  if (class264_9.field3404 != 16) {
                     class142_6.method3147(i_0, i_1, i_2, class264_9.field3404);
                  }

               } else if (i_5 == 4) {
                  if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                     obj_35 = class264_9.method4623(4, i_4, ints_16, i_18, i_17, i_19, (short) -2865);
                  } else {
                     obj_35 = new class102(i_3, 4, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                  }

                  class142_6.method3140(i_0, i_1, i_2, i_17, (class144) obj_35, (class144) null, class64.field512[i_4], 0, 0, 0, long_20, i_22);
               } else {
                  long long_32;
                  Object obj_34;
                  if (i_5 == 5) {
                     i_29 = 16;
                     long_32 = class142_6.method3138(i_0, i_1, i_2);
                     if (long_32 != 0L) {
                        i_29 = class34.method383(class149.method3348(long_32), 615984604).field3404;
                     }

                     if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                        obj_34 = class264_9.method4623(4, i_4, ints_16, i_18, i_17, i_19, (short) 1066);
                     } else {
                        obj_34 = new class102(i_3, 4, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                     }

                     class142_6.method3140(i_0, i_1, i_2, i_17, (class144) obj_34, (class144) null, class64.field512[i_4], 0, i_29 * class64.field518[i_4], i_29 * class64.field515[i_4], long_20, i_22);
                  } else if (i_5 == 6) {
                     i_29 = 8;
                     long_32 = class142_6.method3138(i_0, i_1, i_2);
                     if (long_32 != 0L) {
                        i_29 = class34.method383(class149.method3348(long_32), -1907178713).field3404 / 2;
                     }

                     if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                        obj_34 = class264_9.method4623(4, i_4 + 4, ints_16, i_18, i_17, i_19, (short) -2940);
                     } else {
                        obj_34 = new class102(i_3, 4, i_4 + 4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                     }

                     class142_6.method3140(i_0, i_1, i_2, i_17, (class144) obj_34, (class144) null, 256, i_4, i_29 * class64.field509[i_4], i_29 * class64.field517[i_4], long_20, i_22);
                  } else if (i_5 == 7) {
                     i_24 = i_4 + 2 & 0x3;
                     if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                        obj_35 = class264_9.method4623(4, i_24 + 4, ints_16, i_18, i_17, i_19, (short) -6544);
                     } else {
                        obj_35 = new class102(i_3, 4, i_24 + 4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                     }

                     class142_6.method3140(i_0, i_1, i_2, i_17, (class144) obj_35, (class144) null, 256, i_24, 0, 0, long_20, i_22);
                  } else if (i_5 == 8) {
                     i_29 = 8;
                     long_32 = class142_6.method3138(i_0, i_1, i_2);
                     if (long_32 != 0L) {
                        i_29 = class34.method383(class149.method3348(long_32), -259054920).field3404 / 2;
                     }

                     int i_28 = i_4 + 2 & 0x3;
                     Object obj_27;
                     if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
                        obj_34 = class264_9.method4623(4, i_4 + 4, ints_16, i_18, i_17, i_19, (short) -9095);
                        obj_27 = class264_9.method4623(4, i_28 + 4, ints_16, i_18, i_17, i_19, (short) 140);
                     } else {
                        obj_34 = new class102(i_3, 4, i_4 + 4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                        obj_27 = new class102(i_3, 4, i_28 + 4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
                     }

                     class142_6.method3140(i_0, i_1, i_2, i_17, (class144) obj_34, (class144) obj_27, 256, i_4, i_29 * class64.field509[i_4], i_29 * class64.field517[i_4], long_20, i_22);
                  }
               }
            }
         } else {
            if (class264_9.field3411 == -1 && class264_9.field3421 == null) {
               obj_35 = class264_9.method4623(10, i_4, ints_16, i_18, i_17, i_19, (short) -18803);
            } else {
               obj_35 = new class102(i_3, 10, i_4, i_0, i_1, i_2, class264_9.field3411, class264_9.field3429, (class144) null);
            }

            if (obj_35 != null && class142_6.method3132(i_0, i_1, i_2, i_17, i_10, i_11, (class144) obj_35, i_5 == 11 ? 256 : 0, long_20, i_22) && class264_9.field3425) {
               i_24 = 15;
               if (obj_35 instanceof class136) {
                  i_24 = ((class136) obj_35).method2963() / 4;
                  if (i_24 > 30) {
                     i_24 = 30;
                  }
               }

               for (i_25 = 0; i_25 <= i_10; i_25++) {
                  for (int i_26 = 0; i_26 <= i_11; i_26++) {
                     if (i_24 > class64.field511[i_0][i_25 + i_1][i_26 + i_2]) {
                        class64.field511[i_0][i_25 + i_1][i_26 + i_2] = (byte)i_24;
                     }
                  }
               }
            }

            if (class264_9.field3397 != 0 && class181_7 != null) {
               class181_7.method3586(i_1, i_2, i_10, i_11, class264_9.field3398, (byte) -101);
            }

         }
      }
   }

   static final void method88(class226 class226_0, class265 class265_1, int i_2, int i_3, boolean bool_4, int i_5) {
      String[] arr_6 = class265_1.field3459;
      byte b_7 = -1;
      String str_8 = null;
      if (arr_6 != null && arr_6[i_3] != null) {
         if (i_3 == 0) {
            b_7 = 33;
         } else if (i_3 == 1) {
            b_7 = 34;
         } else if (i_3 == 2) {
            b_7 = 35;
         } else if (i_3 == 3) {
            b_7 = 36;
         } else {
            b_7 = 37;
         }

         str_8 = arr_6[i_3];
      } else if (i_3 == 4) {
         b_7 = 37;
         str_8 = "Drop";
      }

      if (b_7 != -1 && str_8 != null) {
         class40.method597(str_8, class23.method222(16748608, (byte) 13) + class265_1.field3444, b_7, class265_1.field3480, i_2, class226_0.field2586, bool_4, -1609914367);
      }

   }

}
