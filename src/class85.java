import java.util.Calendar;

public class class85 {

   static int field1105;
   static int[] field1090 = new int[5];
   static int[][] field1094 = new int[5][5000];
   static int[] field1095 = new int[1000];
   static String[] field1096 = new String[1000];
   static int field1098 = 0;
   static class65[] field1099 = new class65[50];
   static Calendar field1091 = Calendar.getInstance();
   static final String[] field1101 = new String[] {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
   static boolean field1104 = false;
   static boolean field1103 = false;
   static int field1097 = 0;
   static final double field1093 = Math.log(2.0D);

   static void method2038(class226[] arr_0, int i_1, int i_2, int i_3, boolean bool_4, int i_5) {
      for (int i_6 = 0; i_6 < arr_0.length; i_6++) {
         class226 class226_7 = arr_0[i_6];
         if (class226_7 != null && class226_7.field2670 == i_1) {
            class96.method2156(class226_7, i_2, i_3, bool_4, (byte) -31);
            class7.method89(class226_7, i_2, i_3, -731598276);
            if (class226_7.field2607 > class226_7.field2609 - class226_7.field2601) {
               class226_7.field2607 = class226_7.field2609 - class226_7.field2601;
            }

            if (class226_7.field2607 < 0) {
               class226_7.field2607 = 0;
            }

            if (class226_7.field2608 > class226_7.field2639 - class226_7.field2602) {
               class226_7.field2608 = class226_7.field2639 - class226_7.field2602;
            }

            if (class226_7.field2608 < 0) {
               class226_7.field2608 = 0;
            }

            if (class226_7.field2588 == 0) {
               class77.method1773(arr_0, class226_7, bool_4, (byte) 0);
            }
         }
      }

   }

   public static void method2054(class244 class244_0, int i_1, int i_2, int i_3, boolean bool_4, int i_5) {
      class206.field2421 = 1;
      class221.field2538 = class244_0;
      class79.field1002 = i_1;
      class206.field2425 = i_2;
      class16.field71 = i_3;
      class231.field2772 = bool_4;
      class42.field312 = 10000;
   }

   static boolean method1967(char var_0, int i_1) {
      return "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ".indexOf(var_0) != -1;
   }

}
