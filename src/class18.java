public class class18 {

   byte field83;
   public int field77;
   public int field79;
   public int field78;
   public int field81;
   public int field82;

   public class18() {
   }

   public class18(class310 class310_1, boolean bool_2) {
      this.field83 = class310_1.method5535((byte) 0);
      this.field77 = class310_1.method5729(-461796413);
      this.field79 = class310_1.method5507(1923194264);
      this.field78 = class310_1.method5507(1426878140);
      this.field81 = class310_1.method5507(1059020642);
      this.field82 = class310_1.method5507(1611012007);
   }

   void method171(int i_1, int i_2) {
      this.field83 &= ~0x7;
      this.field83 = (byte)(this.field83 | i_1 & 0x7);
   }

   void method176(int i_1, int i_2) {
      this.field83 &= ~0x8;
      if (i_1 == 1) {
         this.field83 = (byte)(this.field83 | 0x8);
      }

   }

   public int method170(int i_1) {
      return (this.field83 & 0x8) == 8 ? 1 : 0;
   }

   public int method169(int i_1) {
      return this.field83 & 0x7;
   }

   static int method186(int i_0, int i_1, byte b_2) {
      class268 class268_3 = class183.method3619(i_0, 1923247344);
      if (class268_3 == null) {
         return i_1;
      } else if (class268_3.field3537 >= 0) {
         return class268_3.field3537 | ~0xffffff;
      } else {
         int i_5;
         int i_6;
         if (class268_3.field3542 >= 0) {
            i_5 = class139.field1801.vmethod3330(class268_3.field3542, -1889098773);
            byte b_12 = 96;
            int i_4;
            if (i_5 == -2) {
               i_4 = 12345678;
            } else if (i_5 == -1) {
               if (b_12 < 0) {
                  b_12 = 0;
               } else if (b_12 > 127) {
                  b_12 = 127;
               }

               i_6 = 127 - b_12;
               i_4 = i_6;
            } else {
               i_6 = b_12 * (i_5 & 0x7f) / 128;
               if (i_6 < 2) {
                  i_6 = 2;
               } else if (i_6 > 126) {
                  i_6 = 126;
               }

               i_4 = i_6 + (i_5 & 0xff80);
            }

            return class139.field1800[i_4] | ~0xffffff;
         } else if (class268_3.field3534 == 16711935) {
            return i_1;
         } else {
            i_5 = class268_3.field3532;
            i_6 = class268_3.field3539;
            int i_7 = class268_3.field3540;
            if (i_7 > 179) {
               i_6 /= 2;
            }

            if (i_7 > 192) {
               i_6 /= 2;
            }

            if (i_7 > 217) {
               i_6 /= 2;
            }

            if (i_7 > 243) {
               i_6 /= 2;
            }

            int i_8 = (i_6 / 32 << 7) + i_7 / 2 + (i_5 / 4 << 10);
            byte b_10 = 96;
            int i_9;
            if (i_8 == -2) {
               i_9 = 12345678;
            } else {
               int i_13;
               if (i_8 == -1) {
                  if (b_10 < 0) {
                     b_10 = 0;
                  } else if (b_10 > 127) {
                     b_10 = 127;
                  }

                  i_13 = 127 - b_10;
                  i_9 = i_13;
               } else {
                  i_13 = b_10 * (i_8 & 0x7f) / 128;
                  if (i_13 < 2) {
                     i_13 = 2;
                  } else if (i_13 > 126) {
                     i_13 = 126;
                  }

                  i_9 = i_13 + (i_8 & 0xff80);
               }
            }

            return class139.field1800[i_9] | ~0xffffff;
         }
      }
   }

   public static byte[] method188(Object object_0, boolean bool_1, int i_2) {
      if (object_0 == null) {
         return null;
      } else if (object_0 instanceof byte[]) {
         byte[] bytes_7 = (byte[]) ((byte[]) object_0);
         if (bool_1) {
            int i_5 = bytes_7.length;
            byte[] bytes_6 = new byte[i_5];
            System.arraycopy(bytes_7, 0, bytes_6, 0, i_5);
            return bytes_6;
         } else {
            return bytes_7;
         }
      } else if (object_0 instanceof class214) {
         class214 class214_3 = (class214) object_0;
         return class214_3.vmethod3945(-938287989);
      } else {
         throw new IllegalArgumentException();
      }
   }

   static void method187(int i_0) {
      class353 class353_1 = null;

      try {
         class353_1 = class38.method587("", class93.field1174.field3141, true, -2139481165);
         class310 class310_2 = class282.field3617.method1881((byte) -99);
         class353_1.method6565(class310_2.field3752, 0, class310_2.field3751, 1345307435);
      } catch (Exception exception_4) {
         ;
      }

      try {
         if (class353_1 != null) {
            class353_1.method6574(true, 1149430917);
         }
      } catch (Exception exception_3) {
         ;
      }

   }

   static final int method190(int i_0, int i_1, byte b_2) {
      int i_3 = i_1 * 57 + i_0;
      i_3 ^= i_3 << 13;
      int i_4 = (i_3 * i_3 * 15731 + 789221) * i_3 + 1376312589 & 0x7fffffff;
      return i_4 >> 19 & 0xff;
   }

   static final void method175(int i_0, int i_1, int i_2, int i_3) {
      if (i_0 >= 128 && i_1 >= 128 && i_0 <= 13056 && i_1 <= 13056) {
         int i_4 = class97.method2165(i_0, i_1, class151.field1949, 1691731719) - i_2;
         i_0 -= class75.field630;
         i_4 -= class54.field421;
         i_1 -= class69.field558;
         int i_5 = class139.field1797[class141.field1823];
         int i_6 = class139.field1780[class141.field1823];
         int i_7 = class139.field1797[class244.field3176];
         int i_8 = class139.field1780[class244.field3176];
         int i_9 = i_7 * i_1 + i_0 * i_8 >> 16;
         i_1 = i_8 * i_1 - i_0 * i_7 >> 16;
         i_0 = i_9;
         i_9 = i_6 * i_4 - i_5 * i_1 >> 16;
         i_1 = i_4 * i_5 + i_6 * i_1 >> 16;
         if (i_1 >= 50) {
            client.field747 = i_0 * client.field908 / i_1 + client.field906 / 2;
            client.field748 = client.field695 / 2 + i_9 * client.field908 / i_1;
         } else {
            client.field747 = -1;
            client.field748 = -1;
         }

      } else {
         client.field747 = -1;
         client.field748 = -1;
      }
   }

}
