import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class class168 extends class285 {

   static int field2013;
   final boolean field2014;

   public class168(boolean bool_1) {
      this.field2014 = bool_1;
   }

   int method3496(class289 class289_1, class289 class289_2, int i_3) {
      return class289_1.field3650 != 0 && class289_2.field3650 != 0 ? (this.field2014 ? class289_1.method5038(1670678120).method5193(class289_2.method5038(1670678120), -1152802722) : class289_2.method5038(1670678120).method5193(class289_1.method5038(1670678120), -1152802722)) : this.method5051(class289_1, class289_2, -992955252);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3496((class289) object_1, (class289) object_2, 1927447948);
   }

   public static final class114 method3498(class175 class175_0, int i_1, int i_2, byte b_3) {
      if (class114.field1443 == 0) {
         throw new IllegalStateException();
      } else if (i_1 >= 0 && i_1 < 2) {
         if (i_2 < 256) {
            i_2 = 256;
         }

         try {
            class114 class114_4 = class221.field2543.vmethod2366(-2011148953);
            class114_4.field1426 = new int[256 * (class114.field1421 ? 2 : 1)];
            class114_4.field1431 = i_2;
            class114_4.vmethod2465((byte) 99);
            class114_4.field1434 = (i_2 & ~0x3ff) + 1024;
            if (class114_4.field1434 > 16384) {
               class114_4.field1434 = 16384;
            }

            class114_4.vmethod2466(class114_4.field1434, (short) 3928);
            if (class114.field1424 > 0 && class114.field1441 == null) {
               class114.field1441 = new class116();
               class70.field566 = Executors.newScheduledThreadPool(1);
               class70.field566.scheduleAtFixedRate(class114.field1441, 0L, 10L, TimeUnit.MILLISECONDS);
            }

            if (class114.field1441 != null) {
               if (class114.field1441.field1455[i_1] != null) {
                  throw new IllegalArgumentException();
               }

               class114.field1441.field1455[i_1] = class114_4;
            }

            return class114_4;
         } catch (Throwable throwable_5) {
            return new class114();
         }
      } else {
         throw new IllegalArgumentException();
      }
   }

}
