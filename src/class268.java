public class class268 extends class184 {

   static class244 field3543;
   public static class154 field3533 = new class154(64);
   public int field3534 = 0;
   public int field3542 = -1;
   public boolean field3538 = true;
   public int field3537 = -1;
   public int field3532;
   public int field3539;
   public int field3540;
   public int field3541;
   public int field3535;
   public int field3536;

   void method4742(class310 class310_1, int i_2, int i_3, byte b_4) {
      if (i_2 == 1) {
         this.field3534 = class310_1.method5506((short) 239);
      } else if (i_2 == 2) {
         this.field3542 = class310_1.method5661((byte) -80);
      } else if (i_2 == 5) {
         this.field3538 = false;
      } else if (i_2 == 7) {
         this.field3537 = class310_1.method5506((short) 239);
      } else if (i_2 == 8) {
         ;
      }

   }

   void method4743(int i_1, int i_2) {
      double d_3 = (double)(i_1 >> 16 & 0xff) / 256.0D;
      double d_5 = (double)(i_1 >> 8 & 0xff) / 256.0D;
      double d_7 = (double)(i_1 & 0xff) / 256.0D;
      double d_9 = d_3;
      if (d_5 < d_3) {
         d_9 = d_5;
      }

      if (d_7 < d_9) {
         d_9 = d_7;
      }

      double d_11 = d_3;
      if (d_5 > d_3) {
         d_11 = d_5;
      }

      if (d_7 > d_11) {
         d_11 = d_7;
      }

      double d_13 = 0.0D;
      double d_15 = 0.0D;
      double d_17 = (d_11 + d_9) / 2.0D;
      if (d_9 != d_11) {
         if (d_17 < 0.5D) {
            d_15 = (d_11 - d_9) / (d_9 + d_11);
         }

         if (d_17 >= 0.5D) {
            d_15 = (d_11 - d_9) / (2.0D - d_11 - d_9);
         }

         if (d_11 == d_3) {
            d_13 = (d_5 - d_7) / (d_11 - d_9);
         } else if (d_11 == d_5) {
            d_13 = 2.0D + (d_7 - d_3) / (d_11 - d_9);
         } else if (d_7 == d_11) {
            d_13 = 4.0D + (d_3 - d_5) / (d_11 - d_9);
         }
      }

      d_13 /= 6.0D;
      this.field3532 = (int)(256.0D * d_13);
      this.field3539 = (int)(d_15 * 256.0D);
      this.field3540 = (int)(d_17 * 256.0D);
      if (this.field3539 < 0) {
         this.field3539 = 0;
      } else if (this.field3539 > 255) {
         this.field3539 = 255;
      }

      if (this.field3540 < 0) {
         this.field3540 = 0;
      } else if (this.field3540 > 255) {
         this.field3540 = 255;
      }

   }

   void method4740(int i_1) {
      if (this.field3537 != -1) {
         this.method4743(this.field3537, -774110413);
         this.field3541 = this.field3532;
         this.field3535 = this.field3539;
         this.field3536 = this.field3540;
      }

      this.method4743(this.field3534, -774110413);
   }

   void method4741(class310 class310_1, int i_2, int i_3) {
      while (true) {
         int i_4 = class310_1.method5661((byte) -7);
         if (i_4 == 0) {
            return;
         }

         this.method4742(class310_1, i_4, i_2, (byte) 41);
      }
   }

   public static class334 method4756(class244 class244_0, int i_1, byte b_2) {
      byte[] bytes_4 = class244_0.method4167(i_1, 1111207002);
      boolean bool_3;
      if (bytes_4 == null) {
         bool_3 = false;
      } else {
         class232.method4122(bytes_4, 962027364);
         bool_3 = true;
      }

      return !bool_3 ? null : class320.method5894((byte) 8);
   }

}
