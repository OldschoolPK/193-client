public final class class328 {

   int field3876 = 0;
   int field3874;
   class189[] field3873;
   class189 field3872;
   class189 field3875;

   public class328(int i_1) {
      this.field3874 = i_1;
      this.field3873 = new class189[i_1];

      for (int i_2 = 0; i_2 < i_1; i_2++) {
         class189 class189_3 = this.field3873[i_2] = new class189();
         class189_3.field2136 = class189_3;
         class189_3.field2138 = class189_3;
      }

   }

   public class189 method5962() {
      this.field3876 = 0;
      return this.method5957();
   }

   public class189 method5968(long long_1) {
      class189 class189_3 = this.field3873[(int)(long_1 & (long)(this.field3874 - 1))];

      for (this.field3872 = class189_3.field2136; class189_3 != this.field3872; this.field3872 = this.field3872.field2136) {
         if (this.field3872.field2137 == long_1) {
            class189 class189_4 = this.field3872;
            this.field3872 = this.field3872.field2136;
            return class189_4;
         }
      }

      this.field3872 = null;
      return null;
   }

   public class189 method5957() {
      class189 class189_1;
      if (this.field3876 > 0 && this.field3873[this.field3876 - 1] != this.field3875) {
         class189_1 = this.field3875;
         this.field3875 = class189_1.field2136;
         return class189_1;
      } else {
         do {
            if (this.field3876 >= this.field3874) {
               return null;
            }

            class189_1 = this.field3873[this.field3876++].field2136;
         } while (class189_1 == this.field3873[this.field3876 - 1]);

         this.field3875 = class189_1.field2136;
         return class189_1;
      }
   }

   public void method5956(class189 class189_1, long long_2) {
      if (class189_1.field2138 != null) {
         class189_1.method3628();
      }

      class189 class189_4 = this.field3873[(int)(long_2 & (long)(this.field3874 - 1))];
      class189_1.field2138 = class189_4.field2138;
      class189_1.field2136 = class189_4;
      class189_1.field2138.field2136 = class189_1;
      class189_1.field2136.field2138 = class189_1;
      class189_1.field2137 = long_2;
   }

}
