import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class class9 {

   public static class226[][] field44;
   static byte[][][] field40;
   static class334 field42;
   ExecutorService field39 = Executors.newSingleThreadExecutor();
   final class310 field41;
   final class12 field38;
   Future field43;

   public class9(class310 class310_1, class12 class12_2) {
      this.field41 = class310_1;
      this.field38 = class12_2;
      this.method96(-787508892);
   }

   void method96(int i_1) {
      this.field43 = this.field39.submit(new class8(this, this.field41, this.field38));
   }

   public boolean method112(int i_1) {
      return this.field43.isDone();
   }

   public class310 method98(byte b_1) {
      try {
         return (class310) this.field43.get();
      } catch (Exception exception_3) {
         return null;
      }
   }

   public void method97(int i_1) {
      this.field39.shutdown();
      this.field39 = null;
   }

   static final void method107(byte b_0) {
      Iterator iterator_1 = class100.field1285.iterator();

      while (iterator_1.hasNext()) {
         class73 class73_2 = (class73) iterator_1.next();
         class73_2.method1272(534772186);
      }

      if (class35.field244 != null) {
         class35.field244.method5219((byte) 82);
      }

   }

   static final int method101(int i_0, int i_1, byte b_2) {
      if (i_0 == -2) {
         return 12345678;
      } else if (i_0 == -1) {
         if (i_1 < 2) {
            i_1 = 2;
         } else if (i_1 > 126) {
            i_1 = 126;
         }

         return i_1;
      } else {
         i_1 = (i_0 & 0x7f) * i_1 / 128;
         if (i_1 < 2) {
            i_1 = 2;
         } else if (i_1 > 126) {
            i_1 = 126;
         }

         return (i_0 & 0xff80) + i_1;
      }
   }

}
