public abstract class class244 {

   static int field3176;
   static class344 field3175 = new class344();
   static int field3171 = 0;
   boolean field3173;
   boolean field3162;
   public int field3172;
   int field3159;
   int[] field3160;
   int[] field3163;
   int[] field3158;
   int[] field3165;
   int[][] field3166;
   Object[] field3169;
   Object[][] field3170;
   int[] field3161;
   class345 field3164;
   int[][] field3167;
   class345[] field3168;

   class244(boolean bool_1, boolean bool_2) {
      this.field3173 = bool_1;
      this.field3162 = bool_2;
   }

   public int[] method4171(int i_1, int i_2) {
      return i_1 >= 0 && i_1 < this.field3166.length ? this.field3166[i_1] : null;
   }

   public byte[] method4161(int i_1, int i_2, int[] ints_3, byte b_4) {
      if (i_1 >= 0 && i_1 < this.field3170.length && this.field3170[i_1] != null && i_2 >= 0 && i_2 < this.field3170[i_1].length) {
         if (this.field3170[i_1][i_2] == null) {
            boolean bool_5 = this.method4177(i_1, ints_3, (byte) 1);
            if (!bool_5) {
               this.vmethod4279(i_1, 786262695);
               bool_5 = this.method4177(i_1, ints_3, (byte) 1);
               if (!bool_5) {
                  return null;
               }
            }
         }

         byte[] bytes_6 = class18.method188(this.field3170[i_1][i_2], false, -2034592352);
         if (this.field3162) {
            this.field3170[i_1][i_2] = null;
         }

         return bytes_6;
      } else {
         return null;
      }
   }

   public int method4173(int i_1) {
      return this.field3170.length;
   }

   public byte[] method4167(int i_1, int i_2) {
      if (this.field3170.length == 1) {
         return this.method4160(0, i_1, (short) -31259);
      } else if (this.field3170[i_1].length == 1) {
         return this.method4160(i_1, 0, (short) -24153);
      } else {
         throw new RuntimeException();
      }
   }

   public byte[] method4245(int i_1, int i_2, int i_3) {
      if (i_1 >= 0 && i_1 < this.field3170.length && this.field3170[i_1] != null && i_2 >= 0 && i_2 < this.field3170[i_1].length) {
         if (this.field3170[i_1][i_2] == null) {
            boolean bool_4 = this.method4177(i_1, (int[]) null, (byte) 1);
            if (!bool_4) {
               this.vmethod4279(i_1, 786262695);
               bool_4 = this.method4177(i_1, (int[]) null, (byte) 1);
               if (!bool_4) {
                  return null;
               }
            }
         }

         byte[] bytes_5 = class18.method188(this.field3170[i_1][i_2], false, -2034592352);
         return bytes_5;
      } else {
         return null;
      }
   }

   public boolean method4162(int i_1, int i_2, int i_3) {
      if (i_1 >= 0 && i_1 < this.field3170.length && this.field3170[i_1] != null && i_2 >= 0 && i_2 < this.field3170[i_1].length) {
         if (this.field3170[i_1][i_2] != null) {
            return true;
         } else if (this.field3169[i_1] != null) {
            return true;
         } else {
            this.vmethod4279(i_1, 786262695);
            return this.field3169[i_1] != null;
         }
      } else {
         return false;
      }
   }

   public byte[] method4160(int i_1, int i_2, short s_3) {
      return this.method4161(i_1, i_2, (int[]) null, (byte) -14);
   }

   void vmethod4279(int i_1, int i_2) {
   }

   public byte[] method4176(int i_1, byte b_2) {
      if (this.field3170.length == 1) {
         return this.method4245(0, i_1, 311483843);
      } else if (this.field3170[i_1].length == 1) {
         return this.method4245(i_1, 0, -400282182);
      } else {
         throw new RuntimeException();
      }
   }

   public int method4172(int i_1, byte b_2) {
      return this.field3170[i_1].length;
   }

   int vmethod4270(int i_1, int i_2) {
      return this.field3169[i_1] != null ? 100 : 0;
   }

   void method4158(byte[] bytes_1, int i_2) {
      this.field3172 = class102.method2277(bytes_1, bytes_1.length, (byte) -35);
      class310 class310_3 = new class310(class320.method5895(bytes_1, (byte) 1));
      int i_4 = class310_3.method5661((byte) -22);
      if (i_4 >= 5 && i_4 <= 7) {
         if (i_4 >= 6) {
            class310_3.method5507(-1402827651);
         }

         int i_5 = class310_3.method5661((byte) 27);
         if (i_4 >= 7) {
            this.field3159 = class310_3.method5518(661695765);
         } else {
            this.field3159 = class310_3.method5729(-291195091);
         }

         int i_6 = 0;
         int i_7 = -1;
         this.field3160 = new int[this.field3159];
         int i_8;
         if (i_4 >= 7) {
            for (i_8 = 0; i_8 < this.field3159; i_8++) {
               this.field3160[i_8] = i_6 += class310_3.method5518(-1006843344);
               if (this.field3160[i_8] > i_7) {
                  i_7 = this.field3160[i_8];
               }
            }
         } else {
            for (i_8 = 0; i_8 < this.field3159; i_8++) {
               this.field3160[i_8] = i_6 += class310_3.method5729(1396768044);
               if (this.field3160[i_8] > i_7) {
                  i_7 = this.field3160[i_8];
               }
            }
         }

         this.field3163 = new int[i_7 + 1];
         this.field3158 = new int[i_7 + 1];
         this.field3165 = new int[i_7 + 1];
         this.field3166 = new int[i_7 + 1][];
         this.field3169 = new Object[i_7 + 1];
         this.field3170 = new Object[i_7 + 1][];
         if (i_5 != 0) {
            this.field3161 = new int[i_7 + 1];

            for (i_8 = 0; i_8 < this.field3159; i_8++) {
               this.field3161[this.field3160[i_8]] = class310_3.method5507(-853734363);
            }

            this.field3164 = new class345(this.field3161);
         }

         for (i_8 = 0; i_8 < this.field3159; i_8++) {
            this.field3163[this.field3160[i_8]] = class310_3.method5507(-1336422383);
         }

         for (i_8 = 0; i_8 < this.field3159; i_8++) {
            this.field3158[this.field3160[i_8]] = class310_3.method5507(536168343);
         }

         for (i_8 = 0; i_8 < this.field3159; i_8++) {
            this.field3165[this.field3160[i_8]] = class310_3.method5729(45825278);
         }

         int i_9;
         int i_10;
         int i_11;
         int i_12;
         int i_13;
         if (i_4 >= 7) {
            for (i_8 = 0; i_8 < this.field3159; i_8++) {
               i_9 = this.field3160[i_8];
               i_10 = this.field3165[i_9];
               i_6 = 0;
               i_11 = -1;
               this.field3166[i_9] = new int[i_10];

               for (i_12 = 0; i_12 < i_10; i_12++) {
                  i_13 = this.field3166[i_9][i_12] = i_6 += class310_3.method5518(-1522058178);
                  if (i_13 > i_11) {
                     i_11 = i_13;
                  }
               }

               this.field3170[i_9] = new Object[i_11 + 1];
            }
         } else {
            for (i_8 = 0; i_8 < this.field3159; i_8++) {
               i_9 = this.field3160[i_8];
               i_10 = this.field3165[i_9];
               i_6 = 0;
               i_11 = -1;
               this.field3166[i_9] = new int[i_10];

               for (i_12 = 0; i_12 < i_10; i_12++) {
                  i_13 = this.field3166[i_9][i_12] = i_6 += class310_3.method5729(-298938749);
                  if (i_13 > i_11) {
                     i_11 = i_13;
                  }
               }

               this.field3170[i_9] = new Object[i_11 + 1];
            }
         }

         if (i_5 != 0) {
            this.field3167 = new int[i_7 + 1][];
            this.field3168 = new class345[i_7 + 1];

            for (i_8 = 0; i_8 < this.field3159; i_8++) {
               i_9 = this.field3160[i_8];
               i_10 = this.field3165[i_9];
               this.field3167[i_9] = new int[this.field3170[i_9].length];

               for (i_11 = 0; i_11 < i_10; i_11++) {
                  this.field3167[i_9][this.field3166[i_9][i_11]] = class310_3.method5507(-491517048);
               }

               this.field3168[i_9] = new class345(this.field3167[i_9]);
            }
         }

      } else {
         throw new RuntimeException("");
      }
   }

   public boolean method4182(String string_1, String string_2, byte b_3) {
      string_1 = string_1.toLowerCase();
      string_2 = string_2.toLowerCase();
      int i_4 = this.field3164.method6513(class176.method3537(string_1, (byte) 30), -348250816);
      int i_5 = this.field3168[i_4].method6513(class176.method3537(string_2, (byte) 13), -218786835);
      return this.method4162(i_4, i_5, -2034642742);
   }

   void vmethod4264(int i_1, int i_2) {
   }

   public boolean method4164(int i_1, int i_2) {
      if (this.field3169[i_1] != null) {
         return true;
      } else {
         this.vmethod4279(i_1, 786262695);
         return this.field3169[i_1] != null;
      }
   }

   public boolean method4180(String string_1, String string_2, int i_3) {
      string_1 = string_1.toLowerCase();
      string_2 = string_2.toLowerCase();
      int i_4 = this.field3164.method6513(class176.method3537(string_1, (byte) 67), -198060564);
      if (i_4 < 0) {
         return false;
      } else {
         int i_5 = this.field3168[i_4].method6513(class176.method3537(string_2, (byte) 93), -889051712);
         return i_5 >= 0;
      }
   }

   public int method4206(String string_1, int i_2) {
      string_1 = string_1.toLowerCase();
      return this.field3164.method6513(class176.method3537(string_1, (byte) -63), -984559372);
   }

   public int method4235(int i_1, String string_2, int i_3) {
      string_2 = string_2.toLowerCase();
      return this.field3168[i_1].method6513(class176.method3537(string_2, (byte) 19), -235854541);
   }

   public boolean method4163(int i_1, short s_2) {
      if (this.field3170.length == 1) {
         return this.method4162(0, i_1, 1467902976);
      } else if (this.field3170[i_1].length == 1) {
         return this.method4162(i_1, 0, -1702133284);
      } else {
         throw new RuntimeException();
      }
   }

   boolean method4177(int i_1, int[] ints_2, byte b_3) {
      if (this.field3169[i_1] == null) {
         return false;
      } else {
         int i_4 = this.field3165[i_1];
         int[] ints_5 = this.field3166[i_1];
         Object[] arr_6 = this.field3170[i_1];
         boolean bool_7 = true;

         for (int i_8 = 0; i_8 < i_4; i_8++) {
            if (arr_6[ints_5[i_8]] == null) {
               bool_7 = false;
               break;
            }
         }

         if (bool_7) {
            return true;
         } else {
            byte[] bytes_22;
            if (ints_2 != null && (ints_2[0] != 0 || ints_2[1] != 0 || ints_2[2] != 0 || ints_2[3] != 0)) {
               bytes_22 = class18.method188(this.field3169[i_1], true, -2034592352);
               class310 class310_9 = new class310(bytes_22);
               class310_9.method5524(ints_2, 5, class310_9.field3752.length, -1521995814);
            } else {
               bytes_22 = class18.method188(this.field3169[i_1], false, -2034592352);
            }

            byte[] bytes_26 = class320.method5895(bytes_22, (byte) 1);
            if (this.field3173) {
               this.field3169[i_1] = null;
            }

            int i_11;
            if (i_4 > 1) {
               int i_10 = bytes_26.length;
               --i_10;
               i_11 = bytes_26[i_10] & 0xff;
               i_10 -= i_11 * i_4 * 4;
               class310 class310_12 = new class310(bytes_26);
               int[] ints_13 = new int[i_4];
               class310_12.field3751 = i_10;

               int i_15;
               int i_16;
               for (int i_14 = 0; i_14 < i_11; i_14++) {
                  i_15 = 0;

                  for (i_16 = 0; i_16 < i_4; i_16++) {
                     i_15 += class310_12.method5507(2031859009);
                     ints_13[i_16] += i_15;
                  }
               }

               byte[][] bytes_23 = new byte[i_4][];

               for (i_15 = 0; i_15 < i_4; i_15++) {
                  bytes_23[i_15] = new byte[ints_13[i_15]];
                  ints_13[i_15] = 0;
               }

               class310_12.field3751 = i_10;
               i_15 = 0;

               int i_18;
               for (i_16 = 0; i_16 < i_11; i_16++) {
                  int i_17 = 0;

                  for (i_18 = 0; i_18 < i_4; i_18++) {
                     i_17 += class310_12.method5507(-1009749813);
                     System.arraycopy(bytes_26, i_15, bytes_23[i_18], ints_13[i_18], i_17);
                     ints_13[i_18] += i_17;
                     i_15 += i_17;
                  }
               }

               for (i_16 = 0; i_16 < i_4; i_16++) {
                  if (!this.field3162) {
                     i_18 = ints_5[i_16];
                     byte[] bytes_20 = bytes_23[i_16];
                     Object obj_19;
                     if (bytes_20 == null) {
                        obj_19 = null;
                     } else if (bytes_20.length > 136) {
                        class215 class215_21 = new class215();
                        class215_21.vmethod3947(bytes_20, 108633452);
                        obj_19 = class215_21;
                     } else {
                        obj_19 = bytes_20;
                     }

                     arr_6[i_18] = obj_19;
                  } else {
                     arr_6[ints_5[i_16]] = bytes_23[i_16];
                  }
               }
            } else if (!this.field3162) {
               i_11 = ints_5[0];
               Object obj_27;
               if (bytes_26 == null) {
                  obj_27 = null;
               } else if (bytes_26.length > 136) {
                  class215 class215_28 = new class215();
                  class215_28.vmethod3947(bytes_26, 505970915);
                  obj_27 = class215_28;
               } else {
                  obj_27 = bytes_26;
               }

               arr_6[i_11] = obj_27;
            } else {
               arr_6[ints_5[0]] = bytes_26;
            }

            return true;
         }
      }
   }

   public byte[] method4181(String string_1, String string_2, int i_3) {
      string_1 = string_1.toLowerCase();
      string_2 = string_2.toLowerCase();
      int i_4 = this.field3164.method6513(class176.method3537(string_1, (byte) -24), -1937020348);
      int i_5 = this.field3168[i_4].method6513(class176.method3537(string_2, (byte) 70), -1093547316);
      return this.method4160(i_4, i_5, (short) -5483);
   }

   public boolean method4165(int i_1) {
      boolean bool_2 = true;

      for (int i_3 = 0; i_3 < this.field3160.length; i_3++) {
         int i_4 = this.field3160[i_3];
         if (this.field3169[i_4] == null) {
            this.vmethod4279(i_4, 786262695);
            if (this.field3169[i_4] == null) {
               bool_2 = false;
            }
         }
      }

      return bool_2;
   }

   public boolean method4183(String string_1, byte b_2) {
      string_1 = string_1.toLowerCase();
      int i_3 = this.field3164.method6513(class176.method3537(string_1, (byte) -39), -399403277);
      return this.method4164(i_3, 1210314032);
   }

   public int method4185(String string_1, short s_2) {
      string_1 = string_1.toLowerCase();
      int i_3 = this.field3164.method6513(class176.method3537(string_1, (byte) 107), -1576264497);
      return this.vmethod4270(i_3, 1303048942);
   }

   public void method4175(int i_1, int i_2) {
      for (int i_3 = 0; i_3 < this.field3170[i_1].length; i_3++) {
         this.field3170[i_1][i_3] = null;
      }

   }

   public void method4222(byte b_1) {
      for (int i_2 = 0; i_2 < this.field3169.length; i_2++) {
         this.field3169[i_2] = null;
      }

   }

   public void method4217(int i_1) {
      for (int i_2 = 0; i_2 < this.field3170.length; i_2++) {
         if (this.field3170[i_2] != null) {
            for (int i_3 = 0; i_3 < this.field3170[i_2].length; i_3++) {
               this.field3170[i_2][i_3] = null;
            }
         }
      }

   }

   public void method4184(String string_1, int i_2) {
      string_1 = string_1.toLowerCase();
      int i_3 = this.field3164.method6513(class176.method3537(string_1, (byte) -6), -1610554828);
      if (i_3 >= 0) {
         this.vmethod4264(i_3, -1807571765);
      }
   }

}
