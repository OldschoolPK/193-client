import java.util.Comparator;

public class class324 implements Comparator {

   final boolean field3860;

   public class324(boolean bool_1) {
      this.field3860 = bool_1;
   }

   int method5903(class284 class284_1, class284 class284_2, byte b_3) {
      return this.field3860 ? class284_1.vmethod5207(class284_2, -477933893) : class284_2.vmethod5207(class284_1, -1680318605);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method5903((class284) object_1, (class284) object_2, (byte) -72);
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

}
