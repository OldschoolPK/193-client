final class class4 implements class0 {

   static long field17;

   void method47(String string_1, class310 class310_2, byte b_3) {
      class310_2.method5492(string_1, -1748540705);
   }

   public Object vmethod48(class310 class310_1, byte b_2) {
      return class310_1.method5589(-1621249490);
   }

   public void vmethod49(Object object_1, class310 class310_2, int i_3) {
      this.method47((String) object_1, class310_2, (byte) 7);
   }

   public static void method53(int i_0) {
      class63 class63_1 = class63.field485;
      synchronized(class63.field485) {
         class63.field491 = class63.field494;
         class63.field492 = class63.field488;
         class63.field499 = class63.field505;
         class63.field501 = class63.field490;
         class63.field483 = class63.field495;
         class63.field489 = class63.field496;
         class63.field502 = class63.field497;
         class63.field500 = class63.field498;
         class63.field495 = 0;
      }
   }

   static final int method56(int i_0, int i_1, int i_2, byte b_3) {
      int i_4 = i_0 / i_2;
      int i_5 = i_0 & i_2 - 1;
      int i_6 = i_1 / i_2;
      int i_7 = i_1 & i_2 - 1;
      int i_8 = class36.method468(i_4, i_6, -327345288);
      int i_9 = class36.method468(i_4 + 1, i_6, -1097183689);
      int i_10 = class36.method468(i_4, i_6 + 1, -1224691474);
      int i_11 = class36.method468(i_4 + 1, i_6 + 1, 727531529);
      int i_13 = 65536 - class139.field1780[i_5 * 1024 / i_2] >> 1;
      int i_12 = ((65536 - i_13) * i_8 >> 16) + (i_13 * i_9 >> 16);
      int i_15 = 65536 - class139.field1780[i_5 * 1024 / i_2] >> 1;
      int i_14 = ((65536 - i_15) * i_10 >> 16) + (i_15 * i_11 >> 16);
      int i_17 = 65536 - class139.field1780[i_7 * 1024 / i_2] >> 1;
      int i_16 = ((65536 - i_17) * i_12 >> 16) + (i_17 * i_14 >> 16);
      return i_16;
   }

}
