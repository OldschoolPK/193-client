import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class class63 implements MouseListener, MouseMotionListener, FocusListener {

   static int field493;
   static int[] field487;
   static class63 field485 = new class63();
   static volatile int field486 = 0;
   static volatile int field494 = 0;
   static volatile int field488 = -1;
   static volatile int field505 = -1;
   static volatile long field490 = -1L;
   public static int field491 = 0;
   public static int field492 = 0;
   public static int field499 = 0;
   public static long field501 = 0L;
   static volatile int field495 = 0;
   static volatile int field496 = 0;
   static volatile int field497 = 0;
   static volatile long field498 = 0L;
   public static int field483 = 0;
   public static int field489 = 0;
   public static int field502 = 0;
   public static long field500 = 0L;

   public final synchronized void mouseMoved(MouseEvent mouseevent_1) {
      if (field485 != null) {
         field486 = 0;
         field488 = mouseevent_1.getX();
         field505 = mouseevent_1.getY();
         field490 = mouseevent_1.getWhen();
      }

   }

   final int method1134(MouseEvent mouseevent_1, int i_2) {
      int i_3 = mouseevent_1.getButton();
      return !mouseevent_1.isAltDown() && i_3 != 2 ? (!mouseevent_1.isMetaDown() && i_3 != 3 ? 1 : 2) : 4;
   }

   public final synchronized void focusLost(FocusEvent focusevent_1) {
      if (field485 != null) {
         field494 = 0;
      }

   }

   public final synchronized void mousePressed(MouseEvent mouseevent_1) {
      if (field485 != null) {
         field486 = 0;
         field496 = mouseevent_1.getX();
         field497 = mouseevent_1.getY();
         field498 = class298.method5270(255749540);
         field495 = this.method1134(mouseevent_1, -2088746983);
         if (field495 != 0) {
            field494 = field495;
         }
      }

      if (mouseevent_1.isPopupTrigger()) {
         mouseevent_1.consume();
      }

   }

   public final void mouseClicked(MouseEvent mouseevent_1) {
      if (mouseevent_1.isPopupTrigger()) {
         mouseevent_1.consume();
      }

   }

   public final synchronized void mouseExited(MouseEvent mouseevent_1) {
      if (field485 != null) {
         field486 = 0;
         field488 = -1;
         field505 = -1;
         field490 = mouseevent_1.getWhen();
      }

   }

   public final synchronized void mouseReleased(MouseEvent mouseevent_1) {
      if (field485 != null) {
         field486 = 0;
         field494 = 0;
      }

      if (mouseevent_1.isPopupTrigger()) {
         mouseevent_1.consume();
      }

   }

   public final void focusGained(FocusEvent focusevent_1) {
   }

   public final synchronized void mouseEntered(MouseEvent mouseevent_1) {
      this.mouseMoved(mouseevent_1);
   }

   public final synchronized void mouseDragged(MouseEvent mouseevent_1) {
      this.mouseMoved(mouseevent_1);
   }

   public static void method1157(class244 class244_0, int i_1) {
      class268.field3543 = class244_0;
   }

   static int method1156(int i_0, int i_1) {
      return (int)((Math.log((double)i_0) / class85.field1093 - 7.0D) * 256.0D);
   }

   static boolean method1150(int i_0) {
      return (client.field768 & 0x8) != 0;
   }

}
