public final class class132 {

   public class144 field1628;
   int field1625;
   public long field1629;
   int field1627;
   int field1632;
   int field1630;

   public static int method2906(String string_0, byte b_1) {
      return string_0.length() + 1;
   }

   static final void method2904(String string_0, int i_1) {
      class196 class196_2 = class68.method1249(class192.field2241, client.field694.field1328, (byte) 1);
      class196_2.field2360.method5482(method2906(string_0, (byte) 45), (byte) -102);
      class196_2.field2360.method5492(string_0, -1748540705);
      client.field694.method2295(class196_2, -794081522);
   }

   static final void method2905(class75 class75_0, int i_1, int i_2, int i_3, int i_4) {
      if (class223.field2562 != class75_0) {
         if (client.field785 < 400) {
            String string_5;
            int i_8;
            if (class75_0.field612 == 0) {
               String string_6 = class75_0.field610[0] + class75_0.field619 + class75_0.field610[1];
               i_8 = class75_0.field611;
               int i_9 = class223.field2562.field611;
               int i_10 = i_9 - i_8;
               String string_7;
               if (i_10 < -9) {
                  string_7 = class23.method222(16711680, (byte) 63);
               } else if (i_10 < -6) {
                  string_7 = class23.method222(16723968, (byte) 126);
               } else if (i_10 < -3) {
                  string_7 = class23.method222(16740352, (byte) 77);
               } else if (i_10 < 0) {
                  string_7 = class23.method222(16756736, (byte) 99);
               } else if (i_10 > 9) {
                  string_7 = class23.method222(65280, (byte) 61);
               } else if (i_10 > 6) {
                  string_7 = class23.method222(4259584, (byte) 17);
               } else if (i_10 > 3) {
                  string_7 = class23.method222(8453888, (byte) 75);
               } else if (i_10 > 0) {
                  string_7 = class23.method222(12648192, (byte) 107);
               } else {
                  string_7 = class23.method222(16776960, (byte) 38);
               }

               string_5 = string_6 + string_7 + " " + " (" + "level-" + class75_0.field611 + ")" + class75_0.field610[2];
            } else {
               string_5 = class75_0.field610[0] + class75_0.field619 + class75_0.field610[1] + " " + " (" + "skill-" + class75_0.field612 + ")" + class75_0.field610[2];
            }

            int i_11;
            if (client.field801 == 1) {
               class38.method564("Use", client.field744 + " " + "->" + " " + class23.method222(16777215, (byte) 123) + string_5, 14, i_1, i_2, i_3, -2061981388);
            } else if (client.field803) {
               if ((class75.field637 & 0x8) == 8) {
                  class38.method564(client.field851, client.field807 + " " + "->" + " " + class23.method222(16777215, (byte) 67) + string_5, 15, i_1, i_2, i_3, -1253011578);
               }
            } else {
               for (i_11 = 7; i_11 >= 0; --i_11) {
                  if (client.field772[i_11] != null) {
                     short s_12 = 0;
                     if (client.field772[i_11].equalsIgnoreCase("Attack")) {
                        if (class93.field1177 == client.field671) {
                           continue;
                        }

                        if (client.field671 == class93.field1175 || class93.field1176 == client.field671 && class75_0.field611 > class223.field2562.field611) {
                           s_12 = 2000;
                        }

                        if (class223.field2562.field616 != 0 && class75_0.field616 != 0) {
                           if (class75_0.field616 == class223.field2562.field616) {
                              s_12 = 2000;
                           } else {
                              s_12 = 0;
                           }
                        }
                     } else if (client.field773[i_11]) {
                        s_12 = 2000;
                     }

                     boolean bool_13 = false;
                     i_8 = client.field771[i_11] + s_12;
                     class38.method564(client.field772[i_11], class23.method222(16777215, (byte) 93) + string_5, i_8, i_1, i_2, i_3, -1505275529);
                  }
               }
            }

            for (i_11 = 0; i_11 < client.field785; i_11++) {
               if (client.field844[i_11] == 23) {
                  client.field791[i_11] = class23.method222(16777215, (byte) 13) + string_5;
                  break;
               }
            }

         }
      }
   }

   static void method2903(int i_0, String string_1, byte b_2) {
      int i_3 = class98.field1263;
      int[] ints_4 = class98.field1268;
      boolean bool_5 = false;
      class293 class293_6 = new class293(string_1, class19.field92);

      for (int i_7 = 0; i_7 < i_3; i_7++) {
         class75 class75_8 = client.field764[ints_4[i_7]];
         if (class75_8 != null && class75_8 != class223.field2562 && class75_8.field619 != null && class75_8.field619.equals(class293_6)) {
            class196 class196_9;
            if (i_0 == 1) {
               class196_9 = class68.method1249(class192.field2260, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5528(0, 1712020733);
               class196_9.field2360.method5721(ints_4[i_7], 1133202553);
               client.field694.method2295(class196_9, 1782146536);
            } else if (i_0 == 4) {
               class196_9 = class68.method1249(class192.field2312, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5721(ints_4[i_7], 1133202553);
               class196_9.field2360.method5528(0, 1712020733);
               client.field694.method2295(class196_9, -1683383484);
            } else if (i_0 == 6) {
               class196_9 = class68.method1249(class192.field2242, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5528(0, 1712020733);
               class196_9.field2360.method5538(ints_4[i_7], (short) -29605);
               client.field694.method2295(class196_9, -1135471203);
            } else if (i_0 == 7) {
               class196_9 = class68.method1249(class192.field2268, client.field694.field1328, (byte) 1);
               class196_9.field2360.method5538(ints_4[i_7], (short) -14252);
               class196_9.field2360.method5530(0, -1493586164);
               client.field694.method2295(class196_9, -514743511);
            }

            bool_5 = true;
            break;
         }
      }

      if (!bool_5) {
         class234.method4129(4, "", "Unable to find " + string_1, (byte) 48);
      }

   }

}
