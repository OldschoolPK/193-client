public class class72 implements Runnable {

   static byte[][] field590;
   boolean field586 = true;
   Object field588 = new Object();
   int field591 = 0;
   int[] field587 = new int[500];
   int[] field589 = new int[500];
   long[] field585 = new long[500];

   public void run() {
      for (; this.field586; class236.method4132(50L)) {
         Object object_1 = this.field588;
         synchronized(this.field588) {
            if (this.field591 < 500) {
               this.field587[this.field591] = class63.field492;
               this.field589[this.field591] = class63.field499;
               this.field585[this.field591] = class63.field501;
               ++this.field591;
            }
         }
      }

   }

   static void method1264(int i_0) {
      class196 class196_1 = class68.method1249(class192.field2273, client.field694.field1328, (byte) 1);
      class196_1.field2360.method5482(class116.method2537((byte) 12), (byte) -20);
      class196_1.field2360.method5486(class286.field3638, (byte) -37);
      class196_1.field2360.method5486(class143.field1892, (byte) -123);
      client.field694.method2295(class196_1, -1829999495);
   }

   static final void method1262(class70 class70_0, boolean bool_1, int i_2) {
      int i_3 = class70_0.field563;
      int i_4 = (int)class70_0.field2137;
      class70_0.method3628();
      if (bool_1 && i_3 != -1 && class195.field2355[i_3]) {
         class58.field427.method4175(i_3, 176740011);
         if (class9.field44[i_3] != null) {
            boolean bool_7 = true;

            for (int i_6 = 0; i_6 < class9.field44[i_3].length; i_6++) {
               if (class9.field44[i_3][i_6] != null) {
                  if (class9.field44[i_3][i_6].field2588 != 2) {
                     class9.field44[i_3][i_6] = null;
                  } else {
                     bool_7 = false;
                  }
               }
            }

            if (bool_7) {
               class9.field44[i_3] = null;
            }

            class195.field2355[i_3] = false;
         }
      }

      class38.method589(i_3, -1135522977);
      class226 class226_5 = class181.method3610(i_4, -1342440470);
      if (class226_5 != null) {
         class181.method3609(class226_5, 1307854475);
      }

      class48.method767(155276335);
      if (client.field873 != -1) {
         class31.method330(client.field873, 1, 1039727263);
      }

   }

   static final void method1265(int i_0, int i_1, int i_2) {
      class272 class272_3 = client.field776[class151.field1949][i_0][i_1];
      if (class272_3 == null) {
         class67.field536.method3152(class151.field1949, i_0, i_1);
      } else {
         long long_4 = -99999999L;
         class96 class96_6 = null;

         class96 class96_7;
         for (class96_7 = (class96) class272_3.method4879(); class96_7 != null; class96_7 = (class96) class272_3.method4884()) {
            class265 class265_8 = class66.method1223(class96_7.field1238, 97561494);
            long long_9 = (long)class265_8.field3477;
            if (class265_8.field3455 == 1) {
               long_9 *= (long)(class96_7.field1239 + 1);
            }

            if (long_9 > long_4) {
               long_4 = long_9;
               class96_6 = class96_7;
            }
         }

         if (class96_6 == null) {
            class67.field536.method3152(class151.field1949, i_0, i_1);
         } else {
            class272_3.method4878(class96_6);
            class96 class96_13 = null;
            class96 class96_12 = null;

            for (class96_7 = (class96) class272_3.method4879(); class96_7 != null; class96_7 = (class96) class272_3.method4884()) {
               if (class96_6.field1238 != class96_7.field1238) {
                  if (class96_13 == null) {
                     class96_13 = class96_7;
                  }

                  if (class96_13.field1238 != class96_7.field1238 && class96_12 == null) {
                     class96_12 = class96_7;
                  }
               }
            }

            long long_10 = class88.method2085(i_0, i_1, 3, false, 0, (byte) -40);
            class67.field536.method3242(class151.field1949, i_0, i_1, class97.method2165(i_0 * 128 + 64, i_1 * 128 + 64, class151.field1949, 2015702543), class96_6, long_10, class96_13, class96_12);
         }
      }
   }

   static int method1266(class80 class80_0, class80 class80_1, int i_2, boolean bool_3, int i_4) {
      if (i_2 == 1) {
         int i_5 = class80_0.field1020;
         int i_6 = class80_1.field1020;
         if (!bool_3) {
            if (i_5 == -1) {
               i_5 = 2001;
            }

            if (i_6 == -1) {
               i_6 = 2001;
            }
         }

         return i_5 - i_6;
      } else {
         return i_2 == 2 ? class80_0.field1023 - class80_1.field1023 : (i_2 == 3 ? (class80_0.field1022.equals("-") ? (class80_1.field1022.equals("-") ? 0 : (bool_3 ? -1 : 1)) : (class80_1.field1022.equals("-") ? (bool_3 ? 1 : -1) : class80_0.field1022.compareTo(class80_1.field1022))) : (i_2 == 4 ? (class80_0.method1803(-1688533510) ? (class80_1.method1803(-2040946968) ? 0 : 1) : (class80_1.method1803(-1678443991) ? -1 : 0)) : (i_2 == 5 ? (class80_0.method1801(-2039082864) ? (class80_1.method1801(-2039082864) ? 0 : 1) : (class80_1.method1801(-2039082864) ? -1 : 0)) : (i_2 == 6 ? (class80_0.method1802(-1147025288) ? (class80_1.method1802(-1900317072) ? 0 : 1) : (class80_1.method1802(-1414396588) ? -1 : 0)) : (i_2 == 7 ? (class80_0.method1800((byte) 1) ? (class80_1.method1800((byte) 1) ? 0 : 1) : (class80_1.method1800((byte) 1) ? -1 : 0)) : class80_0.field1018 - class80_1.field1018)))));
      }
   }

}
