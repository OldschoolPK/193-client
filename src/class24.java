import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.RandomAccessFile;
import java.io.StreamCorruptedException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.SecureRandom;
//import netscape.javascript.JSObject;

public class class24 extends class30 {

   static SecureRandom field114;
   static int field112;

   void method234(class310 class310_1, int i_2) {
      int i_3 = class310_1.method5661((byte) 54);
      if (i_3 != class40.field292.field291) {
         throw new IllegalStateException("");
      } else {
         super.field181 = class310_1.method5661((byte) -5);
         super.field189 = class310_1.method5661((byte) 12);
         super.field187 = class310_1.method5729(1744855794);
         super.field182 = class310_1.method5729(729501472);
         super.field180 = class310_1.method5729(-969178893);
         super.field188 = class310_1.method5729(524239753);
         super.field183 = class310_1.method5519((short) 1005);
         super.field184 = class310_1.method5519((short) 1005);
      }
   }

   void vmethod745(class310 class310_1, int i_2) {
      super.field189 = Math.min(super.field189, 4);
      super.field185 = new short[1][64][64];
      super.field186 = new short[super.field189][64][64];
      super.field178 = new byte[super.field189][64][64];
      super.field177 = new byte[super.field189][64][64];
      super.field179 = new class34[super.field189][64][64][];
      int i_3 = class310_1.method5661((byte) -14);
      if (i_3 != class39.field278.field280) {
         throw new IllegalStateException("");
      } else {
         int i_4 = class310_1.method5661((byte) -10);
         int i_5 = class310_1.method5661((byte) -83);
         if (i_4 == super.field180 && i_5 == super.field188) {
            for (int i_6 = 0; i_6 < 64; i_6++) {
               for (int i_7 = 0; i_7 < 64; i_7++) {
                  this.method305(i_6, i_7, class310_1, -734499768);
               }
            }

         } else {
            throw new IllegalStateException("");
         }
      }
   }

   public boolean equals(Object object_1) {
      if (!(object_1 instanceof class24)) {
         return false;
      } else {
         class24 class24_2 = (class24) object_1;
         return class24_2.field180 == super.field180 && super.field188 == class24_2.field188;
      }
   }

   public int hashCode() {
      return super.field180 | super.field188 << 8;
   }

   public static boolean method247(char var_0, int i_1) {
      if ((var_0 <= 0 || var_0 >= 128) && (var_0 < 160 || var_0 > 255)) {
         if (var_0 != 0) {
            char[] arr_2 = class298.field3688;

            for (int i_3 = 0; i_3 < arr_2.length; i_3++) {
               char var_4 = arr_2[i_3];
               if (var_0 == var_4) {
                  return true;
               }
            }
         }

         return false;
      } else {
         return true;
      }
   }

   static void method242(int i_0, int i_1) {
      if (i_0 == -1 && !client.field883) {
         class38.method588(-1543260238);
      } else if (i_0 != -1 && i_0 != client.field779 && class282.field3617.field1056 != 0 && !client.field883) {
         class66.method1221(2, class217.field2520, i_0, 0, class282.field3617.field1056, false, 141166757);
      }

      client.field779 = i_0;
   }

   static final void method246(boolean bool_0, class309 class309_1, int i_2) {
      client.field769 = 0;
      client.field826 = 0;
      class309 class309_3 = client.field694.field1325;
      class309_3.method5454(1312033135);
      int i_4 = class309_3.method5455(8, (byte) -84);
      int i_5;
      if (i_4 < client.field690) {
         for (i_5 = i_4; i_5 < client.field690; i_5++) {
            client.field770[++client.field769 - 1] = client.field691[i_5];
         }
      }

      if (i_4 > client.field690) {
         throw new RuntimeException("");
      } else {
         client.field690 = 0;

         int i_6;
         int i_8;
         int i_9;
         int i_10;
         int i_11;
         int i_12;
         for (i_5 = 0; i_5 < i_4; i_5++) {
            i_6 = client.field691[i_5];
            class88 class88_7 = client.field689[i_6];
            i_8 = class309_3.method5455(1, (byte) -52);
            if (i_8 == 0) {
               client.field691[++client.field690 - 1] = i_6;
               class88_7.field987 = client.field655;
            } else {
               i_9 = class309_3.method5455(2, (byte) -96);
               if (i_9 == 0) {
                  client.field691[++client.field690 - 1] = i_6;
                  class88_7.field987 = client.field655;
                  client.field654[++client.field826 - 1] = i_6;
               } else if (i_9 == 1) {
                  client.field691[++client.field690 - 1] = i_6;
                  class88_7.field987 = client.field655;
                  i_10 = class309_3.method5455(3, (byte) -29);
                  class88_7.method2074(i_10, (byte) 1, 2110230609);
                  i_11 = class309_3.method5455(1, (byte) -99);
                  if (i_11 == 1) {
                     client.field654[++client.field826 - 1] = i_6;
                  }
               } else if (i_9 == 2) {
                  client.field691[++client.field690 - 1] = i_6;
                  class88_7.field987 = client.field655;
                  i_10 = class309_3.method5455(3, (byte) -126);
                  class88_7.method2074(i_10, (byte) 2, 1912112100);
                  i_11 = class309_3.method5455(3, (byte) -107);
                  class88_7.method2074(i_11, (byte) 2, 1570104798);
                  i_12 = class309_3.method5455(1, (byte) -58);
                  if (i_12 == 1) {
                     client.field654[++client.field826 - 1] = i_6;
                  }
               } else if (i_9 == 3) {
                  client.field770[++client.field769 - 1] = i_6;
               }
            }
         }

         class201.method3683(bool_0, class309_1, (byte) 57);

         int i_15;
         for (i_15 = 0; i_15 < client.field826; i_15++) {
            i_4 = client.field654[i_15];
            class88 class88_16 = client.field689[i_4];
            i_6 = class309_1.method5661((byte) 86);
            int i_17;
            if ((i_6 & 0x8) != 0) {
               class88_16.field975 = class309_1.method5499(-1597569596);
               i_17 = class309_1.method5507(1832730277);
               class88_16.field979 = i_17 >> 16;
               class88_16.field978 = (i_17 & 0xffff) + client.field655;
               class88_16.field976 = 0;
               class88_16.field977 = 0;
               if (class88_16.field978 > client.field655) {
                  class88_16.field976 = -1;
               }

               if (class88_16.field975 == 65535) {
                  class88_16.field975 = -1;
               }
            }

            if ((i_6 & 0x2) != 0) {
               i_17 = class309_1.method5542(468907616);
               i_8 = class309_1.method5499(-1597569596);
               i_9 = class88_16.field990 - (i_17 - class243.field3156 - class243.field3156) * 64;
               i_10 = class88_16.field938 - (i_8 - class41.field300 - class41.field300) * 64;
               if (i_9 != 0 || i_10 != 0) {
                  class88_16.field959 = (int)(Math.atan2((double)i_9, (double)i_10) * 325.949D) & 0x7ff;
               }
            }

            if ((i_6 & 0x10) != 0) {
               class88_16.field980 = class309_1.method5540(-1856185425);
               if (class88_16.field980 == 65535) {
                  class88_16.field980 = -1;
               }
            }

            if ((i_6 & 0x20) != 0) {
               class88_16.field951 = class309_1.method5589(-978418235);
               class88_16.field991 = 100;
            }

            if ((i_6 & 0x1) != 0) {
               class88_16.field1135 = class27.method272(class309_1.method5540(-1856185425), -1562808952);
               class88_16.field941 = class88_16.field1135.field3500;
               class88_16.field940 = class88_16.field1135.field3523;
               class88_16.field946 = class88_16.field1135.field3525;
               class88_16.field947 = class88_16.field1135.field3507;
               class88_16.field948 = class88_16.field1135.field3508;
               class88_16.field949 = class88_16.field1135.field3503;
               class88_16.field943 = class88_16.field1135.field3506;
               class88_16.field944 = class88_16.field1135.field3504;
               class88_16.field945 = class88_16.field1135.field3505;
            }

            if ((i_6 & 0x40) != 0) {
               i_17 = class309_1.method5542(468907616);
               if (i_17 == 65535) {
                  i_17 = -1;
               }

               i_8 = class309_1.method5661((byte) -92);
               if (i_17 == class88_16.field970 && i_17 != -1) {
                  i_9 = class260.method4510(i_17, -1411174483).field3548;
                  if (i_9 == 1) {
                     class88_16.field971 = 0;
                     class88_16.field972 = 0;
                     class88_16.field937 = i_8;
                     class88_16.field974 = 0;
                  }

                  if (i_9 == 2) {
                     class88_16.field974 = 0;
                  }
               } else if (i_17 == -1 || class88_16.field970 == -1 || class260.method4510(i_17, -1809212065).field3557 >= class260.method4510(class88_16.field970, -1998918780).field3557) {
                  class88_16.field970 = i_17;
                  class88_16.field971 = 0;
                  class88_16.field972 = 0;
                  class88_16.field937 = i_8;
                  class88_16.field974 = 0;
                  class88_16.field996 = class88_16.field985;
               }
            }

            if ((i_6 & 0x4) != 0) {
               i_17 = class309_1.method5531(1416689634);
               int i_13;
               if (i_17 > 0) {
                  for (i_8 = 0; i_8 < i_17; i_8++) {
                     i_10 = -1;
                     i_11 = -1;
                     i_12 = -1;
                     i_9 = class309_1.method5483(772809583);
                     if (i_9 == 32767) {
                        i_9 = class309_1.method5483(772809583);
                        i_11 = class309_1.method5483(772809583);
                        i_10 = class309_1.method5483(772809583);
                        i_12 = class309_1.method5483(772809583);
                     } else if (i_9 != 32766) {
                        i_11 = class309_1.method5483(772809583);
                     } else {
                        i_9 = -1;
                     }

                     i_13 = class309_1.method5483(772809583);
                     class88_16.method1778(i_9, i_11, i_10, i_12, client.field655, i_13, 794886398);
                  }
               }

               i_8 = class309_1.method5661((byte) 2);
               if (i_8 > 0) {
                  for (i_9 = 0; i_9 < i_8; i_9++) {
                     i_10 = class309_1.method5483(772809583);
                     i_11 = class309_1.method5483(772809583);
                     if (i_11 != 32767) {
                        i_12 = class309_1.method5483(772809583);
                        i_13 = class309_1.method5532(-1946834755);
                        int i_14 = i_11 > 0 ? class309_1.method5532(-1946834755) : i_13;
                        class88_16.method1787(i_10, client.field655, i_11, i_12, i_13, i_14, -1554709476);
                     } else {
                        class88_16.method1779(i_10, -324524490);
                     }
                  }
               }
            }

            if ((i_6 & 0x80) != 0) {
               class88_16.field981 = class309_1.method5605(-498797750);
               class88_16.field982 = class309_1.method5605(-603042415);
               class88_16.field958 = class309_1.method5605(-1837456490);
               class88_16.field956 = class309_1.method5605(-615363600);
               class88_16.field984 = class309_1.method5729(-1458492297) + client.field655;
               class88_16.field973 = class309_1.method5542(468907616) + client.field655;
               class88_16.field986 = class309_1.method5729(679718235);
               class88_16.field985 = 1;
               class88_16.field996 = 0;
               class88_16.field981 += class88_16.field993[0];
               class88_16.field982 += class88_16.field968[0];
               class88_16.field958 += class88_16.field993[0];
               class88_16.field956 += class88_16.field968[0];
            }
         }

         for (i_15 = 0; i_15 < client.field769; i_15++) {
            i_4 = client.field770[i_15];
            if (client.field689[i_4].field987 != client.field655) {
               client.field689[i_4].field1135 = null;
               client.field689[i_4] = null;
            }
         }

         if (class309_1.field3751 != client.field694.field1338) {
            throw new RuntimeException(class309_1.field3751 + "," + client.field694.field1338);
         } else {
            for (i_15 = 0; i_15 < client.field690; i_15++) {
               if (client.field689[client.field691[i_15]] == null) {
                  throw new RuntimeException(i_15 + "," + client.field690);
               }
            }

         }
      }
   }

   static final void method243(int i_0, int i_1, byte b_2) {
      if (client.field785 >= 2 || client.field801 != 0 || client.field803) {
         if (client.field796) {
            int i_3 = class36.method463(646610765);
            String string_4;
            if (client.field801 == 1 && client.field785 < 2) {
               string_4 = "Use" + " " + client.field744 + " " + "->";
            } else if (client.field803 && client.field785 < 2) {
               string_4 = client.field851 + " " + client.field807 + " " + "->";
            } else {
               string_4 = class67.method1230(i_3, 636889033);
            }

            if (client.field785 > 2) {
               string_4 = string_4 + class23.method222(16777215, (byte) 114) + " " + '/' + " " + (client.field785 - 2) + " more options";
            }

            class17.field74.method5389(string_4, i_0 + 4, i_1 + 15, 16777215, 0, client.field655 / 1000);
         }
      }
   }

   public static void method233(class309 class309_0, short s_1) {
      class106 class106_2 = (class106) class105.field1342.method4806();
      if (class106_2 != null) {
         int i_3 = class309_0.field3751;
         class309_0.method5510(class106_2.field1354, (byte) -55);

         for (int i_4 = 0; i_4 < class106_2.field1348; i_4++) {
            if (class106_2.field1350[i_4] != 0) {
               class309_0.method5482(class106_2.field1350[i_4], (byte) -123);
            } else {
               try {
                  int i_5 = class106_2.field1347[i_4];
                  Field field_6;
                  int i_7;
                  if (i_5 == 0) {
                     field_6 = class106_2.field1351[i_4];
                     i_7 = field_6.getInt((Object) null);
                     class309_0.method5482(0, (byte) -117);
                     class309_0.method5510(i_7, (byte) -27);
                  } else if (i_5 == 1) {
                     field_6 = class106_2.field1351[i_4];
                     field_6.setInt((Object) null, class106_2.field1352[i_4]);
                     class309_0.method5482(0, (byte) -53);
                  } else if (i_5 == 2) {
                     field_6 = class106_2.field1351[i_4];
                     i_7 = field_6.getModifiers();
                     class309_0.method5482(0, (byte) -30);
                     class309_0.method5510(i_7, (byte) -110);
                  }

                  Method method_26;
                  if (i_5 != 3) {
                     if (i_5 == 4) {
                        method_26 = class106_2.field1353[i_4];
                        i_7 = method_26.getModifiers();
                        class309_0.method5482(0, (byte) -118);
                        class309_0.method5510(i_7, (byte) -126);
                     }
                  } else {
                     method_26 = class106_2.field1353[i_4];
                     byte[][] bytes_11 = class106_2.field1349[i_4];
                     Object[] arr_8 = new Object[bytes_11.length];

                     for (int i_9 = 0; i_9 < bytes_11.length; i_9++) {
                        ObjectInputStream objectinputstream_10 = new ObjectInputStream(new ByteArrayInputStream(bytes_11[i_9]));
                        arr_8[i_9] = objectinputstream_10.readObject();
                     }

                     Object object_12 = method_26.invoke((Object) null, arr_8);
                     if (object_12 == null) {
                        class309_0.method5482(0, (byte) -77);
                     } else if (object_12 instanceof Number) {
                        class309_0.method5482(1, (byte) -41);
                        class309_0.method5490(((Number) object_12).longValue());
                     } else if (object_12 instanceof String) {
                        class309_0.method5482(2, (byte) -93);
                        class309_0.method5492((String) object_12, -1748540705);
                     } else {
                        class309_0.method5482(4, (byte) -44);
                     }
                  }
               } catch (ClassNotFoundException classnotfoundexception_14) {
                  class309_0.method5482(-10, (byte) -78);
               } catch (InvalidClassException invalidclassexception_15) {
                  class309_0.method5482(-11, (byte) -77);
               } catch (StreamCorruptedException streamcorruptedexception_16) {
                  class309_0.method5482(-12, (byte) -104);
               } catch (OptionalDataException optionaldataexception_17) {
                  class309_0.method5482(-13, (byte) -30);
               } catch (IllegalAccessException illegalaccessexception_18) {
                  class309_0.method5482(-14, (byte) -82);
               } catch (IllegalArgumentException illegalargumentexception_19) {
                  class309_0.method5482(-15, (byte) -10);
               } catch (InvocationTargetException invocationtargetexception_20) {
                  class309_0.method5482(-16, (byte) -108);
               } catch (SecurityException securityexception_21) {
                  class309_0.method5482(-17, (byte) -6);
               } catch (IOException ioexception_22) {
                  class309_0.method5482(-18, (byte) -50);
               } catch (NullPointerException nullpointerexception_23) {
                  class309_0.method5482(-19, (byte) -87);
               } catch (Exception exception_24) {
                  class309_0.method5482(-20, (byte) -37);
               } catch (Throwable throwable_25) {
                  class309_0.method5482(-21, (byte) -116);
               }
            }
         }

         class309_0.method5526(i_3, 1467040648);
         class106_2.method3628();
      }
   }

   public static void method237(int i_0) {
      try {
         File file_1 = new File(class129.field1559, "random.dat");
         int i_3;
         if (file_1.exists()) {
            class176.field2065 = new class352(new class353(file_1, "rw", 25L), 24, 0);
         } else {
            label37:
            for (int i_2 = 0; i_2 < class102.field1318.length; i_2++) {
               for (i_3 = 0; i_3 < class86.field1117.length; i_3++) {
                  File file_4 = new File(class86.field1117[i_3] + class102.field1318[i_2] + File.separatorChar + "random.dat");
                  if (file_4.exists()) {
                     class176.field2065 = new class352(new class353(file_4, "rw", 25L), 24, 0);
                     break label37;
                  }
               }
            }
         }

         if (class176.field2065 == null) {
            RandomAccessFile randomaccessfile_5 = new RandomAccessFile(file_1, "rw");
            i_3 = randomaccessfile_5.read();
            randomaccessfile_5.seek(0L);
            randomaccessfile_5.write(i_3);
            randomaccessfile_5.seek(0L);
            randomaccessfile_5.close();
            class176.field2065 = new class352(new class353(file_1, "rw", 25L), 24, 0);
         }
      } catch (IOException ioexception_6) {
         ;
      }

   }

   static void method244(String string_0, byte b_1) {
      class42.field317 = string_0;

      try {
         String string_2 = class27.field147.getParameter(Integer.toString(18));
         String string_3 = class27.field147.getParameter(Integer.toString(13));
         String string_4 = string_2 + "settings=" + string_0 + "; version=1; path=/; domain=" + string_3;
         if (string_0.length() == 0) {
            string_4 = string_4 + "; Expires=Thu, 01-Jan-1970 00:00:00 GMT; Max-Age=0";
         } else {
            string_4 = string_4 + "; Expires=" + class3.method40(class298.method5270(255749540) + 94608000000L) + "; Max-Age=" + 94608000L;
         }

         client client_5 = class27.field147;
         String string_6 = "document.cookie=\"" + string_4 + "\"";
         //yJSObject.getWindow(client_5).eval(string_6);
      } catch (Throwable throwable_7) {
         ;
      }

   }

   static int method245(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      if (i_0 == 3200) {
         class253.field3267 -= 3;
         class232.method4123(class85.field1095[class253.field3267], class85.field1095[class253.field3267 + 1], class85.field1095[class253.field3267 + 2], 1164340551);
         return 1;
      } else if (i_0 == 3201) {
         method242(class85.field1095[--class253.field3267], 722310732);
         return 1;
      } else if (i_0 == 3202) {
         class253.field3267 -= 2;
         class193.method3643(class85.field1095[class253.field3267], class85.field1095[class253.field3267 + 1], (byte) -40);
         return 1;
      } else {
         int i_4;
         if (i_0 == 3203) {
            i_4 = Math.min(Math.max(class85.field1095[--class253.field3267], 0), 100);
            class50.method810(Math.round((float)i_4 * 2.55F), 1393718801);
            return 1;
         } else if (i_0 == 3204) {
            class85.field1095[++class253.field3267 - 1] = Math.round((float)class282.field3617.field1056 / 2.55F);
            return 1;
         } else if (i_0 == 3205) {
            i_4 = Math.min(Math.max(class85.field1095[--class253.field3267], 0), 100);
            class44.method662(Math.round((float)i_4 * 1.27F), -1435995968);
            return 1;
         } else if (i_0 == 3206) {
            class85.field1095[++class253.field3267 - 1] = Math.round((float)class282.field3617.field1059 / 1.27F);
            return 1;
         } else if (i_0 == 3207) {
            i_4 = Math.min(Math.max(class85.field1095[--class253.field3267], 0), 100);
            class194.method3647(Math.round(1.27F * (float)i_4), -1005104193);
            return 1;
         } else if (i_0 == 3208) {
            class85.field1095[++class253.field3267 - 1] = Math.round((float)class282.field3617.field1060 / 1.27F);
            return 1;
         } else {
            return 2;
         }
      }
   }

}
