import java.util.Comparator;

public class class160 implements Comparator {

   public static boolean field1988;
   final boolean field1987;

   public class160(boolean bool_1) {
      this.field1987 = bool_1;
   }

   int method3440(class289 class289_1, class289 class289_2, int i_3) {
      return this.field1987 ? class289_1.field3649 - class289_2.field3649 : class289_2.field3649 - class289_1.field3649;
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3440((class289) object_1, (class289) object_2, 1051999362);
   }

   public static class147 method3442(class244 class244_0, class244 class244_1, int i_2, boolean bool_3, int i_4) {
      boolean bool_5 = true;
      int[] ints_6 = class244_0.method4171(i_2, -956683932);

      for (int i_7 = 0; i_7 < ints_6.length; i_7++) {
         byte[] bytes_8 = class244_0.method4245(i_2, ints_6[i_7], 2110561538);
         if (bytes_8 == null) {
            bool_5 = false;
         } else {
            int i_9 = (bytes_8[0] & 0xff) << 8 | bytes_8[1] & 0xff;
            byte[] bytes_10;
            if (bool_3) {
               bytes_10 = class244_1.method4245(0, i_9, 867847917);
            } else {
               bytes_10 = class244_1.method4245(i_9, 0, -1562488620);
            }

            if (bytes_10 == null) {
               bool_5 = false;
            }
         }
      }

      if (!bool_5) {
         return null;
      } else {
         try {
            return new class147(class244_0, class244_1, i_2, bool_3);
         } catch (Exception exception_12) {
            return null;
         }
      }
   }

   static final void method3437(class194 class194_0, int i_1) {
      class309 class309_2 = client.field694.field1325;
      int i_3;
      int i_4;
      int i_5;
      int i_6;
      int i_7;
      int i_8;
      int i_9;
      int i_10;
      int i_11;
      if (class194.field2342 == class194_0) {
         i_3 = class309_2.method5542(468907616);
         i_4 = class309_2.method5531(1106264368);
         i_5 = (i_4 >> 4 & 0x7) + class104.field1331;
         i_6 = (i_4 & 0x7) + class248.field3222;
         i_7 = class309_2.method5531(1076874125);
         i_8 = i_7 >> 4 & 0xf;
         i_9 = i_7 & 0x7;
         i_10 = class309_2.method5532(-1946834755);
         if (i_5 >= 0 && i_6 >= 0 && i_5 < 104 && i_6 < 104) {
            i_11 = i_8 + 1;
            if (class223.field2562.field993[0] >= i_5 - i_11 && class223.field2562.field993[0] <= i_11 + i_5 && class223.field2562.field968[0] >= i_6 - i_11 && class223.field2562.field968[0] <= i_6 + i_11 && class282.field3617.field1060 != 0 && i_9 > 0 && client.field884 < 50) {
               client.field857[client.field884] = i_3;
               client.field886[client.field884] = i_9;
               client.field887[client.field884] = i_10;
               client.field889[client.field884] = null;
               client.field888[client.field884] = i_8 + (i_6 << 8) + (i_5 << 16);
               ++client.field884;
            }
         }
      }

      if (class194.field2349 == class194_0) {
         i_3 = class309_2.method5531(1296335406);
         i_4 = (i_3 >> 4 & 0x7) + class104.field1331;
         i_5 = (i_3 & 0x7) + class248.field3222;
         i_6 = class309_2.method5542(468907616);
         i_7 = class309_2.method5729(1832498992);
         i_8 = class309_2.method5661((byte) 51);
         if (i_4 >= 0 && i_5 >= 0 && i_4 < 104 && i_5 < 104) {
            i_4 = i_4 * 128 + 64;
            i_5 = i_5 * 128 + 64;
            class86 class86_43 = new class86(i_6, class151.field1949, i_4, i_5, class97.method2165(i_4, i_5, class151.field1949, 1677703058) - i_8, i_7, client.field655);
            client.field734.method4888(class86_43);
         }

      } else {
         int i_12;
         int i_14;
         int i_15;
         if (class194.field2346 == class194_0) {
            i_3 = class309_2.method5533((byte) -63) * 4;
            byte b_39 = class309_2.method5535((byte) 0);
            byte b_40 = class309_2.method5535((byte) 0);
            i_6 = class309_2.method5661((byte) -36);
            i_7 = class309_2.method5540(-1856185425);
            i_8 = class309_2.method5544(560777013);
            i_9 = class309_2.method5531(-257244595);
            i_10 = (i_9 >> 4 & 0x7) + class104.field1331;
            i_11 = (i_9 & 0x7) + class248.field3222;
            i_12 = class309_2.method5729(161140018);
            int i_42 = class309_2.method5542(468907616);
            i_14 = class309_2.method5531(2103164033);
            i_15 = class309_2.method5533((byte) -52) * 4;
            i_4 = b_39 + i_10;
            i_5 = b_40 + i_11;
            if (i_10 >= 0 && i_11 >= 0 && i_10 < 104 && i_11 < 104 && i_4 >= 0 && i_5 >= 0 && i_4 < 104 && i_5 < 104 && i_42 != 65535) {
               i_10 = i_10 * 128 + 64;
               i_11 = i_11 * 128 + 64;
               i_4 = i_4 * 128 + 64;
               i_5 = i_5 * 128 + 64;
               class95 class95_16 = new class95(i_42, class151.field1949, i_10, i_11, class97.method2165(i_10, i_11, class151.field1949, 2124259995) - i_3, i_7 + client.field655, i_12 + client.field655, i_14, i_6, i_8, i_15);
               class95_16.method2147(i_4, i_5, class97.method2165(i_4, i_5, class151.field1949, 1948664082) - i_15, i_7 + client.field655, 595136450);
               client.field778.method4888(class95_16);
            }

         } else if (class194.field2343 == class194_0) {
            i_3 = class309_2.method5729(-2006214281);
            i_4 = class309_2.method5533((byte) -118);
            i_5 = i_4 >> 2;
            i_6 = i_4 & 0x3;
            i_7 = client.field708[i_5];
            i_8 = class309_2.method5661((byte) 13);
            i_9 = (i_8 >> 4 & 0x7) + class104.field1331;
            i_10 = (i_8 & 0x7) + class248.field3222;
            if (i_9 >= 0 && i_10 >= 0 && i_9 < 104 && i_10 < 104) {
               class67.method1224(class151.field1949, i_9, i_10, i_7, i_3, i_5, i_6, 0, -1, -2003250230);
            }

         } else {
            class96 class96_33;
            if (class194.field2345 == class194_0) {
               i_3 = class309_2.method5542(468907616);
               i_4 = class309_2.method5532(-1946834755);
               i_5 = (i_4 >> 4 & 0x7) + class104.field1331;
               i_6 = (i_4 & 0x7) + class248.field3222;
               i_7 = class309_2.method5542(468907616);
               if (i_5 >= 0 && i_6 >= 0 && i_5 < 104 && i_6 < 104) {
                  class96_33 = new class96();
                  class96_33.field1238 = i_7;
                  class96_33.field1239 = i_3;
                  if (client.field776[class151.field1949][i_5][i_6] == null) {
                     client.field776[class151.field1949][i_5][i_6] = new class272();
                  }

                  client.field776[class151.field1949][i_5][i_6].method4888(class96_33);
                  class72.method1265(i_5, i_6, -1072800067);
               }

            } else if (class194.field2350 == class194_0) {
               i_3 = class309_2.method5540(-1856185425);
               i_4 = class309_2.method5532(-1946834755);
               i_5 = (i_4 >> 4 & 0x7) + class104.field1331;
               i_6 = (i_4 & 0x7) + class248.field3222;
               i_7 = class309_2.method5540(-1856185425);
               i_8 = class309_2.method5540(-1856185425);
               if (i_5 >= 0 && i_6 >= 0 && i_5 < 104 && i_6 < 104) {
                  class272 class272_32 = client.field776[class151.field1949][i_5][i_6];
                  if (class272_32 != null) {
                     for (class96 class96_34 = (class96) class272_32.method4879(); class96_34 != null; class96_34 = (class96) class272_32.method4884()) {
                        if ((i_3 & 0x7fff) == class96_34.field1238 && i_8 == class96_34.field1239) {
                           class96_34.field1239 = i_7;
                           break;
                        }
                     }

                     class72.method1265(i_5, i_6, 1040364737);
                  }
               }

            } else if (class194.field2348 == class194_0) {
               i_3 = class309_2.method5540(-1856185425);
               i_4 = class309_2.method5532(-1946834755);
               i_5 = (i_4 >> 4 & 0x7) + class104.field1331;
               i_6 = (i_4 & 0x7) + class248.field3222;
               if (i_5 >= 0 && i_6 >= 0 && i_5 < 104 && i_6 < 104) {
                  class272 class272_35 = client.field776[class151.field1949][i_5][i_6];
                  if (class272_35 != null) {
                     for (class96_33 = (class96) class272_35.method4879(); class96_33 != null; class96_33 = (class96) class272_35.method4884()) {
                        if ((i_3 & 0x7fff) == class96_33.field1238) {
                           class96_33.method3628();
                           break;
                        }
                     }

                     if (class272_35.method4879() == null) {
                        client.field776[class151.field1949][i_5][i_6] = null;
                     }

                     class72.method1265(i_5, i_6, 1390562958);
                  }
               }

            } else if (class194.field2351 == class194_0) {
               i_3 = class309_2.method5532(-1946834755);
               i_4 = i_3 >> 2;
               i_5 = i_3 & 0x3;
               i_6 = client.field708[i_4];
               i_7 = class309_2.method5729(-853945648);
               i_8 = class309_2.method5532(-1946834755);
               i_9 = (i_8 >> 4 & 0x7) + class104.field1331;
               i_10 = (i_8 & 0x7) + class248.field3222;
               if (i_9 >= 0 && i_10 >= 0 && i_9 < 103 && i_10 < 103) {
                  if (i_6 == 0) {
                     class145 class145_36 = class67.field536.method3169(class151.field1949, i_9, i_10);
                     if (class145_36 != null) {
                        i_12 = class149.method3348(class145_36.field1902);
                        if (i_4 == 2) {
                           class145_36.field1899 = new class102(i_12, 2, i_5 + 4, class151.field1949, i_9, i_10, i_7, false, class145_36.field1899);
                           class145_36.field1897 = new class102(i_12, 2, i_5 + 1 & 0x3, class151.field1949, i_9, i_10, i_7, false, class145_36.field1897);
                        } else {
                           class145_36.field1899 = new class102(i_12, i_4, i_5, class151.field1949, i_9, i_10, i_7, false, class145_36.field1899);
                        }
                     }
                  }

                  if (i_6 == 1) {
                     class150 class150_44 = class67.field536.method3206(class151.field1949, i_9, i_10);
                     if (class150_44 != null) {
                        i_12 = class149.method3348(class150_44.field1940);
                        if (i_4 != 4 && i_4 != 5) {
                           if (i_4 == 6) {
                              class150_44.field1938 = new class102(i_12, 4, i_5 + 4, class151.field1949, i_9, i_10, i_7, false, class150_44.field1938);
                           } else if (i_4 == 7) {
                              class150_44.field1938 = new class102(i_12, 4, (i_5 + 2 & 0x3) + 4, class151.field1949, i_9, i_10, i_7, false, class150_44.field1938);
                           } else if (i_4 == 8) {
                              class150_44.field1938 = new class102(i_12, 4, i_5 + 4, class151.field1949, i_9, i_10, i_7, false, class150_44.field1938);
                              class150_44.field1942 = new class102(i_12, 4, (i_5 + 2 & 0x3) + 4, class151.field1949, i_9, i_10, i_7, false, class150_44.field1942);
                           }
                        } else {
                           class150_44.field1938 = new class102(i_12, 4, i_5, class151.field1949, i_9, i_10, i_7, false, class150_44.field1938);
                        }
                     }
                  }

                  if (i_6 == 2) {
                     class151 class151_45 = class67.field536.method3155(class151.field1949, i_9, i_10);
                     if (i_4 == 11) {
                        i_4 = 10;
                     }

                     if (class151_45 != null) {
                        class151_45.field1946 = new class102(class149.method3348(class151_45.field1957), i_4, i_5, class151.field1949, i_9, i_10, i_7, false, class151_45.field1946);
                     }
                  }

                  if (i_6 == 3) {
                     class132 class132_46 = class67.field536.method3154(class151.field1949, i_9, i_10);
                     if (class132_46 != null) {
                        class132_46.field1628 = new class102(class149.method3348(class132_46.field1629), 22, i_5, class151.field1949, i_9, i_10, i_7, false, class132_46.field1628);
                     }
                  }
               }

            } else {
               if (class194.field2344 == class194_0) {
                  byte b_38 = class309_2.method5605(-847953375);
                  i_4 = class309_2.method5531(31665116);
                  i_5 = i_4 >> 2;
                  i_6 = i_4 & 0x3;
                  i_7 = client.field708[i_5];
                  byte b_41 = class309_2.method5605(-1039670635);
                  i_9 = class309_2.method5729(1275817890);
                  i_10 = class309_2.method5532(-1946834755);
                  i_11 = (i_10 >> 4 & 0x7) + class104.field1331;
                  i_12 = (i_10 & 0x7) + class248.field3222;
                  byte b_13 = class309_2.method5535((byte) 0);
                  i_14 = class309_2.method5729(-2044054768);
                  i_15 = class309_2.method5499(-1597569596);
                  int i_37 = class309_2.method5729(-90948503);
                  byte b_17 = class309_2.method5535((byte) 0);
                  class75 class75_18;
                  if (i_14 == client.field765) {
                     class75_18 = class223.field2562;
                  } else {
                     class75_18 = client.field764[i_14];
                  }

                  if (class75_18 != null) {
                     class264 class264_19 = class34.method383(i_15, -1627723406);
                     int i_20;
                     int i_21;
                     if (i_6 != 1 && i_6 != 3) {
                        i_20 = class264_19.field3418;
                        i_21 = class264_19.field3396;
                     } else {
                        i_20 = class264_19.field3396;
                        i_21 = class264_19.field3418;
                     }

                     int i_22 = i_11 + (i_20 >> 1);
                     int i_23 = i_11 + (i_20 + 1 >> 1);
                     int i_24 = i_12 + (i_21 >> 1);
                     int i_25 = i_12 + (i_21 + 1 >> 1);
                     int[][] ints_26 = class64.field519[class151.field1949];
                     int i_27 = ints_26[i_23][i_25] + ints_26[i_22][i_25] + ints_26[i_22][i_24] + ints_26[i_23][i_24] >> 2;
                     int i_28 = (i_11 << 7) + (i_20 << 6);
                     int i_29 = (i_12 << 7) + (i_21 << 6);
                     class136 class136_30 = class264_19.method4586(i_5, i_6, ints_26, i_28, i_27, i_29, -1672761222);
                     if (class136_30 != null) {
                        class67.method1224(class151.field1949, i_11, i_12, i_7, -1, 0, 0, i_37 + 1, i_9 + 1, -1809446908);
                        class75_18.field614 = i_37 + client.field655;
                        class75_18.field615 = i_9 + client.field655;
                        class75_18.field607 = class136_30;
                        class75_18.field634 = i_11 * 128 + i_20 * 64;
                        class75_18.field626 = i_12 * 128 + i_21 * 64;
                        class75_18.field617 = i_27;
                        byte b_31;
                        if (b_13 > b_41) {
                           b_31 = b_13;
                           b_13 = b_41;
                           b_41 = b_31;
                        }

                        if (b_38 > b_17) {
                           b_31 = b_38;
                           b_38 = b_17;
                           b_17 = b_31;
                        }

                        class75_18.field620 = i_11 + b_13;
                        class75_18.field622 = b_41 + i_11;
                        class75_18.field606 = i_12 + b_38;
                        class75_18.field623 = i_12 + b_17;
                     }
                  }
               }

               if (class194.field2347 == class194_0) {
                  i_3 = class309_2.method5532(-1946834755);
                  i_4 = (i_3 >> 4 & 0x7) + class104.field1331;
                  i_5 = (i_3 & 0x7) + class248.field3222;
                  i_6 = class309_2.method5531(-627175264);
                  i_7 = i_6 >> 2;
                  i_8 = i_6 & 0x3;
                  i_9 = client.field708[i_7];
                  if (i_4 >= 0 && i_5 >= 0 && i_4 < 104 && i_5 < 104) {
                     class67.method1224(class151.field1949, i_4, i_5, i_9, -1, i_7, i_8, 0, -1, -1661387441);
                  }

               }
            }
         }
      }
   }

   static void method3441(class246 class246_0, String string_1, byte b_2) {
      class67 class67_3 = new class67(class246_0, string_1);
      client.field916.add(class67_3);
      client.field666 += class67_3.field534;
   }

}
