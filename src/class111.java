public class class111 extends class189 {

   static boolean field1387 = false;
   int field1401;
   int field1372;
   int field1370;
   int field1398;
   boolean field1375;
   byte[][] field1383;
   int field1389;
   float[] field1388;
   byte[] field1386;
   int field1402;
   int field1385;
   static float[] field1392;
   int field1390;
   boolean field1377;
   static int field1378;
   static int field1371;
   static byte[] field1376;
   static int field1399;
   static int field1380;
   static class109[] field1373;
   static class113[] field1374;
   static class120[] field1403;
   static class125[] field1384;
   static boolean[] field1382;
   static int[] field1379;
   static float[] field1393;
   static float[] field1394;
   static float[] field1395;
   static int[] field1391;
   static float[] field1396;
   static float[] field1397;
   static float[] field1381;
   static int[] field1400;

   class111(byte[] bytes_1) {
      this.method2395(bytes_1);
   }

   void method2395(byte[] bytes_1) {
      class310 class310_2 = new class310(bytes_1);
      this.field1401 = class310_2.method5507(1264077520);
      this.field1372 = class310_2.method5507(-1360710242);
      this.field1370 = class310_2.method5507(2058503230);
      this.field1398 = class310_2.method5507(384794433);
      if (this.field1398 < 0) {
         this.field1398 = ~this.field1398;
         this.field1375 = true;
      }

      int i_3 = class310_2.method5507(-436969329);
      this.field1383 = new byte[i_3][];

      for (int i_4 = 0; i_4 < i_3; i_4++) {
         int i_5 = 0;

         int i_6;
         do {
            i_6 = class310_2.method5661((byte) -14);
            i_5 += i_6;
         } while (i_6 >= 255);

         byte[] bytes_7 = new byte[i_5];
         class310_2.method5514(bytes_7, 0, i_5, 1075075180);
         this.field1383[i_4] = bytes_7;
      }

   }

   class112 method2401(int[] ints_1) {
      if (ints_1 != null && ints_1[0] <= 0) {
         return null;
      } else {
         if (this.field1386 == null) {
            this.field1389 = 0;
            this.field1388 = new float[field1380];
            this.field1386 = new byte[this.field1372];
            this.field1402 = 0;
            this.field1385 = 0;
         }

         for (; this.field1385 < this.field1383.length; this.field1385++) {
            if (ints_1 != null && ints_1[0] <= 0) {
               return null;
            }

            float[] floats_2 = this.method2397(this.field1385);
            if (floats_2 != null) {
               int i_3 = this.field1402;
               int i_4 = floats_2.length;
               if (i_4 > this.field1372 - i_3) {
                  i_4 = this.field1372 - i_3;
               }

               for (int i_5 = 0; i_5 < i_4; i_5++) {
                  int i_6 = (int)(128.0F + floats_2[i_5] * 128.0F);
                  if ((i_6 & ~0xff) != 0) {
                     i_6 = ~i_6 >> 31;
                  }

                  this.field1386[i_3++] = (byte)(i_6 - 128);
               }

               if (ints_1 != null) {
                  ints_1[0] -= i_3 - this.field1402;
               }

               this.field1402 = i_3;
            }
         }

         this.field1388 = null;
         byte[] bytes_7 = this.field1386;
         this.field1386 = null;
         return new class112(this.field1401, bytes_7, this.field1370, this.field1398, this.field1375);
      }
   }

   float[] method2397(int i_1) {
      method2392(this.field1383[i_1], 0);
      method2393();
      int i_2 = method2394(class286.method5061(field1379.length - 1, (short) -18749));
      boolean bool_3 = field1382[i_2];
      int i_4 = bool_3 ? field1380 : field1399;
      boolean bool_5 = false;
      boolean bool_6 = false;
      if (bool_3) {
         bool_5 = method2393() != 0;
         bool_6 = method2393() != 0;
      }

      int i_7 = i_4 >> 1;
      int i_8;
      int i_9;
      int i_10;
      if (bool_3 && !bool_5) {
         i_8 = (i_4 >> 2) - (field1399 >> 2);
         i_9 = (field1399 >> 2) + (i_4 >> 2);
         i_10 = field1399 >> 1;
      } else {
         i_8 = 0;
         i_9 = i_7;
         i_10 = i_4 >> 1;
      }

      int i_11;
      int i_12;
      int i_13;
      if (bool_3 && !bool_6) {
         i_11 = i_4 - (i_4 >> 2) - (field1399 >> 2);
         i_12 = (field1399 >> 2) + (i_4 - (i_4 >> 2));
         i_13 = field1399 >> 1;
      } else {
         i_11 = i_7;
         i_12 = i_4;
         i_13 = i_4 >> 1;
      }

      class125 class125_14 = field1384[field1379[i_2]];
      int i_16 = class125_14.field1511;
      int i_17 = class125_14.field1513[i_16];
      boolean bool_15 = !field1374[i_17].method2436();
      boolean bool_45 = bool_15;

      for (i_17 = 0; i_17 < class125_14.field1510; i_17++) {
         class120 class120_18 = field1403[class125_14.field1512[i_17]];
         float[] floats_19 = field1392;
         class120_18.method2558(floats_19, i_4 >> 1, bool_45);
      }

      int i_40;
      if (!bool_15) {
         i_17 = class125_14.field1511;
         i_40 = class125_14.field1513[i_17];
         field1374[i_40].method2437(field1392, i_4 >> 1);
      }

      int i_42;
      if (bool_15) {
         for (i_17 = i_4 >> 1; i_17 < i_4; i_17++) {
            field1392[i_17] = 0.0F;
         }
      } else {
         i_17 = i_4 >> 1;
         i_40 = i_4 >> 2;
         i_42 = i_4 >> 3;
         float[] floats_43 = field1392;

         int i_21;
         for (i_21 = 0; i_21 < i_17; i_21++) {
            floats_43[i_21] *= 0.5F;
         }

         for (i_21 = i_17; i_21 < i_4; i_21++) {
            floats_43[i_21] = -floats_43[i_4 - i_21 - 1];
         }

         float[] floats_44 = bool_3 ? field1396 : field1393;
         float[] floats_22 = bool_3 ? field1397 : field1394;
         float[] floats_23 = bool_3 ? field1381 : field1395;
         int[] ints_24 = bool_3 ? field1400 : field1391;

         int i_25;
         float f_26;
         float f_27;
         float f_28;
         float f_29;
         for (i_25 = 0; i_25 < i_40; i_25++) {
            f_26 = floats_43[i_25 * 4] - floats_43[i_4 - i_25 * 4 - 1];
            f_27 = floats_43[i_25 * 4 + 2] - floats_43[i_4 - i_25 * 4 - 3];
            f_28 = floats_44[i_25 * 2];
            f_29 = floats_44[i_25 * 2 + 1];
            floats_43[i_4 - i_25 * 4 - 1] = f_26 * f_28 - f_27 * f_29;
            floats_43[i_4 - i_25 * 4 - 3] = f_26 * f_29 + f_27 * f_28;
         }

         float f_30;
         float f_31;
         for (i_25 = 0; i_25 < i_42; i_25++) {
            f_26 = floats_43[i_17 + i_25 * 4 + 3];
            f_27 = floats_43[i_17 + i_25 * 4 + 1];
            f_28 = floats_43[i_25 * 4 + 3];
            f_29 = floats_43[i_25 * 4 + 1];
            floats_43[i_17 + i_25 * 4 + 3] = f_26 + f_28;
            floats_43[i_17 + i_25 * 4 + 1] = f_27 + f_29;
            f_30 = floats_44[i_17 - 4 - i_25 * 4];
            f_31 = floats_44[i_17 - 3 - i_25 * 4];
            floats_43[i_25 * 4 + 3] = (f_26 - f_28) * f_30 - (f_27 - f_29) * f_31;
            floats_43[i_25 * 4 + 1] = (f_27 - f_29) * f_30 + (f_26 - f_28) * f_31;
         }

         i_25 = class286.method5061(i_4 - 1, (short) -27335);

         int i_47;
         int i_48;
         int i_49;
         int i_50;
         for (i_47 = 0; i_47 < i_25 - 3; i_47++) {
            i_48 = i_4 >> i_47 + 2;
            i_49 = 8 << i_47;

            for (i_50 = 0; i_50 < 2 << i_47; i_50++) {
               int i_51 = i_4 - i_48 * i_50 * 2;
               int i_52 = i_4 - i_48 * (i_50 * 2 + 1);

               for (int i_32 = 0; i_32 < i_4 >> i_47 + 4; i_32++) {
                  int i_33 = i_32 * 4;
                  float f_34 = floats_43[i_51 - 1 - i_33];
                  float f_35 = floats_43[i_51 - 3 - i_33];
                  float f_36 = floats_43[i_52 - 1 - i_33];
                  float f_37 = floats_43[i_52 - 3 - i_33];
                  floats_43[i_51 - 1 - i_33] = f_34 + f_36;
                  floats_43[i_51 - 3 - i_33] = f_35 + f_37;
                  float f_38 = floats_44[i_32 * i_49];
                  float f_39 = floats_44[i_32 * i_49 + 1];
                  floats_43[i_52 - 1 - i_33] = (f_34 - f_36) * f_38 - (f_35 - f_37) * f_39;
                  floats_43[i_52 - 3 - i_33] = (f_35 - f_37) * f_38 + (f_34 - f_36) * f_39;
               }
            }
         }

         for (i_47 = 1; i_47 < i_42 - 1; i_47++) {
            i_48 = ints_24[i_47];
            if (i_47 < i_48) {
               i_49 = i_47 * 8;
               i_50 = i_48 * 8;
               f_30 = floats_43[i_49 + 1];
               floats_43[i_49 + 1] = floats_43[i_50 + 1];
               floats_43[i_50 + 1] = f_30;
               f_30 = floats_43[i_49 + 3];
               floats_43[i_49 + 3] = floats_43[i_50 + 3];
               floats_43[i_50 + 3] = f_30;
               f_30 = floats_43[i_49 + 5];
               floats_43[i_49 + 5] = floats_43[i_50 + 5];
               floats_43[i_50 + 5] = f_30;
               f_30 = floats_43[i_49 + 7];
               floats_43[i_49 + 7] = floats_43[i_50 + 7];
               floats_43[i_50 + 7] = f_30;
            }
         }

         for (i_47 = 0; i_47 < i_17; i_47++) {
            floats_43[i_47] = floats_43[i_47 * 2 + 1];
         }

         for (i_47 = 0; i_47 < i_42; i_47++) {
            floats_43[i_4 - 1 - i_47 * 2] = floats_43[i_47 * 4];
            floats_43[i_4 - 2 - i_47 * 2] = floats_43[i_47 * 4 + 1];
            floats_43[i_4 - i_40 - 1 - i_47 * 2] = floats_43[i_47 * 4 + 2];
            floats_43[i_4 - i_40 - 2 - i_47 * 2] = floats_43[i_47 * 4 + 3];
         }

         for (i_47 = 0; i_47 < i_42; i_47++) {
            f_27 = floats_23[i_47 * 2];
            f_28 = floats_23[i_47 * 2 + 1];
            f_29 = floats_43[i_17 + i_47 * 2];
            f_30 = floats_43[i_17 + i_47 * 2 + 1];
            f_31 = floats_43[i_4 - 2 - i_47 * 2];
            float f_53 = floats_43[i_4 - 1 - i_47 * 2];
            float f_54 = f_28 * (f_29 - f_31) + f_27 * (f_30 + f_53);
            floats_43[i_17 + i_47 * 2] = (f_29 + f_31 + f_54) * 0.5F;
            floats_43[i_4 - 2 - i_47 * 2] = (f_29 + f_31 - f_54) * 0.5F;
            f_54 = f_28 * (f_30 + f_53) - f_27 * (f_29 - f_31);
            floats_43[i_17 + i_47 * 2 + 1] = (f_30 - f_53 + f_54) * 0.5F;
            floats_43[i_4 - 1 - i_47 * 2] = (-f_30 + f_53 + f_54) * 0.5F;
         }

         for (i_47 = 0; i_47 < i_40; i_47++) {
            floats_43[i_47] = floats_43[i_17 + i_47 * 2] * floats_22[i_47 * 2] + floats_43[i_17 + i_47 * 2 + 1] * floats_22[i_47 * 2 + 1];
            floats_43[i_17 - 1 - i_47] = floats_43[i_17 + i_47 * 2] * floats_22[i_47 * 2 + 1] - floats_43[i_17 + i_47 * 2 + 1] * floats_22[i_47 * 2];
         }

         for (i_47 = 0; i_47 < i_40; i_47++) {
            floats_43[i_47 + (i_4 - i_40)] = -floats_43[i_47];
         }

         for (i_47 = 0; i_47 < i_40; i_47++) {
            floats_43[i_47] = floats_43[i_40 + i_47];
         }

         for (i_47 = 0; i_47 < i_40; i_47++) {
            floats_43[i_40 + i_47] = -floats_43[i_40 - i_47 - 1];
         }

         for (i_47 = 0; i_47 < i_40; i_47++) {
            floats_43[i_17 + i_47] = floats_43[i_4 - i_47 - 1];
         }

         for (i_47 = i_8; i_47 < i_9; i_47++) {
            f_27 = (float)Math.sin(((double)(i_47 - i_8) + 0.5D) / (double)i_10 * 0.5D * 3.141592653589793D);
            field1392[i_47] *= (float)Math.sin(1.5707963267948966D * (double)f_27 * (double)f_27);
         }

         for (i_47 = i_11; i_47 < i_12; i_47++) {
            f_27 = (float)Math.sin(((double)(i_47 - i_11) + 0.5D) / (double)i_13 * 0.5D * 3.141592653589793D + 1.5707963267948966D);
            field1392[i_47] *= (float)Math.sin(1.5707963267948966D * (double)f_27 * (double)f_27);
         }
      }

      float[] floats_41 = null;
      if (this.field1389 > 0) {
         i_40 = i_4 + this.field1389 >> 2;
         floats_41 = new float[i_40];
         int i_20;
         if (!this.field1377) {
            for (i_42 = 0; i_42 < this.field1390; i_42++) {
               i_20 = i_42 + (this.field1389 >> 1);
               floats_41[i_42] += this.field1388[i_20];
            }
         }

         if (!bool_15) {
            for (i_42 = i_8; i_42 < i_4 >> 1; i_42++) {
               i_20 = floats_41.length - (i_4 >> 1) + i_42;
               floats_41[i_20] += field1392[i_42];
            }
         }
      }

      float[] floats_46 = this.field1388;
      this.field1388 = field1392;
      field1392 = floats_46;
      this.field1389 = i_4;
      this.field1390 = i_12 - (i_4 >> 1);
      this.field1377 = bool_15;
      return floats_41;
   }

   static int method2393() {
      int i_0 = field1376[field1371] >> field1378 & 0x1;
      ++field1378;
      field1371 += field1378 >> 3;
      field1378 &= 0x7;
      return i_0;
   }

   static int method2394(int i_0) {
      int i_1 = 0;

      int i_2;
      int i_3;
      for (i_2 = 0; i_0 >= 8 - field1378; i_0 -= i_3) {
         i_3 = 8 - field1378;
         int i_4 = (1 << i_3) - 1;
         i_1 += (field1376[field1371] >> field1378 & i_4) << i_2;
         field1378 = 0;
         ++field1371;
         i_2 += i_3;
      }

      if (i_0 > 0) {
         i_3 = (1 << i_0) - 1;
         i_1 += (field1376[field1371] >> field1378 & i_3) << i_2;
         field1378 += i_0;
      }

      return i_1;
   }

   static void method2392(byte[] bytes_0, int i_1) {
      field1376 = bytes_0;
      field1371 = i_1;
      field1378 = 0;
   }

   static boolean method2422(class244 class244_0) {
      if (!field1387) {
         byte[] bytes_1 = class244_0.method4160(0, 0, (short) -17526);
         if (bytes_1 == null) {
            return false;
         }

         method2424(bytes_1);
         field1387 = true;
      }

      return true;
   }

   static class111 method2396(class244 class244_0, int i_1, int i_2) {
      if (!method2422(class244_0)) {
         class244_0.method4162(i_1, i_2, 832490845);
         return null;
      } else {
         byte[] bytes_3 = class244_0.method4160(i_1, i_2, (short) -17247);
         return bytes_3 == null ? null : new class111(bytes_3);
      }
   }

   static void method2424(byte[] bytes_0) {
      method2392(bytes_0, 0);
      field1399 = 1 << method2394(4);
      field1380 = 1 << method2394(4);
      field1392 = new float[field1380];

      int i_1;
      int i_2;
      int i_3;
      int i_4;
      int i_5;
      for (i_1 = 0; i_1 < 2; i_1++) {
         i_2 = i_1 != 0 ? field1380 : field1399;
         i_3 = i_2 >> 1;
         i_4 = i_2 >> 2;
         i_5 = i_2 >> 3;
         float[] floats_6 = new float[i_3];

         for (int i_7 = 0; i_7 < i_4; i_7++) {
            floats_6[i_7 * 2] = (float)Math.cos((double)(i_7 * 4) * 3.141592653589793D / (double)i_2);
            floats_6[i_7 * 2 + 1] = -((float)Math.sin((double)(i_7 * 4) * 3.141592653589793D / (double)i_2));
         }

         float[] floats_12 = new float[i_3];

         for (int i_8 = 0; i_8 < i_4; i_8++) {
            floats_12[i_8 * 2] = (float)Math.cos((double)(i_8 * 2 + 1) * 3.141592653589793D / (double)(i_2 * 2));
            floats_12[i_8 * 2 + 1] = (float)Math.sin((double)(i_8 * 2 + 1) * 3.141592653589793D / (double)(i_2 * 2));
         }

         float[] floats_13 = new float[i_4];

         for (int i_9 = 0; i_9 < i_5; i_9++) {
            floats_13[i_9 * 2] = (float)Math.cos((double)(i_9 * 4 + 2) * 3.141592653589793D / (double)i_2);
            floats_13[i_9 * 2 + 1] = -((float)Math.sin((double)(i_9 * 4 + 2) * 3.141592653589793D / (double)i_2));
         }

         int[] ints_14 = new int[i_5];
         int i_10 = class286.method5061(i_5 - 1, (short) -2342);

         for (int i_11 = 0; i_11 < i_5; i_11++) {
            ints_14[i_11] = class205.method3692(i_11, i_10, (byte) -94);
         }

         if (i_1 != 0) {
            field1396 = floats_6;
            field1397 = floats_12;
            field1381 = floats_13;
            field1400 = ints_14;
         } else {
            field1393 = floats_6;
            field1394 = floats_12;
            field1395 = floats_13;
            field1391 = ints_14;
         }
      }

      i_1 = method2394(8) + 1;
      field1373 = new class109[i_1];

      for (i_2 = 0; i_2 < i_1; i_2++) {
         field1373[i_2] = new class109();
      }

      i_2 = method2394(6) + 1;

      for (i_3 = 0; i_3 < i_2; i_3++) {
         method2394(16);
      }

      i_2 = method2394(6) + 1;
      field1374 = new class113[i_2];

      for (i_3 = 0; i_3 < i_2; i_3++) {
         field1374[i_3] = new class113();
      }

      i_3 = method2394(6) + 1;
      field1403 = new class120[i_3];

      for (i_4 = 0; i_4 < i_3; i_4++) {
         field1403[i_4] = new class120();
      }

      i_4 = method2394(6) + 1;
      field1384 = new class125[i_4];

      for (i_5 = 0; i_5 < i_4; i_5++) {
         field1384[i_5] = new class125();
      }

      i_5 = method2394(6) + 1;
      field1382 = new boolean[i_5];
      field1379 = new int[i_5];

      for (int i_15 = 0; i_15 < i_5; i_15++) {
         field1382[i_15] = method2393() != 0;
         method2394(16);
         method2394(16);
         field1379[i_15] = method2394(8);
      }

   }

   static float method2398(int i_0) {
      int i_1 = i_0 & 0x1fffff;
      int i_2 = i_0 & ~0x7fffffff;
      int i_3 = (i_0 & 0x7fe00000) >> 21;
      if (i_2 != 0) {
         i_1 = -i_1;
      }

      return (float)((double)i_1 * Math.pow(2.0D, (double)(i_3 - 788)));
   }

}
