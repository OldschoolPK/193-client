public class class253 extends class184 {

   static class244 field3266;
   static int field3267;
   static class226 field3264;
   static class154 field3265 = new class154(64);
   public boolean field3268 = false;

   void method4387(class310 class310_1, int i_2, byte b_3) {
      if (i_2 == 2) {
         this.field3268 = true;
      }

   }

   void method4383(class310 class310_1, short s_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) 38);
         if (i_3 == 0) {
            return;
         }

         this.method4387(class310_1, i_3, (byte) 8);
      }
   }

   public static void method4393(int i_0) {
      class257.field3308.method3375();
      class257.field3309.method3375();
   }

   static final void method4395(int i_0, int i_1, boolean bool_2, int i_3) {
      if (!bool_2 || i_0 != class182.field2112 || class168.field2013 != i_1) {
         class182.field2112 = i_0;
         class168.field2013 = i_1;
         class44.method663(25, -2050562485);
         class221.method3988("Loading - please wait.", true, (byte) -59);
         int i_4 = class243.field3156;
         int i_5 = class41.field300;
         class243.field3156 = (i_0 - 6) * 8;
         class41.field300 = (i_1 - 6) * 8;
         int i_6 = class243.field3156 - i_4;
         int i_7 = class41.field300 - i_5;
         i_4 = class243.field3156;
         i_5 = class41.field300;

         int i_8;
         int i_10;
         for (i_8 = 0; i_8 < 32768; i_8++) {
            class88 class88_9 = client.field689[i_8];
            if (class88_9 != null) {
               for (i_10 = 0; i_10 < 10; i_10++) {
                  class88_9.field993[i_10] -= i_6;
                  class88_9.field968[i_10] -= i_7;
               }

               class88_9.field990 -= i_6 * 128;
               class88_9.field938 -= i_7 * 128;
            }
         }

         for (i_8 = 0; i_8 < 2048; i_8++) {
            class75 class75_22 = client.field764[i_8];
            if (class75_22 != null) {
               for (i_10 = 0; i_10 < 10; i_10++) {
                  class75_22.field993[i_10] -= i_6;
                  class75_22.field968[i_10] -= i_7;
               }

               class75_22.field990 -= i_6 * 128;
               class75_22.field938 -= i_7 * 128;
            }
         }

         byte b_21 = 0;
         byte b_19 = 104;
         byte b_23 = 1;
         if (i_6 < 0) {
            b_21 = 103;
            b_19 = -1;
            b_23 = -1;
         }

         byte b_11 = 0;
         byte b_12 = 104;
         byte b_13 = 1;
         if (i_7 < 0) {
            b_11 = 103;
            b_12 = -1;
            b_13 = -1;
         }

         int i_15;
         for (int i_14 = b_21; b_19 != i_14; i_14 += b_23) {
            for (i_15 = b_11; b_12 != i_15; i_15 += b_13) {
               int i_16 = i_6 + i_14;
               int i_17 = i_7 + i_15;

               for (int i_18 = 0; i_18 < 4; i_18++) {
                  if (i_16 >= 0 && i_17 >= 0 && i_16 < 104 && i_17 < 104) {
                     client.field776[i_18][i_14][i_15] = client.field776[i_18][i_16][i_17];
                  } else {
                     client.field776[i_18][i_14][i_15] = null;
                  }
               }
            }
         }

         for (class77 class77_20 = (class77) client.field876.method4879(); class77_20 != null; class77_20 = (class77) client.field876.method4884()) {
            class77_20.field926 -= i_6;
            class77_20.field927 -= i_7;
            if (class77_20.field926 < 0 || class77_20.field927 < 0 || class77_20.field926 >= 104 || class77_20.field927 >= 104) {
               class77_20.method3628();
            }
         }

         if (client.field774 != 0) {
            client.field774 -= i_6;
            client.field880 -= i_7;
         }

         client.field884 = 0;
         client.field890 = false;
         class75.field630 -= i_6 << 7;
         class69.field558 -= i_7 << 7;
         class97.field1244 -= i_6 << 7;
         class121.field1477 -= i_7 << 7;
         client.field874 = -1;
         client.field734.method4876();
         client.field778.method4876();

         for (i_15 = 0; i_15 < 4; i_15++) {
            client.field831[i_15].method3581(-220031908);
         }

      }
   }

   public static int method4394(byte b_0) {
      return class54.field418;
   }

}
