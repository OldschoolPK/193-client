public class class226 extends class189 {

   public class226[] field2672;
   public Object[] field2701;
   public byte[][] field2649;
   public int[] field2684;
   public int[] field2679;
   public int[] field2664;
   public byte[][] field2637;
   public Object[] field2581;
   public Object[] field2716;
   public Object[] field2696;
   public Object[] field2697;
   public Object[] field2698;
   public Object[] field2699;
   public Object[] field2610;
   public Object[] field2702;
   public Object[] field2703;
   public static class154 field2580 = new class154(200);
   public static class154 field2714 = new class154(50);
   public static class154 field2681 = new class154(20);
   public static class154 field2728 = new class154(8);
   public static boolean field2603 = false;
   public boolean field2585 = false;
   public int field2586 = -1;
   public int field2587 = -1;
   public int field2589 = 0;
   public int field2695 = 0;
   public int field2591 = 0;
   public int field2692 = 0;
   public int field2593 = 0;
   public int field2594 = 0;
   public int field2595 = 0;
   public int field2579 = 0;
   public int field2597 = 0;
   public int field2598 = 0;
   public int field2694 = 0;
   public int field2600 = 0;
   public int field2601 = 0;
   public int field2602 = 0;
   public int field2660 = 1;
   public int field2604 = 1;
   public int field2670 = -1;
   public boolean field2643 = false;
   public int field2607 = 0;
   public int field2608 = 0;
   public int field2609 = 0;
   public int field2639 = 0;
   public int field2611 = 0;
   public int field2612 = 0;
   public int field2666 = 0;
   public int field2614 = 0;
   public boolean field2615 = false;
   public class333 field2616;
   public int field2652;
   public int field2636;
   public int field2619;
   public boolean field2620;
   public int field2621;
   public int field2622;
   public int field2627;
   public boolean field2624;
   public int field2625;
   public int field2626;
   public int field2629;
   public int field2687;
   int field2631;
   int field2720;
   public int field2633;
   public int field2634;
   public int field2635;
   public int field2680;
   public int field2583;
   public int field2638;
   public int field2657;
   public int field2640;
   public int field2641;
   public int field2642;
   public boolean field2710;
   public boolean field2673;
   public int field2644;
   public int field2646;
   public String field2647;
   public String field2648;
   public int field2663;
   public int field2650;
   public int field2651;
   public boolean field2693;
   public int field2707;
   public int field2654;
   public int field2659;
   public boolean field2590;
   public String field2665;
   public class226 field2667;
   public int field2668;
   public int field2669;
   public boolean field2671;
   public String field2576;
   public boolean field2575;
   public int field2582;
   public String field2708;
   public String field2709;
   public int field2712;
   public int field2713;
   public int field2718;
   public int field2715;
   public boolean field2717;
   public boolean field2605;
   public int field2719;
   public int field2645;
   public int field2721;
   public int field2725;
   public int field2723;
   public int field2596;
   public boolean field2726;
   public boolean field2727;
   public boolean field2623;
   public int field2588;
   public boolean field2617;
   public String[] field2662;
   public boolean field2628;
   public Object[] field2691;
   public Object[] field2678;
   public Object[] field2606;
   public Object[] field2584;
   public Object[] field2683;
   public Object[] field2685;
   public Object[] field2632;
   public Object[] field2689;
   public Object[] field2630;
   public Object[] field2613;
   public Object[] field2618;
   public Object[] field2674;
   public Object[] field2675;
   public Object[] field2676;
   public Object[] field2677;
   public Object[] field2592;
   public Object[] field2682;
   public Object[] field2653;
   public int[] field2661;
   public int[] field2688;
   public int[] field2690;
   public int[] field2705;
   public int[] field2706;
   public int[][] field2599;
   public int[] field2704;
   public int[] field2711;
   public int[] field2655;
   public int[] field2656;
   public int[] field2722;
   public String[] field2658;

   public class226() {
      this.field2616 = class333.field3901;
      this.field2652 = 0;
      this.field2636 = 0;
      this.field2619 = 1;
      this.field2620 = false;
      this.field2621 = -1;
      this.field2622 = -1;
      this.field2627 = 0;
      this.field2624 = false;
      this.field2625 = 0;
      this.field2626 = 0;
      this.field2629 = 1;
      this.field2687 = -1;
      this.field2631 = 1;
      this.field2720 = -1;
      this.field2633 = -1;
      this.field2634 = -1;
      this.field2635 = 0;
      this.field2680 = 0;
      this.field2583 = 0;
      this.field2638 = 0;
      this.field2657 = 0;
      this.field2640 = 100;
      this.field2641 = 0;
      this.field2642 = 0;
      this.field2710 = false;
      this.field2673 = false;
      this.field2644 = 2;
      this.field2646 = -1;
      this.field2647 = "";
      this.field2648 = "";
      this.field2663 = 0;
      this.field2650 = 0;
      this.field2651 = 0;
      this.field2693 = false;
      this.field2707 = 0;
      this.field2654 = 0;
      this.field2659 = 0;
      this.field2590 = false;
      this.field2665 = "";
      this.field2667 = null;
      this.field2668 = 0;
      this.field2669 = 0;
      this.field2671 = false;
      this.field2576 = "";
      this.field2575 = false;
      this.field2582 = -1;
      this.field2708 = "";
      this.field2709 = "Ok";
      this.field2712 = -1;
      this.field2713 = 0;
      this.field2718 = 0;
      this.field2715 = 0;
      this.field2717 = false;
      this.field2605 = false;
      this.field2719 = -1;
      this.field2645 = 0;
      this.field2721 = 0;
      this.field2725 = 0;
      this.field2723 = -1;
      this.field2596 = -1;
      this.field2726 = false;
      this.field2727 = false;
      this.field2623 = false;
   }

   public class335 method4091(boolean bool_1, byte b_2) {
      field2603 = false;
      int i_3;
      if (bool_1) {
         i_3 = this.field2622;
      } else {
         i_3 = this.field2621;
      }

      if (i_3 == -1) {
         return null;
      } else {
         long long_4 = ((long)this.field2626 << 40) + ((this.field2617 ? 1L : 0L) << 38) + ((long)this.field2625 << 36) + (long)i_3 + ((this.field2628 ? 1L : 0L) << 39);
         class335 class335_6 = (class335) field2580.method3376(long_4);
         if (class335_6 != null) {
            return class335_6;
         } else {
            class335_6 = class230.method4115(class1.field2, i_3, 0, (byte) 75);
            if (class335_6 == null) {
               field2603 = true;
               return null;
            } else {
               if (this.field2617) {
                  class335_6.method6127();
               }

               if (this.field2628) {
                  class335_6.method6119();
               }

               if (this.field2625 > 0) {
                  class335_6.method6195(this.field2625);
               }

               if (this.field2625 >= 1) {
                  class335_6.method6122(1);
               }

               if (this.field2625 >= 2) {
                  class335_6.method6122(16777215);
               }

               if (this.field2626 != 0) {
                  class335_6.method6123(this.field2626);
               }

               field2580.method3374(class335_6, long_4);
               return class335_6;
            }
         }
      }
   }

   void method4045(class310 class310_1, byte b_2) {
      class310_1.method5661((byte) 78);
      this.field2585 = true;
      this.field2588 = class310_1.method5661((byte) -50);
      this.field2695 = class310_1.method5729(-1828381106);
      this.field2595 = class310_1.method5505((short) 11997);
      this.field2579 = class310_1.method5505((short) 7960);
      this.field2597 = class310_1.method5729(-1321152950);
      if (this.field2588 == 9) {
         this.field2598 = class310_1.method5505((short) 12990);
      } else {
         this.field2598 = class310_1.method5729(1745781708);
      }

      this.field2593 = class310_1.method5535((byte) 0);
      this.field2594 = class310_1.method5535((byte) 0);
      this.field2591 = class310_1.method5535((byte) 0);
      this.field2692 = class310_1.method5535((byte) 0);
      this.field2670 = class310_1.method5729(-1464330317);
      if (this.field2670 == 65535) {
         this.field2670 = -1;
      } else {
         this.field2670 += this.field2586 & ~0xffff;
      }

      this.field2643 = class310_1.method5661((byte) -81) == 1;
      if (this.field2588 == 0) {
         this.field2609 = class310_1.method5729(-2037124795);
         this.field2639 = class310_1.method5729(-1288381374);
         this.field2726 = class310_1.method5661((byte) 3) == 1;
      }

      if (this.field2588 == 5) {
         this.field2621 = class310_1.method5507(-820455538);
         this.field2627 = class310_1.method5729(1725282595);
         this.field2624 = class310_1.method5661((byte) 59) == 1;
         this.field2652 = class310_1.method5661((byte) -14);
         this.field2625 = class310_1.method5661((byte) -16);
         this.field2626 = class310_1.method5507(-451158568);
         this.field2617 = class310_1.method5661((byte) 52) == 1;
         this.field2628 = class310_1.method5661((byte) 5) == 1;
      }

      if (this.field2588 == 6) {
         this.field2629 = 1;
         this.field2687 = class310_1.method5729(559353367);
         if (this.field2687 == 65535) {
            this.field2687 = -1;
         }

         this.field2635 = class310_1.method5505((short) 25693);
         this.field2680 = class310_1.method5505((short) 17814);
         this.field2583 = class310_1.method5729(190410850);
         this.field2638 = class310_1.method5729(-1725917493);
         this.field2657 = class310_1.method5729(819848863);
         this.field2640 = class310_1.method5729(-1900628478);
         this.field2633 = class310_1.method5729(425496357);
         if (this.field2633 == 65535) {
            this.field2633 = -1;
         }

         this.field2710 = class310_1.method5661((byte) 6) == 1;
         class310_1.method5729(1007728887);
         if (this.field2593 != 0) {
            this.field2641 = class310_1.method5729(199125763);
         }

         if (this.field2594 != 0) {
            class310_1.method5729(1718655758);
         }
      }

      if (this.field2588 == 4) {
         this.field2646 = class310_1.method5729(2134606872);
         if (this.field2646 == 65535) {
            this.field2646 = -1;
         }

         this.field2647 = class310_1.method5589(1900506295);
         this.field2663 = class310_1.method5661((byte) -54);
         this.field2650 = class310_1.method5661((byte) 52);
         this.field2651 = class310_1.method5661((byte) 109);
         this.field2693 = class310_1.method5661((byte) 21) == 1;
         this.field2611 = class310_1.method5507(-1414962335);
      }

      if (this.field2588 == 3) {
         this.field2611 = class310_1.method5507(-1647995296);
         this.field2615 = class310_1.method5661((byte) 44) == 1;
         this.field2652 = class310_1.method5661((byte) -4);
      }

      if (this.field2588 == 9) {
         this.field2619 = class310_1.method5661((byte) 69);
         this.field2611 = class310_1.method5507(-234370706);
         this.field2620 = class310_1.method5661((byte) -8) == 1;
      }

      this.field2659 = class310_1.method5506((short) 239);
      this.field2665 = class310_1.method5589(1482520540);
      int i_3 = class310_1.method5661((byte) -33);
      if (i_3 > 0) {
         this.field2662 = new String[i_3];

         for (int i_4 = 0; i_4 < i_3; i_4++) {
            this.field2662[i_4] = class310_1.method5589(1231533839);
         }
      }

      this.field2668 = class310_1.method5661((byte) 64);
      this.field2669 = class310_1.method5661((byte) 18);
      this.field2671 = class310_1.method5661((byte) 41) == 1;
      this.field2576 = class310_1.method5589(912272414);
      this.field2691 = this.method4046(class310_1, -747120554);
      this.field2678 = this.method4046(class310_1, -1336949437);
      this.field2606 = this.method4046(class310_1, 2076735289);
      this.field2584 = this.method4046(class310_1, 877539014);
      this.field2683 = this.method4046(class310_1, 1649242994);
      this.field2685 = this.method4046(class310_1, 727081818);
      this.field2632 = this.method4046(class310_1, -522451146);
      this.field2689 = this.method4046(class310_1, -892128577);
      this.field2630 = this.method4046(class310_1, 920677236);
      this.field2613 = this.method4046(class310_1, -1154367748);
      this.field2618 = this.method4046(class310_1, -1151034983);
      this.field2674 = this.method4046(class310_1, -1786914853);
      this.field2675 = this.method4046(class310_1, 1421382777);
      this.field2676 = this.method4046(class310_1, -1237632857);
      this.field2677 = this.method4046(class310_1, 334557403);
      this.field2592 = this.method4046(class310_1, 607672990);
      this.field2682 = this.method4046(class310_1, 15374363);
      this.field2653 = this.method4046(class310_1, -1625159656);
      this.field2661 = this.method4078(class310_1, -149240461);
      this.field2688 = this.method4078(class310_1, -149240461);
      this.field2690 = this.method4078(class310_1, -149240461);
   }

   void method4061(class310 class310_1, int i_2) {
      this.field2585 = false;
      this.field2588 = class310_1.method5661((byte) -66);
      this.field2589 = class310_1.method5661((byte) 5);
      this.field2695 = class310_1.method5729(-494778828);
      this.field2595 = class310_1.method5505((short) 9732);
      this.field2579 = class310_1.method5505((short) 12355);
      this.field2597 = class310_1.method5729(900858650);
      this.field2598 = class310_1.method5729(-683749703);
      this.field2652 = class310_1.method5661((byte) -15);
      this.field2670 = class310_1.method5729(241909822);
      if (this.field2670 == 65535) {
         this.field2670 = -1;
      } else {
         this.field2670 += this.field2586 & ~0xffff;
      }

      this.field2582 = class310_1.method5729(-2006269571);
      if (this.field2582 == 65535) {
         this.field2582 = -1;
      }

      int i_3 = class310_1.method5661((byte) 12);
      int i_4;
      if (i_3 > 0) {
         this.field2705 = new int[i_3];
         this.field2706 = new int[i_3];

         for (i_4 = 0; i_4 < i_3; i_4++) {
            this.field2705[i_4] = class310_1.method5661((byte) 17);
            this.field2706[i_4] = class310_1.method5729(641195393);
         }
      }

      i_4 = class310_1.method5661((byte) -99);
      int i_5;
      int i_6;
      int i_7;
      if (i_4 > 0) {
         this.field2599 = new int[i_4][];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            i_6 = class310_1.method5729(-855918929);
            this.field2599[i_5] = new int[i_6];

            for (i_7 = 0; i_7 < i_6; i_7++) {
               this.field2599[i_5][i_7] = class310_1.method5729(487270201);
               if (this.field2599[i_5][i_7] == 65535) {
                  this.field2599[i_5][i_7] = -1;
               }
            }
         }
      }

      if (this.field2588 == 0) {
         this.field2639 = class310_1.method5729(1047355248);
         this.field2643 = class310_1.method5661((byte) 23) == 1;
      }

      if (this.field2588 == 1) {
         class310_1.method5729(1731333783);
         class310_1.method5661((byte) 16);
      }

      if (this.field2588 == 2) {
         this.field2704 = new int[this.field2597 * this.field2598];
         this.field2711 = new int[this.field2598 * this.field2597];
         i_5 = class310_1.method5661((byte) -42);
         if (i_5 == 1) {
            this.field2659 |= 0x10000000;
         }

         i_6 = class310_1.method5661((byte) -15);
         if (i_6 == 1) {
            this.field2659 |= 0x40000000;
         }

         i_7 = class310_1.method5661((byte) -40);
         if (i_7 == 1) {
            this.field2659 |= ~0x7fffffff;
         }

         int i_8 = class310_1.method5661((byte) -24);
         if (i_8 == 1) {
            this.field2659 |= 0x20000000;
         }

         this.field2707 = class310_1.method5661((byte) 58);
         this.field2654 = class310_1.method5661((byte) 10);
         this.field2655 = new int[20];
         this.field2656 = new int[20];
         this.field2722 = new int[20];

         int i_9;
         for (i_9 = 0; i_9 < 20; i_9++) {
            int i_10 = class310_1.method5661((byte) -13);
            if (i_10 == 1) {
               this.field2655[i_9] = class310_1.method5505((short) 27222);
               this.field2656[i_9] = class310_1.method5505((short) 205);
               this.field2722[i_9] = class310_1.method5507(1139002008);
            } else {
               this.field2722[i_9] = -1;
            }
         }

         this.field2658 = new String[5];

         for (i_9 = 0; i_9 < 5; i_9++) {
            String string_12 = class310_1.method5589(-1356878761);
            if (string_12.length() > 0) {
               this.field2658[i_9] = string_12;
               this.field2659 |= 1 << i_9 + 23;
            }
         }
      }

      if (this.field2588 == 3) {
         this.field2615 = class310_1.method5661((byte) 72) == 1;
      }

      if (this.field2588 == 4 || this.field2588 == 1) {
         this.field2650 = class310_1.method5661((byte) -6);
         this.field2651 = class310_1.method5661((byte) 1);
         this.field2663 = class310_1.method5661((byte) 6);
         this.field2646 = class310_1.method5729(1959822306);
         if (this.field2646 == 65535) {
            this.field2646 = -1;
         }

         this.field2693 = class310_1.method5661((byte) 18) == 1;
      }

      if (this.field2588 == 4) {
         this.field2647 = class310_1.method5589(-1266650028);
         this.field2648 = class310_1.method5589(1736890294);
      }

      if (this.field2588 == 1 || this.field2588 == 3 || this.field2588 == 4) {
         this.field2611 = class310_1.method5507(1105677905);
      }

      if (this.field2588 == 3 || this.field2588 == 4) {
         this.field2612 = class310_1.method5507(2016315852);
         this.field2666 = class310_1.method5507(276221800);
         this.field2614 = class310_1.method5507(886353874);
      }

      if (this.field2588 == 5) {
         this.field2621 = class310_1.method5507(-621437596);
         this.field2622 = class310_1.method5507(-163447484);
      }

      if (this.field2588 == 6) {
         this.field2629 = 1;
         this.field2687 = class310_1.method5729(191233587);
         if (this.field2687 == 65535) {
            this.field2687 = -1;
         }

         this.field2631 = 1;
         this.field2720 = class310_1.method5729(-1361980871);
         if (this.field2720 == 65535) {
            this.field2720 = -1;
         }

         this.field2633 = class310_1.method5729(-1213306979);
         if (this.field2633 == 65535) {
            this.field2633 = -1;
         }

         this.field2634 = class310_1.method5729(-2017161528);
         if (this.field2634 == 65535) {
            this.field2634 = -1;
         }

         this.field2640 = class310_1.method5729(480483825);
         this.field2583 = class310_1.method5729(-1200737004);
         this.field2638 = class310_1.method5729(-1690406556);
      }

      if (this.field2588 == 7) {
         this.field2704 = new int[this.field2597 * this.field2598];
         this.field2711 = new int[this.field2598 * this.field2597];
         this.field2650 = class310_1.method5661((byte) -10);
         this.field2646 = class310_1.method5729(-1810818587);
         if (this.field2646 == 65535) {
            this.field2646 = -1;
         }

         this.field2693 = class310_1.method5661((byte) 30) == 1;
         this.field2611 = class310_1.method5507(1488619429);
         this.field2707 = class310_1.method5505((short) 12388);
         this.field2654 = class310_1.method5505((short) 32027);
         i_5 = class310_1.method5661((byte) 65);
         if (i_5 == 1) {
            this.field2659 |= 0x40000000;
         }

         this.field2658 = new String[5];

         for (i_6 = 0; i_6 < 5; i_6++) {
            String string_11 = class310_1.method5589(-1107147929);
            if (string_11.length() > 0) {
               this.field2658[i_6] = string_11;
               this.field2659 |= 1 << i_6 + 23;
            }
         }
      }

      if (this.field2588 == 8) {
         this.field2647 = class310_1.method5589(-1010089990);
      }

      if (this.field2589 == 2 || this.field2588 == 2) {
         this.field2576 = class310_1.method5589(-594455710);
         this.field2708 = class310_1.method5589(-1030900272);
         i_5 = class310_1.method5729(-24547377) & 0x3f;
         this.field2659 |= i_5 << 11;
      }

      if (this.field2589 == 1 || this.field2589 == 4 || this.field2589 == 5 || this.field2589 == 6) {
         this.field2709 = class310_1.method5589(1825572939);
         if (this.field2709.length() == 0) {
            if (this.field2589 == 1) {
               this.field2709 = "Ok";
            }

            if (this.field2589 == 4) {
               this.field2709 = "Select";
            }

            if (this.field2589 == 5) {
               this.field2709 = "Select";
            }

            if (this.field2589 == 6) {
               this.field2709 = "Continue";
            }
         }
      }

      if (this.field2589 == 1 || this.field2589 == 4 || this.field2589 == 5) {
         this.field2659 |= 0x400000;
      }

      if (this.field2589 == 6) {
         this.field2659 |= 0x1;
      }

   }

   public class220 method4072(boolean bool_1, byte b_2) {
      if (this.field2622 == -1) {
         bool_1 = false;
      }

      int i_3 = bool_1 ? this.field2622 : this.field2621;
      if (i_3 == -1) {
         return null;
      } else {
         long long_4 = ((long)this.field2626 << 40) + ((this.field2617 ? 1L : 0L) << 38) + ((long)this.field2625 << 36) + (long)i_3 + ((this.field2628 ? 1L : 0L) << 39);
         class220 class220_6 = (class220) field2728.method3376(long_4);
         if (class220_6 != null) {
            return class220_6;
         } else {
            class335 class335_7 = this.method4091(bool_1, (byte) 14);
            if (class335_7 == null) {
               return null;
            } else {
               class335 class335_8 = class335_7.method6116();
               int[] ints_9 = new int[class335_8.field3911];
               int[] ints_10 = new int[class335_8.field3911];

               for (int i_11 = 0; i_11 < class335_8.field3911; i_11++) {
                  int i_12 = 0;
                  int i_13 = class335_8.field3913;

                  int i_14;
                  for (i_14 = 0; i_14 < class335_8.field3913; i_14++) {
                     if (class335_8.field3917[i_14 + i_11 * class335_8.field3913] == 0) {
                        i_12 = i_14;
                        break;
                     }
                  }

                  for (i_14 = class335_8.field3913 - 1; i_14 >= i_12; --i_14) {
                     if (class335_8.field3917[i_14 + i_11 * class335_8.field3913] == 0) {
                        i_13 = i_14 + 1;
                        break;
                     }
                  }

                  ints_9[i_11] = i_12;
                  ints_10[i_11] = i_13 - i_12;
               }

               class220_6 = new class220(class335_8.field3913, class335_8.field3911, ints_10, ints_9, i_3);
               field2728.method3374(class220_6, long_4);
               return class220_6;
            }
         }
      }
   }

   public class305 method4050(int i_1) {
      field2603 = false;
      if (this.field2646 == -1) {
         return null;
      } else {
         class305 class305_2 = (class305) field2681.method3376((long)this.field2646);
         if (class305_2 != null) {
            return class305_2;
         } else {
            class305_2 = class320.method5891(class1.field2, class329.field3877, this.field2646, 0, 1224864347);
            if (class305_2 != null) {
               field2681.method3374(class305_2, (long)this.field2646);
            } else {
               field2603 = true;
            }

            return class305_2;
         }
      }
   }

   public class136 method4052(class269 class269_1, int i_2, boolean bool_3, class223 class223_4, int i_5) {
      field2603 = false;
      int i_6;
      int i_7;
      if (bool_3) {
         i_6 = this.field2631;
         i_7 = this.field2720;
      } else {
         i_6 = this.field2629;
         i_7 = this.field2687;
      }

      if (i_6 == 0) {
         return null;
      } else if (i_6 == 1 && i_7 == -1) {
         return null;
      } else {
         class136 class136_8 = (class136) field2714.method3376((long)(i_7 + (i_6 << 16)));
         if (class136_8 == null) {
            class130 class130_9;
            if (i_6 == 1) {
               class130_9 = class130.method2824(class14.field54, i_7, 0);
               if (class130_9 == null) {
                  field2603 = true;
                  return null;
               }

               class136_8 = class130_9.method2876(64, 768, -50, -10, -50);
            }

            if (i_6 == 2) {
               class130_9 = class27.method272(i_7, 555799614).method4703(-1865075990);
               if (class130_9 == null) {
                  field2603 = true;
                  return null;
               }

               class136_8 = class130_9.method2876(64, 768, -50, -10, -50);
            }

            if (i_6 == 3) {
               if (class223_4 == null) {
                  return null;
               }

               class130_9 = class223_4.method4015(1602412572);
               if (class130_9 == null) {
                  field2603 = true;
                  return null;
               }

               class136_8 = class130_9.method2876(64, 768, -50, -10, -50);
            }

            if (i_6 == 4) {
               class265 class265_10 = class66.method1223(i_7, -18525950);
               class130_9 = class265_10.method4678(10, 928211522);
               if (class130_9 == null) {
                  field2603 = true;
                  return null;
               }

               class136_8 = class130_9.method2876(class265_10.field3481 + 64, class265_10.field3474 + 768, -50, -10, -50);
            }

            field2714.method3374(class136_8, (long)(i_7 + (i_6 << 16)));
         }

         if (class269_1 != null) {
            class136_8 = class269_1.method4765(class136_8, i_2, (byte) 4);
         }

         return class136_8;
      }
   }

   public class335 method4051(int i_1, int i_2) {
      field2603 = false;
      if (i_1 >= 0 && i_1 < this.field2722.length) {
         int i_3 = this.field2722[i_1];
         if (i_3 == -1) {
            return null;
         } else {
            class335 class335_4 = (class335) field2580.method3376((long)i_3);
            if (class335_4 != null) {
               return class335_4;
            } else {
               class335_4 = class230.method4115(class1.field2, i_3, 0, (byte) 37);
               if (class335_4 != null) {
                  field2580.method3374(class335_4, (long)i_3);
               } else {
                  field2603 = true;
               }

               return class335_4;
            }
         }
      } else {
         return null;
      }
   }

   Object[] method4046(class310 class310_1, int i_2) {
      int i_3 = class310_1.method5661((byte) 25);
      if (i_3 == 0) {
         return null;
      } else {
         Object[] arr_4 = new Object[i_3];

         for (int i_5 = 0; i_5 < i_3; i_5++) {
            int i_6 = class310_1.method5661((byte) 24);
            if (i_6 == 0) {
               arr_4[i_5] = new Integer(class310_1.method5507(-1597790678));
            } else if (i_6 == 1) {
               arr_4[i_5] = class310_1.method5589(718762793);
            }
         }

         this.field2575 = true;
         return arr_4;
      }
   }

   public void method4054(int i_1, String string_2, int i_3) {
      if (this.field2662 == null || this.field2662.length <= i_1) {
         String[] arr_4 = new String[i_1 + 1];
         if (this.field2662 != null) {
            for (int i_5 = 0; i_5 < this.field2662.length; i_5++) {
               arr_4[i_5] = this.field2662[i_5];
            }
         }

         this.field2662 = arr_4;
      }

      this.field2662[i_1] = string_2;
   }

   public void method4074(int i_1, int i_2, byte b_3) {
      int i_4 = this.field2704[i_2];
      this.field2704[i_2] = this.field2704[i_1];
      this.field2704[i_1] = i_4;
      i_4 = this.field2711[i_2];
      this.field2711[i_2] = this.field2711[i_1];
      this.field2711[i_1] = i_4;
   }

   int[] method4078(class310 class310_1, int i_2) {
      int i_3 = class310_1.method5661((byte) 10);
      if (i_3 == 0) {
         return null;
      } else {
         int[] ints_4 = new int[i_3];

         for (int i_5 = 0; i_5 < i_3; i_5++) {
            ints_4[i_5] = class310_1.method5507(1922517041);
         }

         return ints_4;
      }
   }

   static void method4085(class246 class246_0, int i_1, int i_2, int i_3, byte b_4, boolean bool_5, int i_6) {
      long long_7 = (long)((i_1 << 16) + i_2);
      class243 class243_9 = (class243) class247.field3196.method5968(long_7);
      if (class243_9 == null) {
         class243_9 = (class243) class247.field3198.method5968(long_7);
         if (class243_9 == null) {
            class243_9 = (class243) class247.field3201.method5968(long_7);
            if (class243_9 != null) {
               if (bool_5) {
                  class243_9.method3622();
                  class247.field3196.method5956(class243_9, long_7);
                  --class247.field3211;
                  ++class247.field3193;
               }

            } else {
               if (!bool_5) {
                  class243_9 = (class243) class247.field3203.method5968(long_7);
                  if (class243_9 != null) {
                     return;
                  }
               }

               class243_9 = new class243();
               class243_9.field3154 = class246_0;
               class243_9.field3152 = i_3;
               class243_9.field3153 = b_4;
               if (bool_5) {
                  class247.field3196.method5956(class243_9, long_7);
                  ++class247.field3193;
               } else {
                  class247.field3200.method4792(class243_9);
                  class247.field3201.method5956(class243_9, long_7);
                  ++class247.field3211;
               }

            }
         }
      }
   }

   public static int method4044(long long_0) {
      return (int)(long_0 >>> 14 & 0x3L);
   }

}
