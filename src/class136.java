public class class136 extends class144 {

   static class136 field1728 = new class136();
   static byte[] field1685 = new byte[1];
   static class136 field1686 = new class136();
   static byte[] field1687 = new byte[1];
   static boolean[] field1749 = new boolean[4700];
   static boolean[] field1724 = new boolean[4700];
   static int[] field1725 = new int[4700];
   static int[] field1726 = new int[4700];
   static int[] field1727 = new int[4700];
   static int[] field1703 = new int[4700];
   static int[] field1729 = new int[4700];
   static int[] field1730 = new int[4700];
   static int[] field1732 = new int[1600];
   static int[][] field1733 = new int[1600][512];
   static int[] field1734 = new int[12];
   static int[][] field1735 = new int[12][2000];
   static int[] field1736 = new int[2000];
   static int[] field1737 = new int[2000];
   static int[] field1738 = new int[12];
   static int[] field1689 = new int[10];
   static int[] field1702 = new int[10];
   static int[] field1741 = new int[10];
   static boolean field1731 = true;
   static int[] field1697;
   static int[] field1743;
   static int[] field1750;
   static int[] field1751;
   int field1688 = 0;
   int field1748 = 0;
   byte field1722 = 0;
   int field1704 = 0;
   public boolean field1710 = false;
   int field1719 = -1;
   int field1720 = -1;
   int field1742 = -1;
   int[] field1716;
   int[] field1690;
   int[] field1691;
   int[] field1740;
   int[] field1694;
   int[] field1739;
   int[] field1696;
   int[] field1684;
   int[] field1698;
   byte[] field1699;
   byte[] field1693;
   short[] field1746;
   byte[] field1701;
   int[] field1723;
   int[] field1745;
   int[] field1707;
   int field1711;
   int field1712;
   int field1713;
   int field1715;
   int field1714;
   static int field1705;
   static int field1695;
   static int field1744;
   int[][] field1708;
   int[][] field1709;
   int field1706;
   int field1717;
   int field1718;

   static {
      field1697 = class139.field1797;
      field1743 = class139.field1780;
      field1750 = class139.field1800;
      field1751 = class139.field1803;
   }

   class136() {
   }

   public class136(class136[] arr_1, int i_2) {
      boolean bool_3 = false;
      boolean bool_4 = false;
      boolean bool_5 = false;
      boolean bool_6 = false;
      this.field1688 = 0;
      this.field1748 = 0;
      this.field1704 = 0;
      this.field1722 = -1;

      int i_7;
      class136 class136_8;
      for (i_7 = 0; i_7 < i_2; i_7++) {
         class136_8 = arr_1[i_7];
         if (class136_8 != null) {
            this.field1688 += class136_8.field1688;
            this.field1748 += class136_8.field1748;
            this.field1704 += class136_8.field1704;
            if (class136_8.field1699 != null) {
               bool_3 = true;
            } else {
               if (this.field1722 == -1) {
                  this.field1722 = class136_8.field1722;
               }

               if (this.field1722 != class136_8.field1722) {
                  bool_3 = true;
               }
            }

            bool_4 |= class136_8.field1693 != null;
            bool_5 |= class136_8.field1746 != null;
            bool_6 |= class136_8.field1701 != null;
         }
      }

      this.field1716 = new int[this.field1688];
      this.field1690 = new int[this.field1688];
      this.field1691 = new int[this.field1688];
      this.field1740 = new int[this.field1748];
      this.field1694 = new int[this.field1748];
      this.field1739 = new int[this.field1748];
      this.field1696 = new int[this.field1748];
      this.field1684 = new int[this.field1748];
      this.field1698 = new int[this.field1748];
      if (bool_3) {
         this.field1699 = new byte[this.field1748];
      }

      if (bool_4) {
         this.field1693 = new byte[this.field1748];
      }

      if (bool_5) {
         this.field1746 = new short[this.field1748];
      }

      if (bool_6) {
         this.field1701 = new byte[this.field1748];
      }

      if (this.field1704 > 0) {
         this.field1723 = new int[this.field1704];
         this.field1745 = new int[this.field1704];
         this.field1707 = new int[this.field1704];
      }

      this.field1688 = 0;
      this.field1748 = 0;
      this.field1704 = 0;

      for (i_7 = 0; i_7 < i_2; i_7++) {
         class136_8 = arr_1[i_7];
         if (class136_8 != null) {
            int i_9;
            for (i_9 = 0; i_9 < class136_8.field1748; i_9++) {
               this.field1740[this.field1748] = this.field1688 + class136_8.field1740[i_9];
               this.field1694[this.field1748] = this.field1688 + class136_8.field1694[i_9];
               this.field1739[this.field1748] = this.field1688 + class136_8.field1739[i_9];
               this.field1696[this.field1748] = class136_8.field1696[i_9];
               this.field1684[this.field1748] = class136_8.field1684[i_9];
               this.field1698[this.field1748] = class136_8.field1698[i_9];
               if (bool_3) {
                  if (class136_8.field1699 != null) {
                     this.field1699[this.field1748] = class136_8.field1699[i_9];
                  } else {
                     this.field1699[this.field1748] = class136_8.field1722;
                  }
               }

               if (bool_4 && class136_8.field1693 != null) {
                  this.field1693[this.field1748] = class136_8.field1693[i_9];
               }

               if (bool_5) {
                  if (class136_8.field1746 != null) {
                     this.field1746[this.field1748] = class136_8.field1746[i_9];
                  } else {
                     this.field1746[this.field1748] = -1;
                  }
               }

               if (bool_6) {
                  if (class136_8.field1701 != null && class136_8.field1701[i_9] != -1) {
                     this.field1701[this.field1748] = (byte)(this.field1704 + class136_8.field1701[i_9]);
                  } else {
                     this.field1701[this.field1748] = -1;
                  }
               }

               ++this.field1748;
            }

            for (i_9 = 0; i_9 < class136_8.field1704; i_9++) {
               this.field1723[this.field1704] = this.field1688 + class136_8.field1723[i_9];
               this.field1745[this.field1704] = this.field1688 + class136_8.field1745[i_9];
               this.field1707[this.field1704] = this.field1688 + class136_8.field1707[i_9];
               ++this.field1704;
            }

            for (i_9 = 0; i_9 < class136_8.field1688; i_9++) {
               this.field1716[this.field1688] = class136_8.field1716[i_9];
               this.field1690[this.field1688] = class136_8.field1690[i_9];
               this.field1691[this.field1688] = class136_8.field1691[i_9];
               ++this.field1688;
            }
         }
      }

   }

   public void method2946() {
      if (this.field1711 != 1) {
         this.field1711 = 1;
         super.field1894 = 0;
         this.field1712 = 0;
         this.field1713 = 0;

         for (int i_1 = 0; i_1 < this.field1688; i_1++) {
            int i_2 = this.field1716[i_1];
            int i_3 = this.field1690[i_1];
            int i_4 = this.field1691[i_1];
            if (-i_3 > super.field1894) {
               super.field1894 = -i_3;
            }

            if (i_3 > this.field1712) {
               this.field1712 = i_3;
            }

            int i_5 = i_2 * i_2 + i_4 * i_4;
            if (i_5 > this.field1713) {
               this.field1713 = i_5;
            }
         }

         this.field1713 = (int)(Math.sqrt((double)this.field1713) + 0.99D);
         this.field1715 = (int)(Math.sqrt((double)(this.field1713 * this.field1713 + super.field1894 * super.field1894)) + 0.99D);
         this.field1714 = this.field1715 + (int)(Math.sqrt((double)(this.field1713 * this.field1713 + this.field1712 * this.field1712)) + 0.99D);
      }
   }

   public void method2929(class147 class147_1, int i_2) {
      if (this.field1708 != null) {
         if (i_2 != -1) {
            class134 class134_3 = class147_1.field1909[i_2];
            class141 class141_4 = class134_3.field1654;
            field1705 = 0;
            field1695 = 0;
            field1744 = 0;

            for (int i_5 = 0; i_5 < class134_3.field1655; i_5++) {
               int i_6 = class134_3.field1656[i_5];
               this.method2931(class141_4.field1818[i_6], class141_4.field1819[i_6], class134_3.field1650[i_5], class134_3.field1658[i_5], class134_3.field1653[i_5]);
            }

            this.method2928();
         }
      }
   }

   void method2928() {
      this.field1711 = 0;
      this.field1719 = -1;
   }

   final void method2950(int i_1) {
      int i_2 = class139.field1785;
      int i_3 = class139.field1792;
      int i_4 = 0;
      int i_5 = this.field1740[i_1];
      int i_6 = this.field1694[i_1];
      int i_7 = this.field1739[i_1];
      int i_8 = field1730[i_5];
      int i_9 = field1730[i_6];
      int i_10 = field1730[i_7];
      if (this.field1693 == null) {
         class139.field1783 = 0;
      } else {
         class139.field1783 = this.field1693[i_1] & 0xff;
      }

      int i_11;
      int i_12;
      int i_13;
      int i_14;
      if (i_8 >= 50) {
         field1689[i_4] = field1725[i_5];
         field1702[i_4] = field1726[i_5];
         field1741[i_4++] = this.field1696[i_1];
      } else {
         i_11 = field1703[i_5];
         i_12 = field1729[i_5];
         i_13 = this.field1696[i_1];
         if (i_10 >= 50) {
            i_14 = field1751[i_10 - i_8] * (50 - i_8);
            field1689[i_4] = i_2 + class139.field1796 * (i_11 + ((field1703[i_7] - i_11) * i_14 >> 16)) / 50;
            field1702[i_4] = i_3 + class139.field1796 * (i_12 + ((field1729[i_7] - i_12) * i_14 >> 16)) / 50;
            field1741[i_4++] = i_13 + ((this.field1698[i_1] - i_13) * i_14 >> 16);
         }

         if (i_9 >= 50) {
            i_14 = field1751[i_9 - i_8] * (50 - i_8);
            field1689[i_4] = i_2 + class139.field1796 * (i_11 + ((field1703[i_6] - i_11) * i_14 >> 16)) / 50;
            field1702[i_4] = i_3 + class139.field1796 * (i_12 + ((field1729[i_6] - i_12) * i_14 >> 16)) / 50;
            field1741[i_4++] = i_13 + ((this.field1684[i_1] - i_13) * i_14 >> 16);
         }
      }

      if (i_9 >= 50) {
         field1689[i_4] = field1725[i_6];
         field1702[i_4] = field1726[i_6];
         field1741[i_4++] = this.field1684[i_1];
      } else {
         i_11 = field1703[i_6];
         i_12 = field1729[i_6];
         i_13 = this.field1684[i_1];
         if (i_8 >= 50) {
            i_14 = field1751[i_8 - i_9] * (50 - i_9);
            field1689[i_4] = i_2 + class139.field1796 * (i_11 + ((field1703[i_5] - i_11) * i_14 >> 16)) / 50;
            field1702[i_4] = i_3 + class139.field1796 * (i_12 + ((field1729[i_5] - i_12) * i_14 >> 16)) / 50;
            field1741[i_4++] = i_13 + ((this.field1696[i_1] - i_13) * i_14 >> 16);
         }

         if (i_10 >= 50) {
            i_14 = field1751[i_10 - i_9] * (50 - i_9);
            field1689[i_4] = i_2 + class139.field1796 * (i_11 + ((field1703[i_7] - i_11) * i_14 >> 16)) / 50;
            field1702[i_4] = i_3 + class139.field1796 * (i_12 + ((field1729[i_7] - i_12) * i_14 >> 16)) / 50;
            field1741[i_4++] = i_13 + ((this.field1698[i_1] - i_13) * i_14 >> 16);
         }
      }

      if (i_10 >= 50) {
         field1689[i_4] = field1725[i_7];
         field1702[i_4] = field1726[i_7];
         field1741[i_4++] = this.field1698[i_1];
      } else {
         i_11 = field1703[i_7];
         i_12 = field1729[i_7];
         i_13 = this.field1698[i_1];
         if (i_9 >= 50) {
            i_14 = field1751[i_9 - i_10] * (50 - i_10);
            field1689[i_4] = i_2 + class139.field1796 * (i_11 + ((field1703[i_6] - i_11) * i_14 >> 16)) / 50;
            field1702[i_4] = i_3 + class139.field1796 * (i_12 + ((field1729[i_6] - i_12) * i_14 >> 16)) / 50;
            field1741[i_4++] = i_13 + ((this.field1684[i_1] - i_13) * i_14 >> 16);
         }

         if (i_8 >= 50) {
            i_14 = field1751[i_8 - i_10] * (50 - i_10);
            field1689[i_4] = i_2 + class139.field1796 * (i_11 + ((field1703[i_5] - i_11) * i_14 >> 16)) / 50;
            field1702[i_4] = i_3 + class139.field1796 * (i_12 + ((field1729[i_5] - i_12) * i_14 >> 16)) / 50;
            field1741[i_4++] = i_13 + ((this.field1696[i_1] - i_13) * i_14 >> 16);
         }
      }

      i_11 = field1689[0];
      i_12 = field1689[1];
      i_13 = field1689[2];
      i_14 = field1702[0];
      int i_15 = field1702[1];
      int i_16 = field1702[2];
      class139.field1791 = false;
      int i_17;
      int i_18;
      int i_19;
      int i_20;
      if (i_4 == 3) {
         if (i_11 < 0 || i_12 < 0 || i_13 < 0 || i_11 > class139.field1793 || i_12 > class139.field1793 || i_13 > class139.field1793) {
            class139.field1791 = true;
         }

         if (this.field1746 != null && this.field1746[i_1] != -1) {
            if (this.field1701 != null && this.field1701[i_1] != -1) {
               i_20 = this.field1701[i_1] & 0xff;
               i_17 = this.field1723[i_20];
               i_18 = this.field1745[i_20];
               i_19 = this.field1707[i_20];
            } else {
               i_17 = i_5;
               i_18 = i_6;
               i_19 = i_7;
            }

            if (this.field1698[i_1] == -1) {
               class139.method3055(i_14, i_15, i_16, i_11, i_12, i_13, this.field1696[i_1], this.field1696[i_1], this.field1696[i_1], field1703[i_17], field1703[i_18], field1703[i_19], field1729[i_17], field1729[i_18], field1729[i_19], field1730[i_17], field1730[i_18], field1730[i_19], this.field1746[i_1]);
            } else {
               class139.method3055(i_14, i_15, i_16, i_11, i_12, i_13, field1741[0], field1741[1], field1741[2], field1703[i_17], field1703[i_18], field1703[i_19], field1729[i_17], field1729[i_18], field1729[i_19], field1730[i_17], field1730[i_18], field1730[i_19], this.field1746[i_1]);
            }
         } else if (this.field1698[i_1] == -1) {
            class139.method3053(i_14, i_15, i_16, i_11, i_12, i_13, field1750[this.field1696[i_1]]);
         } else {
            class139.method3051(i_14, i_15, i_16, i_11, i_12, i_13, field1741[0], field1741[1], field1741[2]);
         }
      }

      if (i_4 == 4) {
         if (i_11 < 0 || i_12 < 0 || i_13 < 0 || i_11 > class139.field1793 || i_12 > class139.field1793 || i_13 > class139.field1793 || field1689[3] < 0 || field1689[3] > class139.field1793) {
            class139.field1791 = true;
         }

         if (this.field1746 != null && this.field1746[i_1] != -1) {
            if (this.field1701 != null && this.field1701[i_1] != -1) {
               i_20 = this.field1701[i_1] & 0xff;
               i_17 = this.field1723[i_20];
               i_18 = this.field1745[i_20];
               i_19 = this.field1707[i_20];
            } else {
               i_17 = i_5;
               i_18 = i_6;
               i_19 = i_7;
            }

            short s_21 = this.field1746[i_1];
            if (this.field1698[i_1] == -1) {
               class139.method3055(i_14, i_15, i_16, i_11, i_12, i_13, this.field1696[i_1], this.field1696[i_1], this.field1696[i_1], field1703[i_17], field1703[i_18], field1703[i_19], field1729[i_17], field1729[i_18], field1729[i_19], field1730[i_17], field1730[i_18], field1730[i_19], s_21);
               class139.method3055(i_14, i_16, field1702[3], i_11, i_13, field1689[3], this.field1696[i_1], this.field1696[i_1], this.field1696[i_1], field1703[i_17], field1703[i_18], field1703[i_19], field1729[i_17], field1729[i_18], field1729[i_19], field1730[i_17], field1730[i_18], field1730[i_19], s_21);
            } else {
               class139.method3055(i_14, i_15, i_16, i_11, i_12, i_13, field1741[0], field1741[1], field1741[2], field1703[i_17], field1703[i_18], field1703[i_19], field1729[i_17], field1729[i_18], field1729[i_19], field1730[i_17], field1730[i_18], field1730[i_19], s_21);
               class139.method3055(i_14, i_16, field1702[3], i_11, i_13, field1689[3], field1741[0], field1741[2], field1741[3], field1703[i_17], field1703[i_18], field1703[i_19], field1729[i_17], field1729[i_18], field1729[i_19], field1730[i_17], field1730[i_18], field1730[i_19], s_21);
            }
         } else if (this.field1698[i_1] == -1) {
            i_17 = field1750[this.field1696[i_1]];
            class139.method3053(i_14, i_15, i_16, i_11, i_12, i_13, i_17);
            class139.method3053(i_14, i_16, field1702[3], i_11, i_13, field1689[3], i_17);
         } else {
            class139.method3051(i_14, i_15, i_16, i_11, i_12, i_13, field1741[0], field1741[1], field1741[2]);
            class139.method3051(i_14, i_16, field1702[3], i_11, i_13, field1689[3], field1741[0], field1741[2], field1741[3]);
         }
      }

   }

   class136 method2920(boolean bool_1, class136 class136_2, byte[] bytes_3) {
      class136_2.field1688 = this.field1688;
      class136_2.field1748 = this.field1748;
      class136_2.field1704 = this.field1704;
      if (class136_2.field1716 == null || class136_2.field1716.length < this.field1688) {
         class136_2.field1716 = new int[this.field1688 + 100];
         class136_2.field1690 = new int[this.field1688 + 100];
         class136_2.field1691 = new int[this.field1688 + 100];
      }

      int i_4;
      for (i_4 = 0; i_4 < this.field1688; i_4++) {
         class136_2.field1716[i_4] = this.field1716[i_4];
         class136_2.field1690[i_4] = this.field1690[i_4];
         class136_2.field1691[i_4] = this.field1691[i_4];
      }

      if (bool_1) {
         class136_2.field1693 = this.field1693;
      } else {
         class136_2.field1693 = bytes_3;
         if (this.field1693 == null) {
            for (i_4 = 0; i_4 < this.field1748; i_4++) {
               class136_2.field1693[i_4] = 0;
            }
         } else {
            for (i_4 = 0; i_4 < this.field1748; i_4++) {
               class136_2.field1693[i_4] = this.field1693[i_4];
            }
         }
      }

      class136_2.field1740 = this.field1740;
      class136_2.field1694 = this.field1694;
      class136_2.field1739 = this.field1739;
      class136_2.field1696 = this.field1696;
      class136_2.field1684 = this.field1684;
      class136_2.field1698 = this.field1698;
      class136_2.field1699 = this.field1699;
      class136_2.field1701 = this.field1701;
      class136_2.field1746 = this.field1746;
      class136_2.field1722 = this.field1722;
      class136_2.field1723 = this.field1723;
      class136_2.field1745 = this.field1745;
      class136_2.field1707 = this.field1707;
      class136_2.field1708 = this.field1708;
      class136_2.field1709 = this.field1709;
      class136_2.field1710 = this.field1710;
      class136_2.method2928();
      return class136_2;
   }

   void method2924(int i_1) {
      if (this.field1719 == -1) {
         int i_2 = 0;
         int i_3 = 0;
         int i_4 = 0;
         int i_5 = 0;
         int i_6 = 0;
         int i_7 = 0;
         int i_8 = field1743[i_1];
         int i_9 = field1697[i_1];

         for (int i_10 = 0; i_10 < this.field1688; i_10++) {
            int i_11 = class139.method3060(this.field1716[i_10], this.field1691[i_10], i_8, i_9);
            int i_12 = this.field1690[i_10];
            int i_13 = class139.method3115(this.field1716[i_10], this.field1691[i_10], i_8, i_9);
            if (i_11 < i_2) {
               i_2 = i_11;
            }

            if (i_11 > i_5) {
               i_5 = i_11;
            }

            if (i_12 < i_3) {
               i_3 = i_12;
            }

            if (i_12 > i_6) {
               i_6 = i_12;
            }

            if (i_13 < i_4) {
               i_4 = i_13;
            }

            if (i_13 > i_7) {
               i_7 = i_13;
            }
         }

         this.field1706 = (i_5 + i_2) / 2;
         this.field1717 = (i_6 + i_3) / 2;
         this.field1718 = (i_7 + i_4) / 2;
         this.field1719 = (i_5 - i_2 + 1) / 2;
         this.field1720 = (i_6 - i_3 + 1) / 2;
         this.field1742 = (i_7 - i_4 + 1) / 2;
         if (this.field1719 < 32) {
            this.field1719 = 32;
         }

         if (this.field1742 < 32) {
            this.field1742 = 32;
         }

         if (this.field1710) {
            this.field1719 += 8;
            this.field1742 += 8;
         }

      }
   }

   public class136 method2921(boolean bool_1) {
      if (!bool_1 && field1685.length < this.field1748) {
         field1685 = new byte[this.field1748 + 100];
      }

      return this.method2920(bool_1, field1728, field1685);
   }

   void method2919() {
      if (this.field1711 != 2) {
         this.field1711 = 2;
         this.field1713 = 0;

         for (int i_1 = 0; i_1 < this.field1688; i_1++) {
            int i_2 = this.field1716[i_1];
            int i_3 = this.field1690[i_1];
            int i_4 = this.field1691[i_1];
            int i_5 = i_2 * i_2 + i_4 * i_4 + i_3 * i_3;
            if (i_5 > this.field1713) {
               this.field1713 = i_5;
            }
         }

         this.field1713 = (int)(Math.sqrt((double)this.field1713) + 0.99D);
         this.field1715 = this.field1713;
         this.field1714 = this.field1713 + this.field1713;
      }
   }

   public class136 method2922(boolean bool_1) {
      if (!bool_1 && field1687.length < this.field1748) {
         field1687 = new byte[this.field1748 + 100];
      }

      return this.method2920(bool_1, field1686, field1687);
   }

   public void method2935(int i_1) {
      int i_2 = field1697[i_1];
      int i_3 = field1743[i_1];

      for (int i_4 = 0; i_4 < this.field1688; i_4++) {
         int i_5 = i_3 * this.field1690[i_4] - i_2 * this.field1691[i_4] >> 16;
         this.field1691[i_4] = i_2 * this.field1690[i_4] + i_3 * this.field1691[i_4] >> 16;
         this.field1690[i_4] = i_5;
      }

      this.method2928();
   }

   public class136 method2977(int[][] ints_1, int i_2, int i_3, int i_4, boolean bool_5, int i_6) {
      this.method2946();
      int i_7 = i_2 - this.field1713;
      int i_8 = i_2 + this.field1713;
      int i_9 = i_4 - this.field1713;
      int i_10 = i_4 + this.field1713;
      if (i_7 >= 0 && i_8 + 128 >> 7 < ints_1.length && i_9 >= 0 && i_10 + 128 >> 7 < ints_1[0].length) {
         i_7 >>= 7;
         i_8 = i_8 + 127 >> 7;
         i_9 >>= 7;
         i_10 = i_10 + 127 >> 7;
         if (i_3 == ints_1[i_7][i_9] && i_3 == ints_1[i_8][i_9] && i_3 == ints_1[i_7][i_10] && i_3 == ints_1[i_8][i_10]) {
            return this;
         } else {
            class136 class136_11;
            if (bool_5) {
               class136_11 = new class136();
               class136_11.field1688 = this.field1688;
               class136_11.field1748 = this.field1748;
               class136_11.field1704 = this.field1704;
               class136_11.field1716 = this.field1716;
               class136_11.field1691 = this.field1691;
               class136_11.field1740 = this.field1740;
               class136_11.field1694 = this.field1694;
               class136_11.field1739 = this.field1739;
               class136_11.field1696 = this.field1696;
               class136_11.field1684 = this.field1684;
               class136_11.field1698 = this.field1698;
               class136_11.field1699 = this.field1699;
               class136_11.field1693 = this.field1693;
               class136_11.field1701 = this.field1701;
               class136_11.field1746 = this.field1746;
               class136_11.field1722 = this.field1722;
               class136_11.field1723 = this.field1723;
               class136_11.field1745 = this.field1745;
               class136_11.field1707 = this.field1707;
               class136_11.field1708 = this.field1708;
               class136_11.field1709 = this.field1709;
               class136_11.field1710 = this.field1710;
               class136_11.field1690 = new int[class136_11.field1688];
            } else {
               class136_11 = this;
            }

            int i_12;
            int i_13;
            int i_14;
            int i_15;
            int i_16;
            int i_17;
            int i_18;
            int i_19;
            int i_20;
            int i_21;
            if (i_6 == 0) {
               for (i_12 = 0; i_12 < class136_11.field1688; i_12++) {
                  i_13 = i_2 + this.field1716[i_12];
                  i_14 = i_4 + this.field1691[i_12];
                  i_15 = i_13 & 0x7f;
                  i_16 = i_14 & 0x7f;
                  i_17 = i_13 >> 7;
                  i_18 = i_14 >> 7;
                  i_19 = ints_1[i_17][i_18] * (128 - i_15) + ints_1[i_17 + 1][i_18] * i_15 >> 7;
                  i_20 = ints_1[i_17][i_18 + 1] * (128 - i_15) + i_15 * ints_1[i_17 + 1][i_18 + 1] >> 7;
                  i_21 = i_19 * (128 - i_16) + i_20 * i_16 >> 7;
                  class136_11.field1690[i_12] = i_21 + this.field1690[i_12] - i_3;
               }
            } else {
               for (i_12 = 0; i_12 < class136_11.field1688; i_12++) {
                  i_13 = (-this.field1690[i_12] << 16) / super.field1894;
                  if (i_13 < i_6) {
                     i_14 = i_2 + this.field1716[i_12];
                     i_15 = i_4 + this.field1691[i_12];
                     i_16 = i_14 & 0x7f;
                     i_17 = i_15 & 0x7f;
                     i_18 = i_14 >> 7;
                     i_19 = i_15 >> 7;
                     i_20 = ints_1[i_18][i_19] * (128 - i_16) + ints_1[i_18 + 1][i_19] * i_16 >> 7;
                     i_21 = ints_1[i_18][i_19 + 1] * (128 - i_16) + i_16 * ints_1[i_18 + 1][i_19 + 1] >> 7;
                     int i_22 = i_20 * (128 - i_17) + i_21 * i_17 >> 7;
                     class136_11.field1690[i_12] = (i_6 - i_13) * (i_22 - i_3) / i_6 + this.field1690[i_12];
                  }
               }
            }

            class136_11.method2928();
            return class136_11;
         }
      } else {
         return this;
      }
   }

   void vmethod3310(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, long long_9) {
      field1732[0] = -1;
      if (this.field1711 != 1) {
         this.method2946();
      }

      this.method2924(i_1);
      int i_11 = i_5 * i_8 - i_4 * i_6 >> 16;
      int i_12 = i_2 * i_7 + i_3 * i_11 >> 16;
      int i_13 = i_3 * this.field1713 >> 16;
      int i_14 = i_12 + i_13;
      if (i_14 > 50 && i_12 < 3500) {
         int i_15 = i_8 * i_4 + i_5 * i_6 >> 16;
         int i_16 = (i_15 - this.field1713) * class139.field1796;
         if (i_16 / i_14 < class139.field1805) {
            int i_17 = (i_15 + this.field1713) * class139.field1796;
            if (i_17 / i_14 > class139.field1795) {
               int i_18 = i_3 * i_7 - i_11 * i_2 >> 16;
               int i_19 = i_2 * this.field1713 >> 16;
               int i_20 = (i_18 + i_19) * class139.field1796;
               if (i_20 / i_14 > class139.field1798) {
                  int i_21 = (i_3 * super.field1894 >> 16) + i_19;
                  int i_22 = (i_18 - i_21) * class139.field1796;
                  if (i_22 / i_14 < class139.field1790) {
                     int i_23 = i_13 + (i_2 * super.field1894 >> 16);
                     boolean bool_24 = false;
                     boolean bool_25 = false;
                     if (i_12 - i_23 <= 50) {
                        bool_25 = true;
                     }

                     boolean bool_26 = bool_25 || this.field1704 > 0;
                     int i_27 = class84.method1965(725701035);
                     int i_28 = class137.field1755;
                     boolean bool_30 = class302.method5315(1883982153);
                     boolean bool_31 = class83.method1915(long_9);
                     boolean bool_32 = false;
                     int i_37;
                     int i_38;
                     int i_39;
                     int i_40;
                     int i_48;
                     int i_54;
                     int i_55;
                     int i_56;
                     if (bool_31 && bool_30) {
                        boolean bool_33 = false;
                        if (field1731) {
                           boolean bool_36 = class137.field1756;
                           boolean bool_34;
                           if (!bool_36) {
                              bool_34 = false;
                           } else {
                              int i_43;
                              int i_44;
                              int i_45;
                              int i_57;
                              if (!class137.field1757) {
                                 i_37 = class142.field1850;
                                 i_38 = class142.field1851;
                                 i_39 = class142.field1862;
                                 i_40 = class142.field1855;
                                 byte b_41 = 50;
                                 short s_42 = 3500;
                                 i_43 = (class137.field1764 - class139.field1785) * b_41 / class139.field1796;
                                 i_44 = (class137.field1755 - class139.field1792) * b_41 / class139.field1796;
                                 i_45 = (class137.field1764 - class139.field1785) * s_42 / class139.field1796;
                                 int i_46 = (class137.field1755 - class139.field1792) * s_42 / class139.field1796;
                                 int i_47 = class139.method3064(i_44, b_41, i_38, i_37);
                                 i_56 = class139.method3066(i_44, b_41, i_38, i_37);
                                 i_44 = i_47;
                                 i_47 = class139.method3064(i_46, s_42, i_38, i_37);
                                 i_57 = class139.method3066(i_46, s_42, i_38, i_37);
                                 i_46 = i_47;
                                 i_47 = class139.method3062(i_43, i_56, i_40, i_39);
                                 i_56 = class139.method3105(i_43, i_56, i_40, i_39);
                                 i_43 = i_47;
                                 i_47 = class139.method3062(i_45, i_57, i_40, i_39);
                                 i_57 = class139.method3105(i_45, i_57, i_40, i_39);
                                 class137.field1758 = (i_43 + i_47) / 2;
                                 class137.field1759 = (i_44 + i_46) / 2;
                                 class137.field1765 = (i_56 + i_57) / 2;
                                 class137.field1761 = (i_47 - i_43) / 2;
                                 class137.field1762 = (i_46 - i_44) / 2;
                                 class232.field2778 = (i_57 - i_56) / 2;
                                 class83.field1065 = Math.abs(class137.field1761);
                                 class220.field2535 = Math.abs(class137.field1762);
                                 class27.field146 = Math.abs(class232.field2778);
                              }

                              i_37 = this.field1706 + i_6;
                              i_38 = i_7 + this.field1717;
                              i_39 = i_8 + this.field1718;
                              i_40 = this.field1719;
                              i_56 = this.field1720;
                              i_57 = this.field1742;
                              i_43 = class137.field1758 - i_37;
                              i_44 = class137.field1759 - i_38;
                              i_45 = class137.field1765 - i_39;
                              if (Math.abs(i_43) > i_40 + class83.field1065) {
                                 bool_34 = false;
                              } else if (Math.abs(i_44) > i_56 + class220.field2535) {
                                 bool_34 = false;
                              } else if (Math.abs(i_45) > i_57 + class27.field146) {
                                 bool_34 = false;
                              } else if (Math.abs(i_45 * class137.field1762 - i_44 * class232.field2778) > i_56 * class27.field146 + i_57 * class220.field2535) {
                                 bool_34 = false;
                              } else if (Math.abs(i_43 * class232.field2778 - i_45 * class137.field1761) > i_40 * class27.field146 + i_57 * class83.field1065) {
                                 bool_34 = false;
                              } else if (Math.abs(i_44 * class137.field1761 - i_43 * class137.field1762) > i_40 * class220.field2535 + i_56 * class83.field1065) {
                                 bool_34 = false;
                              } else {
                                 bool_34 = true;
                              }
                           }

                           bool_33 = bool_34;
                        } else {
                           i_55 = i_12 - i_13;
                           if (i_55 <= 50) {
                              i_55 = 50;
                           }

                           if (i_15 > 0) {
                              i_16 /= i_14;
                              i_17 /= i_55;
                           } else {
                              i_17 /= i_14;
                              i_16 /= i_55;
                           }

                           if (i_18 > 0) {
                              i_22 /= i_14;
                              i_20 /= i_55;
                           } else {
                              i_20 /= i_14;
                              i_22 /= i_55;
                           }

                           i_48 = i_27 - class139.field1785;
                           i_54 = i_28 - class139.field1792;
                           if (i_48 > i_16 && i_48 < i_17 && i_54 > i_22 && i_54 < i_20) {
                              bool_33 = true;
                           }
                        }

                        if (bool_33) {
                           if (this.field1710) {
                              class137.field1768[++class137.field1763 - 1] = long_9;
                           } else {
                              bool_32 = true;
                           }
                        }
                     }

                     int i_53 = class139.field1785;
                     i_55 = class139.field1792;
                     i_48 = 0;
                     i_54 = 0;
                     if (i_1 != 0) {
                        i_48 = field1697[i_1];
                        i_54 = field1743[i_1];
                     }

                     for (i_37 = 0; i_37 < this.field1688; i_37++) {
                        i_38 = this.field1716[i_37];
                        i_39 = this.field1690[i_37];
                        i_40 = this.field1691[i_37];
                        if (i_1 != 0) {
                           i_56 = i_40 * i_48 + i_38 * i_54 >> 16;
                           i_40 = i_40 * i_54 - i_38 * i_48 >> 16;
                           i_38 = i_56;
                        }

                        i_38 += i_6;
                        i_39 += i_7;
                        i_40 += i_8;
                        i_56 = i_40 * i_4 + i_5 * i_38 >> 16;
                        i_40 = i_5 * i_40 - i_38 * i_4 >> 16;
                        i_38 = i_56;
                        i_56 = i_3 * i_39 - i_40 * i_2 >> 16;
                        i_40 = i_39 * i_2 + i_3 * i_40 >> 16;
                        field1727[i_37] = i_40 - i_12;
                        if (i_40 >= 50) {
                           field1725[i_37] = i_38 * class139.field1796 / i_40 + i_53;
                           field1726[i_37] = i_56 * class139.field1796 / i_40 + i_55;
                        } else {
                           field1725[i_37] = -5000;
                           bool_24 = true;
                        }

                        if (bool_26) {
                           field1703[i_37] = i_38;
                           field1729[i_37] = i_56;
                           field1730[i_37] = i_40;
                        }
                     }

                     try {
                        this.method2934(bool_24, bool_32, this.field1710, long_9);
                     } catch (Exception exception_52) {
                        ;
                     }

                  }
               }
            }
         }
      }
   }

   public void method3006() {
      for (int i_1 = 0; i_1 < this.field1688; i_1++) {
         int i_2 = this.field1691[i_1];
         this.field1691[i_1] = this.field1716[i_1];
         this.field1716[i_1] = -i_2;
      }

      this.method2928();
   }

   public void method2937(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < this.field1688; i_4++) {
         this.field1716[i_4] = this.field1716[i_4] * i_1 / 128;
         this.field1690[i_4] = i_2 * this.field1690[i_4] / 128;
         this.field1691[i_4] = i_3 * this.field1691[i_4] / 128;
      }

      this.method2928();
   }

   public void method2933() {
      for (int i_1 = 0; i_1 < this.field1688; i_1++) {
         this.field1716[i_1] = -this.field1716[i_1];
         this.field1691[i_1] = -this.field1691[i_1];
      }

      this.method2928();
   }

   public void method2945() {
      for (int i_1 = 0; i_1 < this.field1688; i_1++) {
         int i_2 = this.field1716[i_1];
         this.field1716[i_1] = this.field1691[i_1];
         this.field1691[i_1] = -i_2;
      }

      this.method2928();
   }

   final void method2936(int i_1) {
      if (field1724[i_1]) {
         this.method2950(i_1);
      } else {
         int i_2 = this.field1740[i_1];
         int i_3 = this.field1694[i_1];
         int i_4 = this.field1739[i_1];
         class139.field1791 = field1749[i_1];
         if (this.field1693 == null) {
            class139.field1783 = 0;
         } else {
            class139.field1783 = this.field1693[i_1] & 0xff;
         }

         if (this.field1746 != null && this.field1746[i_1] != -1) {
            int i_5;
            int i_6;
            int i_7;
            if (this.field1701 != null && this.field1701[i_1] != -1) {
               int i_8 = this.field1701[i_1] & 0xff;
               i_5 = this.field1723[i_8];
               i_6 = this.field1745[i_8];
               i_7 = this.field1707[i_8];
            } else {
               i_5 = i_2;
               i_6 = i_3;
               i_7 = i_4;
            }

            if (this.field1698[i_1] == -1) {
               class139.method3055(field1726[i_2], field1726[i_3], field1726[i_4], field1725[i_2], field1725[i_3], field1725[i_4], this.field1696[i_1], this.field1696[i_1], this.field1696[i_1], field1703[i_5], field1703[i_6], field1703[i_7], field1729[i_5], field1729[i_6], field1729[i_7], field1730[i_5], field1730[i_6], field1730[i_7], this.field1746[i_1]);
            } else {
               class139.method3055(field1726[i_2], field1726[i_3], field1726[i_4], field1725[i_2], field1725[i_3], field1725[i_4], this.field1696[i_1], this.field1684[i_1], this.field1698[i_1], field1703[i_5], field1703[i_6], field1703[i_7], field1729[i_5], field1729[i_6], field1729[i_7], field1730[i_5], field1730[i_6], field1730[i_7], this.field1746[i_1]);
            }
         } else if (this.field1698[i_1] == -1) {
            class139.method3053(field1726[i_2], field1726[i_3], field1726[i_4], field1725[i_2], field1725[i_3], field1725[i_4], field1750[this.field1696[i_1]]);
         } else {
            class139.method3051(field1726[i_2], field1726[i_3], field1726[i_4], field1725[i_2], field1725[i_3], field1725[i_4], this.field1696[i_1], this.field1684[i_1], this.field1698[i_1]);
         }

      }
   }

   public void method2930(class147 class147_1, int i_2, class147 class147_3, int i_4, int[] ints_5) {
      if (i_2 != -1) {
         if (ints_5 != null && i_4 != -1) {
            class134 class134_6 = class147_1.field1909[i_2];
            class134 class134_7 = class147_3.field1909[i_4];
            class141 class141_8 = class134_6.field1654;
            field1705 = 0;
            field1695 = 0;
            field1744 = 0;
            byte b_9 = 0;
            int i_13 = b_9 + 1;
            int i_10 = ints_5[b_9];

            int i_11;
            int i_12;
            for (i_11 = 0; i_11 < class134_6.field1655; i_11++) {
               for (i_12 = class134_6.field1656[i_11]; i_12 > i_10; i_10 = ints_5[i_13++]) {
                  ;
               }

               if (i_12 != i_10 || class141_8.field1818[i_12] == 0) {
                  this.method2931(class141_8.field1818[i_12], class141_8.field1819[i_12], class134_6.field1650[i_11], class134_6.field1658[i_11], class134_6.field1653[i_11]);
               }
            }

            field1705 = 0;
            field1695 = 0;
            field1744 = 0;
            b_9 = 0;
            i_13 = b_9 + 1;
            i_10 = ints_5[b_9];

            for (i_11 = 0; i_11 < class134_7.field1655; i_11++) {
               for (i_12 = class134_7.field1656[i_11]; i_12 > i_10; i_10 = ints_5[i_13++]) {
                  ;
               }

               if (i_12 == i_10 || class141_8.field1818[i_12] == 0) {
                  this.method2931(class141_8.field1818[i_12], class141_8.field1819[i_12], class134_7.field1650[i_11], class134_7.field1658[i_11], class134_7.field1653[i_11]);
               }
            }

            this.method2928();
         } else {
            this.method2929(class147_1, i_2);
         }
      }
   }

   void method2931(int i_1, int[] ints_2, int i_3, int i_4, int i_5) {
      int i_6 = ints_2.length;
      int i_7;
      int i_8;
      int i_11;
      int i_12;
      if (i_1 == 0) {
         i_7 = 0;
         field1705 = 0;
         field1695 = 0;
         field1744 = 0;

         for (i_8 = 0; i_8 < i_6; i_8++) {
            int i_9 = ints_2[i_8];
            if (i_9 < this.field1708.length) {
               int[] ints_10 = this.field1708[i_9];

               for (i_11 = 0; i_11 < ints_10.length; i_11++) {
                  i_12 = ints_10[i_11];
                  field1705 += this.field1716[i_12];
                  field1695 += this.field1690[i_12];
                  field1744 += this.field1691[i_12];
                  ++i_7;
               }
            }
         }

         if (i_7 > 0) {
            field1705 = i_3 + field1705 / i_7;
            field1695 = i_4 + field1695 / i_7;
            field1744 = i_5 + field1744 / i_7;
         } else {
            field1705 = i_3;
            field1695 = i_4;
            field1744 = i_5;
         }

      } else {
         int[] ints_18;
         int i_19;
         if (i_1 == 1) {
            for (i_7 = 0; i_7 < i_6; i_7++) {
               i_8 = ints_2[i_7];
               if (i_8 < this.field1708.length) {
                  ints_18 = this.field1708[i_8];

                  for (i_19 = 0; i_19 < ints_18.length; i_19++) {
                     i_11 = ints_18[i_19];
                     this.field1716[i_11] += i_3;
                     this.field1690[i_11] += i_4;
                     this.field1691[i_11] += i_5;
                  }
               }
            }

         } else if (i_1 == 2) {
            for (i_7 = 0; i_7 < i_6; i_7++) {
               i_8 = ints_2[i_7];
               if (i_8 < this.field1708.length) {
                  ints_18 = this.field1708[i_8];

                  for (i_19 = 0; i_19 < ints_18.length; i_19++) {
                     i_11 = ints_18[i_19];
                     this.field1716[i_11] -= field1705;
                     this.field1690[i_11] -= field1695;
                     this.field1691[i_11] -= field1744;
                     i_12 = (i_3 & 0xff) * 8;
                     int i_13 = (i_4 & 0xff) * 8;
                     int i_14 = (i_5 & 0xff) * 8;
                     int i_15;
                     int i_16;
                     int i_17;
                     if (i_14 != 0) {
                        i_15 = field1697[i_14];
                        i_16 = field1743[i_14];
                        i_17 = i_15 * this.field1690[i_11] + i_16 * this.field1716[i_11] >> 16;
                        this.field1690[i_11] = i_16 * this.field1690[i_11] - i_15 * this.field1716[i_11] >> 16;
                        this.field1716[i_11] = i_17;
                     }

                     if (i_12 != 0) {
                        i_15 = field1697[i_12];
                        i_16 = field1743[i_12];
                        i_17 = i_16 * this.field1690[i_11] - i_15 * this.field1691[i_11] >> 16;
                        this.field1691[i_11] = i_15 * this.field1690[i_11] + i_16 * this.field1691[i_11] >> 16;
                        this.field1690[i_11] = i_17;
                     }

                     if (i_13 != 0) {
                        i_15 = field1697[i_13];
                        i_16 = field1743[i_13];
                        i_17 = i_15 * this.field1691[i_11] + i_16 * this.field1716[i_11] >> 16;
                        this.field1691[i_11] = i_16 * this.field1691[i_11] - i_15 * this.field1716[i_11] >> 16;
                        this.field1716[i_11] = i_17;
                     }

                     this.field1716[i_11] += field1705;
                     this.field1690[i_11] += field1695;
                     this.field1691[i_11] += field1744;
                  }
               }
            }

         } else if (i_1 == 3) {
            for (i_7 = 0; i_7 < i_6; i_7++) {
               i_8 = ints_2[i_7];
               if (i_8 < this.field1708.length) {
                  ints_18 = this.field1708[i_8];

                  for (i_19 = 0; i_19 < ints_18.length; i_19++) {
                     i_11 = ints_18[i_19];
                     this.field1716[i_11] -= field1705;
                     this.field1690[i_11] -= field1695;
                     this.field1691[i_11] -= field1744;
                     this.field1716[i_11] = i_3 * this.field1716[i_11] / 128;
                     this.field1690[i_11] = i_4 * this.field1690[i_11] / 128;
                     this.field1691[i_11] = i_5 * this.field1691[i_11] / 128;
                     this.field1716[i_11] += field1705;
                     this.field1690[i_11] += field1695;
                     this.field1691[i_11] += field1744;
                  }
               }
            }

         } else if (i_1 == 5) {
            if (this.field1709 != null && this.field1693 != null) {
               for (i_7 = 0; i_7 < i_6; i_7++) {
                  i_8 = ints_2[i_7];
                  if (i_8 < this.field1709.length) {
                     ints_18 = this.field1709[i_8];

                     for (i_19 = 0; i_19 < ints_18.length; i_19++) {
                        i_11 = ints_18[i_19];
                        i_12 = (this.field1693[i_11] & 0xff) + i_3 * 8;
                        if (i_12 < 0) {
                           i_12 = 0;
                        } else if (i_12 > 255) {
                           i_12 = 255;
                        }

                        this.field1693[i_11] = (byte)i_12;
                     }
                  }
               }
            }

         }
      }
   }

   final void method2934(boolean bool_1, boolean bool_2, boolean bool_3, long long_4) {
      if (this.field1714 < 1600) {
         int i_6;
         for (i_6 = 0; i_6 < this.field1714; i_6++) {
            field1732[i_6] = 0;
         }

         i_6 = bool_3 ? 20 : 5;

         int i_7;
         int i_8;
         int i_10;
         int i_11;
         int i_12;
         int i_13;
         int i_15;
         int i_16;
         int i_18;
         int i_27;
         for (i_7 = 0; i_7 < this.field1748; i_7++) {
            if (this.field1698[i_7] != -2) {
               i_8 = this.field1740[i_7];
               i_27 = this.field1694[i_7];
               i_10 = this.field1739[i_7];
               i_11 = field1725[i_8];
               i_12 = field1725[i_27];
               i_13 = field1725[i_10];
               int i_14;
               int i_17;
               if (!bool_1 || i_11 != -5000 && i_12 != -5000 && i_13 != -5000) {
                  if (bool_2) {
                     i_15 = field1726[i_8];
                     i_16 = field1726[i_27];
                     i_17 = field1726[i_10];
                     i_18 = i_6 + class137.field1755;
                     boolean bool_34;
                     if (i_18 < i_15 && i_18 < i_16 && i_18 < i_17) {
                        bool_34 = false;
                     } else {
                        i_18 = class137.field1755 - i_6;
                        if (i_18 > i_15 && i_18 > i_16 && i_18 > i_17) {
                           bool_34 = false;
                        } else {
                           i_18 = i_6 + class137.field1764;
                           if (i_18 < i_11 && i_18 < i_12 && i_18 < i_13) {
                              bool_34 = false;
                           } else {
                              i_18 = class137.field1764 - i_6;
                              if (i_18 > i_11 && i_18 > i_12 && i_18 > i_13) {
                                 bool_34 = false;
                              } else {
                                 bool_34 = true;
                              }
                           }
                        }
                     }

                     if (bool_34) {
                        class137.field1768[++class137.field1763 - 1] = long_4;
                        bool_2 = false;
                     }
                  }

                  if ((i_11 - i_12) * (field1726[i_10] - field1726[i_27]) - (i_13 - i_12) * (field1726[i_8] - field1726[i_27]) > 0) {
                     field1724[i_7] = false;
                     if (i_11 >= 0 && i_12 >= 0 && i_13 >= 0 && i_11 <= class139.field1793 && i_12 <= class139.field1793 && i_13 <= class139.field1793) {
                        field1749[i_7] = false;
                     } else {
                        field1749[i_7] = true;
                     }

                     i_14 = (field1727[i_8] + field1727[i_27] + field1727[i_10]) / 3 + this.field1715;
                     field1733[i_14][field1732[i_14]++] = i_7;
                  }
               } else {
                  i_14 = field1703[i_8];
                  i_15 = field1703[i_27];
                  i_16 = field1703[i_10];
                  i_17 = field1729[i_8];
                  i_18 = field1729[i_27];
                  int i_19 = field1729[i_10];
                  int i_20 = field1730[i_8];
                  int i_21 = field1730[i_27];
                  int i_22 = field1730[i_10];
                  i_14 -= i_15;
                  i_16 -= i_15;
                  i_17 -= i_18;
                  i_19 -= i_18;
                  i_20 -= i_21;
                  i_22 -= i_21;
                  int i_23 = i_17 * i_22 - i_20 * i_19;
                  int i_24 = i_20 * i_16 - i_14 * i_22;
                  int i_25 = i_14 * i_19 - i_17 * i_16;
                  if (i_15 * i_23 + i_18 * i_24 + i_21 * i_25 > 0) {
                     field1724[i_7] = true;
                     int i_26 = (field1727[i_8] + field1727[i_27] + field1727[i_10]) / 3 + this.field1715;
                     field1733[i_26][field1732[i_26]++] = i_7;
                  }
               }
            }
         }

         int[] ints_9;
         if (this.field1699 == null) {
            for (i_7 = this.field1714 - 1; i_7 >= 0; --i_7) {
               i_8 = field1732[i_7];
               if (i_8 > 0) {
                  ints_9 = field1733[i_7];

                  for (i_10 = 0; i_10 < i_8; i_10++) {
                     this.method2936(ints_9[i_10]);
                  }
               }
            }

         } else {
            for (i_7 = 0; i_7 < 12; i_7++) {
               field1734[i_7] = 0;
               field1738[i_7] = 0;
            }

            for (i_7 = this.field1714 - 1; i_7 >= 0; --i_7) {
               i_8 = field1732[i_7];
               if (i_8 > 0) {
                  ints_9 = field1733[i_7];

                  for (i_10 = 0; i_10 < i_8; i_10++) {
                     i_11 = ints_9[i_10];
                     byte b_33 = this.field1699[i_11];
                     i_13 = field1734[b_33]++;
                     field1735[b_33][i_13] = i_11;
                     if (b_33 < 10) {
                        field1738[b_33] += i_7;
                     } else if (b_33 == 10) {
                        field1736[i_13] = i_7;
                     } else {
                        field1737[i_13] = i_7;
                     }
                  }
               }
            }

            i_7 = 0;
            if (field1734[1] > 0 || field1734[2] > 0) {
               i_7 = (field1738[1] + field1738[2]) / (field1734[1] + field1734[2]);
            }

            i_8 = 0;
            if (field1734[3] > 0 || field1734[4] > 0) {
               i_8 = (field1738[3] + field1738[4]) / (field1734[3] + field1734[4]);
            }

            i_27 = 0;
            if (field1734[6] > 0 || field1734[8] > 0) {
               i_27 = (field1738[8] + field1738[6]) / (field1734[8] + field1734[6]);
            }

            i_11 = 0;
            i_12 = field1734[10];
            int[] ints_30 = field1735[10];
            int[] ints_31 = field1736;
            if (i_11 == i_12) {
               i_11 = 0;
               i_12 = field1734[11];
               ints_30 = field1735[11];
               ints_31 = field1737;
            }

            if (i_11 < i_12) {
               i_10 = ints_31[i_11];
            } else {
               i_10 = -1000;
            }

            for (i_15 = 0; i_15 < 10; i_15++) {
               while (i_15 == 0 && i_10 > i_7) {
                  this.method2936(ints_30[i_11++]);
                  if (i_11 == i_12 && ints_30 != field1735[11]) {
                     i_11 = 0;
                     i_12 = field1734[11];
                     ints_30 = field1735[11];
                     ints_31 = field1737;
                  }

                  if (i_11 < i_12) {
                     i_10 = ints_31[i_11];
                  } else {
                     i_10 = -1000;
                  }
               }

               while (i_15 == 3 && i_10 > i_8) {
                  this.method2936(ints_30[i_11++]);
                  if (i_11 == i_12 && ints_30 != field1735[11]) {
                     i_11 = 0;
                     i_12 = field1734[11];
                     ints_30 = field1735[11];
                     ints_31 = field1737;
                  }

                  if (i_11 < i_12) {
                     i_10 = ints_31[i_11];
                  } else {
                     i_10 = -1000;
                  }
               }

               while (i_15 == 5 && i_10 > i_27) {
                  this.method2936(ints_30[i_11++]);
                  if (i_11 == i_12 && ints_30 != field1735[11]) {
                     i_11 = 0;
                     i_12 = field1734[11];
                     ints_30 = field1735[11];
                     ints_31 = field1737;
                  }

                  if (i_11 < i_12) {
                     i_10 = ints_31[i_11];
                  } else {
                     i_10 = -1000;
                  }
               }

               i_16 = field1734[i_15];
               int[] ints_32 = field1735[i_15];

               for (i_18 = 0; i_18 < i_16; i_18++) {
                  this.method2936(ints_32[i_18]);
               }
            }

            while (i_10 != -1000) {
               this.method2936(ints_30[i_11++]);
               if (i_11 == i_12 && ints_30 != field1735[11]) {
                  i_11 = 0;
                  ints_30 = field1735[11];
                  i_12 = field1734[11];
                  ints_31 = field1737;
               }

               if (i_11 < i_12) {
                  i_10 = ints_31[i_11];
               } else {
                  i_10 = -1000;
               }
            }

         }
      }
   }

   public void method2923(int i_1, int i_2, int i_3) {
      for (int i_4 = 0; i_4 < this.field1688; i_4++) {
         this.field1716[i_4] += i_1;
         this.field1690[i_4] += i_2;
         this.field1691[i_4] += i_3;
      }

      this.method2928();
   }

   public final void method2938(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      field1732[0] = -1;
      if (this.field1711 != 2 && this.field1711 != 1) {
         this.method2919();
      }

      int i_8 = class139.field1785;
      int i_9 = class139.field1792;
      int i_10 = field1697[i_1];
      int i_11 = field1743[i_1];
      int i_12 = field1697[i_2];
      int i_13 = field1743[i_2];
      int i_14 = field1697[i_3];
      int i_15 = field1743[i_3];
      int i_16 = field1697[i_4];
      int i_17 = field1743[i_4];
      int i_18 = i_16 * i_6 + i_17 * i_7 >> 16;

      for (int i_19 = 0; i_19 < this.field1688; i_19++) {
         int i_20 = this.field1716[i_19];
         int i_21 = this.field1690[i_19];
         int i_22 = this.field1691[i_19];
         int i_23;
         if (i_3 != 0) {
            i_23 = i_21 * i_14 + i_20 * i_15 >> 16;
            i_21 = i_21 * i_15 - i_20 * i_14 >> 16;
            i_20 = i_23;
         }

         if (i_1 != 0) {
            i_23 = i_21 * i_11 - i_22 * i_10 >> 16;
            i_22 = i_21 * i_10 + i_22 * i_11 >> 16;
            i_21 = i_23;
         }

         if (i_2 != 0) {
            i_23 = i_22 * i_12 + i_20 * i_13 >> 16;
            i_22 = i_22 * i_13 - i_20 * i_12 >> 16;
            i_20 = i_23;
         }

         i_20 += i_5;
         i_21 += i_6;
         i_22 += i_7;
         i_23 = i_21 * i_17 - i_22 * i_16 >> 16;
         i_22 = i_21 * i_16 + i_22 * i_17 >> 16;
         field1727[i_19] = i_22 - i_18;
         field1725[i_19] = i_20 * class139.field1796 / i_22 + i_8;
         field1726[i_19] = i_23 * class139.field1796 / i_22 + i_9;
         if (this.field1704 > 0) {
            field1703[i_19] = i_20;
            field1729[i_19] = i_23;
            field1730[i_19] = i_22;
         }
      }

      try {
         this.method2934(false, false, false, 0L);
      } catch (Exception exception_25) {
         ;
      }

   }

   public final void method2939(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8) {
      field1732[0] = -1;
      if (this.field1711 != 2 && this.field1711 != 1) {
         this.method2919();
      }

      int i_9 = class139.field1785;
      int i_10 = class139.field1792;
      int i_11 = field1697[i_1];
      int i_12 = field1743[i_1];
      int i_13 = field1697[i_2];
      int i_14 = field1743[i_2];
      int i_15 = field1697[i_3];
      int i_16 = field1743[i_3];
      int i_17 = field1697[i_4];
      int i_18 = field1743[i_4];
      int i_19 = i_17 * i_6 + i_18 * i_7 >> 16;

      for (int i_20 = 0; i_20 < this.field1688; i_20++) {
         int i_21 = this.field1716[i_20];
         int i_22 = this.field1690[i_20];
         int i_23 = this.field1691[i_20];
         int i_24;
         if (i_3 != 0) {
            i_24 = i_22 * i_15 + i_21 * i_16 >> 16;
            i_22 = i_22 * i_16 - i_21 * i_15 >> 16;
            i_21 = i_24;
         }

         if (i_1 != 0) {
            i_24 = i_22 * i_12 - i_23 * i_11 >> 16;
            i_23 = i_22 * i_11 + i_23 * i_12 >> 16;
            i_22 = i_24;
         }

         if (i_2 != 0) {
            i_24 = i_23 * i_13 + i_21 * i_14 >> 16;
            i_23 = i_23 * i_14 - i_21 * i_13 >> 16;
            i_21 = i_24;
         }

         i_21 += i_5;
         i_22 += i_6;
         i_23 += i_7;
         i_24 = i_22 * i_18 - i_23 * i_17 >> 16;
         i_23 = i_22 * i_17 + i_23 * i_18 >> 16;
         field1727[i_20] = i_23 - i_19;
         field1725[i_20] = i_9 + i_21 * class139.field1796 / i_8;
         field1726[i_20] = i_10 + i_24 * class139.field1796 / i_8;
         if (this.field1704 > 0) {
            field1703[i_20] = i_21;
            field1729[i_20] = i_24;
            field1730[i_20] = i_23;
         }
      }

      try {
         this.method2934(false, false, false, 0L);
      } catch (Exception exception_26) {
         ;
      }

   }

   public int method2963() {
      this.method2946();
      return this.field1713;
   }

}
