public class class267 extends class184 {

   static class244 field3495;
   static class244 field3522;
   public int field3531;
   static class154 field3496 = new class154(64);
   static class154 field3497 = new class154(50);
   public String field3499 = "null";
   public int field3500 = 1;
   public int field3506 = -1;
   public int field3504 = -1;
   public int field3505 = -1;
   public int field3525 = -1;
   public int field3507 = -1;
   public int field3508 = -1;
   public int field3503 = -1;
   public String[] field3528 = new String[5];
   public boolean field3515 = true;
   public int field3516 = -1;
   int field3517 = 128;
   int field3518 = 128;
   public boolean field3494 = false;
   int field3520 = 0;
   int field3521 = 0;
   public int field3519 = -1;
   public int field3523 = 32;
   int field3498 = -1;
   int field3526 = -1;
   public boolean field3527 = true;
   public boolean field3513 = true;
   public boolean field3529 = false;
   int[] field3501;
   short[] field3509;
   short[] field3511;
   short[] field3512;
   short[] field3514;
   int[] field3502;
   public int[] field3524;
   class326 field3530;

   public final class267 method4731(byte b_1) {
      int i_2 = -1;
      if (this.field3498 != -1) {
         i_2 = class222.method3999(this.field3498, 7567292);
      } else if (this.field3526 != -1) {
         i_2 = class221.field2541[this.field3526];
      }

      int i_3;
      if (i_2 >= 0 && i_2 < this.field3524.length - 1) {
         i_3 = this.field3524[i_2];
      } else {
         i_3 = this.field3524[this.field3524.length - 1];
      }

      return i_3 != -1 ? class27.method272(i_3, -671171592) : null;
   }

   void method4733(class310 class310_1, int i_2, byte b_3) {
      int i_4;
      int i_5;
      if (i_2 == 1) {
         i_4 = class310_1.method5661((byte) -13);
         this.field3501 = new int[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3501[i_5] = class310_1.method5729(-1227533071);
         }
      } else if (i_2 == 2) {
         this.field3499 = class310_1.method5589(250694355);
      } else if (i_2 == 12) {
         this.field3500 = class310_1.method5661((byte) -34);
      } else if (i_2 == 13) {
         this.field3506 = class310_1.method5729(206409007);
      } else if (i_2 == 14) {
         this.field3525 = class310_1.method5729(-440917770);
      } else if (i_2 == 15) {
         this.field3504 = class310_1.method5729(783609081);
      } else if (i_2 == 16) {
         this.field3505 = class310_1.method5729(425740769);
      } else if (i_2 == 17) {
         this.field3525 = class310_1.method5729(-486357402);
         this.field3507 = class310_1.method5729(1037697574);
         this.field3508 = class310_1.method5729(-84520019);
         this.field3503 = class310_1.method5729(-703564438);
      } else if (i_2 >= 30 && i_2 < 35) {
         this.field3528[i_2 - 30] = class310_1.method5589(502285426);
         if (this.field3528[i_2 - 30].equalsIgnoreCase("Hidden")) {
            this.field3528[i_2 - 30] = null;
         }
      } else if (i_2 == 40) {
         i_4 = class310_1.method5661((byte) 60);
         this.field3509 = new short[i_4];
         this.field3511 = new short[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3509[i_5] = (short)class310_1.method5729(-975245788);
            this.field3511[i_5] = (short)class310_1.method5729(-1012757868);
         }
      } else if (i_2 == 41) {
         i_4 = class310_1.method5661((byte) 65);
         this.field3512 = new short[i_4];
         this.field3514 = new short[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3512[i_5] = (short)class310_1.method5729(1551124167);
            this.field3514[i_5] = (short)class310_1.method5729(1254602459);
         }
      } else if (i_2 == 60) {
         i_4 = class310_1.method5661((byte) -65);
         this.field3502 = new int[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3502[i_5] = class310_1.method5729(-1880123445);
         }
      } else if (i_2 == 93) {
         this.field3515 = false;
      } else if (i_2 == 95) {
         this.field3516 = class310_1.method5729(-174788138);
      } else if (i_2 == 97) {
         this.field3517 = class310_1.method5729(-1730388177);
      } else if (i_2 == 98) {
         this.field3518 = class310_1.method5729(-1528636785);
      } else if (i_2 == 99) {
         this.field3494 = true;
      } else if (i_2 == 100) {
         this.field3520 = class310_1.method5535((byte) 0);
      } else if (i_2 == 101) {
         this.field3521 = class310_1.method5535((byte) 0) * 5;
      } else if (i_2 == 102) {
         this.field3519 = class310_1.method5729(686830155);
      } else if (i_2 == 103) {
         this.field3523 = class310_1.method5729(-434692720);
      } else if (i_2 != 106 && i_2 != 118) {
         if (i_2 == 107) {
            this.field3527 = false;
         } else if (i_2 == 109) {
            this.field3513 = false;
         } else if (i_2 == 111) {
            this.field3529 = true;
         } else if (i_2 == 249) {
            this.field3530 = class180.method3576(class310_1, this.field3530, -1178223597);
         }
      } else {
         this.field3498 = class310_1.method5729(-22922657);
         if (this.field3498 == 65535) {
            this.field3498 = -1;
         }

         this.field3526 = class310_1.method5729(1562651635);
         if (this.field3526 == 65535) {
            this.field3526 = -1;
         }

         i_4 = -1;
         if (i_2 == 118) {
            i_4 = class310_1.method5729(1110698533);
            if (i_4 == 65535) {
               i_4 = -1;
            }
         }

         i_5 = class310_1.method5661((byte) -1);
         this.field3524 = new int[i_5 + 2];

         for (int i_6 = 0; i_6 <= i_5; i_6++) {
            this.field3524[i_6] = class310_1.method5729(-1707912331);
            if (this.field3524[i_6] == 65535) {
               this.field3524[i_6] = -1;
            }
         }

         this.field3524[i_5 + 1] = i_4;
      }

   }

   public final class130 method4703(int i_1) {
      if (this.field3524 != null) {
         class267 class267_2 = this.method4731((byte) -19);
         return class267_2 == null ? null : class267_2.method4703(-1687954211);
      } else if (this.field3502 == null) {
         return null;
      } else {
         boolean bool_6 = false;

         for (int i_3 = 0; i_3 < this.field3502.length; i_3++) {
            if (!field3495.method4162(this.field3502[i_3], 0, 1697855638)) {
               bool_6 = true;
            }
         }

         if (bool_6) {
            return null;
         } else {
            class130[] arr_7 = new class130[this.field3502.length];

            for (int i_4 = 0; i_4 < this.field3502.length; i_4++) {
               arr_7[i_4] = class130.method2824(field3495, this.field3502[i_4], 0);
            }

            class130 class130_8;
            if (arr_7.length == 1) {
               class130_8 = arr_7[0];
            } else {
               class130_8 = new class130(arr_7, arr_7.length);
            }

            int i_5;
            if (this.field3509 != null) {
               for (i_5 = 0; i_5 < this.field3509.length; i_5++) {
                  class130_8.method2837(this.field3509[i_5], this.field3511[i_5]);
               }
            }

            if (this.field3512 != null) {
               for (i_5 = 0; i_5 < this.field3512.length; i_5++) {
                  class130_8.method2833(this.field3512[i_5], this.field3514[i_5]);
               }
            }

            return class130_8;
         }
      }
   }

   public final class136 method4704(class269 class269_1, int i_2, class269 class269_3, int i_4, byte b_5) {
      if (this.field3524 != null) {
         class267 class267_13 = this.method4731((byte) -79);
         return class267_13 == null ? null : class267_13.method4704(class269_1, i_2, class269_3, i_4, (byte) 11);
      } else {
         class136 class136_6 = (class136) field3497.method3376((long)this.field3531);
         if (class136_6 == null) {
            boolean bool_7 = false;

            for (int i_8 = 0; i_8 < this.field3501.length; i_8++) {
               if (!field3495.method4162(this.field3501[i_8], 0, 606168151)) {
                  bool_7 = true;
               }
            }

            if (bool_7) {
               return null;
            }

            class130[] arr_9 = new class130[this.field3501.length];

            int i_10;
            for (i_10 = 0; i_10 < this.field3501.length; i_10++) {
               arr_9[i_10] = class130.method2824(field3495, this.field3501[i_10], 0);
            }

            class130 class130_12;
            if (arr_9.length == 1) {
               class130_12 = arr_9[0];
            } else {
               class130_12 = new class130(arr_9, arr_9.length);
            }

            if (this.field3509 != null) {
               for (i_10 = 0; i_10 < this.field3509.length; i_10++) {
                  class130_12.method2837(this.field3509[i_10], this.field3511[i_10]);
               }
            }

            if (this.field3512 != null) {
               for (i_10 = 0; i_10 < this.field3512.length; i_10++) {
                  class130_12.method2833(this.field3512[i_10], this.field3514[i_10]);
               }
            }

            class136_6 = class130_12.method2876(this.field3520 + 64, this.field3521 + 850, -30, -50, -30);
            field3497.method3374(class136_6, (long)this.field3531);
         }

         class136 class136_11;
         if (class269_1 != null && class269_3 != null) {
            class136_11 = class269_1.method4781(class136_6, i_2, class269_3, i_4, (byte) -66);
         } else if (class269_1 != null) {
            class136_11 = class269_1.method4761(class136_6, i_2, (byte) 13);
         } else if (class269_3 != null) {
            class136_11 = class269_3.method4761(class136_6, i_4, (byte) 7);
         } else {
            class136_11 = class136_6.method2921(true);
         }

         if (this.field3517 != 128 || this.field3518 != 128) {
            class136_11.method2937(this.field3517, this.field3518, this.field3517);
         }

         return class136_11;
      }
   }

   void method4699(int i_1) {
   }

   void method4700(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -2);
         if (i_3 == 0) {
            return;
         }

         this.method4733(class310_1, i_3, (byte) -100);
      }
   }

   public boolean method4713(int i_1) {
      if (this.field3524 == null) {
         return true;
      } else {
         int i_2 = -1;
         if (this.field3498 != -1) {
            i_2 = class222.method3999(this.field3498, 39289056);
         } else if (this.field3526 != -1) {
            i_2 = class221.field2541[this.field3526];
         }

         return i_2 >= 0 && i_2 < this.field3524.length ? this.field3524[i_2] != -1 : this.field3524[this.field3524.length - 1] != -1;
      }
   }

   public int method4706(int i_1, int i_2, int i_3) {
      class326 class326_5 = this.field3530;
      int i_4;
      if (class326_5 == null) {
         i_4 = i_2;
      } else {
         class188 class188_6 = (class188) class326_5.method5919((long)i_1);
         if (class188_6 == null) {
            i_4 = i_2;
         } else {
            i_4 = class188_6.field2135;
         }
      }

      return i_4;
   }

   public String method4707(int i_1, String string_2, int i_3) {
      class326 class326_5 = this.field3530;
      String string_4;
      if (class326_5 == null) {
         string_4 = string_2;
      } else {
         class185 class185_6 = (class185) class326_5.method5919((long)i_1);
         if (class185_6 == null) {
            string_4 = string_2;
         } else {
            string_4 = (String) class185_6.field2131;
         }
      }

      return string_4;
   }

   static final void method4726(int i_0, int i_1) {
      if (class41.method603(i_0, -1581404925)) {
         class141.method3128(class9.field44[i_0], -1, -1212206355);
      }
   }

}
