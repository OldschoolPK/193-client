import java.util.Comparator;

final class class19 implements Comparator {

   static class348 field92;
   static class246 field90;
   static class246 field89;
   static class335[] field87;

   int method192(class16 class16_1, class16 class16_2, int i_3) {
      return class16_1.field72.field79 < class16_2.field72.field79 ? -1 : (class16_2.field72.field79 == class16_1.field72.field79 ? 0 : 1);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method192((class16) object_1, (class16) object_2, 1020581698);
   }

   public boolean equals(Object object_1) {
      return super.equals(object_1);
   }

   static int method200(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      class226 class226_4 = bool_2 ? class223.field2561 : class253.field3264;
      if (i_0 == 1600) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2607;
         return 1;
      } else if (i_0 == 1601) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2608;
         return 1;
      } else if (i_0 == 1602) {
         class85.field1096[++class85.field1105 - 1] = class226_4.field2647;
         return 1;
      } else if (i_0 == 1603) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2609;
         return 1;
      } else if (i_0 == 1604) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2639;
         return 1;
      } else if (i_0 == 1605) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2640;
         return 1;
      } else if (i_0 == 1606) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2583;
         return 1;
      } else if (i_0 == 1607) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2657;
         return 1;
      } else if (i_0 == 1608) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2638;
         return 1;
      } else if (i_0 == 1609) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2652;
         return 1;
      } else if (i_0 == 1610) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2636;
         return 1;
      } else if (i_0 == 1611) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2611;
         return 1;
      } else if (i_0 == 1612) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2612;
         return 1;
      } else if (i_0 == 1613) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2616.vmethod6086(-213626918);
         return 1;
      } else if (i_0 == 1614) {
         class85.field1095[++class253.field3267 - 1] = class226_4.field2673 ? 1 : 0;
         return 1;
      } else {
         return 2;
      }
   }

   static final void method191(class226 class226_0, int i_1, int i_2) {
      if (class226_0.field2649 == null) {
         throw new RuntimeException();
      } else {
         if (class226_0.field2684 == null) {
            class226_0.field2684 = new int[class226_0.field2649.length];
         }

         class226_0.field2684[i_1] = Integer.MAX_VALUE;
      }
   }

}
