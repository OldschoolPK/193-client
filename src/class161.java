public class class161 extends class285 {

   static boolean field1989;
   final boolean field1991;

   public class161(boolean bool_1) {
      this.field1991 = bool_1;
   }

   int method3446(class289 class289_1, class289 class289_2, byte b_3) {
      return client.field645 == class289_1.field3650 && class289_2.field3650 == client.field645 ? (this.field1991 ? class289_1.method5038(1670678120).method5193(class289_2.method5038(1670678120), -1152802722) : class289_2.method5038(1670678120).method5193(class289_1.method5038(1670678120), -1152802722)) : this.method5051(class289_1, class289_2, -204379903);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3446((class289) object_1, (class289) object_2, (byte) -18);
   }

   static final void method3449(int i_0) {
      for (int i_1 = 0; i_1 < client.field690; i_1++) {
         int i_2 = client.field691[i_1];
         class88 class88_3 = client.field689[i_2];
         if (class88_3 != null) {
            class40.method598(class88_3, class88_3.field1135.field3500, 2061184541);
         }
      }

   }

   static int method3450(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      class226 class226_4;
      if (i_0 >= 2000) {
         i_0 -= 1000;
         class226_4 = class181.method3610(class85.field1095[--class253.field3267], 665210667);
      } else {
         class226_4 = bool_2 ? class223.field2561 : class253.field3264;
      }

      class181.method3609(class226_4, 1804905249);
      if (i_0 != 1200 && i_0 != 1205 && i_0 != 1212) {
         if (i_0 == 1201) {
            class226_4.field2629 = 2;
            class226_4.field2687 = class85.field1095[--class253.field3267];
            return 1;
         } else if (i_0 == 1202) {
            class226_4.field2629 = 3;
            class226_4.field2687 = class223.field2562.field621.method4016(-2118996761);
            return 1;
         } else {
            return 2;
         }
      } else {
         class253.field3267 -= 2;
         int i_5 = class85.field1095[class253.field3267];
         int i_6 = class85.field1095[class253.field3267 + 1];
         class226_4.field2712 = i_5;
         class226_4.field2713 = i_6;
         class265 class265_7 = class66.method1223(i_5, 532366314);
         class226_4.field2583 = class265_7.field3457;
         class226_4.field2638 = class265_7.field3451;
         class226_4.field2657 = class265_7.field3452;
         class226_4.field2635 = class265_7.field3453;
         class226_4.field2680 = class265_7.field3472;
         class226_4.field2640 = class265_7.field3449;
         if (i_0 == 1205) {
            class226_4.field2644 = 0;
         } else if (i_0 == 1212 | class265_7.field3455 == 1) {
            class226_4.field2644 = 1;
         } else {
            class226_4.field2644 = 2;
         }

         if (class226_4.field2641 > 0) {
            class226_4.field2640 = class226_4.field2640 * 32 / class226_4.field2641;
         } else if (class226_4.field2597 > 0) {
            class226_4.field2640 = class226_4.field2640 * 32 / class226_4.field2597;
         }

         return 1;
      }
   }

}
