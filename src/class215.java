import java.nio.ByteBuffer;

public class class215 extends class214 {

   public static int field2511;
   static class334 field2513;
   ByteBuffer field2512;

   public void vmethod3947(byte[] bytes_1, int i_2) {
      this.field2512 = ByteBuffer.allocateDirect(bytes_1.length);
      this.field2512.position(0);
      this.field2512.put(bytes_1);
   }

   byte[] vmethod3945(int i_1) {
      byte[] bytes_2 = new byte[this.field2512.capacity()];
      this.field2512.position(0);
      this.field2512.get(bytes_2);
      return bytes_2;
   }

   static final void method3951(class226 class226_0, int i_1, int i_2, int i_3, int i_4) {
      class302.method5318((byte) 8);
      class220 class220_5 = class226_0.method4072(false, (byte) -56);
      if (class220_5 != null) {
         class331.method5996(i_1, i_2, class220_5.field2537 + i_1, i_2 + class220_5.field2532);
         if (client.field894 != 2 && client.field894 != 5) {
            int i_6 = client.field830 & 0x7ff;
            int i_7 = class223.field2562.field990 / 32 + 48;
            int i_8 = 464 - class223.field2562.field938 / 32;
            class25.field126.method6143(i_1, i_2, class220_5.field2537, class220_5.field2532, i_7, i_8, i_6, 256, class220_5.field2531, class220_5.field2533);

            int i_9;
            int i_10;
            int i_11;
            for (i_9 = 0; i_9 < client.field756; i_9++) {
               i_10 = client.field717[i_9] * 4 + 2 - class223.field2562.field990 / 32;
               i_11 = client.field877[i_9] * 4 + 2 - class223.field2562.field938 / 32;
               class25.method250(i_1, i_2, i_10, i_11, client.field878[i_9], class220_5, -1445160201);
            }

            int i_12;
            int i_13;
            for (i_9 = 0; i_9 < 104; i_9++) {
               for (i_10 = 0; i_10 < 104; i_10++) {
                  class272 class272_16 = client.field776[class151.field1949][i_9][i_10];
                  if (class272_16 != null) {
                     i_12 = i_9 * 4 + 2 - class223.field2562.field990 / 32;
                     i_13 = i_10 * 4 + 2 - class223.field2562.field938 / 32;
                     class25.method250(i_1, i_2, i_12, i_13, class78.field998[0], class220_5, -1541467427);
                  }
               }
            }

            for (i_9 = 0; i_9 < client.field690; i_9++) {
               class88 class88_17 = client.field689[client.field691[i_9]];
               if (class88_17 != null && class88_17.vmethod2080(1661589479)) {
                  class267 class267_19 = class88_17.field1135;
                  if (class267_19 != null && class267_19.field3524 != null) {
                     class267_19 = class267_19.method4731((byte) -118);
                  }

                  if (class267_19 != null && class267_19.field3515 && class267_19.field3527) {
                     i_12 = class88_17.field990 / 32 - class223.field2562.field990 / 32;
                     i_13 = class88_17.field938 / 32 - class223.field2562.field938 / 32;
                     class25.method250(i_1, i_2, i_12, i_13, class78.field998[1], class220_5, -1949880024);
                  }
               }
            }

            i_9 = class98.field1263;
            int[] ints_20 = class98.field1268;

            for (i_11 = 0; i_11 < i_9; i_11++) {
               class75 class75_18 = client.field764[ints_20[i_11]];
               if (class75_18 != null && class75_18.vmethod2080(-777948998) && !class75_18.field625 && class75_18 != class223.field2562) {
                  i_13 = class75_18.field990 / 32 - class223.field2562.field990 / 32;
                  int i_14 = class75_18.field938 / 32 - class223.field2562.field938 / 32;
                  boolean bool_15 = false;
                  if (class223.field2562.field616 != 0 && class75_18.field616 != 0 && class75_18.field616 == class223.field2562.field616) {
                     bool_15 = true;
                  }

                  if (class75_18.method1331(338154305)) {
                     class25.method250(i_1, i_2, i_13, i_14, class78.field998[3], class220_5, -1524406499);
                  } else if (bool_15) {
                     class25.method250(i_1, i_2, i_13, i_14, class78.field998[4], class220_5, -1773818561);
                  } else if (class75_18.method1330((byte) -26)) {
                     class25.method250(i_1, i_2, i_13, i_14, class78.field998[5], class220_5, -2074229035);
                  } else {
                     class25.method250(i_1, i_2, i_13, i_14, class78.field998[2], class220_5, -1445959649);
                  }
               }
            }

            if (client.field740 != 0 && client.field655 % 20 < 10) {
               if (client.field740 == 1 && client.field664 >= 0 && client.field664 < client.field689.length) {
                  class88 class88_21 = client.field689[client.field664];
                  if (class88_21 != null) {
                     i_12 = class88_21.field990 / 32 - class223.field2562.field990 / 32;
                     i_13 = class88_21.field938 / 32 - class223.field2562.field938 / 32;
                     class30.method320(i_1, i_2, i_12, i_13, class19.field87[1], class220_5, -942051498);
                  }
               }

               if (client.field740 == 2) {
                  i_11 = client.field709 * 4 - class243.field3156 * 4 + 2 - class223.field2562.field990 / 32;
                  i_12 = client.field667 * 4 - class41.field300 * 4 + 2 - class223.field2562.field938 / 32;
                  class30.method320(i_1, i_2, i_11, i_12, class19.field87[1], class220_5, -1060387318);
               }

               if (client.field740 == 10 && client.field665 >= 0 && client.field665 < client.field764.length) {
                  class75 class75_22 = client.field764[client.field665];
                  if (class75_22 != null) {
                     i_12 = class75_22.field990 / 32 - class223.field2562.field990 / 32;
                     i_13 = class75_22.field938 / 32 - class223.field2562.field938 / 32;
                     class30.method320(i_1, i_2, i_12, i_13, class19.field87[1], class220_5, -1285396256);
                  }
               }
            }

            if (client.field774 != 0) {
               i_11 = client.field774 * 4 + 2 - class223.field2562.field990 / 32;
               i_12 = client.field880 * 4 + 2 - class223.field2562.field938 / 32;
               class25.method250(i_1, i_2, i_11, i_12, class19.field87[0], class220_5, -1529996370);
            }

            if (!class223.field2562.field625) {
               class331.method6004(class220_5.field2537 / 2 + i_1 - 1, class220_5.field2532 / 2 + i_2 - 1, 3, 3, 16777215);
            }
         } else {
            class331.method6018(i_1, i_2, 0, class220_5.field2531, class220_5.field2533);
         }

         client.field854[i_3] = true;
      }
   }

   static void method3952(byte b_0) {
      class64.field506 = 99;
      class64.field514 = new byte[4][104][104];
      class64.field508 = new byte[4][104][104];
      class9.field40 = new byte[4][104][104];
      class293.field3663 = new byte[4][104][104];
      class14.field52 = new int[4][105][105];
      class64.field511 = new byte[4][105][105];
      class92.field1173 = new int[105][105];
      class259.field3325 = new int[104];
      class245.field3181 = new int[104];
      class92.field1172 = new int[104];
      class164.field1998 = new int[104];
      class99.field1283 = new int[104];
   }

}
