public class class210 extends class189 {

   static int field2497;
   class112[] field2493 = new class112[128];
   short[] field2494 = new short[128];
   byte[] field2495 = new byte[128];
   byte[] field2496 = new byte[128];
   class205[] field2492 = new class205[128];
   byte[] field2498 = new byte[128];
   int[] field2500 = new int[128];
   int field2499;

   class210(byte[] bytes_1) {
      class310 class310_2 = new class310(bytes_1);

      int i_3;
      for (i_3 = 0; class310_2.field3752[i_3 + class310_2.field3751] != 0; i_3++) {
         ;
      }

      byte[] bytes_4 = new byte[i_3];

      int i_5;
      for (i_5 = 0; i_5 < i_3; i_5++) {
         bytes_4[i_5] = class310_2.method5535((byte) 0);
      }

      ++class310_2.field3751;
      ++i_3;
      i_5 = class310_2.field3751;
      class310_2.field3751 += i_3;

      int i_6;
      for (i_6 = 0; class310_2.field3752[i_6 + class310_2.field3751] != 0; i_6++) {
         ;
      }

      byte[] bytes_7 = new byte[i_6];

      int i_8;
      for (i_8 = 0; i_8 < i_6; i_8++) {
         bytes_7[i_8] = class310_2.method5535((byte) 0);
      }

      ++class310_2.field3751;
      ++i_6;
      i_8 = class310_2.field3751;
      class310_2.field3751 += i_6;

      int i_9;
      for (i_9 = 0; class310_2.field3752[i_9 + class310_2.field3751] != 0; i_9++) {
         ;
      }

      byte[] bytes_10 = new byte[i_9];

      for (int i_11 = 0; i_11 < i_9; i_11++) {
         bytes_10[i_11] = class310_2.method5535((byte) 0);
      }

      ++class310_2.field3751;
      ++i_9;
      byte[] bytes_38 = new byte[i_9];
      int i_12;
      int i_14;
      if (i_9 > 1) {
         bytes_38[1] = 1;
         int i_13 = 1;
         i_12 = 2;

         for (i_14 = 2; i_14 < i_9; i_14++) {
            int i_15 = class310_2.method5661((byte) 32);
            if (i_15 == 0) {
               i_13 = i_12++;
            } else {
               if (i_15 <= i_13) {
                  --i_15;
               }

               i_13 = i_15;
            }

            bytes_38[i_14] = (byte)i_13;
         }
      } else {
         i_12 = i_9;
      }

      class205[] arr_39 = new class205[i_12];

      class205 class205_40;
      for (i_14 = 0; i_14 < arr_39.length; i_14++) {
         class205_40 = arr_39[i_14] = new class205();
         int i_16 = class310_2.method5661((byte) -85);
         if (i_16 > 0) {
            class205_40.field2411 = new byte[i_16 * 2];
         }

         i_16 = class310_2.method5661((byte) 16);
         if (i_16 > 0) {
            class205_40.field2412 = new byte[i_16 * 2 + 2];
            class205_40.field2412[1] = 64;
         }
      }

      i_14 = class310_2.method5661((byte) 53);
      byte[] bytes_47 = i_14 > 0 ? new byte[i_14 * 2] : null;
      i_14 = class310_2.method5661((byte) 20);
      byte[] bytes_41 = i_14 > 0 ? new byte[i_14 * 2] : null;

      int i_17;
      for (i_17 = 0; class310_2.field3752[i_17 + class310_2.field3751] != 0; i_17++) {
         ;
      }

      byte[] bytes_18 = new byte[i_17];

      int i_19;
      for (i_19 = 0; i_19 < i_17; i_19++) {
         bytes_18[i_19] = class310_2.method5535((byte) 0);
      }

      ++class310_2.field3751;
      ++i_17;
      i_19 = 0;

      int i_20;
      for (i_20 = 0; i_20 < 128; i_20++) {
         i_19 += class310_2.method5661((byte) -41);
         this.field2494[i_20] = (short)i_19;
      }

      i_19 = 0;

      for (i_20 = 0; i_20 < 128; i_20++) {
         i_19 += class310_2.method5661((byte) 79);
         this.field2494[i_20] = (short)(this.field2494[i_20] + (i_19 << 8));
      }

      i_20 = 0;
      int i_21 = 0;
      int i_22 = 0;

      int i_23;
      for (i_23 = 0; i_23 < 128; i_23++) {
         if (i_20 == 0) {
            if (i_21 < bytes_18.length) {
               i_20 = bytes_18[i_21++];
            } else {
               i_20 = -1;
            }

            i_22 = class310_2.method5520((byte) -111);
         }

         this.field2494[i_23] = (short)(this.field2494[i_23] + ((i_22 - 1 & 0x2) << 14));
         this.field2500[i_23] = i_22;
         --i_20;
      }

      i_20 = 0;
      i_21 = 0;
      i_23 = 0;

      int i_24;
      for (i_24 = 0; i_24 < 128; i_24++) {
         if (this.field2500[i_24] != 0) {
            if (i_20 == 0) {
               if (i_21 < bytes_4.length) {
                  i_20 = bytes_4[i_21++];
               } else {
                  i_20 = -1;
               }

               i_23 = class310_2.field3752[i_5++] - 1;
            }

            this.field2498[i_24] = (byte)i_23;
            --i_20;
         }
      }

      i_20 = 0;
      i_21 = 0;
      i_24 = 0;

      for (int i_25 = 0; i_25 < 128; i_25++) {
         if (this.field2500[i_25] != 0) {
            if (i_20 == 0) {
               if (i_21 < bytes_7.length) {
                  i_20 = bytes_7[i_21++];
               } else {
                  i_20 = -1;
               }

               i_24 = class310_2.field3752[i_8++] + 16 << 2;
            }

            this.field2496[i_25] = (byte)i_24;
            --i_20;
         }
      }

      i_20 = 0;
      i_21 = 0;
      class205 class205_42 = null;

      int i_26;
      for (i_26 = 0; i_26 < 128; i_26++) {
         if (this.field2500[i_26] != 0) {
            if (i_20 == 0) {
               class205_42 = arr_39[bytes_38[i_21]];
               if (i_21 < bytes_10.length) {
                  i_20 = bytes_10[i_21++];
               } else {
                  i_20 = -1;
               }
            }

            this.field2492[i_26] = class205_42;
            --i_20;
         }
      }

      i_20 = 0;
      i_21 = 0;
      i_26 = 0;

      int i_27;
      for (i_27 = 0; i_27 < 128; i_27++) {
         if (i_20 == 0) {
            if (i_21 < bytes_18.length) {
               i_20 = bytes_18[i_21++];
            } else {
               i_20 = -1;
            }

            if (this.field2500[i_27] > 0) {
               i_26 = class310_2.method5661((byte) 74) + 1;
            }
         }

         this.field2495[i_27] = (byte)i_26;
         --i_20;
      }

      this.field2499 = class310_2.method5661((byte) -22) + 1;

      class205 class205_28;
      int i_29;
      for (i_27 = 0; i_27 < i_12; i_27++) {
         class205_28 = arr_39[i_27];
         if (class205_28.field2411 != null) {
            for (i_29 = 1; i_29 < class205_28.field2411.length; i_29 += 2) {
               class205_28.field2411[i_29] = class310_2.method5535((byte) 0);
            }
         }

         if (class205_28.field2412 != null) {
            for (i_29 = 3; i_29 < class205_28.field2412.length - 2; i_29 += 2) {
               class205_28.field2412[i_29] = class310_2.method5535((byte) 0);
            }
         }
      }

      if (bytes_47 != null) {
         for (i_27 = 1; i_27 < bytes_47.length; i_27 += 2) {
            bytes_47[i_27] = class310_2.method5535((byte) 0);
         }
      }

      if (bytes_41 != null) {
         for (i_27 = 1; i_27 < bytes_41.length; i_27 += 2) {
            bytes_41[i_27] = class310_2.method5535((byte) 0);
         }
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         class205_28 = arr_39[i_27];
         if (class205_28.field2412 != null) {
            i_19 = 0;

            for (i_29 = 2; i_29 < class205_28.field2412.length; i_29 += 2) {
               i_19 = 1 + i_19 + class310_2.method5661((byte) 30);
               class205_28.field2412[i_29] = (byte)i_19;
            }
         }
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         class205_28 = arr_39[i_27];
         if (class205_28.field2411 != null) {
            i_19 = 0;

            for (i_29 = 2; i_29 < class205_28.field2411.length; i_29 += 2) {
               i_19 = i_19 + 1 + class310_2.method5661((byte) 80);
               class205_28.field2411[i_29] = (byte)i_19;
            }
         }
      }

      byte b_30;
      int i_32;
      int i_33;
      int i_34;
      int i_35;
      int i_36;
      int i_44;
      byte b_46;
      if (bytes_47 != null) {
         i_19 = class310_2.method5661((byte) 29);
         bytes_47[0] = (byte)i_19;

         for (i_27 = 2; i_27 < bytes_47.length; i_27 += 2) {
            i_19 = i_19 + 1 + class310_2.method5661((byte) -74);
            bytes_47[i_27] = (byte)i_19;
         }

         b_46 = bytes_47[0];
         byte b_43 = bytes_47[1];

         for (i_29 = 0; i_29 < b_46; i_29++) {
            this.field2495[i_29] = (byte)(b_43 * this.field2495[i_29] + 32 >> 6);
         }

         for (i_29 = 2; i_29 < bytes_47.length; i_29 += 2) {
            b_30 = bytes_47[i_29];
            byte b_31 = bytes_47[i_29 + 1];
            i_32 = b_43 * (b_30 - b_46) + (b_30 - b_46) / 2;

            for (i_33 = b_46; i_33 < b_30; i_33++) {
               i_35 = b_30 - b_46;
               i_36 = i_32 >>> 31;
               i_34 = (i_32 + i_36) / i_35 - i_36;
               this.field2495[i_33] = (byte)(i_34 * this.field2495[i_33] + 32 >> 6);
               i_32 += b_31 - b_43;
            }

            b_46 = b_30;
            b_43 = b_31;
         }

         for (i_44 = b_46; i_44 < 128; i_44++) {
            this.field2495[i_44] = (byte)(b_43 * this.field2495[i_44] + 32 >> 6);
         }

         class205_40 = null;
      }

      if (bytes_41 != null) {
         i_19 = class310_2.method5661((byte) -93);
         bytes_41[0] = (byte)i_19;

         for (i_27 = 2; i_27 < bytes_41.length; i_27 += 2) {
            i_19 = 1 + i_19 + class310_2.method5661((byte) 84);
            bytes_41[i_27] = (byte)i_19;
         }

         b_46 = bytes_41[0];
         int i_49 = bytes_41[1] << 1;

         for (i_29 = 0; i_29 < b_46; i_29++) {
            i_44 = i_49 + (this.field2496[i_29] & 0xff);
            if (i_44 < 0) {
               i_44 = 0;
            }

            if (i_44 > 128) {
               i_44 = 128;
            }

            this.field2496[i_29] = (byte)i_44;
         }

         int i_45;
         for (i_29 = 2; i_29 < bytes_41.length; i_29 += 2) {
            b_30 = bytes_41[i_29];
            i_45 = bytes_41[i_29 + 1] << 1;
            i_32 = i_49 * (b_30 - b_46) + (b_30 - b_46) / 2;

            for (i_33 = b_46; i_33 < b_30; i_33++) {
               i_35 = b_30 - b_46;
               i_36 = i_32 >>> 31;
               i_34 = (i_32 + i_36) / i_35 - i_36;
               int i_37 = i_34 + (this.field2496[i_33] & 0xff);
               if (i_37 < 0) {
                  i_37 = 0;
               }

               if (i_37 > 128) {
                  i_37 = 128;
               }

               this.field2496[i_33] = (byte)i_37;
               i_32 += i_45 - i_49;
            }

            b_46 = b_30;
            i_49 = i_45;
         }

         for (i_44 = b_46; i_44 < 128; i_44++) {
            i_45 = i_49 + (this.field2496[i_44] & 0xff);
            if (i_45 < 0) {
               i_45 = 0;
            }

            if (i_45 > 128) {
               i_45 = 128;
            }

            this.field2496[i_44] = (byte)i_45;
         }

         Object obj_48 = null;
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         arr_39[i_27].field2410 = class310_2.method5661((byte) 4);
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         class205_28 = arr_39[i_27];
         if (class205_28.field2411 != null) {
            class205_28.field2417 = class310_2.method5661((byte) 40);
         }

         if (class205_28.field2412 != null) {
            class205_28.field2413 = class310_2.method5661((byte) 58);
         }

         if (class205_28.field2410 > 0) {
            class205_28.field2414 = class310_2.method5661((byte) -16);
         }
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         arr_39[i_27].field2416 = class310_2.method5661((byte) -3);
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         class205_28 = arr_39[i_27];
         if (class205_28.field2416 > 0) {
            class205_28.field2415 = class310_2.method5661((byte) -5);
         }
      }

      for (i_27 = 0; i_27 < i_12; i_27++) {
         class205_28 = arr_39[i_27];
         if (class205_28.field2415 > 0) {
            class205_28.field2409 = class310_2.method5661((byte) -29);
         }
      }

   }

   void method3890(byte b_1) {
      this.field2500 = null;
   }

   boolean method3889(class117 class117_1, byte[] bytes_2, int[] ints_3, int i_4) {
      boolean bool_5 = true;
      int i_6 = 0;
      class112 class112_7 = null;

      for (int i_8 = 0; i_8 < 128; i_8++) {
         if (bytes_2 == null || bytes_2[i_8] != 0) {
            int i_9 = this.field2500[i_8];
            if (i_9 != 0) {
               if (i_6 != i_9) {
                  i_6 = i_9--;
                  if ((i_9 & 0x1) == 0) {
                     class112_7 = class117_1.method2542(i_9 >> 2, ints_3, 122861408);
                  } else {
                     class112_7 = class117_1.method2545(i_9 >> 2, ints_3, -2010455709);
                  }

                  if (class112_7 == null) {
                     bool_5 = false;
                  }
               }

               if (class112_7 != null) {
                  this.field2493[i_8] = class112_7;
                  this.field2500[i_8] = 0;
               }
            }
         }
      }

      return bool_5;
   }

}
