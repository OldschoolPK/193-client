public class class141 extends class189 {

   static int field1823;
   int field1817;
   int field1820;
   int[] field1818;
   int[][] field1819;

   class141(int i_1, byte[] bytes_2) {
      this.field1817 = i_1;
      class310 class310_3 = new class310(bytes_2);
      this.field1820 = class310_3.method5661((byte) -46);
      this.field1818 = new int[this.field1820];
      this.field1819 = new int[this.field1820][];

      int i_4;
      for (i_4 = 0; i_4 < this.field1820; i_4++) {
         this.field1818[i_4] = class310_3.method5661((byte) 70);
      }

      for (i_4 = 0; i_4 < this.field1820; i_4++) {
         this.field1819[i_4] = new int[class310_3.method5661((byte) -19)];
      }

      for (i_4 = 0; i_4 < this.field1820; i_4++) {
         for (int i_5 = 0; i_5 < this.field1819[i_4].length; i_5++) {
            this.field1819[i_4][i_5] = class310_3.method5661((byte) -45);
         }
      }

   }

   static final void method3128(class226[] arr_0, int i_1, int i_2) {
      for (int i_3 = 0; i_3 < arr_0.length; i_3++) {
         class226 class226_4 = arr_0[i_3];
         if (class226_4 != null && class226_4.field2670 == i_1 && (!class226_4.field2585 || !class55.method896(class226_4, (byte) 97))) {
            if (class226_4.field2588 == 0) {
               if (!class226_4.field2585 && class55.method896(class226_4, (byte) 54) && class226_4 != class262.field3350) {
                  continue;
               }

               method3128(arr_0, class226_4.field2586, -1995367376);
               if (class226_4.field2672 != null) {
                  method3128(class226_4.field2672, class226_4.field2586, 1444948177);
               }

               class70 class70_5 = (class70) client.field833.method5968((long)class226_4.field2586);
               if (class70_5 != null) {
                  class267.method4726(class70_5.field563, 480576960);
               }
            }

            if (class226_4.field2588 == 6) {
               int i_6;
               if (class226_4.field2633 != -1 || class226_4.field2634 != -1) {
                  boolean bool_8 = class8.method91(class226_4, -2010469963);
                  if (bool_8) {
                     i_6 = class226_4.field2634;
                  } else {
                     i_6 = class226_4.field2633;
                  }

                  if (i_6 != -1) {
                     class269 class269_7 = class260.method4510(i_6, -1656189059);

                     for (class226_4.field2715 += client.field850; class226_4.field2715 > class269_7.field3552[class226_4.field2718]; class181.method3609(class226_4, -1729167843)) {
                        class226_4.field2715 -= class269_7.field3552[class226_4.field2718];
                        ++class226_4.field2718;
                        if (class226_4.field2718 >= class269_7.field3550.length) {
                           class226_4.field2718 -= class269_7.field3558;
                           if (class226_4.field2718 < 0 || class226_4.field2718 >= class269_7.field3550.length) {
                              class226_4.field2718 = 0;
                           }
                        }
                     }
                  }
               }

               if (class226_4.field2642 != 0 && !class226_4.field2585) {
                  int i_9 = class226_4.field2642 >> 16;
                  i_6 = class226_4.field2642 << 16 >> 16;
                  i_9 *= client.field850;
                  i_6 *= client.field850;
                  class226_4.field2583 = i_9 + class226_4.field2583 & 0x7ff;
                  class226_4.field2638 = i_6 + class226_4.field2638 & 0x7ff;
                  class181.method3609(class226_4, -1991799464);
               }
            }
         }
      }

   }

   static final void method3127(String string_0, byte b_1) {
      class196 class196_2 = class68.method1249(class192.field2319, client.field694.field1328, (byte) 1);
      class196_2.field2360.method5482(class132.method2906(string_0, (byte) -127), (byte) -10);
      class196_2.field2360.method5492(string_0, -1748540705);
      client.field694.method2295(class196_2, 1332920645);
   }

   static final void method3129(boolean bool_0, int i_1) {
      for (int i_2 = 0; i_2 < client.field690; i_2++) {
         class88 class88_3 = client.field689[client.field691[i_2]];
         if (class88_3 != null && class88_3.vmethod2080(1494358679) && class88_3.field1135.field3494 == bool_0 && class88_3.field1135.method4713(1926927124)) {
            int i_4 = class88_3.field990 >> 7;
            int i_5 = class88_3.field938 >> 7;
            if (i_4 >= 0 && i_4 < 104 && i_5 >= 0 && i_5 < 104) {
               if (class88_3.field941 == 1 && (class88_3.field990 & 0x7f) == 64 && (class88_3.field938 & 0x7f) == 64) {
                  if (client.field745[i_4][i_5] == client.field746) {
                     continue;
                  }

                  client.field745[i_4][i_5] = client.field746;
               }

               long long_6 = class88.method2085(0, 0, 1, !class88_3.field1135.field3527, client.field691[i_2], (byte) -105);
               class88_3.field983 = client.field655;
               class67.field536.method3142(class151.field1949, class88_3.field990, class88_3.field938, class97.method2165(class88_3.field941 * 64 - 64 + class88_3.field990, class88_3.field941 * 64 - 64 + class88_3.field938, class151.field1949, 2063793979), class88_3.field941 * 64 - 64 + 60, class88_3, class88_3.field939, long_6, class88_3.field950);
            }
         }
      }

   }

}
