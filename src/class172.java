public class class172 extends class179 {

   long field2029 = System.nanoTime();

   public int vmethod3547(int i_1, int i_2, int i_3) {
      long long_4 = 1000000L * (long)i_2;
      long long_6 = this.field2029 - System.nanoTime();
      if (long_6 < long_4) {
         long_6 = long_4;
      }

      class236.method4132(long_6 / 1000000L);
      long long_8 = System.nanoTime();

      int i_10;
      for (i_10 = 0; i_10 < 10 && (i_10 < 1 || this.field2029 < long_8); this.field2029 += (long)i_1 * 1000000L) {
         ++i_10;
      }

      if (this.field2029 < long_8) {
         this.field2029 = long_8;
      }

      return i_10;
   }

   public void vmethod3550(int i_1) {
      this.field2029 = System.nanoTime();
   }

}
