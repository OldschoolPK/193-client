public class class42 implements class46 {

   static String field317;
   static int field312;
   static class91 field308;
   static int field313;
   int field314;
   int field315;
   int field304;
   int field307;
   int field306;
   int field309;

   public boolean vmethod814(int i_1, int i_2, int i_3) {
      return i_1 >> 6 == this.field306 && i_2 >> 6 == this.field309;
   }

   public boolean vmethod813(int i_1, int i_2, int i_3, int i_4) {
      return i_1 >= this.field314 && i_1 < this.field315 + this.field314 ? i_2 >> 6 == this.field304 && i_3 >> 6 == this.field307 : false;
   }

   public void vmethod812(class35 class35_1, short s_2) {
      if (class35_1.field234 > this.field306) {
         class35_1.field234 = this.field306;
      }

      if (class35_1.field228 < this.field306) {
         class35_1.field228 = this.field306;
      }

      if (class35_1.field236 > this.field309) {
         class35_1.field236 = this.field309;
      }

      if (class35_1.field239 < this.field309) {
         class35_1.field239 = this.field309;
      }

   }

   public class222 vmethod835(int i_1, int i_2, byte b_3) {
      if (!this.vmethod814(i_1, i_2, 639074408)) {
         return null;
      } else {
         int i_4 = this.field304 * 64 - this.field306 * 64 + i_1;
         int i_5 = this.field307 * 64 - this.field309 * 64 + i_2;
         return new class222(this.field314, i_4, i_5);
      }
   }

   public int[] vmethod819(int i_1, int i_2, int i_3, int i_4) {
      if (!this.vmethod813(i_1, i_2, i_3, -210698464)) {
         return null;
      } else {
         int[] ints_5 = new int[] {this.field306 * 64 - this.field304 * 64 + i_2, i_3 + (this.field309 * 64 - this.field307 * 64)};
         return ints_5;
      }
   }

   void method613(int i_1) {
   }

   public void vmethod825(class310 class310_1, byte b_2) {
      this.field314 = class310_1.method5661((byte) 12);
      this.field315 = class310_1.method5661((byte) -20);
      this.field304 = class310_1.method5729(632506140);
      this.field307 = class310_1.method5729(-1411519441);
      this.field306 = class310_1.method5729(1842979517);
      this.field309 = class310_1.method5729(2023699025);
      this.method613(-154945566);
   }

   static boolean method624(int i_0, int i_1) {
      return i_0 == 57 || i_0 == 58 || i_0 == 1007 || i_0 == 25 || i_0 == 30;
   }

   static final void method626(double d_0) {
      class139.method3047(d_0);
      ((class128) class139.field1801).method2789(d_0);
      class265.field3440.method3375();
      class282.field3617.field1057 = d_0;
      class18.method187(-857243938);
   }

   static final void method619(int i_0, int i_1) {
      class31.method333(1640489535);
      switch(i_0) {
      case 1:
         class94.field1191 = 24;
         class62.method1087("", "You were disconnected from the server.", "", -967065540);
         break;
      case 2:
         class100.method2257(804061602);
      }

   }

   static void method627(String string_0, boolean bool_1, byte b_2) {
      string_0 = string_0.toLowerCase();
      short[] shorts_3 = new short[16];
      int i_4 = 0;

      for (int i_5 = 0; i_5 < class99.field1278; i_5++) {
         class265 class265_6 = class66.method1223(i_5, -1555536558);
         if ((!bool_1 || class265_6.field3484) && class265_6.field3432 * -582464211 == -1 && class265_6.field3444.toLowerCase().indexOf(string_0) != -1) {
            if (i_4 >= 250) {
               class196.field2367 = -1;
               class153.field1970 = null;
               return;
            }

            if (i_4 >= shorts_3.length) {
               short[] shorts_7 = new short[shorts_3.length * 2];

               for (int i_8 = 0; i_8 < i_4; i_8++) {
                  shorts_7[i_8] = shorts_3[i_8];
               }

               shorts_3 = shorts_7;
            }

            shorts_3[i_4++] = (short)i_5;
         }
      }

      class153.field1970 = shorts_3;
      class247.field3214 = 0;
      class196.field2367 = i_4;
      String[] arr_9 = new String[class196.field2367];

      for (int i_10 = 0; i_10 < class196.field2367; i_10++) {
         arr_9[i_10] = class66.method1223(shorts_3[i_10], 264303680).field3444;
      }

      short[] shorts_11 = class153.field1970;
      class89.method2093(arr_9, shorts_11, 0, arr_9.length - 1, -483488480);
   }

}
