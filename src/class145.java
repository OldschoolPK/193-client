public final class class145 {

   int field1896;
   int field1898;
   public class144 field1899;
   int field1901;
   public class144 field1897;
   int field1895;
   int field1900;
   public long field1902 = 0L;
   int field1903 = 0;

   public static boolean method3314(int i_0, byte b_1) {
      return i_0 == class229.field2766.field2767;
   }

   public static boolean method3313(int i_0, byte b_1) {
      return (i_0 >> 29 & 0x1) != 0;
   }

   public static String method3315(CharSequence charsequence_0, int i_1) {
      int i_2 = charsequence_0.length();
      StringBuilder stringbuilder_3 = new StringBuilder(i_2);

      for (int i_4 = 0; i_4 < i_2; i_4++) {
         char var_5 = charsequence_0.charAt(i_4);
         if ((var_5 < 97 || var_5 > 122) && (var_5 < 65 || var_5 > 90) && (var_5 < 48 || var_5 > 57) && var_5 != 46 && var_5 != 45 && var_5 != 42 && var_5 != 95) {
            if (var_5 == 32) {
               stringbuilder_3.append('+');
            } else {
               byte b_6 = class221.method3992(var_5, -1961396660);
               stringbuilder_3.append('%');
               int i_7 = b_6 >> 4 & 0xf;
               if (i_7 >= 10) {
                  stringbuilder_3.append((char)(i_7 + 55));
               } else {
                  stringbuilder_3.append((char)(i_7 + 48));
               }

               i_7 = b_6 & 0xf;
               if (i_7 >= 10) {
                  stringbuilder_3.append((char)(i_7 + 55));
               } else {
                  stringbuilder_3.append((char)(i_7 + 48));
               }
            }
         } else {
            stringbuilder_3.append(var_5);
         }
      }

      return stringbuilder_3.toString();
   }

}
