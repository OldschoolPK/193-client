public class class252 extends class184 {

   static class252[] field3243;
   public static int field3262;
   static class244 field3241;
   public static class154 field3240 = new class154(256);
   public int field3245 = -1;
   int field3246 = -1;
   public int field3249 = 0;
   public boolean field3250 = true;
   public boolean field3263 = false;
   public String[] field3244 = new String[5];
   int field3255 = Integer.MAX_VALUE;
   int field3256 = Integer.MAX_VALUE;
   int field3257 = Integer.MIN_VALUE;
   int field3258 = Integer.MIN_VALUE;
   public class266 field3259;
   public class249 field3260;
   public int field3252;
   public final int field3248;
   public String field3247;
   public int field3251;
   int[] field3254;
   public String field3253;
   int[] field3261;
   byte[] field3242;

   class252(int i_1) {
      this.field3259 = class266.field3489;
      this.field3260 = class249.field3226;
      this.field3252 = -1;
      this.field3248 = i_1;
   }

   public class335 method4374(boolean bool_1, int i_2) {
      int i_3 = this.field3245;
      return this.method4360(i_3, 954144981);
   }

   class335 method4360(int i_1, int i_2) {
      if (i_1 < 0) {
         return null;
      } else {
         class335 class335_3 = (class335) field3240.method3376((long)i_1);
         if (class335_3 != null) {
            return class335_3;
         } else {
            class335_3 = class230.method4115(field3241, i_1, 0, (byte) 97);
            if (class335_3 != null) {
               field3240.method3374(class335_3, (long)i_1);
            }

            return class335_3;
         }
      }
   }

   void method4357(class310 class310_1, int i_2, int i_3) {
      if (i_2 == 1) {
         this.field3245 = class310_1.method5519((short) 1005);
      } else if (i_2 == 2) {
         this.field3246 = class310_1.method5519((short) 1005);
      } else if (i_2 == 3) {
         this.field3247 = class310_1.method5589(-1982725224);
      } else if (i_2 == 4) {
         this.field3251 = class310_1.method5506((short) 239);
      } else if (i_2 == 5) {
         class310_1.method5506((short) 239);
      } else if (i_2 == 6) {
         this.field3249 = class310_1.method5661((byte) 37);
      } else {
         int i_4;
         if (i_2 == 7) {
            i_4 = class310_1.method5661((byte) 67);
            if ((i_4 & 0x1) == 0) {
               this.field3250 = false;
            }

            if ((i_4 & 0x2) == 2) {
               this.field3263 = true;
            }
         } else if (i_2 == 8) {
            class310_1.method5661((byte) 76);
         } else if (i_2 >= 10 && i_2 <= 14) {
            this.field3244[i_2 - 10] = class310_1.method5589(1826945575);
         } else if (i_2 == 15) {
            i_4 = class310_1.method5661((byte) -11);
            this.field3254 = new int[i_4 * 2];

            int i_5;
            for (i_5 = 0; i_5 < i_4 * 2; i_5++) {
               this.field3254[i_5] = class310_1.method5505((short) 1506);
            }

            class310_1.method5507(-198385064);
            i_5 = class310_1.method5661((byte) -29);
            this.field3261 = new int[i_5];

            int i_6;
            for (i_6 = 0; i_6 < this.field3261.length; i_6++) {
               this.field3261[i_6] = class310_1.method5507(157423910);
            }

            this.field3242 = new byte[i_4];

            for (i_6 = 0; i_6 < i_4; i_6++) {
               this.field3242[i_6] = class310_1.method5535((byte) 0);
            }
         } else if (i_2 != 16) {
            if (i_2 == 17) {
               this.field3253 = class310_1.method5589(-2143763482);
            } else if (i_2 == 18) {
               class310_1.method5519((short) 1005);
            } else if (i_2 == 19) {
               this.field3252 = class310_1.method5729(2053157232);
            } else if (i_2 == 21) {
               class310_1.method5507(1101795617);
            } else if (i_2 == 22) {
               class310_1.method5507(1163547473);
            } else if (i_2 == 23) {
               class310_1.method5661((byte) 50);
               class310_1.method5661((byte) 17);
               class310_1.method5661((byte) 78);
            } else if (i_2 == 24) {
               class310_1.method5505((short) 19839);
               class310_1.method5505((short) 9145);
            } else if (i_2 == 25) {
               class310_1.method5519((short) 1005);
            } else if (i_2 == 28) {
               class310_1.method5661((byte) 10);
            } else if (i_2 == 29) {
               class266[] arr_7 = new class266[] {class266.field3491, class266.field3489, class266.field3490};
               this.field3259 = (class266) class152.method3363(arr_7, class310_1.method5661((byte) -23), -2029089073);
            } else if (i_2 == 30) {
               class249[] arr_8 = new class249[] {class249.field3225, class249.field3229, class249.field3226};
               this.field3260 = (class249) class152.method3363(arr_8, class310_1.method5661((byte) 66), 1157974487);
            }
         }
      }

   }

   public int method4361(int i_1) {
      return this.field3248;
   }

   void method4359(class310 class310_1, int i_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -51);
         if (i_3 == 0) {
            return;
         }

         this.method4357(class310_1, i_3, -694353382);
      }
   }

   void method4358(int i_1) {
      if (this.field3254 != null) {
         for (int i_2 = 0; i_2 < this.field3254.length; i_2 += 2) {
            if (this.field3254[i_2] < this.field3255) {
               this.field3255 = this.field3254[i_2];
            } else if (this.field3254[i_2] > this.field3257) {
               this.field3257 = this.field3254[i_2];
            }

            if (this.field3254[i_2 + 1] < this.field3256) {
               this.field3256 = this.field3254[i_2 + 1];
            } else if (this.field3254[i_2 + 1] > this.field3258) {
               this.field3258 = this.field3254[i_2 + 1];
            }
         }
      }

   }

   public static class255 method4372(int i_0, int i_1) {
      class255 class255_2 = (class255) class255.field3288.method3376((long)i_0);
      if (class255_2 != null) {
         return class255_2;
      } else {
         byte[] bytes_3 = class255.field3287.method4160(1, i_0, (short) -14445);
         class255_2 = new class255();
         if (bytes_3 != null) {
            class255_2.method4415(new class310(bytes_3), i_0, 731438667);
         }

         class255_2.method4414(1848989672);
         class255.field3288.method3374(class255_2, (long)i_0);
         return class255_2;
      }
   }

}
