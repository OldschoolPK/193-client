import java.util.HashMap;

public class class248 {

   static int[] field3219;
   static int field3222;
   final HashMap field3223 = new HashMap();
   class329 field3217 = new class329(0, 0);
   int[] field3218 = new int[2048];
   int[] field3216 = new int[2048];
   int field3220 = 0;

   public class248() {
      class195.method3651(1214913884);
   }

   class335 method4310(int i_1, int i_2) {
      if (!this.field3223.containsKey(Integer.valueOf(i_1))) {
         this.method4309(i_1, 105508707);
      }

      return (class335) this.field3223.get(Integer.valueOf(i_1));
   }

   void method4309(int i_1, int i_2) {
      int i_3 = i_1 * 2 + 1;
      double[] doubles_4 = class245.method4261(0.0D, (double)((float)i_1 / 3.0F), i_1, 961736597);
      double d_5 = doubles_4[i_1] * doubles_4[i_1];
      int[] ints_7 = new int[i_3 * i_3];
      boolean bool_8 = false;

      for (int i_9 = 0; i_9 < i_3; i_9++) {
         for (int i_10 = 0; i_10 < i_3; i_10++) {
            int i_11 = ints_7[i_10 + i_3 * i_9] = (int)(256.0D * (doubles_4[i_10] * doubles_4[i_9] / d_5));
            if (!bool_8 && i_11 > 0) {
               bool_8 = true;
            }
         }
      }

      class335 class335_12 = new class335(ints_7, i_3, i_3);
      this.field3223.put(Integer.valueOf(i_1), class335_12);
   }

   public final void method4312(int i_1) {
      this.field3220 = 0;
   }

   public final void method4311(int i_1, int i_2, int i_3) {
      if (this.field3220 < this.field3218.length) {
         this.field3218[this.field3220] = i_1;
         this.field3216[this.field3220] = i_2;
         ++this.field3220;
      }
   }

   void method4314(class335 class335_1, class335 class335_2, class329 class329_3, byte b_4) {
      if (class329_3.field3879 != 0 && class329_3.field3880 != 0) {
         int i_5 = 0;
         int i_6 = 0;
         if (class329_3.field3881 == 0) {
            i_5 = class335_1.field3913 - class329_3.field3879;
         }

         if (class329_3.field3878 == 0) {
            i_6 = class335_1.field3911 - class329_3.field3880;
         }

         int i_7 = i_5 + i_6 * class335_1.field3913;
         int i_8 = class335_2.field3913 * class329_3.field3878 + class329_3.field3881;

         for (int i_9 = 0; i_9 < class329_3.field3880; i_9++) {
            for (int i_10 = 0; i_10 < class329_3.field3879; i_10++) {
               int i_10001 = i_8++;
               class335_2.field3917[i_10001] += class335_1.field3917[i_7++];
            }

            i_7 += class335_1.field3913 - class329_3.field3879;
            i_8 += class335_2.field3913 - class329_3.field3879;
         }

      }
   }

   public final void method4313(int i_1, int i_2, class335 class335_3, float f_4, byte b_5) {
      int i_6 = (int)(f_4 * 18.0F);
      class335 class335_7 = this.method4310(i_6, -2015707218);
      int i_8 = i_6 * 2 + 1;
      class329 class329_9 = new class329(0, 0, class335_3.field3913, class335_3.field3911);
      class329 class329_10 = new class329(0, 0);
      this.field3217.method5970(i_8, i_8, -1587228886);
      System.nanoTime();

      int i_11;
      int i_12;
      int i_13;
      for (i_11 = 0; i_11 < this.field3220; i_11++) {
         i_12 = this.field3218[i_11];
         i_13 = this.field3216[i_11];
         int i_14 = (int)((float)(i_12 - i_1) * f_4) - i_6;
         int i_15 = (int)((float)class335_3.field3911 - (float)(i_13 - i_2) * f_4) - i_6;
         this.field3217.method5969(i_14, i_15, 354280622);
         this.field3217.method5971(class329_9, class329_10, -769796699);
         this.method4314(class335_7, class335_3, class329_10, (byte) 36);
      }

      System.nanoTime();
      System.nanoTime();

      for (i_11 = 0; i_11 < class335_3.field3917.length; i_11++) {
         if (class335_3.field3917[i_11] == 0) {
            class335_3.field3917[i_11] = -16777216;
         } else {
            i_12 = (class335_3.field3917[i_11] + 64 - 1) / 256;
            if (i_12 <= 0) {
               class335_3.field3917[i_11] = -16777216;
            } else {
               if (i_12 > field3219.length) {
                  i_12 = field3219.length;
               }

               i_13 = field3219[i_12 - 1];
               class335_3.field3917[i_11] = ~0xffffff | i_13;
            }
         }
      }

      System.nanoTime();
   }

}
