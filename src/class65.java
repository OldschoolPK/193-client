public class class65 {

   static int field521;
   class101 field522;
   int[] field523;
   String[] field524;
   int field525 = -1;

   static int method1213(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      boolean bool_4 = true;
      class226 class226_5;
      if (i_0 >= 2000) {
         i_0 -= 1000;
         class226_5 = class181.method3610(class85.field1095[--class253.field3267], -448928611);
         bool_4 = false;
      } else {
         class226_5 = bool_2 ? class223.field2561 : class253.field3264;
      }

      int i_12;
      if (i_0 == 1300) {
         i_12 = class85.field1095[--class253.field3267] - 1;
         if (i_12 >= 0 && i_12 <= 9) {
            class226_5.method4054(i_12, class85.field1096[--class85.field1105], 925556129);
            return 1;
         } else {
            --class85.field1105;
            return 1;
         }
      } else {
         int i_7;
         if (i_0 == 1301) {
            class253.field3267 -= 2;
            i_12 = class85.field1095[class253.field3267];
            i_7 = class85.field1095[class253.field3267 + 1];
            class226_5.field2667 = class92.method2103(i_12, i_7, -100010964);
            return 1;
         } else if (i_0 == 1302) {
            class226_5.field2671 = class85.field1095[--class253.field3267] == 1;
            return 1;
         } else if (i_0 == 1303) {
            class226_5.field2668 = class85.field1095[--class253.field3267];
            return 1;
         } else if (i_0 == 1304) {
            class226_5.field2669 = class85.field1095[--class253.field3267];
            return 1;
         } else if (i_0 == 1305) {
            class226_5.field2665 = class85.field1096[--class85.field1105];
            return 1;
         } else if (i_0 == 1306) {
            class226_5.field2576 = class85.field1096[--class85.field1105];
            return 1;
         } else if (i_0 == 1307) {
            class226_5.field2662 = null;
            return 1;
         } else if (i_0 == 1308) {
            class226_5.field2623 = class85.field1095[--class253.field3267] == 1;
            return 1;
         } else if (i_0 == 1309) {
            --class253.field3267;
            return 1;
         } else {
            int i_8;
            byte[] bytes_10;
            if (i_0 != 1350) {
               byte b_6;
               if (i_0 == 1351) {
                  class253.field3267 -= 2;
                  b_6 = 10;
                  bytes_10 = new byte[] {(byte)class85.field1095[class253.field3267]};
                  byte[] bytes_11 = new byte[] {(byte)class85.field1095[class253.field3267 + 1]};
                  class68.method1248(class226_5, b_6, bytes_10, bytes_11, -21779272);
                  return 1;
               } else if (i_0 == 1352) {
                  class253.field3267 -= 3;
                  i_12 = class85.field1095[class253.field3267] - 1;
                  i_7 = class85.field1095[class253.field3267 + 1];
                  i_8 = class85.field1095[class253.field3267 + 2];
                  if (i_12 >= 0 && i_12 <= 9) {
                     class75.method1303(class226_5, i_12, i_7, i_8, -1395561786);
                     return 1;
                  } else {
                     throw new RuntimeException();
                  }
               } else if (i_0 == 1353) {
                  b_6 = 10;
                  i_7 = class85.field1095[--class253.field3267];
                  i_8 = class85.field1095[--class253.field3267];
                  class75.method1303(class226_5, b_6, i_7, i_8, 715521473);
                  return 1;
               } else if (i_0 == 1354) {
                  --class253.field3267;
                  i_12 = class85.field1095[class253.field3267] - 1;
                  if (i_12 >= 0 && i_12 <= 9) {
                     class19.method191(class226_5, i_12, 705653347);
                     return 1;
                  } else {
                     throw new RuntimeException();
                  }
               } else if (i_0 == 1355) {
                  b_6 = 10;
                  class19.method191(class226_5, b_6, 1825626795);
                  return 1;
               } else {
                  return 2;
               }
            } else {
               byte[] bytes_9 = null;
               bytes_10 = null;
               if (bool_4) {
                  class253.field3267 -= 10;

                  for (i_8 = 0; i_8 < 10 && class85.field1095[i_8 + class253.field3267] >= 0; i_8 += 2) {
                     ;
                  }

                  if (i_8 > 0) {
                     bytes_9 = new byte[i_8 / 2];
                     bytes_10 = new byte[i_8 / 2];

                     for (i_8 -= 2; i_8 >= 0; i_8 -= 2) {
                        bytes_9[i_8 / 2] = (byte)class85.field1095[i_8 + class253.field3267];
                        bytes_10[i_8 / 2] = (byte)class85.field1095[i_8 + class253.field3267 + 1];
                     }
                  }
               } else {
                  class253.field3267 -= 2;
                  bytes_9 = new byte[] {(byte)class85.field1095[class253.field3267]};
                  bytes_10 = new byte[] {(byte)class85.field1095[class253.field3267 + 1]};
               }

               i_8 = class85.field1095[--class253.field3267] - 1;
               if (i_8 >= 0 && i_8 <= 9) {
                  class68.method1248(class226_5, i_8, bytes_9, bytes_10, -1238651075);
                  return 1;
               } else {
                  throw new RuntimeException();
               }
            }
         }
      }
   }

}
