public class class209 {

   static final byte[] field2484 = new byte[] {(byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 1, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 2, (byte) 0, (byte) 1, (byte) 2, (byte) 1, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0, (byte) 0};
   class310 field2478 = new class310((byte[]) null);
   int[] field2479;
   int[] field2481;
   int[] field2482;
   int[] field2483;
   int field2487;
   int field2480;
   long field2486;

   class209(byte[] bytes_1) {
      this.method3887(bytes_1);
   }

   class209() {
   }

   int method3845(int i_1) {
      byte b_2 = this.field2478.field3752[this.field2478.field3751];
      int i_5;
      if (b_2 < 0) {
         i_5 = b_2 & 0xff;
         this.field2483[i_1] = i_5;
         ++this.field2478.field3751;
      } else {
         i_5 = this.field2483[i_1];
      }

      if (i_5 != 240 && i_5 != 247) {
         return this.method3846(i_1, i_5);
      } else {
         int i_3 = this.field2478.method5520((byte) -68);
         if (i_5 == 247 && i_3 > 0) {
            int i_4 = this.field2478.field3752[this.field2478.field3751] & 0xff;
            if (i_4 >= 241 && i_4 <= 243 || i_4 == 246 || i_4 == 248 || i_4 >= 250 && i_4 <= 252 || i_4 == 254) {
               ++this.field2478.field3751;
               this.field2483[i_1] = i_4;
               return this.method3846(i_1, i_4);
            }
         }

         this.field2478.field3751 += i_3;
         return 0;
      }
   }

   void method3837() {
      this.field2478.field3752 = null;
      this.field2479 = null;
      this.field2481 = null;
      this.field2482 = null;
      this.field2483 = null;
   }

   boolean method3838() {
      return this.field2478.field3752 != null;
   }

   void method3887(byte[] bytes_1) {
      this.field2478.field3752 = bytes_1;
      this.field2478.field3751 = 10;
      int i_2 = this.field2478.method5729(-1931039616);
      this.field2487 = this.field2478.method5729(-1631266479);
      this.field2480 = 500000;
      this.field2479 = new int[i_2];

      int i_3;
      int i_5;
      for (i_3 = 0; i_3 < i_2; this.field2478.field3751 += i_5) {
         int i_4 = this.field2478.method5507(-428637090);
         i_5 = this.field2478.method5507(-335584272);
         if (i_4 == 1297379947) {
            this.field2479[i_3] = this.field2478.field3751;
            ++i_3;
         }
      }

      this.field2486 = 0L;
      this.field2481 = new int[i_2];

      for (i_3 = 0; i_3 < i_2; i_3++) {
         this.field2481[i_3] = this.field2479[i_3];
      }

      this.field2482 = new int[i_2];
      this.field2483 = new int[i_2];
   }

   void method3840(int i_1) {
      this.field2478.field3751 = this.field2481[i_1];
   }

   int method3848() {
      int i_1 = this.field2481.length;
      int i_2 = -1;
      int i_3 = Integer.MAX_VALUE;

      for (int i_4 = 0; i_4 < i_1; i_4++) {
         if (this.field2481[i_4] >= 0 && this.field2482[i_4] < i_3) {
            i_2 = i_4;
            i_3 = this.field2482[i_4];
         }
      }

      return i_2;
   }

   int method3839() {
      return this.field2481.length;
   }

   int method3844(int i_1) {
      int i_2 = this.method3845(i_1);
      return i_2;
   }

   void method3843(int i_1) {
      int i_2 = this.field2478.method5520((byte) -14);
      this.field2482[i_1] += i_2;
   }

   int method3846(int i_1, int i_2) {
      int i_4;
      if (i_2 == 255) {
         int i_7 = this.field2478.method5661((byte) 23);
         i_4 = this.field2478.method5520((byte) -124);
         if (i_7 == 47) {
            this.field2478.field3751 += i_4;
            return 1;
         } else if (i_7 == 81) {
            int i_5 = this.field2478.method5506((short) 239);
            i_4 -= 3;
            int i_6 = this.field2482[i_1];
            this.field2486 += (long)i_6 * (long)(this.field2480 - i_5);
            this.field2480 = i_5;
            this.field2478.field3751 += i_4;
            return 2;
         } else {
            this.field2478.field3751 += i_4;
            return 3;
         }
      } else {
         byte b_3 = field2484[i_2 - 128];
         i_4 = i_2;
         if (b_3 >= 1) {
            i_4 = i_2 | this.field2478.method5661((byte) -5) << 8;
         }

         if (b_3 >= 2) {
            i_4 |= this.field2478.method5661((byte) 20) << 16;
         }

         return i_4;
      }
   }

   long method3847(int i_1) {
      return this.field2486 + (long)i_1 * (long)this.field2480;
   }

   void method3842() {
      this.field2478.field3751 = -1;
   }

   void method3841(int i_1) {
      this.field2481[i_1] = this.field2478.field3751;
   }

   boolean method3860() {
      int i_1 = this.field2481.length;

      for (int i_2 = 0; i_2 < i_1; i_2++) {
         if (this.field2481[i_2] >= 0) {
            return false;
         }
      }

      return true;
   }

   void method3850(long long_1) {
      this.field2486 = long_1;
      int i_3 = this.field2481.length;

      for (int i_4 = 0; i_4 < i_3; i_4++) {
         this.field2482[i_4] = 0;
         this.field2483[i_4] = 0;
         this.field2478.field3751 = this.field2479[i_4];
         this.method3843(i_4);
         this.field2481[i_4] = this.field2478.field3751;
      }

   }

}
