public final class class36 {

   static String field252;
   static class246 field253;
   final int[] field247;

   class36() {
      this.field247 = new int[4096];
   }

   class36(int[] ints_1) {
      this.field247 = ints_1;
   }

   final int method462(int i_1, int i_2, int i_3) {
      return this.field247[i_1 + i_2 * 64];
   }

   static final int method463(int i_0) {
      return client.field785 - 1;
   }

   static final void method467(int i_0) {
      for (class86 class86_1 = (class86) client.field734.method4879(); class86_1 != null; class86_1 = (class86) client.field734.method4884()) {
         if (class86_1.field1112 == class151.field1949 && !class86_1.field1120) {
            if (client.field655 >= class86_1.field1119) {
               class86_1.method2061(client.field850, 1007700617);
               if (class86_1.field1120) {
                  class86_1.method3628();
               } else {
                  class67.field536.method3142(class86_1.field1112, class86_1.field1113, class86_1.field1114, class86_1.field1111, 60, class86_1, 0, -1L, false);
               }
            }
         } else {
            class86_1.method3628();
         }
      }

   }

   static final int method468(int i_0, int i_1, int i_2) {
      int i_3 = class18.method190(i_0 - 1, i_1 - 1, (byte) 66) + class18.method190(i_0 + 1, i_1 - 1, (byte) 55) + class18.method190(i_0 - 1, 1 + i_1, (byte) 40) + class18.method190(1 + i_0, 1 + i_1, (byte) 8);
      int i_4 = class18.method190(i_0 - 1, i_1, (byte) 69) + class18.method190(1 + i_0, i_1, (byte) 89) + class18.method190(i_0, i_1 - 1, (byte) 27) + class18.method190(i_0, 1 + i_1, (byte) 67);
      int i_5 = class18.method190(i_0, i_1, (byte) 16);
      return i_3 / 16 + i_4 / 8 + i_5 / 4;
   }

}
