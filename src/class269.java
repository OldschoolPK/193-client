import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class class269 extends class184 {

   static class244 field3546;
   static class244 field3547;
   static class244 field3553;
   public static class154 field3562 = new class154(64);
   public static class154 field3549 = new class154(100);
   public int field3558 = -1;
   public boolean field3556 = false;
   public int field3557 = 5;
   public int field3563 = -1;
   public int field3559 = -1;
   public int field3554 = 99;
   public int field3560 = -1;
   public int field3555 = -1;
   public int field3548 = 2;
   public int[] field3552;
   int[] field3545;
   public int[] field3550;
   int[] field3551;
   public int[] field3561;

   void method4770(class310 class310_1, int i_2, int i_3) {
      int i_4;
      int i_5;
      if (i_2 == 1) {
         i_4 = class310_1.method5729(-1819107024);
         this.field3552 = new int[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3552[i_5] = class310_1.method5729(-911776338);
         }

         this.field3550 = new int[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3550[i_5] = class310_1.method5729(1277575577);
         }

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3550[i_5] += class310_1.method5729(-1349449933) << 16;
         }
      } else if (i_2 == 2) {
         this.field3558 = class310_1.method5729(-1350087884);
      } else if (i_2 == 3) {
         i_4 = class310_1.method5661((byte) -12);
         this.field3545 = new int[i_4 + 1];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3545[i_5] = class310_1.method5661((byte) -8);
         }

         this.field3545[i_4] = 9999999;
      } else if (i_2 == 4) {
         this.field3556 = true;
      } else if (i_2 == 5) {
         this.field3557 = class310_1.method5661((byte) -76);
      } else if (i_2 == 6) {
         this.field3563 = class310_1.method5729(-286516371);
      } else if (i_2 == 7) {
         this.field3559 = class310_1.method5729(1550839069);
      } else if (i_2 == 8) {
         this.field3554 = class310_1.method5661((byte) -23);
      } else if (i_2 == 9) {
         this.field3560 = class310_1.method5661((byte) -26);
      } else if (i_2 == 10) {
         this.field3555 = class310_1.method5661((byte) 33);
      } else if (i_2 == 11) {
         this.field3548 = class310_1.method5661((byte) 53);
      } else if (i_2 == 12) {
         i_4 = class310_1.method5661((byte) -89);
         this.field3551 = new int[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3551[i_5] = class310_1.method5729(-1478290761);
         }

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3551[i_5] += class310_1.method5729(-2070830678) << 16;
         }
      } else if (i_2 == 13) {
         i_4 = class310_1.method5661((byte) -12);
         this.field3561 = new int[i_4];

         for (i_5 = 0; i_5 < i_4; i_5++) {
            this.field3561[i_5] = class310_1.method5506((short) 239);
         }
      }

   }

   public class136 method4761(class136 class136_1, int i_2, byte b_3) {
      i_2 = this.field3550[i_2];
      class147 class147_4 = class242.method4154(i_2 >> 16, (byte) -84);
      i_2 &= 0xffff;
      if (class147_4 == null) {
         return class136_1.method2921(true);
      } else {
         class136 class136_5 = class136_1.method2921(!class147_4.method3320(i_2, -1297210916));
         class136_5.method2929(class147_4, i_2);
         return class136_5;
      }
   }

   public class136 method4781(class136 class136_1, int i_2, class269 class269_3, int i_4, byte b_5) {
      i_2 = this.field3550[i_2];
      class147 class147_6 = class242.method4154(i_2 >> 16, (byte) 16);
      i_2 &= 0xffff;
      if (class147_6 == null) {
         return class269_3.method4761(class136_1, i_4, (byte) -73);
      } else {
         i_4 = class269_3.field3550[i_4];
         class147 class147_7 = class242.method4154(i_4 >> 16, (byte) -46);
         i_4 &= 0xffff;
         class136 class136_8;
         if (class147_7 == null) {
            class136_8 = class136_1.method2921(!class147_6.method3320(i_2, -1239705769));
            class136_8.method2929(class147_6, i_2);
            return class136_8;
         } else {
            class136_8 = class136_1.method2921(!class147_6.method3320(i_2, -1747080103) & !class147_7.method3320(i_4, -1880927779));
            class136_8.method2930(class147_6, i_2, class147_7, i_4, this.field3545);
            return class136_8;
         }
      }
   }

   class136 method4759(class136 class136_1, int i_2, int i_3, int i_4) {
      i_2 = this.field3550[i_2];
      class147 class147_5 = class242.method4154(i_2 >> 16, (byte) -99);
      i_2 &= 0xffff;
      if (class147_5 == null) {
         return class136_1.method2921(true);
      } else {
         class136 class136_6 = class136_1.method2921(!class147_5.method3320(i_2, -1644886228));
         i_3 &= 0x3;
         if (i_3 == 1) {
            class136_6.method3006();
         } else if (i_3 == 2) {
            class136_6.method2933();
         } else if (i_3 == 3) {
            class136_6.method2945();
         }

         class136_6.method2929(class147_5, i_2);
         if (i_3 == 1) {
            class136_6.method2945();
         } else if (i_3 == 2) {
            class136_6.method2933();
         } else if (i_3 == 3) {
            class136_6.method3006();
         }

         return class136_6;
      }
   }

   class136 method4785(class136 class136_1, int i_2, int i_3) {
      i_2 = this.field3550[i_2];
      class147 class147_4 = class242.method4154(i_2 >> 16, (byte) -61);
      i_2 &= 0xffff;
      if (class147_4 == null) {
         return class136_1.method2922(true);
      } else {
         class136 class136_5 = class136_1.method2922(!class147_4.method3320(i_2, -1353807148));
         class136_5.method2929(class147_4, i_2);
         return class136_5;
      }
   }

   public class136 method4765(class136 class136_1, int i_2, byte b_3) {
      int i_4 = this.field3550[i_2];
      class147 class147_5 = class242.method4154(i_4 >> 16, (byte) -3);
      i_4 &= 0xffff;
      if (class147_5 == null) {
         return class136_1.method2921(true);
      } else {
         class147 class147_6 = null;
         int i_7 = 0;
         if (this.field3551 != null && i_2 < this.field3551.length) {
            i_7 = this.field3551[i_2];
            class147_6 = class242.method4154(i_7 >> 16, (byte) -42);
            i_7 &= 0xffff;
         }

         class136 class136_8;
         if (class147_6 != null && i_7 != 65535) {
            class136_8 = class136_1.method2921(!class147_5.method3320(i_4, -1928540860) & !class147_6.method3320(i_7, -1217832294));
            class136_8.method2929(class147_5, i_4);
            class136_8.method2929(class147_6, i_7);
            return class136_8;
         } else {
            class136_8 = class136_1.method2921(!class147_5.method3320(i_4, -1661796819));
            class136_8.method2929(class147_5, i_4);
            return class136_8;
         }
      }
   }

   void method4757(int i_1) {
      if (this.field3560 == -1) {
         if (this.field3545 != null) {
            this.field3560 = 2;
         } else {
            this.field3560 = 0;
         }
      }

      if (this.field3555 == -1) {
         if (this.field3545 != null) {
            this.field3555 = 2;
         } else {
            this.field3555 = 0;
         }
      }

   }

   void method4772(class310 class310_1, byte b_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -89);
         if (i_3 == 0) {
            return;
         }

         this.method4770(class310_1, i_3, 360291550);
      }
   }

   static void method4789(class62 class62_0, byte b_1) {
      int i_3;
      int i_4;
      int i_5;
      if (class94.field1198) {
         while (true) {
            if (!class20.method202(-369903170)) {
               if (class63.field483 != 1 && (class161.field1989 || class63.field483 != 4)) {
                  break;
               }

               int i_2 = class94.field1188 + 280;
               if (class63.field489 >= i_2 && class63.field489 <= i_2 + 14 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(0, 0, -231667488);
                  break;
               }

               if (class63.field489 >= i_2 + 15 && class63.field489 <= i_2 + 80 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(0, 1, -231667488);
                  break;
               }

               i_3 = class94.field1188 + 390;
               if (class63.field489 >= i_3 && class63.field489 <= i_3 + 14 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(1, 0, -231667488);
                  break;
               }

               if (class63.field489 >= i_3 + 15 && class63.field489 <= i_3 + 80 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(1, 1, -231667488);
                  break;
               }

               i_4 = class94.field1188 + 500;
               if (class63.field489 >= i_4 && class63.field489 <= i_4 + 14 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(2, 0, -231667488);
                  break;
               }

               if (class63.field489 >= i_4 + 15 && class63.field489 <= i_4 + 80 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(2, 1, -231667488);
                  break;
               }

               i_5 = class94.field1188 + 610;
               if (class63.field489 >= i_5 && class63.field489 <= i_5 + 14 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(3, 0, -231667488);
                  break;
               }

               if (class63.field489 >= i_5 + 15 && class63.field489 <= i_5 + 80 && class63.field502 >= 4 && class63.field502 <= 18) {
                  class257.method4477(3, 1, -231667488);
                  break;
               }

               if (class63.field489 >= class94.field1188 + 708 && class63.field502 >= 4 && class63.field489 <= class94.field1188 + 708 + 50 && class63.field502 <= 20) {
                  class94.field1198 = false;
                  class25.field124.method6124(class94.field1188, 0);
                  class224.field2564.method6124(class94.field1188 + 382, 0);
                  class224.field2571.method6091(class94.field1188 + 382 - class224.field2571.field3905 / 2, 18);
                  break;
               }

               if (class94.field1206 != -1) {
                  class80 class80_6 = class80.field1012[class94.field1206];
                  class66.method1222(class80_6, 997445897);
                  class94.field1198 = false;
                  class25.field124.method6124(class94.field1188, 0);
                  class224.field2564.method6124(class94.field1188 + 382, 0);
                  class224.field2571.method6091(class94.field1188 + 382 - class224.field2571.field3905 / 2, 18);
               } else {
                  if (class94.field1182 > 0 && class249.field3224 != null && class63.field489 >= 0 && class63.field489 <= class249.field3224.field3905 && class63.field502 >= class143.field1892 / 2 - 50 && class63.field502 <= class143.field1892 / 2 + 50) {
                     --class94.field1182;
                  }

                  if (class94.field1182 < class94.field1207 && class8.field34 != null && class63.field489 >= class286.field3638 - class8.field34.field3905 - 5 && class63.field489 <= class286.field3638 && class63.field502 >= class143.field1892 / 2 - 50 && class63.field502 <= class143.field1892 / 2 + 50) {
                     ++class94.field1182;
                  }
               }
               break;
            }

            if (class241.field3135 == 13) {
               class94.field1198 = false;
               class25.field124.method6124(class94.field1188, 0);
               class224.field2564.method6124(class94.field1188 + 382, 0);
               class224.field2571.method6091(class94.field1188 + 382 - class224.field2571.field3905 / 2, 18);
               break;
            }

            if (class241.field3135 == 96) {
               if (class94.field1182 > 0 && class249.field3224 != null) {
                  --class94.field1182;
               }
            } else if (class241.field3135 == 97 && class94.field1182 < class94.field1207 && class8.field34 != null) {
               ++class94.field1182;
            }
         }

      } else {
         if ((class63.field483 == 1 || !class161.field1989 && class63.field483 == 4) && class63.field489 >= class94.field1188 + 765 - 50 && class63.field502 >= 453) {
            class282.field3617.field1063 = !class282.field3617.field1063;
            class18.method187(296728880);
            if (!class282.field3617.field1063) {
               class246 class246_22 = class217.field2520;
               i_3 = class246_22.method4206("scape main", -2131214347);
               i_4 = class246_22.method4235(i_3, "", 1284626629);
               class85.method2054(class246_22, i_3, i_4, 255, false, 781493841);
            } else {
               class38.method588(-1183681872);
            }
         }

         if (client.field653 != 5) {
            if (class94.field1208 == -1L) {
               class94.field1208 = class298.method5270(255749540) + 1000L;
            }

            long long_23 = class298.method5270(255749540);
            boolean bool_34;
            if (client.field916 != null && client.field922 < client.field916.size()) {
               while (true) {
                  if (client.field922 >= client.field916.size()) {
                     bool_34 = true;
                     break;
                  }

                  class67 class67_25 = (class67) client.field916.get(client.field922);
                  if (!class67_25.method1228(-1844563500)) {
                     bool_34 = false;
                     break;
                  }

                  ++client.field922;
               }
            } else {
               bool_34 = true;
            }

            if (bool_34 && class94.field1209 == -1L) {
               class94.field1209 = long_23;
               if (class94.field1209 > class94.field1208) {
                  class94.field1208 = class94.field1209;
               }
            }

            if (client.field653 == 10 || client.field653 == 11) {
               if (class199.field2382 == class49.field372) {
                  if (class63.field483 == 1 || !class161.field1989 && class63.field483 == 4) {
                     i_5 = class94.field1188 + 5;
                     short s_26 = 463;
                     byte b_7 = 100;
                     byte b_8 = 35;
                     if (class63.field489 >= i_5 && class63.field489 <= b_7 + i_5 && class63.field502 >= s_26 && class63.field502 <= s_26 + b_8) {
                        if (class16.method157(2125539222)) {
                           class94.field1198 = true;
                           class94.field1182 = 0;
                           class94.field1207 = 0;
                        }

                        return;
                     }
                  }

                  if (class80.field1026 != null && class16.method157(-2058065052)) {
                     class94.field1198 = true;
                     class94.field1182 = 0;
                     class94.field1207 = 0;
                  }
               }

               i_5 = class63.field483;
               int i_45 = class63.field489;
               int i_35 = class63.field502;
               if (i_5 == 0) {
                  i_45 = class63.field492;
                  i_35 = class63.field499;
               }

               if (!class161.field1989 && i_5 == 4) {
                  i_5 = 1;
               }

               short s_38;
               int i_39;
               if (class94.field1191 == 0) {
                  boolean bool_43 = false;

                  while (class20.method202(-369903170)) {
                     if (class241.field3135 == 84) {
                        bool_43 = true;
                     }
                  }

                  i_39 = class94.field1186 - 80;
                  s_38 = 291;
                  if (i_5 == 1 && i_45 >= i_39 - 75 && i_45 <= i_39 + 75 && i_35 >= s_38 - 20 && i_35 <= s_38 + 20) {
                     class93.method2109(class43.method632("secure", true, (byte) 11) + "m=account-creation/g=oldscape/create_account_funnel.ws", true, false, 262935584);
                  }

                  i_39 = class94.field1186 + 80;
                  if (i_5 == 1 && i_45 >= i_39 - 75 && i_45 <= i_39 + 75 && i_35 >= s_38 - 20 && i_35 <= s_38 + 20 || bool_43) {
                     if ((client.field646 & 0x2000000) != 0) {
                        class94.field1201 = "";
                        class94.field1193 = "This is a <col=00ffff>Beta<col=ffffff> world.";
                        class94.field1194 = "Your normal account will not be affected.";
                        class94.field1195 = "";
                        class94.field1191 = 1;
                        if (client.field684 && class94.field1196 != null && class94.field1196.length() > 0) {
                           class94.field1181 = 1;
                        } else {
                           class94.field1181 = 0;
                        }
                     } else if ((client.field646 & 0x4) != 0) {
                        if ((client.field646 & 0x400) != 0) {
                           class94.field1193 = "This is a <col=ffff00>High Risk <col=ff0000>PvP<col=ffffff> world.";
                           class94.field1194 = "Players can attack each other almost everywhere";
                           class94.field1195 = "and the Protect Item prayer won\'t work.";
                        } else {
                           class94.field1193 = "This is a <col=ff0000>PvP<col=ffffff> world.";
                           class94.field1194 = "Players can attack each other";
                           class94.field1195 = "almost everywhere.";
                        }

                        class94.field1201 = "Warning!";
                        class94.field1191 = 1;
                        client.method1771(-256549050);
                     } else if ((client.field646 & 0x400) != 0) {
                        class94.field1193 = "This is a <col=ffff00>High Risk<col=ffffff> world.";
                        class94.field1194 = "The Protect Item prayer will";
                        class94.field1195 = "not work on this world.";
                        class94.field1201 = "Warning!";
                        class94.field1191 = 1;
                        client.method1771(-256549050);
                     } else {
                        class43.method631(false, 339234172);
                     }
                  }
               } else {
                  short s_9;
                  int i_36;
                  if (class94.field1191 == 1) {
                     while (true) {
                        if (!class20.method202(-369903170)) {
                           i_36 = class94.field1186 - 80;
                           s_9 = 321;
                           if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                              class43.method631(false, 339234172);
                           }

                           i_36 = class94.field1186 + 80;
                           if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                              class94.field1191 = 0;
                           }
                           break;
                        }

                        if (class241.field3135 == 84) {
                           class43.method631(false, 339234172);
                        } else if (class241.field3135 == 13) {
                           class94.field1191 = 0;
                        }
                     }
                  } else {
                     int i_12;
                     short s_37;
                     boolean bool_40;
                     if (class94.field1191 == 2) {
                        s_37 = 201;
                        i_36 = s_37 + 52;
                        if (i_5 == 1 && i_35 >= i_36 - 12 && i_35 < i_36 + 2) {
                           class94.field1181 = 0;
                        }

                        i_36 += 15;
                        if (i_5 == 1 && i_35 >= i_36 - 12 && i_35 < i_36 + 2) {
                           class94.field1181 = 1;
                        }

                        i_36 += 15;
                        s_37 = 361;
                        if (class62.field481 != null) {
                           i_39 = class62.field481.field3879 / 2;
                           if (i_5 == 1 && i_45 >= class62.field481.field3881 - i_39 && i_45 <= i_39 + class62.field481.field3881 && i_35 >= s_37 - 15 && i_35 < s_37) {
                              switch(class94.field1190) {
                              case 1:
                                 class62.method1087("Please enter your username.", "If you created your account after November", "2010, this will be the creation email address.", -1191198372);
                                 class94.field1191 = 5;
                                 return;
                              case 2:
                                 class93.method2109("https://support.runescape.com/hc/en-gb", true, false, 134733249);
                              }
                           }
                        }

                        i_39 = class94.field1186 - 80;
                        s_38 = 321;
                        if (i_5 == 1 && i_45 >= i_39 - 75 && i_45 <= i_39 + 75 && i_35 >= s_38 - 20 && i_35 <= s_38 + 20) {
                           class94.field1196 = class94.field1196.trim();
                           if (class94.field1196.length() == 0) {
                              class62.method1087("", "Please enter your username/email address.", "", -1074001445);
                              return;
                           }

                           if (class94.field1197.length() == 0) {
                              class62.method1087("", "Please enter your password.", "", -1517138530);
                              return;
                           }

                           class62.method1087("", "Connecting to server...", "", -2068055303);
                           class34.method380(false, -1755335386);
                           class44.method663(20, -1771442088);
                           return;
                        }

                        i_39 = class94.field1185 + 180 + 80;
                        if (i_5 == 1 && i_45 >= i_39 - 75 && i_45 <= i_39 + 75 && i_35 >= s_38 - 20 && i_35 <= s_38 + 20) {
                           class94.field1191 = 0;
                           class94.field1196 = "";
                           class94.field1197 = "";
                           class42.field313 = 0;
                           class66.field528 = "";
                           class94.field1200 = true;
                        }

                        i_39 = class94.field1186 + -117;
                        s_38 = 277;
                        class94.field1192 = i_45 >= i_39 && i_45 < i_39 + class83.field1071 && i_35 >= s_38 && i_35 < s_38 + class98.field1260;
                        if (i_5 == 1 && class94.field1192) {
                           client.field684 = !client.field684;
                           if (!client.field684 && class282.field3617.field1050 != null) {
                              class282.field3617.field1050 = null;
                              class18.method187(1379602621);
                           }
                        }

                        i_39 = class94.field1186 + 24;
                        s_38 = 277;
                        class94.field1180 = i_45 >= i_39 && i_45 < i_39 + class83.field1071 && i_35 >= s_38 && i_35 < s_38 + class98.field1260;
                        if (i_5 == 1 && class94.field1180) {
                           class282.field3617.field1053 = !class282.field3617.field1053;
                           if (!class282.field3617.field1053) {
                              class94.field1196 = "";
                              class282.field3617.field1050 = null;
                              client.method1771(-256549050);
                           }

                           class18.method187(316209027);
                        }

                        while (true) {
                           int i_13;
                           Transferable transferable_28;
                           do {
                              while (true) {
                                 label1161:
                                 do {
                                    while (true) {
                                       while (class20.method202(-369903170)) {
                                          if (class241.field3135 != 13) {
                                             if (class94.field1181 != 0) {
                                                continue label1161;
                                             }

                                             char var_41 = class32.field200;

                                             for (i_12 = 0; i_12 < "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ".length() && var_41 != "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ".charAt(i_12); i_12++) {
                                                ;
                                             }

                                             if (class241.field3135 == 85 && class94.field1196.length() > 0) {
                                                class94.field1196 = class94.field1196.substring(0, class94.field1196.length() - 1);
                                             }

                                             if (class241.field3135 == 84 || class241.field3135 == 80) {
                                                class94.field1181 = 1;
                                             }

                                             if (class85.method1967(class32.field200, 636889033) && class94.field1196.length() < 320) {
                                                class94.field1196 = class94.field1196 + class32.field200;
                                             }
                                          } else {
                                             class94.field1191 = 0;
                                             class94.field1196 = "";
                                             class94.field1197 = "";
                                             class42.field313 = 0;
                                             class66.field528 = "";
                                             class94.field1200 = true;
                                          }
                                       }

                                       return;
                                    }
                                 } while (class94.field1181 != 1);

                                 if (class241.field3135 == 85 && class94.field1197.length() > 0) {
                                    class94.field1197 = class94.field1197.substring(0, class94.field1197.length() - 1);
                                 } else if (class241.field3135 == 84 || class241.field3135 == 80) {
                                    class94.field1181 = 0;
                                    if (class241.field3135 == 84) {
                                       class94.field1196 = class94.field1196.trim();
                                       if (class94.field1196.length() == 0) {
                                          class62.method1087("", "Please enter your username/email address.", "", -1130819310);
                                          return;
                                       }

                                       if (class94.field1197.length() == 0) {
                                          class62.method1087("", "Please enter your password.", "", -954370257);
                                          return;
                                       }

                                       class62.method1087("", "Connecting to server...", "", -768533232);
                                       class34.method380(false, 951148371);
                                       class44.method663(20, -1035129128);
                                       return;
                                    }
                                 }

                                 if ((class54.field416[82] || class54.field416[87]) && class241.field3135 == 67) {
                                    Clipboard clipboard_27 = Toolkit.getDefaultToolkit().getSystemClipboard();
                                    transferable_28 = clipboard_27.getContents(class27.field147);
                                    i_13 = 20 - class94.field1197.length();
                                    break;
                                 }

                                 char var_42 = class32.field200;
                                 if ((var_42 < 32 || var_42 >= 127) && (var_42 <= 127 || var_42 >= 160) && (var_42 <= 160 || var_42 > 255)) {
                                    label1364: {
                                       if (var_42 != 0) {
                                          char[] arr_29 = class298.field3688;

                                          for (int i_30 = 0; i_30 < arr_29.length; i_30++) {
                                             char var_15 = arr_29[i_30];
                                             if (var_15 == var_42) {
                                                bool_40 = true;
                                                break label1364;
                                             }
                                          }
                                       }

                                       bool_40 = false;
                                    }
                                 } else {
                                    bool_40 = true;
                                 }

                                 if (bool_40 && class85.method1967(class32.field200, 636889033) && class94.field1197.length() < 20) {
                                    class94.field1197 = class94.field1197 + class32.field200;
                                 }
                              }
                           } while (i_13 <= 0);

                           try {
                              String string_14 = (String) transferable_28.getTransferData(DataFlavor.stringFlavor);
                              int i_44 = Math.min(i_13, string_14.length());

                              for (int i_16 = 0; i_16 < i_44; i_16++) {
                                 char var_18 = string_14.charAt(i_16);
                                 boolean bool_17;
                                 if (var_18 >= 32 && var_18 < 127 || var_18 > 127 && var_18 < 160 || var_18 > 160 && var_18 <= 255) {
                                    bool_17 = true;
                                 } else {
                                    label1378: {
                                       if (var_18 != 0) {
                                          char[] arr_19 = class298.field3688;

                                          for (int i_20 = 0; i_20 < arr_19.length; i_20++) {
                                             char var_21 = arr_19[i_20];
                                             if (var_18 == var_21) {
                                                bool_17 = true;
                                                break label1378;
                                             }
                                          }
                                       }

                                       bool_17 = false;
                                    }
                                 }

                                 if (!bool_17 || !class85.method1967(string_14.charAt(i_16), 636889033)) {
                                    class94.field1191 = 3;
                                    return;
                                 }
                              }

                              class94.field1197 = class94.field1197 + string_14.substring(0, i_44);
                           } catch (UnsupportedFlavorException unsupportedflavorexception_32) {
                              ;
                           } catch (IOException ioexception_33) {
                              ;
                           }
                        }
                     } else if (class94.field1191 == 3) {
                        i_36 = class94.field1185 + 180;
                        s_9 = 276;
                        if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                           class43.method631(false, 339234172);
                        }

                        i_36 = class94.field1185 + 180;
                        s_9 = 326;
                        if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                           class62.method1087("Please enter your username.", "If you created your account after November", "2010, this will be the creation email address.", -1024924303);
                           class94.field1191 = 5;
                           return;
                        }
                     } else {
                        int i_11;
                        if (class94.field1191 == 4) {
                           i_36 = class94.field1185 + 180 - 80;
                           s_9 = 321;
                           if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                              class66.field528.trim();
                              if (class66.field528.length() != 6) {
                                 class62.method1087("", "Please enter a 6-digit PIN.", "", -744185764);
                                 return;
                              }

                              class42.field313 = Integer.parseInt(class66.field528);
                              class66.field528 = "";
                              class34.method380(true, 1662959699);
                              class62.method1087("", "Connecting to server...", "", -782039125);
                              class44.method663(20, -1610749960);
                              return;
                           }

                           if (i_5 == 1 && i_45 >= class94.field1185 + 180 - 9 && i_45 <= class94.field1185 + 180 + 130 && i_35 >= 263 && i_35 <= 296) {
                              class94.field1200 = !class94.field1200;
                           }

                           if (i_5 == 1 && i_45 >= class94.field1185 + 180 - 34 && i_45 <= class94.field1185 + 34 + 180 && i_35 >= 351 && i_35 <= 363) {
                              class93.method2109(class43.method632("secure", true, (byte) 12) + "m=totp-authenticator/disableTOTPRequest", true, false, 132666900);
                           }

                           i_36 = class94.field1185 + 180 + 80;
                           if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                              class94.field1191 = 0;
                              class94.field1196 = "";
                              class94.field1197 = "";
                              class42.field313 = 0;
                              class66.field528 = "";
                           }

                           while (class20.method202(-369903170)) {
                              boolean bool_10 = false;

                              for (i_11 = 0; i_11 < "1234567890".length(); i_11++) {
                                 if (class32.field200 == "1234567890".charAt(i_11)) {
                                    bool_10 = true;
                                    break;
                                 }
                              }

                              if (class241.field3135 == 13) {
                                 class94.field1191 = 0;
                                 class94.field1196 = "";
                                 class94.field1197 = "";
                                 class42.field313 = 0;
                                 class66.field528 = "";
                              } else {
                                 if (class241.field3135 == 85 && class66.field528.length() > 0) {
                                    class66.field528 = class66.field528.substring(0, class66.field528.length() - 1);
                                 }

                                 if (class241.field3135 == 84) {
                                    class66.field528.trim();
                                    if (class66.field528.length() != 6) {
                                       class62.method1087("", "Please enter a 6-digit PIN.", "", -1859979992);
                                       return;
                                    }

                                    class42.field313 = Integer.parseInt(class66.field528);
                                    class66.field528 = "";
                                    class34.method380(true, -1934068850);
                                    class62.method1087("", "Connecting to server...", "", -2074496141);
                                    class44.method663(20, -1092788430);
                                    return;
                                 }

                                 if (bool_10 && class66.field528.length() < 6) {
                                    class66.field528 = class66.field528 + class32.field200;
                                 }
                              }
                           }
                        } else if (class94.field1191 == 5) {
                           i_36 = class94.field1185 + 180 - 80;
                           s_9 = 321;
                           if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                              class7.method83(-249600986);
                              return;
                           }

                           i_36 = class94.field1185 + 180 + 80;
                           if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                              class43.method631(true, 339234172);
                           }

                           s_38 = 361;
                           if (class76.field643 != null) {
                              i_11 = class76.field643.field3879 / 2;
                              if (i_5 == 1 && i_45 >= class76.field643.field3881 - i_11 && i_45 <= i_11 + class76.field643.field3881 && i_35 >= s_38 - 15 && i_35 < s_38) {
                                 class93.method2109(class43.method632("secure", true, (byte) 56) + "m=weblogin/g=oldscape/cant_log_in", true, false, 317896520);
                              }
                           }

                           while (class20.method202(-369903170)) {
                              bool_40 = false;

                              for (i_12 = 0; i_12 < "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ".length(); i_12++) {
                                 if (class32.field200 == "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ".charAt(i_12)) {
                                    bool_40 = true;
                                    break;
                                 }
                              }

                              if (class241.field3135 == 13) {
                                 class43.method631(true, 339234172);
                              } else {
                                 if (class241.field3135 == 85 && class94.field1196.length() > 0) {
                                    class94.field1196 = class94.field1196.substring(0, class94.field1196.length() - 1);
                                 }

                                 if (class241.field3135 == 84) {
                                    class7.method83(-249600986);
                                    return;
                                 }

                                 if (bool_40 && class94.field1196.length() < 320) {
                                    class94.field1196 = class94.field1196 + class32.field200;
                                 }
                              }
                           }
                        } else if (class94.field1191 != 6) {
                           if (class94.field1191 == 7) {
                              i_36 = class94.field1185 + 180 - 80;
                              s_9 = 321;
                              if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                                 class93.method2109(class43.method632("secure", true, (byte) 32) + "m=dob/set_dob.ws", true, false, 573111679);
                                 class62.method1087("", "Page has opened in a new window.", "(Please check your popup blocker.)", -1633698345);
                                 class94.field1191 = 6;
                                 return;
                              }

                              i_36 = class94.field1185 + 180 + 80;
                              if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                                 class43.method631(true, 339234172);
                              }
                           } else if (class94.field1191 == 8) {
                              i_36 = class94.field1185 + 180 - 80;
                              s_9 = 321;
                              if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                                 class93.method2109("https://www.jagex.com/terms/privacy", true, false, 777634859);
                                 class62.method1087("", "Page has opened in a new window.", "(Please check your popup blocker.)", -2066118378);
                                 class94.field1191 = 6;
                                 return;
                              }

                              i_36 = class94.field1185 + 180 + 80;
                              if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                                 class43.method631(true, 339234172);
                              }
                           } else if (class94.field1191 == 12) {
                              String str_31 = "";
                              switch(class94.field1199) {
                              case 0:
                                 str_31 = "https://support.runescape.com/hc/en-gb/articles/115002238729-Account-Bans";
                                 break;
                              case 1:
                                 str_31 = "https://support.runescape.com/hc/en-gb/articles/206103939-My-account-is-locked";
                                 break;
                              default:
                                 class43.method631(false, 339234172);
                              }

                              i_39 = class94.field1185 + 180;
                              s_38 = 276;
                              if (i_5 == 1 && i_45 >= i_39 - 75 && i_45 <= i_39 + 75 && i_35 >= s_38 - 20 && i_35 <= s_38 + 20) {
                                 class93.method2109(str_31, true, false, 1094585498);
                                 class62.method1087("", "Page has opened in a new window.", "(Please check your popup blocker.)", -1355761930);
                                 class94.field1191 = 6;
                                 return;
                              }

                              i_39 = class94.field1185 + 180;
                              s_38 = 326;
                              if (i_5 == 1 && i_45 >= i_39 - 75 && i_45 <= i_39 + 75 && i_35 >= s_38 - 20 && i_35 <= s_38 + 20) {
                                 class43.method631(false, 339234172);
                              }
                           } else if (class94.field1191 == 24) {
                              i_36 = class94.field1185 + 180;
                              s_9 = 301;
                              if (i_5 == 1 && i_45 >= i_36 - 75 && i_45 <= i_36 + 75 && i_35 >= s_9 - 20 && i_35 <= s_9 + 20) {
                                 class43.method631(false, 339234172);
                              }
                           }
                        } else {
                           while (true) {
                              do {
                                 if (!class20.method202(-369903170)) {
                                    s_37 = 321;
                                    if (i_5 == 1 && i_35 >= s_37 - 20 && i_35 <= s_37 + 20) {
                                       class43.method631(true, 339234172);
                                    }

                                    return;
                                 }
                              } while (class241.field3135 != 84 && class241.field3135 != 13);

                              class43.method631(true, 339234172);
                           }
                        }
                     }
                  }
               }

            }
         }
      }
   }

   static final void method4764(class226 class226_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (client.field692) {
         client.field720 = 32;
      } else {
         client.field720 = 0;
      }

      client.field692 = false;
      int i_8;
      if (class63.field491 == 1 || !class161.field1989 && class63.field491 == 4) {
         if (i_5 >= i_1 && i_5 < i_1 + 16 && i_6 >= i_2 && i_6 < i_2 + 16) {
            class226_0.field2608 -= 4;
            class181.method3609(class226_0, 632329710);
         } else if (i_5 >= i_1 && i_5 < i_1 + 16 && i_6 >= i_3 + i_2 - 16 && i_6 < i_3 + i_2) {
            class226_0.field2608 += 4;
            class181.method3609(class226_0, -313266691);
         } else if (i_5 >= i_1 - client.field720 && i_5 < client.field720 + i_1 + 16 && i_6 >= i_2 + 16 && i_6 < i_3 + i_2 - 16) {
            i_8 = i_3 * (i_3 - 32) / i_4;
            if (i_8 < 8) {
               i_8 = 8;
            }

            int i_9 = i_6 - i_2 - 16 - i_8 / 2;
            int i_10 = i_3 - 32 - i_8;
            class226_0.field2608 = i_9 * (i_4 - i_3) / i_10;
            class181.method3609(class226_0, -808545962);
            client.field692 = true;
         }
      }

      if (client.field846 != 0) {
         i_8 = class226_0.field2601;
         if (i_5 >= i_1 - i_8 && i_6 >= i_2 && i_5 < i_1 + 16 && i_6 <= i_3 + i_2) {
            class226_0.field2608 += client.field846 * 45;
            class181.method3609(class226_0, -2122908437);
         }
      }

   }

}
