public class class173 extends class179 {

   public static class83 field2036;
   long[] field2030 = new long[10];
   int field2031 = 256;
   int field2032 = 1;
   int field2034 = 0;
   long field2038 = class298.method5270(255749540);
   int field2035;

   public class173() {
      for (int i_1 = 0; i_1 < 10; i_1++) {
         this.field2030[i_1] = this.field2038;
      }

   }

   public int vmethod3547(int i_1, int i_2, int i_3) {
      int i_4 = this.field2031;
      int i_5 = this.field2032;
      this.field2031 = 300;
      this.field2032 = 1;
      this.field2038 = class298.method5270(255749540);
      if (this.field2030[this.field2035] == 0L) {
         this.field2031 = i_4;
         this.field2032 = i_5;
      } else if (this.field2038 > this.field2030[this.field2035]) {
         this.field2031 = (int)((long)(i_1 * 2560) / (this.field2038 - this.field2030[this.field2035]));
      }

      if (this.field2031 < 25) {
         this.field2031 = 25;
      }

      if (this.field2031 > 256) {
         this.field2031 = 256;
         this.field2032 = (int)((long)i_1 - (this.field2038 - this.field2030[this.field2035]) / 10L);
      }

      if (this.field2032 > i_1) {
         this.field2032 = i_1;
      }

      this.field2030[this.field2035] = this.field2038;
      this.field2035 = (this.field2035 + 1) % 10;
      int i_6;
      if (this.field2032 > 1) {
         for (i_6 = 0; i_6 < 10; i_6++) {
            if (this.field2030[i_6] != 0L) {
               this.field2030[i_6] += (long)this.field2032;
            }
         }
      }

      if (this.field2032 < i_2) {
         this.field2032 = i_2;
      }

      class236.method4132((long)this.field2032);

      for (i_6 = 0; this.field2034 < 256; this.field2034 += this.field2031) {
         ++i_6;
      }

      this.field2034 &= 0xff;
      return i_6;
   }

   public void vmethod3550(int i_1) {
      for (int i_2 = 0; i_2 < 10; i_2++) {
         this.field2030[i_2] = 0L;
      }

   }

}
