public final class class95 extends class144 {

   boolean field1223 = false;
   int field1235 = 0;
   int field1236 = 0;
   int field1212;
   int field1225;
   int field1214;
   int field1215;
   int field1216;
   int field1218;
   int field1234;
   int field1226;
   int field1221;
   int field1231;
   int field1227;
   class269 field1213;
   double field1224;
   double field1217;
   double field1232;
   double field1230;
   int field1222;
   int field1233;
   double field1220;
   double field1228;
   double field1229;
   double field1219;

   class95(int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8, int i_9, int i_10, int i_11) {
      this.field1212 = i_1;
      this.field1225 = i_2;
      this.field1214 = i_3;
      this.field1215 = i_4;
      this.field1216 = i_5;
      this.field1218 = i_6;
      this.field1234 = i_7;
      this.field1226 = i_8;
      this.field1221 = i_9;
      this.field1231 = i_10;
      this.field1227 = i_11;
      this.field1223 = false;
      int i_12 = class96.method2160(this.field1212, (byte) 59).field3284;
      if (i_12 != -1) {
         this.field1213 = class260.method4510(i_12, -2034264900);
      } else {
         this.field1213 = null;
      }

   }

   protected final class136 vmethod3305(int i_1) {
      class254 class254_2 = class96.method2160(this.field1212, (byte) 61);
      class136 class136_3 = class254_2.method4411(this.field1235, -1209345232);
      if (class136_3 == null) {
         return null;
      } else {
         class136_3.method2935(this.field1233);
         return class136_3;
      }
   }

   final void method2149(int i_1, byte b_2) {
      this.field1223 = true;
      this.field1224 += (double)i_1 * this.field1220;
      this.field1217 += this.field1228 * (double)i_1;
      this.field1232 += (double)i_1 * (double)i_1 * 0.5D * this.field1219 + (double)i_1 * this.field1230;
      this.field1230 += (double)i_1 * this.field1219;
      this.field1222 = (int)(Math.atan2(this.field1220, this.field1228) * 325.949D) + 1024 & 0x7ff;
      this.field1233 = (int)(Math.atan2(this.field1230, this.field1229) * 325.949D) & 0x7ff;
      if (this.field1213 != null) {
         this.field1236 += i_1;

         while (true) {
            do {
               do {
                  if (this.field1236 <= this.field1213.field3552[this.field1235]) {
                     return;
                  }

                  this.field1236 -= this.field1213.field3552[this.field1235];
                  ++this.field1235;
               } while (this.field1235 < this.field1213.field3550.length);

               this.field1235 -= this.field1213.field3558;
            } while (this.field1235 >= 0 && this.field1235 < this.field1213.field3550.length);

            this.field1235 = 0;
         }
      }
   }

   final void method2147(int i_1, int i_2, int i_3, int i_4, int i_5) {
      double d_6;
      if (!this.field1223) {
         d_6 = (double)(i_1 - this.field1214);
         double d_8 = (double)(i_2 - this.field1215);
         double d_10 = Math.sqrt(d_6 * d_6 + d_8 * d_8);
         this.field1224 = (double)this.field1214 + d_6 * (double)this.field1221 / d_10;
         this.field1217 = (double)this.field1215 + (double)this.field1221 * d_8 / d_10;
         this.field1232 = (double)this.field1216;
      }

      d_6 = (double)(this.field1234 + 1 - i_4);
      this.field1220 = ((double)i_1 - this.field1224) / d_6;
      this.field1228 = ((double)i_2 - this.field1217) / d_6;
      this.field1229 = Math.sqrt(this.field1220 * this.field1220 + this.field1228 * this.field1228);
      if (!this.field1223) {
         this.field1230 = -this.field1229 * Math.tan((double)this.field1226 * 0.02454369D);
      }

      this.field1219 = 2.0D * ((double)i_3 - this.field1232 - d_6 * this.field1230) / (d_6 * d_6);
   }

   public static byte[] method2143(CharSequence charsequence_0, int i_1) {
      int i_2 = charsequence_0.length();
      byte[] bytes_3 = new byte[i_2];

      for (int i_4 = 0; i_4 < i_2; i_4++) {
         char var_5 = charsequence_0.charAt(i_4);
         if (var_5 > 0 && var_5 < 128 || var_5 >= 160 && var_5 <= 255) {
            bytes_3[i_4] = (byte)var_5;
         } else if (var_5 == 8364) {
            bytes_3[i_4] = -128;
         } else if (var_5 == 8218) {
            bytes_3[i_4] = -126;
         } else if (var_5 == 402) {
            bytes_3[i_4] = -125;
         } else if (var_5 == 8222) {
            bytes_3[i_4] = -124;
         } else if (var_5 == 8230) {
            bytes_3[i_4] = -123;
         } else if (var_5 == 8224) {
            bytes_3[i_4] = -122;
         } else if (var_5 == 8225) {
            bytes_3[i_4] = -121;
         } else if (var_5 == 710) {
            bytes_3[i_4] = -120;
         } else if (var_5 == 8240) {
            bytes_3[i_4] = -119;
         } else if (var_5 == 352) {
            bytes_3[i_4] = -118;
         } else if (var_5 == 8249) {
            bytes_3[i_4] = -117;
         } else if (var_5 == 338) {
            bytes_3[i_4] = -116;
         } else if (var_5 == 381) {
            bytes_3[i_4] = -114;
         } else if (var_5 == 8216) {
            bytes_3[i_4] = -111;
         } else if (var_5 == 8217) {
            bytes_3[i_4] = -110;
         } else if (var_5 == 8220) {
            bytes_3[i_4] = -109;
         } else if (var_5 == 8221) {
            bytes_3[i_4] = -108;
         } else if (var_5 == 8226) {
            bytes_3[i_4] = -107;
         } else if (var_5 == 8211) {
            bytes_3[i_4] = -106;
         } else if (var_5 == 8212) {
            bytes_3[i_4] = -105;
         } else if (var_5 == 732) {
            bytes_3[i_4] = -104;
         } else if (var_5 == 8482) {
            bytes_3[i_4] = -103;
         } else if (var_5 == 353) {
            bytes_3[i_4] = -102;
         } else if (var_5 == 8250) {
            bytes_3[i_4] = -101;
         } else if (var_5 == 339) {
            bytes_3[i_4] = -100;
         } else if (var_5 == 382) {
            bytes_3[i_4] = -98;
         } else if (var_5 == 376) {
            bytes_3[i_4] = -97;
         } else {
            bytes_3[i_4] = 63;
         }
      }

      return bytes_3;
   }

}
