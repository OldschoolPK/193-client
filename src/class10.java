public class class10 {

   final int field47;
   final int field48;
   final String field49;

   class10(class310 class310_1) {
      this(class310_1.method5661((byte) 100), class310_1.method5661((byte) 13), class310_1.method5589(1398193536));
   }

   class10(int i_1, int i_2, String string_3) {
      this.field47 = i_1;
      this.field48 = i_2;
      this.field49 = string_3;
   }

   int method114() {
      return this.field48;
   }

   String method121() {
      return Integer.toHexString(this.field47) + Integer.toHexString(this.field48) + this.field49;
   }

}
