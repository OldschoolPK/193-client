public class class101 extends class184 {

   static class50 field1302;
   int[] field1300;
   int[] field1294;
   int field1296;
   int field1297;
   String[] field1295;
   int field1298;
   int field1292;
   class326[] field1305;
   static class154 field1299 = new class154(128);

   class326[] method2259(int i_1, int i_2) {
      return new class326[i_1];
   }

   static Class method2260(String string_0, int i_1) throws ClassNotFoundException {
      return string_0.equals("B") ? Byte.TYPE : (string_0.equals("I") ? Integer.TYPE : (string_0.equals("S") ? Short.TYPE : (string_0.equals("J") ? Long.TYPE : (string_0.equals("Z") ? Boolean.TYPE : (string_0.equals("F") ? Float.TYPE : (string_0.equals("D") ? Double.TYPE : (string_0.equals("C") ? Character.TYPE : (string_0.equals("void") ? Void.TYPE : Class.forName(string_0)))))))));
   }

   static final void method2271(int i_0, int i_1) {
      class195.method3652(917549695);

      for (class84 class84_2 = (class84) class84.field1075.method4879(); class84_2 != null; class84_2 = (class84) class84.field1075.method4884()) {
         if (class84_2.field1087 != null) {
            class84_2.method1959(-315645588);
         }
      }

      int i_5 = class219.method3976(i_0, -117853859).field3238;
      if (i_5 != 0) {
         int i_3 = class221.field2541[i_0];
         if (i_5 == 1) {
            if (i_3 == 1) {
               class42.method626(0.9D);
            }

            if (i_3 == 2) {
               class42.method626(0.8D);
            }

            if (i_3 == 3) {
               class42.method626(0.7D);
            }

            if (i_3 == 4) {
               class42.method626(0.6D);
            }
         }

         if (i_5 == 3) {
            if (i_3 == 0) {
               class50.method810(255, 1393718801);
            }

            if (i_3 == 1) {
               class50.method810(192, 1393718801);
            }

            if (i_3 == 2) {
               class50.method810(128, 1393718801);
            }

            if (i_3 == 3) {
               class50.method810(64, 1393718801);
            }

            if (i_3 == 4) {
               class50.method810(0, 1393718801);
            }
         }

         if (i_5 == 4) {
            if (i_3 == 0) {
               class44.method662(127, 1105794074);
            }

            if (i_3 == 1) {
               class44.method662(96, -1457389641);
            }

            if (i_3 == 2) {
               class44.method662(64, -1079285981);
            }

            if (i_3 == 3) {
               class44.method662(32, -1207050231);
            }

            if (i_3 == 4) {
               class44.method662(0, 745425227);
            }
         }

         if (i_5 == 5) {
            client.field882 = i_3;
         }

         if (i_5 == 6) {
            client.field812 = i_3;
         }

         if (i_5 == 9) {
            client.field865 = i_3;
         }

         if (i_5 == 10) {
            if (i_3 == 0) {
               class194.method3647(127, -1005104193);
            }

            if (i_3 == 1) {
               class194.method3647(96, -1005104193);
            }

            if (i_3 == 2) {
               class194.method3647(64, -1005104193);
            }

            if (i_3 == 3) {
               class194.method3647(32, -1005104193);
            }

            if (i_3 == 4) {
               class194.method3647(0, -1005104193);
            }
         }

         if (i_5 == 17) {
            client.field818 = i_3 & 0xffff;
         }

         class93[] arr_4;
         if (i_5 == 18) {
            arr_4 = new class93[] {class93.field1177, class93.field1176, class93.field1175, class93.field1179};
            client.field671 = (class93) class152.method3363(arr_4, i_3, 867235442);
            if (client.field671 == null) {
               client.field671 = class93.field1176;
            }
         }

         if (i_5 == 19) {
            if (i_3 == -1) {
               client.field814 = -1;
            } else {
               client.field814 = i_3 & 0x7ff;
            }
         }

         if (i_5 == 22) {
            arr_4 = new class93[] {class93.field1177, class93.field1176, class93.field1175, class93.field1179};
            client.field672 = (class93) class152.method3363(arr_4, i_3, 431119166);
            if (client.field672 == null) {
               client.field672 = class93.field1176;
            }
         }

      }
   }

}
