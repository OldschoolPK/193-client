public class class167 extends class285 {

   static class246 field2010;
   final boolean field2009;

   public class167(boolean bool_1) {
      this.field2009 = bool_1;
   }

   int method3490(class289 class289_1, class289 class289_2, int i_3) {
      return class289_2.field3651 != class289_1.field3651 ? (this.field2009 ? class289_1.field3651 - class289_2.field3651 : class289_2.field3651 - class289_1.field3651) : this.method5051(class289_1, class289_2, 1504560686);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3490((class289) object_1, (class289) object_2, 1171495407);
   }

   static void method3494(int i_0, int i_1, short s_2) {
      int i_3 = class17.field74.method5340("Choose Option");

      int i_4;
      int i_5;
      for (i_4 = 0; i_4 < client.field785; i_4++) {
         i_5 = class17.field74.method5340(class67.method1230(i_4, 636889033));
         if (i_5 > i_3) {
            i_3 = i_5;
         }
      }

      i_3 += 8;
      i_4 = client.field785 * 15 + 22;
      i_5 = i_0 - i_3 / 2;
      if (i_3 + i_5 > class286.field3638) {
         i_5 = class286.field3638 - i_3;
      }

      if (i_5 < 0) {
         i_5 = 0;
      }

      int i_6 = i_1;
      if (i_4 + i_1 > class143.field1892) {
         i_6 = class143.field1892 - i_4;
      }

      if (i_6 < 0) {
         i_6 = 0;
      }

      class285.field3631 = i_5;
      class102.field1315 = i_6;
      class149.field1928 = i_3;
      class182.field2120 = client.field785 * 15 + 22;
   }

}
