public class class230 {

   public static final boolean[] field2769 = new boolean[] {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false};
   public static int[] field2768 = new int[99];

   static {
      int i_0 = 0;

      for (int i_1 = 0; i_1 < 99; i_1++) {
         int i_2 = i_1 + 1;
         int i_3 = (int)((double)i_2 + 300.0D * Math.pow(2.0D, (double)i_2 / 7.0D));
         i_0 += i_3;
         field2768[i_1] = i_0 / 4;
      }

   }

   public static class335 method4115(class244 class244_0, int i_1, int i_2, byte b_3) {
      if (!class304.method5323(class244_0, i_1, i_2, 975575420)) {
         return null;
      } else {
         class335 class335_5 = new class335();
         class335_5.field3916 = class215.field2511;
         class335_5.field3915 = class336.field3929;
         class335_5.field3918 = class336.field3925[0];
         class335_5.field3912 = class336.field3926[0];
         class335_5.field3913 = class336.field3927[0];
         class335_5.field3911 = class336.field3928[0];
         int i_6 = class335_5.field3913 * class335_5.field3911;
         byte[] bytes_7 = class15.field62[0];
         class335_5.field3917 = new int[i_6];

         for (int i_8 = 0; i_8 < i_6; i_8++) {
            class335_5.field3917[i_8] = class336.field3924[bytes_7[i_8] & 0xff];
         }

         class336.field3925 = null;
         class336.field3926 = null;
         class336.field3927 = null;
         class336.field3928 = null;
         class336.field3924 = null;
         class15.field62 = null;
         return class335_5;
      }
   }

   static void method4114(int i_0, class318 class318_1, class246 class246_2, int i_3) {
      class242 class242_4 = new class242();
      class242_4.field3149 = 1;
      class242_4.field2137 = (long)i_0;
      class242_4.field3146 = class318_1;
      class242_4.field3147 = class246_2;
      class272 class272_5 = class245.field3177;
      synchronized(class245.field3177) {
         class245.field3177.method4888(class242_4);
      }

      Object object_10 = class245.field3180;
      synchronized(class245.field3180) {
         if (class245.field3178 == 0) {
            class97.field1247 = new Thread(new class245());
            class97.field1247.setDaemon(true);
            class97.field1247.start();
            class97.field1247.setPriority(5);
         }

         class245.field3178 = 600;
      }
   }

   static int method4113(class309 class309_0, int i_1) {
      int i_2 = class309_0.method5455(2, (byte) -114);
      int i_3;
      if (i_2 == 0) {
         i_3 = 0;
      } else if (i_2 == 1) {
         i_3 = class309_0.method5455(5, (byte) -71);
      } else if (i_2 == 2) {
         i_3 = class309_0.method5455(8, (byte) -53);
      } else {
         i_3 = class309_0.method5455(11, (byte) -18);
      }

      return i_3;
   }

}
