public class class39 {

   public static int field288;
   static class334[] field282;
   static final class39 field278 = new class39(0);
   static final class39 field277 = new class39(1);
   final int field280;

   class39(int i_1) {
      this.field280 = i_1;
   }

   static final void method596(short s_0) {
      class142.field1839 = false;
      client.field910 = false;
   }

   public static String method591(byte[] bytes_0, int i_1, int i_2, int i_3) {
      char[] arr_4 = new char[i_2];
      int i_5 = 0;
      int i_6 = i_1;

      int i_9;
      for (int i_7 = i_2 + i_1; i_6 < i_7; arr_4[i_5++] = (char)i_9) {
         int i_8 = bytes_0[i_6++] & 0xff;
         if (i_8 < 128) {
            if (i_8 == 0) {
               i_9 = 65533;
            } else {
               i_9 = i_8;
            }
         } else if (i_8 < 192) {
            i_9 = 65533;
         } else if (i_8 < 224) {
            if (i_6 < i_7 && (bytes_0[i_6] & 0xc0) == 128) {
               i_9 = (i_8 & 0x1f) << 6 | bytes_0[i_6++] & 0x3f;
               if (i_9 < 128) {
                  i_9 = 65533;
               }
            } else {
               i_9 = 65533;
            }
         } else if (i_8 < 240) {
            if (i_6 + 1 < i_7 && (bytes_0[i_6] & 0xc0) == 128 && (bytes_0[i_6 + 1] & 0xc0) == 128) {
               i_9 = (i_8 & 0xf) << 12 | (bytes_0[i_6++] & 0x3f) << 6 | bytes_0[i_6++] & 0x3f;
               if (i_9 < 2048) {
                  i_9 = 65533;
               }
            } else {
               i_9 = 65533;
            }
         } else if (i_8 < 248) {
            if (i_6 + 2 < i_7 && (bytes_0[i_6] & 0xc0) == 128 && (bytes_0[i_6 + 1] & 0xc0) == 128 && (bytes_0[i_6 + 2] & 0xc0) == 128) {
               i_9 = (i_8 & 0x7) << 18 | (bytes_0[i_6++] & 0x3f) << 12 | (bytes_0[i_6++] & 0x3f) << 6 | bytes_0[i_6++] & 0x3f;
               if (i_9 >= 65536 && i_9 <= 1114111) {
                  i_9 = 65533;
               } else {
                  i_9 = 65533;
               }
            } else {
               i_9 = 65533;
            }
         } else {
            i_9 = 65533;
         }
      }

      return new String(arr_4, 0, i_5);
   }

   public static boolean method592(char var_0, int i_1) {
      return var_0 >= 48 && var_0 <= 57;
   }

   static final void method594(int i_0, int i_1, int i_2, int i_3, int i_4) {
      for (int i_5 = i_1; i_5 <= i_3 + i_1; i_5++) {
         for (int i_6 = i_0; i_6 <= i_0 + i_2; i_6++) {
            if (i_6 >= 0 && i_6 < 104 && i_5 >= 0 && i_5 < 104) {
               class64.field511[0][i_6][i_5] = 127;
               if (i_0 == i_6 && i_6 > 0) {
                  class64.field519[0][i_6][i_5] = class64.field519[0][i_6 - 1][i_5];
               }

               if (i_6 == i_0 + i_2 && i_6 < 103) {
                  class64.field519[0][i_6][i_5] = class64.field519[0][i_6 + 1][i_5];
               }

               if (i_5 == i_1 && i_5 > 0) {
                  class64.field519[0][i_6][i_5] = class64.field519[0][i_6][i_5 - 1];
               }

               if (i_3 + i_1 == i_5 && i_5 < 103) {
                  class64.field519[0][i_6][i_5] = class64.field519[0][i_6][i_5 + 1];
               }
            }
         }
      }

   }

   static void method593(int i_0, int i_1, int i_2) {
      int[] ints_3 = new int[9];

      for (int i_4 = 0; i_4 < ints_3.length; i_4++) {
         int i_5 = i_4 * 32 + 15 + 128;
         int i_6 = class45.method704(i_5, 1776281745);
         int i_7 = class139.field1797[i_5];
         i_6 = class180.method3559(i_6, i_1, (byte) -84);
         ints_3[i_4] = i_7 * i_6 >> 16;
      }

      class142.method3133(ints_3, 500, 800, i_0 * 334 / i_1, 334);
   }

   static int method595(int i_0, int i_1) {
      class73 class73_2 = (class73) class100.field1285.method5919((long)i_0);
      return class73_2 == null ? -1 : (class73_2.field2128 == class100.field1284.field3579 ? -1 : ((class73) class73_2.field2128).field598);
   }

}
