import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;

public class class28 implements class46 {

   int field161;
   int field157;
   int field153;
   int field158;
   int field160;
   int field155;
   int field159;
   int field163;
   int field156;
   int field162;
   int field164;
   int field154;
   int field152;
   int field165;

   public boolean vmethod814(int i_1, int i_2, int i_3) {
      return i_1 >= (this.field156 << 6) + (this.field162 << 3) && i_1 <= (this.field156 << 6) + (this.field164 << 3) + 7 && i_2 >= (this.field154 << 6) + (this.field152 << 3) && i_2 <= (this.field154 << 6) + (this.field165 << 3) + 7;
   }

   public boolean vmethod813(int i_1, int i_2, int i_3, int i_4) {
      return i_1 >= this.field161 && i_1 < this.field157 + this.field161 ? i_2 >= (this.field153 << 6) + (this.field158 << 3) && i_2 <= (this.field153 << 6) + (this.field160 << 3) + 7 && i_3 >= (this.field155 << 6) + (this.field159 << 3) && i_3 <= (this.field155 << 6) + (this.field163 << 3) + 7 : false;
   }

   public void vmethod812(class35 class35_1, short s_2) {
      if (class35_1.field234 > this.field156) {
         class35_1.field234 = this.field156;
      }

      if (class35_1.field228 < this.field156) {
         class35_1.field228 = this.field156;
      }

      if (class35_1.field236 > this.field154) {
         class35_1.field236 = this.field154;
      }

      if (class35_1.field239 < this.field154) {
         class35_1.field239 = this.field154;
      }

   }

   public class222 vmethod835(int i_1, int i_2, byte b_3) {
      if (!this.vmethod814(i_1, i_2, 639074408)) {
         return null;
      } else {
         int i_4 = this.field153 * 64 - this.field156 * 64 + (this.field158 * 8 - this.field162 * 8) + i_1;
         int i_5 = this.field155 * 64 - this.field154 * 64 + i_2 + (this.field159 * 8 - this.field152 * 8);
         return new class222(this.field161, i_4, i_5);
      }
   }

   public int[] vmethod819(int i_1, int i_2, int i_3, int i_4) {
      if (!this.vmethod813(i_1, i_2, i_3, -1481972223)) {
         return null;
      } else {
         int[] ints_5 = new int[] {this.field156 * 64 - this.field153 * 64 + i_2 + (this.field162 * 8 - this.field158 * 8), i_3 + (this.field154 * 64 - this.field155 * 64) + (this.field152 * 8 - this.field159 * 8)};
         return ints_5;
      }
   }

   public void vmethod825(class310 class310_1, byte b_2) {
      this.field161 = class310_1.method5661((byte) -78);
      this.field157 = class310_1.method5661((byte) 19);
      this.field153 = class310_1.method5729(1644948922);
      this.field158 = class310_1.method5661((byte) 69);
      this.field160 = class310_1.method5661((byte) 12);
      this.field155 = class310_1.method5729(1686169437);
      this.field159 = class310_1.method5661((byte) -19);
      this.field163 = class310_1.method5661((byte) 65);
      this.field156 = class310_1.method5729(1825025963);
      this.field162 = class310_1.method5661((byte) 26);
      this.field164 = class310_1.method5661((byte) -67);
      this.field154 = class310_1.method5729(2069347665);
      this.field152 = class310_1.method5661((byte) 11);
      this.field165 = class310_1.method5661((byte) -17);
      this.method292(-2143649574);
   }

   void method292(int i_1) {
   }

   public static final class335 method285(byte[] bytes_0, int i_1) {
      BufferedImage bufferedimage_2 = null;

      try {
         bufferedimage_2 = ImageIO.read(new ByteArrayInputStream(bytes_0));
         int i_3 = bufferedimage_2.getWidth();
         int i_4 = bufferedimage_2.getHeight();
         int[] ints_5 = new int[i_3 * i_4];
         PixelGrabber pixelgrabber_6 = new PixelGrabber(bufferedimage_2, 0, 0, i_3, i_4, ints_5, 0, i_3);
         pixelgrabber_6.grabPixels();
         return new class335(ints_5, i_3, i_4);
      } catch (IOException ioexception_8) {
         ;
      } catch (InterruptedException interruptedexception_9) {
         ;
      }

      return new class335(0, 0);
   }

   static boolean method286(class309 class309_0, int i_1, int i_2) {
      int i_3 = class309_0.method5455(2, (byte) -58);
      int i_4;
      int i_5;
      int i_8;
      int i_9;
      int i_10;
      int i_11;
      if (i_3 == 0) {
         if (class309_0.method5455(1, (byte) -60) != 0) {
            method286(class309_0, i_1, 1197361247);
         }

         i_4 = class309_0.method5455(13, (byte) -53);
         i_5 = class309_0.method5455(13, (byte) -25);
         boolean bool_13 = class309_0.method5455(1, (byte) -3) == 1;
         if (bool_13) {
            class98.field1265[++class98.field1262 - 1] = i_1;
         }

         if (client.field764[i_1] != null) {
            throw new RuntimeException();
         } else {
            class75 class75_7 = client.field764[i_1] = new class75();
            class75_7.field628 = i_1;
            if (class98.field1254[i_1] != null) {
               class75_7.method1334(class98.field1254[i_1], (byte) 9);
            }

            class75_7.field989 = class98.field1269[i_1];
            class75_7.field980 = class98.field1261[i_1];
            i_8 = class98.field1259[i_1];
            i_9 = i_8 >> 28;
            i_10 = i_8 >> 14 & 0xff;
            i_11 = i_8 & 0xff;
            class75_7.field995[0] = class98.field1253[i_1];
            class75_7.field627 = (byte)i_9;
            class75_7.method1314((i_10 << 13) + i_4 - class243.field3156, (i_11 << 13) + i_5 - class41.field300, 66302213);
            class75_7.field631 = false;
            return true;
         }
      } else if (i_3 == 1) {
         i_4 = class309_0.method5455(2, (byte) -95);
         i_5 = class98.field1259[i_1];
         class98.field1259[i_1] = (i_5 & 0xfffffff) + (((i_5 >> 28) + i_4 & 0x3) << 28);
         return false;
      } else {
         int i_6;
         int i_12;
         if (i_3 == 2) {
            i_4 = class309_0.method5455(5, (byte) 0);
            i_5 = i_4 >> 3;
            i_6 = i_4 & 0x7;
            i_12 = class98.field1259[i_1];
            i_8 = (i_12 >> 28) + i_5 & 0x3;
            i_9 = i_12 >> 14 & 0xff;
            i_10 = i_12 & 0xff;
            if (i_6 == 0) {
               --i_9;
               --i_10;
            }

            if (i_6 == 1) {
               --i_10;
            }

            if (i_6 == 2) {
               ++i_9;
               --i_10;
            }

            if (i_6 == 3) {
               --i_9;
            }

            if (i_6 == 4) {
               ++i_9;
            }

            if (i_6 == 5) {
               --i_9;
               ++i_10;
            }

            if (i_6 == 6) {
               ++i_10;
            }

            if (i_6 == 7) {
               ++i_9;
               ++i_10;
            }

            class98.field1259[i_1] = (i_9 << 14) + i_10 + (i_8 << 28);
            return false;
         } else {
            i_4 = class309_0.method5455(18, (byte) -121);
            i_5 = i_4 >> 16;
            i_6 = i_4 >> 8 & 0xff;
            i_12 = i_4 & 0xff;
            i_8 = class98.field1259[i_1];
            i_9 = (i_8 >> 28) + i_5 & 0x3;
            i_10 = i_6 + (i_8 >> 14) & 0xff;
            i_11 = i_8 + i_12 & 0xff;
            class98.field1259[i_1] = (i_10 << 14) + i_11 + (i_9 << 28);
            return false;
         }
      }
   }

}
