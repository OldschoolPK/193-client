import java.io.EOFException;
import java.io.IOException;

public final class class318 {

   static byte[] field3820 = new byte[520];
   class352 field3821 = null;
   class352 field3822 = null;
   int field3823 = 65000;
   int field3824;

   public class318(int i_1, class352 class352_2, class352 class352_3, int i_4) {
      this.field3824 = i_1;
      this.field3821 = class352_2;
      this.field3822 = class352_3;
      this.field3823 = i_4;
   }

   boolean method5859(int i_1, byte[] bytes_2, int i_3, boolean bool_4, int i_5) {
      class352 class352_6 = this.field3821;
      synchronized(this.field3821) {
         try {
            int i_7;
            boolean bool_10000;
            if (bool_4) {
               if (this.field3822.method6537(-1796387438) < (long)(i_1 * 6 + 6)) {
                  bool_10000 = false;
                  return bool_10000;
               }

               this.field3822.method6561((long)(i_1 * 6));
               this.field3822.method6539(field3820, 0, 6, -345399566);
               i_7 = (field3820[5] & 0xff) + ((field3820[3] & 0xff) << 16) + ((field3820[4] & 0xff) << 8);
               if (i_7 <= 0 || (long)i_7 > this.field3821.method6537(1126760383) / 520L) {
                  bool_10000 = false;
                  return bool_10000;
               }
            } else {
               i_7 = (int)((this.field3821.method6537(1251861159) + 519L) / 520L);
               if (i_7 == 0) {
                  i_7 = 1;
               }
            }

            field3820[0] = (byte)(i_3 >> 16);
            field3820[1] = (byte)(i_3 >> 8);
            field3820[2] = (byte)i_3;
            field3820[3] = (byte)(i_7 >> 16);
            field3820[4] = (byte)(i_7 >> 8);
            field3820[5] = (byte)i_7;
            this.field3822.method6561((long)(i_1 * 6));
            this.field3822.method6541(field3820, 0, 6, (byte) 65);
            int i_8 = 0;
            int i_9 = 0;

            while (true) {
               if (i_8 < i_3) {
                  label170: {
                     int i_10 = 0;
                     int i_11;
                     if (bool_4) {
                        this.field3821.method6561(520L * (long)i_7);
                        int i_12;
                        int i_13;
                        if (i_1 > 65535) {
                           try {
                              this.field3821.method6539(field3820, 0, 10, 941347640);
                           } catch (EOFException eofexception_18) {
                              break label170;
                           }

                           i_11 = ((field3820[1] & 0xff) << 16) + ((field3820[0] & 0xff) << 24) + (field3820[3] & 0xff) + ((field3820[2] & 0xff) << 8);
                           i_12 = (field3820[5] & 0xff) + ((field3820[4] & 0xff) << 8);
                           i_10 = (field3820[8] & 0xff) + ((field3820[7] & 0xff) << 8) + ((field3820[6] & 0xff) << 16);
                           i_13 = field3820[9] & 0xff;
                        } else {
                           try {
                              this.field3821.method6539(field3820, 0, 8, -1016766570);
                           } catch (EOFException eofexception_17) {
                              break label170;
                           }

                           i_11 = (field3820[1] & 0xff) + ((field3820[0] & 0xff) << 8);
                           i_12 = (field3820[3] & 0xff) + ((field3820[2] & 0xff) << 8);
                           i_10 = ((field3820[5] & 0xff) << 8) + ((field3820[4] & 0xff) << 16) + (field3820[6] & 0xff);
                           i_13 = field3820[7] & 0xff;
                        }

                        if (i_11 != i_1 || i_9 != i_12 || i_13 != this.field3824) {
                           bool_10000 = false;
                           return bool_10000;
                        }

                        if (i_10 < 0 || (long)i_10 > this.field3821.method6537(304125418) / 520L) {
                           bool_10000 = false;
                           return bool_10000;
                        }
                     }

                     if (i_10 == 0) {
                        bool_4 = false;
                        i_10 = (int)((this.field3821.method6537(622068109) + 519L) / 520L);
                        if (i_10 == 0) {
                           ++i_10;
                        }

                        if (i_10 == i_7) {
                           ++i_10;
                        }
                     }

                     if (i_1 > 65535) {
                        if (i_3 - i_8 <= 510) {
                           i_10 = 0;
                        }

                        field3820[0] = (byte)(i_1 >> 24);
                        field3820[1] = (byte)(i_1 >> 16);
                        field3820[2] = (byte)(i_1 >> 8);
                        field3820[3] = (byte)i_1;
                        field3820[4] = (byte)(i_9 >> 8);
                        field3820[5] = (byte)i_9;
                        field3820[6] = (byte)(i_10 >> 16);
                        field3820[7] = (byte)(i_10 >> 8);
                        field3820[8] = (byte)i_10;
                        field3820[9] = (byte)this.field3824;
                        this.field3821.method6561(520L * (long)i_7);
                        this.field3821.method6541(field3820, 0, 10, (byte) 65);
                        i_11 = i_3 - i_8;
                        if (i_11 > 510) {
                           i_11 = 510;
                        }

                        this.field3821.method6541(bytes_2, i_8, i_11, (byte) 65);
                        i_8 += i_11;
                     } else {
                        if (i_3 - i_8 <= 512) {
                           i_10 = 0;
                        }

                        field3820[0] = (byte)(i_1 >> 8);
                        field3820[1] = (byte)i_1;
                        field3820[2] = (byte)(i_9 >> 8);
                        field3820[3] = (byte)i_9;
                        field3820[4] = (byte)(i_10 >> 16);
                        field3820[5] = (byte)(i_10 >> 8);
                        field3820[6] = (byte)i_10;
                        field3820[7] = (byte)this.field3824;
                        this.field3821.method6561(520L * (long)i_7);
                        this.field3821.method6541(field3820, 0, 8, (byte) 65);
                        i_11 = i_3 - i_8;
                        if (i_11 > 512) {
                           i_11 = 512;
                        }

                        this.field3821.method6541(bytes_2, i_8, i_11, (byte) 65);
                        i_8 += i_11;
                     }

                     i_7 = i_10;
                     ++i_9;
                     continue;
                  }
               }

               bool_10000 = true;
               return bool_10000;
            }
         } catch (IOException ioexception_19) {
            return false;
         }
      }
   }

   public boolean method5860(int i_1, byte[] bytes_2, int i_3, int i_4) {
      class352 class352_5 = this.field3821;
      synchronized(this.field3821) {
         if (i_3 >= 0 && i_3 <= this.field3823) {
            boolean bool_6 = this.method5859(i_1, bytes_2, i_3, true, -1633261977);
            if (!bool_6) {
               bool_6 = this.method5859(i_1, bytes_2, i_3, false, -1860840708);
            }

            return bool_6;
         } else {
            throw new IllegalArgumentException("" + this.field3824 + ',' + i_1 + ',' + i_3);
         }
      }
   }

   public byte[] method5864(int i_1, int i_2) {
      class352 class352_3 = this.field3821;
      synchronized(this.field3821) {
         try {
            Object obj_10000;
            if (this.field3822.method6537(-878696422) < (long)(i_1 * 6 + 6)) {
               obj_10000 = null;
               return (byte[]) obj_10000;
            } else {
               this.field3822.method6561((long)(i_1 * 6));
               this.field3822.method6539(field3820, 0, 6, -707431061);
               int i_4 = ((field3820[0] & 0xff) << 16) + (field3820[2] & 0xff) + ((field3820[1] & 0xff) << 8);
               int i_5 = (field3820[5] & 0xff) + ((field3820[3] & 0xff) << 16) + ((field3820[4] & 0xff) << 8);
               if (i_4 < 0 || i_4 > this.field3823) {
                  obj_10000 = null;
                  return (byte[]) obj_10000;
               } else if (i_5 <= 0 || (long)i_5 > this.field3821.method6537(-1755028343) / 520L) {
                  obj_10000 = null;
                  return (byte[]) obj_10000;
               } else {
                  byte[] bytes_6 = new byte[i_4];
                  int i_7 = 0;

                  for (int i_8 = 0; i_7 < i_4; i_8++) {
                     if (i_5 == 0) {
                        obj_10000 = null;
                        return (byte[]) obj_10000;
                     }

                     this.field3821.method6561((long)i_5 * 520L);
                     int i_9 = i_4 - i_7;
                     int i_10;
                     int i_11;
                     int i_12;
                     int i_13;
                     byte b_14;
                     if (i_1 > 65535) {
                        if (i_9 > 510) {
                           i_9 = 510;
                        }

                        b_14 = 10;
                        this.field3821.method6539(field3820, 0, b_14 + i_9, 242266459);
                        i_10 = ((field3820[1] & 0xff) << 16) + ((field3820[0] & 0xff) << 24) + (field3820[3] & 0xff) + ((field3820[2] & 0xff) << 8);
                        i_11 = (field3820[5] & 0xff) + ((field3820[4] & 0xff) << 8);
                        i_12 = (field3820[8] & 0xff) + ((field3820[7] & 0xff) << 8) + ((field3820[6] & 0xff) << 16);
                        i_13 = field3820[9] & 0xff;
                     } else {
                        if (i_9 > 512) {
                           i_9 = 512;
                        }

                        b_14 = 8;
                        this.field3821.method6539(field3820, 0, i_9 + b_14, -1018265264);
                        i_10 = (field3820[1] & 0xff) + ((field3820[0] & 0xff) << 8);
                        i_11 = (field3820[3] & 0xff) + ((field3820[2] & 0xff) << 8);
                        i_12 = ((field3820[5] & 0xff) << 8) + ((field3820[4] & 0xff) << 16) + (field3820[6] & 0xff);
                        i_13 = field3820[7] & 0xff;
                     }

                     if (i_10 != i_1 || i_8 != i_11 || i_13 != this.field3824) {
                        obj_10000 = null;
                        return (byte[]) obj_10000;
                     }

                     if (i_12 < 0 || (long)i_12 > this.field3821.method6537(-2056708143) / 520L) {
                        obj_10000 = null;
                        return (byte[]) obj_10000;
                     }

                     int i_15 = b_14 + i_9;

                     for (int i_16 = b_14; i_16 < i_15; i_16++) {
                        bytes_6[i_7++] = field3820[i_16];
                     }

                     i_5 = i_12;
                  }

                  byte[] bytes_21 = bytes_6;
                  return bytes_21;
               }
            }
         } catch (IOException ioexception_19) {
            return null;
         }
      }
   }

   public String abd() {
      return "" + this.field3824;
   }

   public String aba() {
      return "" + this.field3824 * 447484744;
   }

   public String toString() {
      return "" + this.field3824;
   }

}
