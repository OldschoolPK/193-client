public class class48 extends class30 {

   int field361;
   int field359;
   int field360;
   int field363;

   int method741(byte b_1) {
      return this.field360;
   }

   int method742(int i_1) {
      return this.field363;
   }

   int method739(int i_1) {
      return this.field361;
   }

   void vmethod745(class310 class310_1, int i_2) {
      super.field189 = Math.min(super.field189, 4);
      super.field185 = new short[1][64][64];
      super.field186 = new short[super.field189][64][64];
      super.field178 = new byte[super.field189][64][64];
      super.field177 = new byte[super.field189][64][64];
      super.field179 = new class34[super.field189][64][64][];
      int i_3 = class310_1.method5661((byte) -18);
      if (i_3 != class39.field277.field280) {
         throw new IllegalStateException("");
      } else {
         int i_4 = class310_1.method5661((byte) -21);
         int i_5 = class310_1.method5661((byte) 5);
         int i_6 = class310_1.method5661((byte) -50);
         int i_7 = class310_1.method5661((byte) 25);
         if (i_4 == super.field180 && i_5 == super.field188 && i_6 == this.field360 && i_7 == this.field363) {
            for (int i_8 = 0; i_8 < 8; i_8++) {
               for (int i_9 = 0; i_9 < 8; i_9++) {
                  this.method305(i_8 + this.field360 * 8, i_9 + this.field363 * 8, class310_1, -9597111);
               }
            }

         } else {
            throw new IllegalStateException("");
         }
      }
   }

   void method737(class310 class310_1, int i_2) {
      int i_3 = class310_1.method5661((byte) 25);
      if (i_3 != class40.field296.field291) {
         throw new IllegalStateException("");
      } else {
         super.field181 = class310_1.method5661((byte) -66);
         super.field189 = class310_1.method5661((byte) 42);
         super.field187 = class310_1.method5729(1228788773);
         super.field182 = class310_1.method5729(1174963588);
         this.field361 = class310_1.method5661((byte) -54);
         this.field359 = class310_1.method5661((byte) 60);
         super.field180 = class310_1.method5729(312326630);
         super.field188 = class310_1.method5729(1472486849);
         this.field360 = class310_1.method5661((byte) -31);
         this.field363 = class310_1.method5661((byte) -91);
         super.field183 = class310_1.method5519((short) 1005);
         super.field184 = class310_1.method5519((short) 1005);
      }
   }

   int method740(int i_1) {
      return this.field359;
   }

   public boolean equals(Object object_1) {
      if (!(object_1 instanceof class48)) {
         return false;
      } else {
         class48 class48_2 = (class48) object_1;
         return class48_2.field180 == super.field180 && class48_2.field188 == super.field188 ? class48_2.field360 == this.field360 && this.field363 == class48_2.field363 : false;
      }
   }

   public int hashCode() {
      return super.field180 | super.field188 << 8 | this.field360 << 16 | this.field363 << 24;
   }

   public static class256 method766(int i_0, int i_1) {
      class256 class256_2 = (class256) class256.field3294.method3376((long)i_0);
      if (class256_2 != null) {
         return class256_2;
      } else {
         byte[] bytes_3 = class256.field3299.method4160(3, i_0, (short) -18441);
         class256_2 = new class256();
         if (bytes_3 != null) {
            class256_2.method4429(new class310(bytes_3), (short) 24464);
         }

         class256.field3294.method3374(class256_2, (long)i_0);
         return class256_2;
      }
   }

   static void method767(int i_0) {
      for (int i_1 = 0; i_1 < client.field785; i_1++) {
         if (class42.method624(client.field844[i_1], -546925347)) {
            if (i_1 < client.field785 - 1) {
               for (int i_2 = i_1; i_2 < client.field785 - 1; i_2++) {
                  client.field790[i_2] = client.field790[i_2 + 1];
                  client.field791[i_2] = client.field791[i_2 + 1];
                  client.field844[i_2] = client.field844[i_2 + 1];
                  client.field789[i_2] = client.field789[i_2 + 1];
                  client.field786[i_2] = client.field786[i_2 + 1];
                  client.field787[i_2] = client.field787[i_2 + 1];
                  client.field792[i_2] = client.field792[i_2 + 1];
               }
            }

            --i_1;
            --client.field785;
         }
      }

      class167.method3494(class149.field1928 / 2 + class285.field3631, class102.field1315, (short) 16655);
   }

   static final void method765(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (i_2 >= 1 && i_3 >= 1 && i_2 <= 102 && i_3 <= 102) {
         if (client.field910 && i_0 != class151.field1949) {
            return;
         }

         long long_8 = 0L;
         boolean bool_10 = true;
         boolean bool_11 = false;
         boolean bool_12 = false;
         if (i_1 == 0) {
            long_8 = class67.field536.method3138(i_0, i_2, i_3);
         }

         if (i_1 == 1) {
            long_8 = class67.field536.method3158(i_0, i_2, i_3);
         }

         if (i_1 == 2) {
            long_8 = class67.field536.method3159(i_0, i_2, i_3);
         }

         if (i_1 == 3) {
            long_8 = class67.field536.method3160(i_0, i_2, i_3);
         }

         int i_13;
         if (long_8 != 0L) {
            i_13 = class67.field536.method3139(i_0, i_2, i_3, long_8);
            int i_15 = class149.method3348(long_8);
            int i_16 = i_13 & 0x1f;
            int i_17 = i_13 >> 6 & 0x3;
            class264 class264_14;
            if (i_1 == 0) {
               class67.field536.method3148(i_0, i_2, i_3);
               class264_14 = class34.method383(i_15, -1671397719);
               if (class264_14.field3397 != 0) {
                  client.field831[i_0].method3587(i_2, i_3, i_16, i_17, class264_14.field3398, 1790847002);
               }
            }

            if (i_1 == 1) {
               class67.field536.method3149(i_0, i_2, i_3);
            }

            if (i_1 == 2) {
               class67.field536.method3150(i_0, i_2, i_3);
               class264_14 = class34.method383(i_15, -821024710);
               if (i_2 + class264_14.field3418 > 103 || i_3 + class264_14.field3418 > 103 || i_2 + class264_14.field3396 > 103 || i_3 + class264_14.field3396 > 103) {
                  return;
               }

               if (class264_14.field3397 != 0) {
                  client.field831[i_0].method3588(i_2, i_3, class264_14.field3418, class264_14.field3396, i_17, class264_14.field3398, (byte) 4);
               }
            }

            if (i_1 == 3) {
               class67.field536.method3151(i_0, i_2, i_3);
               class264_14 = class34.method383(i_15, -687020054);
               if (class264_14.field3397 == 1) {
                  client.field831[i_0].method3590(i_2, i_3, (byte) 22);
               }
            }
         }

         if (i_4 >= 0) {
            i_13 = i_0;
            if (i_0 < 3 && (class64.field507[1][i_2][i_3] & 0x2) == 2) {
               i_13 = i_0 + 1;
            }

            class71.method1259(i_0, i_13, i_2, i_3, i_4, i_5, i_6, class67.field536, client.field831[i_0], (byte) -82);
         }
      }

   }

   static int method736(int i_0, class101 class101_1, boolean bool_2, byte b_3) {
      int i_5 = -1;
      class226 class226_4;
      if (i_0 >= 2000) {
         i_0 -= 1000;
         i_5 = class85.field1095[--class253.field3267];
         class226_4 = class181.method3610(i_5, -388324411);
      } else {
         class226_4 = bool_2 ? class223.field2561 : class253.field3264;
      }

      if (i_0 == 1100) {
         class253.field3267 -= 2;
         class226_4.field2607 = class85.field1095[class253.field3267];
         if (class226_4.field2607 > class226_4.field2609 - class226_4.field2601) {
            class226_4.field2607 = class226_4.field2609 - class226_4.field2601;
         }

         if (class226_4.field2607 < 0) {
            class226_4.field2607 = 0;
         }

         class226_4.field2608 = class85.field1095[class253.field3267 + 1];
         if (class226_4.field2608 > class226_4.field2639 - class226_4.field2602) {
            class226_4.field2608 = class226_4.field2639 - class226_4.field2602;
         }

         if (class226_4.field2608 < 0) {
            class226_4.field2608 = 0;
         }

         class181.method3609(class226_4, -1821957000);
         return 1;
      } else if (i_0 == 1101) {
         class226_4.field2611 = class85.field1095[--class253.field3267];
         class181.method3609(class226_4, 508530437);
         return 1;
      } else if (i_0 == 1102) {
         class226_4.field2615 = class85.field1095[--class253.field3267] == 1;
         class181.method3609(class226_4, 834859726);
         return 1;
      } else if (i_0 == 1103) {
         class226_4.field2652 = class85.field1095[--class253.field3267];
         class181.method3609(class226_4, 367224428);
         return 1;
      } else if (i_0 == 1104) {
         class226_4.field2619 = class85.field1095[--class253.field3267];
         class181.method3609(class226_4, -1224446666);
         return 1;
      } else if (i_0 == 1105) {
         class226_4.field2621 = class85.field1095[--class253.field3267];
         class181.method3609(class226_4, -1628929223);
         return 1;
      } else if (i_0 == 1106) {
         class226_4.field2627 = class85.field1095[--class253.field3267];
         class181.method3609(class226_4, -1189595314);
         return 1;
      } else if (i_0 == 1107) {
         class226_4.field2624 = class85.field1095[--class253.field3267] == 1;
         class181.method3609(class226_4, -485253570);
         return 1;
      } else if (i_0 == 1108) {
         class226_4.field2629 = 1;
         class226_4.field2687 = class85.field1095[--class253.field3267];
         class181.method3609(class226_4, -23322416);
         return 1;
      } else if (i_0 == 1109) {
         class253.field3267 -= 6;
         class226_4.field2635 = class85.field1095[class253.field3267];
         class226_4.field2680 = class85.field1095[class253.field3267 + 1];
         class226_4.field2583 = class85.field1095[class253.field3267 + 2];
         class226_4.field2638 = class85.field1095[class253.field3267 + 3];
         class226_4.field2657 = class85.field1095[class253.field3267 + 4];
         class226_4.field2640 = class85.field1095[class253.field3267 + 5];
         class181.method3609(class226_4, -1512501689);
         return 1;
      } else {
         int i_9;
         if (i_0 == 1110) {
            i_9 = class85.field1095[--class253.field3267];
            if (i_9 != class226_4.field2633) {
               class226_4.field2633 = i_9;
               class226_4.field2718 = 0;
               class226_4.field2715 = 0;
               class181.method3609(class226_4, 1523059024);
            }

            return 1;
         } else if (i_0 == 1111) {
            class226_4.field2710 = class85.field1095[--class253.field3267] == 1;
            class181.method3609(class226_4, -830933240);
            return 1;
         } else if (i_0 == 1112) {
            String string_8 = class85.field1096[--class85.field1105];
            if (!string_8.equals(class226_4.field2647)) {
               class226_4.field2647 = string_8;
               class181.method3609(class226_4, 68329903);
            }

            return 1;
         } else if (i_0 == 1113) {
            class226_4.field2646 = class85.field1095[--class253.field3267];
            class181.method3609(class226_4, -1603754620);
            return 1;
         } else if (i_0 == 1114) {
            class253.field3267 -= 3;
            class226_4.field2650 = class85.field1095[class253.field3267];
            class226_4.field2651 = class85.field1095[class253.field3267 + 1];
            class226_4.field2663 = class85.field1095[class253.field3267 + 2];
            class181.method3609(class226_4, -762940629);
            return 1;
         } else if (i_0 == 1115) {
            class226_4.field2693 = class85.field1095[--class253.field3267] == 1;
            class181.method3609(class226_4, -1280504396);
            return 1;
         } else if (i_0 == 1116) {
            class226_4.field2625 = class85.field1095[--class253.field3267];
            class181.method3609(class226_4, 350780325);
            return 1;
         } else if (i_0 == 1117) {
            class226_4.field2626 = class85.field1095[--class253.field3267];
            class181.method3609(class226_4, -1604512943);
            return 1;
         } else if (i_0 == 1118) {
            class226_4.field2617 = class85.field1095[--class253.field3267] == 1;
            class181.method3609(class226_4, -1325894786);
            return 1;
         } else if (i_0 == 1119) {
            class226_4.field2628 = class85.field1095[--class253.field3267] == 1;
            class181.method3609(class226_4, 516636582);
            return 1;
         } else if (i_0 == 1120) {
            class253.field3267 -= 2;
            class226_4.field2609 = class85.field1095[class253.field3267];
            class226_4.field2639 = class85.field1095[class253.field3267 + 1];
            class181.method3609(class226_4, -485518265);
            if (i_5 != -1 && class226_4.field2588 == 0) {
               class77.method1773(class9.field44[i_5 >> 16], class226_4, false, (byte) 0);
            }

            return 1;
         } else if (i_0 == 1121) {
            class102.method2275(class226_4.field2586, class226_4.field2587, 600674276);
            client.field822 = class226_4;
            class181.method3609(class226_4, 404451854);
            return 1;
         } else if (i_0 == 1122) {
            class226_4.field2622 = class85.field1095[--class253.field3267];
            class181.method3609(class226_4, -1181830958);
            return 1;
         } else if (i_0 == 1123) {
            class226_4.field2612 = class85.field1095[--class253.field3267];
            class181.method3609(class226_4, 505261322);
            return 1;
         } else if (i_0 == 1124) {
            class226_4.field2636 = class85.field1095[--class253.field3267];
            class181.method3609(class226_4, 1327467672);
            return 1;
         } else if (i_0 == 1125) {
            i_9 = class85.field1095[--class253.field3267];
            class333 class333_7 = (class333) class152.method3363(class180.method3573((byte) 8), i_9, 642042854);
            if (class333_7 != null) {
               class226_4.field2616 = class333_7;
               class181.method3609(class226_4, -1173601969);
            }

            return 1;
         } else {
            boolean bool_6;
            if (i_0 == 1126) {
               bool_6 = class85.field1095[--class253.field3267] == 1;
               class226_4.field2620 = bool_6;
               return 1;
            } else if (i_0 == 1127) {
               bool_6 = class85.field1095[--class253.field3267] == 1;
               class226_4.field2673 = bool_6;
               return 1;
            } else {
               return 2;
            }
         }
      }
   }

}
