public class class296 extends class187 {

   static class114 field3677;
   public int field3680 = (int)(class298.method5270(255749540) / 1000L);
   public class293 field3678;
   public short field3679;

   class296(class293 class293_1, int i_2) {
      this.field3678 = class293_1;
      this.field3679 = (short)i_2;
   }

   public static boolean method5250(int i_0, byte b_1) {
      return i_0 >= class229.field2761.field2767 && i_0 <= class229.field2747.field2767 || i_0 == class229.field2748.field2767;
   }

   public static boolean method5249(char var_0, int i_1) {
      return var_0 >= 48 && var_0 <= 57 || var_0 >= 65 && var_0 <= 90 || var_0 >= 97 && var_0 <= 122;
   }

}
