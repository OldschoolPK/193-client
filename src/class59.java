import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.DataLine.Info;

public class class59 extends class114 {

   SourceDataLine field436;
   int field437;
   AudioFormat field435;
   byte[] field438;

   protected void vmethod2487(byte b_1) {
      this.field436.flush();
   }

   protected void vmethod2469(int i_1) {
      if (this.field436 != null) {
         this.field436.close();
         this.field436 = null;
      }

   }

   protected int vmethod2467(byte b_1) {
      return this.field437 - (this.field436.available() >> (class114.field1421 ? 2 : 1));
   }

   protected void vmethod2466(int i_1, short s_2) throws LineUnavailableException {
      try {
         Info dataline$info_3 = new Info(SourceDataLine.class, this.field435, i_1 << (class114.field1421 ? 2 : 1));
         this.field436 = (SourceDataLine) AudioSystem.getLine(dataline$info_3);
         this.field436.open();
         this.field436.start();
         this.field437 = i_1;
      } catch (LineUnavailableException lineunavailableexception_6) {
         int i_5 = (i_1 >>> 1 & 0x55555555) + (i_1 & 0x55555555);
         i_5 = (i_5 >>> 2 & 0x33333333) + (i_5 & 0x33333333);
         i_5 = (i_5 >>> 4) + i_5 & 0xf0f0f0f;
         i_5 += i_5 >>> 8;
         i_5 += i_5 >>> 16;
         int i_4 = i_5 & 0xff;
         if (i_4 != 1) {
            this.vmethod2466(class80.method1842(i_1, 719435212), (short) 10334);
         } else {
            this.field436 = null;
            throw lineunavailableexception_6;
         }
      }
   }

   protected void vmethod2468() {
      int i_1 = 256;
      if (class114.field1421) {
         i_1 <<= 1;
      }

      for (int i_2 = 0; i_2 < i_1; i_2++) {
         int i_3 = super.field1426[i_2];
         if ((i_3 + 8388608 & ~0xffffff) != 0) {
            i_3 = 0x7fffff ^ i_3 >> 31;
         }

         this.field438[i_2 * 2] = (byte)(i_3 >> 8);
         this.field438[i_2 * 2 + 1] = (byte)(i_3 >> 16);
      }

      this.field436.write(this.field438, 0, i_1 << 1);
   }

   protected void vmethod2465(byte b_1) {
      this.field435 = new AudioFormat((float)class114.field1443, 16, class114.field1421 ? 2 : 1, true, false);
      this.field438 = new byte[256 << (class114.field1421 ? 2 : 1)];
   }

}
