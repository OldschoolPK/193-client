public abstract class class179 {

   public abstract int vmethod3547(int var1, int var2, int var3);

   public abstract void vmethod3550(int var1);

   public static String method3552(byte[] bytes_0, int i_1, int i_2, byte b_3) {
      char[] arr_4 = new char[i_2];
      int i_5 = 0;

      for (int i_6 = 0; i_6 < i_2; i_6++) {
         int i_7 = bytes_0[i_6 + i_1] & 0xff;
         if (i_7 != 0) {
            if (i_7 >= 128 && i_7 < 160) {
               char var_8 = class298.field3688[i_7 - 128];
               if (var_8 == 0) {
                  var_8 = 63;
               }

               i_7 = var_8;
            }

            arr_4[i_5++] = (char)i_7;
         }
      }

      return new String(arr_4, 0, i_5);
   }

}
