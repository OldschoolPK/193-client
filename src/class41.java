public class class41 {

   static int field300;
   static int[] field297;
   static class246 field299;
   static int field302;

   public static boolean method603(int i_0, int i_1) {
      if (class195.field2355[i_0]) {
         return true;
      } else if (!class58.field427.method4164(i_0, 1897125977)) {
         return false;
      } else {
         int i_2 = class58.field427.method4172(i_0, (byte) 34);
         if (i_2 == 0) {
            class195.field2355[i_0] = true;
            return true;
         } else {
            if (class9.field44[i_0] == null) {
               class9.field44[i_0] = new class226[i_2];
            }

            for (int i_3 = 0; i_3 < i_2; i_3++) {
               if (class9.field44[i_0][i_3] == null) {
                  byte[] bytes_4 = class58.field427.method4160(i_0, i_3, (short) -14999);
                  if (bytes_4 != null) {
                     class9.field44[i_0][i_3] = new class226();
                     class9.field44[i_0][i_3].field2586 = i_3 + (i_0 << 16);
                     if (bytes_4[0] == -1) {
                        class9.field44[i_0][i_3].method4045(new class310(bytes_4), (byte) -2);
                     } else {
                        class9.field44[i_0][i_3].method4061(new class310(bytes_4), 1734549663);
                     }
                  }
               }
            }

            class195.field2355[i_0] = true;
            return true;
         }
      }
   }

   static int method602(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      int i_4;
      int i_5;
      if (i_0 == 4000) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_5 + i_4;
         return 1;
      } else if (i_0 == 4001) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 - i_5;
         return 1;
      } else if (i_0 == 4002) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_5 * i_4;
         return 1;
      } else if (i_0 == 4003) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 / i_5;
         return 1;
      } else if (i_0 == 4004) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = (int)(Math.random() * (double)i_4);
         return 1;
      } else if (i_0 == 4005) {
         i_4 = class85.field1095[--class253.field3267];
         class85.field1095[++class253.field3267 - 1] = (int)(Math.random() * (double)(i_4 + 1));
         return 1;
      } else if (i_0 == 4006) {
         class253.field3267 -= 5;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         int i_6 = class85.field1095[class253.field3267 + 2];
         int i_7 = class85.field1095[class253.field3267 + 3];
         int i_8 = class85.field1095[class253.field3267 + 4];
         class85.field1095[++class253.field3267 - 1] = i_4 + (i_5 - i_4) * (i_8 - i_6) / (i_7 - i_6);
         return 1;
      } else if (i_0 == 4007) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 + i_4 * i_5 / 100;
         return 1;
      } else if (i_0 == 4008) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 | 1 << i_5;
         return 1;
      } else if (i_0 == 4009) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 & -1 - (1 << i_5);
         return 1;
      } else if (i_0 == 4010) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = (i_4 & 1 << i_5) != 0 ? 1 : 0;
         return 1;
      } else if (i_0 == 4011) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 % i_5;
         return 1;
      } else if (i_0 == 4012) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         if (i_4 == 0) {
            class85.field1095[++class253.field3267 - 1] = 0;
         } else {
            class85.field1095[++class253.field3267 - 1] = (int)Math.pow((double)i_4, (double)i_5);
         }

         return 1;
      } else if (i_0 == 4013) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         if (i_4 == 0) {
            class85.field1095[++class253.field3267 - 1] = 0;
            return 1;
         } else {
            switch(i_5) {
            case 0:
               class85.field1095[++class253.field3267 - 1] = Integer.MAX_VALUE;
               break;
            case 1:
               class85.field1095[++class253.field3267 - 1] = i_4;
               break;
            case 2:
               class85.field1095[++class253.field3267 - 1] = (int)Math.sqrt((double)i_4);
               break;
            case 3:
               class85.field1095[++class253.field3267 - 1] = (int)Math.cbrt((double)i_4);
               break;
            case 4:
               class85.field1095[++class253.field3267 - 1] = (int)Math.sqrt(Math.sqrt((double)i_4));
               break;
            default:
               class85.field1095[++class253.field3267 - 1] = (int)Math.pow((double)i_4, 1.0D / (double)i_5);
            }

            return 1;
         }
      } else if (i_0 == 4014) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 & i_5;
         return 1;
      } else if (i_0 == 4015) {
         class253.field3267 -= 2;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         class85.field1095[++class253.field3267 - 1] = i_4 | i_5;
         return 1;
      } else if (i_0 == 4018) {
         class253.field3267 -= 3;
         long long_10 = (long)class85.field1095[class253.field3267];
         long long_12 = (long)class85.field1095[class253.field3267 + 1];
         long long_14 = (long)class85.field1095[class253.field3267 + 2];
         class85.field1095[++class253.field3267 - 1] = (int)(long_10 * long_14 / long_12);
         return 1;
      } else {
         return 2;
      }
   }

}
