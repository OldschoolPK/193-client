public class class98 {

   static int field1266;
   static int field1260;
   static int field1267;
   static byte[] field1252 = new byte[2048];
   static byte[] field1253 = new byte[2048];
   static class310[] field1254 = new class310[2048];
   static int field1263 = 0;
   static int[] field1268 = new int[2048];
   static int field1257 = 0;
   static int[] field1258 = new int[2048];
   static int[] field1259 = new int[2048];
   static int[] field1269 = new int[2048];
   static int[] field1261 = new int[2048];
   static int field1262 = 0;
   static int[] field1265 = new int[2048];
   static class310 field1264 = new class310(new byte[5000]);

   static final boolean method2197(int i_0, int i_1) {
      if (i_0 < 0) {
         return false;
      } else {
         int i_2 = client.field844[i_0];
         if (i_2 >= 2000) {
            i_2 -= 2000;
         }

         return i_2 == 1007;
      }
   }

   static void method2180(int i_0) {
      client.field656 = -1L;
      class117.field1461.field591 = 0;
      class71.field573 = true;
      client.field660 = true;
      client.field817 = -1L;
      class105.field1342 = new class271();
      client.field694.method2307((short) -15009);
      client.field694.field1325.field3751 = 0;
      client.field694.field1330 = null;
      client.field694.field1335 = null;
      client.field694.field1336 = null;
      client.field694.field1337 = null;
      client.field694.field1338 = 0;
      client.field694.field1333 = 0;
      client.field662 = 0;
      client.field733 = 0;
      client.field740 = 0;
      client.field785 = 0;
      client.field784 = false;
      class103.method2289(0, (byte) 60);
      class64.method1203(-834710380);
      client.field801 = 0;
      client.field803 = false;
      client.field884 = 0;
      client.field830 = 0;
      client.field723 = 0;
      class27.field148 = null;
      client.field894 = 0;
      client.field874 = -1;
      client.field774 = 0;
      client.field880 = 0;
      client.field671 = class93.field1177;
      client.field672 = class93.field1177;
      client.field690 = 0;
      field1263 = 0;

      int i_1;
      for (i_1 = 0; i_1 < 2048; i_1++) {
         field1254[i_1] = null;
         field1253[i_1] = 1;
      }

      for (i_1 = 0; i_1 < 2048; i_1++) {
         client.field764[i_1] = null;
      }

      for (i_1 = 0; i_1 < 32768; i_1++) {
         client.field689[i_1] = null;
      }

      client.field814 = -1;
      client.field778.method4876();
      client.field734.method4876();

      int i_3;
      for (i_1 = 0; i_1 < 4; i_1++) {
         for (int i_2 = 0; i_2 < 104; i_2++) {
            for (i_3 = 0; i_3 < 104; i_3++) {
               client.field776[i_1][i_2][i_3] = null;
            }
         }
      }

      client.field876 = new class272();
      class173.field2036.method1896(-2051278822);

      for (i_1 = 0; i_1 < class251.field3237; i_1++) {
         class251 class251_4 = class219.method3976(i_1, -1579904760);
         if (class251_4 != null) {
            class221.field2539[i_1] = 0;
            class221.field2541[i_1] = 0;
         }
      }

      class99.field1270.method2229(-1770667153);
      client.field818 = -1;
      if (client.field873 != -1) {
         i_1 = client.field873;
         if (i_1 != -1 && class195.field2355[i_1]) {
            class58.field427.method4175(i_1, 1386061158);
            if (class9.field44[i_1] != null) {
               boolean bool_6 = true;

               for (i_3 = 0; i_3 < class9.field44[i_1].length; i_3++) {
                  if (class9.field44[i_1][i_3] != null) {
                     if (class9.field44[i_1][i_3].field2588 != 2) {
                        class9.field44[i_1][i_3] = null;
                     } else {
                        bool_6 = false;
                     }
                  }
               }

               if (bool_6) {
                  class9.field44[i_1] = null;
               }

               class195.field2355[i_1] = false;
            }
         }
      }

      for (class70 class70_5 = (class70) client.field833.method5962(); class70_5 != null; class70_5 = (class70) client.field833.method5957()) {
         class72.method1262(class70_5, true, 1233572902);
      }

      client.field873 = -1;
      client.field833 = new class328(8);
      client.field822 = null;
      client.field785 = 0;
      client.field784 = false;
      client.field909.method4008((int[]) null, new int[] {0, 0, 0, 0, 0}, false, -1, 1899379562);

      for (i_1 = 0; i_1 < 8; i_1++) {
         client.field772[i_1] = null;
         client.field773[i_1] = false;
      }

      class68.field546 = new class328(32);
      client.field895 = true;

      for (i_1 = 0; i_1 < 100; i_1++) {
         client.field901[i_1] = true;
      }

      class72.method1264(929495019);
      class35.field244 = null;

      for (i_1 = 0; i_1 < 8; i_1++) {
         client.field913[i_1] = new class18();
      }

      class285.field3634 = null;
   }

   static final void method2182(class226[] arr_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7, int i_8) {
      for (int i_9 = 0; i_9 < arr_0.length; i_9++) {
         class226 class226_10 = arr_0[i_9];
         if (class226_10 != null && class226_10.field2670 == i_1 && (!class226_10.field2585 || class226_10.field2588 == 0 || class226_10.field2575 || class146.method3318(class226_10, 1314768981) != 0 || class226_10 == client.field832 || class226_10.field2695 == 1338)) {
            if (class226_10.field2585) {
               if (class55.method896(class226_10, (byte) 119)) {
                  continue;
               }
            } else if (class226_10.field2588 == 0 && class226_10 != class262.field3350 && class55.method896(class226_10, (byte) 30)) {
               continue;
            }

            int i_11 = class226_10.field2694 + i_6;
            int i_12 = i_7 + class226_10.field2600;
            int i_13;
            int i_14;
            int i_15;
            int i_16;
            int i_18;
            int i_19;
            if (class226_10.field2588 == 2) {
               i_13 = i_2;
               i_14 = i_3;
               i_15 = i_4;
               i_16 = i_5;
            } else {
               int i_17;
               if (class226_10.field2588 == 9) {
                  i_17 = i_11;
                  i_18 = i_12;
                  i_19 = i_11 + class226_10.field2601;
                  int i_20 = i_12 + class226_10.field2602;
                  if (i_19 < i_11) {
                     i_17 = i_19;
                     i_19 = i_11;
                  }

                  if (i_20 < i_12) {
                     i_18 = i_20;
                     i_20 = i_12;
                  }

                  ++i_19;
                  ++i_20;
                  i_13 = i_17 > i_2 ? i_17 : i_2;
                  i_14 = i_18 > i_3 ? i_18 : i_3;
                  i_15 = i_19 < i_4 ? i_19 : i_4;
                  i_16 = i_20 < i_5 ? i_20 : i_5;
               } else {
                  i_17 = i_11 + class226_10.field2601;
                  i_18 = i_12 + class226_10.field2602;
                  i_13 = i_11 > i_2 ? i_11 : i_2;
                  i_14 = i_12 > i_3 ? i_12 : i_3;
                  i_15 = i_17 < i_4 ? i_17 : i_4;
                  i_16 = i_18 < i_5 ? i_18 : i_5;
               }
            }

            if (class226_10 == client.field821) {
               client.field829 = true;
               client.field853 = i_11;
               client.field862 = i_12;
            }

            boolean bool_33 = false;
            if (class226_10.field2590) {
               switch(client.field810) {
               case 0:
                  bool_33 = true;
               case 1:
               default:
                  break;
               case 2:
                  if (client.field811 == class226_10.field2586 >>> 16) {
                     bool_33 = true;
                  }
                  break;
               case 3:
                  if (class226_10.field2586 == client.field811) {
                     bool_33 = true;
                  }
               }
            }

            if (bool_33 || !class226_10.field2585 || i_13 < i_15 && i_14 < i_16) {
               if (class226_10.field2585) {
                  class71 class71_27;
                  if (class226_10.field2726) {
                     if (class63.field492 >= i_13 && class63.field499 >= i_14 && class63.field492 < i_15 && class63.field499 < i_16) {
                        for (class71_27 = (class71) client.field847.method4879(); class71_27 != null; class71_27 = (class71) client.field847.method4884()) {
                           if (class71_27.field581) {
                              class71_27.method3628();
                              class71_27.field571.field2717 = false;
                           }
                        }

                        if (class65.field521 == 0) {
                           client.field821 = null;
                           client.field832 = null;
                        }

                        if (!client.field784) {
                           class74.method1301(-866808650);
                        }
                     }
                  } else if (class226_10.field2727 && class63.field492 >= i_13 && class63.field499 >= i_14 && class63.field492 < i_15 && class63.field499 < i_16) {
                     for (class71_27 = (class71) client.field847.method4879(); class71_27 != null; class71_27 = (class71) client.field847.method4884()) {
                        if (class71_27.field581 && class71_27.field571.field2653 == class71_27.field572) {
                           class71_27.method3628();
                        }
                     }
                  }
               }

               i_18 = class63.field492;
               i_19 = class63.field499;
               if (class63.field483 != 0) {
                  i_18 = class63.field489;
                  i_19 = class63.field502;
               }

               boolean bool_34 = i_18 >= i_13 && i_19 >= i_14 && i_18 < i_15 && i_19 < i_16;
               if (class226_10.field2695 == 1337) {
                  if (!client.field895 && !client.field784 && bool_34) {
                     class250.method4335(i_18, i_19, i_13, i_14, -982024968);
                  }
               } else if (class226_10.field2695 == 1338) {
                  class165.method3480(class226_10, i_11, i_12, 171068975);
               } else {
                  if (class226_10.field2695 == 1400) {
                     class31.field198.method6316(class63.field492, class63.field499, bool_34, i_11, i_12, class226_10.field2601, class226_10.field2602, -1174840947);
                  }

                  if (!client.field784 && bool_34) {
                     if (class226_10.field2695 == 1400) {
                        class31.field198.method6501(i_11, i_12, class226_10.field2601, class226_10.field2602, i_18, i_19, -1942025362);
                     } else {
                        class38.method499(class226_10, i_18 - i_11, i_19 - i_12, (byte) 54);
                     }
                  }

                  boolean bool_22;
                  int i_24;
                  if (bool_33) {
                     for (int i_21 = 0; i_21 < class226_10.field2649.length; i_21++) {
                        bool_22 = false;
                        boolean bool_23 = false;
                        if (!bool_22 && class226_10.field2649[i_21] != null) {
                           for (i_24 = 0; i_24 < class226_10.field2649[i_21].length; i_24++) {
                              boolean bool_25 = false;
                              if (class226_10.field2684 != null) {
                                 bool_25 = class54.field416[class226_10.field2649[i_21][i_24]];
                              }

                              if (class57.method911(class226_10.field2649[i_21][i_24], (byte) 27) || bool_25) {
                                 bool_22 = true;
                                 if (class226_10.field2684 != null && class226_10.field2684[i_21] > client.field655) {
                                    break;
                                 }

                                 byte b_26 = class226_10.field2637[i_21][i_24];
                                 if (b_26 == 0 || ((b_26 & 0x8) == 0 || !class54.field416[86] && !class54.field416[82] && !class54.field416[81]) && ((b_26 & 0x2) == 0 || class54.field416[86]) && ((b_26 & 0x1) == 0 || class54.field416[82]) && ((b_26 & 0x4) == 0 || class54.field416[81])) {
                                    bool_23 = true;
                                    break;
                                 }
                              }
                           }
                        }

                        if (bool_23) {
                           if (i_21 < 10) {
                              class3.method31(i_21 + 1, class226_10.field2586, class226_10.field2587, class226_10.field2712, "", 2076133641);
                           } else if (i_21 == 10) {
                              class45.method706(1369174129);
                              class194.method3648(class226_10.field2586, class226_10.field2587, class191.method3638(class146.method3318(class226_10, 1116912381), -1751724590), class226_10.field2712, 1797863012);
                              client.field851 = class241.method4150(class226_10, (byte) -113);
                              if (client.field851 == null) {
                                 client.field851 = "null";
                              }

                              client.field807 = class226_10.field2665 + class23.method222(16777215, (byte) 14);
                           }

                           i_24 = class226_10.field2679[i_21];
                           if (class226_10.field2684 == null) {
                              class226_10.field2684 = new int[class226_10.field2649.length];
                           }

                           if (class226_10.field2664 == null) {
                              class226_10.field2664 = new int[class226_10.field2649.length];
                           }

                           if (i_24 != 0) {
                              if (class226_10.field2684[i_21] == 0) {
                                 class226_10.field2684[i_21] = i_24 + client.field655 + class226_10.field2664[i_21];
                              } else {
                                 class226_10.field2684[i_21] = i_24 + client.field655;
                              }
                           } else {
                              class226_10.field2684[i_21] = Integer.MAX_VALUE;
                           }
                        }

                        if (!bool_22 && class226_10.field2684 != null) {
                           class226_10.field2684[i_21] = 0;
                        }
                     }
                  }

                  if (class226_10.field2585) {
                     if (class63.field492 >= i_13 && class63.field499 >= i_14 && class63.field492 < i_15 && class63.field499 < i_16) {
                        bool_34 = true;
                     } else {
                        bool_34 = false;
                     }

                     boolean bool_35 = false;
                     if ((class63.field491 == 1 || !class161.field1989 && class63.field491 == 4) && bool_34) {
                        bool_35 = true;
                     }

                     bool_22 = false;
                     if ((class63.field483 == 1 || !class161.field1989 && class63.field483 == 4) && class63.field489 >= i_13 && class63.field502 >= i_14 && class63.field489 < i_15 && class63.field502 < i_16) {
                        bool_22 = true;
                     }

                     if (bool_22) {
                        class13.method130(class226_10, class63.field489 - i_11, class63.field502 - i_12, 912859307);
                     }

                     if (class226_10.field2695 == 1400) {
                        class31.field198.method6317(i_18, i_19, bool_34 & bool_35, bool_34 & bool_22, 771999489);
                     }

                     if (client.field821 != null && class226_10 != client.field821 && bool_34 && class66.method1216(class146.method3318(class226_10, -905489926), 1753340272)) {
                        client.field825 = class226_10;
                     }

                     if (class226_10 == client.field832) {
                        client.field859 = true;
                        client.field827 = i_11;
                        client.field716 = i_12;
                     }

                     if (class226_10.field2575) {
                        class71 class71_28;
                        if (bool_34 && client.field846 != 0 && class226_10.field2653 != null) {
                           class71_28 = new class71();
                           class71_28.field581 = true;
                           class71_28.field571 = class226_10;
                           class71_28.field579 = client.field846;
                           class71_28.field572 = class226_10.field2653;
                           client.field847.method4888(class71_28);
                        }

                        if (client.field821 != null || class171.field2027 != null || client.field784) {
                           bool_22 = false;
                           bool_35 = false;
                           bool_34 = false;
                        }

                        if (!class226_10.field2605 && bool_22) {
                           class226_10.field2605 = true;
                           if (class226_10.field2674 != null) {
                              class71_28 = new class71();
                              class71_28.field581 = true;
                              class71_28.field571 = class226_10;
                              class71_28.field570 = class63.field489 - i_11;
                              class71_28.field579 = class63.field502 - i_12;
                              class71_28.field572 = class226_10.field2674;
                              client.field847.method4888(class71_28);
                           }
                        }

                        if (class226_10.field2605 && bool_35 && class226_10.field2675 != null) {
                           class71_28 = new class71();
                           class71_28.field581 = true;
                           class71_28.field571 = class226_10;
                           class71_28.field570 = class63.field492 - i_11;
                           class71_28.field579 = class63.field499 - i_12;
                           class71_28.field572 = class226_10.field2675;
                           client.field847.method4888(class71_28);
                        }

                        if (class226_10.field2605 && !bool_35) {
                           class226_10.field2605 = false;
                           if (class226_10.field2676 != null) {
                              class71_28 = new class71();
                              class71_28.field581 = true;
                              class71_28.field571 = class226_10;
                              class71_28.field570 = class63.field492 - i_11;
                              class71_28.field579 = class63.field499 - i_12;
                              class71_28.field572 = class226_10.field2676;
                              client.field849.method4888(class71_28);
                           }
                        }

                        if (bool_35 && class226_10.field2677 != null) {
                           class71_28 = new class71();
                           class71_28.field581 = true;
                           class71_28.field571 = class226_10;
                           class71_28.field570 = class63.field492 - i_11;
                           class71_28.field579 = class63.field499 - i_12;
                           class71_28.field572 = class226_10.field2677;
                           client.field847.method4888(class71_28);
                        }

                        if (!class226_10.field2717 && bool_34) {
                           class226_10.field2717 = true;
                           if (class226_10.field2678 != null) {
                              class71_28 = new class71();
                              class71_28.field581 = true;
                              class71_28.field571 = class226_10;
                              class71_28.field570 = class63.field492 - i_11;
                              class71_28.field579 = class63.field499 - i_12;
                              class71_28.field572 = class226_10.field2678;
                              client.field847.method4888(class71_28);
                           }
                        }

                        if (class226_10.field2717 && bool_34 && class226_10.field2618 != null) {
                           class71_28 = new class71();
                           class71_28.field581 = true;
                           class71_28.field571 = class226_10;
                           class71_28.field570 = class63.field492 - i_11;
                           class71_28.field579 = class63.field499 - i_12;
                           class71_28.field572 = class226_10.field2618;
                           client.field847.method4888(class71_28);
                        }

                        if (class226_10.field2717 && !bool_34) {
                           class226_10.field2717 = false;
                           if (class226_10.field2606 != null) {
                              class71_28 = new class71();
                              class71_28.field581 = true;
                              class71_28.field571 = class226_10;
                              class71_28.field570 = class63.field492 - i_11;
                              class71_28.field579 = class63.field499 - i_12;
                              class71_28.field572 = class226_10.field2606;
                              client.field849.method4888(class71_28);
                           }
                        }

                        if (class226_10.field2630 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2630;
                           client.field715.method4888(class71_28);
                        }

                        class71 class71_29;
                        int i_36;
                        int i_37;
                        if (class226_10.field2685 != null && client.field835 > class226_10.field2645) {
                           if (class226_10.field2661 != null && client.field835 - class226_10.field2645 <= 32) {
                              label890:
                              for (i_36 = class226_10.field2645; i_36 < client.field835; i_36++) {
                                 i_24 = client.field834[i_36 & 0x1f];

                                 for (i_37 = 0; i_37 < class226_10.field2661.length; i_37++) {
                                    if (i_24 == class226_10.field2661[i_37]) {
                                       class71_29 = new class71();
                                       class71_29.field571 = class226_10;
                                       class71_29.field572 = class226_10.field2685;
                                       client.field847.method4888(class71_29);
                                       break label890;
                                    }
                                 }
                              }
                           } else {
                              class71_28 = new class71();
                              class71_28.field571 = class226_10;
                              class71_28.field572 = class226_10.field2685;
                              client.field847.method4888(class71_28);
                           }

                           class226_10.field2645 = client.field835;
                        }

                        if (class226_10.field2632 != null && client.field837 > class226_10.field2721) {
                           if (class226_10.field2688 != null && client.field837 - class226_10.field2721 <= 32) {
                              label866:
                              for (i_36 = class226_10.field2721; i_36 < client.field837; i_36++) {
                                 i_24 = client.field875[i_36 & 0x1f];

                                 for (i_37 = 0; i_37 < class226_10.field2688.length; i_37++) {
                                    if (i_24 == class226_10.field2688[i_37]) {
                                       class71_29 = new class71();
                                       class71_29.field571 = class226_10;
                                       class71_29.field572 = class226_10.field2632;
                                       client.field847.method4888(class71_29);
                                       break label866;
                                    }
                                 }
                              }
                           } else {
                              class71_28 = new class71();
                              class71_28.field571 = class226_10;
                              class71_28.field572 = class226_10.field2632;
                              client.field847.method4888(class71_28);
                           }

                           class226_10.field2721 = client.field837;
                        }

                        if (class226_10.field2689 != null && client.field839 > class226_10.field2725) {
                           if (class226_10.field2690 != null && client.field839 - class226_10.field2725 <= 32) {
                              label842:
                              for (i_36 = class226_10.field2725; i_36 < client.field839; i_36++) {
                                 i_24 = client.field838[i_36 & 0x1f];

                                 for (i_37 = 0; i_37 < class226_10.field2690.length; i_37++) {
                                    if (i_24 == class226_10.field2690[i_37]) {
                                       class71_29 = new class71();
                                       class71_29.field571 = class226_10;
                                       class71_29.field572 = class226_10.field2689;
                                       client.field847.method4888(class71_29);
                                       break label842;
                                    }
                                 }
                              }
                           } else {
                              class71_28 = new class71();
                              class71_28.field571 = class226_10;
                              class71_28.field572 = class226_10.field2689;
                              client.field847.method4888(class71_28);
                           }

                           class226_10.field2725 = client.field839;
                        }

                        if (client.field840 > class226_10.field2719 && class226_10.field2581 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2581;
                           client.field847.method4888(class71_28);
                        }

                        if (client.field879 > class226_10.field2719 && class226_10.field2696 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2696;
                           client.field847.method4888(class71_28);
                        }

                        if (client.field842 > class226_10.field2719 && class226_10.field2697 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2697;
                           client.field847.method4888(class71_28);
                        }

                        if (client.field685 > class226_10.field2719 && class226_10.field2702 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2702;
                           client.field847.method4888(class71_28);
                        }

                        if (client.field680 > class226_10.field2719 && class226_10.field2703 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2703;
                           client.field847.method4888(class71_28);
                        }

                        if (client.field845 > class226_10.field2719 && class226_10.field2698 != null) {
                           class71_28 = new class71();
                           class71_28.field571 = class226_10;
                           class71_28.field572 = class226_10.field2698;
                           client.field847.method4888(class71_28);
                        }

                        class226_10.field2719 = client.field737;
                        if (class226_10.field2716 != null) {
                           for (i_36 = 0; i_36 < client.field870; i_36++) {
                              class71 class71_32 = new class71();
                              class71_32.field571 = class226_10;
                              class71_32.field576 = client.field693[i_36];
                              class71_32.field577 = client.field661[i_36];
                              class71_32.field572 = class226_10.field2716;
                              client.field847.method4888(class71_32);
                           }
                        }
                     }
                  }

                  if (!class226_10.field2585) {
                     if (client.field821 != null || class171.field2027 != null || client.field784) {
                        continue;
                     }

                     if ((class226_10.field2582 >= 0 || class226_10.field2666 != 0) && class63.field492 >= i_13 && class63.field499 >= i_14 && class63.field492 < i_15 && class63.field499 < i_16) {
                        if (class226_10.field2582 >= 0) {
                           class262.field3350 = arr_0[class226_10.field2582];
                        } else {
                           class262.field3350 = class226_10;
                        }
                     }

                     if (class226_10.field2588 == 8 && class63.field492 >= i_13 && class63.field499 >= i_14 && class63.field492 < i_15 && class63.field499 < i_16) {
                        class181.field2092 = class226_10;
                     }

                     if (class226_10.field2639 > class226_10.field2602) {
                        class269.method4764(class226_10, i_11 + class226_10.field2601, i_12, class226_10.field2602, class226_10.field2639, class63.field492, class63.field499, 1825223582);
                     }
                  }

                  if (class226_10.field2588 == 0) {
                     method2182(arr_0, class226_10.field2586, i_13, i_14, i_15, i_16, i_11 - class226_10.field2607, i_12 - class226_10.field2608, -2059122834);
                     if (class226_10.field2672 != null) {
                        method2182(class226_10.field2672, class226_10.field2586, i_13, i_14, i_15, i_16, i_11 - class226_10.field2607, i_12 - class226_10.field2608, -2097961998);
                     }

                     class70 class70_30 = (class70) client.field833.method5968((long)class226_10.field2586);
                     if (class70_30 != null) {
                        if (class70_30.field560 == 0 && class63.field492 >= i_13 && class63.field499 >= i_14 && class63.field492 < i_15 && class63.field499 < i_16 && !client.field784) {
                           for (class71 class71_31 = (class71) client.field847.method4879(); class71_31 != null; class71_31 = (class71) client.field847.method4884()) {
                              if (class71_31.field581) {
                                 class71_31.method3628();
                                 class71_31.field571.field2717 = false;
                              }
                           }

                           if (class65.field521 == 0) {
                              client.field821 = null;
                              client.field832 = null;
                           }

                           if (!client.field784) {
                              class74.method1301(1369394699);
                           }
                        }

                        class51.method816(class70_30.field563, i_13, i_14, i_15, i_16, i_11, i_12, (byte) 1);
                     }
                  }
               }
            }
         }
      }

   }

}
