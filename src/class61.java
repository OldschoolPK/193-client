import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

public final class class61 extends class332 {

   Image field447;
   Component field448;

   class61(int i_1, int i_2, Component component_3) {
      super.field3892 = i_1;
      super.field3893 = i_2;
      super.field3891 = new int[i_2 * i_1 + 1];
      DataBufferInt databufferint_4 = new DataBufferInt(super.field3891, super.field3891.length);
      DirectColorModel directcolormodel_5 = new DirectColorModel(32, 16711680, 65280, 255);
      WritableRaster writableraster_6 = Raster.createWritableRaster(directcolormodel_5.createCompatibleSampleModel(super.field3892, super.field3893), databufferint_4, (Point) null);
      this.field447 = new BufferedImage(directcolormodel_5, writableraster_6, false, new Hashtable());
      this.method960(component_3, (byte) 70);
      this.method6075(1374263653);
   }

   final void method952(Graphics graphics_1, int i_2, int i_3, int i_4, int i_5, int i_6) {
      try {
         Shape shape_7 = graphics_1.getClip();
         graphics_1.clipRect(i_2, i_3, i_4, i_5);
         graphics_1.drawImage(this.field447, 0, 0, this.field448);
         graphics_1.setClip(shape_7);
      } catch (Exception exception_8) {
         this.field448.repaint();
      }

   }

   final void method951(Graphics graphics_1, int i_2, int i_3, int i_4) {
      try {
         graphics_1.drawImage(this.field447, i_2, i_3, this.field448);
      } catch (Exception exception_6) {
         this.field448.repaint();
      }

   }

   final void method960(Component component_1, byte b_2) {
      this.field448 = component_1;
   }

   public final void vmethod6077(int i_1, int i_2, int i_3) {
      this.method951(this.field448.getGraphics(), i_1, i_2, 1396932744);
   }

   public final void vmethod6080(int i_1, int i_2, int i_3, int i_4, byte b_5) {
      this.method952(this.field448.getGraphics(), i_1, i_2, i_3, i_4, -1924846250);
   }

}
