public class class283 extends class289 {

   class291 field3624;
   class291 field3625;

   class283() {
      this.field3624 = class291.field3653;
      this.field3625 = class291.field3653;
   }

   void method5004(byte b_1) {
      this.field3624 = class173.field2036.field1067.method5073(super.field3628, (byte) 1) ? class291.field3652 : class291.field3654;
   }

   void method5007(byte b_1) {
      this.field3625 = class173.field2036.field1068.method5073(super.field3628, (byte) 1) ? class291.field3652 : class291.field3654;
   }

   void method5009(int i_1) {
      this.field3625 = class291.field3653;
   }

   void method5003(int i_1) {
      this.field3624 = class291.field3653;
   }

   public final boolean method5015(int i_1) {
      if (this.field3624 == class291.field3653) {
         this.method5004((byte) 62);
      }

      return this.field3624 == class291.field3652;
   }

   public final boolean method5006(int i_1) {
      if (this.field3625 == class291.field3653) {
         this.method5007((byte) -77);
      }

      return this.field3625 == class291.field3652;
   }

}
