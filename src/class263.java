public class class263 extends class184 {

   static class244 field3372;
   static class244 field3354;
   static class244 field3371;
   static class154 field3356 = new class154(64);
   static class154 field3357 = new class154(64);
   static class154 field3369 = new class154(20);
   int field3362 = -1;
   public int field3363 = 16777215;
   public int field3364 = 70;
   int field3365 = -1;
   int field3366 = -1;
   int field3355 = -1;
   int field3358 = -1;
   public int field3377 = 0;
   public int field3353 = 0;
   public int field3378 = -1;
   String field3370 = "";
   public int field3373 = -1;
   public int field3374 = 0;
   int field3376 = -1;
   int field3368 = -1;
   public int[] field3375;

   void method4547(class310 class310_1, int i_2, byte b_3) {
      if (i_2 == 1) {
         this.field3362 = class310_1.method5519((short) 1005);
      } else if (i_2 == 2) {
         this.field3363 = class310_1.method5506((short) 239);
      } else if (i_2 == 3) {
         this.field3365 = class310_1.method5519((short) 1005);
      } else if (i_2 == 4) {
         this.field3355 = class310_1.method5519((short) 1005);
      } else if (i_2 == 5) {
         this.field3366 = class310_1.method5519((short) 1005);
      } else if (i_2 == 6) {
         this.field3358 = class310_1.method5519((short) 1005);
      } else if (i_2 == 7) {
         this.field3377 = class310_1.method5505((short) 22225);
      } else if (i_2 == 8) {
         this.field3370 = class310_1.method5734(-519852136);
      } else if (i_2 == 9) {
         this.field3364 = class310_1.method5729(1387152151);
      } else if (i_2 == 10) {
         this.field3353 = class310_1.method5505((short) 9193);
      } else if (i_2 == 11) {
         this.field3378 = 0;
      } else if (i_2 == 12) {
         this.field3373 = class310_1.method5661((byte) -20);
      } else if (i_2 == 13) {
         this.field3374 = class310_1.method5505((short) 3945);
      } else if (i_2 == 14) {
         this.field3378 = class310_1.method5729(118086014);
      } else if (i_2 == 17 || i_2 == 18) {
         this.field3376 = class310_1.method5729(-130504611);
         if (this.field3376 == 65535) {
            this.field3376 = -1;
         }

         this.field3368 = class310_1.method5729(-1369595674);
         if (this.field3368 == 65535) {
            this.field3368 = -1;
         }

         int i_4 = -1;
         if (i_2 == 18) {
            i_4 = class310_1.method5729(-211456148);
            if (i_4 == 65535) {
               i_4 = -1;
            }
         }

         int i_5 = class310_1.method5661((byte) 80);
         this.field3375 = new int[i_5 + 2];

         for (int i_6 = 0; i_6 <= i_5; i_6++) {
            this.field3375[i_6] = class310_1.method5729(630178057);
            if (this.field3375[i_6] == 65535) {
               this.field3375[i_6] = -1;
            }
         }

         this.field3375[i_5 + 1] = i_4;
      }

   }

   void method4546(class310 class310_1, byte b_2) {
      while (true) {
         int i_3 = class310_1.method5661((byte) -46);
         if (i_3 == 0) {
            return;
         }

         this.method4547(class310_1, i_3, (byte) 19);
      }
   }

   public final class263 method4548(byte b_1) {
      int i_2 = -1;
      if (this.field3376 != -1) {
         i_2 = class222.method3999(this.field3376, -1151840113);
      } else if (this.field3368 != -1) {
         i_2 = class221.field2541[this.field3368];
      }

      int i_3;
      if (i_2 >= 0 && i_2 < this.field3375.length - 1) {
         i_3 = this.field3375[i_2];
      } else {
         i_3 = this.field3375[this.field3375.length - 1];
      }

      return i_3 != -1 ? class198.method3668(i_3, 1390109728) : null;
   }

   public class335 method4550(int i_1) {
      if (this.field3365 < 0) {
         return null;
      } else {
         class335 class335_2 = (class335) field3357.method3376((long)this.field3365);
         if (class335_2 != null) {
            return class335_2;
         } else {
            class335_2 = class230.method4115(field3354, this.field3365, 0, (byte) 62);
            if (class335_2 != null) {
               field3357.method3374(class335_2, (long)this.field3365);
            }

            return class335_2;
         }
      }
   }

   public class335 method4545(int i_1) {
      if (this.field3366 < 0) {
         return null;
      } else {
         class335 class335_2 = (class335) field3357.method3376((long)this.field3366);
         if (class335_2 != null) {
            return class335_2;
         } else {
            class335_2 = class230.method4115(field3354, this.field3366, 0, (byte) 3);
            if (class335_2 != null) {
               field3357.method3374(class335_2, (long)this.field3366);
            }

            return class335_2;
         }
      }
   }

   public class335 method4554(int i_1) {
      if (this.field3355 < 0) {
         return null;
      } else {
         class335 class335_2 = (class335) field3357.method3376((long)this.field3355);
         if (class335_2 != null) {
            return class335_2;
         } else {
            class335_2 = class230.method4115(field3354, this.field3355, 0, (byte) 78);
            if (class335_2 != null) {
               field3357.method3374(class335_2, (long)this.field3355);
            }

            return class335_2;
         }
      }
   }

   public class335 method4552(short s_1) {
      if (this.field3358 < 0) {
         return null;
      } else {
         class335 class335_2 = (class335) field3357.method3376((long)this.field3358);
         if (class335_2 != null) {
            return class335_2;
         } else {
            class335_2 = class230.method4115(field3354, this.field3358, 0, (byte) 20);
            if (class335_2 != null) {
               field3357.method3374(class335_2, (long)this.field3358);
            }

            return class335_2;
         }
      }
   }

   public class305 method4553(byte b_1) {
      if (this.field3362 == -1) {
         return null;
      } else {
         class305 class305_2 = (class305) field3369.method3376((long)this.field3362);
         if (class305_2 != null) {
            return class305_2;
         } else {
            class305_2 = class320.method5891(field3354, field3371, this.field3362, 0, 1224864347);
            if (class305_2 != null) {
               field3369.method3374(class305_2, (long)this.field3362);
            }

            return class305_2;
         }
      }
   }

   public String method4549(int i_1, int i_2) {
      String string_3 = this.field3370;

      while (true) {
         int i_4 = string_3.indexOf("%1");
         if (i_4 < 0) {
            return string_3;
         }

         string_3 = string_3.substring(0, i_4) + class257.method4470(i_1, false, (byte) 7) + string_3.substring(i_4 + 2);
      }
   }

   public static int method4579(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, byte b_6) {
      if ((i_5 & 0x1) == 1) {
         int i_7 = i_3;
         i_3 = i_4;
         i_4 = i_7;
      }

      i_2 &= 0x3;
      return i_2 == 0 ? i_0 : (i_2 == 1 ? i_1 : (i_2 == 2 ? 7 - i_0 - (i_3 - 1) : 7 - i_1 - (i_4 - 1)));
   }

}
