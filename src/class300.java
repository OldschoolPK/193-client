public class class300 {

   static char[] field3695 = new char[64];
   static char[] field3693;
   static char[] field3694;
   static int[] field3692;

   static {
      int i_0;
      for (i_0 = 0; i_0 < 26; i_0++) {
         field3695[i_0] = (char)(i_0 + 65);
      }

      for (i_0 = 26; i_0 < 52; i_0++) {
         field3695[i_0] = (char)(i_0 + 97 - 26);
      }

      for (i_0 = 52; i_0 < 62; i_0++) {
         field3695[i_0] = (char)(i_0 + 48 - 52);
      }

      field3695[62] = 43;
      field3695[63] = 47;
      field3693 = new char[64];

      for (i_0 = 0; i_0 < 26; i_0++) {
         field3693[i_0] = (char)(i_0 + 65);
      }

      for (i_0 = 26; i_0 < 52; i_0++) {
         field3693[i_0] = (char)(i_0 + 97 - 26);
      }

      for (i_0 = 52; i_0 < 62; i_0++) {
         field3693[i_0] = (char)(i_0 + 48 - 52);
      }

      field3693[62] = 42;
      field3693[63] = 45;
      field3694 = new char[64];

      for (i_0 = 0; i_0 < 26; i_0++) {
         field3694[i_0] = (char)(i_0 + 65);
      }

      for (i_0 = 26; i_0 < 52; i_0++) {
         field3694[i_0] = (char)(i_0 + 97 - 26);
      }

      for (i_0 = 52; i_0 < 62; i_0++) {
         field3694[i_0] = (char)(i_0 + 48 - 52);
      }

      field3694[62] = 45;
      field3694[63] = 95;
      field3692 = new int[128];

      for (i_0 = 0; i_0 < field3692.length; i_0++) {
         field3692[i_0] = -1;
      }

      for (i_0 = 65; i_0 <= 90; i_0++) {
         field3692[i_0] = i_0 - 65;
      }

      for (i_0 = 97; i_0 <= 122; i_0++) {
         field3692[i_0] = i_0 - 97 + 26;
      }

      for (i_0 = 48; i_0 <= 57; i_0++) {
         field3692[i_0] = i_0 - 48 + 52;
      }

      int[] ints_2 = field3692;
      field3692[43] = 62;
      ints_2[42] = 62;
      int[] ints_1 = field3692;
      field3692[47] = 63;
      ints_1[45] = 63;
   }

}
