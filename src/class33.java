import java.io.IOException;

public class class33 implements class46 {

   int field217;
   int field215;
   int field211;
   int field212;
   int field210;
   int field214;
   int field209;
   int field213;
   int field216;
   int field218;

   public boolean vmethod814(int i_1, int i_2, int i_3) {
      return i_1 >> 6 >= this.field209 && i_1 >> 6 <= this.field216 && i_2 >> 6 >= this.field213 && i_2 >> 6 <= this.field218;
   }

   public boolean vmethod813(int i_1, int i_2, int i_3, int i_4) {
      return i_1 >= this.field217 && i_1 < this.field215 + this.field217 ? i_2 >> 6 >= this.field211 && i_2 >> 6 <= this.field210 && i_3 >> 6 >= this.field212 && i_3 >> 6 <= this.field214 : false;
   }

   public void vmethod812(class35 class35_1, short s_2) {
      if (class35_1.field234 > this.field209) {
         class35_1.field234 = this.field209;
      }

      if (class35_1.field228 < this.field216) {
         class35_1.field228 = this.field216;
      }

      if (class35_1.field236 > this.field213) {
         class35_1.field236 = this.field213;
      }

      if (class35_1.field239 < this.field218) {
         class35_1.field239 = this.field218;
      }

   }

   public class222 vmethod835(int i_1, int i_2, byte b_3) {
      if (!this.vmethod814(i_1, i_2, 639074408)) {
         return null;
      } else {
         int i_4 = this.field211 * 64 - this.field209 * 64 + i_1;
         int i_5 = this.field212 * 64 - this.field213 * 64 + i_2;
         return new class222(this.field217, i_4, i_5);
      }
   }

   public int[] vmethod819(int i_1, int i_2, int i_3, int i_4) {
      if (!this.vmethod813(i_1, i_2, i_3, 1868721564)) {
         return null;
      } else {
         int[] ints_5 = new int[] {this.field209 * 64 - this.field211 * 64 + i_2, i_3 + (this.field213 * 64 - this.field212 * 64)};
         return ints_5;
      }
   }

   public void vmethod825(class310 class310_1, byte b_2) {
      this.field217 = class310_1.method5661((byte) 45);
      this.field215 = class310_1.method5661((byte) -80);
      this.field211 = class310_1.method5729(-1008343131);
      this.field212 = class310_1.method5729(-2129041655);
      this.field210 = class310_1.method5729(-103917105);
      this.field214 = class310_1.method5729(1445742500);
      this.field209 = class310_1.method5729(2083412529);
      this.field213 = class310_1.method5729(538381966);
      this.field216 = class310_1.method5729(1145195661);
      this.field218 = class310_1.method5729(-1674956649);
      this.method364((byte) -44);
   }

   void method364(byte b_1) {
   }

   public static boolean method378(int i_0, int i_1) {
      return (i_0 >> 28 & 0x1) != 0;
   }

   static final void method377(boolean bool_0, int i_1) {
      class302.method5318((byte) 8);
      ++client.field694.field1334;
      if (client.field694.field1334 >= 50 || bool_0) {
         client.field694.field1334 = 0;
         if (!client.field696 && client.field694.method2299(-51701120) != null) {
            class196 class196_2 = class68.method1249(class192.field2322, client.field694.field1328, (byte) 1);
            client.field694.method2295(class196_2, 929497290);

            try {
               client.field694.method2298(-1507185593);
            } catch (IOException ioexception_4) {
               client.field696 = true;
            }
         }

      }
   }

}
