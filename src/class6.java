import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class class6 implements class11 {

   final MessageDigest field20 = this.method68();

   class6(class10 class10_1) {
   }

   byte[] method67(String string_1, long long_2) {
      StringBuilder stringbuilder_4 = new StringBuilder();
      stringbuilder_4.append(string_1).append(Long.toHexString(long_2));
      this.field20.reset();

      try {
         this.field20.update(stringbuilder_4.toString().getBytes("UTF-8"));
      } catch (UnsupportedEncodingException unsupportedencodingexception_6) {
         unsupportedencodingexception_6.printStackTrace();
      }

      return this.field20.digest();
   }

   MessageDigest method68() {
      try {
         return MessageDigest.getInstance("SHA-256");
      } catch (NoSuchAlgorithmException nosuchalgorithmexception_2) {
         nosuchalgorithmexception_2.printStackTrace();
         return null;
      }
   }

   boolean method64(int i_1, String string_2, long long_3) {
      byte[] bytes_5 = this.method67(string_2, long_3);
      return method65(bytes_5) >= i_1;
   }

   static int method65(byte[] bytes_0) {
      int i_1 = 0;
      byte[] bytes_2 = bytes_0;

      for (int i_3 = 0; i_3 < bytes_2.length; i_3++) {
         byte b_4 = bytes_2[i_3];
         int i_5 = method74(b_4);
         i_1 += i_5;
         if (i_5 != 8) {
            break;
         }
      }

      return i_1;
   }

   static int method74(byte b_0) {
      int i_1 = 0;
      if (b_0 == 0) {
         i_1 = 8;
      } else {
         for (int i_2 = b_0 & 0xff; (i_2 & 0x80) == 0; i_2 <<= 1) {
            ++i_1;
         }
      }

      return i_1;
   }

}
