public class class162 extends class285 {

   final boolean field1993;

   public class162(boolean bool_1) {
      this.field1993 = bool_1;
   }

   int method3453(class289 class289_1, class289 class289_2, int i_3) {
      if (client.field645 == class289_1.field3650) {
         if (class289_2.field3650 != client.field645) {
            return this.field1993 ? -1 : 1;
         }
      } else if (class289_2.field3650 == client.field645) {
         return this.field1993 ? 1 : -1;
      }

      return this.method5051(class289_1, class289_2, 466664807);
   }

   public int compare(Object object_1, Object object_2) {
      return this.method3453((class289) object_1, (class289) object_2, -617956062);
   }

   static void method3457(int i_0, byte b_1) {
      if (i_0 == -3) {
         class62.method1087("Connection timed out.", "Please try using a different world.", "", -956419612);
      } else if (i_0 == -2) {
         class62.method1087("Error connecting to server.", "Please try using a different world.", "", -1545073597);
      } else if (i_0 == -1) {
         class62.method1087("No response from server.", "Please try using a different world.", "", -1534700105);
      } else if (i_0 == 3) {
         class94.field1191 = 3;
         class94.field1190 = 1;
      } else if (i_0 == 4) {
         class94.field1191 = 12;
         class94.field1199 = 0;
      } else if (i_0 == 5) {
         class94.field1190 = 2;
         class62.method1087("Your account has not logged out from its last", "session or the server is too busy right now.", "Please try again in a few minutes.", -1830299146);
      } else if (i_0 == 68 || !client.field652 && i_0 == 6) {
         class62.method1087("RuneScape has been updated!", "Please reload this page.", "", -1417433315);
      } else if (i_0 == 7) {
         class62.method1087("This world is full.", "Please use a different world.", "", -1935619393);
      } else if (i_0 == 8) {
         class62.method1087("Unable to connect.", "Login server offline.", "", -834607856);
      } else if (i_0 == 9) {
         class62.method1087("Login limit exceeded.", "Too many connections from your address.", "", -1308959876);
      } else if (i_0 == 10) {
         class62.method1087("Unable to connect.", "Bad session id.", "", -1849570876);
      } else if (i_0 == 11) {
         class62.method1087("We suspect someone knows your password.", "Press \'change your password\' on front page.", "", -1849449020);
      } else if (i_0 == 12) {
         class62.method1087("You need a members account to login to this world.", "Please subscribe, or use a different world.", "", -1094322788);
      } else if (i_0 == 13) {
         class62.method1087("Could not complete login.", "Please try using a different world.", "", -1433921379);
      } else if (i_0 == 14) {
         class62.method1087("The server is being updated.", "Please wait 1 minute and try again.", "", -1269407435);
      } else if (i_0 == 16) {
         class62.method1087("Too many login attempts.", "Please wait a few minutes before trying again.", "", -1853596797);
      } else if (i_0 == 17) {
         class62.method1087("You are standing in a members-only area.", "To play on this world move to a free area first", "", -1871968523);
      } else if (i_0 == 18) {
         class94.field1191 = 12;
         class94.field1199 = 1;
      } else if (i_0 == 19) {
         class62.method1087("This world is running a closed Beta.", "Sorry invited players only.", "Please use a different world.", -1013991546);
      } else if (i_0 == 20) {
         class62.method1087("Invalid loginserver requested.", "Please try using a different world.", "", -1904473010);
      } else if (i_0 == 22) {
         class62.method1087("Malformed login packet.", "Please try again.", "", -1557202918);
      } else if (i_0 == 23) {
         class62.method1087("No reply from loginserver.", "Please wait 1 minute and try again.", "", -722087138);
      } else if (i_0 == 24) {
         class62.method1087("Error loading your profile.", "Please contact customer support.", "", -2040271528);
      } else if (i_0 == 25) {
         class62.method1087("Unexpected loginserver response.", "Please try using a different world.", "", -1567309197);
      } else if (i_0 == 26) {
         class62.method1087("This computers address has been blocked", "as it was used to break our rules.", "", -768103709);
      } else if (i_0 == 27) {
         class62.method1087("", "Service unavailable.", "", -1425786163);
      } else if (i_0 == 31) {
         class62.method1087("Your account must have a displayname set", "in order to play the game.  Please set it", "via the website, or the main game.", -850824590);
      } else if (i_0 == 32) {
         class62.method1087("Your attempt to log into your account was", "unsuccessful.  Don\'t worry, you can sort", "this out by visiting the billing system.", -1492995378);
      } else if (i_0 == 37) {
         class62.method1087("Your account is currently inaccessible.", "Please try again in a few minutes.", "", -1626636894);
      } else if (i_0 == 38) {
         class62.method1087("You need to vote to play!", "Visit runescape.com and vote,", "and then come back here!", -1075476177);
      } else if (i_0 == 55) {
         class94.field1191 = 8;
      } else {
         if (i_0 == 56) {
            class62.method1087("Enter the 6-digit code generated by your", "authenticator app.", "", -2051936119);
            class44.method663(11, -1709930141);
            return;
         }

         if (i_0 == 57) {
            class62.method1087("The code you entered was incorrect.", "Please try again.", "", -1782000796);
            class44.method663(11, -2033406749);
            return;
         }

         if (i_0 == 61) {
            class94.field1191 = 7;
         } else {
            class62.method1087("Unexpected server response", "Please try using a different world.", "", -774274854);
         }
      }

      class44.method663(10, -1366945712);
   }

}
