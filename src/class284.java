public class class284 implements Comparable {

   class293 field3628;
   class293 field3627;

   public int vmethod5207(class284 class284_1, int i_2) {
      return this.field3628.method5193(class284_1.field3628, -1152802722);
   }

   public class293 method5038(int i_1) {
      return this.field3628;
   }

   void method5033(class293 class293_1, class293 class293_2, int i_3) {
      if (class293_1 == null) {
         throw new NullPointerException();
      } else {
         this.field3628 = class293_1;
         this.field3627 = class293_2;
      }
   }

   public String method5025(short s_1) {
      return this.field3628 == null ? "" : this.field3628.method5186(-1714059563);
   }

   public String method5026(int i_1) {
      return this.field3627 == null ? "" : this.field3627.method5186(-1887543843);
   }

   public int compareTo(Object object_1) {
      return this.vmethod5207((class284) object_1, -1545516578);
   }

}
