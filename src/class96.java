import java.util.Iterator;

public final class class96 extends class144 {

   int field1238;
   int field1239;

   protected final class136 vmethod3305(int i_1) {
      return class66.method1223(this.field1238, -1569528030).method4633(this.field1239, 269128271);
   }

   public static class254 method2160(int i_0, byte b_1) {
      class254 class254_2 = (class254) class254.field3281.method3376((long)i_0);
      if (class254_2 != null) {
         return class254_2;
      } else {
         byte[] bytes_3 = class254.field3285.method4160(13, i_0, (short) -18693);
         class254_2 = new class254();
         class254_2.field3274 = i_0;
         if (bytes_3 != null) {
            class254_2.method4397(new class310(bytes_3), -1477796181);
         }

         class254.field3281.method3374(class254_2, (long)i_0);
         return class254_2;
      }
   }

   static void method2156(class226 class226_0, int i_1, int i_2, boolean bool_3, byte b_4) {
      int i_5 = class226_0.field2601;
      int i_6 = class226_0.field2602;
      if (class226_0.field2593 == 0) {
         class226_0.field2601 = class226_0.field2597;
      } else if (class226_0.field2593 == 1) {
         class226_0.field2601 = i_1 - class226_0.field2597;
      } else if (class226_0.field2593 == 2) {
         class226_0.field2601 = class226_0.field2597 * i_1 >> 14;
      }

      if (class226_0.field2594 == 0) {
         class226_0.field2602 = class226_0.field2598;
      } else if (class226_0.field2594 == 1) {
         class226_0.field2602 = i_2 - class226_0.field2598;
      } else if (class226_0.field2594 == 2) {
         class226_0.field2602 = i_2 * class226_0.field2598 >> 14;
      }

      if (class226_0.field2593 == 4) {
         class226_0.field2601 = class226_0.field2602 * class226_0.field2660 / class226_0.field2604;
      }

      if (class226_0.field2594 == 4) {
         class226_0.field2602 = class226_0.field2604 * class226_0.field2601 / class226_0.field2660;
      }

      if (class226_0.field2695 == 1337) {
         client.field869 = class226_0;
      }

      if (bool_3 && class226_0.field2701 != null && (i_5 != class226_0.field2601 || i_6 != class226_0.field2602)) {
         class71 class71_7 = new class71();
         class71_7.field571 = class226_0;
         class71_7.field572 = class226_0.field2701;
         client.field847.method4888(class71_7);
      }

   }

   static void method2161(int i_0) {
      Iterator iterator_1 = class100.field1285.iterator();

      while (iterator_1.hasNext()) {
         class73 class73_2 = (class73) iterator_1.next();
         class73_2.method1269((byte) 4);
      }

   }

   public static void method2159(int i_0) {
      class265.field3437.method3375();
      class265.field3450.method3375();
      class265.field3440.method3375();
   }

   static class101 method2158(int i_0, int i_1, int i_2, short s_3) {
      int i_4 = (i_1 << 8) + i_0;
      class101 class101_7 = (class101) class101.field1299.method3376((long)(i_4 << 16));
      class101 class101_6;
      if (class101_7 != null) {
         class101_6 = class101_7;
      } else {
         String string_8 = String.valueOf(i_4);
         int i_9 = class196.field2361.method4206(string_8, -1994801348);
         if (i_9 == -1) {
            class101_6 = null;
         } else {
            label56: {
               byte[] bytes_10 = class196.field2361.method4167(i_9, 1758855003);
               if (bytes_10 != null) {
                  if (bytes_10.length <= 1) {
                     class101_6 = null;
                     break label56;
                  }

                  class101_7 = class78.method1788(bytes_10, (byte) 0);
                  if (class101_7 != null) {
                     class101.field1299.method3374(class101_7, (long)(i_4 << 16));
                     class101_6 = class101_7;
                     break label56;
                  }
               }

               class101_6 = null;
            }
         }
      }

      if (class101_6 != null) {
         return class101_6;
      } else {
         int i_5 = class211.method3923(i_2, i_0, (byte) -2);
         class101 class101_13 = (class101) class101.field1299.method3376((long)(i_5 << 16));
         class101 class101_14;
         if (class101_13 != null) {
            class101_14 = class101_13;
         } else {
            String string_15 = String.valueOf(i_5);
            int i_11 = class196.field2361.method4206(string_15, -2015120641);
            if (i_11 == -1) {
               class101_14 = null;
            } else {
               byte[] bytes_12 = class196.field2361.method4167(i_11, 1414515638);
               if (bytes_12 != null) {
                  if (bytes_12.length <= 1) {
                     class101_14 = null;
                     return class101_14 != null ? class101_14 : null;
                  }

                  class101_13 = class78.method1788(bytes_12, (byte) 0);
                  if (class101_13 != null) {
                     class101.field1299.method3374(class101_13, (long)(i_5 << 16));
                     class101_14 = class101_13;
                     return class101_14 != null ? class101_14 : null;
                  }
               }

               class101_14 = null;
            }
         }

         return class101_14 != null ? class101_14 : null;
      }
   }

   static final void method2163(class267 class267_0, int i_1, int i_2, int i_3, short s_4) {
      if (client.field785 < 400) {
         if (class267_0.field3524 != null) {
            class267_0 = class267_0.method4731((byte) -109);
         }

         if (class267_0 != null) {
            if (class267_0.field3527) {
               if (!class267_0.field3529 || client.field818 == i_1) {
                  String string_5 = class267_0.field3499;
                  int i_8;
                  int i_9;
                  if (class267_0.field3516 != 0) {
                     i_8 = class267_0.field3516;
                     i_9 = class223.field2562.field611;
                     int i_10 = i_9 - i_8;
                     String string_7;
                     if (i_10 < -9) {
                        string_7 = class23.method222(16711680, (byte) 91);
                     } else if (i_10 < -6) {
                        string_7 = class23.method222(16723968, (byte) 69);
                     } else if (i_10 < -3) {
                        string_7 = class23.method222(16740352, (byte) 14);
                     } else if (i_10 < 0) {
                        string_7 = class23.method222(16756736, (byte) 86);
                     } else if (i_10 > 9) {
                        string_7 = class23.method222(65280, (byte) 78);
                     } else if (i_10 > 6) {
                        string_7 = class23.method222(4259584, (byte) 109);
                     } else if (i_10 > 3) {
                        string_7 = class23.method222(8453888, (byte) 76);
                     } else if (i_10 > 0) {
                        string_7 = class23.method222(12648192, (byte) 105);
                     } else {
                        string_7 = class23.method222(16776960, (byte) 103);
                     }

                     string_5 = string_5 + string_7 + " " + " (" + "level-" + class267_0.field3516 + ")";
                  }

                  if (class267_0.field3529 && client.field793) {
                     class38.method564("Examine", class23.method222(16776960, (byte) 54) + string_5, 1003, i_1, i_2, i_3, -1071513001);
                  }

                  if (client.field801 == 1) {
                     class38.method564("Use", client.field744 + " " + "->" + " " + class23.method222(16776960, (byte) 109) + string_5, 7, i_1, i_2, i_3, -2015295351);
                  } else if (client.field803) {
                     if ((class75.field637 & 0x2) == 2) {
                        class38.method564(client.field851, client.field807 + " " + "->" + " " + class23.method222(16776960, (byte) 125) + string_5, 8, i_1, i_2, i_3, -1970771379);
                     }
                  } else {
                     int i_11 = class267_0.field3529 && client.field793 ? 2000 : 0;
                     String[] arr_12 = class267_0.field3528;
                     if (arr_12 != null) {
                        for (i_8 = 4; i_8 >= 0; --i_8) {
                           if (arr_12[i_8] != null && !arr_12[i_8].equalsIgnoreCase("Attack")) {
                              i_9 = 0;
                              if (i_8 == 0) {
                                 i_9 = i_11 + 9;
                              }

                              if (i_8 == 1) {
                                 i_9 = i_11 + 10;
                              }

                              if (i_8 == 2) {
                                 i_9 = i_11 + 11;
                              }

                              if (i_8 == 3) {
                                 i_9 = i_11 + 12;
                              }

                              if (i_8 == 4) {
                                 i_9 = i_11 + 13;
                              }

                              class38.method564(arr_12[i_8], class23.method222(16776960, (byte) 51) + string_5, i_9, i_1, i_2, i_3, -1237131546);
                           }
                        }
                     }

                     if (arr_12 != null) {
                        for (i_8 = 4; i_8 >= 0; --i_8) {
                           if (arr_12[i_8] != null && arr_12[i_8].equalsIgnoreCase("Attack")) {
                              short s_13 = 0;
                              if (class93.field1177 != client.field672) {
                                 if (client.field672 == class93.field1175 || class93.field1176 == client.field672 && class267_0.field3516 > class223.field2562.field611) {
                                    s_13 = 2000;
                                 }

                                 i_9 = 0;
                                 if (i_8 == 0) {
                                    i_9 = s_13 + 9;
                                 }

                                 if (i_8 == 1) {
                                    i_9 = s_13 + 10;
                                 }

                                 if (i_8 == 2) {
                                    i_9 = s_13 + 11;
                                 }

                                 if (i_8 == 3) {
                                    i_9 = s_13 + 12;
                                 }

                                 if (i_8 == 4) {
                                    i_9 = s_13 + 13;
                                 }

                                 class38.method564(arr_12[i_8], class23.method222(16776960, (byte) 69) + string_5, i_9, i_1, i_2, i_3, -1832437547);
                              }
                           }
                        }
                     }

                     if (!class267_0.field3529 || !client.field793) {
                        class38.method564("Examine", class23.method222(16776960, (byte) 93) + string_5, 1003, i_1, i_2, i_3, -2040210615);
                     }
                  }

               }
            }
         }
      }
   }

}
