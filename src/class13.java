import java.applet.Applet;
import java.net.URL;
//import netscape.javascript.JSObject;

public final class class13 {

   static void method133(int i_0, int i_1) {
      client.field861 = 0L;
      if (i_0 >= 2) {
         client.field668 = true;
      } else {
         client.field668 = false;
      }

      if (class116.method2537((byte) 93) == 1) {
         class27.field147.method967(765, 503, -387026818);
      } else {
         class27.field147.method967(7680, 2160, -1768988970);
      }

      if (client.field653 >= 25) {
         class72.method1264(-2045975634);
      }

   }

   static boolean method131(String string_0, int i_1, String string_2, byte b_3) {
      if (i_1 == 0) {
         try {
            if (!class60.field439.startsWith("win")) {
               throw new Exception();
            } else if (!string_0.startsWith("http://") && !string_0.startsWith("https://")) {
               throw new Exception();
            } else {
               String str_14 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?&=,.%+-_#:/*";

               for (int i_5 = 0; i_5 < string_0.length(); i_5++) {
                  if (str_14.indexOf(string_0.charAt(i_5)) == -1) {
                     throw new Exception();
                  }
               }

               Runtime.getRuntime().exec("cmd /c start \"j\" \"" + string_0 + "\"");
               return true;
            }
         } catch (Throwable throwable_9) {
            return false;
         }
      } else if (i_1 == 1) {
         try {
            Applet applet_8 = class60.field440;
            Object[] arr_6 = new Object[] {(new URL(class60.field440.getCodeBase(), string_0)).toString()};
            //Object object_4 = JSObject.getWindow(applet_8).call(string_2, arr_6);
            return false;
         } catch (Throwable throwable_10) {
            return false;
         }
      } else if (i_1 == 2) {
         try {
            class60.field440.getAppletContext().showDocument(new URL(class60.field440.getCodeBase(), string_0), "_blank");
            return true;
         } catch (Exception exception_11) {
            return false;
         }
      } else if (i_1 == 3) {
         try {
            class56.method901(class60.field440, "loggedout", (byte) 16);
         } catch (Throwable throwable_13) {
            ;
         }

         try {
            class60.field440.getAppletContext().showDocument(new URL(class60.field440.getCodeBase(), string_0), "_top");
            return true;
         } catch (Exception exception_12) {
            return false;
         }
      } else {
         throw new IllegalArgumentException();
      }
   }

   public static void method129(class244 class244_0, byte b_1) {
      class260.field3334 = class244_0;
   }

   static final void method130(class226 class226_0, int i_1, int i_2, int i_3) {
      if (client.field821 == null && !client.field784) {
         if (class226_0 != null && class75.method1351(class226_0, 1020097317) != null) {
            client.field821 = class226_0;
            client.field832 = class75.method1351(class226_0, -562198437);
            client.field823 = i_1;
            client.field809 = i_2;
            class65.field521 = 0;
            client.field800 = false;
            int i_4 = class36.method463(646610765);
            if (i_4 != -1) {
               class42.field308 = new class91();
               class42.field308.field1164 = client.field786[i_4];
               class42.field308.field1161 = client.field787[i_4];
               class42.field308.field1162 = client.field844[i_4];
               class42.field308.field1160 = client.field789[i_4];
               class42.field308.field1163 = client.field790[i_4];
            }

         }
      }
   }

   static int method132(int i_0) {
      if (client.field916 != null && client.field922 < client.field916.size()) {
         int i_1 = 0;

         for (int i_2 = 0; i_2 <= client.field922; i_2++) {
            i_1 += ((class67) client.field916.get(i_2)).field537;
         }

         return i_1 * 10000 / client.field666;
      } else {
         return 10000;
      }
   }

}
