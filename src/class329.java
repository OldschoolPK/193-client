public class class329 {

   public static class244 field3877;
   public int field3881;
   public int field3879;
   public int field3878;
   public int field3880;

   public class329(int i_1, int i_2, int i_3, int i_4) {
      this.method5969(i_1, i_2, -1764840928);
      this.method5970(i_3, i_4, -1587228886);
   }

   public class329(int i_1, int i_2) {
      this(0, 0, i_1, i_2);
   }

   void method5972(class329 class329_1, class329 class329_2, int i_3) {
      class329_2.field3881 = this.field3881;
      class329_2.field3879 = this.field3879;
      if (this.field3881 < class329_1.field3881) {
         class329_2.field3879 -= class329_1.field3881 - this.field3881;
         class329_2.field3881 = class329_1.field3881;
      }

      if (class329_2.method5974(-1901002144) > class329_1.method5974(-1520412655)) {
         class329_2.field3879 -= class329_2.method5974(-1503053615) - class329_1.method5974(-1610015340);
      }

      if (class329_2.field3879 < 0) {
         class329_2.field3879 = 0;
      }

   }

   public void method5969(int i_1, int i_2, int i_3) {
      this.field3881 = i_1;
      this.field3878 = i_2;
   }

   void method5979(class329 class329_1, class329 class329_2, int i_3) {
      class329_2.field3878 = this.field3878;
      class329_2.field3880 = this.field3880;
      if (this.field3878 < class329_1.field3878) {
         class329_2.field3880 -= class329_1.field3878 - this.field3878;
         class329_2.field3878 = class329_1.field3878;
      }

      if (class329_2.method5975(924077669) > class329_1.method5975(1192379454)) {
         class329_2.field3880 -= class329_2.method5975(-1711202372) - class329_1.method5975(-448563963);
      }

      if (class329_2.field3880 < 0) {
         class329_2.field3880 = 0;
      }

   }

   public void method5970(int i_1, int i_2, int i_3) {
      this.field3879 = i_1;
      this.field3880 = i_2;
   }

   int method5974(int i_1) {
      return this.field3879 + this.field3881;
   }

   int method5975(int i_1) {
      return this.field3878 + this.field3880;
   }

   public void method5971(class329 class329_1, class329 class329_2, int i_3) {
      this.method5972(class329_1, class329_2, -1678302488);
      this.method5979(class329_1, class329_2, 394854704);
   }

   public String toString() {
      return null;
   }

   public String abd() {
      return null;
   }

   public String aba() {
      return null;
   }

}
