public class class331 extends class184 {

   public static int field3887 = 0;
   public static int field3888 = 0;
   public static int field3890 = 0;
   public static int field3884 = 0;
   public static int[] field3886;
   public static int field3889;
   public static int field3885;

   public static void method5998(int[] ints_0) {
      ints_0[0] = field3890;
      ints_0[1] = field3887;
      ints_0[2] = field3884;
      ints_0[3] = field3888;
   }

   public static void method5999(int[] ints_0, int i_1, int i_2) {
      field3886 = ints_0;
      field3889 = i_1;
      field3885 = i_2;
      method5996(0, 0, i_1, i_2);
   }

   public static void method6023(int i_0, int i_1, int i_2, int i_3) {
      if (i_0 >= field3890 && i_0 < field3884) {
         if (i_1 < field3887) {
            i_2 -= field3887 - i_1;
            i_1 = field3887;
         }

         if (i_2 + i_1 > field3888) {
            i_2 = field3888 - i_1;
         }

         int i_4 = i_0 + field3889 * i_1;

         for (int i_5 = 0; i_5 < i_2; i_5++) {
            field3886[i_4 + i_5 * field3889] = i_3;
         }

      }
   }

   public static void method6004(int i_0, int i_1, int i_2, int i_3, int i_4) {
      if (i_0 < field3890) {
         i_2 -= field3890 - i_0;
         i_0 = field3890;
      }

      if (i_1 < field3887) {
         i_3 -= field3887 - i_1;
         i_1 = field3887;
      }

      if (i_0 + i_2 > field3884) {
         i_2 = field3884 - i_0;
      }

      if (i_3 + i_1 > field3888) {
         i_3 = field3888 - i_1;
      }

      int i_5 = field3889 - i_2;
      int i_6 = i_0 + field3889 * i_1;

      for (int i_7 = -i_3; i_7 < 0; i_7++) {
         for (int i_8 = -i_2; i_8 < 0; i_8++) {
            field3886[i_6++] = i_4;
         }

         i_6 += i_5;
      }

   }

   public static void method6052(int i_0, int i_1, int i_2, int i_3) {
      if (i_1 >= field3887 && i_1 < field3888) {
         if (i_0 < field3890) {
            i_2 -= field3890 - i_0;
            i_0 = field3890;
         }

         if (i_0 + i_2 > field3884) {
            i_2 = field3884 - i_0;
         }

         int i_4 = i_0 + field3889 * i_1;

         for (int i_5 = 0; i_5 < i_2; i_5++) {
            field3886[i_4 + i_5] = i_3;
         }

      }
   }

   public static void method5996(int i_0, int i_1, int i_2, int i_3) {
      if (i_0 < 0) {
         i_0 = 0;
      }

      if (i_1 < 0) {
         i_1 = 0;
      }

      if (i_2 > field3889) {
         i_2 = field3889;
      }

      if (i_3 > field3885) {
         i_3 = field3885;
      }

      field3890 = i_0;
      field3887 = i_1;
      field3884 = i_2;
      field3888 = i_3;
   }

   public static void method6044(int i_0, int i_1, int i_2, int i_3, int i_4) {
      if (i_4 != 0) {
         if (i_4 == 256) {
            method6030(i_0, i_1, i_2, i_3);
         } else {
            if (i_2 < 0) {
               i_2 = -i_2;
            }

            int i_5 = 256 - i_4;
            int i_6 = (i_3 >> 16 & 0xff) * i_4;
            int i_7 = (i_3 >> 8 & 0xff) * i_4;
            int i_8 = i_4 * (i_3 & 0xff);
            int i_12 = i_1 - i_2;
            if (i_12 < field3887) {
               i_12 = field3887;
            }

            int i_13 = i_2 + i_1 + 1;
            if (i_13 > field3888) {
               i_13 = field3888;
            }

            int i_14 = i_12;
            int i_15 = i_2 * i_2;
            int i_16 = 0;
            int i_17 = i_1 - i_12;
            int i_18 = i_17 * i_17;
            int i_19 = i_18 - i_17;
            if (i_1 > i_13) {
               i_1 = i_13;
            }

            int i_9;
            int i_10;
            int i_11;
            int i_20;
            int i_21;
            int i_22;
            int i_23;
            int i_24;
            while (i_14 < i_1) {
               while (i_19 <= i_15 || i_18 <= i_15) {
                  i_18 = i_18 + i_16 + i_16;
                  i_19 += i_16++ + i_16;
               }

               i_20 = i_0 - i_16 + 1;
               if (i_20 < field3890) {
                  i_20 = field3890;
               }

               i_21 = i_0 + i_16;
               if (i_21 > field3884) {
                  i_21 = field3884;
               }

               i_22 = i_20 + i_14 * field3889;

               for (i_23 = i_20; i_23 < i_21; i_23++) {
                  i_9 = i_5 * (field3886[i_22] >> 16 & 0xff);
                  i_10 = (field3886[i_22] >> 8 & 0xff) * i_5;
                  i_11 = i_5 * (field3886[i_22] & 0xff);
                  i_24 = (i_8 + i_11 >> 8) + (i_6 + i_9 >> 8 << 16) + (i_7 + i_10 >> 8 << 8);
                  field3886[i_22++] = i_24;
               }

               ++i_14;
               i_18 -= i_17-- + i_17;
               i_19 -= i_17 + i_17;
            }

            i_16 = i_2;
            i_17 = -i_17;
            i_19 = i_15 + i_17 * i_17;
            i_18 = i_19 - i_2;

            for (i_19 -= i_17; i_14 < i_13; i_18 += i_17++ + i_17) {
               while (i_19 > i_15 && i_18 > i_15) {
                  i_19 -= i_16-- + i_16;
                  i_18 -= i_16 + i_16;
               }

               i_20 = i_0 - i_16;
               if (i_20 < field3890) {
                  i_20 = field3890;
               }

               i_21 = i_0 + i_16;
               if (i_21 > field3884 - 1) {
                  i_21 = field3884 - 1;
               }

               i_22 = i_20 + i_14 * field3889;

               for (i_23 = i_20; i_23 <= i_21; i_23++) {
                  i_9 = i_5 * (field3886[i_22] >> 16 & 0xff);
                  i_10 = (field3886[i_22] >> 8 & 0xff) * i_5;
                  i_11 = i_5 * (field3886[i_22] & 0xff);
                  i_24 = (i_8 + i_11 >> 8) + (i_6 + i_9 >> 8 << 16) + (i_7 + i_10 >> 8 << 8);
                  field3886[i_22++] = i_24;
               }

               ++i_14;
               i_19 = i_19 + i_17 + i_17;
            }

         }
      }
   }

   public static void method5994(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, byte[] bytes_6, int i_7, boolean bool_8) {
      if (i_0 + i_2 >= 0 && i_3 + i_1 >= 0) {
         if (i_0 < field3889 && i_1 < field3885) {
            int i_9 = 0;
            int i_10 = 0;
            if (i_0 < 0) {
               i_9 -= i_0;
               i_2 += i_0;
            }

            if (i_1 < 0) {
               i_10 -= i_1;
               i_3 += i_1;
            }

            if (i_0 + i_2 > field3889) {
               i_2 = field3889 - i_0;
            }

            if (i_3 + i_1 > field3885) {
               i_3 = field3885 - i_1;
            }

            int i_11 = bytes_6.length / i_7;
            int i_12 = field3889 - i_2;
            int i_13 = i_4 >>> 24;
            int i_14 = i_5 >>> 24;
            int i_15;
            int i_16;
            int i_17;
            int i_18;
            int i_19;
            if (i_13 == 255 && i_14 == 255) {
               i_15 = i_0 + i_9 + (i_10 + i_1) * field3889;

               for (i_16 = i_10 + i_1; i_16 < i_3 + i_10 + i_1; i_16++) {
                  for (i_17 = i_0 + i_9; i_17 < i_0 + i_9 + i_2; i_17++) {
                     i_18 = (i_16 - i_1) % i_11;
                     i_19 = (i_17 - i_0) % i_7;
                     if (bytes_6[i_19 + i_18 * i_7] != 0) {
                        field3886[i_15++] = i_5;
                     } else {
                        field3886[i_15++] = i_4;
                     }
                  }

                  i_15 += i_12;
               }
            } else {
               i_15 = i_0 + i_9 + (i_10 + i_1) * field3889;

               for (i_16 = i_10 + i_1; i_16 < i_3 + i_10 + i_1; i_16++) {
                  for (i_17 = i_0 + i_9; i_17 < i_0 + i_9 + i_2; i_17++) {
                     i_18 = (i_16 - i_1) % i_11;
                     i_19 = (i_17 - i_0) % i_7;
                     int i_20 = i_4;
                     if (bytes_6[i_19 + i_18 * i_7] != 0) {
                        i_20 = i_5;
                     }

                     int i_21 = i_20 >>> 24;
                     int i_22 = 255 - i_21;
                     int i_23 = field3886[i_15];
                     int i_24 = ((i_20 & 0xff00ff) * i_21 + (i_23 & 0xff00ff) * i_22 & ~0xff00ff) + (i_21 * (i_20 & 0xff00) + i_22 * (i_23 & 0xff00) & 0xff0000) >> 8;
                     field3886[i_15++] = i_24;
                  }

                  i_15 += i_12;
               }
            }

         }
      }
   }

   static void method6030(int i_0, int i_1, int i_2, int i_3) {
      if (i_2 == 0) {
         method6015(i_0, i_1, i_3);
      } else {
         if (i_2 < 0) {
            i_2 = -i_2;
         }

         int i_4 = i_1 - i_2;
         if (i_4 < field3887) {
            i_4 = field3887;
         }

         int i_5 = i_2 + i_1 + 1;
         if (i_5 > field3888) {
            i_5 = field3888;
         }

         int i_6 = i_4;
         int i_7 = i_2 * i_2;
         int i_8 = 0;
         int i_9 = i_1 - i_4;
         int i_10 = i_9 * i_9;
         int i_11 = i_10 - i_9;
         if (i_1 > i_5) {
            i_1 = i_5;
         }

         int i_12;
         int i_13;
         int i_14;
         int i_15;
         while (i_6 < i_1) {
            while (i_11 <= i_7 || i_10 <= i_7) {
               i_10 = i_10 + i_8 + i_8;
               i_11 += i_8++ + i_8;
            }

            i_12 = i_0 - i_8 + 1;
            if (i_12 < field3890) {
               i_12 = field3890;
            }

            i_13 = i_0 + i_8;
            if (i_13 > field3884) {
               i_13 = field3884;
            }

            i_14 = i_12 + i_6 * field3889;

            for (i_15 = i_12; i_15 < i_13; i_15++) {
               field3886[i_14++] = i_3;
            }

            ++i_6;
            i_10 -= i_9-- + i_9;
            i_11 -= i_9 + i_9;
         }

         i_8 = i_2;
         i_9 = i_6 - i_1;
         i_11 = i_7 + i_9 * i_9;
         i_10 = i_11 - i_2;

         for (i_11 -= i_9; i_6 < i_5; i_10 += i_9++ + i_9) {
            while (i_11 > i_7 && i_10 > i_7) {
               i_11 -= i_8-- + i_8;
               i_10 -= i_8 + i_8;
            }

            i_12 = i_0 - i_8;
            if (i_12 < field3890) {
               i_12 = field3890;
            }

            i_13 = i_0 + i_8;
            if (i_13 > field3884 - 1) {
               i_13 = field3884 - 1;
            }

            i_14 = i_12 + i_6 * field3889;

            for (i_15 = i_12; i_15 <= i_13; i_15++) {
               field3886[i_14++] = i_3;
            }

            ++i_6;
            i_11 = i_11 + i_9 + i_9;
         }

      }
   }

   public static void method6047() {
      field3890 = 0;
      field3887 = 0;
      field3884 = field3889;
      field3888 = field3885;
   }

   public static void method6003(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5) {
      if (i_0 < field3890) {
         i_2 -= field3890 - i_0;
         i_0 = field3890;
      }

      if (i_1 < field3887) {
         i_3 -= field3887 - i_1;
         i_1 = field3887;
      }

      if (i_0 + i_2 > field3884) {
         i_2 = field3884 - i_0;
      }

      if (i_3 + i_1 > field3888) {
         i_3 = field3888 - i_1;
      }

      i_4 = (i_5 * (i_4 & 0xff00ff) >> 8 & 0xff00ff) + (i_5 * (i_4 & 0xff00) >> 8 & 0xff00);
      int i_6 = 256 - i_5;
      int i_7 = field3889 - i_2;
      int i_8 = i_0 + field3889 * i_1;

      for (int i_9 = 0; i_9 < i_3; i_9++) {
         for (int i_10 = -i_2; i_10 < 0; i_10++) {
            int i_11 = field3886[i_8];
            i_11 = ((i_11 & 0xff00ff) * i_6 >> 8 & 0xff00ff) + (i_6 * (i_11 & 0xff00) >> 8 & 0xff00);
            field3886[i_8++] = i_11 + i_4;
         }

         i_8 += i_7;
      }

   }

   public static void method6008(int i_0, int i_1, int i_2, int i_3, int i_4) {
      method6052(i_0, i_1, i_2, i_4);
      method6052(i_0, i_3 + i_1 - 1, i_2, i_4);
      method6023(i_0, i_1, i_3, i_4);
      method6023(i_0 + i_2 - 1, i_1, i_3, i_4);
   }

   static void method6015(int i_0, int i_1, int i_2) {
      if (i_0 >= field3890 && i_1 >= field3887 && i_0 < field3884 && i_1 < field3888) {
         field3886[i_0 + field3889 * i_1] = i_2;
      }
   }

   public static void method6012(int[] ints_0) {
      field3890 = ints_0[0];
      field3887 = ints_0[1];
      field3884 = ints_0[2];
      field3888 = ints_0[3];
   }

   public static void method6005(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5) {
      if (i_2 > 0 && i_3 > 0) {
         int i_6 = 0;
         int i_7 = 65536 / i_3;
         if (i_0 < field3890) {
            i_2 -= field3890 - i_0;
            i_0 = field3890;
         }

         if (i_1 < field3887) {
            i_6 += (field3887 - i_1) * i_7;
            i_3 -= field3887 - i_1;
            i_1 = field3887;
         }

         if (i_0 + i_2 > field3884) {
            i_2 = field3884 - i_0;
         }

         if (i_3 + i_1 > field3888) {
            i_3 = field3888 - i_1;
         }

         int i_8 = field3889 - i_2;
         int i_9 = i_0 + field3889 * i_1;

         for (int i_10 = -i_3; i_10 < 0; i_10++) {
            int i_11 = 65536 - i_6 >> 8;
            int i_12 = i_6 >> 8;
            int i_13 = (i_12 * (i_5 & 0xff00ff) + i_11 * (i_4 & 0xff00ff) & ~0xff00ff) + (i_12 * (i_5 & 0xff00) + i_11 * (i_4 & 0xff00) & 0xff0000) >>> 8;

            for (int i_14 = -i_2; i_14 < 0; i_14++) {
               field3886[i_9++] = i_13;
            }

            i_9 += i_8;
            i_6 += i_7;
         }

      }
   }

   public static void method6018(int i_0, int i_1, int i_2, int[] ints_3, int[] ints_4) {
      int i_5 = i_0 + field3889 * i_1;

      for (i_1 = 0; i_1 < ints_3.length; i_1++) {
         int i_6 = i_5 + ints_3[i_1];

         for (i_0 = -ints_4[i_1]; i_0 < 0; i_0++) {
            field3886[i_6++] = i_2;
         }

         i_5 += field3889;
      }

   }

   public static void method6009(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5) {
      method6011(i_0, i_1, i_2, i_4, i_5);
      method6011(i_0, i_3 + i_1 - 1, i_2, i_4, i_5);
      if (i_3 >= 3) {
         method6013(i_0, i_1 + 1, i_3 - 2, i_4, i_5);
         method6013(i_0 + i_2 - 1, i_1 + 1, i_3 - 2, i_4, i_5);
      }

   }

   public static void method6014(int i_0, int i_1, int i_2, int i_3, int i_4) {
      i_2 -= i_0;
      i_3 -= i_1;
      if (i_3 == 0) {
         if (i_2 >= 0) {
            method6052(i_0, i_1, i_2 + 1, i_4);
         } else {
            method6052(i_0 + i_2, i_1, -i_2 + 1, i_4);
         }

      } else if (i_2 == 0) {
         if (i_3 >= 0) {
            method6023(i_0, i_1, i_3 + 1, i_4);
         } else {
            method6023(i_0, i_3 + i_1, -i_3 + 1, i_4);
         }

      } else {
         if (i_3 + i_2 < 0) {
            i_0 += i_2;
            i_2 = -i_2;
            i_1 += i_3;
            i_3 = -i_3;
         }

         int i_5;
         int i_6;
         if (i_2 > i_3) {
            i_1 <<= 16;
            i_1 += 32768;
            i_3 <<= 16;
            i_5 = (int)Math.floor((double)i_3 / (double)i_2 + 0.5D);
            i_2 += i_0;
            if (i_0 < field3890) {
               i_1 += i_5 * (field3890 - i_0);
               i_0 = field3890;
            }

            if (i_2 >= field3884) {
               i_2 = field3884 - 1;
            }

            while (i_0 <= i_2) {
               i_6 = i_1 >> 16;
               if (i_6 >= field3887 && i_6 < field3888) {
                  field3886[i_0 + i_6 * field3889] = i_4;
               }

               i_1 += i_5;
               ++i_0;
            }
         } else {
            i_0 <<= 16;
            i_0 += 32768;
            i_2 <<= 16;
            i_5 = (int)Math.floor((double)i_2 / (double)i_3 + 0.5D);
            i_3 += i_1;
            if (i_1 < field3887) {
               i_0 += (field3887 - i_1) * i_5;
               i_1 = field3887;
            }

            if (i_3 >= field3888) {
               i_3 = field3888 - 1;
            }

            while (i_1 <= i_3) {
               i_6 = i_0 >> 16;
               if (i_6 >= field3890 && i_6 < field3884) {
                  field3886[i_6 + field3889 * i_1] = i_4;
               }

               i_0 += i_5;
               ++i_1;
            }
         }

      }
   }

   public static void method6070(int i_0, int i_1, int i_2, int i_3, int i_4, int i_5, int i_6, int i_7) {
      if (i_2 > 0 && i_3 > 0) {
         int i_8 = 0;
         int i_9 = 65536 / i_3;
         if (i_0 < field3890) {
            i_2 -= field3890 - i_0;
            i_0 = field3890;
         }

         if (i_1 < field3887) {
            i_8 += (field3887 - i_1) * i_9;
            i_3 -= field3887 - i_1;
            i_1 = field3887;
         }

         if (i_0 + i_2 > field3884) {
            i_2 = field3884 - i_0;
         }

         if (i_3 + i_1 > field3888) {
            i_3 = field3888 - i_1;
         }

         int i_10 = field3889 - i_2;
         int i_11 = i_0 + field3889 * i_1;

         for (int i_12 = -i_3; i_12 < 0; i_12++) {
            int i_13 = 65536 - i_8 >> 8;
            int i_14 = i_8 >> 8;
            int i_15 = (i_13 * i_6 + i_14 * i_7 & 0xff00) >>> 8;
            if (i_15 == 0) {
               i_11 += field3889;
               i_8 += i_9;
            } else {
               int i_16 = (i_14 * (i_5 & 0xff00ff) + i_13 * (i_4 & 0xff00ff) & ~0xff00ff) + (i_14 * (i_5 & 0xff00) + i_13 * (i_4 & 0xff00) & 0xff0000) >>> 8;
               int i_17 = 255 - i_15;
               int i_18 = ((i_16 & 0xff00ff) * i_15 >> 8 & 0xff00ff) + (i_15 * (i_16 & 0xff00) >> 8 & 0xff00);

               for (int i_19 = -i_2; i_19 < 0; i_19++) {
                  int i_20 = field3886[i_11];
                  if (i_20 == 0) {
                     field3886[i_11++] = i_18;
                  } else {
                     i_20 = ((i_20 & 0xff00ff) * i_17 >> 8 & 0xff00ff) + (i_17 * (i_20 & 0xff00) >> 8 & 0xff00);
                     field3886[i_11++] = i_18 + i_20;
                  }
               }

               i_11 += i_10;
               i_8 += i_9;
            }
         }

      }
   }

   public static void method5997(int i_0, int i_1, int i_2, int i_3) {
      if (field3890 < i_0) {
         field3890 = i_0;
      }

      if (field3887 < i_1) {
         field3887 = i_1;
      }

      if (field3884 > i_2) {
         field3884 = i_2;
      }

      if (field3888 > i_3) {
         field3888 = i_3;
      }

   }

   static void method6011(int i_0, int i_1, int i_2, int i_3, int i_4) {
      if (i_1 >= field3887 && i_1 < field3888) {
         if (i_0 < field3890) {
            i_2 -= field3890 - i_0;
            i_0 = field3890;
         }

         if (i_0 + i_2 > field3884) {
            i_2 = field3884 - i_0;
         }

         int i_5 = 256 - i_4;
         int i_6 = (i_3 >> 16 & 0xff) * i_4;
         int i_7 = (i_3 >> 8 & 0xff) * i_4;
         int i_8 = i_4 * (i_3 & 0xff);
         int i_12 = i_0 + field3889 * i_1;

         for (int i_13 = 0; i_13 < i_2; i_13++) {
            int i_9 = i_5 * (field3886[i_12] >> 16 & 0xff);
            int i_10 = (field3886[i_12] >> 8 & 0xff) * i_5;
            int i_11 = i_5 * (field3886[i_12] & 0xff);
            int i_14 = (i_8 + i_11 >> 8) + (i_6 + i_9 >> 8 << 16) + (i_7 + i_10 >> 8 << 8);
            field3886[i_12++] = i_14;
         }

      }
   }

   public static void method6000() {
      int i_0 = 0;

      int i_1;
      for (i_1 = field3889 * field3885 - 7; i_0 < i_1; field3886[i_0++] = 0) {
         field3886[i_0++] = 0;
         field3886[i_0++] = 0;
         field3886[i_0++] = 0;
         field3886[i_0++] = 0;
         field3886[i_0++] = 0;
         field3886[i_0++] = 0;
         field3886[i_0++] = 0;
      }

      for (i_1 += 7; i_0 < i_1; field3886[i_0++] = 0) {
         ;
      }

   }

   static void method6013(int i_0, int i_1, int i_2, int i_3, int i_4) {
      if (i_0 >= field3890 && i_0 < field3884) {
         if (i_1 < field3887) {
            i_2 -= field3887 - i_1;
            i_1 = field3887;
         }

         if (i_2 + i_1 > field3888) {
            i_2 = field3888 - i_1;
         }

         int i_5 = 256 - i_4;
         int i_6 = (i_3 >> 16 & 0xff) * i_4;
         int i_7 = (i_3 >> 8 & 0xff) * i_4;
         int i_8 = i_4 * (i_3 & 0xff);
         int i_12 = i_0 + field3889 * i_1;

         for (int i_13 = 0; i_13 < i_2; i_13++) {
            int i_9 = i_5 * (field3886[i_12] >> 16 & 0xff);
            int i_10 = (field3886[i_12] >> 8 & 0xff) * i_5;
            int i_11 = i_5 * (field3886[i_12] & 0xff);
            int i_14 = (i_8 + i_11 >> 8) + (i_6 + i_9 >> 8 << 16) + (i_7 + i_10 >> 8 << 8);
            field3886[i_12] = i_14;
            i_12 += field3889;
         }

      }
   }

}
