import java.applet.Applet;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.ImageObserver;
import java.net.URL;

public abstract class class62 extends Applet implements Runnable, FocusListener, WindowListener {

   static class246 field478;
   static class329 field481;
   Frame field468;
   int field464;
   int field467;
   static class62 field475 = null;
   static int field451 = 0;
   static long field452 = 0L;
   static boolean field482 = false;
   static int field455 = 20;
   static int field465 = 1;
   protected static int field457 = 0;
   protected static long[] field449 = new long[32];
   protected static long[] field459 = new long[32];
   static int field471 = 500;
   static volatile boolean field456 = true;
   static long field479 = -1L;
   static long field480 = -1L;
   boolean field454 = false;
   int field462 = 0;
   int field463 = 0;
   volatile boolean field470 = true;
   boolean field474 = false;
   volatile boolean field473 = false;
   volatile long field472 = 0L;
   final EventQueue field477;
   Canvas field458;
   protected int field460;
   protected int field461;
   protected static class175 field469;
   class53 field453;
   Clipboard field476;
   int field466;
   int field450;

   protected class62() {
      EventQueue eventqueue_1 = null;

      try {
         eventqueue_1 = Toolkit.getDefaultToolkit().getSystemEventQueue();
      } catch (Throwable throwable_3) {
         ;
      }

      this.field477 = eventqueue_1;
      class146.method3316(new class55(), 398077002);
   }

   public final synchronized void paint(Graphics graphics_1) {
      if (this == field475 && !field482) {
         this.field470 = true;
         if (class298.method5270(255749540) - this.field472 > 1000L) {
            Rectangle rectangle_2 = graphics_1.getClipBounds();
            if (rectangle_2 == null || rectangle_2.width >= class286.field3638 && rectangle_2.height >= class143.field1892) {
               this.field473 = true;
            }
         }

      }
   }

   public final void destroy() {
      if (this == field475 && !field482) {
         field452 = class298.method5270(255749540);
         class236.method4132(5000L);
         this.method990(646391246);
      }
   }

   protected final boolean method1024(int i_1) {
      String string_2 = this.getDocumentBase().getHost().toLowerCase();
      if (!string_2.equals("jagex.com") && !string_2.endsWith(".jagex.com")) {
         if (!string_2.equals("runescape.com") && !string_2.endsWith(".runescape.com")) {
            if (string_2.endsWith("127.0.0.1")) {
               return true;
            } else {
               while (string_2.length() > 0 && string_2.charAt(string_2.length() - 1) >= 48 && string_2.charAt(string_2.length() - 1) <= 57) {
                  string_2 = string_2.substring(0, string_2.length() - 1);
               }

               if (string_2.endsWith("192.168.1.")) {
                  return true;
               } else {
                  this.method996("invalidhost", -994849210);
                  return false;
               }
            }
         } else {
            return true;
         }
      } else {
         return true;
      }
   }

   class329 method969(int i_1) {
      Container container_2 = this.method997((short) -27901);
      int i_3 = Math.max(container_2.getWidth(), this.field464);
      int i_4 = Math.max(container_2.getHeight(), this.field467);
      if (this.field468 != null) {
         Insets insets_5 = this.field468.getInsets();
         i_3 -= insets_5.right + insets_5.left;
         i_4 -= insets_5.bottom + insets_5.top;
      }

      return new class329(i_3, i_4);
   }

   Container method997(short s_1) {
      return (Container) (this.field468 != null ? this.field468 : this) ;
   }

   protected void method996(String string_1, int i_2) {
      if (!this.field454) {
         this.field454 = true;
         System.out.println("error_game_" + string_1);

         try {
            this.getAppletContext().showDocument(new URL(this.getCodeBase(), "error_game_" + string_1 + ".ws"), "_self");
         } catch (Exception exception_4) {
            ;
         }

      }
   }

   final void method985(int i_1) {
      this.field474 = true;
   }

   final synchronized void method979(int i_1) {
      Container container_2 = this.method997((short) -16488);
      if (this.field458 != null) {
         this.field458.removeFocusListener(this);
         container_2.remove(this.field458);
      }

      class286.field3638 = Math.max(container_2.getWidth(), this.field464);
      class143.field1892 = Math.max(container_2.getHeight(), this.field467);
      Insets insets_3;
      if (this.field468 != null) {
         insets_3 = this.field468.getInsets();
         class286.field3638 -= insets_3.left + insets_3.right;
         class143.field1892 -= insets_3.bottom + insets_3.top;
      }

      this.field458 = new class57(this);
      container_2.setBackground(Color.BLACK);
      container_2.setLayout((LayoutManager) null);
      container_2.add(this.field458);
      this.field458.setSize(class286.field3638, class143.field1892);
      this.field458.setVisible(true);
      this.field458.setBackground(Color.BLACK);
      if (container_2 == this.field468) {
         insets_3 = this.field468.getInsets();
         this.field458.setLocation(insets_3.left + this.field462, this.field463 + insets_3.top);
      } else {
         this.field458.setLocation(this.field462, this.field463);
      }

      this.field458.addFocusListener(this);
      this.field458.requestFocus();
      this.field470 = true;
      if (class27.field150 != null && class286.field3638 == class27.field150.field3892 && class143.field1892 == class27.field150.field3893) {
         ((class61) class27.field150).method960(this.field458, (byte) 93);
         class27.field150.vmethod6077(0, 0, 1343964035);
      } else {
         class27.field150 = new class61(class286.field3638, class143.field1892, this.field458);
      }

      this.field473 = false;
      this.field472 = class298.method5270(255749540);
   }

   final synchronized void method990(int i_1) {
      if (!field482) {
         field482 = true;

         try {
            this.field458.removeFocusListener(this);
         } catch (Exception exception_6) {
            ;
         }

         try {
            this.vmethod1598(-6923156);
         } catch (Exception exception_5) {
            ;
         }

         if (this.field468 != null) {
            try {
               System.exit(0);
            } catch (Throwable throwable_4) {
               ;
            }
         }

         if (field469 != null) {
            try {
               field469.method3520(60003222);
            } catch (Exception exception_3) {
               ;
            }
         }

         this.vmethod1762((byte) 96);
      }
   }

   protected abstract void vmethod1598(int var1);

   final void method974(int i_1) {
      Container container_2 = this.method997((short) -27008);
      if (container_2 != null) {
         class329 class329_3 = this.method969(-1535593933);
         this.field460 = Math.max(class329_3.field3879, this.field464);
         this.field461 = Math.max(class329_3.field3880, this.field467);
         if (this.field460 <= 0) {
            this.field460 = 1;
         }

         if (this.field461 <= 0) {
            this.field461 = 1;
         }

         class286.field3638 = Math.min(this.field460, this.field466);
         class143.field1892 = Math.min(this.field461, this.field450);
         this.field462 = (this.field460 - class286.field3638) / 2;
         this.field463 = 0;
         this.field458.setSize(class286.field3638, class143.field1892);
         class27.field150 = new class61(class286.field3638, class143.field1892, this.field458);
         if (container_2 == this.field468) {
            Insets insets_4 = this.field468.getInsets();
            this.field458.setLocation(insets_4.left + this.field462, this.field463 + insets_4.top);
         } else {
            this.field458.setLocation(this.field462, this.field463);
         }

         this.field470 = true;
         this.vmethod1490(312942940);
      }
   }

   protected abstract void vmethod1718(byte var1);

   protected abstract void vmethod1762(byte var1);

   protected final void method1050(int i_1) {
      class57.field425 = null;
      class16.field65 = null;
      class137.field1754 = null;
   }

   protected final void method978(int i_1, int i_2, int i_3, byte b_4) {
      try {
         if (field475 != null) {
            ++field451;
            if (field451 >= 3) {
               this.method996("alreadyloaded", 1675028511);
               return;
            }

            this.getAppletContext().showDocument(this.getDocumentBase(), "_self");
            return;
         }

         field475 = this;
         class286.field3638 = i_1;
         class143.field1892 = i_2;
         class351.field4084 = i_3;
         class351.field4085 = this;
         if (field469 == null) {
            field469 = new class175();
         }

         field469.method3521(this, 1, -1137156097);
      } catch (Exception exception_6) {
         class223.method4011((String) null, exception_6, 1153680359);
         this.method996("crash", -52529864);
      }

   }

   void method987(int i_1) {
      long long_2 = class298.method5270(255749540);
      long long_4 = field459[class99.field1279];
      field459[class99.field1279] = long_2;
      class99.field1279 = class99.field1279 + 1 & 0x1f;
      if (long_4 != 0L && long_2 > long_4) {
         ;
      }

      synchronized(this) {
         class71.field573 = field456;
      }

      this.vmethod1434(-1376035132);
   }

   void method983(int i_1) {
      Container container_2 = this.method997((short) -4479);
      long long_3 = class298.method5270(255749540);
      long long_5 = field449[class104.field1327];
      field449[class104.field1327] = long_3;
      class104.field1327 = class104.field1327 + 1 & 0x1f;
      if (long_5 != 0L && long_3 > long_5) {
         int i_7 = (int)(long_3 - long_5);
         field457 = ((i_7 >> 1) + 32000) / i_7;
      }

      if (++field471 - 1 > 50) {
         field471 -= 50;
         this.field470 = true;
         this.field458.setSize(class286.field3638, class143.field1892);
         this.field458.setVisible(true);
         if (container_2 == this.field468) {
            Insets insets_8 = this.field468.getInsets();
            this.field458.setLocation(this.field462 + insets_8.left, insets_8.top + this.field463);
         } else {
            this.field458.setLocation(this.field462, this.field463);
         }
      }

      if (this.field473) {
         this.method977(-2006885359);
      }

      this.method984((byte) -26);
      this.vmethod1365(this.field470, -20163413);
      if (this.field470) {
         this.method976(-2111621500);
      }

      this.field470 = false;
   }

   protected final void method994(int i_1, String string_2, boolean bool_3, int i_4) {
      try {
         Graphics graphics_5 = this.field458.getGraphics();
         if (class16.field65 == null) {
            class16.field65 = new Font("Helvetica", 1, 13);
            class137.field1754 = this.field458.getFontMetrics(class16.field65);
         }

         if (bool_3) {
            graphics_5.setColor(Color.black);
            graphics_5.fillRect(0, 0, class286.field3638, class143.field1892);
         }

         Color color_6 = new Color(140, 17, 17);

         try {
            if (class57.field425 == null) {
               class57.field425 = this.field458.createImage(304, 34);
            }

            Graphics graphics_7 = class57.field425.getGraphics();
            graphics_7.setColor(color_6);
            graphics_7.drawRect(0, 0, 303, 33);
            graphics_7.fillRect(2, 2, i_1 * 3, 30);
            graphics_7.setColor(Color.black);
            graphics_7.drawRect(1, 1, 301, 31);
            graphics_7.fillRect(i_1 * 3 + 2, 2, 300 - i_1 * 3, 30);
            graphics_7.setFont(class16.field65);
            graphics_7.setColor(Color.white);
            graphics_7.drawString(string_2, (304 - class137.field1754.stringWidth(string_2)) / 2, 22);
            graphics_5.drawImage(class57.field425, class286.field3638 / 2 - 152, class143.field1892 / 2 - 18, (ImageObserver) null);
         } catch (Exception exception_10) {
            int i_8 = class286.field3638 / 2 - 152;
            int i_9 = class143.field1892 / 2 - 18;
            graphics_5.setColor(color_6);
            graphics_5.drawRect(i_8, i_9, 303, 33);
            graphics_5.fillRect(i_8 + 2, i_9 + 2, i_1 * 3, 30);
            graphics_5.setColor(Color.black);
            graphics_5.drawRect(i_8 + 1, i_9 + 1, 301, 31);
            graphics_5.fillRect(i_1 * 3 + i_8 + 2, i_9 + 2, 300 - i_1 * 3, 30);
            graphics_5.setFont(class16.field65);
            graphics_5.setColor(Color.white);
            graphics_5.drawString(string_2, i_8 + (304 - class137.field1754.stringWidth(string_2)) / 2, i_9 + 22);
         }
      } catch (Exception exception_11) {
         this.field458.repaint();
      }

   }

   protected abstract void vmethod1434(int var1);

   final void method968(Object object_1, int i_2) {
      if (this.field477 != null) {
         for (int i_3 = 0; i_3 < 50 && this.field477.peekEvent() != null; i_3++) {
            class236.method4132(1L);
         }

         if (object_1 != null) {
            this.field477.postEvent(new ActionEvent(object_1, 1001, "dummy"));
         }

      }
   }

   protected final void method972(int i_1) {
      class32.method354((byte) 14);
      class100.method2247(this.field458, -826057575);
   }

   protected final void method973(byte b_1) {
      Canvas canvas_2 = this.field458;
      canvas_2.addMouseListener(class63.field485);
      canvas_2.addMouseMotionListener(class63.field485);
      canvas_2.addFocusListener(class63.field485);
   }

   protected class177 method1118(int i_1) {
      if (this.field453 == null) {
         this.field453 = new class53();
         this.field453.method847(this.field458, (byte) -124);
      }

      return this.field453;
   }

   protected void method970(int i_1) {
      this.field476 = this.getToolkit().getSystemClipboard();
   }

   protected abstract void vmethod1490(int var1);

   protected final void method967(int i_1, int i_2, int i_3) {
      if (this.field466 != i_1 || i_2 != this.field450) {
         this.method985(-945352549);
      }

      this.field466 = i_1;
      this.field450 = i_2;
   }

   final void method977(int i_1) {
      Canvas canvas_2 = this.field458;
      canvas_2.removeKeyListener(class54.field407);
      canvas_2.removeFocusListener(class54.field407);
      class54.field410 = -1;
      Canvas canvas_3 = this.field458;
      canvas_3.removeMouseListener(class63.field485);
      canvas_3.removeMouseMotionListener(class63.field485);
      canvas_3.removeFocusListener(class63.field485);
      class63.field494 = 0;
      if (this.field453 != null) {
         this.field453.method853(this.field458, -1451320085);
      }

      this.method979(1254255664);
      class100.method2247(this.field458, 1144578244);
      Canvas canvas_4 = this.field458;
      canvas_4.addMouseListener(class63.field485);
      canvas_4.addMouseMotionListener(class63.field485);
      canvas_4.addFocusListener(class63.field485);
      if (this.field453 != null) {
         this.field453.method847(this.field458, (byte) -121);
      }

      this.method985(287076222);
   }

   final void method984(byte b_1) {
      class329 class329_2 = this.method969(-2080787573);
      if (class329_2.field3879 != this.field460 || class329_2.field3880 != this.field461 || this.field474) {
         this.method974(1383107169);
         this.field474 = false;
      }

   }

   protected abstract void vmethod1365(boolean var1, int var2);

   void method976(int i_1) {
      int i_2 = this.field462;
      int i_3 = this.field463;
      int i_4 = this.field460 - class286.field3638 - i_2;
      int i_5 = this.field461 - class143.field1892 - i_3;
      if (i_2 > 0 || i_4 > 0 || i_3 > 0 || i_5 > 0) {
         try {
            Container container_6 = this.method997((short) -833);
            int i_7 = 0;
            int i_8 = 0;
            if (container_6 == this.field468) {
               Insets insets_9 = this.field468.getInsets();
               i_7 = insets_9.left;
               i_8 = insets_9.top;
            }

            Graphics graphics_11 = container_6.getGraphics();
            graphics_11.setColor(Color.black);
            if (i_2 > 0) {
               graphics_11.fillRect(i_7, i_8, i_2, this.field461);
            }

            if (i_3 > 0) {
               graphics_11.fillRect(i_7, i_8, this.field460, i_3);
            }

            if (i_4 > 0) {
               graphics_11.fillRect(i_7 + this.field460 - i_4, i_8, i_4, this.field461);
            }

            if (i_5 > 0) {
               graphics_11.fillRect(i_7, i_8 + this.field461 - i_5, this.field460, i_5);
            }
         } catch (Exception exception_10) {
            ;
         }
      }

   }

   protected final boolean method1092(int i_1) {
      return this.field468 != null;
   }

   protected void method971(String string_1, int i_2) {
      this.field476.setContents(new StringSelection(string_1), (ClipboardOwner) null);
   }

   public final void focusGained(FocusEvent focusevent_1) {
      field456 = true;
      this.field470 = true;
   }

   public final void focusLost(FocusEvent focusevent_1) {
      field456 = false;
   }

   public final void windowClosed(WindowEvent windowevent_1) {
   }

   public final void windowClosing(WindowEvent windowevent_1) {
      this.destroy();
   }

   public final void windowDeactivated(WindowEvent windowevent_1) {
   }

   public final void windowIconified(WindowEvent windowevent_1) {
   }

   public final void windowOpened(WindowEvent windowevent_1) {
   }

   public abstract void init();

   public final void stop() {
      if (this == field475 && !field482) {
         field452 = class298.method5270(255749540) + 4000L;
      }
   }

   public final void start() {
      if (this == field475 && !field482) {
         field452 = 0L;
      }
   }

   public void run() {
      try {
         if (class175.field2052 != null) {
            String string_1 = class175.field2052.toLowerCase();
            if (string_1.indexOf("sun") != -1 || string_1.indexOf("apple") != -1) {
               String string_2 = class175.field2051;
               if (string_2.equals("1.1") || string_2.startsWith("1.1.") || string_2.equals("1.2") || string_2.startsWith("1.2.") || string_2.equals("1.3") || string_2.startsWith("1.3.") || string_2.equals("1.4") || string_2.startsWith("1.4.") || string_2.equals("1.5") || string_2.startsWith("1.5.") || string_2.equals("1.6.0")) {
                  this.method996("wrongjava", 2134570011);
                  return;
               }

               if (string_2.startsWith("1.6.0_")) {
                  int i_3;
                  for (i_3 = 6; i_3 < string_2.length() && class39.method592(string_2.charAt(i_3), -1443206500); i_3++) {
                     ;
                  }

                  String string_4 = string_2.substring(6, i_3);
                  if (class242.method4155(string_4, -222972673) && class279.method4974(string_4, (byte) 7) < 10) {
                     this.method996("wrongjava", -1374273478);
                     return;
                  }
               }

               field465 = 5;
            }
         }

         this.setFocusCycleRoot(true);
         this.method979(1254255664);
         this.vmethod1718((byte) -91);

         Object obj_8;
         try {
            obj_8 = new class172();
         } catch (Throwable throwable_6) {
            obj_8 = new class173();
         }

         class225.field2574 = (class179) obj_8;

         while (field452 == 0L || class298.method5270(255749540) < field452) {
            class8.field31 = class225.field2574.vmethod3547(field455, field465, -680717242);

            for (int i_5 = 0; i_5 < class8.field31; i_5++) {
               this.method987(176376535);
            }

            this.method983(-2055112654);
            this.method968(this.field458, 136428180);
         }
      } catch (Exception exception_7) {
         class223.method4011((String) null, exception_7, 2079924758);
         this.method996("crash", 1606890227);
      }

      this.method990(2080217431);
   }

   public final void windowActivated(WindowEvent windowevent_1) {
   }

   public final void windowDeiconified(WindowEvent windowevent_1) {
   }

   public final void update(Graphics graphics_1) {
      this.paint(graphics_1);
   }

   static void method1087(String string_0, String string_1, String string_2, int i_3) {
      class94.field1193 = string_0;
      class94.field1194 = string_1;
      class94.field1195 = string_2;
   }

}
