public class class231 {

   static boolean field2772;

   public static final boolean method4119(int i_0, int i_1, class183 class183_2, class181 class181_3, short s_4) {
      int i_5 = i_0;
      int i_6 = i_1;
      byte b_7 = 64;
      byte b_8 = 64;
      int i_9 = i_0 - b_7;
      int i_10 = i_1 - b_8;
      class182.field2111[b_7][b_8] = 99;
      class182.field2114[b_7][b_8] = 0;
      byte b_11 = 0;
      int i_12 = 0;
      class182.field2118[b_11] = i_0;
      byte b_10001 = b_11;
      int i_19 = b_11 + 1;
      class182.field2119[b_10001] = i_1;
      int[][] ints_13 = class181_3.field2107;

      while (i_12 != i_19) {
         i_5 = class182.field2118[i_12];
         i_6 = class182.field2119[i_12];
         i_12 = i_12 + 1 & 0xfff;
         int i_17 = i_5 - i_9;
         int i_18 = i_6 - i_10;
         int i_14 = i_5 - class181_3.field2103;
         int i_15 = i_6 - class181_3.field2104;
         if (class183_2.vmethod3613(2, i_5, i_6, class181_3, (byte) 104)) {
            class182.field2115 = i_5;
            class182.field2121 = i_6;
            return true;
         }

         int i_16 = class182.field2114[i_17][i_18] + 1;
         if (i_17 > 0 && class182.field2111[i_17 - 1][i_18] == 0 && (ints_13[i_14 - 1][i_15] & 0x124010e) == 0 && (ints_13[i_14 - 1][i_15 + 1] & 0x1240138) == 0) {
            class182.field2118[i_19] = i_5 - 1;
            class182.field2119[i_19] = i_6;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17 - 1][i_18] = 2;
            class182.field2114[i_17 - 1][i_18] = i_16;
         }

         if (i_17 < 126 && class182.field2111[i_17 + 1][i_18] == 0 && (ints_13[i_14 + 2][i_15] & 0x1240183) == 0 && (ints_13[i_14 + 2][i_15 + 1] & 0x12401e0) == 0) {
            class182.field2118[i_19] = i_5 + 1;
            class182.field2119[i_19] = i_6;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17 + 1][i_18] = 8;
            class182.field2114[i_17 + 1][i_18] = i_16;
         }

         if (i_18 > 0 && class182.field2111[i_17][i_18 - 1] == 0 && (ints_13[i_14][i_15 - 1] & 0x124010e) == 0 && (ints_13[i_14 + 1][i_15 - 1] & 0x1240183) == 0) {
            class182.field2118[i_19] = i_5;
            class182.field2119[i_19] = i_6 - 1;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17][i_18 - 1] = 1;
            class182.field2114[i_17][i_18 - 1] = i_16;
         }

         if (i_18 < 126 && class182.field2111[i_17][i_18 + 1] == 0 && (ints_13[i_14][i_15 + 2] & 0x1240138) == 0 && (ints_13[i_14 + 1][i_15 + 2] & 0x12401e0) == 0) {
            class182.field2118[i_19] = i_5;
            class182.field2119[i_19] = i_6 + 1;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17][i_18 + 1] = 4;
            class182.field2114[i_17][i_18 + 1] = i_16;
         }

         if (i_17 > 0 && i_18 > 0 && class182.field2111[i_17 - 1][i_18 - 1] == 0 && (ints_13[i_14 - 1][i_15] & 0x124013e) == 0 && (ints_13[i_14 - 1][i_15 - 1] & 0x124010e) == 0 && (ints_13[i_14][i_15 - 1] & 0x124018f) == 0) {
            class182.field2118[i_19] = i_5 - 1;
            class182.field2119[i_19] = i_6 - 1;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17 - 1][i_18 - 1] = 3;
            class182.field2114[i_17 - 1][i_18 - 1] = i_16;
         }

         if (i_17 < 126 && i_18 > 0 && class182.field2111[i_17 + 1][i_18 - 1] == 0 && (ints_13[i_14 + 1][i_15 - 1] & 0x124018f) == 0 && (ints_13[i_14 + 2][i_15 - 1] & 0x1240183) == 0 && (ints_13[i_14 + 2][i_15] & 0x12401e3) == 0) {
            class182.field2118[i_19] = i_5 + 1;
            class182.field2119[i_19] = i_6 - 1;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17 + 1][i_18 - 1] = 9;
            class182.field2114[i_17 + 1][i_18 - 1] = i_16;
         }

         if (i_17 > 0 && i_18 < 126 && class182.field2111[i_17 - 1][i_18 + 1] == 0 && (ints_13[i_14 - 1][i_15 + 1] & 0x124013e) == 0 && (ints_13[i_14 - 1][i_15 + 2] & 0x1240138) == 0 && (ints_13[i_14][i_15 + 2] & 0x12401f8) == 0) {
            class182.field2118[i_19] = i_5 - 1;
            class182.field2119[i_19] = i_6 + 1;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17 - 1][i_18 + 1] = 6;
            class182.field2114[i_17 - 1][i_18 + 1] = i_16;
         }

         if (i_17 < 126 && i_18 < 126 && class182.field2111[i_17 + 1][i_18 + 1] == 0 && (ints_13[i_14 + 1][i_15 + 2] & 0x12401f8) == 0 && (ints_13[i_14 + 2][i_15 + 2] & 0x12401e0) == 0 && (ints_13[i_14 + 2][i_15 + 1] & 0x12401e3) == 0) {
            class182.field2118[i_19] = i_5 + 1;
            class182.field2119[i_19] = i_6 + 1;
            i_19 = i_19 + 1 & 0xfff;
            class182.field2111[i_17 + 1][i_18 + 1] = 12;
            class182.field2114[i_17 + 1][i_18 + 1] = i_16;
         }
      }

      class182.field2115 = i_5;
      class182.field2121 = i_6;
      return false;
   }

   static int method4121(int i_0, class101 class101_1, boolean bool_2, int i_3) {
      int i_4;
      int i_5;
      if (i_0 == 100) {
         class253.field3267 -= 3;
         i_4 = class85.field1095[class253.field3267];
         i_5 = class85.field1095[class253.field3267 + 1];
         int i_6 = class85.field1095[class253.field3267 + 2];
         if (i_5 == 0) {
            throw new RuntimeException();
         } else {
            class226 class226_7 = class181.method3610(i_4, -1115187610);
            if (class226_7.field2672 == null) {
               class226_7.field2672 = new class226[i_6 + 1];
            }

            if (class226_7.field2672.length <= i_6) {
               class226[] arr_8 = new class226[i_6 + 1];

               for (int i_9 = 0; i_9 < class226_7.field2672.length; i_9++) {
                  arr_8[i_9] = class226_7.field2672[i_9];
               }

               class226_7.field2672 = arr_8;
            }

            if (i_6 > 0 && class226_7.field2672[i_6 - 1] == null) {
               throw new RuntimeException("" + (i_6 - 1));
            } else {
               class226 class226_13 = new class226();
               class226_13.field2588 = i_5;
               class226_13.field2670 = class226_13.field2586 = class226_7.field2586;
               class226_13.field2587 = i_6;
               class226_13.field2585 = true;
               class226_7.field2672[i_6] = class226_13;
               if (bool_2) {
                  class223.field2561 = class226_13;
               } else {
                  class253.field3264 = class226_13;
               }

               class181.method3609(class226_7, 1438942092);
               return 1;
            }
         }
      } else {
         class226 class226_10;
         if (i_0 == 101) {
            class226_10 = bool_2 ? class223.field2561 : class253.field3264;
            class226 class226_11 = class181.method3610(class226_10.field2586, -1945647480);
            class226_11.field2672[class226_10.field2587] = null;
            class181.method3609(class226_11, 916360956);
            return 1;
         } else if (i_0 == 102) {
            class226_10 = class181.method3610(class85.field1095[--class253.field3267], -666910698);
            class226_10.field2672 = null;
            class181.method3609(class226_10, 608466359);
            return 1;
         } else if (i_0 != 200) {
            if (i_0 == 201) {
               class226_10 = class181.method3610(class85.field1095[--class253.field3267], -1921295439);
               if (class226_10 != null) {
                  class85.field1095[++class253.field3267 - 1] = 1;
                  if (bool_2) {
                     class223.field2561 = class226_10;
                  } else {
                     class253.field3264 = class226_10;
                  }
               } else {
                  class85.field1095[++class253.field3267 - 1] = 0;
               }

               return 1;
            } else {
               return 2;
            }
         } else {
            class253.field3267 -= 2;
            i_4 = class85.field1095[class253.field3267];
            i_5 = class85.field1095[class253.field3267 + 1];
            class226 class226_12 = class92.method2103(i_4, i_5, -100010964);
            if (class226_12 != null && i_5 != -1) {
               class85.field1095[++class253.field3267 - 1] = 1;
               if (bool_2) {
                  class223.field2561 = class226_12;
               } else {
                  class253.field3264 = class226_12;
               }
            } else {
               class85.field1095[++class253.field3267 - 1] = 0;
            }

            return 1;
         }
      }
   }

   static final void method4120(int i_0) {
      client.field762 = 0;
      int i_1 = (class223.field2562.field990 >> 7) + class243.field3156;
      int i_2 = (class223.field2562.field938 >> 7) + class41.field300;
      if (i_1 >= 3053 && i_1 <= 3156 && i_2 >= 3056 && i_2 <= 3136) {
         client.field762 = 1;
      }

      if (i_1 >= 3072 && i_1 <= 3118 && i_2 >= 9492 && i_2 <= 9535) {
         client.field762 = 1;
      }

      if (client.field762 == 1 && i_1 >= 3139 && i_1 <= 3199 && i_2 >= 3008 && i_2 <= 3062) {
         client.field762 = 0;
      }

   }

}
