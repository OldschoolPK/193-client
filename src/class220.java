public class class220 extends class184 {

   static int field2535;
   public final int field2537;
   public final int field2532;
   public final int[] field2533;
   public final int[] field2531;

   class220(int i_1, int i_2, int[] ints_3, int[] ints_4, int i_5) {
      this.field2537 = i_1;
      this.field2532 = i_2;
      this.field2533 = ints_3;
      this.field2531 = ints_4;
   }

   public boolean method3981(int i_1, int i_2, short s_3) {
      if (i_2 >= 0 && i_2 < this.field2531.length) {
         int i_4 = this.field2531[i_2];
         if (i_1 >= i_4 && i_1 <= i_4 + this.field2533[i_2]) {
            return true;
         }
      }

      return false;
   }

}
